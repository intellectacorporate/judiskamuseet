


<h2>Statistics for http://www.u5645470.fsdata.se, Week of 2019-7-29 to 2019-8-4</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 10 of 18 File Types (Extensions), 
Sorted by Access Count

</h3>
<p>Individual file types as determined by file extensions. All URLs that
do not contain an extension are counted as directories.</p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Type</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th class="hogerstalld">1</th>
	<td>
	Directory (folder)		
	</td>
	<td align="right">10,934</td>
	<td align="right">22.11</td>
	<td align="right">109,661,597</td>
	<td align="right">1.33</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	js		
	</td>
	<td align="right">10,733</td>
	<td align="right">21.71</td>
	<td align="right">140,263,675</td>
	<td align="right">1.71</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	css		
	</td>
	<td align="right">8,897</td>
	<td align="right">17.99</td>
	<td align="right">142,184,166</td>
	<td align="right">1.73</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	jpg		
	</td>
	<td align="right">8,112</td>
	<td align="right">16.41</td>
	<td align="right">7,075,777,345</td>
	<td align="right">86.01</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	png		
	</td>
	<td align="right">7,104</td>
	<td align="right">14.37</td>
	<td align="right">449,211,785</td>
	<td align="right">5.46</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	php		
	</td>
	<td align="right">1,624</td>
	<td align="right">3.28</td>
	<td align="right">4,398,621</td>
	<td align="right">0.05</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	ttf		
	</td>
	<td align="right">1,164</td>
	<td align="right">2.35</td>
	<td align="right">103,873,812</td>
	<td align="right">1.26</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	txt		
	</td>
	<td align="right">534</td>
	<td align="right">1.08</td>
	<td align="right">36,295</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	jpeg		
	</td>
	<td align="right">158</td>
	<td align="right">0.32</td>
	<td align="right">136,033,328</td>
	<td align="right">1.65</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">10</th>
	<td>
	gif		
	</td>
	<td align="right">79</td>
	<td align="right">0.16</td>
	<td align="right">276,141</td>
	<td align="right">0.00</td>
</tr>

</tbody>
</table>

