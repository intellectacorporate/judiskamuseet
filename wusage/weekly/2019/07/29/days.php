


<h2>Statistics for http://www.u5645470.fsdata.se, Week of 2019-7-29 to 2019-8-4</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Traffic by Day</h3>
<img src="d.png" width="468" height="351">
<br /><br />


<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Date</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	<th scope='col'>Bits per Sec</th>
	<th scope='col'>Bytes per Sec</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th align="right">2019-7-29</th>
	<td align="right">7,949	</td>
	<td align="right">16.08	</td>
	<td align="right">1,215,260,033	</td>
	<td align="right">14.77	</td>
	<td align="right">354	</td>
	<td align="right">14.78	</td>
	<td align="right">112,524.08	</td>
	<td align="right">14,065.51	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-7-30</th>
	<td align="right">8,775	</td>
	<td align="right">17.75	</td>
	<td align="right">1,532,632,015	</td>
	<td align="right">18.63	</td>
	<td align="right">407	</td>
	<td align="right">16.99	</td>
	<td align="right">141,910.37	</td>
	<td align="right">17,738.80	</td>
</tr>
<tr class="udda">
	<th align="right">2019-7-31</th>
	<td align="right">8,381	</td>
	<td align="right">16.95	</td>
	<td align="right">1,456,968,062	</td>
	<td align="right">17.71	</td>
	<td align="right">408	</td>
	<td align="right">17.04	</td>
	<td align="right">134,904.45	</td>
	<td align="right">16,863.06	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-8-1</th>
	<td align="right">6,724	</td>
	<td align="right">13.60	</td>
	<td align="right">1,072,083,351	</td>
	<td align="right">13.03	</td>
	<td align="right">363	</td>
	<td align="right">15.16	</td>
	<td align="right">99,266.98	</td>
	<td align="right">12,408.37	</td>
</tr>
<tr class="udda">
	<th align="right">2019-8-2</th>
	<td align="right">6,267	</td>
	<td align="right">12.68	</td>
	<td align="right">1,107,221,815	</td>
	<td align="right">13.46	</td>
	<td align="right">314	</td>
	<td align="right">13.11	</td>
	<td align="right">102,520.54	</td>
	<td align="right">12,815.07	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-8-3</th>
	<td align="right">5,744	</td>
	<td align="right">11.62	</td>
	<td align="right">1,009,671,414	</td>
	<td align="right">12.27	</td>
	<td align="right">271	</td>
	<td align="right">11.32	</td>
	<td align="right">93,488.09	</td>
	<td align="right">11,686.01	</td>
</tr>
<tr class="udda">
	<th align="right">2019-8-4</th>
	<td align="right">5,602	</td>
	<td align="right">11.33	</td>
	<td align="right">832,451,958	</td>
	<td align="right">10.12	</td>
	<td align="right">278	</td>
	<td align="right">11.61	</td>
	<td align="right">77,078.88	</td>
	<td align="right">9,634.86	</td>
</tr>

</tbody>
</table>

