


<h2>Statistics for http://www.u5645470.fsdata.se, Week of 2019-7-29 to 2019-8-4</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Traffic by Hour</h3>
<p>Figures are averages for that hour
	of each day in the time period analyzed.</p>
<img src="h.png" width="468" height="351">
<br /><br />
<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Hour</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	<th scope='col'>Bits per Sec</th>
	<th scope='col'>Bytes per Sec</th>
	
	</tr>
</thead>
<tbody>
<tr class="jamnudda">
	<th align="right">00:00</th>
	<td align="right">196.71</td>
	<td align="right">2.79</td>
	<td align="right">38,279,800.00</td>
	<td align="right">3.26</td>
	<td align="right">7.20</td>
	<td align="right">2.10</td>
	<td align="right">85,066.22</td>
	<td align="right">10,633.28</td>
	
</tr>
<tr class="jamn">
	<th align="right">01:00</th>
	<td align="right">124.57</td>
	<td align="right">1.76</td>
	<td align="right">14,885,173.00</td>
	<td align="right">1.27</td>
	<td align="right">10.23</td>
	<td align="right">2.99</td>
	<td align="right">33,078.16</td>
	<td align="right">4,134.77</td>
	
</tr>
<tr class="udda">
	<th align="right">02:00</th>
	<td align="right">158.29</td>
	<td align="right">2.24</td>
	<td align="right">20,244,890.43</td>
	<td align="right">1.72</td>
	<td align="right">4.78</td>
	<td align="right">1.40</td>
	<td align="right">44,988.65</td>
	<td align="right">5,623.58</td>
	
</tr>
<tr class="jamn">
	<th align="right">03:00</th>
	<td align="right">110.43</td>
	<td align="right">1.56</td>
	<td align="right">24,471,043.71</td>
	<td align="right">2.08</td>
	<td align="right">4.96</td>
	<td align="right">1.45</td>
	<td align="right">54,380.10</td>
	<td align="right">6,797.51</td>
	
</tr>
<tr class="udda">
	<th align="right">04:00</th>
	<td align="right">94.00</td>
	<td align="right">1.33</td>
	<td align="right">9,461,331.86</td>
	<td align="right">0.81</td>
	<td align="right">7.33</td>
	<td align="right">2.14</td>
	<td align="right">21,025.18</td>
	<td align="right">2,628.15</td>
	
</tr>
<tr class="jamn">
	<th align="right">05:00</th>
	<td align="right">90.57</td>
	<td align="right">1.28</td>
	<td align="right">7,265,828.14</td>
	<td align="right">0.62</td>
	<td align="right">9.39</td>
	<td align="right">2.75</td>
	<td align="right">16,146.28</td>
	<td align="right">2,018.29</td>
	
</tr>
<tr class="udda">
	<th align="right">06:00</th>
	<td align="right">91.29</td>
	<td align="right">1.29</td>
	<td align="right">8,502,326.29</td>
	<td align="right">0.72</td>
	<td align="right">5.90</td>
	<td align="right">1.73</td>
	<td align="right">18,894.06</td>
	<td align="right">2,361.76</td>
	
</tr>
<tr class="jamn">
	<th align="right">07:00</th>
	<td align="right">187.14</td>
	<td align="right">2.65</td>
	<td align="right">31,762,401.86</td>
	<td align="right">2.70</td>
	<td align="right">8.60</td>
	<td align="right">2.51</td>
	<td align="right">70,583.12</td>
	<td align="right">8,822.89</td>
	
</tr>
<tr class="udda">
	<th align="right">08:00</th>
	<td align="right">363.57</td>
	<td align="right">5.15</td>
	<td align="right">60,885,721.14</td>
	<td align="right">5.18</td>
	<td align="right">10.04</td>
	<td align="right">2.93</td>
	<td align="right">135,301.60</td>
	<td align="right">16,912.70</td>
	
</tr>
<tr class="jamn">
	<th align="right">09:00</th>
	<td align="right">393.29</td>
	<td align="right">5.57</td>
	<td align="right">83,103,445.71</td>
	<td align="right">7.07</td>
	<td align="right">15.94</td>
	<td align="right">4.66</td>
	<td align="right">184,674.32</td>
	<td align="right">23,084.29</td>
	
</tr>
<tr class="udda">
	<th align="right">10:00</th>
	<td align="right">513.57</td>
	<td align="right">7.27</td>
	<td align="right">87,134,202.43</td>
	<td align="right">7.41</td>
	<td align="right">20.73</td>
	<td align="right">6.06</td>
	<td align="right">193,631.56</td>
	<td align="right">24,203.95</td>
	
</tr>
<tr class="jamn">
	<th align="right">11:00</th>
	<td align="right">499.57</td>
	<td align="right">7.07</td>
	<td align="right">77,796,963.14</td>
	<td align="right">6.62</td>
	<td align="right">22.78</td>
	<td align="right">6.66</td>
	<td align="right">172,882.14</td>
	<td align="right">21,610.27</td>
	
</tr>
<tr class="udda">
	<th align="right">12:00</th>
	<td align="right">440.29</td>
	<td align="right">6.23</td>
	<td align="right">77,075,621.14</td>
	<td align="right">6.56</td>
	<td align="right">22.47</td>
	<td align="right">6.57</td>
	<td align="right">171,279.16</td>
	<td align="right">21,409.89</td>
	
</tr>
<tr class="jamn">
	<th align="right">13:00</th>
	<td align="right">416.86</td>
	<td align="right">5.90</td>
	<td align="right">78,996,319.00</td>
	<td align="right">6.72</td>
	<td align="right">20.23</td>
	<td align="right">5.91</td>
	<td align="right">175,547.38</td>
	<td align="right">21,943.42</td>
	
</tr>
<tr class="udda">
	<th align="right">14:00</th>
	<td align="right">322.71</td>
	<td align="right">4.57</td>
	<td align="right">54,104,011.86</td>
	<td align="right">4.60</td>
	<td align="right">15.61</td>
	<td align="right">4.56</td>
	<td align="right">120,231.14</td>
	<td align="right">15,028.89</td>
	
</tr>
<tr class="jamn">
	<th align="right">15:00</th>
	<td align="right">456.14</td>
	<td align="right">6.46</td>
	<td align="right">83,708,157.43</td>
	<td align="right">7.12</td>
	<td align="right">19.96</td>
	<td align="right">5.83</td>
	<td align="right">186,018.13</td>
	<td align="right">23,252.27</td>
	
</tr>
<tr class="udda">
	<th align="right">16:00</th>
	<td align="right">339.00</td>
	<td align="right">4.80</td>
	<td align="right">53,612,870.29</td>
	<td align="right">4.56</td>
	<td align="right">19.14</td>
	<td align="right">5.59</td>
	<td align="right">119,139.71</td>
	<td align="right">14,892.46</td>
	
</tr>
<tr class="jamn">
	<th align="right">17:00</th>
	<td align="right">300.57</td>
	<td align="right">4.26</td>
	<td align="right">42,215,053.43</td>
	<td align="right">3.59</td>
	<td align="right">16.73</td>
	<td align="right">4.89</td>
	<td align="right">93,811.23</td>
	<td align="right">11,726.40</td>
	
</tr>
<tr class="udda">
	<th align="right">18:00</th>
	<td align="right">357.86</td>
	<td align="right">5.07</td>
	<td align="right">63,790,475.00</td>
	<td align="right">5.43</td>
	<td align="right">17.72</td>
	<td align="right">5.18</td>
	<td align="right">141,756.61</td>
	<td align="right">17,719.58</td>
	
</tr>
<tr class="jamn">
	<th align="right">19:00</th>
	<td align="right">346.29</td>
	<td align="right">4.90</td>
	<td align="right">53,463,382.71</td>
	<td align="right">4.55</td>
	<td align="right">16.41</td>
	<td align="right">4.80</td>
	<td align="right">118,807.52</td>
	<td align="right">14,850.94</td>
	
</tr>
<tr class="udda">
	<th align="right">20:00</th>
	<td align="right">279.29</td>
	<td align="right">3.95</td>
	<td align="right">56,012,858.86</td>
	<td align="right">4.77</td>
	<td align="right">13.86</td>
	<td align="right">4.05</td>
	<td align="right">124,473.02</td>
	<td align="right">15,559.13</td>
	
</tr>
<tr class="jamn">
	<th align="right">21:00</th>
	<td align="right">376.00</td>
	<td align="right">5.32</td>
	<td align="right">53,508,305.29</td>
	<td align="right">4.55</td>
	<td align="right">17.05</td>
	<td align="right">4.98</td>
	<td align="right">118,907.35</td>
	<td align="right">14,863.42</td>
	
</tr>
<tr class="udda">
	<th align="right">22:00</th>
	<td align="right">316.00</td>
	<td align="right">4.47</td>
	<td align="right">59,592,562.29</td>
	<td align="right">5.07</td>
	<td align="right">11.79</td>
	<td align="right">3.44</td>
	<td align="right">132,427.92</td>
	<td align="right">16,553.49</td>
	
</tr>
<tr class="jamn">
	<th align="right">23:00</th>
	<td align="right">289.14</td>
	<td align="right">4.09</td>
	<td align="right">35,311,347.57</td>
	<td align="right">3.00</td>
	<td align="right">23.29</td>
	<td align="right">6.81</td>
	<td align="right">78,469.66</td>
	<td align="right">9,808.71</td>
	
</tr>

</tbody>
</table>

