


<h2>Statistics for http://www.u5645470.fsdata.se, Week of 2019-7-29 to 2019-8-4</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 20 of 2,506 Visitor Sites,
Sorted by Access Count

</h3>
<p></p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Site</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="jamnudda">
	<th class="hogerstalld">1</th>
	<td>
	63.143.42.243		
	</td>
	<td align="right">1,290</td>
	<td align="right">2.61</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	46.59.25.88		
	</td>
	<td align="right">580</td>
	<td align="right">1.17</td>
	<td align="right">176,401,368</td>
	<td align="right">2.14</td>
	<td align="right">24</td>
	<td align="right">1.03</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	89.221.250.27		
	</td>
	<td align="right">560</td>
	<td align="right">1.13</td>
	<td align="right">11,018</td>
	<td align="right">0.00</td>
	<td align="right">7</td>
	<td align="right">0.33</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	95.195.145.67		
	</td>
	<td align="right">528</td>
	<td align="right">1.07</td>
	<td align="right">6,204,307</td>
	<td align="right">0.08</td>
	<td align="right">1</td>
	<td align="right">0.05</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	52.5.190.19		
	</td>
	<td align="right">506</td>
	<td align="right">1.02</td>
	<td align="right">75,895,337</td>
	<td align="right">0.92</td>
	<td align="right">1</td>
	<td align="right">0.05</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	66.249.64.18		
	</td>
	<td align="right">335</td>
	<td align="right">0.68</td>
	<td align="right">16,310,896</td>
	<td align="right">0.20</td>
	<td align="right">58</td>
	<td align="right">2.43</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	66.249.64.19		
	</td>
	<td align="right">293</td>
	<td align="right">0.59</td>
	<td align="right">9,627,231</td>
	<td align="right">0.12</td>
	<td align="right">62</td>
	<td align="right">2.61</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	66.249.64.20		
	</td>
	<td align="right">214</td>
	<td align="right">0.43</td>
	<td align="right">5,504,091</td>
	<td align="right">0.07</td>
	<td align="right">38</td>
	<td align="right">1.59</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	5.150.234.24		
	</td>
	<td align="right">214</td>
	<td align="right">0.43</td>
	<td align="right">32,304,903</td>
	<td align="right">0.39</td>
	<td align="right">7</td>
	<td align="right">0.33</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">10</th>
	<td>
	213.163.137.68		
	</td>
	<td align="right">175</td>
	<td align="right">0.35</td>
	<td align="right">33,066,054</td>
	<td align="right">0.40</td>
	<td align="right">4</td>
	<td align="right">0.19</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">11</th>
	<td>
	81.224.141.224		
	</td>
	<td align="right">175</td>
	<td align="right">0.35</td>
	<td align="right">28,693,082</td>
	<td align="right">0.35</td>
	<td align="right">2</td>
	<td align="right">0.09</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">12</th>
	<td>
	89.233.194.11		
	</td>
	<td align="right">154</td>
	<td align="right">0.31</td>
	<td align="right">75,935,058</td>
	<td align="right">0.92</td>
	<td align="right">1</td>
	<td align="right">0.05</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">13</th>
	<td>
	79.136.91.86		
	</td>
	<td align="right">153</td>
	<td align="right">0.31</td>
	<td align="right">23,348,696</td>
	<td align="right">0.28</td>
	<td align="right">5</td>
	<td align="right">0.23</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">14</th>
	<td>
	90.224.120.102		
	</td>
	<td align="right">152</td>
	<td align="right">0.31</td>
	<td align="right">32,639,526</td>
	<td align="right">0.40</td>
	<td align="right">2</td>
	<td align="right">0.09</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">15</th>
	<td>
	90.227.61.239		
	</td>
	<td align="right">145</td>
	<td align="right">0.29</td>
	<td align="right">10,733,109</td>
	<td align="right">0.13</td>
	<td align="right">1</td>
	<td align="right">0.05</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">16</th>
	<td>
	185.152.65.167		
	</td>
	<td align="right">131</td>
	<td align="right">0.26</td>
	<td align="right">990,373</td>
	<td align="right">0.01</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">17</th>
	<td>
	62.101.34.90		
	</td>
	<td align="right">131</td>
	<td align="right">0.26</td>
	<td align="right">46,439,863</td>
	<td align="right">0.56</td>
	<td align="right">3</td>
	<td align="right">0.14</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">18</th>
	<td>
	151.106.52.134		
	</td>
	<td align="right">130</td>
	<td align="right">0.26</td>
	<td align="right">975,660</td>
	<td align="right">0.01</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">19</th>
	<td>
	185.39.146.215		
	</td>
	<td align="right">129</td>
	<td align="right">0.26</td>
	<td align="right">958,838</td>
	<td align="right">0.01</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">20</th>
	<td>
	66.249.64.29		
	</td>
	<td align="right">129</td>
	<td align="right">0.26</td>
	<td align="right">1,018,023</td>
	<td align="right">0.01</td>
	<td align="right">20</td>
	<td align="right">0.84</td>
</tr>

</tbody>
</table>

