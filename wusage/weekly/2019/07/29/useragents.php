


<h2>Statistics for http://www.u5645470.fsdata.se, Week of 2019-7-29 to 2019-8-4</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 10 of 163 Browsers (User Agents),
Sorted by Access Count

</h3>
<p></p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Product</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th class="hogerstalld">1</th>
	<td>
	Safari/604.1		
	</td>
	<td align="right">14,852</td>
	<td align="right">30.17</td>
	<td align="right">2,909,442,660</td>
	<td align="right">35.41</td>
	<td align="right">527</td>
	<td align="right">22.15</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	Safari/537.36		
	</td>
	<td align="right">13,121</td>
	<td align="right">26.65</td>
	<td align="right">2,502,987,800</td>
	<td align="right">30.46</td>
	<td align="right">513</td>
	<td align="right">21.57</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	Mozilla/5.0 		
	</td>
	<td align="right">5,082</td>
	<td align="right">10.32</td>
	<td align="right">844,417,857</td>
	<td align="right">10.28</td>
	<td align="right">307</td>
	<td align="right">12.91</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	Safari/605.1.15		
	</td>
	<td align="right">2,243</td>
	<td align="right">4.56</td>
	<td align="right">346,370,565</td>
	<td align="right">4.22</td>
	<td align="right">78</td>
	<td align="right">3.28</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	bingbot/2.0		
	</td>
	<td align="right">1,650</td>
	<td align="right">3.35</td>
	<td align="right">167,054,362</td>
	<td align="right">2.03</td>
	<td align="right">343</td>
	<td align="right">14.45</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	UptimeRobot/2.0		
	</td>
	<td align="right">1,290</td>
	<td align="right">2.62</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	Pingdom.com_bot_version_1.4_		
	</td>
	<td align="right">1,268</td>
	<td align="right">2.58</td>
	<td align="right">9,598,898</td>
	<td align="right">0.12</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	Safari/605.1		
	</td>
	<td align="right">1,116</td>
	<td align="right">2.27</td>
	<td align="right">199,744,688</td>
	<td align="right">2.43</td>
	<td align="right">41</td>
	<td align="right">1.74</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	Safari/537.36 Edge/17.17134		
	</td>
	<td align="right">1,048</td>
	<td align="right">2.13</td>
	<td align="right">226,863,541</td>
	<td align="right">2.76</td>
	<td align="right">37</td>
	<td align="right">1.59</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">10</th>
	<td>
	Safari/602.1		
	</td>
	<td align="right">983</td>
	<td align="right">2.00</td>
	<td align="right">141,496,317</td>
	<td align="right">1.72</td>
	<td align="right">37</td>
	<td align="right">1.59</td>
</tr>

</tbody>
</table>

