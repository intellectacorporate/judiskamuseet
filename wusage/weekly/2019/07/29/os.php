


<h2>Statistics for http://www.u5645470.fsdata.se, Week of 2019-7-29 to 2019-8-4</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 9 of 9 Operating Systems,
Sorted by Access Count

</h3>
<p></p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Product</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th class="hogerstalld">1</th>
	<td>
	Mac OS X		
	</td>
	<td align="right">37,808</td>
	<td align="right">76.80</td>
	<td align="right">6,895,072,646</td>
	<td align="right">83.92</td>
	<td align="right">1,547</td>
	<td align="right">64.60</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	Other		
	</td>
	<td align="right">7,860</td>
	<td align="right">15.97</td>
	<td align="right">789,666,416</td>
	<td align="right">9.61</td>
	<td align="right">596</td>
	<td align="right">24.89</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	Windows NT		
	</td>
	<td align="right">2,458</td>
	<td align="right">4.99</td>
	<td align="right">489,825,545</td>
	<td align="right">5.96</td>
	<td align="right">105</td>
	<td align="right">4.42</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	Linux		
	</td>
	<td align="right">961</td>
	<td align="right">1.95</td>
	<td align="right">17,061,607</td>
	<td align="right">0.21</td>
	<td align="right">140</td>
	<td align="right">5.86</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	Windows Codename Longhorn		
	</td>
	<td align="right">73</td>
	<td align="right">0.15</td>
	<td align="right">23,618,619</td>
	<td align="right">0.29</td>
	<td align="right">2</td>
	<td align="right">0.09</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	Windows XP		
	</td>
	<td align="right">38</td>
	<td align="right">0.08</td>
	<td align="right">699,854</td>
	<td align="right">0.01</td>
	<td align="right">3</td>
	<td align="right">0.14</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	Mac OS		
	</td>
	<td align="right">18</td>
	<td align="right">0.04</td>
	<td align="right">340</td>
	<td align="right">0.00</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	Windows 2003		
	</td>
	<td align="right">11</td>
	<td align="right">0.02</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	Windows 2000		
	</td>
	<td align="right">2</td>
	<td align="right">0.00</td>
	<td align="right">63,881</td>
	<td align="right">0.00</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>

</tbody>
</table>

