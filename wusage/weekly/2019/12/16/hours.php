


<h2>Statistics for http://www.u5645470.fsdata.se, Week of 2019-12-16 to 2019-12-22</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Traffic by Hour</h3>
<p>Figures are averages for that hour
	of each day in the time period analyzed.</p>
<img src="h.png" width="468" height="351">
<br /><br />
<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Hour</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	<th scope='col'>Bits per Sec</th>
	<th scope='col'>Bytes per Sec</th>
	
	</tr>
</thead>
<tbody>
<tr class="jamnudda">
	<th align="right">00:00</th>
	<td align="right">314.14</td>
	<td align="right">3.09</td>
	<td align="right">9,031,518.86</td>
	<td align="right">1.97</td>
	<td align="right">7.95</td>
	<td align="right">1.79</td>
	<td align="right">20,070.04</td>
	<td align="right">2,508.76</td>
	
</tr>
<tr class="jamn">
	<th align="right">01:00</th>
	<td align="right">300.14</td>
	<td align="right">2.95</td>
	<td align="right">10,609,101.29</td>
	<td align="right">2.31</td>
	<td align="right">11.74</td>
	<td align="right">2.64</td>
	<td align="right">23,575.78</td>
	<td align="right">2,946.97</td>
	
</tr>
<tr class="udda">
	<th align="right">02:00</th>
	<td align="right">263.71</td>
	<td align="right">2.60</td>
	<td align="right">4,084,934.29</td>
	<td align="right">0.89</td>
	<td align="right">10.71</td>
	<td align="right">2.41</td>
	<td align="right">9,077.63</td>
	<td align="right">1,134.70</td>
	
</tr>
<tr class="jamn">
	<th align="right">03:00</th>
	<td align="right">268.29</td>
	<td align="right">2.64</td>
	<td align="right">4,772,626.14</td>
	<td align="right">1.04</td>
	<td align="right">11.39</td>
	<td align="right">2.56</td>
	<td align="right">10,605.84</td>
	<td align="right">1,325.73</td>
	
</tr>
<tr class="udda">
	<th align="right">04:00</th>
	<td align="right">252.00</td>
	<td align="right">2.48</td>
	<td align="right">5,621,398.86</td>
	<td align="right">1.22</td>
	<td align="right">9.90</td>
	<td align="right">2.22</td>
	<td align="right">12,492.00</td>
	<td align="right">1,561.50</td>
	
</tr>
<tr class="jamn">
	<th align="right">05:00</th>
	<td align="right">270.57</td>
	<td align="right">2.66</td>
	<td align="right">5,217,456.29</td>
	<td align="right">1.14</td>
	<td align="right">11.72</td>
	<td align="right">2.63</td>
	<td align="right">11,594.35</td>
	<td align="right">1,449.29</td>
	
</tr>
<tr class="udda">
	<th align="right">06:00</th>
	<td align="right">298.29</td>
	<td align="right">2.94</td>
	<td align="right">6,034,382.43</td>
	<td align="right">1.31</td>
	<td align="right">8.37</td>
	<td align="right">1.88</td>
	<td align="right">13,409.74</td>
	<td align="right">1,676.22</td>
	
</tr>
<tr class="jamn">
	<th align="right">07:00</th>
	<td align="right">320.86</td>
	<td align="right">3.16</td>
	<td align="right">9,312,823.00</td>
	<td align="right">2.03</td>
	<td align="right">12.32</td>
	<td align="right">2.77</td>
	<td align="right">20,695.16</td>
	<td align="right">2,586.90</td>
	
</tr>
<tr class="udda">
	<th align="right">08:00</th>
	<td align="right">366.71</td>
	<td align="right">3.61</td>
	<td align="right">14,594,683.43</td>
	<td align="right">3.18</td>
	<td align="right">18.63</td>
	<td align="right">4.18</td>
	<td align="right">32,432.63</td>
	<td align="right">4,054.08</td>
	
</tr>
<tr class="jamn">
	<th align="right">09:00</th>
	<td align="right">533.43</td>
	<td align="right">5.25</td>
	<td align="right">30,248,397.00</td>
	<td align="right">6.59</td>
	<td align="right">25.49</td>
	<td align="right">5.73</td>
	<td align="right">67,218.66</td>
	<td align="right">8,402.33</td>
	
</tr>
<tr class="udda">
	<th align="right">10:00</th>
	<td align="right">503.57</td>
	<td align="right">4.96</td>
	<td align="right">19,481,071.86</td>
	<td align="right">4.24</td>
	<td align="right">27.25</td>
	<td align="right">6.12</td>
	<td align="right">43,291.27</td>
	<td align="right">5,411.41</td>
	
</tr>
<tr class="jamn">
	<th align="right">11:00</th>
	<td align="right">595.43</td>
	<td align="right">5.86</td>
	<td align="right">36,341,981.29</td>
	<td align="right">7.92</td>
	<td align="right">25.91</td>
	<td align="right">5.82</td>
	<td align="right">80,759.96</td>
	<td align="right">10,094.99</td>
	
</tr>
<tr class="udda">
	<th align="right">12:00</th>
	<td align="right">529.57</td>
	<td align="right">5.21</td>
	<td align="right">29,305,479.86</td>
	<td align="right">6.39</td>
	<td align="right">21.88</td>
	<td align="right">4.91</td>
	<td align="right">65,123.29</td>
	<td align="right">8,140.41</td>
	
</tr>
<tr class="jamn">
	<th align="right">13:00</th>
	<td align="right">521.14</td>
	<td align="right">5.13</td>
	<td align="right">26,608,751.43</td>
	<td align="right">5.80</td>
	<td align="right">24.77</td>
	<td align="right">5.56</td>
	<td align="right">59,130.56</td>
	<td align="right">7,391.32</td>
	
</tr>
<tr class="udda">
	<th align="right">14:00</th>
	<td align="right">471.00</td>
	<td align="right">4.63</td>
	<td align="right">21,273,439.86</td>
	<td align="right">4.64</td>
	<td align="right">17.41</td>
	<td align="right">3.91</td>
	<td align="right">47,274.31</td>
	<td align="right">5,909.29</td>
	
</tr>
<tr class="jamn">
	<th align="right">15:00</th>
	<td align="right">690.86</td>
	<td align="right">6.80</td>
	<td align="right">50,496,172.14</td>
	<td align="right">11.00</td>
	<td align="right">23.16</td>
	<td align="right">5.20</td>
	<td align="right">112,213.72</td>
	<td align="right">14,026.71</td>
	
</tr>
<tr class="udda">
	<th align="right">16:00</th>
	<td align="right">523.57</td>
	<td align="right">5.15</td>
	<td align="right">19,910,682.71</td>
	<td align="right">4.34</td>
	<td align="right">22.43</td>
	<td align="right">5.04</td>
	<td align="right">44,245.96</td>
	<td align="right">5,530.75</td>
	
</tr>
<tr class="jamn">
	<th align="right">17:00</th>
	<td align="right">431.86</td>
	<td align="right">4.25</td>
	<td align="right">23,567,679.00</td>
	<td align="right">5.14</td>
	<td align="right">17.68</td>
	<td align="right">3.97</td>
	<td align="right">52,372.62</td>
	<td align="right">6,546.58</td>
	
</tr>
<tr class="udda">
	<th align="right">18:00</th>
	<td align="right">476.57</td>
	<td align="right">4.69</td>
	<td align="right">27,169,735.86</td>
	<td align="right">5.92</td>
	<td align="right">23.71</td>
	<td align="right">5.33</td>
	<td align="right">60,377.19</td>
	<td align="right">7,547.15</td>
	
</tr>
<tr class="jamn">
	<th align="right">19:00</th>
	<td align="right">414.00</td>
	<td align="right">4.07</td>
	<td align="right">19,746,780.86</td>
	<td align="right">4.30</td>
	<td align="right">21.58</td>
	<td align="right">4.85</td>
	<td align="right">43,881.74</td>
	<td align="right">5,485.22</td>
	
</tr>
<tr class="udda">
	<th align="right">20:00</th>
	<td align="right">469.86</td>
	<td align="right">4.62</td>
	<td align="right">22,650,504.14</td>
	<td align="right">4.94</td>
	<td align="right">24.01</td>
	<td align="right">5.39</td>
	<td align="right">50,334.45</td>
	<td align="right">6,291.81</td>
	
</tr>
<tr class="jamn">
	<th align="right">21:00</th>
	<td align="right">479.00</td>
	<td align="right">4.71</td>
	<td align="right">28,857,058.14</td>
	<td align="right">6.29</td>
	<td align="right">22.10</td>
	<td align="right">4.97</td>
	<td align="right">64,126.80</td>
	<td align="right">8,015.85</td>
	
</tr>
<tr class="udda">
	<th align="right">22:00</th>
	<td align="right">415.86</td>
	<td align="right">4.09</td>
	<td align="right">16,141,631.14</td>
	<td align="right">3.52</td>
	<td align="right">20.15</td>
	<td align="right">4.53</td>
	<td align="right">35,870.29</td>
	<td align="right">4,483.79</td>
	
</tr>
<tr class="jamn">
	<th align="right">23:00</th>
	<td align="right">451.43</td>
	<td align="right">4.44</td>
	<td align="right">17,876,111.14</td>
	<td align="right">3.89</td>
	<td align="right">24.88</td>
	<td align="right">5.59</td>
	<td align="right">39,724.69</td>
	<td align="right">4,965.59</td>
	
</tr>

</tbody>
</table>

