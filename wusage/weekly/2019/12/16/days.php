


<h2>Statistics for http://www.u5645470.fsdata.se, Week of 2019-12-16 to 2019-12-22</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Traffic by Day</h3>
<img src="d.png" width="468" height="351">
<br /><br />


<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Date</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	<th scope='col'>Bits per Sec</th>
	<th scope='col'>Bytes per Sec</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th align="right">2019-12-16</th>
	<td align="right">10,996	</td>
	<td align="right">15.46	</td>
	<td align="right">591,840,214	</td>
	<td align="right">18.42	</td>
	<td align="right">487	</td>
	<td align="right">15.63	</td>
	<td align="right">54,800.02	</td>
	<td align="right">6,850.00	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-12-17</th>
	<td align="right">11,588	</td>
	<td align="right">16.29	</td>
	<td align="right">501,354,466	</td>
	<td align="right">15.61	</td>
	<td align="right">508	</td>
	<td align="right">16.30	</td>
	<td align="right">46,421.71	</td>
	<td align="right">5,802.71	</td>
</tr>
<tr class="udda">
	<th align="right">2019-12-18</th>
	<td align="right">10,692	</td>
	<td align="right">15.03	</td>
	<td align="right">503,063,275	</td>
	<td align="right">15.66	</td>
	<td align="right">501	</td>
	<td align="right">16.08	</td>
	<td align="right">46,579.93	</td>
	<td align="right">5,822.49	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-12-19</th>
	<td align="right">10,671	</td>
	<td align="right">15.00	</td>
	<td align="right">489,522,291	</td>
	<td align="right">15.24	</td>
	<td align="right">466	</td>
	<td align="right">14.96	</td>
	<td align="right">45,326.14	</td>
	<td align="right">5,665.77	</td>
</tr>
<tr class="udda">
	<th align="right">2019-12-20</th>
	<td align="right">10,330	</td>
	<td align="right">14.52	</td>
	<td align="right">424,405,766	</td>
	<td align="right">13.21	</td>
	<td align="right">426	</td>
	<td align="right">13.67	</td>
	<td align="right">39,296.83	</td>
	<td align="right">4,912.10	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-12-21</th>
	<td align="right">8,212	</td>
	<td align="right">11.54	</td>
	<td align="right">330,474,668	</td>
	<td align="right">10.29	</td>
	<td align="right">369	</td>
	<td align="right">11.84	</td>
	<td align="right">30,599.51	</td>
	<td align="right">3,824.94	</td>
</tr>
<tr class="udda">
	<th align="right">2019-12-22</th>
	<td align="right">8,644	</td>
	<td align="right">12.15	</td>
	<td align="right">372,020,127	</td>
	<td align="right">11.58	</td>
	<td align="right">359	</td>
	<td align="right">11.52	</td>
	<td align="right">34,446.31	</td>
	<td align="right">4,305.79	</td>
</tr>

</tbody>
</table>

