


<h2>Statistics for http://www.u5645470.fsdata.se, Week of 2019-12-16 to 2019-12-22</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 10 of 182 Browsers (User Agents),
Sorted by Access Count

</h3>
<p></p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Product</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th class="hogerstalld">1</th>
	<td>
	Pingdom.com_bot_version_1.4_		
	</td>
	<td align="right">29,544</td>
	<td align="right">41.57</td>
	<td align="right">238,919,487</td>
	<td align="right">7.45</td>
	<td align="right">28</td>
	<td align="right">0.92</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	Safari/537.36		
	</td>
	<td align="right">14,926</td>
	<td align="right">21.00</td>
	<td align="right">1,258,739,858</td>
	<td align="right">39.26</td>
	<td align="right">671</td>
	<td align="right">21.62</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	Safari/604.1		
	</td>
	<td align="right">7,799</td>
	<td align="right">10.97</td>
	<td align="right">720,119,433</td>
	<td align="right">22.46</td>
	<td align="right">488</td>
	<td align="right">15.71</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	Mozilla/5.0 		
	</td>
	<td align="right">4,883</td>
	<td align="right">6.87</td>
	<td align="right">230,215,544</td>
	<td align="right">7.18</td>
	<td align="right">937</td>
	<td align="right">30.17</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	Safari/605.1.15		
	</td>
	<td align="right">2,586</td>
	<td align="right">3.64</td>
	<td align="right">181,724,296</td>
	<td align="right">5.67</td>
	<td align="right">130</td>
	<td align="right">4.21</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	UptimeRobot/2.0		
	</td>
	<td align="right">1,404</td>
	<td align="right">1.98</td>
	<td align="right">142,942</td>
	<td align="right">0.00</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	bingbot/2.0		
	</td>
	<td align="right">1,099</td>
	<td align="right">1.55</td>
	<td align="right">19,213,279</td>
	<td align="right">0.60</td>
	<td align="right">255</td>
	<td align="right">8.22</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	MJ12bot/v1.4.8		
	</td>
	<td align="right">964</td>
	<td align="right">1.36</td>
	<td align="right">32,192,357</td>
	<td align="right">1.00</td>
	<td align="right">62</td>
	<td align="right">2.02</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	Safari/537.36 Edge/18.18362		
	</td>
	<td align="right">826</td>
	<td align="right">1.16</td>
	<td align="right">101,732,134</td>
	<td align="right">3.17</td>
	<td align="right">29</td>
	<td align="right">0.95</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">10</th>
	<td>
	curl/7.54.0		
	</td>
	<td align="right">539</td>
	<td align="right">0.76</td>
	<td align="right">5,141,321</td>
	<td align="right">0.16</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>

</tbody>
</table>

