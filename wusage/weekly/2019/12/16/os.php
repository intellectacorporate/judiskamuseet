


<h2>Statistics for http://www.u5645470.fsdata.se, Week of 2019-12-16 to 2019-12-22</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 10 of 14 Operating Systems,
Sorted by Access Count

</h3>
<p></p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Product</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th class="hogerstalld">1</th>
	<td>
	Other		
	</td>
	<td align="right">36,994</td>
	<td align="right">52.06</td>
	<td align="right">458,071,994</td>
	<td align="right">14.29</td>
	<td align="right">684</td>
	<td align="right">21.96</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	Mac OS X		
	</td>
	<td align="right">29,544</td>
	<td align="right">41.57</td>
	<td align="right">2,555,813,913</td>
	<td align="right">79.71</td>
	<td align="right">1,511</td>
	<td align="right">48.51</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	Linux		
	</td>
	<td align="right">2,472</td>
	<td align="right">3.48</td>
	<td align="right">8,828,483</td>
	<td align="right">0.28</td>
	<td align="right">820</td>
	<td align="right">26.33</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	Windows NT		
	</td>
	<td align="right">1,827</td>
	<td align="right">2.57</td>
	<td align="right">170,720,843</td>
	<td align="right">5.32</td>
	<td align="right">95</td>
	<td align="right">3.06</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	Windows XP		
	</td>
	<td align="right">117</td>
	<td align="right">0.16</td>
	<td align="right">5,580,089</td>
	<td align="right">0.17</td>
	<td align="right">2</td>
	<td align="right">0.07</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	Windows Codename Longhorn		
	</td>
	<td align="right">45</td>
	<td align="right">0.06</td>
	<td align="right">5,227,165</td>
	<td align="right">0.16</td>
	<td align="right">1</td>
	<td align="right">0.04</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	Windows 2003		
	</td>
	<td align="right">30</td>
	<td align="right">0.04</td>
	<td align="right">490,554</td>
	<td align="right">0.02</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	Windows 2000		
	</td>
	<td align="right">10</td>
	<td align="right">0.01</td>
	<td align="right">465,136</td>
	<td align="right">0.01</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	Windows 95		
	</td>
	<td align="right">9</td>
	<td align="right">0.01</td>
	<td align="right">490,554</td>
	<td align="right">0.02</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">10</th>
	<td>
	Windows 98		
	</td>
	<td align="right">6</td>
	<td align="right">0.01</td>
	<td align="right">327,036</td>
	<td align="right">0.01</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>

</tbody>
</table>

