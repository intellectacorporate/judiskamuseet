


<h2>Statistics for http://www.u5645470.fsdata.se, Week of 2019-12-16 to 2019-12-22</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 20 of 3,338 Visitor Sites,
Sorted by Access Count

</h3>
<p></p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Site</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="jamnudda">
	<th class="hogerstalld">1</th>
	<td>
	46.246.122.10		
	</td>
	<td align="right">2,991</td>
	<td align="right">4.20</td>
	<td align="right">24,056,123</td>
	<td align="right">0.75</td>
	<td align="right">7</td>
	<td align="right">0.26</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	185.93.3.92		
	</td>
	<td align="right">2,961</td>
	<td align="right">4.16</td>
	<td align="right">23,923,221</td>
	<td align="right">0.74</td>
	<td align="right">7</td>
	<td align="right">0.26</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	46.59.25.88		
	</td>
	<td align="right">2,784</td>
	<td align="right">3.91</td>
	<td align="right">150,218,676</td>
	<td align="right">4.68</td>
	<td align="right">34</td>
	<td align="right">1.12</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	85.195.116.134		
	</td>
	<td align="right">2,565</td>
	<td align="right">3.61</td>
	<td align="right">20,351,618</td>
	<td align="right">0.63</td>
	<td align="right">5</td>
	<td align="right">0.18</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	82.103.145.126		
	</td>
	<td align="right">2,285</td>
	<td align="right">3.21</td>
	<td align="right">17,923,120</td>
	<td align="right">0.56</td>
	<td align="right">3</td>
	<td align="right">0.11</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	46.20.45.18		
	</td>
	<td align="right">1,978</td>
	<td align="right">2.78</td>
	<td align="right">14,969,942</td>
	<td align="right">0.47</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	82.103.139.165		
	</td>
	<td align="right">1,970</td>
	<td align="right">2.77</td>
	<td align="right">14,939,438</td>
	<td align="right">0.47</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	185.136.156.82		
	</td>
	<td align="right">1,960</td>
	<td align="right">2.76</td>
	<td align="right">14,925,541</td>
	<td align="right">0.46</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	94.75.211.74		
	</td>
	<td align="right">1,932</td>
	<td align="right">2.72</td>
	<td align="right">14,479,814</td>
	<td align="right">0.45</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">10</th>
	<td>
	185.246.208.82		
	</td>
	<td align="right">1,853</td>
	<td align="right">2.60</td>
	<td align="right">14,096,802</td>
	<td align="right">0.44</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">11</th>
	<td>
	63.143.42.242		
	</td>
	<td align="right">1,401</td>
	<td align="right">1.97</td>
	<td align="right">59,292</td>
	<td align="right">0.00</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">12</th>
	<td>
	37.252.231.50		
	</td>
	<td align="right">1,206</td>
	<td align="right">1.70</td>
	<td align="right">9,806,896</td>
	<td align="right">0.31</td>
	<td align="right">3</td>
	<td align="right">0.11</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">13</th>
	<td>
	83.170.113.210		
	</td>
	<td align="right">1,165</td>
	<td align="right">1.64</td>
	<td align="right">8,850,305</td>
	<td align="right">0.28</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">14</th>
	<td>
	89.163.242.206		
	</td>
	<td align="right">1,008</td>
	<td align="right">1.42</td>
	<td align="right">9,142,560</td>
	<td align="right">0.28</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">15</th>
	<td>
	185.180.12.65		
	</td>
	<td align="right">1,008</td>
	<td align="right">1.42</td>
	<td align="right">9,142,560</td>
	<td align="right">0.28</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">16</th>
	<td>
	81.17.62.205		
	</td>
	<td align="right">1,008</td>
	<td align="right">1.42</td>
	<td align="right">9,142,560</td>
	<td align="right">0.28</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">17</th>
	<td>
	151.106.52.134		
	</td>
	<td align="right">988</td>
	<td align="right">1.39</td>
	<td align="right">8,961,160</td>
	<td align="right">0.28</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">18</th>
	<td>
	82.103.136.16		
	</td>
	<td align="right">984</td>
	<td align="right">1.38</td>
	<td align="right">8,924,880</td>
	<td align="right">0.28</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">19</th>
	<td>
	185.39.146.214		
	</td>
	<td align="right">697</td>
	<td align="right">0.98</td>
	<td align="right">6,321,790</td>
	<td align="right">0.20</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">20</th>
	<td>
	94.75.211.73		
	</td>
	<td align="right">680</td>
	<td align="right">0.96</td>
	<td align="right">6,149,462</td>
	<td align="right">0.19</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>

</tbody>
</table>

