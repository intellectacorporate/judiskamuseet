

<script>
var componentsData = new Array(
        
        "*end*"
);
var componentsByPage = new Array();
var epochRange = "";
var sortedByAccesses = 0;
var sortedByBytes = 0;
var sortedByVisits = 0;
var alphabetized = 0;
var withBytes = 0;
var withVisits = 0;
var withDownloads = 0;
var subreportKeyword = "";
var subreportTitle = "";
var subreportAll = 0;
var subreportTop = 0;
var subreportTotal = 0;
var subreportPie = "";
var subreportGreenbarFlag = 0;
var pc = 0, ac = 0;
var dataEnd = 0;
var data;
var dataArrays = new Array(
	new Array(
		"Week of 2019-12-16 to 2019-12-22",	
		"*sorted-by-accesses*",
		"*bytes*",
		"*visits*",
		"*subreport*",
	"pages",
	"Documents",
	"*top*",
		30,
	
	"*total*",
	"2,602",
	"*pie*",
	"",
	"*row*",
	"1",
	"/",
	"*local-link*",
		"http://www.u5645470.fsdata.se/",
	
	"23,727",
	"33.36",
	"175,311,168",
		"5.46",
	"867",
		"2.46",
	"*row*",
	"2",
	"/wp-content/uploads/2017/06/Judiska_centr_svart_web-1.png",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/uploads/2017/06/Judiska_centr_svart_web-1.png",
	
	"10,964",
	"15.41",
	"99,389,060",
		"3.09",
	"1,053",
		"2.99",
	"*row*",
	"3",
	"/wp-login.php",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-login.php",
	
	"1,692",
	"2.38",
	"2,684,271",
		"0.08",
	"832",
		"2.36",
	"*row*",
	"4",
	"/wp-content/cache/minify/fa7ff.css",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/cache/minify/fa7ff.css",
	
	"1,090",
	"1.53",
	"33,947,688",
		"1.06",
	"1,187",
		"3.37",
	"*row*",
	"5",
	"/wp-content/cache/minify/1abb3.js",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/cache/minify/1abb3.js",
	
	"1,055",
	"1.48",
	"46,634,500",
		"1.45",
	"1,153",
		"3.27",
	"*row*",
	"6",
	"/wp-content/cache/minify/768dd.css",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/cache/minify/768dd.css",
	
	"1,051",
	"1.48",
	"99,712,501",
		"3.10",
	"1,148",
		"3.26",
	"*row*",
	"7",
	"/wp-content/cache/minify/dfacc.js",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/cache/minify/dfacc.js",
	
	"1,036",
	"1.46",
	"3,827,494",
		"0.12",
	"1,135",
		"3.22",
	"*row*",
	"8",
	"/wp-content/cache/minify/29e8b.js",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/cache/minify/29e8b.js",
	
	"1,032",
	"1.45",
	"7,855,241",
		"0.24",
	"1,133",
		"3.22",
	"*row*",
	"9",
	"/wp-content/cache/minify/f6d37.js",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/cache/minify/f6d37.js",
	
	"1,031",
	"1.45",
	"367,750",
		"0.01",
	"1,136",
		"3.22",
	"*row*",
	"10",
	"/wp-content/cache/minify/17e60.js",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/cache/minify/17e60.js",
	
	"1,024",
	"1.44",
	"69,101,937",
		"2.15",
	"1,124",
		"3.19",
	"*row*",
	"11",
	"/wp-admin/admin-ajax.php",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-admin/admin-ajax.php",
	
	"953",
	"1.34",
	"64,710,084",
		"2.01",
	"23",
		"0.07",
	"*row*",
	"12",
	"/wp-includes/js/wp-emoji-release.min.js",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-includes/js/wp-emoji-release.min.js",
	
	"930",
	"1.31",
	"4,330,684",
		"0.13",
	"1,009",
		"2.86",
	"*row*",
	"13",
	"/wp-content/themes/Divi/core/admin/fonts/modules.ttf",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/themes/Divi/core/admin/fonts/modules.ttf",
	
	"913",
	"1.28",
	"34,204,795",
		"1.06",
	"1,012",
		"2.87",
	"*row*",
	"14",
	"/wp-content/themes/divi-child/img/location.png",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/themes/divi-child/img/location.png",
	
	"801",
	"1.13",
	"2,440,146",
		"0.08",
	"712",
		"2.02",
	"*row*",
	"15",
	"/xmlrpc.php",
	"*local-link*",
		"http://www.u5645470.fsdata.se/xmlrpc.php",
	
	"735",
	"1.03",
	"176,306",
		"0.01",
	"822",
		"2.33",
	"*row*",
	"16",
	"/wp-content/themes/divi-child/img/Instagram.png",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/themes/divi-child/img/Instagram.png",
	
	"719",
	"1.01",
	"4,342,195",
		"0.14",
	"661",
		"1.88",
	"*row*",
	"17",
	"/wp-content/themes/divi-child/img/facebook.png",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/themes/divi-child/img/facebook.png",
	
	"711",
	"1.00",
	"3,437,434",
		"0.11",
	"657",
		"1.86",
	"*row*",
	"18",
	"/wp-content/uploads/2019/05/cropped-Judiska_svart-2-32x32.pn<br>\ng",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/uploads/2019/05/cropped-Judiska_svart-2-32x32.png",
	
	"700",
	"0.98",
	"527,688",
		"0.02",
	"723",
		"2.05",
	"*row*",
	"19",
	"/wp-content/themes/divi-child/img/clock.png",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/themes/divi-child/img/clock.png",
	
	"552",
	"0.78",
	"1,248,500",
		"0.04",
	"618",
		"1.76",
	"*row*",
	"20",
	"/feed",
	"*local-link*",
		"http://www.u5645470.fsdata.se/feed",
	
	"500",
	"0.70",
	"287,496",
		"0.01",
	"43",
		"0.12",
	"*row*",
	"21",
	"/wp-content/uploads/2019/05/cropped-Judiska_svart-2-192x192.<br>\npng",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/uploads/2019/05/cropped-Judiska_svart-2-192x192.png",
	
	"488",
	"0.69",
	"3,256,912",
		"0.10",
	"529",
		"1.50",
	"*row*",
	"22",
	"/utforska/om-judendomen",
	"*local-link*",
		"http://www.u5645470.fsdata.se/utforska/om-judendomen",
	
	"390",
	"0.55",
	"5,906,883",
		"0.18",
	"331",
		"0.94",
	"*row*",
	"23",
	"/robots.txt",
	"*local-link*",
		"http://www.u5645470.fsdata.se/robots.txt",
	
	"369",
	"0.52",
	"23,694",
		"0.00",
	"177",
		"0.50",
	"*row*",
	"24",
	"/wp-content/uploads/2019/07/annons.jpg",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/uploads/2019/07/annons.jpg",
	
	"360",
	"0.51",
	"16,622,352",
		"0.52",
	"375",
		"1.07",
	"*row*",
	"25",
	"/wp-content/plugins/instagram-slider-widget/assets/in.png",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/plugins/instagram-slider-widget/assets/in.png",
	
	"357",
	"0.50",
	"723,036",
		"0.02",
	"394",
		"1.12",
	"*row*",
	"26",
	"/wp-content/uploads/2019/06/till-webb_4-1.jpg",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/uploads/2019/06/till-webb_4-1.jpg",
	
	"356",
	"0.50",
	"35,533,812",
		"1.11",
	"395",
		"1.12",
	"*row*",
	"27",
	"/wp-content/uploads/2019/12/munkar.jpg",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/uploads/2019/12/munkar.jpg",
	
	"330",
	"0.46",
	"67,475,504",
		"2.10",
	"364",
		"1.03",
	"*row*",
	"28",
	"/wp-content/uploads/2019/07/mönster.jpg",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/uploads/2019/07/mönster.jpg",
	
	"327",
	"0.46",
	"52,190,569",
		"1.62",
	"365",
		"1.04",
	"*row*",
	"29",
	"/wp-content/uploads/2019/12/channukian-shopen.jpg",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/uploads/2019/12/channukian-shopen.jpg",
	
	"301",
	"0.42",
	"87,920,651",
		"2.74",
	"336",
		"0.95",
	"*row*",
	"30",
	"/wp-content/uploads/2019/12/Webb-front_0000s_0000_Lager-2.jp<br>\ng",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/uploads/2019/12/Webb-front_0000s_0000_Lager-2.jpg",
	
	"296",
	"0.42",
	"323,069,838",
		"10.06",
	"326",
		"0.93",
	
		"*end*"
		)
	);

function incPC()
{
	pc++;
	if (pc == data.length) {
		if ((ac == dataArrays.length) || (dataArrays[ac] == "*end*")) {
			dataEnd = 1;
		} else {
			ac++;
			data = dataArrays[ac];
			pc = 0;
		}
	}	
}

function resetPC()
{
	pc = 0;
	ac = 0;
	if (dataArrays.length) {
		data = dataArrays[0];
		dataEnd = 0;
	} else {	
		dataEnd = 1;
	}
}

function compileReports()
{
	resetPC();
	compileDocuments();
}

function compileDocuments()
{
	var s = data[pc];
	epochRange = data[pc];
	incPC();
	while (dataEnd == 0) {
		var s = data[pc];
		if (s == "*sorted-by-accesses*") {
			sortedByAccesses = 1;
			incPC();
		} else if (s == "*sorted-by-bytes*") {
			sortedByBytes = 1;
			incPC();
		} else if (s == "*sorted-by-visits*") {
			sortedByVisits = 1;
			incPC();
		} else if (s == "*alphabetized*") {
			alphabetized = 1;
			incPC();
		} else if (s == "*bytes*") {
			withBytes = 1;
			incPC();
		} else if (s == "*visits*") {
			withVisits = 1;
			incPC();
		} else if (s == "*downloads*") {
			withDownloads = 1;
			incPC();
		} else {
			break;
		}
	}
	documentsHead();
	while (dataEnd == 0) {
		if (compileSubreport() == 0) {
			// No more subreports. That's OK 
			break;
		}	
	}
	documentsTail();
	return 1;
}
function compileSubreport()
{
	if (data[pc] != "*subreport*") {
		return 0;
	}
	incPC();	
	subreportKeyword = "";
	subreportTitle = "";
	subreportAll = 0;
	subreportTop = 0;
	subreportTotal = 0;
	subreportPie = "";
	subreportKeyword = data[pc];
	incPC();
	subreportTitle = data[pc];
	incPC();
	if (data[pc] == "*all*") {
		incPC();
		subreportAll = 1;
	}
	if (data[pc] == "*top*") {
		incPC();
		subreportTop = data[pc];
		incPC();
	}
	if (data[pc] == "*total*") {
		incPC();
		subreportTotal = data[pc];
		incPC();
	}
	if (data[pc] == "*pie*") {
		incPC();
		subreportPie = data[pc];
		incPC();
	}
	subreportHead();
	while (dataEnd == 0) {
		if (compileSubreportRow() == 0) {
			// No more subreport rows. That's OK 
			break;
		}
	}
	subreportTail();
	return 1;
}
var rowRank = 0;
var rowItem = "";
var rowLocalLink = "";
var rowTitle = "";
var rowReferrers = "";
var rowAccesses = 0;
var rowAccessesPer = 0;
var rowBytes = 0;
var rowBytesPer = 0;
var rowVisits = 0;
var rowVisitsPer = 0;
var rowDownloadsPer = 0;
var rowComponents = 0;

function compileSubreportRow()
{
	if (data[pc] != "*row*") {
		return 0;
	}
	rowRank = 0;
	rowItem = "";
	rowLocalLink = "";
	rowTitle = "";
	rowReferrers = "";
	rowComponents = 0;
	incPC();
	rowRank = data[pc];
	incPC();
	rowItem = data[pc];
	incPC();
	if (data[pc] == "*local-link*") {
		incPC();
		rowLocalLink = data[pc];
		incPC();
	}
	if (data[pc] == "*title*") {
		incPC();
		rowTitle = data[pc];
		incPC();
	}
	if (data[pc] == "*referrers*") {
		incPC();
		rowReferrers = data[pc];
		incPC();
	}
	if (data[pc] == "*components*") {
		if (compileComponentList() == 0) {
			return 0;
		}
	}		
	rowAccesses = data[pc];	
	incPC();
	rowAccessesPer = data[pc];	
	incPC();
	if (withBytes) {
		rowBytes = data[pc];
		incPC();
		rowBytesPer = data[pc];
		incPC();
	}
	if (withVisits) {
		rowVisits = data[pc];
		incPC();
		rowVisitsPer = data[pc];
		incPC();
	}
	if (withDownloads) {
		rowDownloadsPer = data[pc];
		incPC();
	}
	subreportRow();	
	return 1;
}

function compileComponentList()
{
	if (data[pc] != "*components*") {
		return 0;
	}
	incPC();
	var alreadyFirst = componentsByPage[rowItem + ".1"];
	while (dataEnd == 0) {
		if (data[pc] == "*end*") {
			incPC();
			return 1;
		}
		rowComponents++;
		var index = data[pc];
		incPC();
		if (!alreadyFirst) {
			addComponent(rowItem, index);
		}
	}
	return -1;
}

function listComponents(w, item)
{
	var i = 1;
	while (1) {
		var s = componentsByPage[item + "." + i];
		if (!s) {
			return;
		}
		var offset = s * 5;
		var name = componentsData[offset++];
		var accesses = componentsData[offset++];
		var bytes = componentsData[offset++];
		var visits = componentsData[offset++];
		var downloadsper = componentsData[offset++];
		w.document.writeln("<td>");
		w.document.writeln(name);
		w.document.writeln("</td>");
		w.document.writeln("<td align=right>");
		w.document.writeln(accesses);
		w.document.writeln("</td>");
		if (withBytes) {
			w.document.writeln("<td align=right>");
			w.document.writeln(bytes);
			w.document.writeln("</td>");
		}	
		if (withVisits) {
			w.document.writeln("<td align=right>");
			w.document.writeln(visits);
			w.document.writeln("</td>");
		}	
		if (withDownloads) {
			w.document.writeln("<td align=right>");
			w.document.writeln(downloadsper);
			w.document.writeln("</td>");
		}	
		w.document.writeln("</tr>");
		i++;
	}
}

function addComponent(item, index)
{
	var i = 1;
	while (1) {
		var s = componentsByPage[item + "." + i];
		if (!s) {
			break;
		}
		i++;
	}
	componentsByPage[item + "." + i] = index;
}

function wl(s)
{
	document.writeln(s);
}
function documentsHead()
{
	wl("<h2>" + epochRange + "</h2>");
	

wl("<h3>Menu</h3>");

wl("<div class='colmenu'>");
wl("<div class='column'>");
wl("<ul id='infolankar'>");
wl("<li><a href=\"../../../../index.php\">Calendar of Reports</a></li>");
wl("<li><a href=\"index.php\">Executive Summary and Totals</a></li>");
wl("<li><a href=\"hours.php\">Accesses by Hour</a></li>");
wl("<li><a href=\"days.php\">Accesses by Day</a></li>");
wl("<li><a href=\"documents.php\">Top Documents</a></li>");
wl("<li><a href=\"directories.php\">Documents by Directory</a></li>");
wl("<li><a href=\"types.php\">File Types (Extensions)</a></li>");

wl("</ul>");
wl("</div>");
wl("<div class='column'>");
wl("<ul id='infolankar'>");
wl("<li><a href=\"sites.php\">Top Visitor Sites</a></li>");
wl("<li><a href=\"useragents.php\">Top Web Browsers</a></li>");
wl("<li><a href=\"os.php\">Top Operating Systems</a></li>");
wl("<li><a href=\"referringsites.php\">Top Referring Sites</a></li>");
wl("<li><a href=\"notfound.php\">Documents Not Found</a></li>");
wl("<li><a href=\"resultcodes.php\">Accesses by Result Code</a></li>");

wl("</ul>");
wl("</div>");
wl("<div class='colmenufoot'></div>");
wl("</div>");

}
function documentsTail()
{
}
function subreportHead()
{
	wl("<h3><a name=\"" + subreportKeyword + "\">");
	if (subreportAll) {
		wl("All " + subreportTitle);
	} else {
		wl("Top " + subreportTop + " of " + subreportTotal + " " + subreportTitle + ", ");
	}
        if (sortedByAccesses) {
                wl("Sorted by Access Count");
        } else if (sortedByBytes) {
                wl("Sorted by Bytes");
        } else if (sortedByVisits) {
                wl("Sorted by Visits");
        }
        if (alphabetized) {
                wl("and Alphabetized");
        }
	wl("</a></h3><p></p>"); 

	if (subreportPie) {
		wl("<div align=\"center\">");
		wl(subreportPie);
		wl("</div>");
	}
	wl("<table cellspacing=\"0\" class=\"styladtabell\">");
	wl("<thead>");
	wl("<tr>");
	wl("<th scope=\"col\">Rank</th>");
	wl("<th scope=\"col\">Item</th>");
	wl("<th scope=\"col\">Accesses</th>");
	wl("<th scope=\"col\">%</th>");
	if (withBytes) {
		wl("<th scope=\"col\">Bytes</th>");
		wl("<th scope=\"col\">%</th>");
	}
	if (withVisits) {
		wl("<th scope=\"col\">Visits</th>");
		wl("<th scope=\"col\">%</th>");
	}
	wl("</tr>");
	wl("</thead>");
	wl("<tbody>");
}	
function subreportTail()
{
	wl("</tbody>");
	wl("</table>");
}
function subreportRow()
{
        var gc = "jamn";
        var dirLink = 0;
        if (subreportGreenbarFlag) {
                gc = "udda";
        }
        subreportGreenbarFlag = !subreportGreenbarFlag;
	wl("<tr class=\"" + gc + "\">");
	wl("<th class=\"hogerstalld\">" + rowRank + "</th>");
	wl("<td align=\"right\">");
	if (rowLocalLink) {
		wl("<a href=\"" + rowLocalLink + "\">");
	}
	if (rowTitle) {
		wl("<b>" + rowTitle + "</b><br>");
	}
	wl(rowItem);
	if (rowLocalLink) {
		wl("</a>");
	}	
	if (rowReferrers) {
		wl("<a href=\"" + rowReferrers + "\" target=\"referrers\">[Referrers]</a>");
	}
	wl("</td>");
	wl("<td align=\"right\">" + rowAccesses + "</td>");
	wl("<td align=\"right\">" + rowAccessesPer + "</td>");
	if (withBytes) {
		wl("<td align=\"right\">" + rowBytes + "</td>");
		wl("<td align=\"right\">" + rowBytesPer + "</td>");
	}
	if (withVisits) {
		wl("<td align=\"right\">" + rowVisits + "</td>");
		wl("<td align=\"right\">" + rowVisitsPer + "</td>");
	}
	if (withDownloads) {
		wl("<td align=\"right\">" + rowDownloadsPer + "</td>");
	}
	wl("</tr>");
}

compileReports(0);	
</script>
