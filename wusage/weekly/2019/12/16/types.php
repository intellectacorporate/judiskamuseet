


<h2>Statistics for http://www.u5645470.fsdata.se, Week of 2019-12-16 to 2019-12-22</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 10 of 18 File Types (Extensions), 
Sorted by Access Count

</h3>
<p>Individual file types as determined by file extensions. All URLs that
do not contain an extension are counted as directories.</p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Type</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th class="hogerstalld">1</th>
	<td>
	Directory (folder)		
	</td>
	<td align="right">30,934</td>
	<td align="right">43.49</td>
	<td align="right">275,633,013</td>
	<td align="right">8.58</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	png		
	</td>
	<td align="right">16,362</td>
	<td align="right">23.00</td>
	<td align="right">220,839,565</td>
	<td align="right">6.87</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	js		
	</td>
	<td align="right">7,968</td>
	<td align="right">11.20</td>
	<td align="right">162,021,862</td>
	<td align="right">5.04</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	jpg		
	</td>
	<td align="right">7,730</td>
	<td align="right">10.87</td>
	<td align="right">2,107,237,743</td>
	<td align="right">65.59</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	php		
	</td>
	<td align="right">3,533</td>
	<td align="right">4.97</td>
	<td align="right">82,730,033</td>
	<td align="right">2.58</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	css		
	</td>
	<td align="right">3,078</td>
	<td align="right">4.33</td>
	<td align="right">162,769,197</td>
	<td align="right">5.07</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	ttf		
	</td>
	<td align="right">917</td>
	<td align="right">1.29</td>
	<td align="right">34,283,808</td>
	<td align="right">1.07</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	txt		
	</td>
	<td align="right">369</td>
	<td align="right">0.52</td>
	<td align="right">23,694</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	jpeg		
	</td>
	<td align="right">82</td>
	<td align="right">0.12</td>
	<td align="right">107,867,571</td>
	<td align="right">3.36</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">10</th>
	<td>
	ico		
	</td>
	<td align="right">48</td>
	<td align="right">0.07</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>

</tbody>
</table>

