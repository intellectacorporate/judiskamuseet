


<h2>Statistics for http://www.u5645470.fsdata.se, Week of 2019-12-16 to 2019-12-22</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 20 of 179 Referring Sites,
Sorted by Access Count

</h3>
<p></p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Referring Site</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th class="hogerstalld">1</th>
	<td>
	<a href="https://judiskamuseet.se">
	https://judiskamuseet.se</a>
	</td>
	<td align="right">27,084</td>
	<td align="right">89.10</td>
	<td align="right">2,621,457,928</td>
	<td align="right">97.44</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	<a href="http://judiskamuseet.se">
	http://judiskamuseet.se</a>
	</td>
	<td align="right">1,763</td>
	<td align="right">5.80</td>
	<td align="right">13,599,344</td>
	<td align="right">0.51</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	<a href="https://www.google.com">
	https://www.google.com</a>
	</td>
	<td align="right">701</td>
	<td align="right">2.31</td>
	<td align="right">30,282,997</td>
	<td align="right">1.13</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	<a href="https://www.google.se">
	https://www.google.se</a>
	</td>
	<td align="right">353</td>
	<td align="right">1.16</td>
	<td align="right">13,292,415</td>
	<td align="right">0.49</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	<a href="http://www.google.com">
	http://www.google.com</a>
	</td>
	<td align="right">43</td>
	<td align="right">0.14</td>
	<td align="right">443,411</td>
	<td align="right">0.02</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	<a href="https://www.bing.com">
	https://www.bing.com</a>
	</td>
	<td align="right">27</td>
	<td align="right">0.09</td>
	<td align="right">558,765</td>
	<td align="right">0.02</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	<a href="http://www.judiska-museet.se">
	http://www.judiska-museet.se</a>
	</td>
	<td align="right">12</td>
	<td align="right">0.04</td>
	<td align="right">180</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	<a href="https://r.search.yahoo.com">
	https://r.search.yahoo.com</a>
	</td>
	<td align="right">10</td>
	<td align="right">0.03</td>
	<td align="right">116,728</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	<a href="https://myactivity.google.com">
	https://myactivity.google.com</a>
	</td>
	<td align="right">9</td>
	<td align="right">0.03</td>
	<td align="right">1,098,576</td>
	<td align="right">0.04</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">10</th>
	<td>
	<a href="https://en.wikipedia.org">
	https://en.wikipedia.org</a>
	</td>
	<td align="right">8</td>
	<td align="right">0.03</td>
	<td align="right">58,908</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">11</th>
	<td>
	<a href="https://89.221.250.27:443">
	https://89.221.250.27:443</a>
	</td>
	<td align="right">7</td>
	<td align="right">0.02</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">12</th>
	<td>
	<a href="https://l.facebook.com">
	https://l.facebook.com</a>
	</td>
	<td align="right">7</td>
	<td align="right">0.02</td>
	<td align="right">57,675</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">13</th>
	<td>
	<a href="https://duckduckgo.com">
	https://duckduckgo.com</a>
	</td>
	<td align="right">6</td>
	<td align="right">0.02</td>
	<td align="right">86,525</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">14</th>
	<td>
	<a href="http://posetke.ru">
	http://posetke.ru</a>
	</td>
	<td align="right">6</td>
	<td align="right">0.02</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">15</th>
	<td>
	<a href="https://azinoofficial777.ru">
	https://azinoofficial777.ru</a>
	</td>
	<td align="right">6</td>
	<td align="right">0.02</td>
	<td align="right">327,036</td>
	<td align="right">0.01</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">16</th>
	<td>
	<a href="https://glam-lady.ru">
	https://glam-lady.ru</a>
	</td>
	<td align="right">6</td>
	<td align="right">0.02</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">17</th>
	<td>
	<a href="https://moyaskidka.ru">
	https://moyaskidka.ru</a>
	</td>
	<td align="right">6</td>
	<td align="right">0.02</td>
	<td align="right">345,462</td>
	<td align="right">0.01</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">18</th>
	<td>
	<a href="https://jav-fetish.com">
	https://jav-fetish.com</a>
	</td>
	<td align="right">6</td>
	<td align="right">0.02</td>
	<td align="right">327,036</td>
	<td align="right">0.01</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">19</th>
	<td>
	<a href="https://allabouttravelinc.com">
	https://allabouttravelinc.com</a>
	</td>
	<td align="right">6</td>
	<td align="right">0.02</td>
	<td align="right">327,036</td>
	<td align="right">0.01</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">20</th>
	<td>
	<a href="https://pornhive.org">
	https://pornhive.org</a>
	</td>
	<td align="right">6</td>
	<td align="right">0.02</td>
	<td align="right">327,036</td>
	<td align="right">0.01</td>
</tr>

</tbody>
</table>

