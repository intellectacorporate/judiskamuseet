


<h2>Statistics for http://www.u5645470.fsdata.se, Week of 2019-12-30 to 2020-1-5</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 20 of 153 Referring Sites,
Sorted by Access Count

</h3>
<p></p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Referring Site</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th class="hogerstalld">1</th>
	<td>
	<a href="https://judiskamuseet.se">
	https://judiskamuseet.se</a>
	</td>
	<td align="right">21,479</td>
	<td align="right">87.34</td>
	<td align="right">2,538,996,659</td>
	<td align="right">97.37</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	<a href="http://judiskamuseet.se">
	http://judiskamuseet.se</a>
	</td>
	<td align="right">1,741</td>
	<td align="right">7.08</td>
	<td align="right">17,276,119</td>
	<td align="right">0.66</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	<a href="https://www.google.com">
	https://www.google.com</a>
	</td>
	<td align="right">520</td>
	<td align="right">2.11</td>
	<td align="right">17,802,309</td>
	<td align="right">0.68</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	<a href="https://www.google.se">
	https://www.google.se</a>
	</td>
	<td align="right">340</td>
	<td align="right">1.38</td>
	<td align="right">8,056,922</td>
	<td align="right">0.31</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	<a href="http://www.google.com">
	http://www.google.com</a>
	</td>
	<td align="right">59</td>
	<td align="right">0.24</td>
	<td align="right">383,494</td>
	<td align="right">0.01</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	<a href="https://www.bing.com">
	https://www.bing.com</a>
	</td>
	<td align="right">24</td>
	<td align="right">0.10</td>
	<td align="right">2,030,548</td>
	<td align="right">0.08</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	<a href="http://www.stockholmmuseums.se">
	http://www.stockholmmuseums.se</a>
	</td>
	<td align="right">22</td>
	<td align="right">0.09</td>
	<td align="right">163,056</td>
	<td align="right">0.01</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	<a href="https://stop-nark.ru">
	https://stop-nark.ru</a>
	</td>
	<td align="right">12</td>
	<td align="right">0.05</td>
	<td align="right">717,942</td>
	<td align="right">0.03</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	<a href="https://eldoradorent.az">
	https://eldoradorent.az</a>
	</td>
	<td align="right">12</td>
	<td align="right">0.05</td>
	<td align="right">649,473</td>
	<td align="right">0.02</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">10</th>
	<td>
	<a href="https://translate.googleusercontent.com">
	https://translate.googleusercontent.com</a>
	</td>
	<td align="right">12</td>
	<td align="right">0.05</td>
	<td align="right">746,809</td>
	<td align="right">0.03</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">11</th>
	<td>
	<a href="https://otopleniedomov.com">
	https://otopleniedomov.com</a>
	</td>
	<td align="right">12</td>
	<td align="right">0.05</td>
	<td align="right">654,072</td>
	<td align="right">0.03</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">12</th>
	<td>
	<a href="http://www.judiska-museet.se">
	http://www.judiska-museet.se</a>
	</td>
	<td align="right">10</td>
	<td align="right">0.04</td>
	<td align="right">68,967</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">13</th>
	<td>
	<a href="https://furniturehomewares.com">
	https://furniturehomewares.com</a>
	</td>
	<td align="right">9</td>
	<td align="right">0.04</td>
	<td align="right">485,955</td>
	<td align="right">0.02</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">14</th>
	<td>
	<a href="https://sauni-moskva.ru">
	https://sauni-moskva.ru</a>
	</td>
	<td align="right">9</td>
	<td align="right">0.04</td>
	<td align="right">487,488</td>
	<td align="right">0.02</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">15</th>
	<td>
	<a href="https://duckduckgo.com">
	https://duckduckgo.com</a>
	</td>
	<td align="right">8</td>
	<td align="right">0.03</td>
	<td align="right">114,832</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">16</th>
	<td>
	<a href="https://fitodar.com.ua">
	https://fitodar.com.ua</a>
	</td>
	<td align="right">6</td>
	<td align="right">0.02</td>
	<td align="right">325,503</td>
	<td align="right">0.01</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">17</th>
	<td>
	<a href="https://mir-tourista.ru">
	https://mir-tourista.ru</a>
	</td>
	<td align="right">6</td>
	<td align="right">0.02</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">18</th>
	<td>
	<a href="https://vulkan-oficial.com">
	https://vulkan-oficial.com</a>
	</td>
	<td align="right">6</td>
	<td align="right">0.02</td>
	<td align="right">325,503</td>
	<td align="right">0.01</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">19</th>
	<td>
	<a href="https://savial.ru">
	https://savial.ru</a>
	</td>
	<td align="right">6</td>
	<td align="right">0.02</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">20</th>
	<td>
	<a href="https://avtolombard-voronezh.ru">
	https://avtolombard-voronezh.ru</a>
	</td>
	<td align="right">6</td>
	<td align="right">0.02</td>
	<td align="right">327,036</td>
	<td align="right">0.01</td>
</tr>

</tbody>
</table>

