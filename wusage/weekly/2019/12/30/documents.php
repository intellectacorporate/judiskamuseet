

<script>
var componentsData = new Array(
        
        "*end*"
);
var componentsByPage = new Array();
var epochRange = "";
var sortedByAccesses = 0;
var sortedByBytes = 0;
var sortedByVisits = 0;
var alphabetized = 0;
var withBytes = 0;
var withVisits = 0;
var withDownloads = 0;
var subreportKeyword = "";
var subreportTitle = "";
var subreportAll = 0;
var subreportTop = 0;
var subreportTotal = 0;
var subreportPie = "";
var subreportGreenbarFlag = 0;
var pc = 0, ac = 0;
var dataEnd = 0;
var data;
var dataArrays = new Array(
	new Array(
		"Week of 2019-12-30 to 2020-1-5",	
		"*sorted-by-accesses*",
		"*bytes*",
		"*visits*",
		"*subreport*",
	"pages",
	"Documents",
	"*top*",
		30,
	
	"*total*",
	"1,647",
	"*pie*",
	"",
	"*row*",
	"1",
	"/",
	"*local-link*",
		"http://www.u5645470.fsdata.se/",
	
	"23,157",
	"36.69",
	"172,446,697",
		"5.30",
	"844",
		"2.84",
	"*row*",
	"2",
	"/wp-content/uploads/2017/06/Judiska_centr_svart_web-1.png",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/uploads/2017/06/Judiska_centr_svart_web-1.png",
	
	"10,915",
	"17.29",
	"98,999,050",
		"3.04",
	"955",
		"3.22",
	"*row*",
	"3",
	"/wp-content/cache/minify/768dd.css",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/cache/minify/768dd.css",
	
	"945",
	"1.50",
	"94,712,269",
		"2.91",
	"1,025",
		"3.45",
	"*row*",
	"4",
	"/wp-content/cache/minify/fa7ff.css",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/cache/minify/fa7ff.css",
	
	"900",
	"1.43",
	"28,091,406",
		"0.86",
	"989",
		"3.33",
	"*row*",
	"5",
	"/wp-content/cache/minify/1abb3.js",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/cache/minify/1abb3.js",
	
	"898",
	"1.42",
	"40,110,312",
		"1.23",
	"980",
		"3.30",
	"*row*",
	"6",
	"/wp-content/cache/minify/dfacc.js",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/cache/minify/dfacc.js",
	
	"893",
	"1.41",
	"3,324,005",
		"0.10",
	"986",
		"3.32",
	"*row*",
	"7",
	"/wp-content/cache/minify/f6d37.js",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/cache/minify/f6d37.js",
	
	"891",
	"1.41",
	"320,724",
		"0.01",
	"982",
		"3.31",
	"*row*",
	"8",
	"/wp-content/cache/minify/29e8b.js",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/cache/minify/29e8b.js",
	
	"887",
	"1.41",
	"6,844,764",
		"0.21",
	"983",
		"3.31",
	"*row*",
	"9",
	"/wp-content/cache/minify/17e60.js",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/cache/minify/17e60.js",
	
	"875",
	"1.39",
	"59,285,043",
		"1.82",
	"970",
		"3.27",
	"*row*",
	"10",
	"/wp-content/themes/Divi/core/admin/fonts/modules.ttf",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/themes/Divi/core/admin/fonts/modules.ttf",
	
	"832",
	"1.32",
	"31,429,015",
		"0.97",
	"912",
		"3.07",
	"*row*",
	"11",
	"/wp-includes/js/wp-emoji-release.min.js",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-includes/js/wp-emoji-release.min.js",
	
	"636",
	"1.01",
	"2,976,464",
		"0.09",
	"701",
		"2.36",
	"*row*",
	"12",
	"/wp-login.php",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-login.php",
	
	"620",
	"0.98",
	"967,708",
		"0.03",
	"326",
		"1.10",
	"*row*",
	"13",
	"/wp-content/uploads/2019/05/cropped-Judiska_svart-2-192x192.<br>\npng",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/uploads/2019/05/cropped-Judiska_svart-2-192x192.png",
	
	"571",
	"0.90",
	"3,810,854",
		"0.12",
	"608",
		"2.05",
	"*row*",
	"14",
	"/wp-content/themes/divi-child/img/location.png",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/themes/divi-child/img/location.png",
	
	"561",
	"0.89",
	"1,713,294",
		"0.05",
	"634",
		"2.14",
	"*row*",
	"15",
	"/wp-content/themes/divi-child/img/Instagram.png",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/themes/divi-child/img/Instagram.png",
	
	"535",
	"0.85",
	"3,224,763",
		"0.10",
	"603",
		"2.03",
	"*row*",
	"16",
	"/wp-content/uploads/2019/05/cropped-Judiska_svart-2-32x32.pn<br>\ng",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/uploads/2019/05/cropped-Judiska_svart-2-32x32.png",
	
	"535",
	"0.85",
	"402,948",
		"0.01",
	"551",
		"1.86",
	"*row*",
	"17",
	"/wp-content/themes/divi-child/img/facebook.png",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/themes/divi-child/img/facebook.png",
	
	"535",
	"0.85",
	"2,576,860",
		"0.08",
	"606",
		"2.04",
	"*row*",
	"18",
	"/wp-content/themes/divi-child/img/clock.png",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/themes/divi-child/img/clock.png",
	
	"529",
	"0.84",
	"1,200,830",
		"0.04",
	"598",
		"2.02",
	"*row*",
	"19",
	"/feed",
	"*local-link*",
		"http://www.u5645470.fsdata.se/feed",
	
	"481",
	"0.76",
	"221,688",
		"0.01",
	"41",
		"0.14",
	"*row*",
	"20",
	"/wp-content/plugins/instagram-slider-widget/assets/in.png",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/plugins/instagram-slider-widget/assets/in.png",
	
	"391",
	"0.62",
	"794,121",
		"0.02",
	"434",
		"1.46",
	"*row*",
	"21",
	"/wp-content/uploads/2019/06/till-webb_4-1.jpg",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/uploads/2019/06/till-webb_4-1.jpg",
	
	"388",
	"0.61",
	"38,946,664",
		"1.20",
	"433",
		"1.46",
	"*row*",
	"22",
	"/wp-content/uploads/2019/07/annons.jpg",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/uploads/2019/07/annons.jpg",
	
	"377",
	"0.60",
	"17,418,117",
		"0.54",
	"408",
		"1.38",
	"*row*",
	"23",
	"/wp-content/uploads/2019/07/mönster.jpg",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/uploads/2019/07/mönster.jpg",
	
	"359",
	"0.57",
	"57,828,438",
		"1.78",
	"397",
		"1.34",
	"*row*",
	"24",
	"/wp-content/uploads/2019/12/munkar.jpg",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/uploads/2019/12/munkar.jpg",
	
	"348",
	"0.55",
	"71,269,459",
		"2.19",
	"384",
		"1.30",
	"*row*",
	"25",
	"/wp-content/uploads/2019/12/Webb-front_0000s_0000_Lager-2.jp<br>\ng",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/uploads/2019/12/Webb-front_0000s_0000_Lager-2.jpg",
	
	"338",
	"0.54",
	"364,684,009",
		"11.21",
	"364",
		"1.23",
	"*row*",
	"26",
	"/wp-content/uploads/2019/12/channukian-shopen.jpg",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/uploads/2019/12/channukian-shopen.jpg",
	
	"323",
	"0.51",
	"94,977,827",
		"2.92",
	"359",
		"1.21",
	"*row*",
	"27",
	"/wp-content/uploads/2019/05/Pil-5.png",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/uploads/2019/05/Pil-5.png",
	
	"322",
	"0.51",
	"83,398",
		"0.00",
	"359",
		"1.21",
	"*row*",
	"28",
	"/robots.txt",
	"*local-link*",
		"http://www.u5645470.fsdata.se/robots.txt",
	
	"318",
	"0.50",
	"21,099",
		"0.00",
	"145",
		"0.49",
	"*row*",
	"29",
	"/wp-content/uploads/2019/12/DSF7890_edit.jpg",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/uploads/2019/12/DSF7890_edit.jpg",
	
	"317",
	"0.50",
	"120,003,203",
		"3.69",
	"353",
		"1.19",
	"*row*",
	"30",
	"/xmlrpc.php",
	"*local-link*",
		"http://www.u5645470.fsdata.se/xmlrpc.php",
	
	"273",
	"0.43",
	"66,235",
		"0.00",
	"322",
		"1.08",
	
		"*end*"
		)
	);

function incPC()
{
	pc++;
	if (pc == data.length) {
		if ((ac == dataArrays.length) || (dataArrays[ac] == "*end*")) {
			dataEnd = 1;
		} else {
			ac++;
			data = dataArrays[ac];
			pc = 0;
		}
	}	
}

function resetPC()
{
	pc = 0;
	ac = 0;
	if (dataArrays.length) {
		data = dataArrays[0];
		dataEnd = 0;
	} else {	
		dataEnd = 1;
	}
}

function compileReports()
{
	resetPC();
	compileDocuments();
}

function compileDocuments()
{
	var s = data[pc];
	epochRange = data[pc];
	incPC();
	while (dataEnd == 0) {
		var s = data[pc];
		if (s == "*sorted-by-accesses*") {
			sortedByAccesses = 1;
			incPC();
		} else if (s == "*sorted-by-bytes*") {
			sortedByBytes = 1;
			incPC();
		} else if (s == "*sorted-by-visits*") {
			sortedByVisits = 1;
			incPC();
		} else if (s == "*alphabetized*") {
			alphabetized = 1;
			incPC();
		} else if (s == "*bytes*") {
			withBytes = 1;
			incPC();
		} else if (s == "*visits*") {
			withVisits = 1;
			incPC();
		} else if (s == "*downloads*") {
			withDownloads = 1;
			incPC();
		} else {
			break;
		}
	}
	documentsHead();
	while (dataEnd == 0) {
		if (compileSubreport() == 0) {
			// No more subreports. That's OK 
			break;
		}	
	}
	documentsTail();
	return 1;
}
function compileSubreport()
{
	if (data[pc] != "*subreport*") {
		return 0;
	}
	incPC();	
	subreportKeyword = "";
	subreportTitle = "";
	subreportAll = 0;
	subreportTop = 0;
	subreportTotal = 0;
	subreportPie = "";
	subreportKeyword = data[pc];
	incPC();
	subreportTitle = data[pc];
	incPC();
	if (data[pc] == "*all*") {
		incPC();
		subreportAll = 1;
	}
	if (data[pc] == "*top*") {
		incPC();
		subreportTop = data[pc];
		incPC();
	}
	if (data[pc] == "*total*") {
		incPC();
		subreportTotal = data[pc];
		incPC();
	}
	if (data[pc] == "*pie*") {
		incPC();
		subreportPie = data[pc];
		incPC();
	}
	subreportHead();
	while (dataEnd == 0) {
		if (compileSubreportRow() == 0) {
			// No more subreport rows. That's OK 
			break;
		}
	}
	subreportTail();
	return 1;
}
var rowRank = 0;
var rowItem = "";
var rowLocalLink = "";
var rowTitle = "";
var rowReferrers = "";
var rowAccesses = 0;
var rowAccessesPer = 0;
var rowBytes = 0;
var rowBytesPer = 0;
var rowVisits = 0;
var rowVisitsPer = 0;
var rowDownloadsPer = 0;
var rowComponents = 0;

function compileSubreportRow()
{
	if (data[pc] != "*row*") {
		return 0;
	}
	rowRank = 0;
	rowItem = "";
	rowLocalLink = "";
	rowTitle = "";
	rowReferrers = "";
	rowComponents = 0;
	incPC();
	rowRank = data[pc];
	incPC();
	rowItem = data[pc];
	incPC();
	if (data[pc] == "*local-link*") {
		incPC();
		rowLocalLink = data[pc];
		incPC();
	}
	if (data[pc] == "*title*") {
		incPC();
		rowTitle = data[pc];
		incPC();
	}
	if (data[pc] == "*referrers*") {
		incPC();
		rowReferrers = data[pc];
		incPC();
	}
	if (data[pc] == "*components*") {
		if (compileComponentList() == 0) {
			return 0;
		}
	}		
	rowAccesses = data[pc];	
	incPC();
	rowAccessesPer = data[pc];	
	incPC();
	if (withBytes) {
		rowBytes = data[pc];
		incPC();
		rowBytesPer = data[pc];
		incPC();
	}
	if (withVisits) {
		rowVisits = data[pc];
		incPC();
		rowVisitsPer = data[pc];
		incPC();
	}
	if (withDownloads) {
		rowDownloadsPer = data[pc];
		incPC();
	}
	subreportRow();	
	return 1;
}

function compileComponentList()
{
	if (data[pc] != "*components*") {
		return 0;
	}
	incPC();
	var alreadyFirst = componentsByPage[rowItem + ".1"];
	while (dataEnd == 0) {
		if (data[pc] == "*end*") {
			incPC();
			return 1;
		}
		rowComponents++;
		var index = data[pc];
		incPC();
		if (!alreadyFirst) {
			addComponent(rowItem, index);
		}
	}
	return -1;
}

function listComponents(w, item)
{
	var i = 1;
	while (1) {
		var s = componentsByPage[item + "." + i];
		if (!s) {
			return;
		}
		var offset = s * 5;
		var name = componentsData[offset++];
		var accesses = componentsData[offset++];
		var bytes = componentsData[offset++];
		var visits = componentsData[offset++];
		var downloadsper = componentsData[offset++];
		w.document.writeln("<td>");
		w.document.writeln(name);
		w.document.writeln("</td>");
		w.document.writeln("<td align=right>");
		w.document.writeln(accesses);
		w.document.writeln("</td>");
		if (withBytes) {
			w.document.writeln("<td align=right>");
			w.document.writeln(bytes);
			w.document.writeln("</td>");
		}	
		if (withVisits) {
			w.document.writeln("<td align=right>");
			w.document.writeln(visits);
			w.document.writeln("</td>");
		}	
		if (withDownloads) {
			w.document.writeln("<td align=right>");
			w.document.writeln(downloadsper);
			w.document.writeln("</td>");
		}	
		w.document.writeln("</tr>");
		i++;
	}
}

function addComponent(item, index)
{
	var i = 1;
	while (1) {
		var s = componentsByPage[item + "." + i];
		if (!s) {
			break;
		}
		i++;
	}
	componentsByPage[item + "." + i] = index;
}

function wl(s)
{
	document.writeln(s);
}
function documentsHead()
{
	wl("<h2>" + epochRange + "</h2>");
	

wl("<h3>Menu</h3>");

wl("<div class='colmenu'>");
wl("<div class='column'>");
wl("<ul id='infolankar'>");
wl("<li><a href=\"../../../../index.php\">Calendar of Reports</a></li>");
wl("<li><a href=\"index.php\">Executive Summary and Totals</a></li>");
wl("<li><a href=\"hours.php\">Accesses by Hour</a></li>");
wl("<li><a href=\"days.php\">Accesses by Day</a></li>");
wl("<li><a href=\"documents.php\">Top Documents</a></li>");
wl("<li><a href=\"directories.php\">Documents by Directory</a></li>");
wl("<li><a href=\"types.php\">File Types (Extensions)</a></li>");

wl("</ul>");
wl("</div>");
wl("<div class='column'>");
wl("<ul id='infolankar'>");
wl("<li><a href=\"sites.php\">Top Visitor Sites</a></li>");
wl("<li><a href=\"useragents.php\">Top Web Browsers</a></li>");
wl("<li><a href=\"os.php\">Top Operating Systems</a></li>");
wl("<li><a href=\"referringsites.php\">Top Referring Sites</a></li>");
wl("<li><a href=\"notfound.php\">Documents Not Found</a></li>");
wl("<li><a href=\"resultcodes.php\">Accesses by Result Code</a></li>");

wl("</ul>");
wl("</div>");
wl("<div class='colmenufoot'></div>");
wl("</div>");

}
function documentsTail()
{
}
function subreportHead()
{
	wl("<h3><a name=\"" + subreportKeyword + "\">");
	if (subreportAll) {
		wl("All " + subreportTitle);
	} else {
		wl("Top " + subreportTop + " of " + subreportTotal + " " + subreportTitle + ", ");
	}
        if (sortedByAccesses) {
                wl("Sorted by Access Count");
        } else if (sortedByBytes) {
                wl("Sorted by Bytes");
        } else if (sortedByVisits) {
                wl("Sorted by Visits");
        }
        if (alphabetized) {
                wl("and Alphabetized");
        }
	wl("</a></h3><p></p>"); 

	if (subreportPie) {
		wl("<div align=\"center\">");
		wl(subreportPie);
		wl("</div>");
	}
	wl("<table cellspacing=\"0\" class=\"styladtabell\">");
	wl("<thead>");
	wl("<tr>");
	wl("<th scope=\"col\">Rank</th>");
	wl("<th scope=\"col\">Item</th>");
	wl("<th scope=\"col\">Accesses</th>");
	wl("<th scope=\"col\">%</th>");
	if (withBytes) {
		wl("<th scope=\"col\">Bytes</th>");
		wl("<th scope=\"col\">%</th>");
	}
	if (withVisits) {
		wl("<th scope=\"col\">Visits</th>");
		wl("<th scope=\"col\">%</th>");
	}
	wl("</tr>");
	wl("</thead>");
	wl("<tbody>");
}	
function subreportTail()
{
	wl("</tbody>");
	wl("</table>");
}
function subreportRow()
{
        var gc = "jamn";
        var dirLink = 0;
        if (subreportGreenbarFlag) {
                gc = "udda";
        }
        subreportGreenbarFlag = !subreportGreenbarFlag;
	wl("<tr class=\"" + gc + "\">");
	wl("<th class=\"hogerstalld\">" + rowRank + "</th>");
	wl("<td align=\"right\">");
	if (rowLocalLink) {
		wl("<a href=\"" + rowLocalLink + "\">");
	}
	if (rowTitle) {
		wl("<b>" + rowTitle + "</b><br>");
	}
	wl(rowItem);
	if (rowLocalLink) {
		wl("</a>");
	}	
	if (rowReferrers) {
		wl("<a href=\"" + rowReferrers + "\" target=\"referrers\">[Referrers]</a>");
	}
	wl("</td>");
	wl("<td align=\"right\">" + rowAccesses + "</td>");
	wl("<td align=\"right\">" + rowAccessesPer + "</td>");
	if (withBytes) {
		wl("<td align=\"right\">" + rowBytes + "</td>");
		wl("<td align=\"right\">" + rowBytesPer + "</td>");
	}
	if (withVisits) {
		wl("<td align=\"right\">" + rowVisits + "</td>");
		wl("<td align=\"right\">" + rowVisitsPer + "</td>");
	}
	if (withDownloads) {
		wl("<td align=\"right\">" + rowDownloadsPer + "</td>");
	}
	wl("</tr>");
}

compileReports(0);	
</script>
