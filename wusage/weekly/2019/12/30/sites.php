


<h2>Statistics for http://www.u5645470.fsdata.se, Week of 2019-12-30 to 2020-1-5</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 20 of 2,758 Visitor Sites,
Sorted by Access Count

</h3>
<p></p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Site</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="jamnudda">
	<th class="hogerstalld">1</th>
	<td>
	94.75.211.74		
	</td>
	<td align="right">2,985</td>
	<td align="right">4.73</td>
	<td align="right">24,083,953</td>
	<td align="right">0.74</td>
	<td align="right">8</td>
	<td align="right">0.31</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	94.247.174.83		
	</td>
	<td align="right">2,981</td>
	<td align="right">4.72</td>
	<td align="right">24,078,117</td>
	<td align="right">0.74</td>
	<td align="right">8</td>
	<td align="right">0.31</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	85.195.116.134		
	</td>
	<td align="right">2,963</td>
	<td align="right">4.69</td>
	<td align="right">24,034,857</td>
	<td align="right">0.74</td>
	<td align="right">8</td>
	<td align="right">0.31</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	185.70.76.23		
	</td>
	<td align="right">2,963</td>
	<td align="right">4.69</td>
	<td align="right">24,083,513</td>
	<td align="right">0.74</td>
	<td align="right">8</td>
	<td align="right">0.31</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	185.152.65.167		
	</td>
	<td align="right">2,356</td>
	<td align="right">3.73</td>
	<td align="right">18,412,685</td>
	<td align="right">0.57</td>
	<td align="right">3</td>
	<td align="right">0.13</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	109.123.101.103		
	</td>
	<td align="right">1,974</td>
	<td align="right">3.13</td>
	<td align="right">14,941,339</td>
	<td align="right">0.46</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	5.172.196.188		
	</td>
	<td align="right">1,960</td>
	<td align="right">3.11</td>
	<td align="right">14,955,817</td>
	<td align="right">0.46</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	185.136.156.82		
	</td>
	<td align="right">1,956</td>
	<td align="right">3.10</td>
	<td align="right">14,940,973</td>
	<td align="right">0.46</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	81.17.62.205		
	</td>
	<td align="right">1,758</td>
	<td align="right">2.79</td>
	<td align="right">14,864,184</td>
	<td align="right">0.46</td>
	<td align="right">3</td>
	<td align="right">0.13</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">10</th>
	<td>
	63.143.42.242		
	</td>
	<td align="right">1,316</td>
	<td align="right">2.09</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">11</th>
	<td>
	46.246.122.10		
	</td>
	<td align="right">1,300</td>
	<td align="right">2.06</td>
	<td align="right">9,842,480</td>
	<td align="right">0.30</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">12</th>
	<td>
	37.252.231.50		
	</td>
	<td align="right">1,023</td>
	<td align="right">1.62</td>
	<td align="right">8,275,728</td>
	<td align="right">0.25</td>
	<td align="right">3</td>
	<td align="right">0.14</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">13</th>
	<td>
	82.103.145.126		
	</td>
	<td align="right">1,008</td>
	<td align="right">1.60</td>
	<td align="right">9,142,560</td>
	<td align="right">0.28</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">14</th>
	<td>
	151.106.52.134		
	</td>
	<td align="right">1,008</td>
	<td align="right">1.60</td>
	<td align="right">9,142,560</td>
	<td align="right">0.28</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">15</th>
	<td>
	185.39.146.214		
	</td>
	<td align="right">1,007</td>
	<td align="right">1.60</td>
	<td align="right">9,133,490</td>
	<td align="right">0.28</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">16</th>
	<td>
	82.103.136.16		
	</td>
	<td align="right">876</td>
	<td align="right">1.39</td>
	<td align="right">7,945,320</td>
	<td align="right">0.24</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">17</th>
	<td>
	178.255.153.2		
	</td>
	<td align="right">810</td>
	<td align="right">1.28</td>
	<td align="right">6,570,477</td>
	<td align="right">0.20</td>
	<td align="right">3</td>
	<td align="right">0.13</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">18</th>
	<td>
	185.246.208.82		
	</td>
	<td align="right">794</td>
	<td align="right">1.26</td>
	<td align="right">6,277,732</td>
	<td align="right">0.19</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">19</th>
	<td>
	66.249.64.174		
	</td>
	<td align="right">404</td>
	<td align="right">0.64</td>
	<td align="right">3,233,311</td>
	<td align="right">0.10</td>
	<td align="right">93</td>
	<td align="right">3.52</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">20</th>
	<td>
	88.198.25.244		
	</td>
	<td align="right">357</td>
	<td align="right">0.57</td>
	<td align="right">4,505,261</td>
	<td align="right">0.14</td>
	<td align="right">1</td>
	<td align="right">0.04</td>
</tr>

</tbody>
</table>

