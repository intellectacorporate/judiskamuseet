


<h2>Statistics for http://www.u5645470.fsdata.se, Week of 2019-12-30 to 2020-1-5</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 10 of 174 Browsers (User Agents),
Sorted by Access Count

</h3>
<p></p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Product</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th class="hogerstalld">1</th>
	<td>
	Pingdom.com_bot_version_1.4_		
	</td>
	<td align="right">29,717</td>
	<td align="right">47.29</td>
	<td align="right">240,725,780</td>
	<td align="right">7.47</td>
	<td align="right">46</td>
	<td align="right">1.78</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	Safari/537.36		
	</td>
	<td align="right">8,327</td>
	<td align="right">13.25</td>
	<td align="right">922,740,725</td>
	<td align="right">28.62</td>
	<td align="right">429</td>
	<td align="right">16.32</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	Safari/604.1		
	</td>
	<td align="right">8,290</td>
	<td align="right">13.19</td>
	<td align="right">923,297,678</td>
	<td align="right">28.64</td>
	<td align="right">495</td>
	<td align="right">18.83</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	Mozilla/5.0 		
	</td>
	<td align="right">3,343</td>
	<td align="right">5.32</td>
	<td align="right">243,546,114</td>
	<td align="right">7.55</td>
	<td align="right">426</td>
	<td align="right">16.18</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	Safari/605.1.15		
	</td>
	<td align="right">1,740</td>
	<td align="right">2.77</td>
	<td align="right">188,810,916</td>
	<td align="right">5.86</td>
	<td align="right">90</td>
	<td align="right">3.43</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	UptimeRobot/2.0		
	</td>
	<td align="right">1,316</td>
	<td align="right">2.09</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	bingbot/2.0		
	</td>
	<td align="right">1,290</td>
	<td align="right">2.05</td>
	<td align="right">96,821,525</td>
	<td align="right">3.00</td>
	<td align="right">296</td>
	<td align="right">11.27</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	Safari/537.36 Edge/18.18362		
	</td>
	<td align="right">933</td>
	<td align="right">1.48</td>
	<td align="right">136,688,456</td>
	<td align="right">4.24</td>
	<td align="right">36</td>
	<td align="right">1.39</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	Safari/537.36 (compatible; Googlebot/2.1;  http://www.google.com/bot.html)		
	</td>
	<td align="right">820</td>
	<td align="right">1.31</td>
	<td align="right">5,414,622</td>
	<td align="right">0.17</td>
	<td align="right">204</td>
	<td align="right">7.77</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">10</th>
	<td>
	msnbot/2.0b 		
	</td>
	<td align="right">487</td>
	<td align="right">0.78</td>
	<td align="right">19,686,670</td>
	<td align="right">0.61</td>
	<td align="right">104</td>
	<td align="right">3.97</td>
</tr>

</tbody>
</table>

