


<h2>Statistics for http://www.u5645470.fsdata.se, Week of 2019-12-30 to 2020-1-5</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 10 of 12 Operating Systems,
Sorted by Access Count

</h3>
<p></p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Product</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th class="hogerstalld">1</th>
	<td>
	Other		
	</td>
	<td align="right">37,000</td>
	<td align="right">58.89</td>
	<td align="right">537,362,126</td>
	<td align="right">16.67</td>
	<td align="right">813</td>
	<td align="right">30.51</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	Mac OS X		
	</td>
	<td align="right">22,814</td>
	<td align="right">36.31</td>
	<td align="right">2,487,530,659</td>
	<td align="right">77.16</td>
	<td align="right">1,443</td>
	<td align="right">54.12</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	Windows NT		
	</td>
	<td align="right">1,857</td>
	<td align="right">2.96</td>
	<td align="right">177,832,995</td>
	<td align="right">5.52</td>
	<td align="right">88</td>
	<td align="right">3.30</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	Linux		
	</td>
	<td align="right">933</td>
	<td align="right">1.48</td>
	<td align="right">7,461,239</td>
	<td align="right">0.23</td>
	<td align="right">314</td>
	<td align="right">11.80</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	Windows XP		
	</td>
	<td align="right">108</td>
	<td align="right">0.17</td>
	<td align="right">5,726,123</td>
	<td align="right">0.18</td>
	<td align="right">3</td>
	<td align="right">0.13</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	Windows Codename Longhorn		
	</td>
	<td align="right">68</td>
	<td align="right">0.11</td>
	<td align="right">6,064,920</td>
	<td align="right">0.19</td>
	<td align="right">3</td>
	<td align="right">0.14</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	Windows 2003		
	</td>
	<td align="right">20</td>
	<td align="right">0.03</td>
	<td align="right">395,883</td>
	<td align="right">0.01</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	Windows 95		
	</td>
	<td align="right">18</td>
	<td align="right">0.03</td>
	<td align="right">981,108</td>
	<td align="right">0.03</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	Windows 2000		
	</td>
	<td align="right">11</td>
	<td align="right">0.02</td>
	<td align="right">395,883</td>
	<td align="right">0.01</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">10</th>
	<td>
	Windows Me		
	</td>
	<td align="right">3</td>
	<td align="right">0.00</td>
	<td align="right">163,518</td>
	<td align="right">0.01</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>

</tbody>
</table>

