


<h2>Statistics for http://www.u5645470.fsdata.se, Week of 2019-12-30 to 2020-1-5</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Traffic by Hour</h3>
<p>Figures are averages for that hour
	of each day in the time period analyzed.</p>
<img src="h.png" width="468" height="351">
<br /><br />
<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Hour</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	<th scope='col'>Bits per Sec</th>
	<th scope='col'>Bytes per Sec</th>
	
	</tr>
</thead>
<tbody>
<tr class="jamnudda">
	<th align="right">00:00</th>
	<td align="right">313.00</td>
	<td align="right">3.47</td>
	<td align="right">14,493,458.71</td>
	<td align="right">3.12</td>
	<td align="right">7.30</td>
	<td align="right">1.92</td>
	<td align="right">32,207.69</td>
	<td align="right">4,025.96</td>
	
</tr>
<tr class="jamn">
	<th align="right">01:00</th>
	<td align="right">316.29</td>
	<td align="right">3.51</td>
	<td align="right">11,643,032.86</td>
	<td align="right">2.51</td>
	<td align="right">9.68</td>
	<td align="right">2.54</td>
	<td align="right">25,873.41</td>
	<td align="right">3,234.18</td>
	
</tr>
<tr class="udda">
	<th align="right">02:00</th>
	<td align="right">280.86</td>
	<td align="right">3.12</td>
	<td align="right">8,009,574.00</td>
	<td align="right">1.72</td>
	<td align="right">9.01</td>
	<td align="right">2.37</td>
	<td align="right">17,799.05</td>
	<td align="right">2,224.88</td>
	
</tr>
<tr class="jamn">
	<th align="right">03:00</th>
	<td align="right">294.29</td>
	<td align="right">3.26</td>
	<td align="right">7,295,736.57</td>
	<td align="right">1.57</td>
	<td align="right">7.45</td>
	<td align="right">1.96</td>
	<td align="right">16,212.75</td>
	<td align="right">2,026.59</td>
	
</tr>
<tr class="udda">
	<th align="right">04:00</th>
	<td align="right">251.57</td>
	<td align="right">2.79</td>
	<td align="right">5,446,753.71</td>
	<td align="right">1.17</td>
	<td align="right">8.55</td>
	<td align="right">2.24</td>
	<td align="right">12,103.90</td>
	<td align="right">1,512.99</td>
	
</tr>
<tr class="jamn">
	<th align="right">05:00</th>
	<td align="right">260.86</td>
	<td align="right">2.89</td>
	<td align="right">4,600,237.57</td>
	<td align="right">0.99</td>
	<td align="right">10.76</td>
	<td align="right">2.82</td>
	<td align="right">10,222.75</td>
	<td align="right">1,277.84</td>
	
</tr>
<tr class="udda">
	<th align="right">06:00</th>
	<td align="right">260.86</td>
	<td align="right">2.89</td>
	<td align="right">4,280,581.71</td>
	<td align="right">0.92</td>
	<td align="right">8.87</td>
	<td align="right">2.33</td>
	<td align="right">9,512.40</td>
	<td align="right">1,189.05</td>
	
</tr>
<tr class="jamn">
	<th align="right">07:00</th>
	<td align="right">266.14</td>
	<td align="right">2.95</td>
	<td align="right">5,965,275.86</td>
	<td align="right">1.28</td>
	<td align="right">12.03</td>
	<td align="right">3.16</td>
	<td align="right">13,256.17</td>
	<td align="right">1,657.02</td>
	
</tr>
<tr class="udda">
	<th align="right">08:00</th>
	<td align="right">314.57</td>
	<td align="right">3.49</td>
	<td align="right">12,740,410.29</td>
	<td align="right">2.74</td>
	<td align="right">13.03</td>
	<td align="right">3.42</td>
	<td align="right">28,312.02</td>
	<td align="right">3,539.00</td>
	
</tr>
<tr class="jamn">
	<th align="right">09:00</th>
	<td align="right">408.14</td>
	<td align="right">4.53</td>
	<td align="right">21,096,391.57</td>
	<td align="right">4.54</td>
	<td align="right">20.98</td>
	<td align="right">5.51</td>
	<td align="right">46,880.87</td>
	<td align="right">5,860.11</td>
	
</tr>
<tr class="udda">
	<th align="right">10:00</th>
	<td align="right">443.00</td>
	<td align="right">4.91</td>
	<td align="right">27,068,863.29</td>
	<td align="right">5.82</td>
	<td align="right">19.45</td>
	<td align="right">5.10</td>
	<td align="right">60,153.03</td>
	<td align="right">7,519.13</td>
	
</tr>
<tr class="jamn">
	<th align="right">11:00</th>
	<td align="right">491.71</td>
	<td align="right">5.45</td>
	<td align="right">30,026,949.71</td>
	<td align="right">6.46</td>
	<td align="right">23.23</td>
	<td align="right">6.10</td>
	<td align="right">66,726.55</td>
	<td align="right">8,340.82</td>
	
</tr>
<tr class="udda">
	<th align="right">12:00</th>
	<td align="right">455.86</td>
	<td align="right">5.06</td>
	<td align="right">33,489,753.00</td>
	<td align="right">7.21</td>
	<td align="right">20.83</td>
	<td align="right">5.47</td>
	<td align="right">74,421.67</td>
	<td align="right">9,302.71</td>
	
</tr>
<tr class="jamn">
	<th align="right">13:00</th>
	<td align="right">466.29</td>
	<td align="right">5.17</td>
	<td align="right">34,145,702.14</td>
	<td align="right">7.35</td>
	<td align="right">18.21</td>
	<td align="right">4.78</td>
	<td align="right">75,879.34</td>
	<td align="right">9,484.92</td>
	
</tr>
<tr class="udda">
	<th align="right">14:00</th>
	<td align="right">498.29</td>
	<td align="right">5.53</td>
	<td align="right">31,744,950.71</td>
	<td align="right">6.83</td>
	<td align="right">24.53</td>
	<td align="right">6.44</td>
	<td align="right">70,544.33</td>
	<td align="right">8,818.04</td>
	
</tr>
<tr class="jamn">
	<th align="right">15:00</th>
	<td align="right">438.00</td>
	<td align="right">4.86</td>
	<td align="right">34,649,843.86</td>
	<td align="right">7.46</td>
	<td align="right">19.77</td>
	<td align="right">5.19</td>
	<td align="right">76,999.65</td>
	<td align="right">9,624.96</td>
	
</tr>
<tr class="udda">
	<th align="right">16:00</th>
	<td align="right">429.86</td>
	<td align="right">4.77</td>
	<td align="right">25,708,591.43</td>
	<td align="right">5.53</td>
	<td align="right">17.22</td>
	<td align="right">4.52</td>
	<td align="right">57,130.20</td>
	<td align="right">7,141.28</td>
	
</tr>
<tr class="jamn">
	<th align="right">17:00</th>
	<td align="right">403.86</td>
	<td align="right">4.48</td>
	<td align="right">26,968,285.71</td>
	<td align="right">5.80</td>
	<td align="right">16.26</td>
	<td align="right">4.27</td>
	<td align="right">59,929.52</td>
	<td align="right">7,491.19</td>
	
</tr>
<tr class="udda">
	<th align="right">18:00</th>
	<td align="right">420.57</td>
	<td align="right">4.66</td>
	<td align="right">23,000,262.86</td>
	<td align="right">4.95</td>
	<td align="right">14.65</td>
	<td align="right">3.85</td>
	<td align="right">51,111.70</td>
	<td align="right">6,388.96</td>
	
</tr>
<tr class="jamn">
	<th align="right">19:00</th>
	<td align="right">433.14</td>
	<td align="right">4.80</td>
	<td align="right">26,658,296.29</td>
	<td align="right">5.74</td>
	<td align="right">19.36</td>
	<td align="right">5.08</td>
	<td align="right">59,240.66</td>
	<td align="right">7,405.08</td>
	
</tr>
<tr class="udda">
	<th align="right">20:00</th>
	<td align="right">407.14</td>
	<td align="right">4.52</td>
	<td align="right">17,856,618.71</td>
	<td align="right">3.84</td>
	<td align="right">18.32</td>
	<td align="right">4.81</td>
	<td align="right">39,681.37</td>
	<td align="right">4,960.17</td>
	
</tr>
<tr class="jamn">
	<th align="right">21:00</th>
	<td align="right">429.00</td>
	<td align="right">4.76</td>
	<td align="right">21,965,707.00</td>
	<td align="right">4.73</td>
	<td align="right">17.49</td>
	<td align="right">4.59</td>
	<td align="right">48,812.68</td>
	<td align="right">6,101.59</td>
	
</tr>
<tr class="udda">
	<th align="right">22:00</th>
	<td align="right">389.43</td>
	<td align="right">4.32</td>
	<td align="right">22,251,942.43</td>
	<td align="right">4.79</td>
	<td align="right">18.03</td>
	<td align="right">4.73</td>
	<td align="right">49,448.76</td>
	<td align="right">6,181.10</td>
	
</tr>
<tr class="jamn">
	<th align="right">23:00</th>
	<td align="right">343.14</td>
	<td align="right">3.81</td>
	<td align="right">13,660,772.14</td>
	<td align="right">2.94</td>
	<td align="right">26.00</td>
	<td align="right">6.82</td>
	<td align="right">30,357.27</td>
	<td align="right">3,794.66</td>
	
</tr>

</tbody>
</table>

