


<h2>Statistics for http://www.u5645470.fsdata.se, Week of 2019-12-30 to 2020-1-5</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Traffic by Day</h3>
<img src="d.png" width="468" height="351">
<br /><br />


<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Date</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	<th scope='col'>Bits per Sec</th>
	<th scope='col'>Bytes per Sec</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th align="right">2019-12-30</th>
	<td align="right">7,897	</td>
	<td align="right">12.51	</td>
	<td align="right">301,611,407	</td>
	<td align="right">9.27	</td>
	<td align="right">413	</td>
	<td align="right">15.49	</td>
	<td align="right">27,926.98	</td>
	<td align="right">3,490.87	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-12-31</th>
	<td align="right">7,631	</td>
	<td align="right">12.09	</td>
	<td align="right">299,443,168	</td>
	<td align="right">9.20	</td>
	<td align="right">391	</td>
	<td align="right">14.66	</td>
	<td align="right">27,726.22	</td>
	<td align="right">3,465.78	</td>
</tr>
<tr class="udda">
	<th align="right">2020-1-1</th>
	<td align="right">8,954	</td>
	<td align="right">14.19	</td>
	<td align="right">484,495,549	</td>
	<td align="right">14.89	</td>
	<td align="right">313	</td>
	<td align="right">11.74	</td>
	<td align="right">44,860.70	</td>
	<td align="right">5,607.59	</td>
</tr>
<tr class="jamn">
	<th align="right">2020-1-2</th>
	<td align="right">9,962	</td>
	<td align="right">15.78	</td>
	<td align="right">602,318,604	</td>
	<td align="right">18.51	</td>
	<td align="right">354	</td>
	<td align="right">13.27	</td>
	<td align="right">55,770.24	</td>
	<td align="right">6,971.28	</td>
</tr>
<tr class="udda">
	<th align="right">2020-1-3</th>
	<td align="right">9,759	</td>
	<td align="right">15.46	</td>
	<td align="right">565,553,364	</td>
	<td align="right">17.38	</td>
	<td align="right">385	</td>
	<td align="right">14.44	</td>
	<td align="right">52,366.05	</td>
	<td align="right">6,545.76	</td>
</tr>
<tr class="jamn">
	<th align="right">2020-1-4</th>
	<td align="right">10,256	</td>
	<td align="right">16.25	</td>
	<td align="right">587,207,076	</td>
	<td align="right">18.05	</td>
	<td align="right">444	</td>
	<td align="right">16.65	</td>
	<td align="right">54,371.03	</td>
	<td align="right">6,796.38	</td>
</tr>
<tr class="udda">
	<th align="right">2020-1-5</th>
	<td align="right">8,652	</td>
	<td align="right">13.71	</td>
	<td align="right">412,746,777	</td>
	<td align="right">12.69	</td>
	<td align="right">367	</td>
	<td align="right">13.76	</td>
	<td align="right">38,217.29	</td>
	<td align="right">4,777.16	</td>
</tr>

</tbody>
</table>

