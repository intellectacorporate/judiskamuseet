


<h2>Statistics for http://www.u5645470.fsdata.se, Week of 2019-12-23 to 2019-12-29</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 20 of 189 Referring Sites,
Sorted by Access Count

</h3>
<p></p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Referring Site</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th class="hogerstalld">1</th>
	<td>
	<a href="https://judiskamuseet.se">
	https://judiskamuseet.se</a>
	</td>
	<td align="right">20,986</td>
	<td align="right">86.83</td>
	<td align="right">2,330,428,496</td>
	<td align="right">98.13</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	<a href="http://judiskamuseet.se">
	http://judiskamuseet.se</a>
	</td>
	<td align="right">1,731</td>
	<td align="right">7.16</td>
	<td align="right">9,859,054</td>
	<td align="right">0.42</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	<a href="https://www.google.com">
	https://www.google.com</a>
	</td>
	<td align="right">506</td>
	<td align="right">2.09</td>
	<td align="right">11,091,183</td>
	<td align="right">0.47</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	<a href="https://www.google.se">
	https://www.google.se</a>
	</td>
	<td align="right">371</td>
	<td align="right">1.54</td>
	<td align="right">10,420,747</td>
	<td align="right">0.44</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	<a href="http://www.google.com">
	http://www.google.com</a>
	</td>
	<td align="right">55</td>
	<td align="right">0.23</td>
	<td align="right">380,247</td>
	<td align="right">0.02</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	<a href="http://m.facebook.com">
	http://m.facebook.com</a>
	</td>
	<td align="right">23</td>
	<td align="right">0.10</td>
	<td align="right">255,781</td>
	<td align="right">0.01</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	<a href="https://www.bing.com">
	https://www.bing.com</a>
	</td>
	<td align="right">13</td>
	<td align="right">0.05</td>
	<td align="right">668,907</td>
	<td align="right">0.03</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	<a href="ttps://furniturehomewares.com">
	ttps://furniturehomewares.com</a>
	</td>
	<td align="right">9</td>
	<td align="right">0.04</td>
	<td align="right">490,554</td>
	<td align="right">0.02</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	<a href="https://travel-semantics.com">
	https://travel-semantics.com</a>
	</td>
	<td align="right">9</td>
	<td align="right">0.04</td>
	<td align="right">490,554</td>
	<td align="right">0.02</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">10</th>
	<td>
	<a href="http://www.stockholmmuseums.se">
	http://www.stockholmmuseums.se</a>
	</td>
	<td align="right">8</td>
	<td align="right">0.03</td>
	<td align="right">59,252</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">11</th>
	<td>
	<a href="https://sv.m.wikipedia.org">
	https://sv.m.wikipedia.org</a>
	</td>
	<td align="right">8</td>
	<td align="right">0.03</td>
	<td align="right">59,272</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">12</th>
	<td>
	<a href="https://duckduckgo.com">
	https://duckduckgo.com</a>
	</td>
	<td align="right">8</td>
	<td align="right">0.03</td>
	<td align="right">102,973</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">13</th>
	<td>
	<a href="https://r.search.yahoo.com">
	https://r.search.yahoo.com</a>
	</td>
	<td align="right">8</td>
	<td align="right">0.03</td>
	<td align="right">113,009</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">14</th>
	<td>
	<a href="https://myactivity.google.com">
	https://myactivity.google.com</a>
	</td>
	<td align="right">7</td>
	<td align="right">0.03</td>
	<td align="right">831,630</td>
	<td align="right">0.04</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">15</th>
	<td>
	<a href="http://www.judiska-museet.se">
	http://www.judiska-museet.se</a>
	</td>
	<td align="right">7</td>
	<td align="right">0.03</td>
	<td align="right">80</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">16</th>
	<td>
	<a href="http://www.tsatu.edu.ua">
	http://www.tsatu.edu.ua</a>
	</td>
	<td align="right">6</td>
	<td align="right">0.02</td>
	<td align="right">327,036</td>
	<td align="right">0.01</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">17</th>
	<td>
	<a href="https://89.221.250.27:443">
	https://89.221.250.27:443</a>
	</td>
	<td align="right">6</td>
	<td align="right">0.02</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">18</th>
	<td>
	<a href="https://torrentred.games">
	https://torrentred.games</a>
	</td>
	<td align="right">6</td>
	<td align="right">0.02</td>
	<td align="right">327,036</td>
	<td align="right">0.01</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">19</th>
	<td>
	<a href="https://latestnewsmalta.com">
	https://latestnewsmalta.com</a>
	</td>
	<td align="right">6</td>
	<td align="right">0.02</td>
	<td align="right">327,036</td>
	<td align="right">0.01</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">20</th>
	<td>
	<a href="https://mnimmigrantrights.net">
	https://mnimmigrantrights.net</a>
	</td>
	<td align="right">6</td>
	<td align="right">0.02</td>
	<td align="right">354,675</td>
	<td align="right">0.01</td>
</tr>

</tbody>
</table>

