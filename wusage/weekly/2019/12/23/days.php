


<h2>Statistics for http://www.u5645470.fsdata.se, Week of 2019-12-23 to 2019-12-29</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Traffic by Day</h3>
<img src="d.png" width="468" height="351">
<br /><br />


<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Date</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	<th scope='col'>Bits per Sec</th>
	<th scope='col'>Bytes per Sec</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th align="right">2019-12-23</th>
	<td align="right">9,377	</td>
	<td align="right">14.56	</td>
	<td align="right">463,280,911	</td>
	<td align="right">15.56	</td>
	<td align="right">315	</td>
	<td align="right">11.32	</td>
	<td align="right">42,896.38	</td>
	<td align="right">5,362.05	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-12-24</th>
	<td align="right">8,873	</td>
	<td align="right">13.77	</td>
	<td align="right">387,548,607	</td>
	<td align="right">13.01	</td>
	<td align="right">395	</td>
	<td align="right">14.20	</td>
	<td align="right">35,884.13	</td>
	<td align="right">4,485.52	</td>
</tr>
<tr class="udda">
	<th align="right">2019-12-25</th>
	<td align="right">7,978	</td>
	<td align="right">12.39	</td>
	<td align="right">243,176,488	</td>
	<td align="right">8.17	</td>
	<td align="right">366	</td>
	<td align="right">13.16	</td>
	<td align="right">22,516.34	</td>
	<td align="right">2,814.54	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-12-26</th>
	<td align="right">9,127	</td>
	<td align="right">14.17	</td>
	<td align="right">438,593,319	</td>
	<td align="right">14.73	</td>
	<td align="right">347	</td>
	<td align="right">12.47	</td>
	<td align="right">40,610.49	</td>
	<td align="right">5,076.31	</td>
</tr>
<tr class="udda">
	<th align="right">2019-12-27</th>
	<td align="right">9,739	</td>
	<td align="right">15.12	</td>
	<td align="right">467,087,447	</td>
	<td align="right">15.69	</td>
	<td align="right">493	</td>
	<td align="right">17.72	</td>
	<td align="right">43,248.84	</td>
	<td align="right">5,406.10	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-12-28</th>
	<td align="right">10,121	</td>
	<td align="right">15.71	</td>
	<td align="right">538,836,987	</td>
	<td align="right">18.09	</td>
	<td align="right">495	</td>
	<td align="right">17.79	</td>
	<td align="right">49,892.31	</td>
	<td align="right">6,236.54	</td>
</tr>
<tr class="udda">
	<th align="right">2019-12-29</th>
	<td align="right">9,201	</td>
	<td align="right">14.28	</td>
	<td align="right">439,323,225	</td>
	<td align="right">14.75	</td>
	<td align="right">371	</td>
	<td align="right">13.34	</td>
	<td align="right">40,678.08	</td>
	<td align="right">5,084.76	</td>
</tr>

</tbody>
</table>

