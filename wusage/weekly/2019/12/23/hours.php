


<h2>Statistics for http://www.u5645470.fsdata.se, Week of 2019-12-23 to 2019-12-29</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Traffic by Hour</h3>
<p>Figures are averages for that hour
	of each day in the time period analyzed.</p>
<img src="h.png" width="468" height="351">
<br /><br />
<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Hour</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	<th scope='col'>Bits per Sec</th>
	<th scope='col'>Bytes per Sec</th>
	
	</tr>
</thead>
<tbody>
<tr class="jamnudda">
	<th align="right">00:00</th>
	<td align="right">365.29</td>
	<td align="right">3.97</td>
	<td align="right">14,138,403.57</td>
	<td align="right">3.32</td>
	<td align="right">14.23</td>
	<td align="right">3.58</td>
	<td align="right">31,418.67</td>
	<td align="right">3,927.33</td>
	
</tr>
<tr class="jamn">
	<th align="right">01:00</th>
	<td align="right">277.14</td>
	<td align="right">3.01</td>
	<td align="right">7,833,744.57</td>
	<td align="right">1.84</td>
	<td align="right">10.59</td>
	<td align="right">2.66</td>
	<td align="right">17,408.32</td>
	<td align="right">2,176.04</td>
	
</tr>
<tr class="udda">
	<th align="right">02:00</th>
	<td align="right">285.71</td>
	<td align="right">3.10</td>
	<td align="right">6,107,006.29</td>
	<td align="right">1.44</td>
	<td align="right">8.92</td>
	<td align="right">2.25</td>
	<td align="right">13,571.13</td>
	<td align="right">1,696.39</td>
	
</tr>
<tr class="jamn">
	<th align="right">03:00</th>
	<td align="right">260.29</td>
	<td align="right">2.83</td>
	<td align="right">3,363,277.00</td>
	<td align="right">0.79</td>
	<td align="right">8.67</td>
	<td align="right">2.18</td>
	<td align="right">7,473.95</td>
	<td align="right">934.24</td>
	
</tr>
<tr class="udda">
	<th align="right">04:00</th>
	<td align="right">243.14</td>
	<td align="right">2.64</td>
	<td align="right">4,011,107.43</td>
	<td align="right">0.94</td>
	<td align="right">9.00</td>
	<td align="right">2.26</td>
	<td align="right">8,913.57</td>
	<td align="right">1,114.20</td>
	
</tr>
<tr class="jamn">
	<th align="right">05:00</th>
	<td align="right">265.71</td>
	<td align="right">2.89</td>
	<td align="right">4,437,487.71</td>
	<td align="right">1.04</td>
	<td align="right">7.27</td>
	<td align="right">1.83</td>
	<td align="right">9,861.08</td>
	<td align="right">1,232.64</td>
	
</tr>
<tr class="udda">
	<th align="right">06:00</th>
	<td align="right">278.57</td>
	<td align="right">3.03</td>
	<td align="right">7,343,886.00</td>
	<td align="right">1.73</td>
	<td align="right">9.41</td>
	<td align="right">2.37</td>
	<td align="right">16,319.75</td>
	<td align="right">2,039.97</td>
	
</tr>
<tr class="jamn">
	<th align="right">07:00</th>
	<td align="right">266.57</td>
	<td align="right">2.90</td>
	<td align="right">5,573,720.29</td>
	<td align="right">1.31</td>
	<td align="right">13.78</td>
	<td align="right">3.47</td>
	<td align="right">12,386.05</td>
	<td align="right">1,548.26</td>
	
</tr>
<tr class="udda">
	<th align="right">08:00</th>
	<td align="right">327.00</td>
	<td align="right">3.55</td>
	<td align="right">13,844,330.43</td>
	<td align="right">3.25</td>
	<td align="right">10.03</td>
	<td align="right">2.52</td>
	<td align="right">30,765.18</td>
	<td align="right">3,845.65</td>
	
</tr>
<tr class="jamn">
	<th align="right">09:00</th>
	<td align="right">395.29</td>
	<td align="right">4.30</td>
	<td align="right">16,046,449.86</td>
	<td align="right">3.77</td>
	<td align="right">20.14</td>
	<td align="right">5.07</td>
	<td align="right">35,658.78</td>
	<td align="right">4,457.35</td>
	
</tr>
<tr class="udda">
	<th align="right">10:00</th>
	<td align="right">516.14</td>
	<td align="right">5.61</td>
	<td align="right">36,877,394.43</td>
	<td align="right">8.67</td>
	<td align="right">20.87</td>
	<td align="right">5.25</td>
	<td align="right">81,949.77</td>
	<td align="right">10,243.72</td>
	
</tr>
<tr class="jamn">
	<th align="right">11:00</th>
	<td align="right">452.43</td>
	<td align="right">4.92</td>
	<td align="right">30,240,089.86</td>
	<td align="right">7.11</td>
	<td align="right">23.92</td>
	<td align="right">6.02</td>
	<td align="right">67,200.20</td>
	<td align="right">8,400.02</td>
	
</tr>
<tr class="udda">
	<th align="right">12:00</th>
	<td align="right">452.14</td>
	<td align="right">4.91</td>
	<td align="right">25,576,030.14</td>
	<td align="right">6.01</td>
	<td align="right">17.98</td>
	<td align="right">4.52</td>
	<td align="right">56,835.62</td>
	<td align="right">7,104.45</td>
	
</tr>
<tr class="jamn">
	<th align="right">13:00</th>
	<td align="right">407.29</td>
	<td align="right">4.43</td>
	<td align="right">19,532,430.29</td>
	<td align="right">4.59</td>
	<td align="right">16.79</td>
	<td align="right">4.22</td>
	<td align="right">43,405.40</td>
	<td align="right">5,425.68</td>
	
</tr>
<tr class="udda">
	<th align="right">14:00</th>
	<td align="right">487.00</td>
	<td align="right">5.29</td>
	<td align="right">33,586,583.57</td>
	<td align="right">7.90</td>
	<td align="right">21.01</td>
	<td align="right">5.29</td>
	<td align="right">74,636.85</td>
	<td align="right">9,329.61</td>
	
</tr>
<tr class="jamn">
	<th align="right">15:00</th>
	<td align="right">473.00</td>
	<td align="right">5.14</td>
	<td align="right">19,672,410.71</td>
	<td align="right">4.62</td>
	<td align="right">19.82</td>
	<td align="right">4.99</td>
	<td align="right">43,716.47</td>
	<td align="right">5,464.56</td>
	
</tr>
<tr class="udda">
	<th align="right">16:00</th>
	<td align="right">482.29</td>
	<td align="right">5.24</td>
	<td align="right">24,914,161.57</td>
	<td align="right">5.86</td>
	<td align="right">18.29</td>
	<td align="right">4.60</td>
	<td align="right">55,364.80</td>
	<td align="right">6,920.60</td>
	
</tr>
<tr class="jamn">
	<th align="right">17:00</th>
	<td align="right">392.71</td>
	<td align="right">4.27</td>
	<td align="right">17,909,562.57</td>
	<td align="right">4.21</td>
	<td align="right">17.85</td>
	<td align="right">4.49</td>
	<td align="right">39,799.03</td>
	<td align="right">4,974.88</td>
	
</tr>
<tr class="udda">
	<th align="right">18:00</th>
	<td align="right">388.86</td>
	<td align="right">4.23</td>
	<td align="right">18,869,371.57</td>
	<td align="right">4.44</td>
	<td align="right">16.93</td>
	<td align="right">4.26</td>
	<td align="right">41,931.94</td>
	<td align="right">5,241.49</td>
	
</tr>
<tr class="jamn">
	<th align="right">19:00</th>
	<td align="right">449.57</td>
	<td align="right">4.89</td>
	<td align="right">25,153,132.29</td>
	<td align="right">5.91</td>
	<td align="right">20.57</td>
	<td align="right">5.18</td>
	<td align="right">55,895.85</td>
	<td align="right">6,986.98</td>
	
</tr>
<tr class="udda">
	<th align="right">20:00</th>
	<td align="right">438.43</td>
	<td align="right">4.76</td>
	<td align="right">20,656,286.00</td>
	<td align="right">4.86</td>
	<td align="right">20.28</td>
	<td align="right">5.10</td>
	<td align="right">45,902.86</td>
	<td align="right">5,737.86</td>
	
</tr>
<tr class="jamn">
	<th align="right">21:00</th>
	<td align="right">442.00</td>
	<td align="right">4.80</td>
	<td align="right">26,391,726.29</td>
	<td align="right">6.20</td>
	<td align="right">21.72</td>
	<td align="right">5.47</td>
	<td align="right">58,648.28</td>
	<td align="right">7,331.04</td>
	
</tr>
<tr class="udda">
	<th align="right">22:00</th>
	<td align="right">454.00</td>
	<td align="right">4.93</td>
	<td align="right">24,066,749.43</td>
	<td align="right">5.66</td>
	<td align="right">19.61</td>
	<td align="right">4.93</td>
	<td align="right">53,481.67</td>
	<td align="right">6,685.21</td>
	
</tr>
<tr class="jamn">
	<th align="right">23:00</th>
	<td align="right">401.71</td>
	<td align="right">4.37</td>
	<td align="right">19,261,370.14</td>
	<td align="right">4.53</td>
	<td align="right">29.73</td>
	<td align="right">7.48</td>
	<td align="right">42,803.04</td>
	<td align="right">5,350.38</td>
	
</tr>

</tbody>
</table>

