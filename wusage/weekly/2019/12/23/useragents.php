


<h2>Statistics for http://www.u5645470.fsdata.se, Week of 2019-12-23 to 2019-12-29</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 10 of 205 Browsers (User Agents),
Sorted by Access Count

</h3>
<p></p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Product</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th class="hogerstalld">1</th>
	<td>
	Pingdom.com_bot_version_1.4_		
	</td>
	<td align="right">29,692</td>
	<td align="right">46.15</td>
	<td align="right">240,725,197</td>
	<td align="right">8.13</td>
	<td align="right">40</td>
	<td align="right">1.47</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	Safari/537.36		
	</td>
	<td align="right">9,597</td>
	<td align="right">14.92</td>
	<td align="right">962,954,843</td>
	<td align="right">32.51</td>
	<td align="right">475</td>
	<td align="right">17.17</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	Safari/604.1		
	</td>
	<td align="right">8,461</td>
	<td align="right">13.15</td>
	<td align="right">894,257,467</td>
	<td align="right">30.19</td>
	<td align="right">511</td>
	<td align="right">18.48</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	Mozilla/5.0 		
	</td>
	<td align="right">3,922</td>
	<td align="right">6.10</td>
	<td align="right">263,917,724</td>
	<td align="right">8.91</td>
	<td align="right">613</td>
	<td align="right">22.14</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	Safari/605.1.15		
	</td>
	<td align="right">1,530</td>
	<td align="right">2.38</td>
	<td align="right">137,845,064</td>
	<td align="right">4.65</td>
	<td align="right">99</td>
	<td align="right">3.60</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	UptimeRobot/2.0		
	</td>
	<td align="right">1,371</td>
	<td align="right">2.13</td>
	<td align="right">29,646</td>
	<td align="right">0.00</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	bingbot/2.0		
	</td>
	<td align="right">1,214</td>
	<td align="right">1.89</td>
	<td align="right">13,095,858</td>
	<td align="right">0.44</td>
	<td align="right">249</td>
	<td align="right">9.01</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	Safari/537.36 (compatible; Googlebot/2.1;  http://www.google.com/bot.html)		
	</td>
	<td align="right">765</td>
	<td align="right">1.19</td>
	<td align="right">6,864,897</td>
	<td align="right">0.23</td>
	<td align="right">183</td>
	<td align="right">6.62</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	curl/7.54.0		
	</td>
	<td align="right">593</td>
	<td align="right">0.92</td>
	<td align="right">5,773,877</td>
	<td align="right">0.19</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">10</th>
	<td>
	facebookexternalhit/1.1 		
	</td>
	<td align="right">509</td>
	<td align="right">0.79</td>
	<td align="right">19,635,101</td>
	<td align="right">0.66</td>
	<td align="right">60</td>
	<td align="right">2.20</td>
</tr>

</tbody>
</table>

