

<script>
var componentsData = new Array(
        
        "*end*"
);
var componentsByPage = new Array();
var epochRange = "";
var sortedByAccesses = 0;
var sortedByBytes = 0;
var sortedByVisits = 0;
var alphabetized = 0;
var withBytes = 0;
var withVisits = 0;
var withDownloads = 0;
var subreportKeyword = "";
var subreportTitle = "";
var subreportAll = 0;
var subreportTop = 0;
var subreportTotal = 0;
var subreportPie = "";
var subreportGreenbarFlag = 0;
var pc = 0, ac = 0;
var dataEnd = 0;
var data;
var dataArrays = new Array(
	new Array(
		"Week of 2019-12-23 to 2019-12-29",	
		"*sorted-by-accesses*",
		"*bytes*",
		"*visits*",
		"*subreport*",
	"pages",
	"Documents",
	"*top*",
		30,
	
	"*total*",
	"1,487",
	"*pie*",
	"",
	"*row*",
	"1",
	"/",
	"*local-link*",
		"http://www.u5645470.fsdata.se/",
	
	"23,521",
	"36.51",
	"173,454,081",
		"5.82",
	"788",
		"2.61",
	"*row*",
	"2",
	"/wp-content/uploads/2017/06/Judiska_centr_svart_web-1.png",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/uploads/2017/06/Judiska_centr_svart_web-1.png",
	
	"10,928",
	"16.96",
	"99,107,890",
		"3.33",
	"992",
		"3.28",
	"*row*",
	"3",
	"/wp-login.php",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-login.php",
	
	"1,110",
	"1.72",
	"1,738,427",
		"0.06",
	"485",
		"1.60",
	"*row*",
	"4",
	"/wp-content/cache/minify/768dd.css",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/cache/minify/768dd.css",
	
	"931",
	"1.45",
	"94,193,960",
		"3.16",
	"1,032",
		"3.41",
	"*row*",
	"5",
	"/wp-content/cache/minify/fa7ff.css",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/cache/minify/fa7ff.css",
	
	"931",
	"1.45",
	"29,053,646",
		"0.98",
	"1,050",
		"3.47",
	"*row*",
	"6",
	"/wp-content/cache/minify/dfacc.js",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/cache/minify/dfacc.js",
	
	"911",
	"1.41",
	"3,381,367",
		"0.11",
	"1,030",
		"3.41",
	"*row*",
	"7",
	"/wp-content/cache/minify/f6d37.js",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/cache/minify/f6d37.js",
	
	"907",
	"1.41",
	"324,440",
		"0.01",
	"1,029",
		"3.40",
	"*row*",
	"8",
	"/wp-content/cache/minify/1abb3.js",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/cache/minify/1abb3.js",
	
	"903",
	"1.40",
	"39,978,724",
		"1.34",
	"1,022",
		"3.38",
	"*row*",
	"9",
	"/wp-content/cache/minify/29e8b.js",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/cache/minify/29e8b.js",
	
	"898",
	"1.39",
	"6,882,806",
		"0.23",
	"1,022",
		"3.38",
	"*row*",
	"10",
	"/wp-content/cache/minify/17e60.js",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/cache/minify/17e60.js",
	
	"873",
	"1.36",
	"59,376,024",
		"1.99",
	"988",
		"3.27",
	"*row*",
	"11",
	"/wp-content/themes/Divi/core/admin/fonts/modules.ttf",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/themes/Divi/core/admin/fonts/modules.ttf",
	
	"842",
	"1.31",
	"31,858,420",
		"1.07",
	"949",
		"3.14",
	"*row*",
	"12",
	"/wp-includes/js/wp-emoji-release.min.js",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-includes/js/wp-emoji-release.min.js",
	
	"665",
	"1.03",
	"3,087,444",
		"0.10",
	"747",
		"2.47",
	"*row*",
	"13",
	"/wp-content/uploads/2019/05/cropped-Judiska_svart-2-192x192.<br>\npng",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/uploads/2019/05/cropped-Judiska_svart-2-192x192.png",
	
	"604",
	"0.94",
	"4,031,096",
		"0.14",
	"650",
		"2.15",
	"*row*",
	"14",
	"/wp-content/themes/divi-child/img/location.png",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/themes/divi-child/img/location.png",
	
	"561",
	"0.87",
	"1,710,240",
		"0.06",
	"634",
		"2.10",
	"*row*",
	"15",
	"/wp-content/themes/divi-child/img/Instagram.png",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/themes/divi-child/img/Instagram.png",
	
	"539",
	"0.84",
	"3,242,982",
		"0.11",
	"608",
		"2.01",
	"*row*",
	"16",
	"/wp-content/themes/divi-child/img/facebook.png",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/themes/divi-child/img/facebook.png",
	
	"536",
	"0.83",
	"2,586,584",
		"0.09",
	"607",
		"2.01",
	"*row*",
	"17",
	"/wp-content/uploads/2019/05/cropped-Judiska_svart-2-32x32.pn<br>\ng",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/uploads/2019/05/cropped-Judiska_svart-2-32x32.png",
	
	"533",
	"0.83",
	"398,412",
		"0.01",
	"565",
		"1.87",
	"*row*",
	"18",
	"/wp-content/themes/divi-child/img/clock.png",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/themes/divi-child/img/clock.png",
	
	"532",
	"0.83",
	"1,205,370",
		"0.04",
	"609",
		"2.02",
	"*row*",
	"19",
	"/feed",
	"*local-link*",
		"http://www.u5645470.fsdata.se/feed",
	
	"530",
	"0.82",
	"276,864",
		"0.01",
	"41",
		"0.14",
	"*row*",
	"20",
	"/xmlrpc.php",
	"*local-link*",
		"http://www.u5645470.fsdata.se/xmlrpc.php",
	
	"427",
	"0.66",
	"103,104",
		"0.00",
	"479",
		"1.58",
	"*row*",
	"21",
	"/wp-content/plugins/instagram-slider-widget/assets/in.png",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/plugins/instagram-slider-widget/assets/in.png",
	
	"360",
	"0.56",
	"729,129",
		"0.02",
	"405",
		"1.34",
	"*row*",
	"22",
	"/wp-content/uploads/2019/06/till-webb_4-1.jpg",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/uploads/2019/06/till-webb_4-1.jpg",
	
	"349",
	"0.54",
	"34,931,544",
		"1.17",
	"390",
		"1.29",
	"*row*",
	"23",
	"/wp-content/uploads/2019/07/annons.jpg",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/uploads/2019/07/annons.jpg",
	
	"334",
	"0.52",
	"15,408,360",
		"0.52",
	"372",
		"1.23",
	"*row*",
	"24",
	"/wp-content/uploads/2019/07/mönster.jpg",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/uploads/2019/07/mönster.jpg",
	
	"327",
	"0.51",
	"52,512,732",
		"1.76",
	"368",
		"1.22",
	"*row*",
	"25",
	"/wp-content/uploads/2019/12/munkar.jpg",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/uploads/2019/12/munkar.jpg",
	
	"323",
	"0.50",
	"66,241,196",
		"2.22",
	"361",
		"1.20",
	"*row*",
	"26",
	"/wp-content/uploads/2019/05/Pil-5.png",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/uploads/2019/05/Pil-5.png",
	
	"312",
	"0.48",
	"80,549",
		"0.00",
	"351",
		"1.16",
	"*row*",
	"27",
	"/robots.txt",
	"*local-link*",
		"http://www.u5645470.fsdata.se/robots.txt",
	
	"310",
	"0.48",
	"20,115",
		"0.00",
	"134",
		"0.45",
	"*row*",
	"28",
	"/wp-content/uploads/2019/12/Webb-front_0000s_0000_Lager-2.jp<br>\ng",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/uploads/2019/12/Webb-front_0000s_0000_Lager-2.jpg",
	
	"303",
	"0.47",
	"328,564,223",
		"11.03",
	"336",
		"1.11",
	"*row*",
	"29",
	"/wp-content/uploads/2019/12/channukian-shopen.jpg",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/uploads/2019/12/channukian-shopen.jpg",
	
	"294",
	"0.46",
	"86,156,357",
		"2.89",
	"332",
		"1.10",
	"*row*",
	"30",
	"/wp-content/uploads/2019/12/DSF7890_edit.jpg",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/uploads/2019/12/DSF7890_edit.jpg",
	
	"292",
	"0.45",
	"110,539,228",
		"3.71",
	"329",
		"1.09",
	
		"*end*"
		)
	);

function incPC()
{
	pc++;
	if (pc == data.length) {
		if ((ac == dataArrays.length) || (dataArrays[ac] == "*end*")) {
			dataEnd = 1;
		} else {
			ac++;
			data = dataArrays[ac];
			pc = 0;
		}
	}	
}

function resetPC()
{
	pc = 0;
	ac = 0;
	if (dataArrays.length) {
		data = dataArrays[0];
		dataEnd = 0;
	} else {	
		dataEnd = 1;
	}
}

function compileReports()
{
	resetPC();
	compileDocuments();
}

function compileDocuments()
{
	var s = data[pc];
	epochRange = data[pc];
	incPC();
	while (dataEnd == 0) {
		var s = data[pc];
		if (s == "*sorted-by-accesses*") {
			sortedByAccesses = 1;
			incPC();
		} else if (s == "*sorted-by-bytes*") {
			sortedByBytes = 1;
			incPC();
		} else if (s == "*sorted-by-visits*") {
			sortedByVisits = 1;
			incPC();
		} else if (s == "*alphabetized*") {
			alphabetized = 1;
			incPC();
		} else if (s == "*bytes*") {
			withBytes = 1;
			incPC();
		} else if (s == "*visits*") {
			withVisits = 1;
			incPC();
		} else if (s == "*downloads*") {
			withDownloads = 1;
			incPC();
		} else {
			break;
		}
	}
	documentsHead();
	while (dataEnd == 0) {
		if (compileSubreport() == 0) {
			// No more subreports. That's OK 
			break;
		}	
	}
	documentsTail();
	return 1;
}
function compileSubreport()
{
	if (data[pc] != "*subreport*") {
		return 0;
	}
	incPC();	
	subreportKeyword = "";
	subreportTitle = "";
	subreportAll = 0;
	subreportTop = 0;
	subreportTotal = 0;
	subreportPie = "";
	subreportKeyword = data[pc];
	incPC();
	subreportTitle = data[pc];
	incPC();
	if (data[pc] == "*all*") {
		incPC();
		subreportAll = 1;
	}
	if (data[pc] == "*top*") {
		incPC();
		subreportTop = data[pc];
		incPC();
	}
	if (data[pc] == "*total*") {
		incPC();
		subreportTotal = data[pc];
		incPC();
	}
	if (data[pc] == "*pie*") {
		incPC();
		subreportPie = data[pc];
		incPC();
	}
	subreportHead();
	while (dataEnd == 0) {
		if (compileSubreportRow() == 0) {
			// No more subreport rows. That's OK 
			break;
		}
	}
	subreportTail();
	return 1;
}
var rowRank = 0;
var rowItem = "";
var rowLocalLink = "";
var rowTitle = "";
var rowReferrers = "";
var rowAccesses = 0;
var rowAccessesPer = 0;
var rowBytes = 0;
var rowBytesPer = 0;
var rowVisits = 0;
var rowVisitsPer = 0;
var rowDownloadsPer = 0;
var rowComponents = 0;

function compileSubreportRow()
{
	if (data[pc] != "*row*") {
		return 0;
	}
	rowRank = 0;
	rowItem = "";
	rowLocalLink = "";
	rowTitle = "";
	rowReferrers = "";
	rowComponents = 0;
	incPC();
	rowRank = data[pc];
	incPC();
	rowItem = data[pc];
	incPC();
	if (data[pc] == "*local-link*") {
		incPC();
		rowLocalLink = data[pc];
		incPC();
	}
	if (data[pc] == "*title*") {
		incPC();
		rowTitle = data[pc];
		incPC();
	}
	if (data[pc] == "*referrers*") {
		incPC();
		rowReferrers = data[pc];
		incPC();
	}
	if (data[pc] == "*components*") {
		if (compileComponentList() == 0) {
			return 0;
		}
	}		
	rowAccesses = data[pc];	
	incPC();
	rowAccessesPer = data[pc];	
	incPC();
	if (withBytes) {
		rowBytes = data[pc];
		incPC();
		rowBytesPer = data[pc];
		incPC();
	}
	if (withVisits) {
		rowVisits = data[pc];
		incPC();
		rowVisitsPer = data[pc];
		incPC();
	}
	if (withDownloads) {
		rowDownloadsPer = data[pc];
		incPC();
	}
	subreportRow();	
	return 1;
}

function compileComponentList()
{
	if (data[pc] != "*components*") {
		return 0;
	}
	incPC();
	var alreadyFirst = componentsByPage[rowItem + ".1"];
	while (dataEnd == 0) {
		if (data[pc] == "*end*") {
			incPC();
			return 1;
		}
		rowComponents++;
		var index = data[pc];
		incPC();
		if (!alreadyFirst) {
			addComponent(rowItem, index);
		}
	}
	return -1;
}

function listComponents(w, item)
{
	var i = 1;
	while (1) {
		var s = componentsByPage[item + "." + i];
		if (!s) {
			return;
		}
		var offset = s * 5;
		var name = componentsData[offset++];
		var accesses = componentsData[offset++];
		var bytes = componentsData[offset++];
		var visits = componentsData[offset++];
		var downloadsper = componentsData[offset++];
		w.document.writeln("<td>");
		w.document.writeln(name);
		w.document.writeln("</td>");
		w.document.writeln("<td align=right>");
		w.document.writeln(accesses);
		w.document.writeln("</td>");
		if (withBytes) {
			w.document.writeln("<td align=right>");
			w.document.writeln(bytes);
			w.document.writeln("</td>");
		}	
		if (withVisits) {
			w.document.writeln("<td align=right>");
			w.document.writeln(visits);
			w.document.writeln("</td>");
		}	
		if (withDownloads) {
			w.document.writeln("<td align=right>");
			w.document.writeln(downloadsper);
			w.document.writeln("</td>");
		}	
		w.document.writeln("</tr>");
		i++;
	}
}

function addComponent(item, index)
{
	var i = 1;
	while (1) {
		var s = componentsByPage[item + "." + i];
		if (!s) {
			break;
		}
		i++;
	}
	componentsByPage[item + "." + i] = index;
}

function wl(s)
{
	document.writeln(s);
}
function documentsHead()
{
	wl("<h2>" + epochRange + "</h2>");
	

wl("<h3>Menu</h3>");

wl("<div class='colmenu'>");
wl("<div class='column'>");
wl("<ul id='infolankar'>");
wl("<li><a href=\"../../../../index.php\">Calendar of Reports</a></li>");
wl("<li><a href=\"index.php\">Executive Summary and Totals</a></li>");
wl("<li><a href=\"hours.php\">Accesses by Hour</a></li>");
wl("<li><a href=\"days.php\">Accesses by Day</a></li>");
wl("<li><a href=\"documents.php\">Top Documents</a></li>");
wl("<li><a href=\"directories.php\">Documents by Directory</a></li>");
wl("<li><a href=\"types.php\">File Types (Extensions)</a></li>");

wl("</ul>");
wl("</div>");
wl("<div class='column'>");
wl("<ul id='infolankar'>");
wl("<li><a href=\"sites.php\">Top Visitor Sites</a></li>");
wl("<li><a href=\"useragents.php\">Top Web Browsers</a></li>");
wl("<li><a href=\"os.php\">Top Operating Systems</a></li>");
wl("<li><a href=\"referringsites.php\">Top Referring Sites</a></li>");
wl("<li><a href=\"notfound.php\">Documents Not Found</a></li>");
wl("<li><a href=\"resultcodes.php\">Accesses by Result Code</a></li>");

wl("</ul>");
wl("</div>");
wl("<div class='colmenufoot'></div>");
wl("</div>");

}
function documentsTail()
{
}
function subreportHead()
{
	wl("<h3><a name=\"" + subreportKeyword + "\">");
	if (subreportAll) {
		wl("All " + subreportTitle);
	} else {
		wl("Top " + subreportTop + " of " + subreportTotal + " " + subreportTitle + ", ");
	}
        if (sortedByAccesses) {
                wl("Sorted by Access Count");
        } else if (sortedByBytes) {
                wl("Sorted by Bytes");
        } else if (sortedByVisits) {
                wl("Sorted by Visits");
        }
        if (alphabetized) {
                wl("and Alphabetized");
        }
	wl("</a></h3><p></p>"); 

	if (subreportPie) {
		wl("<div align=\"center\">");
		wl(subreportPie);
		wl("</div>");
	}
	wl("<table cellspacing=\"0\" class=\"styladtabell\">");
	wl("<thead>");
	wl("<tr>");
	wl("<th scope=\"col\">Rank</th>");
	wl("<th scope=\"col\">Item</th>");
	wl("<th scope=\"col\">Accesses</th>");
	wl("<th scope=\"col\">%</th>");
	if (withBytes) {
		wl("<th scope=\"col\">Bytes</th>");
		wl("<th scope=\"col\">%</th>");
	}
	if (withVisits) {
		wl("<th scope=\"col\">Visits</th>");
		wl("<th scope=\"col\">%</th>");
	}
	wl("</tr>");
	wl("</thead>");
	wl("<tbody>");
}	
function subreportTail()
{
	wl("</tbody>");
	wl("</table>");
}
function subreportRow()
{
        var gc = "jamn";
        var dirLink = 0;
        if (subreportGreenbarFlag) {
                gc = "udda";
        }
        subreportGreenbarFlag = !subreportGreenbarFlag;
	wl("<tr class=\"" + gc + "\">");
	wl("<th class=\"hogerstalld\">" + rowRank + "</th>");
	wl("<td align=\"right\">");
	if (rowLocalLink) {
		wl("<a href=\"" + rowLocalLink + "\">");
	}
	if (rowTitle) {
		wl("<b>" + rowTitle + "</b><br>");
	}
	wl(rowItem);
	if (rowLocalLink) {
		wl("</a>");
	}	
	if (rowReferrers) {
		wl("<a href=\"" + rowReferrers + "\" target=\"referrers\">[Referrers]</a>");
	}
	wl("</td>");
	wl("<td align=\"right\">" + rowAccesses + "</td>");
	wl("<td align=\"right\">" + rowAccessesPer + "</td>");
	if (withBytes) {
		wl("<td align=\"right\">" + rowBytes + "</td>");
		wl("<td align=\"right\">" + rowBytesPer + "</td>");
	}
	if (withVisits) {
		wl("<td align=\"right\">" + rowVisits + "</td>");
		wl("<td align=\"right\">" + rowVisitsPer + "</td>");
	}
	if (withDownloads) {
		wl("<td align=\"right\">" + rowDownloadsPer + "</td>");
	}
	wl("</tr>");
}

compileReports(0);	
</script>
