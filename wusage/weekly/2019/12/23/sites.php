


<h2>Statistics for http://www.u5645470.fsdata.se, Week of 2019-12-23 to 2019-12-29</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 20 of 2,737 Visitor Sites,
Sorted by Access Count

</h3>
<p></p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Site</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="jamnudda">
	<th class="hogerstalld">1</th>
	<td>
	85.195.116.134		
	</td>
	<td align="right">2,967</td>
	<td align="right">4.61</td>
	<td align="right">24,004,795</td>
	<td align="right">0.81</td>
	<td align="right">8</td>
	<td align="right">0.29</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	94.75.211.74		
	</td>
	<td align="right">2,233</td>
	<td align="right">3.47</td>
	<td align="right">17,317,024</td>
	<td align="right">0.58</td>
	<td align="right">2</td>
	<td align="right">0.08</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	46.246.122.10		
	</td>
	<td align="right">2,206</td>
	<td align="right">3.42</td>
	<td align="right">17,853,104</td>
	<td align="right">0.60</td>
	<td align="right">7</td>
	<td align="right">0.25</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	185.136.156.82		
	</td>
	<td align="right">1,958</td>
	<td align="right">3.04</td>
	<td align="right">14,940,424</td>
	<td align="right">0.50</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	185.246.208.82		
	</td>
	<td align="right">1,948</td>
	<td align="right">3.02</td>
	<td align="right">14,925,441</td>
	<td align="right">0.50</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	185.180.12.65		
	</td>
	<td align="right">1,751</td>
	<td align="right">2.72</td>
	<td align="right">14,359,147</td>
	<td align="right">0.48</td>
	<td align="right">5</td>
	<td align="right">0.21</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	185.70.76.23		
	</td>
	<td align="right">1,675</td>
	<td align="right">2.60</td>
	<td align="right">13,642,143</td>
	<td align="right">0.46</td>
	<td align="right">4</td>
	<td align="right">0.16</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	82.103.139.165		
	</td>
	<td align="right">1,480</td>
	<td align="right">2.30</td>
	<td align="right">11,264,680</td>
	<td align="right">0.38</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	46.20.45.18		
	</td>
	<td align="right">1,469</td>
	<td align="right">2.28</td>
	<td align="right">11,131,413</td>
	<td align="right">0.37</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">10</th>
	<td>
	63.143.42.242		
	</td>
	<td align="right">1,372</td>
	<td align="right">2.13</td>
	<td align="right">29,647</td>
	<td align="right">0.00</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">11</th>
	<td>
	37.252.231.50		
	</td>
	<td align="right">1,354</td>
	<td align="right">2.10</td>
	<td align="right">11,463,650</td>
	<td align="right">0.38</td>
	<td align="right">3</td>
	<td align="right">0.12</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">12</th>
	<td>
	185.93.3.92		
	</td>
	<td align="right">1,296</td>
	<td align="right">2.01</td>
	<td align="right">10,440,941</td>
	<td align="right">0.35</td>
	<td align="right">4</td>
	<td align="right">0.17</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">13</th>
	<td>
	94.247.174.83		
	</td>
	<td align="right">1,205</td>
	<td align="right">1.87</td>
	<td align="right">10,078,109</td>
	<td align="right">0.34</td>
	<td align="right">2</td>
	<td align="right">0.08</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">14</th>
	<td>
	188.238.66.228		
	</td>
	<td align="right">1,173</td>
	<td align="right">1.82</td>
	<td align="right">153,074,964</td>
	<td align="right">5.14</td>
	<td align="right">5</td>
	<td align="right">0.21</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">15</th>
	<td>
	151.106.52.134		
	</td>
	<td align="right">1,008</td>
	<td align="right">1.56</td>
	<td align="right">9,142,560</td>
	<td align="right">0.31</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">16</th>
	<td>
	82.103.136.16		
	</td>
	<td align="right">1,008</td>
	<td align="right">1.56</td>
	<td align="right">9,142,560</td>
	<td align="right">0.31</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">17</th>
	<td>
	81.17.62.205		
	</td>
	<td align="right">1,008</td>
	<td align="right">1.56</td>
	<td align="right">9,142,560</td>
	<td align="right">0.31</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">18</th>
	<td>
	185.39.146.214		
	</td>
	<td align="right">1,008</td>
	<td align="right">1.56</td>
	<td align="right">9,142,560</td>
	<td align="right">0.31</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">19</th>
	<td>
	178.255.152.2		
	</td>
	<td align="right">968</td>
	<td align="right">1.50</td>
	<td align="right">7,410,860</td>
	<td align="right">0.25</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">20</th>
	<td>
	82.103.145.126		
	</td>
	<td align="right">908</td>
	<td align="right">1.41</td>
	<td align="right">7,690,137</td>
	<td align="right">0.26</td>
	<td align="right">2</td>
	<td align="right">0.08</td>
</tr>

</tbody>
</table>

