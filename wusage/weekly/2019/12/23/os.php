


<h2>Statistics for http://www.u5645470.fsdata.se, Week of 2019-12-23 to 2019-12-29</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 10 of 11 Operating Systems,
Sorted by Access Count

</h3>
<p></p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Product</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th class="hogerstalld">1</th>
	<td>
	Other		
	</td>
	<td align="right">36,657</td>
	<td align="right">56.98</td>
	<td align="right">421,106,409</td>
	<td align="right">14.22</td>
	<td align="right">720</td>
	<td align="right">25.90</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	Mac OS X		
	</td>
	<td align="right">24,548</td>
	<td align="right">38.15</td>
	<td align="right">2,363,653,551</td>
	<td align="right">79.81</td>
	<td align="right">1,497</td>
	<td align="right">53.82</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	Linux		
	</td>
	<td align="right">1,503</td>
	<td align="right">2.34</td>
	<td align="right">2,160,963</td>
	<td align="right">0.07</td>
	<td align="right">474</td>
	<td align="right">17.06</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	Windows NT		
	</td>
	<td align="right">1,350</td>
	<td align="right">2.10</td>
	<td align="right">157,209,285</td>
	<td align="right">5.31</td>
	<td align="right">83</td>
	<td align="right">3.01</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	Windows XP		
	</td>
	<td align="right">190</td>
	<td align="right">0.30</td>
	<td align="right">11,028,049</td>
	<td align="right">0.37</td>
	<td align="right">4</td>
	<td align="right">0.18</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	Windows Codename Longhorn		
	</td>
	<td align="right">54</td>
	<td align="right">0.08</td>
	<td align="right">5,554,562</td>
	<td align="right">0.19</td>
	<td align="right">1</td>
	<td align="right">0.04</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	Windows 2003		
	</td>
	<td align="right">21</td>
	<td align="right">0.03</td>
	<td align="right">327,036</td>
	<td align="right">0.01</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	Windows 2000		
	</td>
	<td align="right">7</td>
	<td align="right">0.01</td>
	<td align="right">232,365</td>
	<td align="right">0.01</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	Windows 95		
	</td>
	<td align="right">3</td>
	<td align="right">0.00</td>
	<td align="right">191,157</td>
	<td align="right">0.01</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">10</th>
	<td>
	Windows Me		
	</td>
	<td align="right">3</td>
	<td align="right">0.00</td>
	<td align="right">163,518</td>
	<td align="right">0.01</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>

</tbody>
</table>

