


<h2>Statistics for http://www.u5645470.fsdata.se, Week of 2019-12-2 to 2019-12-8</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Traffic by Hour</h3>
<p>Figures are averages for that hour
	of each day in the time period analyzed.</p>
<img src="h.png" width="468" height="351">
<br /><br />
<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Hour</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	<th scope='col'>Bits per Sec</th>
	<th scope='col'>Bytes per Sec</th>
	
	</tr>
</thead>
<tbody>
<tr class="jamnudda">
	<th align="right">00:00</th>
	<td align="right">357.29</td>
	<td align="right">2.77</td>
	<td align="right">10,827,403.57</td>
	<td align="right">1.56</td>
	<td align="right">11.06</td>
	<td align="right">1.77</td>
	<td align="right">24,060.90</td>
	<td align="right">3,007.61</td>
	
</tr>
<tr class="jamn">
	<th align="right">01:00</th>
	<td align="right">308.71</td>
	<td align="right">2.39</td>
	<td align="right">8,170,981.57</td>
	<td align="right">1.18</td>
	<td align="right">11.95</td>
	<td align="right">1.91</td>
	<td align="right">18,157.74</td>
	<td align="right">2,269.72</td>
	
</tr>
<tr class="udda">
	<th align="right">02:00</th>
	<td align="right">322.57</td>
	<td align="right">2.50</td>
	<td align="right">6,545,393.57</td>
	<td align="right">0.94</td>
	<td align="right">13.41</td>
	<td align="right">2.14</td>
	<td align="right">14,545.32</td>
	<td align="right">1,818.16</td>
	
</tr>
<tr class="jamn">
	<th align="right">03:00</th>
	<td align="right">287.14</td>
	<td align="right">2.22</td>
	<td align="right">6,514,181.29</td>
	<td align="right">0.94</td>
	<td align="right">16.27</td>
	<td align="right">2.60</td>
	<td align="right">14,475.96</td>
	<td align="right">1,809.49</td>
	
</tr>
<tr class="udda">
	<th align="right">04:00</th>
	<td align="right">278.14</td>
	<td align="right">2.15</td>
	<td align="right">5,057,061.00</td>
	<td align="right">0.73</td>
	<td align="right">10.73</td>
	<td align="right">1.71</td>
	<td align="right">11,237.91</td>
	<td align="right">1,404.74</td>
	
</tr>
<tr class="jamn">
	<th align="right">05:00</th>
	<td align="right">300.43</td>
	<td align="right">2.33</td>
	<td align="right">6,794,507.43</td>
	<td align="right">0.98</td>
	<td align="right">12.19</td>
	<td align="right">1.95</td>
	<td align="right">15,098.91</td>
	<td align="right">1,887.36</td>
	
</tr>
<tr class="udda">
	<th align="right">06:00</th>
	<td align="right">315.57</td>
	<td align="right">2.44</td>
	<td align="right">11,263,589.43</td>
	<td align="right">1.62</td>
	<td align="right">12.32</td>
	<td align="right">1.97</td>
	<td align="right">25,030.20</td>
	<td align="right">3,128.77</td>
	
</tr>
<tr class="jamn">
	<th align="right">07:00</th>
	<td align="right">343.86</td>
	<td align="right">2.66</td>
	<td align="right">9,020,747.29</td>
	<td align="right">1.30</td>
	<td align="right">16.27</td>
	<td align="right">2.60</td>
	<td align="right">20,046.11</td>
	<td align="right">2,505.76</td>
	
</tr>
<tr class="udda">
	<th align="right">08:00</th>
	<td align="right">546.57</td>
	<td align="right">4.23</td>
	<td align="right">28,375,057.57</td>
	<td align="right">4.08</td>
	<td align="right">19.53</td>
	<td align="right">3.12</td>
	<td align="right">63,055.68</td>
	<td align="right">7,881.96</td>
	
</tr>
<tr class="jamn">
	<th align="right">09:00</th>
	<td align="right">678.86</td>
	<td align="right">5.26</td>
	<td align="right">47,109,919.43</td>
	<td align="right">6.78</td>
	<td align="right">32.50</td>
	<td align="right">5.19</td>
	<td align="right">104,688.71</td>
	<td align="right">13,086.09</td>
	
</tr>
<tr class="udda">
	<th align="right">10:00</th>
	<td align="right">714.43</td>
	<td align="right">5.53</td>
	<td align="right">49,639,583.00</td>
	<td align="right">7.14</td>
	<td align="right">34.12</td>
	<td align="right">5.45</td>
	<td align="right">110,310.18</td>
	<td align="right">13,788.77</td>
	
</tr>
<tr class="jamn">
	<th align="right">11:00</th>
	<td align="right">702.57</td>
	<td align="right">5.44</td>
	<td align="right">43,165,208.71</td>
	<td align="right">6.21</td>
	<td align="right">33.10</td>
	<td align="right">5.29</td>
	<td align="right">95,922.69</td>
	<td align="right">11,990.34</td>
	
</tr>
<tr class="udda">
	<th align="right">12:00</th>
	<td align="right">603.57</td>
	<td align="right">4.67</td>
	<td align="right">35,985,431.29</td>
	<td align="right">5.18</td>
	<td align="right">24.24</td>
	<td align="right">3.87</td>
	<td align="right">79,967.63</td>
	<td align="right">9,995.95</td>
	
</tr>
<tr class="jamn">
	<th align="right">13:00</th>
	<td align="right">792.14</td>
	<td align="right">6.13</td>
	<td align="right">50,347,964.43</td>
	<td align="right">7.24</td>
	<td align="right">31.30</td>
	<td align="right">5.00</td>
	<td align="right">111,884.37</td>
	<td align="right">13,985.55</td>
	
</tr>
<tr class="udda">
	<th align="right">14:00</th>
	<td align="right">735.14</td>
	<td align="right">5.69</td>
	<td align="right">51,133,090.29</td>
	<td align="right">7.36</td>
	<td align="right">38.48</td>
	<td align="right">6.15</td>
	<td align="right">113,629.09</td>
	<td align="right">14,203.64</td>
	
</tr>
<tr class="jamn">
	<th align="right">15:00</th>
	<td align="right">633.57</td>
	<td align="right">4.90</td>
	<td align="right">32,834,293.71</td>
	<td align="right">4.72</td>
	<td align="right">33.31</td>
	<td align="right">5.32</td>
	<td align="right">72,965.10</td>
	<td align="right">9,120.64</td>
	
</tr>
<tr class="udda">
	<th align="right">16:00</th>
	<td align="right">691.71</td>
	<td align="right">5.36</td>
	<td align="right">42,878,034.57</td>
	<td align="right">6.17</td>
	<td align="right">29.54</td>
	<td align="right">4.72</td>
	<td align="right">95,284.52</td>
	<td align="right">11,910.57</td>
	
</tr>
<tr class="jamn">
	<th align="right">17:00</th>
	<td align="right">718.86</td>
	<td align="right">5.57</td>
	<td align="right">43,732,033.71</td>
	<td align="right">6.29</td>
	<td align="right">41.17</td>
	<td align="right">6.58</td>
	<td align="right">97,182.30</td>
	<td align="right">12,147.79</td>
	
</tr>
<tr class="udda">
	<th align="right">18:00</th>
	<td align="right">618.71</td>
	<td align="right">4.79</td>
	<td align="right">34,189,578.43</td>
	<td align="right">4.92</td>
	<td align="right">32.11</td>
	<td align="right">5.13</td>
	<td align="right">75,976.84</td>
	<td align="right">9,497.11</td>
	
</tr>
<tr class="jamn">
	<th align="right">19:00</th>
	<td align="right">665.43</td>
	<td align="right">5.15</td>
	<td align="right">38,687,226.14</td>
	<td align="right">5.57</td>
	<td align="right">33.03</td>
	<td align="right">5.28</td>
	<td align="right">85,971.61</td>
	<td align="right">10,746.45</td>
	
</tr>
<tr class="udda">
	<th align="right">20:00</th>
	<td align="right">618.57</td>
	<td align="right">4.79</td>
	<td align="right">36,363,775.00</td>
	<td align="right">5.23</td>
	<td align="right">33.85</td>
	<td align="right">5.41</td>
	<td align="right">80,808.39</td>
	<td align="right">10,101.05</td>
	
</tr>
<tr class="jamn">
	<th align="right">21:00</th>
	<td align="right">651.86</td>
	<td align="right">5.05</td>
	<td align="right">38,821,372.43</td>
	<td align="right">5.59</td>
	<td align="right">34.96</td>
	<td align="right">5.59</td>
	<td align="right">86,269.72</td>
	<td align="right">10,783.71</td>
	
</tr>
<tr class="udda">
	<th align="right">22:00</th>
	<td align="right">552.14</td>
	<td align="right">4.27</td>
	<td align="right">30,866,681.14</td>
	<td align="right">4.44</td>
	<td align="right">31.45</td>
	<td align="right">5.03</td>
	<td align="right">68,592.62</td>
	<td align="right">8,574.08</td>
	
</tr>
<tr class="jamn">
	<th align="right">23:00</th>
	<td align="right">479.00</td>
	<td align="right">3.71</td>
	<td align="right">26,765,126.86</td>
	<td align="right">3.85</td>
	<td align="right">38.83</td>
	<td align="right">6.21</td>
	<td align="right">59,478.06</td>
	<td align="right">7,434.76</td>
	
</tr>

</tbody>
</table>

