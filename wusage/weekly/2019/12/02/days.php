


<h2>Statistics for http://www.u5645470.fsdata.se, Week of 2019-12-2 to 2019-12-8</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Traffic by Day</h3>
<img src="d.png" width="468" height="351">
<br /><br />


<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Date</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	<th scope='col'>Bits per Sec</th>
	<th scope='col'>Bytes per Sec</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th align="right">2019-12-2</th>
	<td align="right">13,094	</td>
	<td align="right">14.48	</td>
	<td align="right">742,293,112	</td>
	<td align="right">15.26	</td>
	<td align="right">606	</td>
	<td align="right">13.84	</td>
	<td align="right">68,730.84	</td>
	<td align="right">8,591.36	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-12-3</th>
	<td align="right">16,907	</td>
	<td align="right">18.70	</td>
	<td align="right">1,026,304,577	</td>
	<td align="right">21.09	</td>
	<td align="right">875	</td>
	<td align="right">19.98	</td>
	<td align="right">95,028.20	</td>
	<td align="right">11,878.53	</td>
</tr>
<tr class="udda">
	<th align="right">2019-12-4</th>
	<td align="right">15,136	</td>
	<td align="right">16.74	</td>
	<td align="right">878,668,308	</td>
	<td align="right">18.06	</td>
	<td align="right">736	</td>
	<td align="right">16.80	</td>
	<td align="right">81,358.18	</td>
	<td align="right">10,169.77	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-12-5</th>
	<td align="right">12,732	</td>
	<td align="right">14.08	</td>
	<td align="right">613,430,839	</td>
	<td align="right">12.61	</td>
	<td align="right">566	</td>
	<td align="right">12.92	</td>
	<td align="right">56,799.15	</td>
	<td align="right">7,099.89	</td>
</tr>
<tr class="udda">
	<th align="right">2019-12-6</th>
	<td align="right">11,260	</td>
	<td align="right">12.45	</td>
	<td align="right">600,116,642	</td>
	<td align="right">12.33	</td>
	<td align="right">471	</td>
	<td align="right">10.75	</td>
	<td align="right">55,566.36	</td>
	<td align="right">6,945.79	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-12-7</th>
	<td align="right">10,690	</td>
	<td align="right">11.82	</td>
	<td align="right">529,728,609	</td>
	<td align="right">10.89	</td>
	<td align="right">601	</td>
	<td align="right">13.72	</td>
	<td align="right">49,048.95	</td>
	<td align="right">6,131.12	</td>
</tr>
<tr class="udda">
	<th align="right">2019-12-8</th>
	<td align="right">10,599	</td>
	<td align="right">11.72	</td>
	<td align="right">475,075,606	</td>
	<td align="right">9.76	</td>
	<td align="right">525	</td>
	<td align="right">11.99	</td>
	<td align="right">43,988.48	</td>
	<td align="right">5,498.56	</td>
</tr>

</tbody>
</table>

