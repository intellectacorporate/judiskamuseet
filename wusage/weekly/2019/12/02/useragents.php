


<h2>Statistics for http://www.u5645470.fsdata.se, Week of 2019-12-2 to 2019-12-8</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 10 of 211 Browsers (User Agents),
Sorted by Access Count

</h3>
<p></p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Product</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th class="hogerstalld">1</th>
	<td>
	Pingdom.com_bot_version_1.4_		
	</td>
	<td align="right">29,396</td>
	<td align="right">32.53</td>
	<td align="right">246,556,554</td>
	<td align="right">5.07</td>
	<td align="right">23</td>
	<td align="right">0.54</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	Safari/537.36		
	</td>
	<td align="right">22,561</td>
	<td align="right">24.96</td>
	<td align="right">1,877,430,594</td>
	<td align="right">38.62</td>
	<td align="right">1,251</td>
	<td align="right">28.60</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	Safari/604.1		
	</td>
	<td align="right">11,367</td>
	<td align="right">12.58</td>
	<td align="right">995,328,845</td>
	<td align="right">20.48</td>
	<td align="right">682</td>
	<td align="right">15.59</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	Mozilla/5.0 		
	</td>
	<td align="right">7,144</td>
	<td align="right">7.90</td>
	<td align="right">387,659,914</td>
	<td align="right">7.97</td>
	<td align="right">948</td>
	<td align="right">21.66</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	Safari/605.1.15		
	</td>
	<td align="right">4,419</td>
	<td align="right">4.89</td>
	<td align="right">388,566,501</td>
	<td align="right">7.99</td>
	<td align="right">257</td>
	<td align="right">5.89</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	curl/7.54.0		
	</td>
	<td align="right">1,922</td>
	<td align="right">2.13</td>
	<td align="right">20,237,442</td>
	<td align="right">0.42</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	UptimeRobot/2.0		
	</td>
	<td align="right">1,310</td>
	<td align="right">1.45</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	Safari/605.1		
	</td>
	<td align="right">1,284</td>
	<td align="right">1.42</td>
	<td align="right">123,779,611</td>
	<td align="right">2.55</td>
	<td align="right">63</td>
	<td align="right">1.46</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	Safari/537.36 (compatible; Googlebot/2.1;  http://www.google.com/bot.html)		
	</td>
	<td align="right">1,182</td>
	<td align="right">1.31</td>
	<td align="right">12,647,018</td>
	<td align="right">0.26</td>
	<td align="right">222</td>
	<td align="right">5.09</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">10</th>
	<td>
	bingbot/2.0		
	</td>
	<td align="right">1,007</td>
	<td align="right">1.11</td>
	<td align="right">77,247,800</td>
	<td align="right">1.59</td>
	<td align="right">211</td>
	<td align="right">4.84</td>
</tr>

</tbody>
</table>

