


<h2>Statistics for http://www.u5645470.fsdata.se, Week of 2019-12-2 to 2019-12-8</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 10 of 14 Operating Systems,
Sorted by Access Count

</h3>
<p></p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Product</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th class="hogerstalld">1</th>
	<td>
	Mac OS X		
	</td>
	<td align="right">46,145</td>
	<td align="right">51.06</td>
	<td align="right">3,861,489,833</td>
	<td align="right">79.44</td>
	<td align="right">2,753</td>
	<td align="right">62.87</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	Other		
	</td>
	<td align="right">38,042</td>
	<td align="right">42.09</td>
	<td align="right">720,991,531</td>
	<td align="right">14.83</td>
	<td align="right">732</td>
	<td align="right">16.72</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	Linux		
	</td>
	<td align="right">3,424</td>
	<td align="right">3.79</td>
	<td align="right">38,915,306</td>
	<td align="right">0.80</td>
	<td align="right">738</td>
	<td align="right">16.86</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	Windows NT		
	</td>
	<td align="right">2,546</td>
	<td align="right">2.82</td>
	<td align="right">230,147,246</td>
	<td align="right">4.73</td>
	<td align="right">148</td>
	<td align="right">3.39</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	Windows XP		
	</td>
	<td align="right">132</td>
	<td align="right">0.15</td>
	<td align="right">6,860,439</td>
	<td align="right">0.14</td>
	<td align="right">4</td>
	<td align="right">0.10</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	Windows 2003		
	</td>
	<td align="right">30</td>
	<td align="right">0.03</td>
	<td align="right">587,592</td>
	<td align="right">0.01</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	Windows Codename Longhorn		
	</td>
	<td align="right">18</td>
	<td align="right">0.02</td>
	<td align="right">35,646</td>
	<td align="right">0.00</td>
	<td align="right">2</td>
	<td align="right">0.05</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	Windows 2000		
	</td>
	<td align="right">16</td>
	<td align="right">0.02</td>
	<td align="right">870,336</td>
	<td align="right">0.02</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	Windows 95		
	</td>
	<td align="right">12</td>
	<td align="right">0.01</td>
	<td align="right">783,456</td>
	<td align="right">0.02</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">10</th>
	<td>
	Windows 98		
	</td>
	<td align="right">3</td>
	<td align="right">0.00</td>
	<td align="right">195,864</td>
	<td align="right">0.00</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>

</tbody>
</table>

