


<h2>Statistics for http://www.u5645470.fsdata.se, Week of 2019-12-2 to 2019-12-8</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 20 of 4,315 Visitor Sites,
Sorted by Access Count

</h3>
<p></p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Site</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="jamnudda">
	<th class="hogerstalld">1</th>
	<td>
	46.246.122.10		
	</td>
	<td align="right">2,952</td>
	<td align="right">3.26</td>
	<td align="right">24,657,045</td>
	<td align="right">0.51</td>
	<td align="right">7</td>
	<td align="right">0.18</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	185.93.3.92		
	</td>
	<td align="right">2,936</td>
	<td align="right">3.25</td>
	<td align="right">24,665,779</td>
	<td align="right">0.51</td>
	<td align="right">7</td>
	<td align="right">0.18</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	94.247.174.83		
	</td>
	<td align="right">2,935</td>
	<td align="right">3.25</td>
	<td align="right">24,602,851</td>
	<td align="right">0.51</td>
	<td align="right">7</td>
	<td align="right">0.18</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	82.103.139.165		
	</td>
	<td align="right">1,949</td>
	<td align="right">2.16</td>
	<td align="right">15,539,404</td>
	<td align="right">0.32</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	85.195.116.134		
	</td>
	<td align="right">1,943</td>
	<td align="right">2.15</td>
	<td align="right">15,524,056</td>
	<td align="right">0.32</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	82.103.145.126		
	</td>
	<td align="right">1,942</td>
	<td align="right">2.15</td>
	<td align="right">15,524,038</td>
	<td align="right">0.32</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	185.39.146.215		
	</td>
	<td align="right">1,926</td>
	<td align="right">2.13</td>
	<td align="right">15,523,728</td>
	<td align="right">0.32</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	94.75.211.74		
	</td>
	<td align="right">1,924</td>
	<td align="right">2.13</td>
	<td align="right">15,523,666</td>
	<td align="right">0.32</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	185.70.76.23		
	</td>
	<td align="right">1,923</td>
	<td align="right">2.13</td>
	<td align="right">15,493,200</td>
	<td align="right">0.32</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">10</th>
	<td>
	185.136.156.82		
	</td>
	<td align="right">1,917</td>
	<td align="right">2.12</td>
	<td align="right">15,523,012</td>
	<td align="right">0.32</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">11</th>
	<td>
	46.59.25.88		
	</td>
	<td align="right">1,490</td>
	<td align="right">1.65</td>
	<td align="right">108,709,179</td>
	<td align="right">2.23</td>
	<td align="right">34</td>
	<td align="right">0.79</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">12</th>
	<td>
	63.143.42.242		
	</td>
	<td align="right">1,311</td>
	<td align="right">1.45</td>
	<td align="right">1</td>
	<td align="right">0.00</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">13</th>
	<td>
	185.180.12.65		
	</td>
	<td align="right">1,008</td>
	<td align="right">1.11</td>
	<td align="right">9,142,560</td>
	<td align="right">0.19</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">14</th>
	<td>
	185.39.146.214		
	</td>
	<td align="right">1,008</td>
	<td align="right">1.11</td>
	<td align="right">9,142,560</td>
	<td align="right">0.19</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">15</th>
	<td>
	151.106.52.134		
	</td>
	<td align="right">1,008</td>
	<td align="right">1.11</td>
	<td align="right">9,142,560</td>
	<td align="right">0.19</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">16</th>
	<td>
	82.103.136.16		
	</td>
	<td align="right">1,008</td>
	<td align="right">1.11</td>
	<td align="right">9,142,560</td>
	<td align="right">0.19</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">17</th>
	<td>
	94.75.211.73		
	</td>
	<td align="right">1,008</td>
	<td align="right">1.11</td>
	<td align="right">9,142,560</td>
	<td align="right">0.19</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">18</th>
	<td>
	89.163.242.206		
	</td>
	<td align="right">1,007</td>
	<td align="right">1.11</td>
	<td align="right">9,133,490</td>
	<td align="right">0.19</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">19</th>
	<td>
	213.66.107.27		
	</td>
	<td align="right">801</td>
	<td align="right">0.89</td>
	<td align="right">34,112,364</td>
	<td align="right">0.70</td>
	<td align="right">22</td>
	<td align="right">0.51</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">20</th>
	<td>
	91.236.116.14		
	</td>
	<td align="right">683</td>
	<td align="right">0.76</td>
	<td align="right">7,146,990</td>
	<td align="right">0.15</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>

</tbody>
</table>

