


<h2>Statistics for http://www.u5645470.fsdata.se, Week of 2019-12-2 to 2019-12-8</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 20 of 199 Referring Sites,
Sorted by Access Count

</h3>
<p></p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Referring Site</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th class="hogerstalld">1</th>
	<td>
	<a href="https://judiskamuseet.se">
	https://judiskamuseet.se</a>
	</td>
	<td align="right">40,411</td>
	<td align="right">90.19</td>
	<td align="right">3,949,829,959</td>
	<td align="right">97.31</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	<a href="http://judiskamuseet.se">
	http://judiskamuseet.se</a>
	</td>
	<td align="right">1,904</td>
	<td align="right">4.25</td>
	<td align="right">18,255,877</td>
	<td align="right">0.45</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	<a href="https://www.google.com">
	https://www.google.com</a>
	</td>
	<td align="right">1,229</td>
	<td align="right">2.74</td>
	<td align="right">52,013,324</td>
	<td align="right">1.28</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	<a href="https://www.google.se">
	https://www.google.se</a>
	</td>
	<td align="right">508</td>
	<td align="right">1.13</td>
	<td align="right">20,244,407</td>
	<td align="right">0.50</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	<a href="https://www.google.de">
	https://www.google.de</a>
	</td>
	<td align="right">114</td>
	<td align="right">0.25</td>
	<td align="right">1,742,001</td>
	<td align="right">0.04</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	<a href="http://www.google.com">
	http://www.google.com</a>
	</td>
	<td align="right">53</td>
	<td align="right">0.12</td>
	<td align="right">444,938</td>
	<td align="right">0.01</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	<a href="http://m.facebook.com">
	http://m.facebook.com</a>
	</td>
	<td align="right">33</td>
	<td align="right">0.07</td>
	<td align="right">403,169</td>
	<td align="right">0.01</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	<a href="https://www.bing.com">
	https://www.bing.com</a>
	</td>
	<td align="right">30</td>
	<td align="right">0.07</td>
	<td align="right">930,678</td>
	<td align="right">0.02</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	<a href="http://blogg.slaktingar.se">
	http://blogg.slaktingar.se</a>
	</td>
	<td align="right">25</td>
	<td align="right">0.06</td>
	<td align="right">167,959</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">10</th>
	<td>
	<a href="https://classroom.google.com">
	https://classroom.google.com</a>
	</td>
	<td align="right">20</td>
	<td align="right">0.04</td>
	<td align="right">201,208</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">11</th>
	<td>
	<a href="https://l.facebook.com">
	https://l.facebook.com</a>
	</td>
	<td align="right">10</td>
	<td align="right">0.02</td>
	<td align="right">62,692</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">12</th>
	<td>
	<a href="https://www.google.nl">
	https://www.google.nl</a>
	</td>
	<td align="right">10</td>
	<td align="right">0.02</td>
	<td align="right">103,156</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">13</th>
	<td>
	<a href="http://www.judiska-museet.se">
	http://www.judiska-museet.se</a>
	</td>
	<td align="right">10</td>
	<td align="right">0.02</td>
	<td align="right">12,213</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">14</th>
	<td>
	<a href="https://myactivity.google.com">
	https://myactivity.google.com</a>
	</td>
	<td align="right">10</td>
	<td align="right">0.02</td>
	<td align="right">3,054,964</td>
	<td align="right">0.08</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">15</th>
	<td>
	<a href="https://zvuqa.net">
	https://zvuqa.net</a>
	</td>
	<td align="right">9</td>
	<td align="right">0.02</td>
	<td align="right">587,592</td>
	<td align="right">0.01</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">16</th>
	<td>
	<a href="https://mailchi.mp">
	https://mailchi.mp</a>
	</td>
	<td align="right">9</td>
	<td align="right">0.02</td>
	<td align="right">120,936</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">17</th>
	<td>
	<a href="http://www.stockholmmuseums.se">
	http://www.stockholmmuseums.se</a>
	</td>
	<td align="right">8</td>
	<td align="right">0.02</td>
	<td align="right">61,607</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">18</th>
	<td>
	<a href="https://www.welma.se">
	https://www.welma.se</a>
	</td>
	<td align="right">7</td>
	<td align="right">0.02</td>
	<td align="right">61,602</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">19</th>
	<td>
	<a href="http://pacificair.com">
	http://pacificair.com</a>
	</td>
	<td align="right">7</td>
	<td align="right">0.02</td>
	<td align="right">274,514</td>
	<td align="right">0.01</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">20</th>
	<td>
	<a href="https://89.221.250.27:443">
	https://89.221.250.27:443</a>
	</td>
	<td align="right">7</td>
	<td align="right">0.02</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>

</tbody>
</table>

