


<h2>Statistics for http://www.u5645470.fsdata.se, Week of 2019-12-9 to 2019-12-15</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Traffic by Day</h3>
<img src="d.png" width="468" height="351">
<br /><br />


<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Date</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	<th scope='col'>Bits per Sec</th>
	<th scope='col'>Bytes per Sec</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th align="right">2019-12-9</th>
	<td align="right">13,005	</td>
	<td align="right">16.06	</td>
	<td align="right">729,721,752	</td>
	<td align="right">16.06	</td>
	<td align="right">607	</td>
	<td align="right">17.06	</td>
	<td align="right">67,566.83	</td>
	<td align="right">8,445.85	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-12-10</th>
	<td align="right">12,064	</td>
	<td align="right">14.90	</td>
	<td align="right">638,643,300	</td>
	<td align="right">14.06	</td>
	<td align="right">528	</td>
	<td align="right">14.84	</td>
	<td align="right">59,133.64	</td>
	<td align="right">7,391.70	</td>
</tr>
<tr class="udda">
	<th align="right">2019-12-11</th>
	<td align="right">12,601	</td>
	<td align="right">15.56	</td>
	<td align="right">684,374,619	</td>
	<td align="right">15.06	</td>
	<td align="right">543	</td>
	<td align="right">15.27	</td>
	<td align="right">63,368.02	</td>
	<td align="right">7,921.00	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-12-12</th>
	<td align="right">12,812	</td>
	<td align="right">15.82	</td>
	<td align="right">807,342,712	</td>
	<td align="right">17.77	</td>
	<td align="right">566	</td>
	<td align="right">15.91	</td>
	<td align="right">74,753.95	</td>
	<td align="right">9,344.24	</td>
</tr>
<tr class="udda">
	<th align="right">2019-12-13</th>
	<td align="right">11,482	</td>
	<td align="right">14.18	</td>
	<td align="right">671,671,359	</td>
	<td align="right">14.78	</td>
	<td align="right">494	</td>
	<td align="right">13.89	</td>
	<td align="right">62,191.79	</td>
	<td align="right">7,773.97	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-12-14</th>
	<td align="right">8,781	</td>
	<td align="right">10.85	</td>
	<td align="right">468,114,776	</td>
	<td align="right">10.30	</td>
	<td align="right">369	</td>
	<td align="right">10.37	</td>
	<td align="right">43,343.96	</td>
	<td align="right">5,418.00	</td>
</tr>
<tr class="udda">
	<th align="right">2019-12-15</th>
	<td align="right">10,223	</td>
	<td align="right">12.63	</td>
	<td align="right">543,799,205	</td>
	<td align="right">11.97	</td>
	<td align="right">450	</td>
	<td align="right">12.65	</td>
	<td align="right">50,351.78	</td>
	<td align="right">6,293.97	</td>
</tr>

</tbody>
</table>

