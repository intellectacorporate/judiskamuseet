


<h2>Statistics for http://www.u5645470.fsdata.se, Week of 2019-12-9 to 2019-12-15</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 10 of 12 Operating Systems,
Sorted by Access Count

</h3>
<p></p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Product</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th class="hogerstalld">1</th>
	<td>
	Mac OS X		
	</td>
	<td align="right">38,600</td>
	<td align="right">47.78</td>
	<td align="right">3,630,036,299</td>
	<td align="right">80.05</td>
	<td align="right">2,151</td>
	<td align="right">60.49</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	Other		
	</td>
	<td align="right">37,831</td>
	<td align="right">46.82</td>
	<td align="right">620,231,501</td>
	<td align="right">13.68</td>
	<td align="right">713</td>
	<td align="right">20.06</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	Linux		
	</td>
	<td align="right">2,117</td>
	<td align="right">2.62</td>
	<td align="right">63,482,762</td>
	<td align="right">1.40</td>
	<td align="right">595</td>
	<td align="right">16.73</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	Windows NT		
	</td>
	<td align="right">2,004</td>
	<td align="right">2.48</td>
	<td align="right">205,467,747</td>
	<td align="right">4.53</td>
	<td align="right">94</td>
	<td align="right">2.66</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	Windows XP		
	</td>
	<td align="right">119</td>
	<td align="right">0.15</td>
	<td align="right">6,650,534</td>
	<td align="right">0.15</td>
	<td align="right">1</td>
	<td align="right">0.03</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	Windows Codename Longhorn		
	</td>
	<td align="right">50</td>
	<td align="right">0.06</td>
	<td align="right">5,451,704</td>
	<td align="right">0.12</td>
	<td align="right">1</td>
	<td align="right">0.03</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	Windows 2003		
	</td>
	<td align="right">35</td>
	<td align="right">0.04</td>
	<td align="right">1,010,088</td>
	<td align="right">0.02</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	Windows 2000		
	</td>
	<td align="right">25</td>
	<td align="right">0.03</td>
	<td align="right">1,451,253</td>
	<td align="right">0.03</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	Windows 95		
	</td>
	<td align="right">6</td>
	<td align="right">0.01</td>
	<td align="right">325,914</td>
	<td align="right">0.01</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">10</th>
	<td>
	Mac OS		
	</td>
	<td align="right">3</td>
	<td align="right">0.00</td>
	<td align="right">6,234</td>
	<td align="right">0.00</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>

</tbody>
</table>

