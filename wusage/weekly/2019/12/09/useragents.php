


<h2>Statistics for http://www.u5645470.fsdata.se, Week of 2019-12-9 to 2019-12-15</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 10 of 204 Browsers (User Agents),
Sorted by Access Count

</h3>
<p></p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Product</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th class="hogerstalld">1</th>
	<td>
	Pingdom.com_bot_version_1.4_		
	</td>
	<td align="right">29,569</td>
	<td align="right">36.60</td>
	<td align="right">246,023,108</td>
	<td align="right">5.43</td>
	<td align="right">24</td>
	<td align="right">0.69</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	Safari/537.36		
	</td>
	<td align="right">18,802</td>
	<td align="right">23.27</td>
	<td align="right">1,686,449,952</td>
	<td align="right">37.19</td>
	<td align="right">974</td>
	<td align="right">27.44</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	Safari/604.1		
	</td>
	<td align="right">10,278</td>
	<td align="right">12.72</td>
	<td align="right">1,106,146,683</td>
	<td align="right">24.39</td>
	<td align="right">563</td>
	<td align="right">15.86</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	Mozilla/5.0 		
	</td>
	<td align="right">5,080</td>
	<td align="right">6.29</td>
	<td align="right">359,863,666</td>
	<td align="right">7.94</td>
	<td align="right">725</td>
	<td align="right">20.42</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	Safari/605.1.15		
	</td>
	<td align="right">4,138</td>
	<td align="right">5.12</td>
	<td align="right">386,453,317</td>
	<td align="right">8.52</td>
	<td align="right">234</td>
	<td align="right">6.61</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	curl/7.54.0		
	</td>
	<td align="right">1,478</td>
	<td align="right">1.83</td>
	<td align="right">15,463,009</td>
	<td align="right">0.34</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	UptimeRobot/2.0		
	</td>
	<td align="right">1,392</td>
	<td align="right">1.72</td>
	<td align="right">102,400</td>
	<td align="right">0.00</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	bingbot/2.0		
	</td>
	<td align="right">1,025</td>
	<td align="right">1.27</td>
	<td align="right">79,587,272</td>
	<td align="right">1.76</td>
	<td align="right">227</td>
	<td align="right">6.39</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	Safari/537.36 Edge/18.18362		
	</td>
	<td align="right">838</td>
	<td align="right">1.04</td>
	<td align="right">125,004,271</td>
	<td align="right">2.76</td>
	<td align="right">37</td>
	<td align="right">1.06</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">10</th>
	<td>
	Safari/605.1		
	</td>
	<td align="right">597</td>
	<td align="right">0.74</td>
	<td align="right">54,172,368</td>
	<td align="right">1.19</td>
	<td align="right">33</td>
	<td align="right">0.94</td>
</tr>

</tbody>
</table>

