


<h2>Statistics for http://www.u5645470.fsdata.se, Week of 2019-12-9 to 2019-12-15</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 20 of 3,487 Visitor Sites,
Sorted by Access Count

</h3>
<p></p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Site</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="jamnudda">
	<th class="hogerstalld">1</th>
	<td>
	46.246.122.10		
	</td>
	<td align="right">2,975</td>
	<td align="right">3.67</td>
	<td align="right">24,626,673</td>
	<td align="right">0.54</td>
	<td align="right">7</td>
	<td align="right">0.22</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	185.93.3.92		
	</td>
	<td align="right">2,966</td>
	<td align="right">3.66</td>
	<td align="right">24,677,938</td>
	<td align="right">0.54</td>
	<td align="right">7</td>
	<td align="right">0.22</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	94.247.174.83		
	</td>
	<td align="right">2,143</td>
	<td align="right">2.65</td>
	<td align="right">17,809,309</td>
	<td align="right">0.39</td>
	<td align="right">6</td>
	<td align="right">0.19</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	82.103.145.126		
	</td>
	<td align="right">1,946</td>
	<td align="right">2.40</td>
	<td align="right">15,404,506</td>
	<td align="right">0.34</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	185.136.156.82		
	</td>
	<td align="right">1,941</td>
	<td align="right">2.40</td>
	<td align="right">15,544,042</td>
	<td align="right">0.34</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	82.103.139.165		
	</td>
	<td align="right">1,941</td>
	<td align="right">2.40</td>
	<td align="right">15,497,517</td>
	<td align="right">0.34</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	94.75.211.74		
	</td>
	<td align="right">1,918</td>
	<td align="right">2.37</td>
	<td align="right">15,218,015</td>
	<td align="right">0.33</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	46.59.25.88		
	</td>
	<td align="right">1,856</td>
	<td align="right">2.29</td>
	<td align="right">131,283,127</td>
	<td align="right">2.89</td>
	<td align="right">47</td>
	<td align="right">1.33</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	37.252.231.50		
	</td>
	<td align="right">1,440</td>
	<td align="right">1.78</td>
	<td align="right">11,806,920</td>
	<td align="right">0.26</td>
	<td align="right">2</td>
	<td align="right">0.06</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">10</th>
	<td>
	185.39.146.215		
	</td>
	<td align="right">1,429</td>
	<td align="right">1.76</td>
	<td align="right">11,240,051</td>
	<td align="right">0.25</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">11</th>
	<td>
	63.143.42.242		
	</td>
	<td align="right">1,389</td>
	<td align="right">1.72</td>
	<td align="right">15,540</td>
	<td align="right">0.00</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">12</th>
	<td>
	46.20.45.18		
	</td>
	<td align="right">1,219</td>
	<td align="right">1.51</td>
	<td align="right">9,681,844</td>
	<td align="right">0.21</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">13</th>
	<td>
	94.75.211.73		
	</td>
	<td align="right">1,007</td>
	<td align="right">1.24</td>
	<td align="right">9,133,490</td>
	<td align="right">0.20</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">14</th>
	<td>
	151.106.52.134		
	</td>
	<td align="right">1,007</td>
	<td align="right">1.24</td>
	<td align="right">9,133,490</td>
	<td align="right">0.20</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">15</th>
	<td>
	89.163.242.206		
	</td>
	<td align="right">1,007</td>
	<td align="right">1.24</td>
	<td align="right">9,133,490</td>
	<td align="right">0.20</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">16</th>
	<td>
	185.180.12.65		
	</td>
	<td align="right">1,001</td>
	<td align="right">1.24</td>
	<td align="right">9,079,070</td>
	<td align="right">0.20</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">17</th>
	<td>
	82.103.136.16		
	</td>
	<td align="right">997</td>
	<td align="right">1.23</td>
	<td align="right">9,042,790</td>
	<td align="right">0.20</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">18</th>
	<td>
	85.195.116.134		
	</td>
	<td align="right">868</td>
	<td align="right">1.07</td>
	<td align="right">6,838,750</td>
	<td align="right">0.15</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">19</th>
	<td>
	178.255.153.2		
	</td>
	<td align="right">763</td>
	<td align="right">0.94</td>
	<td align="right">6,920,410</td>
	<td align="right">0.15</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">20</th>
	<td>
	185.70.76.23		
	</td>
	<td align="right">745</td>
	<td align="right">0.92</td>
	<td align="right">5,877,542</td>
	<td align="right">0.13</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>

</tbody>
</table>

