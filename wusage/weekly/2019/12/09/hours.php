


<h2>Statistics for http://www.u5645470.fsdata.se, Week of 2019-12-9 to 2019-12-15</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Traffic by Hour</h3>
<p>Figures are averages for that hour
	of each day in the time period analyzed.</p>
<img src="h.png" width="468" height="351">
<br /><br />
<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Hour</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	<th scope='col'>Bits per Sec</th>
	<th scope='col'>Bytes per Sec</th>
	
	</tr>
</thead>
<tbody>
<tr class="jamnudda">
	<th align="right">00:00</th>
	<td align="right">361.57</td>
	<td align="right">3.13</td>
	<td align="right">13,861,280.29</td>
	<td align="right">2.14</td>
	<td align="right">7.60</td>
	<td align="right">1.50</td>
	<td align="right">30,802.85</td>
	<td align="right">3,850.36</td>
	
</tr>
<tr class="jamn">
	<th align="right">01:00</th>
	<td align="right">291.57</td>
	<td align="right">2.52</td>
	<td align="right">11,394,431.86</td>
	<td align="right">1.76</td>
	<td align="right">12.70</td>
	<td align="right">2.50</td>
	<td align="right">25,320.96</td>
	<td align="right">3,165.12</td>
	
</tr>
<tr class="udda">
	<th align="right">02:00</th>
	<td align="right">282.29</td>
	<td align="right">2.44</td>
	<td align="right">8,441,565.00</td>
	<td align="right">1.30</td>
	<td align="right">8.41</td>
	<td align="right">1.66</td>
	<td align="right">18,759.03</td>
	<td align="right">2,344.88</td>
	
</tr>
<tr class="jamn">
	<th align="right">03:00</th>
	<td align="right">283.86</td>
	<td align="right">2.45</td>
	<td align="right">6,564,340.57</td>
	<td align="right">1.01</td>
	<td align="right">9.95</td>
	<td align="right">1.96</td>
	<td align="right">14,587.42</td>
	<td align="right">1,823.43</td>
	
</tr>
<tr class="udda">
	<th align="right">04:00</th>
	<td align="right">278.43</td>
	<td align="right">2.41</td>
	<td align="right">5,030,615.43</td>
	<td align="right">0.78</td>
	<td align="right">9.95</td>
	<td align="right">1.96</td>
	<td align="right">11,179.15</td>
	<td align="right">1,397.39</td>
	
</tr>
<tr class="jamn">
	<th align="right">05:00</th>
	<td align="right">266.57</td>
	<td align="right">2.30</td>
	<td align="right">4,685,542.86</td>
	<td align="right">0.72</td>
	<td align="right">10.97</td>
	<td align="right">2.16</td>
	<td align="right">10,412.32</td>
	<td align="right">1,301.54</td>
	
</tr>
<tr class="udda">
	<th align="right">06:00</th>
	<td align="right">290.14</td>
	<td align="right">2.51</td>
	<td align="right">7,315,188.43</td>
	<td align="right">1.13</td>
	<td align="right">10.92</td>
	<td align="right">2.15</td>
	<td align="right">16,255.97</td>
	<td align="right">2,032.00</td>
	
</tr>
<tr class="jamn">
	<th align="right">07:00</th>
	<td align="right">352.43</td>
	<td align="right">3.05</td>
	<td align="right">12,740,463.14</td>
	<td align="right">1.96</td>
	<td align="right">14.17</td>
	<td align="right">2.79</td>
	<td align="right">28,312.14</td>
	<td align="right">3,539.02</td>
	
</tr>
<tr class="udda">
	<th align="right">08:00</th>
	<td align="right">454.71</td>
	<td align="right">3.93</td>
	<td align="right">23,349,405.57</td>
	<td align="right">3.60</td>
	<td align="right">17.46</td>
	<td align="right">3.44</td>
	<td align="right">51,887.57</td>
	<td align="right">6,485.95</td>
	
</tr>
<tr class="jamn">
	<th align="right">09:00</th>
	<td align="right">640.14</td>
	<td align="right">5.53</td>
	<td align="right">38,377,243.71</td>
	<td align="right">5.91</td>
	<td align="right">32.69</td>
	<td align="right">6.43</td>
	<td align="right">85,282.76</td>
	<td align="right">10,660.35</td>
	
</tr>
<tr class="udda">
	<th align="right">10:00</th>
	<td align="right">719.86</td>
	<td align="right">6.22</td>
	<td align="right">52,835,989.86</td>
	<td align="right">8.14</td>
	<td align="right">30.68</td>
	<td align="right">6.04</td>
	<td align="right">117,413.31</td>
	<td align="right">14,676.66</td>
	
</tr>
<tr class="jamn">
	<th align="right">11:00</th>
	<td align="right">640.57</td>
	<td align="right">5.54</td>
	<td align="right">40,149,486.00</td>
	<td align="right">6.19</td>
	<td align="right">31.18</td>
	<td align="right">6.14</td>
	<td align="right">89,221.08</td>
	<td align="right">11,152.64</td>
	
</tr>
<tr class="udda">
	<th align="right">12:00</th>
	<td align="right">590.14</td>
	<td align="right">5.10</td>
	<td align="right">39,857,221.71</td>
	<td align="right">6.14</td>
	<td align="right">32.14</td>
	<td align="right">6.32</td>
	<td align="right">88,571.60</td>
	<td align="right">11,071.45</td>
	
</tr>
<tr class="jamn">
	<th align="right">13:00</th>
	<td align="right">681.43</td>
	<td align="right">5.89</td>
	<td align="right">40,353,885.71</td>
	<td align="right">6.22</td>
	<td align="right">28.02</td>
	<td align="right">5.51</td>
	<td align="right">89,675.30</td>
	<td align="right">11,209.41</td>
	
</tr>
<tr class="udda">
	<th align="right">14:00</th>
	<td align="right">660.71</td>
	<td align="right">5.71</td>
	<td align="right">44,375,607.57</td>
	<td align="right">6.84</td>
	<td align="right">29.60</td>
	<td align="right">5.83</td>
	<td align="right">98,612.46</td>
	<td align="right">12,326.56</td>
	
</tr>
<tr class="jamn">
	<th align="right">15:00</th>
	<td align="right">488.43</td>
	<td align="right">4.22</td>
	<td align="right">29,488,283.71</td>
	<td align="right">4.54</td>
	<td align="right">26.33</td>
	<td align="right">5.18</td>
	<td align="right">65,529.52</td>
	<td align="right">8,191.19</td>
	
</tr>
<tr class="udda">
	<th align="right">16:00</th>
	<td align="right">498.29</td>
	<td align="right">4.31</td>
	<td align="right">32,951,254.71</td>
	<td align="right">5.08</td>
	<td align="right">19.32</td>
	<td align="right">3.80</td>
	<td align="right">73,225.01</td>
	<td align="right">9,153.13</td>
	
</tr>
<tr class="jamn">
	<th align="right">17:00</th>
	<td align="right">521.29</td>
	<td align="right">4.51</td>
	<td align="right">29,734,344.14</td>
	<td align="right">4.58</td>
	<td align="right">24.91</td>
	<td align="right">4.90</td>
	<td align="right">66,076.32</td>
	<td align="right">8,259.54</td>
	
</tr>
<tr class="udda">
	<th align="right">18:00</th>
	<td align="right">553.43</td>
	<td align="right">4.78</td>
	<td align="right">32,230,966.57</td>
	<td align="right">4.97</td>
	<td align="right">25.86</td>
	<td align="right">5.09</td>
	<td align="right">71,624.37</td>
	<td align="right">8,953.05</td>
	
</tr>
<tr class="jamn">
	<th align="right">19:00</th>
	<td align="right">569.86</td>
	<td align="right">4.93</td>
	<td align="right">41,242,443.43</td>
	<td align="right">6.35</td>
	<td align="right">22.01</td>
	<td align="right">4.33</td>
	<td align="right">91,649.87</td>
	<td align="right">11,456.23</td>
	
</tr>
<tr class="udda">
	<th align="right">20:00</th>
	<td align="right">573.00</td>
	<td align="right">4.95</td>
	<td align="right">36,244,691.86</td>
	<td align="right">5.58</td>
	<td align="right">24.72</td>
	<td align="right">4.86</td>
	<td align="right">80,543.76</td>
	<td align="right">10,067.97</td>
	
</tr>
<tr class="jamn">
	<th align="right">21:00</th>
	<td align="right">601.00</td>
	<td align="right">5.20</td>
	<td align="right">43,435,393.86</td>
	<td align="right">6.69</td>
	<td align="right">26.17</td>
	<td align="right">5.15</td>
	<td align="right">96,523.10</td>
	<td align="right">12,065.39</td>
	
</tr>
<tr class="udda">
	<th align="right">22:00</th>
	<td align="right">551.43</td>
	<td align="right">4.77</td>
	<td align="right">34,821,342.71</td>
	<td align="right">5.36</td>
	<td align="right">22.56</td>
	<td align="right">4.44</td>
	<td align="right">77,380.76</td>
	<td align="right">9,672.60</td>
	
</tr>
<tr class="jamn">
	<th align="right">23:00</th>
	<td align="right">415.71</td>
	<td align="right">3.59</td>
	<td align="right">19,614,400.29</td>
	<td align="right">3.02</td>
	<td align="right">29.82</td>
	<td align="right">5.87</td>
	<td align="right">43,587.56</td>
	<td align="right">5,448.44</td>
	
</tr>

</tbody>
</table>

