

<script>
var componentsData = new Array(
        
        "*end*"
);
var componentsByPage = new Array();
var epochRange = "";
var sortedByAccesses = 0;
var sortedByBytes = 0;
var sortedByVisits = 0;
var alphabetized = 0;
var withBytes = 0;
var withVisits = 0;
var withDownloads = 0;
var subreportKeyword = "";
var subreportTitle = "";
var subreportAll = 0;
var subreportTop = 0;
var subreportTotal = 0;
var subreportPie = "";
var subreportGreenbarFlag = 0;
var pc = 0, ac = 0;
var dataEnd = 0;
var data;
var dataArrays = new Array(
	new Array(
		"Week of 2019-12-9 to 2019-12-15",	
		"*sorted-by-accesses*",
		"*bytes*",
		"*visits*",
		"*subreport*",
	"pages",
	"Documents",
	"*top*",
		30,
	
	"*total*",
	"2,009",
	"*pie*",
	"",
	"*row*",
	"1",
	"/",
	"*local-link*",
		"http://www.u5645470.fsdata.se/",
	
	"24,746",
	"30.56",
	"195,734,719",
		"4.31",
	"1,072",
		"2.39",
	"*row*",
	"2",
	"/wp-content/uploads/2017/06/Judiska_centr_svart_web-1.png",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/uploads/2017/06/Judiska_centr_svart_web-1.png",
	
	"11,484",
	"14.18",
	"104,150,810",
		"2.29",
	"1,551",
		"3.46",
	"*row*",
	"3",
	"/wp-content/cache/minify/768dd.css",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/cache/minify/768dd.css",
	
	"1,575",
	"1.95",
	"145,384,907",
		"3.20",
	"1,651",
		"3.69",
	"*row*",
	"4",
	"/wp-content/cache/minify/dfacc.js",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/cache/minify/dfacc.js",
	
	"1,572",
	"1.94",
	"5,820,104",
		"0.13",
	"1,656",
		"3.70",
	"*row*",
	"5",
	"/wp-content/cache/minify/f6d37.js",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/cache/minify/f6d37.js",
	
	"1,555",
	"1.92",
	"553,279",
		"0.01",
	"1,642",
		"3.67",
	"*row*",
	"6",
	"/wp-content/cache/minify/29e8b.js",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/cache/minify/29e8b.js",
	
	"1,552",
	"1.92",
	"11,820,875",
		"0.26",
	"1,641",
		"3.66",
	"*row*",
	"7",
	"/wp-content/cache/minify/17e60.js",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/cache/minify/17e60.js",
	
	"1,543",
	"1.91",
	"104,133,456",
		"2.29",
	"1,630",
		"3.64",
	"*row*",
	"8",
	"/wp-content/cache/minify/300ec.css",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/cache/minify/300ec.css",
	
	"1,461",
	"1.80",
	"41,599,878",
		"0.92",
	"1,538",
		"3.43",
	"*row*",
	"9",
	"/wp-includes/js/wp-emoji-release.min.js",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-includes/js/wp-emoji-release.min.js",
	
	"1,449",
	"1.79",
	"6,747,938",
		"0.15",
	"1,488",
		"3.32",
	"*row*",
	"10",
	"/wp-content/cache/minify/21588.js",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/cache/minify/21588.js",
	
	"1,439",
	"1.78",
	"54,764,955",
		"1.21",
	"1,506",
		"3.36",
	"*row*",
	"11",
	"/wp-content/themes/Divi/core/admin/fonts/modules.ttf",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/themes/Divi/core/admin/fonts/modules.ttf",
	
	"1,432",
	"1.77",
	"53,731,150",
		"1.18",
	"1,531",
		"3.42",
	"*row*",
	"12",
	"/wp-content/uploads/2019/05/cropped-Judiska_svart-2-32x32.pn<br>\ng",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/uploads/2019/05/cropped-Judiska_svart-2-32x32.png",
	
	"1,139",
	"1.41",
	"856,548",
		"0.02",
	"1,146",
		"2.56",
	"*row*",
	"13",
	"/wp-login.php",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-login.php",
	
	"1,125",
	"1.39",
	"1,768,417",
		"0.04",
	"601",
		"1.34",
	"*row*",
	"14",
	"/wp-content/themes/divi-child/img/location.png",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/themes/divi-child/img/location.png",
	
	"946",
	"1.17",
	"2,886,030",
		"0.06",
	"964",
		"2.15",
	"*row*",
	"15",
	"/wp-content/themes/divi-child/img/Instagram.png",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/themes/divi-child/img/Instagram.png",
	
	"889",
	"1.10",
	"5,386,751",
		"0.12",
	"907",
		"2.02",
	"*row*",
	"16",
	"/wp-content/themes/divi-child/img/facebook.png",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/themes/divi-child/img/facebook.png",
	
	"885",
	"1.09",
	"4,293,146",
		"0.09",
	"906",
		"2.02",
	"*row*",
	"17",
	"/wp-content/themes/divi-child/img/clock.png",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/themes/divi-child/img/clock.png",
	
	"812",
	"1.00",
	"1,843,240",
		"0.04",
	"881",
		"1.97",
	"*row*",
	"18",
	"/wp-content/uploads/2019/05/cropped-Judiska_svart-2-192x192.<br>\npng",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/uploads/2019/05/cropped-Judiska_svart-2-192x192.png",
	
	"788",
	"0.97",
	"5,259,112",
		"0.12",
	"797",
		"1.78",
	"*row*",
	"19",
	"/utforska/om-judendomen",
	"*local-link*",
		"http://www.u5645470.fsdata.se/utforska/om-judendomen",
	
	"676",
	"0.83",
	"9,783,797",
		"0.22",
	"569",
		"1.27",
	"*row*",
	"20",
	"/wp-admin/admin-ajax.php",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-admin/admin-ajax.php",
	
	"584",
	"0.72",
	"4,370,384",
		"0.10",
	"20",
		"0.05",
	"*row*",
	"21",
	"/wp-content/uploads/2019/11/stadshuset.jpg",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/uploads/2019/11/stadshuset.jpg",
	
	"567",
	"0.70",
	"102,886,149",
		"2.26",
	"613",
		"1.37",
	"*row*",
	"22",
	"/wp-content/uploads/2019/06/till-webb_4-1.jpg",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/uploads/2019/06/till-webb_4-1.jpg",
	
	"560",
	"0.69",
	"56,211,680",
		"1.24",
	"602",
		"1.34",
	"*row*",
	"23",
	"/wp-content/uploads/2019/12/channukian-shopen.jpg",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/uploads/2019/12/channukian-shopen.jpg",
	
	"547",
	"0.68",
	"160,288,610",
		"3.53",
	"589",
		"1.32",
	"*row*",
	"24",
	"/feed",
	"*local-link*",
		"http://www.u5645470.fsdata.se/feed",
	
	"547",
	"0.68",
	"371,331",
		"0.01",
	"39",
		"0.09",
	"*row*",
	"25",
	"/xmlrpc.php",
	"*local-link*",
		"http://www.u5645470.fsdata.se/xmlrpc.php",
	
	"543",
	"0.67",
	"135,801",
		"0.00",
	"581",
		"1.30",
	"*row*",
	"26",
	"/wp-content/uploads/2019/07/annons.jpg",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/uploads/2019/07/annons.jpg",
	
	"522",
	"0.64",
	"24,141,765",
		"0.53",
	"551",
		"1.23",
	"*row*",
	"27",
	"/wp-content/uploads/2019/07/mönster.jpg",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/uploads/2019/07/mönster.jpg",
	
	"513",
	"0.63",
	"82,635,066",
		"1.82",
	"549",
		"1.23",
	"*row*",
	"28",
	"/wp-content/uploads/2017/06/dörrpostkapsel.jpg",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/uploads/2017/06/dörrpostkapsel.jpg",
	
	"494",
	"0.61",
	"73,650,750",
		"1.62",
	"527",
		"1.18",
	"*row*",
	"29",
	"/wp-content/uploads/2017/06/förbundet.jpg",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/uploads/2017/06/förbundet.jpg",
	
	"445",
	"0.55",
	"125,667,096",
		"2.77",
	"477",
		"1.06",
	"*row*",
	"30",
	"/wp-content/uploads/2017/06/bibel-tora-talmud1.jpg",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/uploads/2017/06/bibel-tora-talmud1.jpg",
	
	"435",
	"0.54",
	"125,583,423",
		"2.76",
	"451",
		"1.01",
	
		"*end*"
		)
	);

function incPC()
{
	pc++;
	if (pc == data.length) {
		if ((ac == dataArrays.length) || (dataArrays[ac] == "*end*")) {
			dataEnd = 1;
		} else {
			ac++;
			data = dataArrays[ac];
			pc = 0;
		}
	}	
}

function resetPC()
{
	pc = 0;
	ac = 0;
	if (dataArrays.length) {
		data = dataArrays[0];
		dataEnd = 0;
	} else {	
		dataEnd = 1;
	}
}

function compileReports()
{
	resetPC();
	compileDocuments();
}

function compileDocuments()
{
	var s = data[pc];
	epochRange = data[pc];
	incPC();
	while (dataEnd == 0) {
		var s = data[pc];
		if (s == "*sorted-by-accesses*") {
			sortedByAccesses = 1;
			incPC();
		} else if (s == "*sorted-by-bytes*") {
			sortedByBytes = 1;
			incPC();
		} else if (s == "*sorted-by-visits*") {
			sortedByVisits = 1;
			incPC();
		} else if (s == "*alphabetized*") {
			alphabetized = 1;
			incPC();
		} else if (s == "*bytes*") {
			withBytes = 1;
			incPC();
		} else if (s == "*visits*") {
			withVisits = 1;
			incPC();
		} else if (s == "*downloads*") {
			withDownloads = 1;
			incPC();
		} else {
			break;
		}
	}
	documentsHead();
	while (dataEnd == 0) {
		if (compileSubreport() == 0) {
			// No more subreports. That's OK 
			break;
		}	
	}
	documentsTail();
	return 1;
}
function compileSubreport()
{
	if (data[pc] != "*subreport*") {
		return 0;
	}
	incPC();	
	subreportKeyword = "";
	subreportTitle = "";
	subreportAll = 0;
	subreportTop = 0;
	subreportTotal = 0;
	subreportPie = "";
	subreportKeyword = data[pc];
	incPC();
	subreportTitle = data[pc];
	incPC();
	if (data[pc] == "*all*") {
		incPC();
		subreportAll = 1;
	}
	if (data[pc] == "*top*") {
		incPC();
		subreportTop = data[pc];
		incPC();
	}
	if (data[pc] == "*total*") {
		incPC();
		subreportTotal = data[pc];
		incPC();
	}
	if (data[pc] == "*pie*") {
		incPC();
		subreportPie = data[pc];
		incPC();
	}
	subreportHead();
	while (dataEnd == 0) {
		if (compileSubreportRow() == 0) {
			// No more subreport rows. That's OK 
			break;
		}
	}
	subreportTail();
	return 1;
}
var rowRank = 0;
var rowItem = "";
var rowLocalLink = "";
var rowTitle = "";
var rowReferrers = "";
var rowAccesses = 0;
var rowAccessesPer = 0;
var rowBytes = 0;
var rowBytesPer = 0;
var rowVisits = 0;
var rowVisitsPer = 0;
var rowDownloadsPer = 0;
var rowComponents = 0;

function compileSubreportRow()
{
	if (data[pc] != "*row*") {
		return 0;
	}
	rowRank = 0;
	rowItem = "";
	rowLocalLink = "";
	rowTitle = "";
	rowReferrers = "";
	rowComponents = 0;
	incPC();
	rowRank = data[pc];
	incPC();
	rowItem = data[pc];
	incPC();
	if (data[pc] == "*local-link*") {
		incPC();
		rowLocalLink = data[pc];
		incPC();
	}
	if (data[pc] == "*title*") {
		incPC();
		rowTitle = data[pc];
		incPC();
	}
	if (data[pc] == "*referrers*") {
		incPC();
		rowReferrers = data[pc];
		incPC();
	}
	if (data[pc] == "*components*") {
		if (compileComponentList() == 0) {
			return 0;
		}
	}		
	rowAccesses = data[pc];	
	incPC();
	rowAccessesPer = data[pc];	
	incPC();
	if (withBytes) {
		rowBytes = data[pc];
		incPC();
		rowBytesPer = data[pc];
		incPC();
	}
	if (withVisits) {
		rowVisits = data[pc];
		incPC();
		rowVisitsPer = data[pc];
		incPC();
	}
	if (withDownloads) {
		rowDownloadsPer = data[pc];
		incPC();
	}
	subreportRow();	
	return 1;
}

function compileComponentList()
{
	if (data[pc] != "*components*") {
		return 0;
	}
	incPC();
	var alreadyFirst = componentsByPage[rowItem + ".1"];
	while (dataEnd == 0) {
		if (data[pc] == "*end*") {
			incPC();
			return 1;
		}
		rowComponents++;
		var index = data[pc];
		incPC();
		if (!alreadyFirst) {
			addComponent(rowItem, index);
		}
	}
	return -1;
}

function listComponents(w, item)
{
	var i = 1;
	while (1) {
		var s = componentsByPage[item + "." + i];
		if (!s) {
			return;
		}
		var offset = s * 5;
		var name = componentsData[offset++];
		var accesses = componentsData[offset++];
		var bytes = componentsData[offset++];
		var visits = componentsData[offset++];
		var downloadsper = componentsData[offset++];
		w.document.writeln("<td>");
		w.document.writeln(name);
		w.document.writeln("</td>");
		w.document.writeln("<td align=right>");
		w.document.writeln(accesses);
		w.document.writeln("</td>");
		if (withBytes) {
			w.document.writeln("<td align=right>");
			w.document.writeln(bytes);
			w.document.writeln("</td>");
		}	
		if (withVisits) {
			w.document.writeln("<td align=right>");
			w.document.writeln(visits);
			w.document.writeln("</td>");
		}	
		if (withDownloads) {
			w.document.writeln("<td align=right>");
			w.document.writeln(downloadsper);
			w.document.writeln("</td>");
		}	
		w.document.writeln("</tr>");
		i++;
	}
}

function addComponent(item, index)
{
	var i = 1;
	while (1) {
		var s = componentsByPage[item + "." + i];
		if (!s) {
			break;
		}
		i++;
	}
	componentsByPage[item + "." + i] = index;
}

function wl(s)
{
	document.writeln(s);
}
function documentsHead()
{
	wl("<h2>" + epochRange + "</h2>");
	

wl("<h3>Menu</h3>");

wl("<div class='colmenu'>");
wl("<div class='column'>");
wl("<ul id='infolankar'>");
wl("<li><a href=\"../../../../index.php\">Calendar of Reports</a></li>");
wl("<li><a href=\"index.php\">Executive Summary and Totals</a></li>");
wl("<li><a href=\"hours.php\">Accesses by Hour</a></li>");
wl("<li><a href=\"days.php\">Accesses by Day</a></li>");
wl("<li><a href=\"documents.php\">Top Documents</a></li>");
wl("<li><a href=\"directories.php\">Documents by Directory</a></li>");
wl("<li><a href=\"types.php\">File Types (Extensions)</a></li>");

wl("</ul>");
wl("</div>");
wl("<div class='column'>");
wl("<ul id='infolankar'>");
wl("<li><a href=\"sites.php\">Top Visitor Sites</a></li>");
wl("<li><a href=\"useragents.php\">Top Web Browsers</a></li>");
wl("<li><a href=\"os.php\">Top Operating Systems</a></li>");
wl("<li><a href=\"referringsites.php\">Top Referring Sites</a></li>");
wl("<li><a href=\"notfound.php\">Documents Not Found</a></li>");
wl("<li><a href=\"resultcodes.php\">Accesses by Result Code</a></li>");

wl("</ul>");
wl("</div>");
wl("<div class='colmenufoot'></div>");
wl("</div>");

}
function documentsTail()
{
}
function subreportHead()
{
	wl("<h3><a name=\"" + subreportKeyword + "\">");
	if (subreportAll) {
		wl("All " + subreportTitle);
	} else {
		wl("Top " + subreportTop + " of " + subreportTotal + " " + subreportTitle + ", ");
	}
        if (sortedByAccesses) {
                wl("Sorted by Access Count");
        } else if (sortedByBytes) {
                wl("Sorted by Bytes");
        } else if (sortedByVisits) {
                wl("Sorted by Visits");
        }
        if (alphabetized) {
                wl("and Alphabetized");
        }
	wl("</a></h3><p></p>"); 

	if (subreportPie) {
		wl("<div align=\"center\">");
		wl(subreportPie);
		wl("</div>");
	}
	wl("<table cellspacing=\"0\" class=\"styladtabell\">");
	wl("<thead>");
	wl("<tr>");
	wl("<th scope=\"col\">Rank</th>");
	wl("<th scope=\"col\">Item</th>");
	wl("<th scope=\"col\">Accesses</th>");
	wl("<th scope=\"col\">%</th>");
	if (withBytes) {
		wl("<th scope=\"col\">Bytes</th>");
		wl("<th scope=\"col\">%</th>");
	}
	if (withVisits) {
		wl("<th scope=\"col\">Visits</th>");
		wl("<th scope=\"col\">%</th>");
	}
	wl("</tr>");
	wl("</thead>");
	wl("<tbody>");
}	
function subreportTail()
{
	wl("</tbody>");
	wl("</table>");
}
function subreportRow()
{
        var gc = "jamn";
        var dirLink = 0;
        if (subreportGreenbarFlag) {
                gc = "udda";
        }
        subreportGreenbarFlag = !subreportGreenbarFlag;
	wl("<tr class=\"" + gc + "\">");
	wl("<th class=\"hogerstalld\">" + rowRank + "</th>");
	wl("<td align=\"right\">");
	if (rowLocalLink) {
		wl("<a href=\"" + rowLocalLink + "\">");
	}
	if (rowTitle) {
		wl("<b>" + rowTitle + "</b><br>");
	}
	wl(rowItem);
	if (rowLocalLink) {
		wl("</a>");
	}	
	if (rowReferrers) {
		wl("<a href=\"" + rowReferrers + "\" target=\"referrers\">[Referrers]</a>");
	}
	wl("</td>");
	wl("<td align=\"right\">" + rowAccesses + "</td>");
	wl("<td align=\"right\">" + rowAccessesPer + "</td>");
	if (withBytes) {
		wl("<td align=\"right\">" + rowBytes + "</td>");
		wl("<td align=\"right\">" + rowBytesPer + "</td>");
	}
	if (withVisits) {
		wl("<td align=\"right\">" + rowVisits + "</td>");
		wl("<td align=\"right\">" + rowVisitsPer + "</td>");
	}
	if (withDownloads) {
		wl("<td align=\"right\">" + rowDownloadsPer + "</td>");
	}
	wl("</tr>");
}

compileReports(0);	
</script>
