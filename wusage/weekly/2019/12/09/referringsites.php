


<h2>Statistics for http://www.u5645470.fsdata.se, Week of 2019-12-9 to 2019-12-15</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 20 of 216 Referring Sites,
Sorted by Access Count

</h3>
<p></p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Referring Site</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th class="hogerstalld">1</th>
	<td>
	<a href="https://judiskamuseet.se">
	https://judiskamuseet.se</a>
	</td>
	<td align="right">34,232</td>
	<td align="right">88.79</td>
	<td align="right">3,742,771,016</td>
	<td align="right">96.52</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	<a href="http://judiskamuseet.se">
	http://judiskamuseet.se</a>
	</td>
	<td align="right">2,002</td>
	<td align="right">5.19</td>
	<td align="right">48,419,552</td>
	<td align="right">1.25</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	<a href="https://www.google.com">
	https://www.google.com</a>
	</td>
	<td align="right">1,099</td>
	<td align="right">2.85</td>
	<td align="right">48,535,137</td>
	<td align="right">1.25</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	<a href="https://www.google.se">
	https://www.google.se</a>
	</td>
	<td align="right">511</td>
	<td align="right">1.33</td>
	<td align="right">19,596,385</td>
	<td align="right">0.51</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	<a href="https://www.google.de">
	https://www.google.de</a>
	</td>
	<td align="right">84</td>
	<td align="right">0.22</td>
	<td align="right">1,277,359</td>
	<td align="right">0.03</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	<a href="http://www.google.com">
	http://www.google.com</a>
	</td>
	<td align="right">65</td>
	<td align="right">0.17</td>
	<td align="right">525,456</td>
	<td align="right">0.01</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	<a href="https://www.bing.com">
	https://www.bing.com</a>
	</td>
	<td align="right">21</td>
	<td align="right">0.05</td>
	<td align="right">402,775</td>
	<td align="right">0.01</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	<a href="http://localhost">
	http://localhost</a>
	</td>
	<td align="right">20</td>
	<td align="right">0.05</td>
	<td align="right">2,083,052</td>
	<td align="right">0.05</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	<a href="http://www.judiska-museet.se">
	http://www.judiska-museet.se</a>
	</td>
	<td align="right">14</td>
	<td align="right">0.04</td>
	<td align="right">14,498</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">10</th>
	<td>
	<a href="https://%20judiskamuseet.se">
	https:// judiskamuseet.se</a>
	</td>
	<td align="right">12</td>
	<td align="right">0.03</td>
	<td align="right">134,154</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">11</th>
	<td>
	<a href="https://mnimmigrantrights.net">
	https://mnimmigrantrights.net</a>
	</td>
	<td align="right">9</td>
	<td align="right">0.02</td>
	<td align="right">554,124</td>
	<td align="right">0.01</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">12</th>
	<td>
	<a href="https://dreamtravelonthego.com">
	https://dreamtravelonthego.com</a>
	</td>
	<td align="right">9</td>
	<td align="right">0.02</td>
	<td align="right">521,778</td>
	<td align="right">0.01</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">13</th>
	<td>
	<a href="https://s-forum.biz">
	https://s-forum.biz</a>
	</td>
	<td align="right">9</td>
	<td align="right">0.02</td>
	<td align="right">520,656</td>
	<td align="right">0.01</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">14</th>
	<td>
	<a href="https://duckduckgo.com">
	https://duckduckgo.com</a>
	</td>
	<td align="right">8</td>
	<td align="right">0.02</td>
	<td align="right">115,261</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">15</th>
	<td>
	<a href="https://myactivity.google.com">
	https://myactivity.google.com</a>
	</td>
	<td align="right">8</td>
	<td align="right">0.02</td>
	<td align="right">980,076</td>
	<td align="right">0.03</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">16</th>
	<td>
	<a href="https://r.search.yahoo.com">
	https://r.search.yahoo.com</a>
	</td>
	<td align="right">8</td>
	<td align="right">0.02</td>
	<td align="right">109,821</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">17</th>
	<td>
	<a href="https://en.wikipedia.org">
	https://en.wikipedia.org</a>
	</td>
	<td align="right">8</td>
	<td align="right">0.02</td>
	<td align="right">46,365</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">18</th>
	<td>
	<a href="https://balakhna.online">
	https://balakhna.online</a>
	</td>
	<td align="right">6</td>
	<td align="right">0.02</td>
	<td align="right">359,382</td>
	<td align="right">0.01</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">19</th>
	<td>
	<a href="https://dimfour.com">
	https://dimfour.com</a>
	</td>
	<td align="right">6</td>
	<td align="right">0.02</td>
	<td align="right">358,260</td>
	<td align="right">0.01</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">20</th>
	<td>
	<a href="https://l.instagram.com">
	https://l.instagram.com</a>
	</td>
	<td align="right">6</td>
	<td align="right">0.02</td>
	<td align="right">66,527</td>
	<td align="right">0.00</td>
</tr>

</tbody>
</table>

