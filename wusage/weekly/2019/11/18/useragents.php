


<h2>Statistics for http://www.u5645470.fsdata.se, Week of 2019-11-18 to 2019-11-24</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 10 of 221 Browsers (User Agents),
Sorted by Access Count

</h3>
<p></p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Product</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th class="hogerstalld">1</th>
	<td>
	Pingdom.com_bot_version_1.4_		
	</td>
	<td align="right">24,280</td>
	<td align="right">29.65</td>
	<td align="right">212,524,131</td>
	<td align="right">4.15</td>
	<td align="right">17</td>
	<td align="right">0.45</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	Safari/537.36		
	</td>
	<td align="right">23,716</td>
	<td align="right">28.96</td>
	<td align="right">2,257,739,314</td>
	<td align="right">44.12</td>
	<td align="right">1,129</td>
	<td align="right">28.61</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	Safari/604.1		
	</td>
	<td align="right">9,571</td>
	<td align="right">11.69</td>
	<td align="right">1,038,600,812</td>
	<td align="right">20.29</td>
	<td align="right">586</td>
	<td align="right">14.85</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	Mozilla/5.0 		
	</td>
	<td align="right">5,344</td>
	<td align="right">6.53</td>
	<td align="right">370,958,646</td>
	<td align="right">7.25</td>
	<td align="right">876</td>
	<td align="right">22.22</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	Safari/605.1.15		
	</td>
	<td align="right">3,912</td>
	<td align="right">4.78</td>
	<td align="right">371,674,662</td>
	<td align="right">7.26</td>
	<td align="right">228</td>
	<td align="right">5.79</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	bingbot/2.0		
	</td>
	<td align="right">1,334</td>
	<td align="right">1.63</td>
	<td align="right">123,894,051</td>
	<td align="right">2.42</td>
	<td align="right">237</td>
	<td align="right">6.01</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	UptimeRobot/2.0		
	</td>
	<td align="right">1,272</td>
	<td align="right">1.55</td>
	<td align="right">83,174</td>
	<td align="right">0.00</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	Safari/605.1		
	</td>
	<td align="right">1,188</td>
	<td align="right">1.45</td>
	<td align="right">132,327,542</td>
	<td align="right">2.59</td>
	<td align="right">63</td>
	<td align="right">1.62</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	curl/7.54.0		
	</td>
	<td align="right">1,049</td>
	<td align="right">1.28</td>
	<td align="right">11,861,277</td>
	<td align="right">0.23</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">10</th>
	<td>
	Safari/537.36 Edge/18.18362		
	</td>
	<td align="right">1,040</td>
	<td align="right">1.27</td>
	<td align="right">109,661,261</td>
	<td align="right">2.14</td>
	<td align="right">48</td>
	<td align="right">1.22</td>
</tr>

</tbody>
</table>

