


<h2>Statistics for http://www.u5645470.fsdata.se, Week of 2019-11-18 to 2019-11-24</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 10 of 20 File Types (Extensions), 
Sorted by Access Count

</h3>
<p>Individual file types as determined by file extensions. All URLs that
do not contain an extension are counted as directories.</p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Type</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th class="hogerstalld">1</th>
	<td>
	Directory (folder)		
	</td>
	<td align="right">32,847</td>
	<td align="right">40.10</td>
	<td align="right">310,294,904</td>
	<td align="right">6.06</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	png		
	</td>
	<td align="right">13,805</td>
	<td align="right">16.85</td>
	<td align="right">194,560,331</td>
	<td align="right">3.80</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	js		
	</td>
	<td align="right">12,514</td>
	<td align="right">15.28</td>
	<td align="right">248,732,299</td>
	<td align="right">4.86</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	jpg		
	</td>
	<td align="right">12,462</td>
	<td align="right">15.21</td>
	<td align="right">3,775,531,969</td>
	<td align="right">73.76</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	css		
	</td>
	<td align="right">4,606</td>
	<td align="right">5.62</td>
	<td align="right">274,250,642</td>
	<td align="right">5.36</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	php		
	</td>
	<td align="right">3,084</td>
	<td align="right">3.76</td>
	<td align="right">35,935,967</td>
	<td align="right">0.70</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	ttf		
	</td>
	<td align="right">1,665</td>
	<td align="right">2.03</td>
	<td align="right">62,516,618</td>
	<td align="right">1.22</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	txt		
	</td>
	<td align="right">362</td>
	<td align="right">0.44</td>
	<td align="right">23,367</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	jpeg		
	</td>
	<td align="right">345</td>
	<td align="right">0.42</td>
	<td align="right">181,188,491</td>
	<td align="right">3.54</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">10</th>
	<td>
	ico		
	</td>
	<td align="right">75</td>
	<td align="right">0.09</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>

</tbody>
</table>

