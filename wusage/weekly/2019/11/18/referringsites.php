


<h2>Statistics for http://www.u5645470.fsdata.se, Week of 2019-11-18 to 2019-11-24</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 20 of 194 Referring Sites,
Sorted by Access Count

</h3>
<p></p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Referring Site</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th class="hogerstalld">1</th>
	<td>
	<a href="https://judiskamuseet.se">
	https://judiskamuseet.se</a>
	</td>
	<td align="right">38,557</td>
	<td align="right">89.33</td>
	<td align="right">4,344,097,361</td>
	<td align="right">96.55</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	<a href="http://judiskamuseet.se">
	http://judiskamuseet.se</a>
	</td>
	<td align="right">2,056</td>
	<td align="right">4.76</td>
	<td align="right">42,254,228</td>
	<td align="right">0.94</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	<a href="https://www.google.com">
	https://www.google.com</a>
	</td>
	<td align="right">1,265</td>
	<td align="right">2.93</td>
	<td align="right">64,031,038</td>
	<td align="right">1.42</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	<a href="https://www.google.se">
	https://www.google.se</a>
	</td>
	<td align="right">498</td>
	<td align="right">1.15</td>
	<td align="right">22,787,659</td>
	<td align="right">0.51</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	<a href="https://www.google.de">
	https://www.google.de</a>
	</td>
	<td align="right">99</td>
	<td align="right">0.23</td>
	<td align="right">1,733,963</td>
	<td align="right">0.04</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	<a href="http://www.google.com">
	http://www.google.com</a>
	</td>
	<td align="right">56</td>
	<td align="right">0.13</td>
	<td align="right">528,013</td>
	<td align="right">0.01</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	<a href="https://www.bing.com">
	https://www.bing.com</a>
	</td>
	<td align="right">54</td>
	<td align="right">0.13</td>
	<td align="right">6,242,705</td>
	<td align="right">0.14</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	<a href="https://%20judiskamuseet.se">
	https:// judiskamuseet.se</a>
	</td>
	<td align="right">52</td>
	<td align="right">0.12</td>
	<td align="right">539,029</td>
	<td align="right">0.01</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	<a href="http://m.facebook.com">
	http://m.facebook.com</a>
	</td>
	<td align="right">36</td>
	<td align="right">0.08</td>
	<td align="right">424,844</td>
	<td align="right">0.01</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">10</th>
	<td>
	<a href="http://www.judiska-museet.se">
	http://www.judiska-museet.se</a>
	</td>
	<td align="right">14</td>
	<td align="right">0.03</td>
	<td align="right">14,570</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">11</th>
	<td>
	<a href="https://se.images.search.yahoo.com">
	https://se.images.search.yahoo.com</a>
	</td>
	<td align="right">14</td>
	<td align="right">0.03</td>
	<td align="right">1,907,144</td>
	<td align="right">0.04</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">12</th>
	<td>
	<a href="https://pshare.biz">
	https://pshare.biz</a>
	</td>
	<td align="right">11</td>
	<td align="right">0.03</td>
	<td align="right">591,580</td>
	<td align="right">0.01</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">13</th>
	<td>
	<a href="http://www.stockholmmuseums.se">
	http://www.stockholmmuseums.se</a>
	</td>
	<td align="right">9</td>
	<td align="right">0.02</td>
	<td align="right">66,553</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">14</th>
	<td>
	<a href="http://progressive-seo.com">
	http://progressive-seo.com</a>
	</td>
	<td align="right">9</td>
	<td align="right">0.02</td>
	<td align="right">52,052</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">15</th>
	<td>
	<a href="https://duckduckgo.com">
	https://duckduckgo.com</a>
	</td>
	<td align="right">9</td>
	<td align="right">0.02</td>
	<td align="right">130,668</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">16</th>
	<td>
	<a href="https://classroom.google.com">
	https://classroom.google.com</a>
	</td>
	<td align="right">9</td>
	<td align="right">0.02</td>
	<td align="right">115,003</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">17</th>
	<td>
	<a href="http://%20www.judiskamuseet.se">
	http:// www.judiskamuseet.se</a>
	</td>
	<td align="right">9</td>
	<td align="right">0.02</td>
	<td align="right">180</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">18</th>
	<td>
	<a href="http://domain.com">
	http://domain.com</a>
	</td>
	<td align="right">8</td>
	<td align="right">0.02</td>
	<td align="right">1,250,100</td>
	<td align="right">0.03</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">19</th>
	<td>
	<a href="https://r.search.yahoo.com">
	https://r.search.yahoo.com</a>
	</td>
	<td align="right">8</td>
	<td align="right">0.02</td>
	<td align="right">119,122</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">20</th>
	<td>
	<a href="https://sv.wikipedia.org">
	https://sv.wikipedia.org</a>
	</td>
	<td align="right">7</td>
	<td align="right">0.02</td>
	<td align="right">47,332</td>
	<td align="right">0.00</td>
</tr>

</tbody>
</table>

