


<h2>Statistics for http://www.u5645470.fsdata.se, Week of 2019-11-18 to 2019-11-24</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Traffic by Day</h3>
<img src="d.png" width="468" height="351">
<br /><br />


<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Date</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	<th scope='col'>Bits per Sec</th>
	<th scope='col'>Bytes per Sec</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th align="right">2019-11-18</th>
	<td align="right">11,482	</td>
	<td align="right">14.02	</td>
	<td align="right">816,618,789	</td>
	<td align="right">15.95	</td>
	<td align="right">616	</td>
	<td align="right">15.61	</td>
	<td align="right">75,612.85	</td>
	<td align="right">9,451.61	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-11-19</th>
	<td align="right">10,989	</td>
	<td align="right">13.42	</td>
	<td align="right">740,635,226	</td>
	<td align="right">14.47	</td>
	<td align="right">602	</td>
	<td align="right">15.26	</td>
	<td align="right">68,577.34	</td>
	<td align="right">8,572.17	</td>
</tr>
<tr class="udda">
	<th align="right">2019-11-20</th>
	<td align="right">11,233	</td>
	<td align="right">13.71	</td>
	<td align="right">725,512,475	</td>
	<td align="right">14.17	</td>
	<td align="right">502	</td>
	<td align="right">12.72	</td>
	<td align="right">67,177.08	</td>
	<td align="right">8,397.14	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-11-21</th>
	<td align="right">14,055	</td>
	<td align="right">17.16	</td>
	<td align="right">868,924,259	</td>
	<td align="right">16.98	</td>
	<td align="right">641	</td>
	<td align="right">16.24	</td>
	<td align="right">80,455.95	</td>
	<td align="right">10,056.99	</td>
</tr>
<tr class="udda">
	<th align="right">2019-11-22</th>
	<td align="right">12,407	</td>
	<td align="right">15.15	</td>
	<td align="right">789,340,173	</td>
	<td align="right">15.42	</td>
	<td align="right">541	</td>
	<td align="right">13.71	</td>
	<td align="right">73,087.05	</td>
	<td align="right">9,135.88	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-11-23</th>
	<td align="right">10,158	</td>
	<td align="right">12.40	</td>
	<td align="right">515,756,129	</td>
	<td align="right">10.08	</td>
	<td align="right">515	</td>
	<td align="right">13.05	</td>
	<td align="right">47,755.20	</td>
	<td align="right">5,969.40	</td>
</tr>
<tr class="udda">
	<th align="right">2019-11-24</th>
	<td align="right">11,590	</td>
	<td align="right">14.15	</td>
	<td align="right">662,009,025	</td>
	<td align="right">12.93	</td>
	<td align="right">529	</td>
	<td align="right">13.41	</td>
	<td align="right">61,297.13	</td>
	<td align="right">7,662.14	</td>
</tr>

</tbody>
</table>

