


<h2>Statistics for http://www.u5645470.fsdata.se, Week of 2019-11-18 to 2019-11-24</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 20 of 3,825 Visitor Sites,
Sorted by Access Count

</h3>
<p></p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Site</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="jamnudda">
	<th class="hogerstalld">1</th>
	<td>
	83.170.113.210		
	</td>
	<td align="right">2,460</td>
	<td align="right">3.00</td>
	<td align="right">21,342,599</td>
	<td align="right">0.42</td>
	<td align="right">4</td>
	<td align="right">0.11</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	95.141.32.46		
	</td>
	<td align="right">2,420</td>
	<td align="right">2.95</td>
	<td align="right">21,157,779</td>
	<td align="right">0.41</td>
	<td align="right">4</td>
	<td align="right">0.11</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	185.246.208.82		
	</td>
	<td align="right">2,420</td>
	<td align="right">2.95</td>
	<td align="right">21,266,303</td>
	<td align="right">0.42</td>
	<td align="right">4</td>
	<td align="right">0.11</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	94.247.174.83		
	</td>
	<td align="right">2,417</td>
	<td align="right">2.95</td>
	<td align="right">21,200,014</td>
	<td align="right">0.41</td>
	<td align="right">4</td>
	<td align="right">0.11</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	178.255.153.2		
	</td>
	<td align="right">1,943</td>
	<td align="right">2.37</td>
	<td align="right">16,766,601</td>
	<td align="right">0.33</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	94.75.211.74		
	</td>
	<td align="right">1,941</td>
	<td align="right">2.37</td>
	<td align="right">16,799,546</td>
	<td align="right">0.33</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	185.39.146.215		
	</td>
	<td align="right">1,928</td>
	<td align="right">2.35</td>
	<td align="right">16,733,021</td>
	<td align="right">0.33</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	185.136.156.82		
	</td>
	<td align="right">1,923</td>
	<td align="right">2.35</td>
	<td align="right">16,716,381</td>
	<td align="right">0.33</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	85.195.116.134		
	</td>
	<td align="right">1,923</td>
	<td align="right">2.35</td>
	<td align="right">16,799,257</td>
	<td align="right">0.33</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">10</th>
	<td>
	46.20.45.18		
	</td>
	<td align="right">1,921</td>
	<td align="right">2.35</td>
	<td align="right">16,650,543</td>
	<td align="right">0.33</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">11</th>
	<td>
	46.59.25.88		
	</td>
	<td align="right">1,767</td>
	<td align="right">2.16</td>
	<td align="right">122,727,329</td>
	<td align="right">2.40</td>
	<td align="right">36</td>
	<td align="right">0.91</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">12</th>
	<td>
	63.143.42.242		
	</td>
	<td align="right">1,272</td>
	<td align="right">1.55</td>
	<td align="right">83,174</td>
	<td align="right">0.00</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">13</th>
	<td>
	213.212.29.165		
	</td>
	<td align="right">1,177</td>
	<td align="right">1.44</td>
	<td align="right">89,046,070</td>
	<td align="right">1.74</td>
	<td align="right">4</td>
	<td align="right">0.11</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">14</th>
	<td>
	89.163.242.206		
	</td>
	<td align="right">498</td>
	<td align="right">0.61</td>
	<td align="right">4,516,860</td>
	<td align="right">0.09</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">15</th>
	<td>
	185.39.146.214		
	</td>
	<td align="right">498</td>
	<td align="right">0.61</td>
	<td align="right">4,516,860</td>
	<td align="right">0.09</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">16</th>
	<td>
	151.106.52.134		
	</td>
	<td align="right">498</td>
	<td align="right">0.61</td>
	<td align="right">4,516,860</td>
	<td align="right">0.09</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">17</th>
	<td>
	185.93.3.92		
	</td>
	<td align="right">498</td>
	<td align="right">0.61</td>
	<td align="right">4,516,860</td>
	<td align="right">0.09</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">18</th>
	<td>
	82.103.136.16		
	</td>
	<td align="right">498</td>
	<td align="right">0.61</td>
	<td align="right">4,516,860</td>
	<td align="right">0.09</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">19</th>
	<td>
	37.252.231.50		
	</td>
	<td align="right">495</td>
	<td align="right">0.60</td>
	<td align="right">4,480,581</td>
	<td align="right">0.09</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">20</th>
	<td>
	90.229.237.5		
	</td>
	<td align="right">490</td>
	<td align="right">0.60</td>
	<td align="right">13,028,878</td>
	<td align="right">0.25</td>
	<td align="right">4</td>
	<td align="right">0.11</td>
</tr>

</tbody>
</table>

