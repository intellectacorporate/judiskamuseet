


<h2>Statistics for http://www.u5645470.fsdata.se, Week of 2019-11-18 to 2019-11-24</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 10 of 12 Operating Systems,
Sorted by Access Count

</h3>
<p></p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Product</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th class="hogerstalld">1</th>
	<td>
	Mac OS X		
	</td>
	<td align="right">44,362</td>
	<td align="right">54.17</td>
	<td align="right">4,283,834,700</td>
	<td align="right">83.71</td>
	<td align="right">2,452</td>
	<td align="right">62.14</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	Other		
	</td>
	<td align="right">32,847</td>
	<td align="right">40.11</td>
	<td align="right">547,197,718</td>
	<td align="right">10.69</td>
	<td align="right">661</td>
	<td align="right">16.78</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	Windows NT		
	</td>
	<td align="right">2,347</td>
	<td align="right">2.87</td>
	<td align="right">258,486,071</td>
	<td align="right">5.05</td>
	<td align="right">115</td>
	<td align="right">2.92</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	Linux		
	</td>
	<td align="right">2,121</td>
	<td align="right">2.59</td>
	<td align="right">14,262,842</td>
	<td align="right">0.28</td>
	<td align="right">713</td>
	<td align="right">18.08</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	Windows XP		
	</td>
	<td align="right">88</td>
	<td align="right">0.11</td>
	<td align="right">5,565,124</td>
	<td align="right">0.11</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	Windows Codename Longhorn		
	</td>
	<td align="right">53</td>
	<td align="right">0.06</td>
	<td align="right">5,425,125</td>
	<td align="right">0.11</td>
	<td align="right">3</td>
	<td align="right">0.08</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	Windows 2003		
	</td>
	<td align="right">36</td>
	<td align="right">0.04</td>
	<td align="right">1,175,184</td>
	<td align="right">0.02</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	Windows 2000		
	</td>
	<td align="right">10</td>
	<td align="right">0.01</td>
	<td align="right">298,025</td>
	<td align="right">0.01</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	Windows 95		
	</td>
	<td align="right">9</td>
	<td align="right">0.01</td>
	<td align="right">587,592</td>
	<td align="right">0.01</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">10</th>
	<td>
	Windows Me		
	</td>
	<td align="right">8</td>
	<td align="right">0.01</td>
	<td align="right">459,099</td>
	<td align="right">0.01</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>

</tbody>
</table>

