


<h2>Statistics for http://www.u5645470.fsdata.se, Week of 2019-11-18 to 2019-11-24</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Traffic by Hour</h3>
<p>Figures are averages for that hour
	of each day in the time period analyzed.</p>
<img src="h.png" width="468" height="351">
<br /><br />
<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Hour</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	<th scope='col'>Bits per Sec</th>
	<th scope='col'>Bytes per Sec</th>
	
	</tr>
</thead>
<tbody>
<tr class="jamnudda">
	<th align="right">00:00</th>
	<td align="right">316.57</td>
	<td align="right">2.71</td>
	<td align="right">16,059,224.43</td>
	<td align="right">2.20</td>
	<td align="right">9.77</td>
	<td align="right">1.73</td>
	<td align="right">35,687.17</td>
	<td align="right">4,460.90</td>
	
</tr>
<tr class="jamn">
	<th align="right">01:00</th>
	<td align="right">266.86</td>
	<td align="right">2.28</td>
	<td align="right">7,763,975.86</td>
	<td align="right">1.06</td>
	<td align="right">15.54</td>
	<td align="right">2.76</td>
	<td align="right">17,253.28</td>
	<td align="right">2,156.66</td>
	
</tr>
<tr class="udda">
	<th align="right">02:00</th>
	<td align="right">290.00</td>
	<td align="right">2.48</td>
	<td align="right">7,068,709.29</td>
	<td align="right">0.97</td>
	<td align="right">8.81</td>
	<td align="right">1.56</td>
	<td align="right">15,708.24</td>
	<td align="right">1,963.53</td>
	
</tr>
<tr class="jamn">
	<th align="right">03:00</th>
	<td align="right">228.14</td>
	<td align="right">1.95</td>
	<td align="right">2,933,538.14</td>
	<td align="right">0.40</td>
	<td align="right">10.33</td>
	<td align="right">1.83</td>
	<td align="right">6,518.97</td>
	<td align="right">814.87</td>
	
</tr>
<tr class="udda">
	<th align="right">04:00</th>
	<td align="right">239.29</td>
	<td align="right">2.04</td>
	<td align="right">5,735,134.00</td>
	<td align="right">0.78</td>
	<td align="right">10.08</td>
	<td align="right">1.79</td>
	<td align="right">12,744.74</td>
	<td align="right">1,593.09</td>
	
</tr>
<tr class="jamn">
	<th align="right">05:00</th>
	<td align="right">228.29</td>
	<td align="right">1.95</td>
	<td align="right">7,523,347.57</td>
	<td align="right">1.03</td>
	<td align="right">12.57</td>
	<td align="right">2.23</td>
	<td align="right">16,718.55</td>
	<td align="right">2,089.82</td>
	
</tr>
<tr class="udda">
	<th align="right">06:00</th>
	<td align="right">257.71</td>
	<td align="right">2.20</td>
	<td align="right">7,466,681.14</td>
	<td align="right">1.02</td>
	<td align="right">11.64</td>
	<td align="right">2.06</td>
	<td align="right">16,592.62</td>
	<td align="right">2,074.08</td>
	
</tr>
<tr class="jamn">
	<th align="right">07:00</th>
	<td align="right">319.71</td>
	<td align="right">2.73</td>
	<td align="right">17,256,483.29</td>
	<td align="right">2.36</td>
	<td align="right">16.99</td>
	<td align="right">3.01</td>
	<td align="right">38,347.74</td>
	<td align="right">4,793.47</td>
	
</tr>
<tr class="udda">
	<th align="right">08:00</th>
	<td align="right">411.00</td>
	<td align="right">3.51</td>
	<td align="right">22,589,342.57</td>
	<td align="right">3.09</td>
	<td align="right">18.70</td>
	<td align="right">3.32</td>
	<td align="right">50,198.54</td>
	<td align="right">6,274.82</td>
	
</tr>
<tr class="jamn">
	<th align="right">09:00</th>
	<td align="right">753.57</td>
	<td align="right">6.44</td>
	<td align="right">62,523,529.00</td>
	<td align="right">8.55</td>
	<td align="right">29.61</td>
	<td align="right">5.25</td>
	<td align="right">138,941.18</td>
	<td align="right">17,367.65</td>
	
</tr>
<tr class="udda">
	<th align="right">10:00</th>
	<td align="right">699.29</td>
	<td align="right">5.98</td>
	<td align="right">47,152,478.14</td>
	<td align="right">6.45</td>
	<td align="right">32.64</td>
	<td align="right">5.79</td>
	<td align="right">104,783.28</td>
	<td align="right">13,097.91</td>
	
</tr>
<tr class="jamn">
	<th align="right">11:00</th>
	<td align="right">776.00</td>
	<td align="right">6.63</td>
	<td align="right">66,454,700.57</td>
	<td align="right">9.09</td>
	<td align="right">38.01</td>
	<td align="right">6.74</td>
	<td align="right">147,677.11</td>
	<td align="right">18,459.64</td>
	
</tr>
<tr class="udda">
	<th align="right">12:00</th>
	<td align="right">632.43</td>
	<td align="right">5.40</td>
	<td align="right">44,068,548.57</td>
	<td align="right">6.03</td>
	<td align="right">40.92</td>
	<td align="right">7.26</td>
	<td align="right">97,930.11</td>
	<td align="right">12,241.26</td>
	
</tr>
<tr class="jamn">
	<th align="right">13:00</th>
	<td align="right">758.86</td>
	<td align="right">6.48</td>
	<td align="right">53,154,617.29</td>
	<td align="right">7.27</td>
	<td align="right">31.07</td>
	<td align="right">5.51</td>
	<td align="right">118,121.37</td>
	<td align="right">14,765.17</td>
	
</tr>
<tr class="udda">
	<th align="right">14:00</th>
	<td align="right">648.57</td>
	<td align="right">5.54</td>
	<td align="right">39,571,285.00</td>
	<td align="right">5.41</td>
	<td align="right">29.18</td>
	<td align="right">5.18</td>
	<td align="right">87,936.19</td>
	<td align="right">10,992.02</td>
	
</tr>
<tr class="jamn">
	<th align="right">15:00</th>
	<td align="right">609.71</td>
	<td align="right">5.21</td>
	<td align="right">46,733,268.71</td>
	<td align="right">6.39</td>
	<td align="right">29.91</td>
	<td align="right">5.31</td>
	<td align="right">103,851.71</td>
	<td align="right">12,981.46</td>
	
</tr>
<tr class="udda">
	<th align="right">16:00</th>
	<td align="right">622.71</td>
	<td align="right">5.32</td>
	<td align="right">40,033,308.57</td>
	<td align="right">5.47</td>
	<td align="right">29.73</td>
	<td align="right">5.27</td>
	<td align="right">88,962.91</td>
	<td align="right">11,120.36</td>
	
</tr>
<tr class="jamn">
	<th align="right">17:00</th>
	<td align="right">577.00</td>
	<td align="right">4.93</td>
	<td align="right">37,063,919.57</td>
	<td align="right">5.07</td>
	<td align="right">26.17</td>
	<td align="right">4.64</td>
	<td align="right">82,364.27</td>
	<td align="right">10,295.53</td>
	
</tr>
<tr class="udda">
	<th align="right">18:00</th>
	<td align="right">550.14</td>
	<td align="right">4.70</td>
	<td align="right">40,612,373.29</td>
	<td align="right">5.55</td>
	<td align="right">28.82</td>
	<td align="right">5.11</td>
	<td align="right">90,249.72</td>
	<td align="right">11,281.21</td>
	
</tr>
<tr class="jamn">
	<th align="right">19:00</th>
	<td align="right">497.71</td>
	<td align="right">4.25</td>
	<td align="right">31,172,512.29</td>
	<td align="right">4.26</td>
	<td align="right">25.97</td>
	<td align="right">4.61</td>
	<td align="right">69,272.25</td>
	<td align="right">8,659.03</td>
	
</tr>
<tr class="udda">
	<th align="right">20:00</th>
	<td align="right">500.00</td>
	<td align="right">4.27</td>
	<td align="right">28,443,228.57</td>
	<td align="right">3.89</td>
	<td align="right">22.95</td>
	<td align="right">4.07</td>
	<td align="right">63,207.17</td>
	<td align="right">7,900.90</td>
	
</tr>
<tr class="jamn">
	<th align="right">21:00</th>
	<td align="right">587.86</td>
	<td align="right">5.02</td>
	<td align="right">33,904,471.43</td>
	<td align="right">4.64</td>
	<td align="right">27.35</td>
	<td align="right">4.85</td>
	<td align="right">75,343.27</td>
	<td align="right">9,417.91</td>
	
</tr>
<tr class="udda">
	<th align="right">22:00</th>
	<td align="right">503.71</td>
	<td align="right">4.30</td>
	<td align="right">37,240,620.71</td>
	<td align="right">5.09</td>
	<td align="right">26.33</td>
	<td align="right">4.67</td>
	<td align="right">82,756.93</td>
	<td align="right">10,344.62</td>
	
</tr>
<tr class="jamn">
	<th align="right">23:00</th>
	<td align="right">426.86</td>
	<td align="right">3.65</td>
	<td align="right">28,731,284.29</td>
	<td align="right">3.93</td>
	<td align="right">30.64</td>
	<td align="right">5.44</td>
	<td align="right">63,847.30</td>
	<td align="right">7,980.91</td>
	
</tr>

</tbody>
</table>

