


<h2>Statistics for http://www.u5645470.fsdata.se, Week of 2019-11-25 to 2019-12-1</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Traffic by Day</h3>
<img src="d.png" width="468" height="351">
<br /><br />


<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Date</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	<th scope='col'>Bits per Sec</th>
	<th scope='col'>Bytes per Sec</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th align="right">2019-11-25</th>
	<td align="right">14,151	</td>
	<td align="right">16.49	</td>
	<td align="right">960,536,515	</td>
	<td align="right">18.23	</td>
	<td align="right">618	</td>
	<td align="right">15.94	</td>
	<td align="right">88,938.57	</td>
	<td align="right">11,117.32	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-11-26</th>
	<td align="right">13,630	</td>
	<td align="right">15.88	</td>
	<td align="right">920,598,943	</td>
	<td align="right">17.47	</td>
	<td align="right">560	</td>
	<td align="right">14.44	</td>
	<td align="right">85,240.64	</td>
	<td align="right">10,655.08	</td>
</tr>
<tr class="udda">
	<th align="right">2019-11-27</th>
	<td align="right">14,709	</td>
	<td align="right">17.14	</td>
	<td align="right">919,788,861	</td>
	<td align="right">17.45	</td>
	<td align="right">652	</td>
	<td align="right">16.81	</td>
	<td align="right">85,165.64	</td>
	<td align="right">10,645.70	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-11-28</th>
	<td align="right">12,230	</td>
	<td align="right">14.25	</td>
	<td align="right">724,785,409	</td>
	<td align="right">13.75	</td>
	<td align="right">594	</td>
	<td align="right">15.32	</td>
	<td align="right">67,109.76	</td>
	<td align="right">8,388.72	</td>
</tr>
<tr class="udda">
	<th align="right">2019-11-29</th>
	<td align="right">10,534	</td>
	<td align="right">12.28	</td>
	<td align="right">610,602,340	</td>
	<td align="right">11.59	</td>
	<td align="right">474	</td>
	<td align="right">12.22	</td>
	<td align="right">56,537.25	</td>
	<td align="right">7,067.16	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-11-30</th>
	<td align="right">9,372	</td>
	<td align="right">10.92	</td>
	<td align="right">497,133,870	</td>
	<td align="right">9.43	</td>
	<td align="right">462	</td>
	<td align="right">11.91	</td>
	<td align="right">46,030.91	</td>
	<td align="right">5,753.86	</td>
</tr>
<tr class="udda">
	<th align="right">2019-12-1</th>
	<td align="right">11,181	</td>
	<td align="right">13.03	</td>
	<td align="right">636,151,817	</td>
	<td align="right">12.07	</td>
	<td align="right">518	</td>
	<td align="right">13.36	</td>
	<td align="right">58,902.95	</td>
	<td align="right">7,362.87	</td>
</tr>

</tbody>
</table>

