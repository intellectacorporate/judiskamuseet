


<h2>Statistics for http://www.u5645470.fsdata.se, Week of 2019-11-25 to 2019-12-1</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Traffic by Hour</h3>
<p>Figures are averages for that hour
	of each day in the time period analyzed.</p>
<img src="h.png" width="468" height="351">
<br /><br />
<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Hour</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	<th scope='col'>Bits per Sec</th>
	<th scope='col'>Bytes per Sec</th>
	
	</tr>
</thead>
<tbody>
<tr class="jamnudda">
	<th align="right">00:00</th>
	<td align="right">330.43</td>
	<td align="right">2.70</td>
	<td align="right">12,669,357.71</td>
	<td align="right">1.68</td>
	<td align="right">7.17</td>
	<td align="right">1.29</td>
	<td align="right">28,154.13</td>
	<td align="right">3,519.27</td>
	
</tr>
<tr class="jamn">
	<th align="right">01:00</th>
	<td align="right">330.57</td>
	<td align="right">2.70</td>
	<td align="right">15,355,621.00</td>
	<td align="right">2.04</td>
	<td align="right">11.14</td>
	<td align="right">2.01</td>
	<td align="right">34,123.60</td>
	<td align="right">4,265.45</td>
	
</tr>
<tr class="udda">
	<th align="right">02:00</th>
	<td align="right">344.29</td>
	<td align="right">2.81</td>
	<td align="right">10,182,346.86</td>
	<td align="right">1.35</td>
	<td align="right">12.35</td>
	<td align="right">2.23</td>
	<td align="right">22,627.44</td>
	<td align="right">2,828.43</td>
	
</tr>
<tr class="jamn">
	<th align="right">03:00</th>
	<td align="right">322.00</td>
	<td align="right">2.63</td>
	<td align="right">4,787,775.57</td>
	<td align="right">0.64</td>
	<td align="right">12.61</td>
	<td align="right">2.28</td>
	<td align="right">10,639.50</td>
	<td align="right">1,329.94</td>
	
</tr>
<tr class="udda">
	<th align="right">04:00</th>
	<td align="right">305.29</td>
	<td align="right">2.49</td>
	<td align="right">8,663,922.00</td>
	<td align="right">1.15</td>
	<td align="right">10.89</td>
	<td align="right">1.97</td>
	<td align="right">19,253.16</td>
	<td align="right">2,406.64</td>
	
</tr>
<tr class="jamn">
	<th align="right">05:00</th>
	<td align="right">280.00</td>
	<td align="right">2.28</td>
	<td align="right">6,068,281.14</td>
	<td align="right">0.81</td>
	<td align="right">13.01</td>
	<td align="right">2.35</td>
	<td align="right">13,485.07</td>
	<td align="right">1,685.63</td>
	
</tr>
<tr class="udda">
	<th align="right">06:00</th>
	<td align="right">304.86</td>
	<td align="right">2.49</td>
	<td align="right">4,507,838.71</td>
	<td align="right">0.60</td>
	<td align="right">10.98</td>
	<td align="right">1.98</td>
	<td align="right">10,017.42</td>
	<td align="right">1,252.18</td>
	
</tr>
<tr class="jamn">
	<th align="right">07:00</th>
	<td align="right">352.86</td>
	<td align="right">2.88</td>
	<td align="right">25,532,893.14</td>
	<td align="right">3.39</td>
	<td align="right">13.26</td>
	<td align="right">2.39</td>
	<td align="right">56,739.76</td>
	<td align="right">7,092.47</td>
	
</tr>
<tr class="udda">
	<th align="right">08:00</th>
	<td align="right">490.71</td>
	<td align="right">4.00</td>
	<td align="right">33,588,022.86</td>
	<td align="right">4.46</td>
	<td align="right">20.71</td>
	<td align="right">3.74</td>
	<td align="right">74,640.05</td>
	<td align="right">9,330.01</td>
	
</tr>
<tr class="jamn">
	<th align="right">09:00</th>
	<td align="right">570.00</td>
	<td align="right">4.65</td>
	<td align="right">35,401,212.43</td>
	<td align="right">4.70</td>
	<td align="right">30.77</td>
	<td align="right">5.55</td>
	<td align="right">78,669.36</td>
	<td align="right">9,833.67</td>
	
</tr>
<tr class="udda">
	<th align="right">10:00</th>
	<td align="right">888.57</td>
	<td align="right">7.25</td>
	<td align="right">70,220,005.57</td>
	<td align="right">9.33</td>
	<td align="right">31.23</td>
	<td align="right">5.64</td>
	<td align="right">156,044.46</td>
	<td align="right">19,505.56</td>
	
</tr>
<tr class="jamn">
	<th align="right">11:00</th>
	<td align="right">612.43</td>
	<td align="right">5.00</td>
	<td align="right">45,810,037.00</td>
	<td align="right">6.09</td>
	<td align="right">34.17</td>
	<td align="right">6.17</td>
	<td align="right">101,800.08</td>
	<td align="right">12,725.01</td>
	
</tr>
<tr class="udda">
	<th align="right">12:00</th>
	<td align="right">694.71</td>
	<td align="right">5.67</td>
	<td align="right">52,593,313.29</td>
	<td align="right">6.99</td>
	<td align="right">28.84</td>
	<td align="right">5.21</td>
	<td align="right">116,874.03</td>
	<td align="right">14,609.25</td>
	
</tr>
<tr class="jamn">
	<th align="right">13:00</th>
	<td align="right">750.86</td>
	<td align="right">6.13</td>
	<td align="right">58,214,165.29</td>
	<td align="right">7.73</td>
	<td align="right">39.01</td>
	<td align="right">7.04</td>
	<td align="right">129,364.81</td>
	<td align="right">16,170.60</td>
	
</tr>
<tr class="udda">
	<th align="right">14:00</th>
	<td align="right">632.00</td>
	<td align="right">5.16</td>
	<td align="right">44,570,969.71</td>
	<td align="right">5.92</td>
	<td align="right">33.46</td>
	<td align="right">6.04</td>
	<td align="right">99,046.60</td>
	<td align="right">12,380.82</td>
	
</tr>
<tr class="jamn">
	<th align="right">15:00</th>
	<td align="right">560.43</td>
	<td align="right">4.57</td>
	<td align="right">36,155,193.86</td>
	<td align="right">4.80</td>
	<td align="right">26.38</td>
	<td align="right">4.76</td>
	<td align="right">80,344.88</td>
	<td align="right">10,043.11</td>
	
</tr>
<tr class="udda">
	<th align="right">16:00</th>
	<td align="right">640.71</td>
	<td align="right">5.23</td>
	<td align="right">40,632,851.57</td>
	<td align="right">5.40</td>
	<td align="right">25.61</td>
	<td align="right">4.62</td>
	<td align="right">90,295.23</td>
	<td align="right">11,286.90</td>
	
</tr>
<tr class="jamn">
	<th align="right">17:00</th>
	<td align="right">507.86</td>
	<td align="right">4.14</td>
	<td align="right">32,272,899.43</td>
	<td align="right">4.29</td>
	<td align="right">26.52</td>
	<td align="right">4.79</td>
	<td align="right">71,717.55</td>
	<td align="right">8,964.69</td>
	
</tr>
<tr class="udda">
	<th align="right">18:00</th>
	<td align="right">577.29</td>
	<td align="right">4.71</td>
	<td align="right">32,544,704.43</td>
	<td align="right">4.32</td>
	<td align="right">25.89</td>
	<td align="right">4.67</td>
	<td align="right">72,321.57</td>
	<td align="right">9,040.20</td>
	
</tr>
<tr class="jamn">
	<th align="right">19:00</th>
	<td align="right">578.43</td>
	<td align="right">4.72</td>
	<td align="right">37,896,175.14</td>
	<td align="right">5.03</td>
	<td align="right">27.75</td>
	<td align="right">5.01</td>
	<td align="right">84,213.72</td>
	<td align="right">10,526.72</td>
	
</tr>
<tr class="udda">
	<th align="right">20:00</th>
	<td align="right">612.43</td>
	<td align="right">5.00</td>
	<td align="right">39,516,629.86</td>
	<td align="right">5.25</td>
	<td align="right">30.37</td>
	<td align="right">5.48</td>
	<td align="right">87,814.73</td>
	<td align="right">10,976.84</td>
	
</tr>
<tr class="jamn">
	<th align="right">21:00</th>
	<td align="right">561.00</td>
	<td align="right">4.58</td>
	<td align="right">44,415,203.86</td>
	<td align="right">5.90</td>
	<td align="right">24.05</td>
	<td align="right">4.34</td>
	<td align="right">98,700.45</td>
	<td align="right">12,337.56</td>
	
</tr>
<tr class="udda">
	<th align="right">22:00</th>
	<td align="right">545.71</td>
	<td align="right">4.45</td>
	<td align="right">38,999,833.29</td>
	<td align="right">5.18</td>
	<td align="right">25.54</td>
	<td align="right">4.61</td>
	<td align="right">86,666.30</td>
	<td align="right">10,833.29</td>
	
</tr>
<tr class="jamn">
	<th align="right">23:00</th>
	<td align="right">464.71</td>
	<td align="right">3.79</td>
	<td align="right">22,200,425.57</td>
	<td align="right">2.95</td>
	<td align="right">32.28</td>
	<td align="right">5.83</td>
	<td align="right">49,334.28</td>
	<td align="right">6,166.78</td>
	
</tr>

</tbody>
</table>

