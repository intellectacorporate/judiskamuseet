


<h2>Statistics for http://www.u5645470.fsdata.se, Week of 2019-11-25 to 2019-12-1</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 20 of 3,893 Visitor Sites,
Sorted by Access Count

</h3>
<p></p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Site</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="jamnudda">
	<th class="hogerstalld">1</th>
	<td>
	94.247.174.83		
	</td>
	<td align="right">2,950</td>
	<td align="right">3.44</td>
	<td align="right">25,422,517</td>
	<td align="right">0.48</td>
	<td align="right">7</td>
	<td align="right">0.20</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	83.170.113.210		
	</td>
	<td align="right">2,660</td>
	<td align="right">3.10</td>
	<td align="right">23,045,150</td>
	<td align="right">0.44</td>
	<td align="right">7</td>
	<td align="right">0.20</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	185.93.3.92		
	</td>
	<td align="right">2,616</td>
	<td align="right">3.05</td>
	<td align="right">22,700,653</td>
	<td align="right">0.43</td>
	<td align="right">6</td>
	<td align="right">0.18</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	46.246.122.10		
	</td>
	<td align="right">2,120</td>
	<td align="right">2.47</td>
	<td align="right">18,134,978</td>
	<td align="right">0.34</td>
	<td align="right">5</td>
	<td align="right">0.15</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	85.195.116.134		
	</td>
	<td align="right">1,936</td>
	<td align="right">2.26</td>
	<td align="right">16,267,043</td>
	<td align="right">0.31</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	185.39.146.215		
	</td>
	<td align="right">1,935</td>
	<td align="right">2.26</td>
	<td align="right">16,331,827</td>
	<td align="right">0.31</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	185.136.156.82		
	</td>
	<td align="right">1,921</td>
	<td align="right">2.24</td>
	<td align="right">16,299,845</td>
	<td align="right">0.31</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	94.75.211.74		
	</td>
	<td align="right">1,634</td>
	<td align="right">1.90</td>
	<td align="right">13,674,194</td>
	<td align="right">0.26</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	193.183.253.33		
	</td>
	<td align="right">1,540</td>
	<td align="right">1.79</td>
	<td align="right">114,857,694</td>
	<td align="right">2.18</td>
	<td align="right">25</td>
	<td align="right">0.66</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">10</th>
	<td>
	82.103.139.165		
	</td>
	<td align="right">1,490</td>
	<td align="right">1.74</td>
	<td align="right">12,478,308</td>
	<td align="right">0.24</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">11</th>
	<td>
	82.103.145.126		
	</td>
	<td align="right">1,305</td>
	<td align="right">1.52</td>
	<td align="right">10,767,099</td>
	<td align="right">0.20</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">12</th>
	<td>
	46.59.25.88		
	</td>
	<td align="right">1,293</td>
	<td align="right">1.51</td>
	<td align="right">103,872,537</td>
	<td align="right">1.97</td>
	<td align="right">39</td>
	<td align="right">1.02</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">13</th>
	<td>
	63.143.42.242		
	</td>
	<td align="right">1,287</td>
	<td align="right">1.50</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">14</th>
	<td>
	185.246.208.82		
	</td>
	<td align="right">1,126</td>
	<td align="right">1.31</td>
	<td align="right">9,939,729</td>
	<td align="right">0.19</td>
	<td align="right">3</td>
	<td align="right">0.09</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">15</th>
	<td>
	89.163.242.206		
	</td>
	<td align="right">1,008</td>
	<td align="right">1.17</td>
	<td align="right">9,142,560</td>
	<td align="right">0.17</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">16</th>
	<td>
	185.39.146.214		
	</td>
	<td align="right">1,008</td>
	<td align="right">1.17</td>
	<td align="right">9,142,560</td>
	<td align="right">0.17</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">17</th>
	<td>
	151.106.52.134		
	</td>
	<td align="right">1,008</td>
	<td align="right">1.17</td>
	<td align="right">9,142,560</td>
	<td align="right">0.17</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">18</th>
	<td>
	82.103.136.16		
	</td>
	<td align="right">1,008</td>
	<td align="right">1.17</td>
	<td align="right">9,142,560</td>
	<td align="right">0.17</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">19</th>
	<td>
	94.75.211.73		
	</td>
	<td align="right">843</td>
	<td align="right">0.98</td>
	<td align="right">7,646,010</td>
	<td align="right">0.15</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">20</th>
	<td>
	185.180.12.65		
	</td>
	<td align="right">779</td>
	<td align="right">0.91</td>
	<td align="right">7,065,530</td>
	<td align="right">0.13</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>

</tbody>
</table>

