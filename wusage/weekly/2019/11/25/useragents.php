


<h2>Statistics for http://www.u5645470.fsdata.se, Week of 2019-11-25 to 2019-12-1</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 10 of 227 Browsers (User Agents),
Sorted by Access Count

</h3>
<p></p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Product</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th class="hogerstalld">1</th>
	<td>
	Pingdom.com_bot_version_1.4_		
	</td>
	<td align="right">29,391</td>
	<td align="right">34.35</td>
	<td align="right">254,330,033</td>
	<td align="right">4.85</td>
	<td align="right">34</td>
	<td align="right">0.88</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	Safari/537.36		
	</td>
	<td align="right">20,417</td>
	<td align="right">23.86</td>
	<td align="right">1,898,589,821</td>
	<td align="right">36.23</td>
	<td align="right">1,089</td>
	<td align="right">28.24</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	Safari/604.1		
	</td>
	<td align="right">9,860</td>
	<td align="right">11.52</td>
	<td align="right">992,310,412</td>
	<td align="right">18.93</td>
	<td align="right">540</td>
	<td align="right">13.99</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	Mozilla/5.0 		
	</td>
	<td align="right">6,042</td>
	<td align="right">7.06</td>
	<td align="right">404,995,050</td>
	<td align="right">7.73</td>
	<td align="right">603</td>
	<td align="right">15.65</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	Safari/605.1.15		
	</td>
	<td align="right">4,046</td>
	<td align="right">4.73</td>
	<td align="right">393,209,435</td>
	<td align="right">7.50</td>
	<td align="right">227</td>
	<td align="right">5.89</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	Safari/537.36 Edge/18.18362		
	</td>
	<td align="right">1,668</td>
	<td align="right">1.95</td>
	<td align="right">230,107,012</td>
	<td align="right">4.39</td>
	<td align="right">66</td>
	<td align="right">1.72</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	Safari/605.1		
	</td>
	<td align="right">1,340</td>
	<td align="right">1.57</td>
	<td align="right">191,732,850</td>
	<td align="right">3.66</td>
	<td align="right">69</td>
	<td align="right">1.80</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	curl/7.54.0		
	</td>
	<td align="right">1,302</td>
	<td align="right">1.52</td>
	<td align="right">14,272,457</td>
	<td align="right">0.27</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	UptimeRobot/2.0		
	</td>
	<td align="right">1,287</td>
	<td align="right">1.50</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">10</th>
	<td>
	bingbot/2.0		
	</td>
	<td align="right">1,152</td>
	<td align="right">1.35</td>
	<td align="right">80,230,862</td>
	<td align="right">1.53</td>
	<td align="right">279</td>
	<td align="right">7.24</td>
</tr>

</tbody>
</table>

