


<h2>Statistics for http://www.u5645470.fsdata.se, Week of 2019-11-25 to 2019-12-1</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 10 of 25 File Types (Extensions), 
Sorted by Access Count

</h3>
<p>Individual file types as determined by file extensions. All URLs that
do not contain an extension are counted as directories.</p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Type</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th class="hogerstalld">1</th>
	<td>
	Directory (folder)		
	</td>
	<td align="right">33,382</td>
	<td align="right">38.90</td>
	<td align="right">326,464,450</td>
	<td align="right">6.20</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	png		
	</td>
	<td align="right">18,430</td>
	<td align="right">21.48</td>
	<td align="right">278,051,224</td>
	<td align="right">5.28</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	jpg		
	</td>
	<td align="right">12,689</td>
	<td align="right">14.79</td>
	<td align="right">3,974,236,721</td>
	<td align="right">75.42</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	js		
	</td>
	<td align="right">12,293</td>
	<td align="right">14.33</td>
	<td align="right">250,644,548</td>
	<td align="right">4.76</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	css		
	</td>
	<td align="right">4,514</td>
	<td align="right">5.26</td>
	<td align="right">272,488,576</td>
	<td align="right">5.17</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	php		
	</td>
	<td align="right">1,969</td>
	<td align="right">2.29</td>
	<td align="right">12,987,061</td>
	<td align="right">0.25</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	ttf		
	</td>
	<td align="right">1,636</td>
	<td align="right">1.91</td>
	<td align="right">61,604,252</td>
	<td align="right">1.17</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	txt		
	</td>
	<td align="right">466</td>
	<td align="right">0.54</td>
	<td align="right">30,782</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	sql		
	</td>
	<td align="right">134</td>
	<td align="right">0.16</td>
	<td align="right">2,620</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">10</th>
	<td>
	jpeg		
	</td>
	<td align="right">118</td>
	<td align="right">0.14</td>
	<td align="right">51,737,341</td>
	<td align="right">0.98</td>
</tr>

</tbody>
</table>

