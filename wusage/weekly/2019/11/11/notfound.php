


<h2>Statistics for http://www.u5645470.fsdata.se, Week of 2019-11-11 to 2019-11-17</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 10 of 163 Documents Not Found,
Sorted by Access Count

</h3>
<p></p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Document</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th class="hogerstalld">1</th>
	<td>
	/wp-json/oembed/1.0/embed
	</td>
	<td align="right">175</td>
	<td align="right">27.78</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	/
	</td>
	<td align="right">68</td>
	<td align="right">10.79</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	/wp-content/themes/Divi/js/html5.js
	</td>
	<td align="right">37</td>
	<td align="right">5.87</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	/wp-content/uploads/2017/09/tefillin-med-påse.jpg
	</td>
	<td align="right">21</td>
	<td align="right">3.33</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	/autodiscover/autodiscover.xml
	</td>
	<td align="right">20</td>
	<td align="right">3.17</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	/wp-content/cache/minify/smush-lazy-load.min.js.map
	</td>
	<td align="right">16</td>
	<td align="right">2.54</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	/wp-content/cache/minify/forms-api.min.js.map
	</td>
	<td align="right">16</td>
	<td align="right">2.54</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	/wp-content/uploads/2017/09/synagogan-på-själagårdsgatan-19.jpg
	</td>
	<td align="right">14</td>
	<td align="right">2.22</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	/ads.txt
	</td>
	<td align="right">14</td>
	<td align="right">2.22</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">10</th>
	<td>
	/tracesofexistence
	</td>
	<td align="right">8</td>
	<td align="right">1.27</td>
</tr>

</tbody>
</table>

