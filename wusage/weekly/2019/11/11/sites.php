


<h2>Statistics for http://www.u5645470.fsdata.se, Week of 2019-11-11 to 2019-11-17</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 20 of 4,103 Visitor Sites,
Sorted by Access Count

</h3>
<p></p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Site</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="jamnudda">
	<th class="hogerstalld">1</th>
	<td>
	46.59.25.88		
	</td>
	<td align="right">2,664</td>
	<td align="right">3.38</td>
	<td align="right">132,608,957</td>
	<td align="right">2.36</td>
	<td align="right">52</td>
	<td align="right">1.30</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	95.141.32.46		
	</td>
	<td align="right">1,943</td>
	<td align="right">2.47</td>
	<td align="right">16,562,578</td>
	<td align="right">0.30</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	94.247.174.83		
	</td>
	<td align="right">1,939</td>
	<td align="right">2.46</td>
	<td align="right">16,497,229</td>
	<td align="right">0.29</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	85.195.116.134		
	</td>
	<td align="right">1,925</td>
	<td align="right">2.44</td>
	<td align="right">16,545,914</td>
	<td align="right">0.29</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	83.170.113.210		
	</td>
	<td align="right">1,925</td>
	<td align="right">2.44</td>
	<td align="right">16,496,956</td>
	<td align="right">0.29</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	185.39.146.215		
	</td>
	<td align="right">1,924</td>
	<td align="right">2.44</td>
	<td align="right">16,480,502</td>
	<td align="right">0.29</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	185.136.156.82		
	</td>
	<td align="right">1,915</td>
	<td align="right">2.43</td>
	<td align="right">16,562,501</td>
	<td align="right">0.30</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	94.75.211.74		
	</td>
	<td align="right">1,906</td>
	<td align="right">2.42</td>
	<td align="right">16,496,253</td>
	<td align="right">0.29</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	46.20.45.18		
	</td>
	<td align="right">1,901</td>
	<td align="right">2.41</td>
	<td align="right">16,480,068</td>
	<td align="right">0.29</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">10</th>
	<td>
	63.143.42.242		
	</td>
	<td align="right">1,257</td>
	<td align="right">1.60</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">11</th>
	<td>
	178.255.153.2		
	</td>
	<td align="right">1,225</td>
	<td align="right">1.55</td>
	<td align="right">10,506,101</td>
	<td align="right">0.19</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">12</th>
	<td>
	185.152.65.167		
	</td>
	<td align="right">966</td>
	<td align="right">1.23</td>
	<td align="right">8,295,749</td>
	<td align="right">0.15</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">13</th>
	<td>
	185.246.208.82		
	</td>
	<td align="right">948</td>
	<td align="right">1.20</td>
	<td align="right">8,217,055</td>
	<td align="right">0.15</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">14</th>
	<td>
	81.17.62.205		
	</td>
	<td align="right">705</td>
	<td align="right">0.89</td>
	<td align="right">6,023,836</td>
	<td align="right">0.11</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">15</th>
	<td>
	194.68.82.234		
	</td>
	<td align="right">342</td>
	<td align="right">0.43</td>
	<td align="right">14,788,324</td>
	<td align="right">0.26</td>
	<td align="right">8</td>
	<td align="right">0.22</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">16</th>
	<td>
	83.241.181.2		
	</td>
	<td align="right">227</td>
	<td align="right">0.29</td>
	<td align="right">20,556,217</td>
	<td align="right">0.37</td>
	<td align="right">8</td>
	<td align="right">0.22</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">17</th>
	<td>
	193.180.154.229		
	</td>
	<td align="right">220</td>
	<td align="right">0.28</td>
	<td align="right">17,004,786</td>
	<td align="right">0.30</td>
	<td align="right">6</td>
	<td align="right">0.16</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">18</th>
	<td>
	90.229.237.5		
	</td>
	<td align="right">192</td>
	<td align="right">0.24</td>
	<td align="right">7,662,947</td>
	<td align="right">0.14</td>
	<td align="right">1</td>
	<td align="right">0.03</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">19</th>
	<td>
	66.249.64.174		
	</td>
	<td align="right">188</td>
	<td align="right">0.24</td>
	<td align="right">3,505,207</td>
	<td align="right">0.06</td>
	<td align="right">28</td>
	<td align="right">0.70</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">20</th>
	<td>
	195.181.242.138		
	</td>
	<td align="right">185</td>
	<td align="right">0.23</td>
	<td align="right">1,780,662</td>
	<td align="right">0.03</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>

</tbody>
</table>

