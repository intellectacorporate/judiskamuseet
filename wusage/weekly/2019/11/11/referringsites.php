


<h2>Statistics for http://www.u5645470.fsdata.se, Week of 2019-11-11 to 2019-11-17</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 20 of 174 Referring Sites,
Sorted by Access Count

</h3>
<p></p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Referring Site</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th class="hogerstalld">1</th>
	<td>
	<a href="https://judiskamuseet.se">
	https://judiskamuseet.se</a>
	</td>
	<td align="right">41,552</td>
	<td align="right">90.23</td>
	<td align="right">4,783,719,206</td>
	<td align="right">96.52</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	<a href="http://judiskamuseet.se">
	http://judiskamuseet.se</a>
	</td>
	<td align="right">1,921</td>
	<td align="right">4.17</td>
	<td align="right">47,987,152</td>
	<td align="right">0.97</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	<a href="https://www.google.com">
	https://www.google.com</a>
	</td>
	<td align="right">1,305</td>
	<td align="right">2.83</td>
	<td align="right">72,714,593</td>
	<td align="right">1.47</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	<a href="https://www.google.se">
	https://www.google.se</a>
	</td>
	<td align="right">589</td>
	<td align="right">1.28</td>
	<td align="right">30,592,092</td>
	<td align="right">0.62</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	<a href="http://www.google.com">
	http://www.google.com</a>
	</td>
	<td align="right">71</td>
	<td align="right">0.15</td>
	<td align="right">600,817</td>
	<td align="right">0.01</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	<a href="http://m.facebook.com">
	http://m.facebook.com</a>
	</td>
	<td align="right">56</td>
	<td align="right">0.12</td>
	<td align="right">683,783</td>
	<td align="right">0.01</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	<a href="https://www.bing.com">
	https://www.bing.com</a>
	</td>
	<td align="right">54</td>
	<td align="right">0.12</td>
	<td align="right">7,229,414</td>
	<td align="right">0.15</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	<a href="http://www.judiska-museet.se">
	http://www.judiska-museet.se</a>
	</td>
	<td align="right">34</td>
	<td align="right">0.07</td>
	<td align="right">180</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	<a href="https://classroom.google.com">
	https://classroom.google.com</a>
	</td>
	<td align="right">29</td>
	<td align="right">0.06</td>
	<td align="right">345,720</td>
	<td align="right">0.01</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">10</th>
	<td>
	<a href="https://translate.googleusercontent.com">
	https://translate.googleusercontent.com</a>
	</td>
	<td align="right">15</td>
	<td align="right">0.03</td>
	<td align="right">368,751</td>
	<td align="right">0.01</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">11</th>
	<td>
	<a href="https://myactivity.google.com">
	https://myactivity.google.com</a>
	</td>
	<td align="right">14</td>
	<td align="right">0.03</td>
	<td align="right">2,227,329</td>
	<td align="right">0.04</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">12</th>
	<td>
	<a href="https://zvuqa.net">
	https://zvuqa.net</a>
	</td>
	<td align="right">13</td>
	<td align="right">0.03</td>
	<td align="right">770,652</td>
	<td align="right">0.02</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">13</th>
	<td>
	<a href="https://www.facebook.com">
	https://www.facebook.com</a>
	</td>
	<td align="right">9</td>
	<td align="right">0.02</td>
	<td align="right">111,616</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">14</th>
	<td>
	<a href="https://r.search.yahoo.com">
	https://r.search.yahoo.com</a>
	</td>
	<td align="right">8</td>
	<td align="right">0.02</td>
	<td align="right">115,053</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">15</th>
	<td>
	<a href="https://l.facebook.com">
	https://l.facebook.com</a>
	</td>
	<td align="right">8</td>
	<td align="right">0.02</td>
	<td align="right">63,793</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">16</th>
	<td>
	<a href="https://89.221.250.27:443">
	https://89.221.250.27:443</a>
	</td>
	<td align="right">8</td>
	<td align="right">0.02</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">17</th>
	<td>
	<a href="http://blogg.slaktingar.se">
	http://blogg.slaktingar.se</a>
	</td>
	<td align="right">7</td>
	<td align="right">0.02</td>
	<td align="right">49,386</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">18</th>
	<td>
	<a href="https://pansionat-evropa.com.ua">
	https://pansionat-evropa.com.ua</a>
	</td>
	<td align="right">7</td>
	<td align="right">0.02</td>
	<td align="right">391,728</td>
	<td align="right">0.01</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">19</th>
	<td>
	<a href="https://www.google.de">
	https://www.google.de</a>
	</td>
	<td align="right">7</td>
	<td align="right">0.02</td>
	<td align="right">384,314</td>
	<td align="right">0.01</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">20</th>
	<td>
	<a href="https://l.instagram.com">
	https://l.instagram.com</a>
	</td>
	<td align="right">7</td>
	<td align="right">0.02</td>
	<td align="right">79,504</td>
	<td align="right">0.00</td>
</tr>

</tbody>
</table>

