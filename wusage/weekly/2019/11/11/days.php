


<h2>Statistics for http://www.u5645470.fsdata.se, Week of 2019-11-11 to 2019-11-17</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Traffic by Day</h3>
<img src="d.png" width="468" height="351">
<br /><br />


<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Date</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	<th scope='col'>Bits per Sec</th>
	<th scope='col'>Bytes per Sec</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th align="right">2019-11-11</th>
	<td align="right">12,899	</td>
	<td align="right">16.37	</td>
	<td align="right">861,938,220	</td>
	<td align="right">15.36	</td>
	<td align="right">535	</td>
	<td align="right">13.33	</td>
	<td align="right">79,809.09	</td>
	<td align="right">9,976.14	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-11-12</th>
	<td align="right">11,471	</td>
	<td align="right">14.56	</td>
	<td align="right">896,549,681	</td>
	<td align="right">15.98	</td>
	<td align="right">545	</td>
	<td align="right">13.57	</td>
	<td align="right">83,013.86	</td>
	<td align="right">10,376.73	</td>
</tr>
<tr class="udda">
	<th align="right">2019-11-13</th>
	<td align="right">11,545	</td>
	<td align="right">14.65	</td>
	<td align="right">807,912,367	</td>
	<td align="right">14.40	</td>
	<td align="right">664	</td>
	<td align="right">16.54	</td>
	<td align="right">74,806.70	</td>
	<td align="right">9,350.84	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-11-14</th>
	<td align="right">13,201	</td>
	<td align="right">16.75	</td>
	<td align="right">889,781,685	</td>
	<td align="right">15.86	</td>
	<td align="right">721	</td>
	<td align="right">17.96	</td>
	<td align="right">82,387.19	</td>
	<td align="right">10,298.40	</td>
</tr>
<tr class="udda">
	<th align="right">2019-11-15</th>
	<td align="right">11,145	</td>
	<td align="right">14.14	</td>
	<td align="right">794,748,350	</td>
	<td align="right">14.16	</td>
	<td align="right">559	</td>
	<td align="right">13.92	</td>
	<td align="right">73,587.81	</td>
	<td align="right">9,198.48	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-11-16</th>
	<td align="right">8,970	</td>
	<td align="right">11.38	</td>
	<td align="right">713,101,191	</td>
	<td align="right">12.71	</td>
	<td align="right">416	</td>
	<td align="right">10.36	</td>
	<td align="right">66,027.89	</td>
	<td align="right">8,253.49	</td>
</tr>
<tr class="udda">
	<th align="right">2019-11-17</th>
	<td align="right">9,571	</td>
	<td align="right">12.15	</td>
	<td align="right">647,034,618	</td>
	<td align="right">11.53	</td>
	<td align="right">575	</td>
	<td align="right">14.32	</td>
	<td align="right">59,910.61	</td>
	<td align="right">7,488.83	</td>
</tr>

</tbody>
</table>

