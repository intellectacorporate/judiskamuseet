


<h2>Statistics for http://www.u5645470.fsdata.se, Week of 2019-11-11 to 2019-11-17</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 10 of 214 Browsers (User Agents),
Sorted by Access Count

</h3>
<p></p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Product</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th class="hogerstalld">1</th>
	<td>
	Safari/537.36		
	</td>
	<td align="right">23,867</td>
	<td align="right">30.39</td>
	<td align="right">2,304,297,873</td>
	<td align="right">41.35</td>
	<td align="right">1,141</td>
	<td align="right">28.81</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	Pingdom.com_bot_version_1.4_		
	</td>
	<td align="right">19,217</td>
	<td align="right">24.47</td>
	<td align="right">165,164,737</td>
	<td align="right">2.96</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	Safari/604.1		
	</td>
	<td align="right">10,958</td>
	<td align="right">13.95</td>
	<td align="right">1,242,206,630</td>
	<td align="right">22.29</td>
	<td align="right">605</td>
	<td align="right">15.28</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	Mozilla/5.0 		
	</td>
	<td align="right">6,074</td>
	<td align="right">7.73</td>
	<td align="right">406,675,455</td>
	<td align="right">7.30</td>
	<td align="right">798</td>
	<td align="right">20.15</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	Safari/605.1.15		
	</td>
	<td align="right">3,949</td>
	<td align="right">5.03</td>
	<td align="right">410,862,783</td>
	<td align="right">7.37</td>
	<td align="right">226</td>
	<td align="right">5.71</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	Safari/537.36 Edge/18.18362		
	</td>
	<td align="right">1,591</td>
	<td align="right">2.03</td>
	<td align="right">208,543,878</td>
	<td align="right">3.74</td>
	<td align="right">77</td>
	<td align="right">1.95</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	UptimeRobot/2.0		
	</td>
	<td align="right">1,257</td>
	<td align="right">1.60</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	bingbot/2.0		
	</td>
	<td align="right">1,209</td>
	<td align="right">1.54</td>
	<td align="right">123,388,670</td>
	<td align="right">2.21</td>
	<td align="right">257</td>
	<td align="right">6.49</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	Safari/605.1		
	</td>
	<td align="right">870</td>
	<td align="right">1.11</td>
	<td align="right">96,081,635</td>
	<td align="right">1.72</td>
	<td align="right">46</td>
	<td align="right">1.18</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">10</th>
	<td>
	Safari/537.36 Edge/18.17763		
	</td>
	<td align="right">864</td>
	<td align="right">1.10</td>
	<td align="right">54,341,333</td>
	<td align="right">0.98</td>
	<td align="right">21</td>
	<td align="right">0.55</td>
</tr>

</tbody>
</table>

