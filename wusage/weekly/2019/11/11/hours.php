


<h2>Statistics for http://www.u5645470.fsdata.se, Week of 2019-11-11 to 2019-11-17</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Traffic by Hour</h3>
<p>Figures are averages for that hour
	of each day in the time period analyzed.</p>
<img src="h.png" width="468" height="351">
<br /><br />
<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Hour</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	<th scope='col'>Bits per Sec</th>
	<th scope='col'>Bytes per Sec</th>
	
	</tr>
</thead>
<tbody>
<tr class="jamnudda">
	<th align="right">00:00</th>
	<td align="right">291.57</td>
	<td align="right">2.59</td>
	<td align="right">12,598,201.86</td>
	<td align="right">1.57</td>
	<td align="right">9.94</td>
	<td align="right">1.73</td>
	<td align="right">27,996.00</td>
	<td align="right">3,499.50</td>
	
</tr>
<tr class="jamn">
	<th align="right">01:00</th>
	<td align="right">217.86</td>
	<td align="right">1.94</td>
	<td align="right">6,787,089.57</td>
	<td align="right">0.85</td>
	<td align="right">11.48</td>
	<td align="right">2.00</td>
	<td align="right">15,082.42</td>
	<td align="right">1,885.30</td>
	
</tr>
<tr class="udda">
	<th align="right">02:00</th>
	<td align="right">204.43</td>
	<td align="right">1.82</td>
	<td align="right">5,975,225.29</td>
	<td align="right">0.75</td>
	<td align="right">10.22</td>
	<td align="right">1.78</td>
	<td align="right">13,278.28</td>
	<td align="right">1,659.78</td>
	
</tr>
<tr class="jamn">
	<th align="right">03:00</th>
	<td align="right">209.71</td>
	<td align="right">1.86</td>
	<td align="right">7,375,055.00</td>
	<td align="right">0.92</td>
	<td align="right">10.22</td>
	<td align="right">1.78</td>
	<td align="right">16,389.01</td>
	<td align="right">2,048.63</td>
	
</tr>
<tr class="udda">
	<th align="right">04:00</th>
	<td align="right">204.43</td>
	<td align="right">1.82</td>
	<td align="right">5,769,318.14</td>
	<td align="right">0.72</td>
	<td align="right">13.20</td>
	<td align="right">2.30</td>
	<td align="right">12,820.71</td>
	<td align="right">1,602.59</td>
	
</tr>
<tr class="jamn">
	<th align="right">05:00</th>
	<td align="right">215.43</td>
	<td align="right">1.91</td>
	<td align="right">8,202,466.43</td>
	<td align="right">1.02</td>
	<td align="right">13.02</td>
	<td align="right">2.27</td>
	<td align="right">18,227.70</td>
	<td align="right">2,278.46</td>
	
</tr>
<tr class="udda">
	<th align="right">06:00</th>
	<td align="right">288.00</td>
	<td align="right">2.56</td>
	<td align="right">15,156,692.57</td>
	<td align="right">1.89</td>
	<td align="right">13.80</td>
	<td align="right">2.41</td>
	<td align="right">33,681.54</td>
	<td align="right">4,210.19</td>
	
</tr>
<tr class="jamn">
	<th align="right">07:00</th>
	<td align="right">296.86</td>
	<td align="right">2.64</td>
	<td align="right">13,852,101.00</td>
	<td align="right">1.73</td>
	<td align="right">16.16</td>
	<td align="right">2.82</td>
	<td align="right">30,782.45</td>
	<td align="right">3,847.81</td>
	
</tr>
<tr class="udda">
	<th align="right">08:00</th>
	<td align="right">440.71</td>
	<td align="right">3.91</td>
	<td align="right">35,526,519.71</td>
	<td align="right">4.43</td>
	<td align="right">20.69</td>
	<td align="right">3.61</td>
	<td align="right">78,947.82</td>
	<td align="right">9,868.48</td>
	
</tr>
<tr class="jamn">
	<th align="right">09:00</th>
	<td align="right">634.43</td>
	<td align="right">5.64</td>
	<td align="right">49,380,917.71</td>
	<td align="right">6.16</td>
	<td align="right">33.52</td>
	<td align="right">5.84</td>
	<td align="right">109,735.37</td>
	<td align="right">13,716.92</td>
	
</tr>
<tr class="udda">
	<th align="right">10:00</th>
	<td align="right">637.86</td>
	<td align="right">5.67</td>
	<td align="right">59,801,950.57</td>
	<td align="right">7.46</td>
	<td align="right">29.41</td>
	<td align="right">5.13</td>
	<td align="right">132,893.22</td>
	<td align="right">16,611.65</td>
	
</tr>
<tr class="jamn">
	<th align="right">11:00</th>
	<td align="right">771.14</td>
	<td align="right">6.85</td>
	<td align="right">67,542,599.43</td>
	<td align="right">8.43</td>
	<td align="right">36.34</td>
	<td align="right">6.34</td>
	<td align="right">150,094.67</td>
	<td align="right">18,761.83</td>
	
</tr>
<tr class="udda">
	<th align="right">12:00</th>
	<td align="right">510.57</td>
	<td align="right">4.54</td>
	<td align="right">36,489,273.71</td>
	<td align="right">4.55</td>
	<td align="right">28.64</td>
	<td align="right">4.99</td>
	<td align="right">81,087.27</td>
	<td align="right">10,135.91</td>
	
</tr>
<tr class="jamn">
	<th align="right">13:00</th>
	<td align="right">654.29</td>
	<td align="right">5.81</td>
	<td align="right">54,573,513.29</td>
	<td align="right">6.81</td>
	<td align="right">31.34</td>
	<td align="right">5.46</td>
	<td align="right">121,274.47</td>
	<td align="right">15,159.31</td>
	
</tr>
<tr class="udda">
	<th align="right">14:00</th>
	<td align="right">612.14</td>
	<td align="right">5.44</td>
	<td align="right">45,512,646.43</td>
	<td align="right">5.68</td>
	<td align="right">28.79</td>
	<td align="right">5.02</td>
	<td align="right">101,139.21</td>
	<td align="right">12,642.40</td>
	
</tr>
<tr class="jamn">
	<th align="right">15:00</th>
	<td align="right">725.71</td>
	<td align="right">6.45</td>
	<td align="right">62,385,615.29</td>
	<td align="right">7.78</td>
	<td align="right">29.60</td>
	<td align="right">5.16</td>
	<td align="right">138,634.70</td>
	<td align="right">17,329.34</td>
	
</tr>
<tr class="udda">
	<th align="right">16:00</th>
	<td align="right">721.57</td>
	<td align="right">6.41</td>
	<td align="right">48,490,322.86</td>
	<td align="right">6.05</td>
	<td align="right">27.92</td>
	<td align="right">4.87</td>
	<td align="right">107,756.27</td>
	<td align="right">13,469.53</td>
	
</tr>
<tr class="jamn">
	<th align="right">17:00</th>
	<td align="right">528.57</td>
	<td align="right">4.70</td>
	<td align="right">41,917,436.00</td>
	<td align="right">5.23</td>
	<td align="right">31.27</td>
	<td align="right">5.45</td>
	<td align="right">93,149.86</td>
	<td align="right">11,643.73</td>
	
</tr>
<tr class="udda">
	<th align="right">18:00</th>
	<td align="right">582.29</td>
	<td align="right">5.17</td>
	<td align="right">44,927,670.86</td>
	<td align="right">5.60</td>
	<td align="right">30.52</td>
	<td align="right">5.32</td>
	<td align="right">99,839.27</td>
	<td align="right">12,479.91</td>
	
</tr>
<tr class="jamn">
	<th align="right">19:00</th>
	<td align="right">518.43</td>
	<td align="right">4.61</td>
	<td align="right">30,188,805.57</td>
	<td align="right">3.77</td>
	<td align="right">34.26</td>
	<td align="right">5.97</td>
	<td align="right">67,086.23</td>
	<td align="right">8,385.78</td>
	
</tr>
<tr class="udda">
	<th align="right">20:00</th>
	<td align="right">567.86</td>
	<td align="right">5.04</td>
	<td align="right">45,666,831.57</td>
	<td align="right">5.70</td>
	<td align="right">29.86</td>
	<td align="right">5.21</td>
	<td align="right">101,481.85</td>
	<td align="right">12,685.23</td>
	
</tr>
<tr class="jamn">
	<th align="right">21:00</th>
	<td align="right">568.00</td>
	<td align="right">5.05</td>
	<td align="right">48,258,958.29</td>
	<td align="right">6.02</td>
	<td align="right">29.99</td>
	<td align="right">5.23</td>
	<td align="right">107,242.13</td>
	<td align="right">13,405.27</td>
	
</tr>
<tr class="udda">
	<th align="right">22:00</th>
	<td align="right">493.43</td>
	<td align="right">4.38</td>
	<td align="right">31,501,050.57</td>
	<td align="right">3.93</td>
	<td align="right">26.08</td>
	<td align="right">4.55</td>
	<td align="right">70,002.33</td>
	<td align="right">8,750.29</td>
	
</tr>
<tr class="jamn">
	<th align="right">23:00</th>
	<td align="right">362.14</td>
	<td align="right">3.22</td>
	<td align="right">23,700,611.43</td>
	<td align="right">2.96</td>
	<td align="right">27.32</td>
	<td align="right">4.76</td>
	<td align="right">52,668.03</td>
	<td align="right">6,583.50</td>
	
</tr>

</tbody>
</table>

