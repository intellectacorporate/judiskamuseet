


<h2>Statistics for http://www.u5645470.fsdata.se, Week of 2019-11-11 to 2019-11-17</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 10 of 23 File Types (Extensions), 
Sorted by Access Count

</h3>
<p>Individual file types as determined by file extensions. All URLs that
do not contain an extension are counted as directories.</p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Type</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th class="hogerstalld">1</th>
	<td>
	Directory (folder)		
	</td>
	<td align="right">31,191</td>
	<td align="right">39.58</td>
	<td align="right">289,748,745</td>
	<td align="right">5.16</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	js		
	</td>
	<td align="right">13,608</td>
	<td align="right">17.27</td>
	<td align="right">271,122,071</td>
	<td align="right">4.83</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	jpg		
	</td>
	<td align="right">13,384</td>
	<td align="right">16.98</td>
	<td align="right">3,987,544,589</td>
	<td align="right">71.07</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	png		
	</td>
	<td align="right">9,897</td>
	<td align="right">12.56</td>
	<td align="right">242,969,929</td>
	<td align="right">4.33</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	css		
	</td>
	<td align="right">4,788</td>
	<td align="right">6.08</td>
	<td align="right">288,710,297</td>
	<td align="right">5.15</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	php		
	</td>
	<td align="right">2,779</td>
	<td align="right">3.53</td>
	<td align="right">43,180,927</td>
	<td align="right">0.77</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	ttf		
	</td>
	<td align="right">1,794</td>
	<td align="right">2.28</td>
	<td align="right">67,296,434</td>
	<td align="right">1.20</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	jpeg		
	</td>
	<td align="right">749</td>
	<td align="right">0.95</td>
	<td align="right">364,021,220</td>
	<td align="right">6.49</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	txt		
	</td>
	<td align="right">394</td>
	<td align="right">0.50</td>
	<td align="right">26,071</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">10</th>
	<td>
	ico		
	</td>
	<td align="right">61</td>
	<td align="right">0.08</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>

</tbody>
</table>

