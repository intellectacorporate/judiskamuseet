


<h2>Statistics for http://www.u5645470.fsdata.se, Week of 2019-11-4 to 2019-11-10</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 20 of 3,777 Visitor Sites,
Sorted by Access Count

</h3>
<p></p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Site</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="jamnudda">
	<th class="hogerstalld">1</th>
	<td>
	81.17.62.205		
	</td>
	<td align="right">1,950</td>
	<td align="right">2.53</td>
	<td align="right">16,253,179</td>
	<td align="right">0.29</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	185.136.156.82		
	</td>
	<td align="right">1,945</td>
	<td align="right">2.52</td>
	<td align="right">16,253,074</td>
	<td align="right">0.29</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	85.195.116.134		
	</td>
	<td align="right">1,935</td>
	<td align="right">2.51</td>
	<td align="right">16,236,729</td>
	<td align="right">0.29</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	94.75.211.74		
	</td>
	<td align="right">1,933</td>
	<td align="right">2.51</td>
	<td align="right">16,252,862</td>
	<td align="right">0.29</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	46.20.45.18		
	</td>
	<td align="right">1,931</td>
	<td align="right">2.50</td>
	<td align="right">16,252,803</td>
	<td align="right">0.29</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	83.170.113.210		
	</td>
	<td align="right">1,927</td>
	<td align="right">2.50</td>
	<td align="right">16,252,714</td>
	<td align="right">0.29</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	185.39.146.215		
	</td>
	<td align="right">1,913</td>
	<td align="right">2.48</td>
	<td align="right">16,236,351</td>
	<td align="right">0.29</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	94.247.174.83		
	</td>
	<td align="right">1,554</td>
	<td align="right">2.02</td>
	<td align="right">12,898,515</td>
	<td align="right">0.23</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	82.103.136.16		
	</td>
	<td align="right">1,377</td>
	<td align="right">1.79</td>
	<td align="right">11,724,897</td>
	<td align="right">0.21</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">10</th>
	<td>
	46.59.25.88		
	</td>
	<td align="right">1,295</td>
	<td align="right">1.68</td>
	<td align="right">127,591,487</td>
	<td align="right">2.29</td>
	<td align="right">44</td>
	<td align="right">1.15</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">11</th>
	<td>
	63.143.42.242		
	</td>
	<td align="right">1,218</td>
	<td align="right">1.58</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">12</th>
	<td>
	46.246.122.10		
	</td>
	<td align="right">997</td>
	<td align="right">1.29</td>
	<td align="right">8,451,029</td>
	<td align="right">0.15</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">13</th>
	<td>
	185.152.65.167		
	</td>
	<td align="right">864</td>
	<td align="right">1.12</td>
	<td align="right">7,253,339</td>
	<td align="right">0.13</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">14</th>
	<td>
	90.229.237.5		
	</td>
	<td align="right">841</td>
	<td align="right">1.09</td>
	<td align="right">18,713,121</td>
	<td align="right">0.34</td>
	<td align="right">8</td>
	<td align="right">0.22</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">15</th>
	<td>
	95.141.32.46		
	</td>
	<td align="right">538</td>
	<td align="right">0.70</td>
	<td align="right">4,495,449</td>
	<td align="right">0.08</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">16</th>
	<td>
	82.209.180.94		
	</td>
	<td align="right">477</td>
	<td align="right">0.62</td>
	<td align="right">30,755,110</td>
	<td align="right">0.55</td>
	<td align="right">3</td>
	<td align="right">0.08</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">17</th>
	<td>
	178.255.155.2		
	</td>
	<td align="right">402</td>
	<td align="right">0.52</td>
	<td align="right">3,370,849</td>
	<td align="right">0.06</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">18</th>
	<td>
	194.16.15.135		
	</td>
	<td align="right">326</td>
	<td align="right">0.42</td>
	<td align="right">33,465,126</td>
	<td align="right">0.60</td>
	<td align="right">13</td>
	<td align="right">0.34</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">19</th>
	<td>
	83.249.119.88		
	</td>
	<td align="right">229</td>
	<td align="right">0.30</td>
	<td align="right">24,776,921</td>
	<td align="right">0.44</td>
	<td align="right">8</td>
	<td align="right">0.22</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">20</th>
	<td>
	148.160.250.14		
	</td>
	<td align="right">202</td>
	<td align="right">0.26</td>
	<td align="right">16,037,849</td>
	<td align="right">0.29</td>
	<td align="right">8</td>
	<td align="right">0.22</td>
</tr>

</tbody>
</table>

