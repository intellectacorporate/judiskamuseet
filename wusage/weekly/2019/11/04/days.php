


<h2>Statistics for http://www.u5645470.fsdata.se, Week of 2019-11-4 to 2019-11-10</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Traffic by Day</h3>
<img src="d.png" width="468" height="351">
<br /><br />


<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Date</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	<th scope='col'>Bits per Sec</th>
	<th scope='col'>Bytes per Sec</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th align="right">2019-11-4</th>
	<td align="right">11,254	</td>
	<td align="right">14.60	</td>
	<td align="right">707,213,035	</td>
	<td align="right">12.70	</td>
	<td align="right">599	</td>
	<td align="right">15.41	</td>
	<td align="right">65,482.69	</td>
	<td align="right">8,185.34	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-11-5</th>
	<td align="right">11,956	</td>
	<td align="right">15.51	</td>
	<td align="right">905,179,091	</td>
	<td align="right">16.25	</td>
	<td align="right">571	</td>
	<td align="right">14.69	</td>
	<td align="right">83,812.88	</td>
	<td align="right">10,476.61	</td>
</tr>
<tr class="udda">
	<th align="right">2019-11-6</th>
	<td align="right">11,096	</td>
	<td align="right">14.39	</td>
	<td align="right">913,602,391	</td>
	<td align="right">16.40	</td>
	<td align="right">524	</td>
	<td align="right">13.48	</td>
	<td align="right">84,592.81	</td>
	<td align="right">10,574.10	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-11-7</th>
	<td align="right">11,142	</td>
	<td align="right">14.45	</td>
	<td align="right">714,124,976	</td>
	<td align="right">12.82	</td>
	<td align="right">611	</td>
	<td align="right">15.72	</td>
	<td align="right">66,122.68	</td>
	<td align="right">8,265.34	</td>
</tr>
<tr class="udda">
	<th align="right">2019-11-8</th>
	<td align="right">9,400	</td>
	<td align="right">12.19	</td>
	<td align="right">710,299,175	</td>
	<td align="right">12.75	</td>
	<td align="right">483	</td>
	<td align="right">12.43	</td>
	<td align="right">65,768.44	</td>
	<td align="right">8,221.06	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-11-9</th>
	<td align="right">9,861	</td>
	<td align="right">12.79	</td>
	<td align="right">725,961,188	</td>
	<td align="right">13.03	</td>
	<td align="right">478	</td>
	<td align="right">12.30	</td>
	<td align="right">67,218.63	</td>
	<td align="right">8,402.33	</td>
</tr>
<tr class="udda">
	<th align="right">2019-11-10</th>
	<td align="right">12,397	</td>
	<td align="right">16.08	</td>
	<td align="right">893,099,860	</td>
	<td align="right">16.04	</td>
	<td align="right">620	</td>
	<td align="right">15.95	</td>
	<td align="right">82,694.43	</td>
	<td align="right">10,336.80	</td>
</tr>

</tbody>
</table>

