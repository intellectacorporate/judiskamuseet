


<h2>Statistics for http://www.u5645470.fsdata.se, Week of 2019-11-4 to 2019-11-10</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 10 of 224 Browsers (User Agents),
Sorted by Access Count

</h3>
<p></p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Product</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th class="hogerstalld">1</th>
	<td>
	Safari/537.36		
	</td>
	<td align="right">21,471</td>
	<td align="right">27.91</td>
	<td align="right">2,119,996,145</td>
	<td align="right">38.39</td>
	<td align="right">1,089</td>
	<td align="right">28.09</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	Pingdom.com_bot_version_1.4_		
	</td>
	<td align="right">19,323</td>
	<td align="right">25.12</td>
	<td align="right">162,447,905</td>
	<td align="right">2.94</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	Safari/604.1		
	</td>
	<td align="right">11,807</td>
	<td align="right">15.35</td>
	<td align="right">1,310,276,452</td>
	<td align="right">23.73</td>
	<td align="right">661</td>
	<td align="right">17.04</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	Mozilla/5.0 		
	</td>
	<td align="right">6,527</td>
	<td align="right">8.48</td>
	<td align="right">461,501,550</td>
	<td align="right">8.36</td>
	<td align="right">658</td>
	<td align="right">16.98</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	Safari/605.1.15		
	</td>
	<td align="right">4,119</td>
	<td align="right">5.35</td>
	<td align="right">488,349,553</td>
	<td align="right">8.84</td>
	<td align="right">220</td>
	<td align="right">5.68</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	Safari/537.36 Edge/18.18362		
	</td>
	<td align="right">1,308</td>
	<td align="right">1.70</td>
	<td align="right">159,106,160</td>
	<td align="right">2.88</td>
	<td align="right">53</td>
	<td align="right">1.38</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	UptimeRobot/2.0		
	</td>
	<td align="right">1,218</td>
	<td align="right">1.58</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	bingbot/2.0		
	</td>
	<td align="right">1,111</td>
	<td align="right">1.44</td>
	<td align="right">81,427,667</td>
	<td align="right">1.47</td>
	<td align="right">258</td>
	<td align="right">6.67</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	Safari/605.1		
	</td>
	<td align="right">1,007</td>
	<td align="right">1.31</td>
	<td align="right">120,976,608</td>
	<td align="right">2.19</td>
	<td align="right">54</td>
	<td align="right">1.40</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">10</th>
	<td>
	Safari/537.36 Edge/18.17763		
	</td>
	<td align="right">649</td>
	<td align="right">0.84</td>
	<td align="right">78,179,233</td>
	<td align="right">1.42</td>
	<td align="right">21</td>
	<td align="right">0.56</td>
</tr>

</tbody>
</table>

