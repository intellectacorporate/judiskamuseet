


<h2>Statistics for http://www.u5645470.fsdata.se, Week of 2019-11-4 to 2019-11-10</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 20 of 178 Referring Sites,
Sorted by Access Count

</h3>
<p></p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Referring Site</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th class="hogerstalld">1</th>
	<td>
	<a href="https://judiskamuseet.se">
	https://judiskamuseet.se</a>
	</td>
	<td align="right">41,302</td>
	<td align="right">90.39</td>
	<td align="right">4,857,118,324</td>
	<td align="right">97.34</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	<a href="http://judiskamuseet.se">
	http://judiskamuseet.se</a>
	</td>
	<td align="right">1,736</td>
	<td align="right">3.80</td>
	<td align="right">21,440,110</td>
	<td align="right">0.43</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	<a href="https://www.google.com">
	https://www.google.com</a>
	</td>
	<td align="right">1,239</td>
	<td align="right">2.71</td>
	<td align="right">53,513,703</td>
	<td align="right">1.07</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	<a href="https://www.google.se">
	https://www.google.se</a>
	</td>
	<td align="right">623</td>
	<td align="right">1.36</td>
	<td align="right">26,497,815</td>
	<td align="right">0.53</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	<a href="http://m.facebook.com">
	http://m.facebook.com</a>
	</td>
	<td align="right">134</td>
	<td align="right">0.29</td>
	<td align="right">1,619,825</td>
	<td align="right">0.03</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	<a href="http://www.google.com">
	http://www.google.com</a>
	</td>
	<td align="right">72</td>
	<td align="right">0.16</td>
	<td align="right">562,401</td>
	<td align="right">0.01</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	<a href="https://www.bing.com">
	https://www.bing.com</a>
	</td>
	<td align="right">40</td>
	<td align="right">0.09</td>
	<td align="right">4,815,313</td>
	<td align="right">0.10</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	<a href="https://www.google.de">
	https://www.google.de</a>
	</td>
	<td align="right">30</td>
	<td align="right">0.07</td>
	<td align="right">481,626</td>
	<td align="right">0.01</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	<a href="https://myactivity.google.com">
	https://myactivity.google.com</a>
	</td>
	<td align="right">18</td>
	<td align="right">0.04</td>
	<td align="right">3,189,618</td>
	<td align="right">0.06</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">10</th>
	<td>
	<a href="https://duckduckgo.com">
	https://duckduckgo.com</a>
	</td>
	<td align="right">17</td>
	<td align="right">0.04</td>
	<td align="right">203,891</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">11</th>
	<td>
	<a href="https://jjbabskoe.ru">
	https://jjbabskoe.ru</a>
	</td>
	<td align="right">15</td>
	<td align="right">0.03</td>
	<td align="right">816,483</td>
	<td align="right">0.02</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">12</th>
	<td>
	<a href="https://www.facebook.com">
	https://www.facebook.com</a>
	</td>
	<td align="right">15</td>
	<td align="right">0.03</td>
	<td align="right">220,132</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">13</th>
	<td>
	<a href="https://sv.wikipedia.org">
	https://sv.wikipedia.org</a>
	</td>
	<td align="right">12</td>
	<td align="right">0.03</td>
	<td align="right">62,479</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">14</th>
	<td>
	<a href="https://zvuqa.net">
	https://zvuqa.net</a>
	</td>
	<td align="right">11</td>
	<td align="right">0.02</td>
	<td align="right">553,830</td>
	<td align="right">0.01</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">15</th>
	<td>
	<a href="http://www.judiska-museet.se">
	http://www.judiska-museet.se</a>
	</td>
	<td align="right">11</td>
	<td align="right">0.02</td>
	<td align="right">160</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">16</th>
	<td>
	<a href="https://m.facebook.com">
	https://m.facebook.com</a>
	</td>
	<td align="right">10</td>
	<td align="right">0.02</td>
	<td align="right">139,730</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">17</th>
	<td>
	<a href="http://site.ru">
	http://site.ru</a>
	</td>
	<td align="right">9</td>
	<td align="right">0.02</td>
	<td align="right">233,229</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">18</th>
	<td>
	<a href="http://barnett.su">
	http://barnett.su</a>
	</td>
	<td align="right">9</td>
	<td align="right">0.02</td>
	<td align="right">479,034</td>
	<td align="right">0.01</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">19</th>
	<td>
	<a href="https://r.search.yahoo.com">
	https://r.search.yahoo.com</a>
	</td>
	<td align="right">8</td>
	<td align="right">0.02</td>
	<td align="right">108,463</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">20</th>
	<td>
	<a href="https://nortonsafe.search.ask.com">
	https://nortonsafe.search.ask.com</a>
	</td>
	<td align="right">7</td>
	<td align="right">0.02</td>
	<td align="right">92,038</td>
	<td align="right">0.00</td>
</tr>

</tbody>
</table>

