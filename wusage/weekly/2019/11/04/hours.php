


<h2>Statistics for http://www.u5645470.fsdata.se, Week of 2019-11-4 to 2019-11-10</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Traffic by Hour</h3>
<p>Figures are averages for that hour
	of each day in the time period analyzed.</p>
<img src="h.png" width="468" height="351">
<br /><br />
<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Hour</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	<th scope='col'>Bits per Sec</th>
	<th scope='col'>Bytes per Sec</th>
	
	</tr>
</thead>
<tbody>
<tr class="jamnudda">
	<th align="right">00:00</th>
	<td align="right">266.57</td>
	<td align="right">2.42</td>
	<td align="right">13,780,887.43</td>
	<td align="right">1.73</td>
	<td align="right">9.50</td>
	<td align="right">1.71</td>
	<td align="right">30,624.19</td>
	<td align="right">3,828.02</td>
	
</tr>
<tr class="jamn">
	<th align="right">01:00</th>
	<td align="right">212.57</td>
	<td align="right">1.93</td>
	<td align="right">8,938,341.29</td>
	<td align="right">1.12</td>
	<td align="right">12.29</td>
	<td align="right">2.21</td>
	<td align="right">19,862.98</td>
	<td align="right">2,482.87</td>
	
</tr>
<tr class="udda">
	<th align="right">02:00</th>
	<td align="right">244.14</td>
	<td align="right">2.22</td>
	<td align="right">10,754,204.29</td>
	<td align="right">1.35</td>
	<td align="right">11.54</td>
	<td align="right">2.08</td>
	<td align="right">23,898.23</td>
	<td align="right">2,987.28</td>
	
</tr>
<tr class="jamn">
	<th align="right">03:00</th>
	<td align="right">198.86</td>
	<td align="right">1.81</td>
	<td align="right">6,605,548.14</td>
	<td align="right">0.83</td>
	<td align="right">13.05</td>
	<td align="right">2.35</td>
	<td align="right">14,679.00</td>
	<td align="right">1,834.87</td>
	
</tr>
<tr class="udda">
	<th align="right">04:00</th>
	<td align="right">215.57</td>
	<td align="right">1.96</td>
	<td align="right">3,937,202.00</td>
	<td align="right">0.49</td>
	<td align="right">9.18</td>
	<td align="right">1.65</td>
	<td align="right">8,749.34</td>
	<td align="right">1,093.67</td>
	
</tr>
<tr class="jamn">
	<th align="right">05:00</th>
	<td align="right">199.71</td>
	<td align="right">1.81</td>
	<td align="right">3,200,724.00</td>
	<td align="right">0.40</td>
	<td align="right">9.32</td>
	<td align="right">1.68</td>
	<td align="right">7,112.72</td>
	<td align="right">889.09</td>
	
</tr>
<tr class="udda">
	<th align="right">06:00</th>
	<td align="right">241.29</td>
	<td align="right">2.19</td>
	<td align="right">9,490,452.00</td>
	<td align="right">1.19</td>
	<td align="right">11.50</td>
	<td align="right">2.07</td>
	<td align="right">21,089.89</td>
	<td align="right">2,636.24</td>
	
</tr>
<tr class="jamn">
	<th align="right">07:00</th>
	<td align="right">315.29</td>
	<td align="right">2.86</td>
	<td align="right">19,955,925.00</td>
	<td align="right">2.51</td>
	<td align="right">13.05</td>
	<td align="right">2.35</td>
	<td align="right">44,346.50</td>
	<td align="right">5,543.31</td>
	
</tr>
<tr class="udda">
	<th align="right">08:00</th>
	<td align="right">420.71</td>
	<td align="right">3.82</td>
	<td align="right">30,428,132.00</td>
	<td align="right">3.82</td>
	<td align="right">17.72</td>
	<td align="right">3.19</td>
	<td align="right">67,618.07</td>
	<td align="right">8,452.26</td>
	
</tr>
<tr class="jamn">
	<th align="right">09:00</th>
	<td align="right">719.71</td>
	<td align="right">6.53</td>
	<td align="right">61,643,674.29</td>
	<td align="right">7.75</td>
	<td align="right">38.80</td>
	<td align="right">6.99</td>
	<td align="right">136,985.94</td>
	<td align="right">17,123.24</td>
	
</tr>
<tr class="udda">
	<th align="right">10:00</th>
	<td align="right">711.00</td>
	<td align="right">6.45</td>
	<td align="right">59,263,848.43</td>
	<td align="right">7.45</td>
	<td align="right">35.38</td>
	<td align="right">6.37</td>
	<td align="right">131,697.44</td>
	<td align="right">16,462.18</td>
	
</tr>
<tr class="jamn">
	<th align="right">11:00</th>
	<td align="right">607.00</td>
	<td align="right">5.51</td>
	<td align="right">49,329,378.29</td>
	<td align="right">6.20</td>
	<td align="right">29.03</td>
	<td align="right">5.23</td>
	<td align="right">109,620.84</td>
	<td align="right">13,702.61</td>
	
</tr>
<tr class="udda">
	<th align="right">12:00</th>
	<td align="right">617.57</td>
	<td align="right">5.61</td>
	<td align="right">50,615,299.86</td>
	<td align="right">6.36</td>
	<td align="right">28.41</td>
	<td align="right">5.12</td>
	<td align="right">112,478.44</td>
	<td align="right">14,059.81</td>
	
</tr>
<tr class="jamn">
	<th align="right">13:00</th>
	<td align="right">755.43</td>
	<td align="right">6.86</td>
	<td align="right">59,116,016.29</td>
	<td align="right">7.43</td>
	<td align="right">32.34</td>
	<td align="right">5.83</td>
	<td align="right">131,368.93</td>
	<td align="right">16,421.12</td>
	
</tr>
<tr class="udda">
	<th align="right">14:00</th>
	<td align="right">628.00</td>
	<td align="right">5.70</td>
	<td align="right">52,359,647.00</td>
	<td align="right">6.58</td>
	<td align="right">32.79</td>
	<td align="right">5.91</td>
	<td align="right">116,354.77</td>
	<td align="right">14,544.35</td>
	
</tr>
<tr class="jamn">
	<th align="right">15:00</th>
	<td align="right">526.71</td>
	<td align="right">4.78</td>
	<td align="right">39,830,305.86</td>
	<td align="right">5.01</td>
	<td align="right">28.59</td>
	<td align="right">5.15</td>
	<td align="right">88,511.79</td>
	<td align="right">11,063.97</td>
	
</tr>
<tr class="udda">
	<th align="right">16:00</th>
	<td align="right">680.57</td>
	<td align="right">6.18</td>
	<td align="right">59,572,493.86</td>
	<td align="right">7.49</td>
	<td align="right">29.00</td>
	<td align="right">5.22</td>
	<td align="right">132,383.32</td>
	<td align="right">16,547.91</td>
	
</tr>
<tr class="jamn">
	<th align="right">17:00</th>
	<td align="right">522.71</td>
	<td align="right">4.75</td>
	<td align="right">43,239,330.86</td>
	<td align="right">5.43</td>
	<td align="right">27.95</td>
	<td align="right">5.03</td>
	<td align="right">96,087.40</td>
	<td align="right">12,010.93</td>
	
</tr>
<tr class="udda">
	<th align="right">18:00</th>
	<td align="right">464.29</td>
	<td align="right">4.21</td>
	<td align="right">34,654,918.00</td>
	<td align="right">4.36</td>
	<td align="right">24.89</td>
	<td align="right">4.48</td>
	<td align="right">77,010.93</td>
	<td align="right">9,626.37</td>
	
</tr>
<tr class="jamn">
	<th align="right">19:00</th>
	<td align="right">508.43</td>
	<td align="right">4.62</td>
	<td align="right">32,711,033.00</td>
	<td align="right">4.11</td>
	<td align="right">22.85</td>
	<td align="right">4.12</td>
	<td align="right">72,691.18</td>
	<td align="right">9,086.40</td>
	
</tr>
<tr class="udda">
	<th align="right">20:00</th>
	<td align="right">466.57</td>
	<td align="right">4.24</td>
	<td align="right">32,686,292.86</td>
	<td align="right">4.11</td>
	<td align="right">28.99</td>
	<td align="right">5.22</td>
	<td align="right">72,636.21</td>
	<td align="right">9,079.53</td>
	
</tr>
<tr class="jamn">
	<th align="right">21:00</th>
	<td align="right">600.57</td>
	<td align="right">5.45</td>
	<td align="right">47,867,276.71</td>
	<td align="right">6.02</td>
	<td align="right">29.34</td>
	<td align="right">5.28</td>
	<td align="right">106,371.73</td>
	<td align="right">13,296.47</td>
	
</tr>
<tr class="udda">
	<th align="right">22:00</th>
	<td align="right">531.43</td>
	<td align="right">4.82</td>
	<td align="right">43,593,053.43</td>
	<td align="right">5.48</td>
	<td align="right">30.11</td>
	<td align="right">5.42</td>
	<td align="right">96,873.45</td>
	<td align="right">12,109.18</td>
	
</tr>
<tr class="jamn">
	<th align="right">23:00</th>
	<td align="right">360.43</td>
	<td align="right">3.27</td>
	<td align="right">22,065,974.57</td>
	<td align="right">2.77</td>
	<td align="right">29.53</td>
	<td align="right">5.32</td>
	<td align="right">49,035.50</td>
	<td align="right">6,129.44</td>
	
</tr>

</tbody>
</table>

