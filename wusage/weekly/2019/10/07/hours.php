


<h2>Statistics for http://www.u5645470.fsdata.se, Week of 2019-10-7 to 2019-10-13</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Traffic by Hour</h3>
<p>Figures are averages for that hour
	of each day in the time period analyzed.</p>
<img src="h.png" width="468" height="351">
<br /><br />
<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Hour</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	<th scope='col'>Bits per Sec</th>
	<th scope='col'>Bytes per Sec</th>
	
	</tr>
</thead>
<tbody>
<tr class="jamnudda">
	<th align="right">00:00</th>
	<td align="right">281.00</td>
	<td align="right">2.34</td>
	<td align="right">15,469,672.14</td>
	<td align="right">1.69</td>
	<td align="right">8.31</td>
	<td align="right">1.43</td>
	<td align="right">34,377.05</td>
	<td align="right">4,297.13</td>
	
</tr>
<tr class="jamn">
	<th align="right">01:00</th>
	<td align="right">204.00</td>
	<td align="right">1.70</td>
	<td align="right">5,493,082.86</td>
	<td align="right">0.60</td>
	<td align="right">9.69</td>
	<td align="right">1.67</td>
	<td align="right">12,206.85</td>
	<td align="right">1,525.86</td>
	
</tr>
<tr class="udda">
	<th align="right">02:00</th>
	<td align="right">191.14</td>
	<td align="right">1.59</td>
	<td align="right">6,153,072.43</td>
	<td align="right">0.67</td>
	<td align="right">9.37</td>
	<td align="right">1.61</td>
	<td align="right">13,673.49</td>
	<td align="right">1,709.19</td>
	
</tr>
<tr class="jamn">
	<th align="right">03:00</th>
	<td align="right">206.14</td>
	<td align="right">1.72</td>
	<td align="right">7,144,171.43</td>
	<td align="right">0.78</td>
	<td align="right">9.14</td>
	<td align="right">1.57</td>
	<td align="right">15,875.94</td>
	<td align="right">1,984.49</td>
	
</tr>
<tr class="udda">
	<th align="right">04:00</th>
	<td align="right">257.14</td>
	<td align="right">2.14</td>
	<td align="right">13,840,201.43</td>
	<td align="right">1.51</td>
	<td align="right">10.18</td>
	<td align="right">1.75</td>
	<td align="right">30,756.00</td>
	<td align="right">3,844.50</td>
	
</tr>
<tr class="jamn">
	<th align="right">05:00</th>
	<td align="right">217.57</td>
	<td align="right">1.81</td>
	<td align="right">5,232,922.00</td>
	<td align="right">0.57</td>
	<td align="right">11.81</td>
	<td align="right">2.03</td>
	<td align="right">11,628.72</td>
	<td align="right">1,453.59</td>
	
</tr>
<tr class="udda">
	<th align="right">06:00</th>
	<td align="right">243.57</td>
	<td align="right">2.03</td>
	<td align="right">14,998,717.86</td>
	<td align="right">1.64</td>
	<td align="right">10.15</td>
	<td align="right">1.74</td>
	<td align="right">33,330.48</td>
	<td align="right">4,166.31</td>
	
</tr>
<tr class="jamn">
	<th align="right">07:00</th>
	<td align="right">324.14</td>
	<td align="right">2.70</td>
	<td align="right">18,929,246.71</td>
	<td align="right">2.06</td>
	<td align="right">11.86</td>
	<td align="right">2.04</td>
	<td align="right">42,064.99</td>
	<td align="right">5,258.12</td>
	
</tr>
<tr class="udda">
	<th align="right">08:00</th>
	<td align="right">590.57</td>
	<td align="right">4.92</td>
	<td align="right">34,871,189.14</td>
	<td align="right">3.80</td>
	<td align="right">22.93</td>
	<td align="right">3.94</td>
	<td align="right">77,491.53</td>
	<td align="right">9,686.44</td>
	
</tr>
<tr class="jamn">
	<th align="right">09:00</th>
	<td align="right">747.86</td>
	<td align="right">6.22</td>
	<td align="right">58,497,748.86</td>
	<td align="right">6.38</td>
	<td align="right">33.54</td>
	<td align="right">5.76</td>
	<td align="right">129,995.00</td>
	<td align="right">16,249.37</td>
	
</tr>
<tr class="udda">
	<th align="right">10:00</th>
	<td align="right">755.43</td>
	<td align="right">6.29</td>
	<td align="right">52,032,240.43</td>
	<td align="right">5.67</td>
	<td align="right">35.48</td>
	<td align="right">6.09</td>
	<td align="right">115,627.20</td>
	<td align="right">14,453.40</td>
	
</tr>
<tr class="jamn">
	<th align="right">11:00</th>
	<td align="right">749.43</td>
	<td align="right">6.24</td>
	<td align="right">58,374,911.00</td>
	<td align="right">6.37</td>
	<td align="right">37.03</td>
	<td align="right">6.36</td>
	<td align="right">129,722.02</td>
	<td align="right">16,215.25</td>
	
</tr>
<tr class="udda">
	<th align="right">12:00</th>
	<td align="right">697.43</td>
	<td align="right">5.80</td>
	<td align="right">55,756,700.29</td>
	<td align="right">6.08</td>
	<td align="right">37.41</td>
	<td align="right">6.43</td>
	<td align="right">123,903.78</td>
	<td align="right">15,487.97</td>
	
</tr>
<tr class="jamn">
	<th align="right">13:00</th>
	<td align="right">671.57</td>
	<td align="right">5.59</td>
	<td align="right">46,475,029.86</td>
	<td align="right">5.07</td>
	<td align="right">35.97</td>
	<td align="right">6.18</td>
	<td align="right">103,277.84</td>
	<td align="right">12,909.73</td>
	
</tr>
<tr class="udda">
	<th align="right">14:00</th>
	<td align="right">808.57</td>
	<td align="right">6.73</td>
	<td align="right">125,172,931.29</td>
	<td align="right">13.65</td>
	<td align="right">33.30</td>
	<td align="right">5.72</td>
	<td align="right">278,162.07</td>
	<td align="right">34,770.26</td>
	
</tr>
<tr class="jamn">
	<th align="right">15:00</th>
	<td align="right">642.57</td>
	<td align="right">5.35</td>
	<td align="right">63,677,948.71</td>
	<td align="right">6.94</td>
	<td align="right">34.77</td>
	<td align="right">5.97</td>
	<td align="right">141,506.55</td>
	<td align="right">17,688.32</td>
	
</tr>
<tr class="udda">
	<th align="right">16:00</th>
	<td align="right">586.00</td>
	<td align="right">4.88</td>
	<td align="right">49,830,520.86</td>
	<td align="right">5.43</td>
	<td align="right">27.19</td>
	<td align="right">4.67</td>
	<td align="right">110,734.49</td>
	<td align="right">13,841.81</td>
	
</tr>
<tr class="jamn">
	<th align="right">17:00</th>
	<td align="right">615.00</td>
	<td align="right">5.12</td>
	<td align="right">49,866,907.57</td>
	<td align="right">5.44</td>
	<td align="right">27.38</td>
	<td align="right">4.70</td>
	<td align="right">110,815.35</td>
	<td align="right">13,851.92</td>
	
</tr>
<tr class="udda">
	<th align="right">18:00</th>
	<td align="right">680.00</td>
	<td align="right">5.66</td>
	<td align="right">45,987,242.29</td>
	<td align="right">5.02</td>
	<td align="right">29.68</td>
	<td align="right">5.10</td>
	<td align="right">102,193.87</td>
	<td align="right">12,774.23</td>
	
</tr>
<tr class="jamn">
	<th align="right">19:00</th>
	<td align="right">559.86</td>
	<td align="right">4.66</td>
	<td align="right">33,544,693.57</td>
	<td align="right">3.66</td>
	<td align="right">28.14</td>
	<td align="right">4.83</td>
	<td align="right">74,543.76</td>
	<td align="right">9,317.97</td>
	
</tr>
<tr class="udda">
	<th align="right">20:00</th>
	<td align="right">558.57</td>
	<td align="right">4.65</td>
	<td align="right">42,556,116.86</td>
	<td align="right">4.64</td>
	<td align="right">31.02</td>
	<td align="right">5.33</td>
	<td align="right">94,569.15</td>
	<td align="right">11,821.14</td>
	
</tr>
<tr class="jamn">
	<th align="right">21:00</th>
	<td align="right">544.43</td>
	<td align="right">4.53</td>
	<td align="right">43,151,205.29</td>
	<td align="right">4.71</td>
	<td align="right">27.19</td>
	<td align="right">4.67</td>
	<td align="right">95,891.57</td>
	<td align="right">11,986.45</td>
	
</tr>
<tr class="udda">
	<th align="right">22:00</th>
	<td align="right">508.86</td>
	<td align="right">4.24</td>
	<td align="right">37,324,756.14</td>
	<td align="right">4.07</td>
	<td align="right">29.06</td>
	<td align="right">4.99</td>
	<td align="right">82,943.90</td>
	<td align="right">10,367.99</td>
	
</tr>
<tr class="jamn">
	<th align="right">23:00</th>
	<td align="right">373.43</td>
	<td align="right">3.11</td>
	<td align="right">32,539,594.86</td>
	<td align="right">3.55</td>
	<td align="right">31.54</td>
	<td align="right">5.42</td>
	<td align="right">72,310.21</td>
	<td align="right">9,038.78</td>
	
</tr>

</tbody>
</table>

