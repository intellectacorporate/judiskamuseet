


<h2>Statistics for http://www.u5645470.fsdata.se, Week of 2019-10-7 to 2019-10-13</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 10 of 22 File Types (Extensions), 
Sorted by Access Count

</h3>
<p>Individual file types as determined by file extensions. All URLs that
do not contain an extension are counted as directories.</p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Type</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th class="hogerstalld">1</th>
	<td>
	Directory (folder)		
	</td>
	<td align="right">33,408</td>
	<td align="right">39.72</td>
	<td align="right">330,410,227</td>
	<td align="right">5.15</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	jpg		
	</td>
	<td align="right">14,938</td>
	<td align="right">17.76</td>
	<td align="right">4,405,956,140</td>
	<td align="right">68.65</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	js		
	</td>
	<td align="right">14,924</td>
	<td align="right">17.75</td>
	<td align="right">295,587,524</td>
	<td align="right">4.61</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	png		
	</td>
	<td align="right">9,884</td>
	<td align="right">11.75</td>
	<td align="right">252,637,480</td>
	<td align="right">3.94</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	css		
	</td>
	<td align="right">5,453</td>
	<td align="right">6.48</td>
	<td align="right">313,560,738</td>
	<td align="right">4.89</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	php		
	</td>
	<td align="right">2,584</td>
	<td align="right">3.07</td>
	<td align="right">31,639,197</td>
	<td align="right">0.49</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	ttf		
	</td>
	<td align="right">1,869</td>
	<td align="right">2.22</td>
	<td align="right">69,954,007</td>
	<td align="right">1.09</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	txt		
	</td>
	<td align="right">656</td>
	<td align="right">0.78</td>
	<td align="right">43,219</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	jpeg		
	</td>
	<td align="right">153</td>
	<td align="right">0.18</td>
	<td align="right">78,977,887</td>
	<td align="right">1.23</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">10</th>
	<td>
	ico		
	</td>
	<td align="right">50</td>
	<td align="right">0.06</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>

</tbody>
</table>

