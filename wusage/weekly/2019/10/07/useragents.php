


<h2>Statistics for http://www.u5645470.fsdata.se, Week of 2019-10-7 to 2019-10-13</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 10 of 328 Browsers (User Agents),
Sorted by Access Count

</h3>
<p></p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Product</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th class="hogerstalld">1</th>
	<td>
	Safari/537.36		
	</td>
	<td align="right">23,458</td>
	<td align="right">27.98</td>
	<td align="right">2,628,062,410</td>
	<td align="right">41.22</td>
	<td align="right">1,144</td>
	<td align="right">28.43</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	Pingdom.com_bot_version_1.4_		
	</td>
	<td align="right">19,457</td>
	<td align="right">23.21</td>
	<td align="right">161,488,083</td>
	<td align="right">2.53</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	Safari/604.1		
	</td>
	<td align="right">11,837</td>
	<td align="right">14.12</td>
	<td align="right">1,182,265,432</td>
	<td align="right">18.54</td>
	<td align="right">598</td>
	<td align="right">14.86</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	Mozilla/5.0 		
	</td>
	<td align="right">7,761</td>
	<td align="right">9.26</td>
	<td align="right">644,057,865</td>
	<td align="right">10.10</td>
	<td align="right">797</td>
	<td align="right">19.80</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	Safari/605.1.15		
	</td>
	<td align="right">3,931</td>
	<td align="right">4.69</td>
	<td align="right">383,281,685</td>
	<td align="right">6.01</td>
	<td align="right">183</td>
	<td align="right">4.56</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	Safari/537.36 Edge/18.18362		
	</td>
	<td align="right">1,926</td>
	<td align="right">2.30</td>
	<td align="right">183,634,563</td>
	<td align="right">2.88</td>
	<td align="right">70</td>
	<td align="right">1.76</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	Safari/605.1		
	</td>
	<td align="right">1,471</td>
	<td align="right">1.75</td>
	<td align="right">207,104,032</td>
	<td align="right">3.25</td>
	<td align="right">68</td>
	<td align="right">1.71</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	UptimeRobot/2.0		
	</td>
	<td align="right">1,289</td>
	<td align="right">1.54</td>
	<td align="right">16,041</td>
	<td align="right">0.00</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	bingbot/2.0		
	</td>
	<td align="right">1,288</td>
	<td align="right">1.54</td>
	<td align="right">59,187,448</td>
	<td align="right">0.93</td>
	<td align="right">263</td>
	<td align="right">6.54</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">10</th>
	<td>
	MSIE 5.5		
	</td>
	<td align="right">1,001</td>
	<td align="right">1.19</td>
	<td align="right">12,846,858</td>
	<td align="right">0.20</td>
	<td align="right">1</td>
	<td align="right">0.03</td>
</tr>

</tbody>
</table>

