


<h2>Statistics for http://www.u5645470.fsdata.se, Week of 2019-10-7 to 2019-10-13</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 20 of 3,895 Visitor Sites,
Sorted by Access Count

</h3>
<p></p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Site</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="jamnudda">
	<th class="hogerstalld">1</th>
	<td>
	94.247.174.83		
	</td>
	<td align="right">1,953</td>
	<td align="right">2.32</td>
	<td align="right">16,155,254</td>
	<td align="right">0.25</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	81.17.62.205		
	</td>
	<td align="right">1,950</td>
	<td align="right">2.32</td>
	<td align="right">16,155,199</td>
	<td align="right">0.25</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	151.106.52.134		
	</td>
	<td align="right">1,944</td>
	<td align="right">2.31</td>
	<td align="right">16,155,091</td>
	<td align="right">0.25</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	185.93.3.92		
	</td>
	<td align="right">1,941</td>
	<td align="right">2.31</td>
	<td align="right">16,139,049</td>
	<td align="right">0.25</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	85.195.116.134		
	</td>
	<td align="right">1,940</td>
	<td align="right">2.31</td>
	<td align="right">16,139,298</td>
	<td align="right">0.25</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	94.75.211.73		
	</td>
	<td align="right">1,599</td>
	<td align="right">1.90</td>
	<td align="right">13,204,264</td>
	<td align="right">0.21</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	5.172.196.188		
	</td>
	<td align="right">1,413</td>
	<td align="right">1.68</td>
	<td align="right">11,648,227</td>
	<td align="right">0.18</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	63.143.42.242		
	</td>
	<td align="right">1,289</td>
	<td align="right">1.53</td>
	<td align="right">16,041</td>
	<td align="right">0.00</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	185.180.12.65		
	</td>
	<td align="right">1,240</td>
	<td align="right">1.47</td>
	<td align="right">10,252,620</td>
	<td align="right">0.16</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">10</th>
	<td>
	82.103.139.165		
	</td>
	<td align="right">1,188</td>
	<td align="right">1.41</td>
	<td align="right">9,931,595</td>
	<td align="right">0.15</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">11</th>
	<td>
	185.65.135.252		
	</td>
	<td align="right">1,052</td>
	<td align="right">1.25</td>
	<td align="right">12,988,961</td>
	<td align="right">0.20</td>
	<td align="right">2</td>
	<td align="right">0.05</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">12</th>
	<td>
	188.151.113.17		
	</td>
	<td align="right">1,050</td>
	<td align="right">1.25</td>
	<td align="right">366,797,052</td>
	<td align="right">5.71</td>
	<td align="right">14</td>
	<td align="right">0.37</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">13</th>
	<td>
	109.123.101.103		
	</td>
	<td align="right">1,009</td>
	<td align="right">1.20</td>
	<td align="right">8,452,326</td>
	<td align="right">0.13</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">14</th>
	<td>
	46.59.25.88		
	</td>
	<td align="right">985</td>
	<td align="right">1.17</td>
	<td align="right">72,205,743</td>
	<td align="right">1.12</td>
	<td align="right">25</td>
	<td align="right">0.63</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">15</th>
	<td>
	169.51.2.22		
	</td>
	<td align="right">927</td>
	<td align="right">1.10</td>
	<td align="right">7,718,806</td>
	<td align="right">0.12</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">16</th>
	<td>
	185.70.76.23		
	</td>
	<td align="right">900</td>
	<td align="right">1.07</td>
	<td align="right">7,442,464</td>
	<td align="right">0.12</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">17</th>
	<td>
	185.246.208.82		
	</td>
	<td align="right">751</td>
	<td align="right">0.89</td>
	<td align="right">6,207,407</td>
	<td align="right">0.10</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">18</th>
	<td>
	185.152.65.167		
	</td>
	<td align="right">706</td>
	<td align="right">0.84</td>
	<td align="right">5,886,487</td>
	<td align="right">0.09</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">19</th>
	<td>
	185.15.93.69		
	</td>
	<td align="right">677</td>
	<td align="right">0.80</td>
	<td align="right">21,182,390</td>
	<td align="right">0.33</td>
	<td align="right">72</td>
	<td align="right">1.78</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">20</th>
	<td>
	85.93.88.91		
	</td>
	<td align="right">492</td>
	<td align="right">0.59</td>
	<td align="right">57,953,547</td>
	<td align="right">0.90</td>
	<td align="right">157</td>
	<td align="right">3.86</td>
</tr>

</tbody>
</table>

