


<h2>Statistics for http://www.u5645470.fsdata.se, Week of 2019-10-7 to 2019-10-13</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 20 of 192 Referring Sites,
Sorted by Access Count

</h3>
<p></p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Referring Site</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th class="hogerstalld">1</th>
	<td>
	<a href="https://judiskamuseet.se">
	https://judiskamuseet.se</a>
	</td>
	<td align="right">44,753</td>
	<td align="right">87.81</td>
	<td align="right">5,572,680,207</td>
	<td align="right">97.54</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	<a href="http://judiskamuseet.se">
	http://judiskamuseet.se</a>
	</td>
	<td align="right">2,295</td>
	<td align="right">4.50</td>
	<td align="right">39,952,867</td>
	<td align="right">0.70</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	<a href="https://www.google.com">
	https://www.google.com</a>
	</td>
	<td align="right">1,327</td>
	<td align="right">2.60</td>
	<td align="right">45,171,425</td>
	<td align="right">0.79</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	<a href="https://judiskamuseet.se:443">
	https://judiskamuseet.se:443</a>
	</td>
	<td align="right">1,075</td>
	<td align="right">2.11</td>
	<td align="right">12,492,087</td>
	<td align="right">0.22</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	<a href="https://www.google.se">
	https://www.google.se</a>
	</td>
	<td align="right">520</td>
	<td align="right">1.02</td>
	<td align="right">22,553,854</td>
	<td align="right">0.39</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	<a href="https://www.google.de">
	https://www.google.de</a>
	</td>
	<td align="right">121</td>
	<td align="right">0.24</td>
	<td align="right">1,932,300</td>
	<td align="right">0.03</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	<a href="http://m.facebook.com">
	http://m.facebook.com</a>
	</td>
	<td align="right">112</td>
	<td align="right">0.22</td>
	<td align="right">1,375,919</td>
	<td align="right">0.02</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	<a href="http://www.google.com">
	http://www.google.com</a>
	</td>
	<td align="right">94</td>
	<td align="right">0.18</td>
	<td align="right">671,987</td>
	<td align="right">0.01</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	<a href="http://instagram.com">
	http://instagram.com</a>
	</td>
	<td align="right">79</td>
	<td align="right">0.16</td>
	<td align="right">624,997</td>
	<td align="right">0.01</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">10</th>
	<td>
	<a href="https://www.bing.com">
	https://www.bing.com</a>
	</td>
	<td align="right">57</td>
	<td align="right">0.11</td>
	<td align="right">1,218,272</td>
	<td align="right">0.02</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">11</th>
	<td>
	<a href="http://judiskamuseet.se:80">
	http://judiskamuseet.se:80</a>
	</td>
	<td align="right">30</td>
	<td align="right">0.06</td>
	<td align="right">322,667</td>
	<td align="right">0.01</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">12</th>
	<td>
	<a href="https://www.netcraft.com">
	https://www.netcraft.com</a>
	</td>
	<td align="right">23</td>
	<td align="right">0.05</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">13</th>
	<td>
	<a href="http://www.judiska-museet.se">
	http://www.judiska-museet.se</a>
	</td>
	<td align="right">19</td>
	<td align="right">0.04</td>
	<td align="right">180</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">14</th>
	<td>
	<a href="http://site.ru">
	http://site.ru</a>
	</td>
	<td align="right">19</td>
	<td align="right">0.04</td>
	<td align="right">857,624</td>
	<td align="right">0.02</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">15</th>
	<td>
	<a href="https://myactivity.google.com">
	https://myactivity.google.com</a>
	</td>
	<td align="right">16</td>
	<td align="right">0.03</td>
	<td align="right">2,209,797</td>
	<td align="right">0.04</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">16</th>
	<td>
	<a href="https://www.facebook.com">
	https://www.facebook.com</a>
	</td>
	<td align="right">12</td>
	<td align="right">0.02</td>
	<td align="right">156,302</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">17</th>
	<td>
	<a href="http://l.instagram.com">
	http://l.instagram.com</a>
	</td>
	<td align="right">10</td>
	<td align="right">0.02</td>
	<td align="right">96,047</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">18</th>
	<td>
	<a href="https://www.dominicacitizenship.info">
	https://www.dominicacitizenship.info</a>
	</td>
	<td align="right">9</td>
	<td align="right">0.02</td>
	<td align="right">424,755</td>
	<td align="right">0.01</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">19</th>
	<td>
	<a href="https://avigo.md">
	https://avigo.md</a>
	</td>
	<td align="right">9</td>
	<td align="right">0.02</td>
	<td align="right">424,755</td>
	<td align="right">0.01</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">20</th>
	<td>
	<a href="https://r.search.yahoo.com">
	https://r.search.yahoo.com</a>
	</td>
	<td align="right">8</td>
	<td align="right">0.02</td>
	<td align="right">125,639</td>
	<td align="right">0.00</td>
</tr>

</tbody>
</table>

