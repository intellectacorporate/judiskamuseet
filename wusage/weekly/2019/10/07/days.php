


<h2>Statistics for http://www.u5645470.fsdata.se, Week of 2019-10-7 to 2019-10-13</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Traffic by Day</h3>
<img src="d.png" width="468" height="351">
<br /><br />


<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Date</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	<th scope='col'>Bits per Sec</th>
	<th scope='col'>Bytes per Sec</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th align="right">2019-10-7</th>
	<td align="right">12,197	</td>
	<td align="right">14.50	</td>
	<td align="right">934,828,185	</td>
	<td align="right">14.56	</td>
	<td align="right">590	</td>
	<td align="right">14.48	</td>
	<td align="right">86,558.17	</td>
	<td align="right">10,819.77	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-10-8</th>
	<td align="right">12,338	</td>
	<td align="right">14.67	</td>
	<td align="right">889,266,312	</td>
	<td align="right">13.85	</td>
	<td align="right">606	</td>
	<td align="right">14.87	</td>
	<td align="right">82,339.47	</td>
	<td align="right">10,292.43	</td>
</tr>
<tr class="udda">
	<th align="right">2019-10-9</th>
	<td align="right">12,586	</td>
	<td align="right">14.97	</td>
	<td align="right">1,047,359,609	</td>
	<td align="right">16.32	</td>
	<td align="right">498	</td>
	<td align="right">12.22	</td>
	<td align="right">96,977.74	</td>
	<td align="right">12,122.22	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-10-10</th>
	<td align="right">13,184	</td>
	<td align="right">15.68	</td>
	<td align="right">1,184,753,049	</td>
	<td align="right">18.46	</td>
	<td align="right">648	</td>
	<td align="right">15.90	</td>
	<td align="right">109,699.36	</td>
	<td align="right">13,712.42	</td>
</tr>
<tr class="udda">
	<th align="right">2019-10-11</th>
	<td align="right">13,067	</td>
	<td align="right">15.54	</td>
	<td align="right">905,689,866	</td>
	<td align="right">14.11	</td>
	<td align="right">663	</td>
	<td align="right">16.27	</td>
	<td align="right">83,860.17	</td>
	<td align="right">10,482.52	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-10-12</th>
	<td align="right">10,427	</td>
	<td align="right">12.40	</td>
	<td align="right">733,794,926	</td>
	<td align="right">11.43	</td>
	<td align="right">561	</td>
	<td align="right">13.77	</td>
	<td align="right">67,943.97	</td>
	<td align="right">8,493.00	</td>
</tr>
<tr class="udda">
	<th align="right">2019-10-13</th>
	<td align="right">10,301	</td>
	<td align="right">12.25	</td>
	<td align="right">722,753,820	</td>
	<td align="right">11.26	</td>
	<td align="right">509	</td>
	<td align="right">12.49	</td>
	<td align="right">66,921.65	</td>
	<td align="right">8,365.21	</td>
</tr>

</tbody>
</table>

