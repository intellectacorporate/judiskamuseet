


<h2>Statistics for http://www.u5645470.fsdata.se, Week of 2019-10-7 to 2019-10-13</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 10 of 11 Operating Systems,
Sorted by Access Count

</h3>
<p></p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Product</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th class="hogerstalld">1</th>
	<td>
	Mac OS X		
	</td>
	<td align="right">50,994</td>
	<td align="right">60.83</td>
	<td align="right">5,458,121,708</td>
	<td align="right">85.61</td>
	<td align="right">2,599</td>
	<td align="right">63.80</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	Other		
	</td>
	<td align="right">26,668</td>
	<td align="right">31.81</td>
	<td align="right">574,391,570</td>
	<td align="right">9.01</td>
	<td align="right">816</td>
	<td align="right">20.04</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	Windows NT		
	</td>
	<td align="right">3,928</td>
	<td align="right">4.69</td>
	<td align="right">328,567,886</td>
	<td align="right">5.15</td>
	<td align="right">150</td>
	<td align="right">3.70</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	Linux		
	</td>
	<td align="right">2,042</td>
	<td align="right">2.44</td>
	<td align="right">7,074,477</td>
	<td align="right">0.11</td>
	<td align="right">503</td>
	<td align="right">12.36</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	Windows XP		
	</td>
	<td align="right">126</td>
	<td align="right">0.15</td>
	<td align="right">4,982,910</td>
	<td align="right">0.08</td>
	<td align="right">3</td>
	<td align="right">0.08</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	Windows 2000		
	</td>
	<td align="right">29</td>
	<td align="right">0.03</td>
	<td align="right">1,248,326</td>
	<td align="right">0.02</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	Windows 2003		
	</td>
	<td align="right">23</td>
	<td align="right">0.03</td>
	<td align="right">295,895</td>
	<td align="right">0.00</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	Windows Codename Longhorn		
	</td>
	<td align="right">16</td>
	<td align="right">0.02</td>
	<td align="right">401,625</td>
	<td align="right">0.01</td>
	<td align="right">1</td>
	<td align="right">0.03</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	Mac OS		
	</td>
	<td align="right">3</td>
	<td align="right">0.00</td>
	<td align="right">2,046</td>
	<td align="right">0.00</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">10</th>
	<td>
	Windows Me		
	</td>
	<td align="right">3</td>
	<td align="right">0.00</td>
	<td align="right">141,585</td>
	<td align="right">0.00</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>

</tbody>
</table>

