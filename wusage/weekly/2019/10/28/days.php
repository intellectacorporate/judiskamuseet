


<h2>Statistics for http://www.u5645470.fsdata.se, Week of 2019-10-28 to 2019-11-3</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Traffic by Day</h3>
<img src="d.png" width="468" height="351">
<br /><br />


<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Date</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	<th scope='col'>Bits per Sec</th>
	<th scope='col'>Bytes per Sec</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th align="right">2019-10-28</th>
	<td align="right">10,529	</td>
	<td align="right">13.34	</td>
	<td align="right">717,035,066	</td>
	<td align="right">12.22	</td>
	<td align="right">545	</td>
	<td align="right">15.35	</td>
	<td align="right">66,392.14	</td>
	<td align="right">8,299.02	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-10-29</th>
	<td align="right">10,656	</td>
	<td align="right">13.50	</td>
	<td align="right">786,717,361	</td>
	<td align="right">13.41	</td>
	<td align="right">463	</td>
	<td align="right">13.04	</td>
	<td align="right">72,844.20	</td>
	<td align="right">9,105.53	</td>
</tr>
<tr class="udda">
	<th align="right">2019-10-30</th>
	<td align="right">10,952	</td>
	<td align="right">13.88	</td>
	<td align="right">906,425,079	</td>
	<td align="right">15.45	</td>
	<td align="right">471	</td>
	<td align="right">13.26	</td>
	<td align="right">83,928.25	</td>
	<td align="right">10,491.03	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-10-31</th>
	<td align="right">10,674	</td>
	<td align="right">13.53	</td>
	<td align="right">900,527,660	</td>
	<td align="right">15.35	</td>
	<td align="right">441	</td>
	<td align="right">12.42	</td>
	<td align="right">83,382.19	</td>
	<td align="right">10,422.77	</td>
</tr>
<tr class="udda">
	<th align="right">2019-11-1</th>
	<td align="right">15,407	</td>
	<td align="right">19.52	</td>
	<td align="right">1,132,110,123	</td>
	<td align="right">19.30	</td>
	<td align="right">697	</td>
	<td align="right">19.63	</td>
	<td align="right">104,825.01	</td>
	<td align="right">13,103.13	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-11-2</th>
	<td align="right">10,724	</td>
	<td align="right">13.59	</td>
	<td align="right">624,568,453	</td>
	<td align="right">10.65	</td>
	<td align="right">469	</td>
	<td align="right">13.21	</td>
	<td align="right">57,830.41	</td>
	<td align="right">7,228.80	</td>
</tr>
<tr class="udda">
	<th align="right">2019-11-3</th>
	<td align="right">9,976	</td>
	<td align="right">12.64	</td>
	<td align="right">799,166,460	</td>
	<td align="right">13.62	</td>
	<td align="right">465	</td>
	<td align="right">13.09	</td>
	<td align="right">73,996.89	</td>
	<td align="right">9,249.61	</td>
</tr>

</tbody>
</table>

