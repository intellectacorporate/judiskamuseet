


<h2>Statistics for http://www.u5645470.fsdata.se, Week of 2019-10-28 to 2019-11-3</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 10 of 20 File Types (Extensions), 
Sorted by Access Count

</h3>
<p>Individual file types as determined by file extensions. All URLs that
do not contain an extension are counted as directories.</p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Type</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th class="hogerstalld">1</th>
	<td>
	Directory (folder)		
	</td>
	<td align="right">31,931</td>
	<td align="right">40.46</td>
	<td align="right">314,719,572</td>
	<td align="right">5.36</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	js		
	</td>
	<td align="right">14,550</td>
	<td align="right">18.44</td>
	<td align="right">283,236,292</td>
	<td align="right">4.83</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	jpg		
	</td>
	<td align="right">13,201</td>
	<td align="right">16.73</td>
	<td align="right">4,176,548,455</td>
	<td align="right">71.19</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	png		
	</td>
	<td align="right">9,251</td>
	<td align="right">11.72</td>
	<td align="right">218,508,921</td>
	<td align="right">3.72</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	css		
	</td>
	<td align="right">5,210</td>
	<td align="right">6.60</td>
	<td align="right">300,649,306</td>
	<td align="right">5.12</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	ttf		
	</td>
	<td align="right">1,845</td>
	<td align="right">2.34</td>
	<td align="right">69,356,547</td>
	<td align="right">1.18</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	php		
	</td>
	<td align="right">1,408</td>
	<td align="right">1.78</td>
	<td align="right">29,437,601</td>
	<td align="right">0.50</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	jpeg		
	</td>
	<td align="right">848</td>
	<td align="right">1.07</td>
	<td align="right">206,211,042</td>
	<td align="right">3.52</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	txt		
	</td>
	<td align="right">435</td>
	<td align="right">0.55</td>
	<td align="right">28,599</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">10</th>
	<td>
	ico		
	</td>
	<td align="right">56</td>
	<td align="right">0.07</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>

</tbody>
</table>

