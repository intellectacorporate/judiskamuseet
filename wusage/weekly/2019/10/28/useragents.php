


<h2>Statistics for http://www.u5645470.fsdata.se, Week of 2019-10-28 to 2019-11-3</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 10 of 303 Browsers (User Agents),
Sorted by Access Count

</h3>
<p></p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Product</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th class="hogerstalld">1</th>
	<td>
	Pingdom.com_bot_version_1.4_		
	</td>
	<td align="right">19,460</td>
	<td align="right">24.67</td>
	<td align="right">162,431,364</td>
	<td align="right">2.77</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	Safari/537.36		
	</td>
	<td align="right">18,912</td>
	<td align="right">23.98</td>
	<td align="right">2,297,323,604</td>
	<td align="right">39.18</td>
	<td align="right">918</td>
	<td align="right">25.90</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	Safari/604.1		
	</td>
	<td align="right">12,469</td>
	<td align="right">15.81</td>
	<td align="right">1,426,956,072</td>
	<td align="right">24.33</td>
	<td align="right">581</td>
	<td align="right">16.40</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	Mozilla/5.0 		
	</td>
	<td align="right">7,862</td>
	<td align="right">9.97</td>
	<td align="right">521,834,617</td>
	<td align="right">8.90</td>
	<td align="right">629</td>
	<td align="right">17.76</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	Safari/605.1.15		
	</td>
	<td align="right">3,520</td>
	<td align="right">4.46</td>
	<td align="right">363,578,564</td>
	<td align="right">6.20</td>
	<td align="right">181</td>
	<td align="right">5.12</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	Safari/605.1		
	</td>
	<td align="right">1,757</td>
	<td align="right">2.23</td>
	<td align="right">209,833,926</td>
	<td align="right">3.58</td>
	<td align="right">74</td>
	<td align="right">2.09</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	Safari/537.36 &#091;FB_IAB/FB4A;FBAV/245.0.0.39.118;&#093;		
	</td>
	<td align="right">1,554</td>
	<td align="right">1.97</td>
	<td align="right">59,605,175</td>
	<td align="right">1.02</td>
	<td align="right">101</td>
	<td align="right">2.86</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	bingbot/2.0		
	</td>
	<td align="right">1,302</td>
	<td align="right">1.65</td>
	<td align="right">87,679,025</td>
	<td align="right">1.50</td>
	<td align="right">278</td>
	<td align="right">7.85</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	UptimeRobot/2.0		
	</td>
	<td align="right">1,233</td>
	<td align="right">1.56</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">10</th>
	<td>
	Safari/537.36 Edge/18.18362		
	</td>
	<td align="right">1,222</td>
	<td align="right">1.55</td>
	<td align="right">161,670,623</td>
	<td align="right">2.76</td>
	<td align="right">44</td>
	<td align="right">1.27</td>
</tr>

</tbody>
</table>

