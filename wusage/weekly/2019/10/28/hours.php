


<h2>Statistics for http://www.u5645470.fsdata.se, Week of 2019-10-28 to 2019-11-3</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Traffic by Hour</h3>
<p>Figures are averages for that hour
	of each day in the time period analyzed.</p>
<img src="h.png" width="468" height="351">
<br /><br />
<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Hour</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	<th scope='col'>Bits per Sec</th>
	<th scope='col'>Bytes per Sec</th>
	
	</tr>
</thead>
<tbody>
<tr class="jamnudda">
	<th align="right">00:00</th>
	<td align="right">305.43</td>
	<td align="right">2.71</td>
	<td align="right">20,042,072.00</td>
	<td align="right">2.39</td>
	<td align="right">8.13</td>
	<td align="right">1.60</td>
	<td align="right">44,537.94</td>
	<td align="right">5,567.24</td>
	
</tr>
<tr class="jamn">
	<th align="right">01:00</th>
	<td align="right">231.86</td>
	<td align="right">2.06</td>
	<td align="right">16,100,135.86</td>
	<td align="right">1.92</td>
	<td align="right">10.48</td>
	<td align="right">2.07</td>
	<td align="right">35,778.08</td>
	<td align="right">4,472.26</td>
	
</tr>
<tr class="udda">
	<th align="right">02:00</th>
	<td align="right">213.29</td>
	<td align="right">1.89</td>
	<td align="right">7,887,132.43</td>
	<td align="right">0.94</td>
	<td align="right">9.71</td>
	<td align="right">1.91</td>
	<td align="right">17,526.96</td>
	<td align="right">2,190.87</td>
	
</tr>
<tr class="jamn">
	<th align="right">03:00</th>
	<td align="right">205.29</td>
	<td align="right">1.82</td>
	<td align="right">3,617,851.29</td>
	<td align="right">0.43</td>
	<td align="right">6.92</td>
	<td align="right">1.36</td>
	<td align="right">8,039.67</td>
	<td align="right">1,004.96</td>
	
</tr>
<tr class="udda">
	<th align="right">04:00</th>
	<td align="right">198.00</td>
	<td align="right">1.76</td>
	<td align="right">5,896,657.71</td>
	<td align="right">0.70</td>
	<td align="right">7.70</td>
	<td align="right">1.52</td>
	<td align="right">13,103.68</td>
	<td align="right">1,637.96</td>
	
</tr>
<tr class="jamn">
	<th align="right">05:00</th>
	<td align="right">209.00</td>
	<td align="right">1.85</td>
	<td align="right">5,449,578.14</td>
	<td align="right">0.65</td>
	<td align="right">9.68</td>
	<td align="right">1.91</td>
	<td align="right">12,110.17</td>
	<td align="right">1,513.77</td>
	
</tr>
<tr class="udda">
	<th align="right">06:00</th>
	<td align="right">253.14</td>
	<td align="right">2.25</td>
	<td align="right">17,423,847.14</td>
	<td align="right">2.08</td>
	<td align="right">9.83</td>
	<td align="right">1.94</td>
	<td align="right">38,719.66</td>
	<td align="right">4,839.96</td>
	
</tr>
<tr class="jamn">
	<th align="right">07:00</th>
	<td align="right">361.29</td>
	<td align="right">3.20</td>
	<td align="right">23,808,969.86</td>
	<td align="right">2.84</td>
	<td align="right">12.77</td>
	<td align="right">2.52</td>
	<td align="right">52,908.82</td>
	<td align="right">6,613.60</td>
	
</tr>
<tr class="udda">
	<th align="right">08:00</th>
	<td align="right">474.29</td>
	<td align="right">4.21</td>
	<td align="right">33,977,744.14</td>
	<td align="right">4.05</td>
	<td align="right">21.87</td>
	<td align="right">4.31</td>
	<td align="right">75,506.10</td>
	<td align="right">9,438.26</td>
	
</tr>
<tr class="jamn">
	<th align="right">09:00</th>
	<td align="right">661.29</td>
	<td align="right">5.87</td>
	<td align="right">53,882,144.71</td>
	<td align="right">6.43</td>
	<td align="right">23.38</td>
	<td align="right">4.61</td>
	<td align="right">119,738.10</td>
	<td align="right">14,967.26</td>
	
</tr>
<tr class="udda">
	<th align="right">10:00</th>
	<td align="right">609.71</td>
	<td align="right">5.41</td>
	<td align="right">57,393,905.86</td>
	<td align="right">6.85</td>
	<td align="right">27.09</td>
	<td align="right">5.34</td>
	<td align="right">127,542.01</td>
	<td align="right">15,942.75</td>
	
</tr>
<tr class="jamn">
	<th align="right">11:00</th>
	<td align="right">626.57</td>
	<td align="right">5.56</td>
	<td align="right">50,689,131.43</td>
	<td align="right">6.05</td>
	<td align="right">24.86</td>
	<td align="right">4.90</td>
	<td align="right">112,642.51</td>
	<td align="right">14,080.31</td>
	
</tr>
<tr class="udda">
	<th align="right">12:00</th>
	<td align="right">483.71</td>
	<td align="right">4.29</td>
	<td align="right">43,761,686.86</td>
	<td align="right">5.22</td>
	<td align="right">22.91</td>
	<td align="right">4.52</td>
	<td align="right">97,248.19</td>
	<td align="right">12,156.02</td>
	
</tr>
<tr class="jamn">
	<th align="right">13:00</th>
	<td align="right">640.86</td>
	<td align="right">5.68</td>
	<td align="right">57,045,248.14</td>
	<td align="right">6.81</td>
	<td align="right">25.53</td>
	<td align="right">5.03</td>
	<td align="right">126,767.22</td>
	<td align="right">15,845.90</td>
	
</tr>
<tr class="udda">
	<th align="right">14:00</th>
	<td align="right">632.86</td>
	<td align="right">5.61</td>
	<td align="right">46,864,425.57</td>
	<td align="right">5.59</td>
	<td align="right">28.88</td>
	<td align="right">5.69</td>
	<td align="right">104,143.17</td>
	<td align="right">13,017.90</td>
	
</tr>
<tr class="jamn">
	<th align="right">15:00</th>
	<td align="right">622.43</td>
	<td align="right">5.52</td>
	<td align="right">51,975,034.86</td>
	<td align="right">6.20</td>
	<td align="right">23.83</td>
	<td align="right">4.70</td>
	<td align="right">115,500.08</td>
	<td align="right">14,437.51</td>
	
</tr>
<tr class="udda">
	<th align="right">16:00</th>
	<td align="right">718.57</td>
	<td align="right">6.37</td>
	<td align="right">46,865,032.14</td>
	<td align="right">5.59</td>
	<td align="right">28.20</td>
	<td align="right">5.56</td>
	<td align="right">104,144.52</td>
	<td align="right">13,018.06</td>
	
</tr>
<tr class="jamn">
	<th align="right">17:00</th>
	<td align="right">704.29</td>
	<td align="right">6.25</td>
	<td align="right">42,480,041.57</td>
	<td align="right">5.07</td>
	<td align="right">38.31</td>
	<td align="right">7.55</td>
	<td align="right">94,400.09</td>
	<td align="right">11,800.01</td>
	
</tr>
<tr class="udda">
	<th align="right">18:00</th>
	<td align="right">630.00</td>
	<td align="right">5.59</td>
	<td align="right">43,416,404.86</td>
	<td align="right">5.18</td>
	<td align="right">37.21</td>
	<td align="right">7.34</td>
	<td align="right">96,480.90</td>
	<td align="right">12,060.11</td>
	
</tr>
<tr class="jamn">
	<th align="right">19:00</th>
	<td align="right">571.86</td>
	<td align="right">5.07</td>
	<td align="right">80,177,584.71</td>
	<td align="right">9.57</td>
	<td align="right">26.55</td>
	<td align="right">5.23</td>
	<td align="right">178,172.41</td>
	<td align="right">22,271.55</td>
	
</tr>
<tr class="udda">
	<th align="right">20:00</th>
	<td align="right">562.29</td>
	<td align="right">4.99</td>
	<td align="right">44,161,458.00</td>
	<td align="right">5.27</td>
	<td align="right">25.98</td>
	<td align="right">5.12</td>
	<td align="right">98,136.57</td>
	<td align="right">12,267.07</td>
	
</tr>
<tr class="jamn">
	<th align="right">21:00</th>
	<td align="right">518.14</td>
	<td align="right">4.60</td>
	<td align="right">39,200,813.57</td>
	<td align="right">4.68</td>
	<td align="right">23.85</td>
	<td align="right">4.70</td>
	<td align="right">87,112.92</td>
	<td align="right">10,889.11</td>
	
</tr>
<tr class="udda">
	<th align="right">22:00</th>
	<td align="right">477.14</td>
	<td align="right">4.23</td>
	<td align="right">29,439,747.14</td>
	<td align="right">3.51</td>
	<td align="right">27.84</td>
	<td align="right">5.49</td>
	<td align="right">65,421.66</td>
	<td align="right">8,177.71</td>
	
</tr>
<tr class="jamn">
	<th align="right">23:00</th>
	<td align="right">362.71</td>
	<td align="right">3.22</td>
	<td align="right">16,521,952.29</td>
	<td align="right">1.97</td>
	<td align="right">25.75</td>
	<td align="right">5.08</td>
	<td align="right">36,715.45</td>
	<td align="right">4,589.43</td>
	
</tr>

</tbody>
</table>

