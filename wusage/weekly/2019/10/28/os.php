


<h2>Statistics for http://www.u5645470.fsdata.se, Week of 2019-10-28 to 2019-11-3</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 10 of 12 Operating Systems,
Sorted by Access Count

</h3>
<p></p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Product</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th class="hogerstalld">1</th>
	<td>
	Mac OS X		
	</td>
	<td align="right">48,755</td>
	<td align="right">61.81</td>
	<td align="right">5,035,097,204</td>
	<td align="right">85.86</td>
	<td align="right">2,515</td>
	<td align="right">70.84</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	Other		
	</td>
	<td align="right">25,607</td>
	<td align="right">32.46</td>
	<td align="right">467,589,260</td>
	<td align="right">7.97</td>
	<td align="right">666</td>
	<td align="right">18.77</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	Windows NT		
	</td>
	<td align="right">2,971</td>
	<td align="right">3.77</td>
	<td align="right">304,027,347</td>
	<td align="right">5.18</td>
	<td align="right">146</td>
	<td align="right">4.12</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	Linux		
	</td>
	<td align="right">1,254</td>
	<td align="right">1.59</td>
	<td align="right">40,896,908</td>
	<td align="right">0.70</td>
	<td align="right">219</td>
	<td align="right">6.18</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	Windows XP		
	</td>
	<td align="right">173</td>
	<td align="right">0.22</td>
	<td align="right">8,177,905</td>
	<td align="right">0.14</td>
	<td align="right">1</td>
	<td align="right">0.03</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	Windows Codename Longhorn		
	</td>
	<td align="right">41</td>
	<td align="right">0.05</td>
	<td align="right">5,311,425</td>
	<td align="right">0.09</td>
	<td align="right">2</td>
	<td align="right">0.06</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	Windows 2000		
	</td>
	<td align="right">26</td>
	<td align="right">0.03</td>
	<td align="right">1,106,630</td>
	<td align="right">0.02</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	Windows 2003		
	</td>
	<td align="right">23</td>
	<td align="right">0.03</td>
	<td align="right">707,925</td>
	<td align="right">0.01</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	Windows 95		
	</td>
	<td align="right">12</td>
	<td align="right">0.02</td>
	<td align="right">566,340</td>
	<td align="right">0.01</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">10</th>
	<td>
	Windows 98		
	</td>
	<td align="right">7</td>
	<td align="right">0.01</td>
	<td align="right">364,994</td>
	<td align="right">0.01</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>

</tbody>
</table>

