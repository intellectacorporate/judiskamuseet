


<h2>Statistics for http://www.u5645470.fsdata.se, Week of 2019-10-28 to 2019-11-3</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 20 of 3,385 Visitor Sites,
Sorted by Access Count

</h3>
<p></p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Site</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="jamnudda">
	<th class="hogerstalld">1</th>
	<td>
	46.246.122.10		
	</td>
	<td align="right">1,951</td>
	<td align="right">2.47</td>
	<td align="right">16,231,986</td>
	<td align="right">0.28</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	185.136.156.82		
	</td>
	<td align="right">1,950</td>
	<td align="right">2.47</td>
	<td align="right">16,248,057</td>
	<td align="right">0.28</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	85.195.116.134		
	</td>
	<td align="right">1,950</td>
	<td align="right">2.47</td>
	<td align="right">16,231,896</td>
	<td align="right">0.28</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	185.39.146.215		
	</td>
	<td align="right">1,946</td>
	<td align="right">2.47</td>
	<td align="right">16,231,874</td>
	<td align="right">0.28</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	82.103.136.16		
	</td>
	<td align="right">1,944</td>
	<td align="right">2.46</td>
	<td align="right">16,247,944</td>
	<td align="right">0.28</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	83.170.113.210		
	</td>
	<td align="right">1,576</td>
	<td align="right">2.00</td>
	<td align="right">13,056,777</td>
	<td align="right">0.22</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	185.152.65.167		
	</td>
	<td align="right">1,541</td>
	<td align="right">1.95</td>
	<td align="right">12,877,282</td>
	<td align="right">0.22</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	46.59.25.88		
	</td>
	<td align="right">1,433</td>
	<td align="right">1.82</td>
	<td align="right">107,605,377</td>
	<td align="right">1.83</td>
	<td align="right">31</td>
	<td align="right">0.87</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	185.246.208.82		
	</td>
	<td align="right">1,383</td>
	<td align="right">1.75</td>
	<td align="right">11,635,235</td>
	<td align="right">0.20</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">10</th>
	<td>
	63.143.42.242		
	</td>
	<td align="right">1,233</td>
	<td align="right">1.56</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">11</th>
	<td>
	185.93.3.92		
	</td>
	<td align="right">1,011</td>
	<td align="right">1.28</td>
	<td align="right">8,444,104</td>
	<td align="right">0.14</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">12</th>
	<td>
	95.199.146.230		
	</td>
	<td align="right">957</td>
	<td align="right">1.21</td>
	<td align="right">37,484,854</td>
	<td align="right">0.64</td>
	<td align="right">3</td>
	<td align="right">0.09</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">13</th>
	<td>
	94.247.174.83		
	</td>
	<td align="right">939</td>
	<td align="right">1.19</td>
	<td align="right">7,803,919</td>
	<td align="right">0.13</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">14</th>
	<td>
	94.75.211.74		
	</td>
	<td align="right">932</td>
	<td align="right">1.18</td>
	<td align="right">7,803,779</td>
	<td align="right">0.13</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">15</th>
	<td>
	94.75.211.73		
	</td>
	<td align="right">900</td>
	<td align="right">1.14</td>
	<td align="right">7,509,476</td>
	<td align="right">0.13</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">16</th>
	<td>
	185.65.135.167		
	</td>
	<td align="right">764</td>
	<td align="right">0.97</td>
	<td align="right">9,378,088</td>
	<td align="right">0.16</td>
	<td align="right">5</td>
	<td align="right">0.15</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">17</th>
	<td>
	46.20.45.18		
	</td>
	<td align="right">743</td>
	<td align="right">0.94</td>
	<td align="right">6,208,509</td>
	<td align="right">0.11</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">18</th>
	<td>
	185.15.93.69		
	</td>
	<td align="right">622</td>
	<td align="right">0.79</td>
	<td align="right">19,608,890</td>
	<td align="right">0.33</td>
	<td align="right">64</td>
	<td align="right">1.82</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">19</th>
	<td>
	185.6.8.3		
	</td>
	<td align="right">435</td>
	<td align="right">0.55</td>
	<td align="right">23,335,522</td>
	<td align="right">0.40</td>
	<td align="right">1</td>
	<td align="right">0.03</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">20</th>
	<td>
	81.17.62.205		
	</td>
	<td align="right">400</td>
	<td align="right">0.51</td>
	<td align="right">3,370,601</td>
	<td align="right">0.06</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>

</tbody>
</table>

