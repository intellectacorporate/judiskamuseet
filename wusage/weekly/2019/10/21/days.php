


<h2>Statistics for http://www.u5645470.fsdata.se, Week of 2019-10-21 to 2019-10-27</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Traffic by Day</h3>
<img src="d.png" width="468" height="351">
<br /><br />


<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Date</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	<th scope='col'>Bits per Sec</th>
	<th scope='col'>Bytes per Sec</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th align="right">2019-10-21</th>
	<td align="right">10,772	</td>
	<td align="right">14.10	</td>
	<td align="right">705,651,821	</td>
	<td align="right">13.18	</td>
	<td align="right">558	</td>
	<td align="right">14.80	</td>
	<td align="right">65,338.13	</td>
	<td align="right">8,167.27	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-10-22</th>
	<td align="right">11,937	</td>
	<td align="right">15.63	</td>
	<td align="right">839,756,417	</td>
	<td align="right">15.68	</td>
	<td align="right">581	</td>
	<td align="right">15.41	</td>
	<td align="right">77,755.22	</td>
	<td align="right">9,719.40	</td>
</tr>
<tr class="udda">
	<th align="right">2019-10-23</th>
	<td align="right">11,932	</td>
	<td align="right">15.62	</td>
	<td align="right">857,831,835	</td>
	<td align="right">16.02	</td>
	<td align="right">678	</td>
	<td align="right">17.98	</td>
	<td align="right">79,428.87	</td>
	<td align="right">9,928.61	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-10-24</th>
	<td align="right">13,500	</td>
	<td align="right">17.68	</td>
	<td align="right">996,202,457	</td>
	<td align="right">18.60	</td>
	<td align="right">647	</td>
	<td align="right">17.16	</td>
	<td align="right">92,240.97	</td>
	<td align="right">11,530.12	</td>
</tr>
<tr class="udda">
	<th align="right">2019-10-25</th>
	<td align="right">9,847	</td>
	<td align="right">12.89	</td>
	<td align="right">665,389,179	</td>
	<td align="right">12.43	</td>
	<td align="right">442	</td>
	<td align="right">11.72	</td>
	<td align="right">61,610.11	</td>
	<td align="right">7,701.26	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-10-26</th>
	<td align="right">9,178	</td>
	<td align="right">12.02	</td>
	<td align="right">664,953,994	</td>
	<td align="right">12.42	</td>
	<td align="right">358	</td>
	<td align="right">9.49	</td>
	<td align="right">61,569.81	</td>
	<td align="right">7,696.23	</td>
</tr>
<tr class="udda">
	<th align="right">2019-10-27</th>
	<td align="right">9,212	</td>
	<td align="right">12.06	</td>
	<td align="right">625,275,782	</td>
	<td align="right">11.68	</td>
	<td align="right">507	</td>
	<td align="right">13.44	</td>
	<td align="right">57,895.91	</td>
	<td align="right">7,236.99	</td>
</tr>

</tbody>
</table>

