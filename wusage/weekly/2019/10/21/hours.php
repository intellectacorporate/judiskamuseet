


<h2>Statistics for http://www.u5645470.fsdata.se, Week of 2019-10-21 to 2019-10-27</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Traffic by Hour</h3>
<p>Figures are averages for that hour
	of each day in the time period analyzed.</p>
<img src="h.png" width="468" height="351">
<br /><br />
<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Hour</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	<th scope='col'>Bits per Sec</th>
	<th scope='col'>Bytes per Sec</th>
	
	</tr>
</thead>
<tbody>
<tr class="jamnudda">
	<th align="right">00:00</th>
	<td align="right">281.43</td>
	<td align="right">2.58</td>
	<td align="right">11,051,911.86</td>
	<td align="right">1.44</td>
	<td align="right">9.10</td>
	<td align="right">1.69</td>
	<td align="right">24,559.80</td>
	<td align="right">3,069.98</td>
	
</tr>
<tr class="jamn">
	<th align="right">01:00</th>
	<td align="right">230.29</td>
	<td align="right">2.11</td>
	<td align="right">7,034,845.57</td>
	<td align="right">0.92</td>
	<td align="right">12.98</td>
	<td align="right">2.41</td>
	<td align="right">15,632.99</td>
	<td align="right">1,954.12</td>
	
</tr>
<tr class="udda">
	<th align="right">02:00</th>
	<td align="right">202.71</td>
	<td align="right">1.86</td>
	<td align="right">5,654,150.14</td>
	<td align="right">0.74</td>
	<td align="right">6.80</td>
	<td align="right">1.26</td>
	<td align="right">12,564.78</td>
	<td align="right">1,570.60</td>
	
</tr>
<tr class="jamn">
	<th align="right">03:00</th>
	<td align="right">192.71</td>
	<td align="right">1.77</td>
	<td align="right">2,205,961.71</td>
	<td align="right">0.29</td>
	<td align="right">8.50</td>
	<td align="right">1.58</td>
	<td align="right">4,902.14</td>
	<td align="right">612.77</td>
	
</tr>
<tr class="udda">
	<th align="right">04:00</th>
	<td align="right">191.29</td>
	<td align="right">1.75</td>
	<td align="right">2,393,749.00</td>
	<td align="right">0.31</td>
	<td align="right">9.61</td>
	<td align="right">1.78</td>
	<td align="right">5,319.44</td>
	<td align="right">664.93</td>
	
</tr>
<tr class="jamn">
	<th align="right">05:00</th>
	<td align="right">231.57</td>
	<td align="right">2.12</td>
	<td align="right">8,022,444.00</td>
	<td align="right">1.05</td>
	<td align="right">11.46</td>
	<td align="right">2.13</td>
	<td align="right">17,827.65</td>
	<td align="right">2,228.46</td>
	
</tr>
<tr class="udda">
	<th align="right">06:00</th>
	<td align="right">291.57</td>
	<td align="right">2.67</td>
	<td align="right">12,689,191.57</td>
	<td align="right">1.66</td>
	<td align="right">14.70</td>
	<td align="right">2.73</td>
	<td align="right">28,198.20</td>
	<td align="right">3,524.78</td>
	
</tr>
<tr class="jamn">
	<th align="right">07:00</th>
	<td align="right">305.00</td>
	<td align="right">2.80</td>
	<td align="right">16,240,326.71</td>
	<td align="right">2.12</td>
	<td align="right">15.32</td>
	<td align="right">2.84</td>
	<td align="right">36,089.61</td>
	<td align="right">4,511.20</td>
	
</tr>
<tr class="udda">
	<th align="right">08:00</th>
	<td align="right">494.43</td>
	<td align="right">4.53</td>
	<td align="right">32,990,370.00</td>
	<td align="right">4.31</td>
	<td align="right">21.94</td>
	<td align="right">4.07</td>
	<td align="right">73,311.93</td>
	<td align="right">9,163.99</td>
	
</tr>
<tr class="jamn">
	<th align="right">09:00</th>
	<td align="right">735.43</td>
	<td align="right">6.74</td>
	<td align="right">61,982,677.14</td>
	<td align="right">8.10</td>
	<td align="right">32.94</td>
	<td align="right">6.11</td>
	<td align="right">137,739.28</td>
	<td align="right">17,217.41</td>
	
</tr>
<tr class="udda">
	<th align="right">10:00</th>
	<td align="right">846.00</td>
	<td align="right">7.75</td>
	<td align="right">77,106,989.14</td>
	<td align="right">10.08</td>
	<td align="right">40.18</td>
	<td align="right">7.46</td>
	<td align="right">171,348.86</td>
	<td align="right">21,418.61</td>
	
</tr>
<tr class="jamn">
	<th align="right">11:00</th>
	<td align="right">692.86</td>
	<td align="right">6.35</td>
	<td align="right">62,557,664.86</td>
	<td align="right">8.18</td>
	<td align="right">39.42</td>
	<td align="right">7.32</td>
	<td align="right">139,017.03</td>
	<td align="right">17,377.13</td>
	
</tr>
<tr class="udda">
	<th align="right">12:00</th>
	<td align="right">627.00</td>
	<td align="right">5.75</td>
	<td align="right">51,749,250.86</td>
	<td align="right">6.76</td>
	<td align="right">30.15</td>
	<td align="right">5.60</td>
	<td align="right">114,998.34</td>
	<td align="right">14,374.79</td>
	
</tr>
<tr class="jamn">
	<th align="right">13:00</th>
	<td align="right">602.71</td>
	<td align="right">5.52</td>
	<td align="right">46,144,988.29</td>
	<td align="right">6.03</td>
	<td align="right">32.81</td>
	<td align="right">6.09</td>
	<td align="right">102,544.42</td>
	<td align="right">12,818.05</td>
	
</tr>
<tr class="udda">
	<th align="right">14:00</th>
	<td align="right">601.14</td>
	<td align="right">5.51</td>
	<td align="right">50,165,714.57</td>
	<td align="right">6.56</td>
	<td align="right">27.70</td>
	<td align="right">5.14</td>
	<td align="right">111,479.37</td>
	<td align="right">13,934.92</td>
	
</tr>
<tr class="jamn">
	<th align="right">15:00</th>
	<td align="right">564.29</td>
	<td align="right">5.17</td>
	<td align="right">38,841,424.71</td>
	<td align="right">5.08</td>
	<td align="right">27.50</td>
	<td align="right">5.10</td>
	<td align="right">86,314.28</td>
	<td align="right">10,789.28</td>
	
</tr>
<tr class="udda">
	<th align="right">16:00</th>
	<td align="right">556.14</td>
	<td align="right">5.10</td>
	<td align="right">47,583,283.29</td>
	<td align="right">6.22</td>
	<td align="right">25.23</td>
	<td align="right">4.68</td>
	<td align="right">105,740.63</td>
	<td align="right">13,217.58</td>
	
</tr>
<tr class="jamn">
	<th align="right">17:00</th>
	<td align="right">487.43</td>
	<td align="right">4.47</td>
	<td align="right">32,284,503.29</td>
	<td align="right">4.22</td>
	<td align="right">23.21</td>
	<td align="right">4.31</td>
	<td align="right">71,743.34</td>
	<td align="right">8,967.92</td>
	
</tr>
<tr class="udda">
	<th align="right">18:00</th>
	<td align="right">497.29</td>
	<td align="right">4.56</td>
	<td align="right">36,222,063.14</td>
	<td align="right">4.73</td>
	<td align="right">25.08</td>
	<td align="right">4.66</td>
	<td align="right">80,493.47</td>
	<td align="right">10,061.68</td>
	
</tr>
<tr class="jamn">
	<th align="right">19:00</th>
	<td align="right">457.57</td>
	<td align="right">4.19</td>
	<td align="right">32,750,324.29</td>
	<td align="right">4.28</td>
	<td align="right">22.75</td>
	<td align="right">4.22</td>
	<td align="right">72,778.50</td>
	<td align="right">9,097.31</td>
	
</tr>
<tr class="udda">
	<th align="right">20:00</th>
	<td align="right">518.29</td>
	<td align="right">4.75</td>
	<td align="right">42,289,649.29</td>
	<td align="right">5.53</td>
	<td align="right">24.29</td>
	<td align="right">4.51</td>
	<td align="right">93,977.00</td>
	<td align="right">11,747.12</td>
	
</tr>
<tr class="jamn">
	<th align="right">21:00</th>
	<td align="right">539.00</td>
	<td align="right">4.94</td>
	<td align="right">39,510,481.14</td>
	<td align="right">5.16</td>
	<td align="right">26.63</td>
	<td align="right">4.94</td>
	<td align="right">87,801.07</td>
	<td align="right">10,975.13</td>
	
</tr>
<tr class="udda">
	<th align="right">22:00</th>
	<td align="right">434.57</td>
	<td align="right">3.98</td>
	<td align="right">29,627,519.86</td>
	<td align="right">3.87</td>
	<td align="right">26.13</td>
	<td align="right">4.85</td>
	<td align="right">65,838.93</td>
	<td align="right">8,229.87</td>
	
</tr>
<tr class="jamn">
	<th align="right">23:00</th>
	<td align="right">330.43</td>
	<td align="right">3.03</td>
	<td align="right">17,909,299.14</td>
	<td align="right">2.34</td>
	<td align="right">24.29</td>
	<td align="right">4.51</td>
	<td align="right">39,798.44</td>
	<td align="right">4,974.81</td>
	
</tr>

</tbody>
</table>

