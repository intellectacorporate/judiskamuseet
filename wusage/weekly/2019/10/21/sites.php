


<h2>Statistics for http://www.u5645470.fsdata.se, Week of 2019-10-21 to 2019-10-27</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 20 of 3,588 Visitor Sites,
Sorted by Access Count

</h3>
<p></p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Site</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="jamnudda">
	<th class="hogerstalld">1</th>
	<td>
	46.59.25.88		
	</td>
	<td align="right">2,172</td>
	<td align="right">2.84</td>
	<td align="right">249,855,691</td>
	<td align="right">4.67</td>
	<td align="right">46</td>
	<td align="right">1.23</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	185.246.208.82		
	</td>
	<td align="right">1,952</td>
	<td align="right">2.56</td>
	<td align="right">16,199,970</td>
	<td align="right">0.30</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	85.195.116.134		
	</td>
	<td align="right">1,940</td>
	<td align="right">2.54</td>
	<td align="right">16,183,378</td>
	<td align="right">0.30</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	94.75.211.73		
	</td>
	<td align="right">1,935</td>
	<td align="right">2.53</td>
	<td align="right">16,183,549</td>
	<td align="right">0.30</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	185.93.3.92		
	</td>
	<td align="right">1,923</td>
	<td align="right">2.52</td>
	<td align="right">16,183,193</td>
	<td align="right">0.30</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	82.103.136.16		
	</td>
	<td align="right">1,805</td>
	<td align="right">2.36</td>
	<td align="right">14,920,375</td>
	<td align="right">0.28</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	185.152.65.167		
	</td>
	<td align="right">1,793</td>
	<td align="right">2.35</td>
	<td align="right">14,920,145</td>
	<td align="right">0.28</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	83.170.113.210		
	</td>
	<td align="right">1,790</td>
	<td align="right">2.34</td>
	<td align="right">14,904,059</td>
	<td align="right">0.28</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	185.39.146.215		
	</td>
	<td align="right">1,788</td>
	<td align="right">2.34</td>
	<td align="right">14,920,041</td>
	<td align="right">0.28</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">10</th>
	<td>
	46.246.122.10		
	</td>
	<td align="right">1,788</td>
	<td align="right">2.34</td>
	<td align="right">14,920,046</td>
	<td align="right">0.28</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">11</th>
	<td>
	185.136.156.82		
	</td>
	<td align="right">1,782</td>
	<td align="right">2.33</td>
	<td align="right">14,903,956</td>
	<td align="right">0.28</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">12</th>
	<td>
	63.143.42.242		
	</td>
	<td align="right">1,297</td>
	<td align="right">1.70</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">13</th>
	<td>
	185.15.93.69		
	</td>
	<td align="right">711</td>
	<td align="right">0.93</td>
	<td align="right">22,326,431</td>
	<td align="right">0.42</td>
	<td align="right">67</td>
	<td align="right">1.78</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">14</th>
	<td>
	185.6.8.7		
	</td>
	<td align="right">358</td>
	<td align="right">0.47</td>
	<td align="right">19,270,017</td>
	<td align="right">0.36</td>
	<td align="right">1</td>
	<td align="right">0.03</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">15</th>
	<td>
	148.251.182.223		
	</td>
	<td align="right">317</td>
	<td align="right">0.42</td>
	<td align="right">3,915,912</td>
	<td align="right">0.07</td>
	<td align="right">4</td>
	<td align="right">0.12</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">16</th>
	<td>
	66.249.64.174		
	</td>
	<td align="right">273</td>
	<td align="right">0.36</td>
	<td align="right">7,708,334</td>
	<td align="right">0.14</td>
	<td align="right">32</td>
	<td align="right">0.86</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">17</th>
	<td>
	212.37.28.30		
	</td>
	<td align="right">264</td>
	<td align="right">0.35</td>
	<td align="right">32,780,000</td>
	<td align="right">0.61</td>
	<td align="right">12</td>
	<td align="right">0.34</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">18</th>
	<td>
	79.142.244.252		
	</td>
	<td align="right">264</td>
	<td align="right">0.35</td>
	<td align="right">46,635,605</td>
	<td align="right">0.87</td>
	<td align="right">6</td>
	<td align="right">0.17</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">19</th>
	<td>
	66.249.64.176		
	</td>
	<td align="right">207</td>
	<td align="right">0.27</td>
	<td align="right">2,379,364</td>
	<td align="right">0.04</td>
	<td align="right">33</td>
	<td align="right">0.89</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">20</th>
	<td>
	81.16.160.45		
	</td>
	<td align="right">206</td>
	<td align="right">0.27</td>
	<td align="right">17,498,160</td>
	<td align="right">0.33</td>
	<td align="right">9</td>
	<td align="right">0.26</td>
</tr>

</tbody>
</table>

