


<h2>Statistics for http://www.u5645470.fsdata.se, Week of 2019-10-21 to 2019-10-27</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 20 of 145 Referring Sites,
Sorted by Access Count

</h3>
<p></p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Referring Site</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th class="hogerstalld">1</th>
	<td>
	<a href="https://judiskamuseet.se">
	https://judiskamuseet.se</a>
	</td>
	<td align="right">39,784</td>
	<td align="right">89.58</td>
	<td align="right">4,746,312,456</td>
	<td align="right">97.52</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	<a href="http://judiskamuseet.se">
	http://judiskamuseet.se</a>
	</td>
	<td align="right">2,011</td>
	<td align="right">4.53</td>
	<td align="right">33,982,898</td>
	<td align="right">0.70</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	<a href="https://www.google.com">
	https://www.google.com</a>
	</td>
	<td align="right">1,200</td>
	<td align="right">2.70</td>
	<td align="right">43,284,424</td>
	<td align="right">0.89</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	<a href="https://www.google.se">
	https://www.google.se</a>
	</td>
	<td align="right">575</td>
	<td align="right">1.29</td>
	<td align="right">18,663,861</td>
	<td align="right">0.38</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	<a href="https://www.google.de">
	https://www.google.de</a>
	</td>
	<td align="right">118</td>
	<td align="right">0.27</td>
	<td align="right">1,871,853</td>
	<td align="right">0.04</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	<a href="http://www.google.com">
	http://www.google.com</a>
	</td>
	<td align="right">75</td>
	<td align="right">0.17</td>
	<td align="right">611,686</td>
	<td align="right">0.01</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	<a href="http://m.facebook.com">
	http://m.facebook.com</a>
	</td>
	<td align="right">62</td>
	<td align="right">0.14</td>
	<td align="right">771,108</td>
	<td align="right">0.02</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	<a href="http://www.sverigesmuseer.se">
	http://www.sverigesmuseer.se</a>
	</td>
	<td align="right">61</td>
	<td align="right">0.14</td>
	<td align="right">694,049</td>
	<td align="right">0.01</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	<a href="https://www.bing.com">
	https://www.bing.com</a>
	</td>
	<td align="right">58</td>
	<td align="right">0.13</td>
	<td align="right">8,553,845</td>
	<td align="right">0.18</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">10</th>
	<td>
	<a href="http://site.ru">
	http://site.ru</a>
	</td>
	<td align="right">20</td>
	<td align="right">0.05</td>
	<td align="right">861,753</td>
	<td align="right">0.02</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">11</th>
	<td>
	<a href="https://m.facebook.com">
	https://m.facebook.com</a>
	</td>
	<td align="right">16</td>
	<td align="right">0.04</td>
	<td align="right">204,262</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">12</th>
	<td>
	<a href="https://%20judiskamuseet.se">
	https:// judiskamuseet.se</a>
	</td>
	<td align="right">14</td>
	<td align="right">0.03</td>
	<td align="right">145,286</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">13</th>
	<td>
	<a href="https://duckduckgo.com">
	https://duckduckgo.com</a>
	</td>
	<td align="right">13</td>
	<td align="right">0.03</td>
	<td align="right">191,550</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">14</th>
	<td>
	<a href="http://www.judiska-museet.se">
	http://www.judiska-museet.se</a>
	</td>
	<td align="right">12</td>
	<td align="right">0.03</td>
	<td align="right">140</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">15</th>
	<td>
	<a href="http://barnett.su">
	http://barnett.su</a>
	</td>
	<td align="right">12</td>
	<td align="right">0.03</td>
	<td align="right">566,340</td>
	<td align="right">0.01</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">16</th>
	<td>
	<a href="https://89.221.250.27:443">
	https://89.221.250.27:443</a>
	</td>
	<td align="right">10</td>
	<td align="right">0.02</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">17</th>
	<td>
	<a href="http://footfetish-planet.net">
	http://footfetish-planet.net</a>
	</td>
	<td align="right">9</td>
	<td align="right">0.02</td>
	<td align="right">424,755</td>
	<td align="right">0.01</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">18</th>
	<td>
	<a href="https://1news.com.ua">
	https://1news.com.ua</a>
	</td>
	<td align="right">9</td>
	<td align="right">0.02</td>
	<td align="right">424,755</td>
	<td align="right">0.01</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">19</th>
	<td>
	<a href="https://migrantsmalta.org">
	https://migrantsmalta.org</a>
	</td>
	<td align="right">9</td>
	<td align="right">0.02</td>
	<td align="right">424,755</td>
	<td align="right">0.01</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">20</th>
	<td>
	<a href="https://www.facebook.com">
	https://www.facebook.com</a>
	</td>
	<td align="right">9</td>
	<td align="right">0.02</td>
	<td align="right">114,133</td>
	<td align="right">0.00</td>
</tr>

</tbody>
</table>

