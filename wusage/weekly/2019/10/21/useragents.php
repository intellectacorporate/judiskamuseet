


<h2>Statistics for http://www.u5645470.fsdata.se, Week of 2019-10-21 to 2019-10-27</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 10 of 191 Browsers (User Agents),
Sorted by Access Count

</h3>
<p></p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Product</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th class="hogerstalld">1</th>
	<td>
	Safari/537.36		
	</td>
	<td align="right">22,009</td>
	<td align="right">28.86</td>
	<td align="right">2,173,649,566</td>
	<td align="right">40.60</td>
	<td align="right">1,190</td>
	<td align="right">31.57</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	Pingdom.com_bot_version_1.4_		
	</td>
	<td align="right">19,416</td>
	<td align="right">25.46</td>
	<td align="right">161,787,889</td>
	<td align="right">3.02</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	Safari/604.1		
	</td>
	<td align="right">11,027</td>
	<td align="right">14.46</td>
	<td align="right">1,251,106,431</td>
	<td align="right">23.37</td>
	<td align="right">597</td>
	<td align="right">15.83</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	Mozilla/5.0 		
	</td>
	<td align="right">6,188</td>
	<td align="right">8.12</td>
	<td align="right">445,076,047</td>
	<td align="right">8.31</td>
	<td align="right">690</td>
	<td align="right">18.31</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	Safari/605.1.15		
	</td>
	<td align="right">4,132</td>
	<td align="right">5.42</td>
	<td align="right">439,788,206</td>
	<td align="right">8.21</td>
	<td align="right">209</td>
	<td align="right">5.54</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	Safari/537.36 Edge/18.18362		
	</td>
	<td align="right">1,447</td>
	<td align="right">1.90</td>
	<td align="right">189,061,203</td>
	<td align="right">3.53</td>
	<td align="right">61</td>
	<td align="right">1.64</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	UptimeRobot/2.0		
	</td>
	<td align="right">1,297</td>
	<td align="right">1.70</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	bingbot/2.0		
	</td>
	<td align="right">1,273</td>
	<td align="right">1.67</td>
	<td align="right">62,819,613</td>
	<td align="right">1.17</td>
	<td align="right">239</td>
	<td align="right">6.34</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	Safari/605.1		
	</td>
	<td align="right">1,104</td>
	<td align="right">1.45</td>
	<td align="right">129,095,483</td>
	<td align="right">2.41</td>
	<td align="right">59</td>
	<td align="right">1.58</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">10</th>
	<td>
	Safari/537.36 (compatible; Googlebot/2.1;  http://www.google.com/bot.html)		
	</td>
	<td align="right">635</td>
	<td align="right">0.83</td>
	<td align="right">11,723,613</td>
	<td align="right">0.22</td>
	<td align="right">111</td>
	<td align="right">2.95</td>
</tr>

</tbody>
</table>

