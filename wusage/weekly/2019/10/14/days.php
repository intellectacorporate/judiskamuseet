


<h2>Statistics for http://www.u5645470.fsdata.se, Week of 2019-10-14 to 2019-10-20</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Traffic by Day</h3>
<img src="d.png" width="468" height="351">
<br /><br />


<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Date</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	<th scope='col'>Bits per Sec</th>
	<th scope='col'>Bytes per Sec</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th align="right">2019-10-14</th>
	<td align="right">11,841	</td>
	<td align="right">14.09	</td>
	<td align="right">816,860,548	</td>
	<td align="right">13.17	</td>
	<td align="right">516	</td>
	<td align="right">13.43	</td>
	<td align="right">75,635.24	</td>
	<td align="right">9,454.40	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-10-15</th>
	<td align="right">11,585	</td>
	<td align="right">13.79	</td>
	<td align="right">830,525,306	</td>
	<td align="right">13.39	</td>
	<td align="right">529	</td>
	<td align="right">13.77	</td>
	<td align="right">76,900.49	</td>
	<td align="right">9,612.56	</td>
</tr>
<tr class="udda">
	<th align="right">2019-10-16</th>
	<td align="right">12,432	</td>
	<td align="right">14.79	</td>
	<td align="right">1,009,401,185	</td>
	<td align="right">16.28	</td>
	<td align="right">567	</td>
	<td align="right">14.76	</td>
	<td align="right">93,463.07	</td>
	<td align="right">11,682.88	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-10-17</th>
	<td align="right">15,900	</td>
	<td align="right">18.92	</td>
	<td align="right">1,091,865,755	</td>
	<td align="right">17.61	</td>
	<td align="right">830	</td>
	<td align="right">21.61	</td>
	<td align="right">101,098.68	</td>
	<td align="right">12,637.34	</td>
</tr>
<tr class="udda">
	<th align="right">2019-10-18</th>
	<td align="right">11,580	</td>
	<td align="right">13.78	</td>
	<td align="right">910,604,461	</td>
	<td align="right">14.69	</td>
	<td align="right">529	</td>
	<td align="right">13.77	</td>
	<td align="right">84,315.23	</td>
	<td align="right">10,539.40	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-10-19</th>
	<td align="right">9,977	</td>
	<td align="right">11.87	</td>
	<td align="right">745,791,322	</td>
	<td align="right">12.03	</td>
	<td align="right">416	</td>
	<td align="right">10.83	</td>
	<td align="right">69,054.75	</td>
	<td align="right">8,631.84	</td>
</tr>
<tr class="udda">
	<th align="right">2019-10-20</th>
	<td align="right">10,715	</td>
	<td align="right">12.75	</td>
	<td align="right">795,303,949	</td>
	<td align="right">12.83	</td>
	<td align="right">454	</td>
	<td align="right">11.82	</td>
	<td align="right">73,639.25	</td>
	<td align="right">9,204.91	</td>
</tr>

</tbody>
</table>

