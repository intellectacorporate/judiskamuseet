


<h2>Statistics for http://www.u5645470.fsdata.se, Week of 2019-10-14 to 2019-10-20</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 10 of 243 Browsers (User Agents),
Sorted by Access Count

</h3>
<p></p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Product</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th class="hogerstalld">1</th>
	<td>
	Safari/537.36		
	</td>
	<td align="right">25,033</td>
	<td align="right">29.80</td>
	<td align="right">2,418,422,333</td>
	<td align="right">39.01</td>
	<td align="right">1,298</td>
	<td align="right">33.80</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	Pingdom.com_bot_version_1.4_		
	</td>
	<td align="right">19,414</td>
	<td align="right">23.11</td>
	<td align="right">162,405,494</td>
	<td align="right">2.62</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	Safari/604.1		
	</td>
	<td align="right">12,260</td>
	<td align="right">14.59</td>
	<td align="right">1,430,681,502</td>
	<td align="right">23.08</td>
	<td align="right">671</td>
	<td align="right">17.48</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	Mozilla/5.0 		
	</td>
	<td align="right">5,798</td>
	<td align="right">6.90</td>
	<td align="right">499,055,591</td>
	<td align="right">8.05</td>
	<td align="right">423</td>
	<td align="right">11.02</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	Safari/605.1.15		
	</td>
	<td align="right">4,956</td>
	<td align="right">5.90</td>
	<td align="right">514,586,151</td>
	<td align="right">8.30</td>
	<td align="right">206</td>
	<td align="right">5.36</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	Safari/537.36 Edge/18.18362		
	</td>
	<td align="right">2,102</td>
	<td align="right">2.50</td>
	<td align="right">184,290,536</td>
	<td align="right">2.97</td>
	<td align="right">71</td>
	<td align="right">1.85</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	UptimeRobot/2.0		
	</td>
	<td align="right">1,313</td>
	<td align="right">1.56</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	bingbot/2.0		
	</td>
	<td align="right">1,150</td>
	<td align="right">1.37</td>
	<td align="right">72,743,806</td>
	<td align="right">1.17</td>
	<td align="right">236</td>
	<td align="right">6.17</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	Safari/605.1		
	</td>
	<td align="right">1,109</td>
	<td align="right">1.32</td>
	<td align="right">157,419,010</td>
	<td align="right">2.54</td>
	<td align="right">54</td>
	<td align="right">1.43</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">10</th>
	<td>
	Safari/602.1		
	</td>
	<td align="right">836</td>
	<td align="right">1.00</td>
	<td align="right">112,522,732</td>
	<td align="right">1.82</td>
	<td align="right">46</td>
	<td align="right">1.20</td>
</tr>

</tbody>
</table>

