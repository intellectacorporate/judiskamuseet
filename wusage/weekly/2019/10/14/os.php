


<h2>Statistics for http://www.u5645470.fsdata.se, Week of 2019-10-14 to 2019-10-20</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 10 of 12 Operating Systems,
Sorted by Access Count

</h3>
<p></p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Product</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th class="hogerstalld">1</th>
	<td>
	Mac OS X		
	</td>
	<td align="right">51,741</td>
	<td align="right">61.59</td>
	<td align="right">5,287,239,236</td>
	<td align="right">85.29</td>
	<td align="right">2,697</td>
	<td align="right">70.22</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	Other		
	</td>
	<td align="right">27,311</td>
	<td align="right">32.51</td>
	<td align="right">545,711,820</td>
	<td align="right">8.80</td>
	<td align="right">788</td>
	<td align="right">20.52</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	Windows NT		
	</td>
	<td align="right">3,066</td>
	<td align="right">3.65</td>
	<td align="right">303,879,389</td>
	<td align="right">4.90</td>
	<td align="right">147</td>
	<td align="right">3.84</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	Linux		
	</td>
	<td align="right">1,371</td>
	<td align="right">1.63</td>
	<td align="right">40,384,739</td>
	<td align="right">0.65</td>
	<td align="right">198</td>
	<td align="right">5.17</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	Windows XP		
	</td>
	<td align="right">375</td>
	<td align="right">0.45</td>
	<td align="right">12,324,272</td>
	<td align="right">0.20</td>
	<td align="right">7</td>
	<td align="right">0.20</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	Windows Codename Longhorn		
	</td>
	<td align="right">63</td>
	<td align="right">0.07</td>
	<td align="right">6,168,201</td>
	<td align="right">0.10</td>
	<td align="right">2</td>
	<td align="right">0.06</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	Windows 2000		
	</td>
	<td align="right">34</td>
	<td align="right">0.04</td>
	<td align="right">2,190,146</td>
	<td align="right">0.04</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	Windows 2003		
	</td>
	<td align="right">16</td>
	<td align="right">0.02</td>
	<td align="right">566,340</td>
	<td align="right">0.01</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	Windows Me		
	</td>
	<td align="right">10</td>
	<td align="right">0.01</td>
	<td align="right">426,800</td>
	<td align="right">0.01</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">10</th>
	<td>
	Windows 95		
	</td>
	<td align="right">9</td>
	<td align="right">0.01</td>
	<td align="right">424,755</td>
	<td align="right">0.01</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>

</tbody>
</table>

