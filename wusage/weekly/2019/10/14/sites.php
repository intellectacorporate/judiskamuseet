


<h2>Statistics for http://www.u5645470.fsdata.se, Week of 2019-10-14 to 2019-10-20</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 20 of 3,378 Visitor Sites,
Sorted by Access Count

</h3>
<p></p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Site</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="jamnudda">
	<th class="hogerstalld">1</th>
	<td>
	46.59.25.88		
	</td>
	<td align="right">2,686</td>
	<td align="right">3.20</td>
	<td align="right">158,994,834</td>
	<td align="right">2.56</td>
	<td align="right">53</td>
	<td align="right">1.39</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	169.51.2.22		
	</td>
	<td align="right">1,952</td>
	<td align="right">2.32</td>
	<td align="right">16,224,526</td>
	<td align="right">0.26</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	82.103.139.165		
	</td>
	<td align="right">1,951</td>
	<td align="right">2.32</td>
	<td align="right">16,240,907</td>
	<td align="right">0.26</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	94.75.211.73		
	</td>
	<td align="right">1,950</td>
	<td align="right">2.32</td>
	<td align="right">16,256,577</td>
	<td align="right">0.26</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	151.106.52.134		
	</td>
	<td align="right">1,946</td>
	<td align="right">2.32</td>
	<td align="right">16,240,613</td>
	<td align="right">0.26</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	185.180.12.65		
	</td>
	<td align="right">1,945</td>
	<td align="right">2.31</td>
	<td align="right">16,208,586</td>
	<td align="right">0.26</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	85.195.116.134		
	</td>
	<td align="right">1,939</td>
	<td align="right">2.31</td>
	<td align="right">16,224,462</td>
	<td align="right">0.26</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	81.17.62.205		
	</td>
	<td align="right">1,936</td>
	<td align="right">2.30</td>
	<td align="right">16,256,764</td>
	<td align="right">0.26</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	185.93.3.92		
	</td>
	<td align="right">1,923</td>
	<td align="right">2.29</td>
	<td align="right">16,223,942</td>
	<td align="right">0.26</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">10</th>
	<td>
	178.255.155.2		
	</td>
	<td align="right">1,631</td>
	<td align="right">1.94</td>
	<td align="right">13,704,162</td>
	<td align="right">0.22</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">11</th>
	<td>
	63.143.42.242		
	</td>
	<td align="right">1,313</td>
	<td align="right">1.56</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">12</th>
	<td>
	185.246.208.82		
	</td>
	<td align="right">1,110</td>
	<td align="right">1.32</td>
	<td align="right">9,279,945</td>
	<td align="right">0.15</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">13</th>
	<td>
	5.172.196.188		
	</td>
	<td align="right">831</td>
	<td align="right">0.99</td>
	<td align="right">6,992,925</td>
	<td align="right">0.11</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">14</th>
	<td>
	85.93.88.91		
	</td>
	<td align="right">596</td>
	<td align="right">0.71</td>
	<td align="right">16,621,544</td>
	<td align="right">0.27</td>
	<td align="right">116</td>
	<td align="right">3.02</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">15</th>
	<td>
	148.160.250.14		
	</td>
	<td align="right">591</td>
	<td align="right">0.70</td>
	<td align="right">65,061,685</td>
	<td align="right">1.05</td>
	<td align="right">5</td>
	<td align="right">0.14</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">16</th>
	<td>
	185.15.93.69		
	</td>
	<td align="right">564</td>
	<td align="right">0.67</td>
	<td align="right">17,655,704</td>
	<td align="right">0.28</td>
	<td align="right">59</td>
	<td align="right">1.56</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">17</th>
	<td>
	185.6.8.3		
	</td>
	<td align="right">432</td>
	<td align="right">0.51</td>
	<td align="right">23,170,428</td>
	<td align="right">0.37</td>
	<td align="right">1</td>
	<td align="right">0.03</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">18</th>
	<td>
	188.151.113.17		
	</td>
	<td align="right">426</td>
	<td align="right">0.51</td>
	<td align="right">30,492,606</td>
	<td align="right">0.49</td>
	<td align="right">8</td>
	<td align="right">0.22</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">19</th>
	<td>
	46.4.10.166		
	</td>
	<td align="right">383</td>
	<td align="right">0.46</td>
	<td align="right">2,029,961</td>
	<td align="right">0.03</td>
	<td align="right">5</td>
	<td align="right">0.14</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">20</th>
	<td>
	52.204.97.54		
	</td>
	<td align="right">376</td>
	<td align="right">0.45</td>
	<td align="right">17,233,746</td>
	<td align="right">0.28</td>
	<td align="right">1</td>
	<td align="right">0.03</td>
</tr>

</tbody>
</table>

