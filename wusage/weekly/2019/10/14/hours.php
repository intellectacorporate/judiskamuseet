


<h2>Statistics for http://www.u5645470.fsdata.se, Week of 2019-10-14 to 2019-10-20</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Traffic by Hour</h3>
<p>Figures are averages for that hour
	of each day in the time period analyzed.</p>
<img src="h.png" width="468" height="351">
<br /><br />
<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Hour</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	<th scope='col'>Bits per Sec</th>
	<th scope='col'>Bytes per Sec</th>
	
	</tr>
</thead>
<tbody>
<tr class="jamnudda">
	<th align="right">00:00</th>
	<td align="right">296.86</td>
	<td align="right">2.47</td>
	<td align="right">16,765,776.86</td>
	<td align="right">1.89</td>
	<td align="right">10.92</td>
	<td align="right">1.99</td>
	<td align="right">37,257.28</td>
	<td align="right">4,657.16</td>
	
</tr>
<tr class="jamn">
	<th align="right">01:00</th>
	<td align="right">260.00</td>
	<td align="right">2.17</td>
	<td align="right">13,378,048.00</td>
	<td align="right">1.51</td>
	<td align="right">10.48</td>
	<td align="right">1.91</td>
	<td align="right">29,729.00</td>
	<td align="right">3,716.12</td>
	
</tr>
<tr class="udda">
	<th align="right">02:00</th>
	<td align="right">220.43</td>
	<td align="right">1.84</td>
	<td align="right">6,329,814.43</td>
	<td align="right">0.71</td>
	<td align="right">8.49</td>
	<td align="right">1.55</td>
	<td align="right">14,066.25</td>
	<td align="right">1,758.28</td>
	
</tr>
<tr class="jamn">
	<th align="right">03:00</th>
	<td align="right">196.86</td>
	<td align="right">1.64</td>
	<td align="right">4,869,681.71</td>
	<td align="right">0.55</td>
	<td align="right">9.24</td>
	<td align="right">1.68</td>
	<td align="right">10,821.51</td>
	<td align="right">1,352.69</td>
	
</tr>
<tr class="udda">
	<th align="right">04:00</th>
	<td align="right">196.71</td>
	<td align="right">1.64</td>
	<td align="right">3,872,346.71</td>
	<td align="right">0.44</td>
	<td align="right">6.61</td>
	<td align="right">1.20</td>
	<td align="right">8,605.21</td>
	<td align="right">1,075.65</td>
	
</tr>
<tr class="jamn">
	<th align="right">05:00</th>
	<td align="right">202.29</td>
	<td align="right">1.69</td>
	<td align="right">4,141,246.43</td>
	<td align="right">0.47</td>
	<td align="right">8.76</td>
	<td align="right">1.60</td>
	<td align="right">9,202.77</td>
	<td align="right">1,150.35</td>
	
</tr>
<tr class="udda">
	<th align="right">06:00</th>
	<td align="right">275.86</td>
	<td align="right">2.30</td>
	<td align="right">10,905,411.86</td>
	<td align="right">1.23</td>
	<td align="right">9.08</td>
	<td align="right">1.65</td>
	<td align="right">24,234.25</td>
	<td align="right">3,029.28</td>
	
</tr>
<tr class="jamn">
	<th align="right">07:00</th>
	<td align="right">293.71</td>
	<td align="right">2.45</td>
	<td align="right">16,230,592.57</td>
	<td align="right">1.83</td>
	<td align="right">13.84</td>
	<td align="right">2.52</td>
	<td align="right">36,067.98</td>
	<td align="right">4,508.50</td>
	
</tr>
<tr class="udda">
	<th align="right">08:00</th>
	<td align="right">505.43</td>
	<td align="right">4.21</td>
	<td align="right">39,236,930.43</td>
	<td align="right">4.43</td>
	<td align="right">16.61</td>
	<td align="right">3.03</td>
	<td align="right">87,193.18</td>
	<td align="right">10,899.15</td>
	
</tr>
<tr class="jamn">
	<th align="right">09:00</th>
	<td align="right">634.43</td>
	<td align="right">5.29</td>
	<td align="right">47,651,886.57</td>
	<td align="right">5.38</td>
	<td align="right">29.61</td>
	<td align="right">5.40</td>
	<td align="right">105,893.08</td>
	<td align="right">13,236.64</td>
	
</tr>
<tr class="udda">
	<th align="right">10:00</th>
	<td align="right">825.14</td>
	<td align="right">6.87</td>
	<td align="right">75,835,564.00</td>
	<td align="right">8.56</td>
	<td align="right">34.18</td>
	<td align="right">6.23</td>
	<td align="right">168,523.48</td>
	<td align="right">21,065.43</td>
	
</tr>
<tr class="jamn">
	<th align="right">11:00</th>
	<td align="right">788.43</td>
	<td align="right">6.57</td>
	<td align="right">66,256,512.00</td>
	<td align="right">7.48</td>
	<td align="right">32.57</td>
	<td align="right">5.94</td>
	<td align="right">147,236.69</td>
	<td align="right">18,404.59</td>
	
</tr>
<tr class="udda">
	<th align="right">12:00</th>
	<td align="right">714.29</td>
	<td align="right">5.95</td>
	<td align="right">64,258,656.43</td>
	<td align="right">7.25</td>
	<td align="right">29.08</td>
	<td align="right">5.30</td>
	<td align="right">142,797.01</td>
	<td align="right">17,849.63</td>
	
</tr>
<tr class="jamn">
	<th align="right">13:00</th>
	<td align="right">710.14</td>
	<td align="right">5.92</td>
	<td align="right">60,080,844.14</td>
	<td align="right">6.78</td>
	<td align="right">32.89</td>
	<td align="right">5.99</td>
	<td align="right">133,512.99</td>
	<td align="right">16,689.12</td>
	
</tr>
<tr class="udda">
	<th align="right">14:00</th>
	<td align="right">728.86</td>
	<td align="right">6.07</td>
	<td align="right">58,547,252.57</td>
	<td align="right">6.61</td>
	<td align="right">28.56</td>
	<td align="right">5.20</td>
	<td align="right">130,105.01</td>
	<td align="right">16,263.13</td>
	
</tr>
<tr class="jamn">
	<th align="right">15:00</th>
	<td align="right">643.43</td>
	<td align="right">5.36</td>
	<td align="right">55,545,763.00</td>
	<td align="right">6.27</td>
	<td align="right">28.90</td>
	<td align="right">5.27</td>
	<td align="right">123,435.03</td>
	<td align="right">15,429.38</td>
	
</tr>
<tr class="udda">
	<th align="right">16:00</th>
	<td align="right">716.43</td>
	<td align="right">5.97</td>
	<td align="right">56,657,818.57</td>
	<td align="right">6.40</td>
	<td align="right">31.73</td>
	<td align="right">5.78</td>
	<td align="right">125,906.26</td>
	<td align="right">15,738.28</td>
	
</tr>
<tr class="jamn">
	<th align="right">17:00</th>
	<td align="right">634.71</td>
	<td align="right">5.29</td>
	<td align="right">43,512,559.29</td>
	<td align="right">4.91</td>
	<td align="right">35.32</td>
	<td align="right">6.44</td>
	<td align="right">96,694.58</td>
	<td align="right">12,086.82</td>
	
</tr>
<tr class="udda">
	<th align="right">18:00</th>
	<td align="right">566.86</td>
	<td align="right">4.72</td>
	<td align="right">41,140,234.00</td>
	<td align="right">4.64</td>
	<td align="right">30.67</td>
	<td align="right">5.59</td>
	<td align="right">91,422.74</td>
	<td align="right">11,427.84</td>
	
</tr>
<tr class="jamn">
	<th align="right">19:00</th>
	<td align="right">577.00</td>
	<td align="right">4.81</td>
	<td align="right">49,477,635.29</td>
	<td align="right">5.59</td>
	<td align="right">29.17</td>
	<td align="right">5.32</td>
	<td align="right">109,950.30</td>
	<td align="right">13,743.79</td>
	
</tr>
<tr class="udda">
	<th align="right">20:00</th>
	<td align="right">524.14</td>
	<td align="right">4.37</td>
	<td align="right">39,038,738.00</td>
	<td align="right">4.41</td>
	<td align="right">30.25</td>
	<td align="right">5.51</td>
	<td align="right">86,752.75</td>
	<td align="right">10,844.09</td>
	
</tr>
<tr class="jamn">
	<th align="right">21:00</th>
	<td align="right">534.86</td>
	<td align="right">4.46</td>
	<td align="right">41,885,142.57</td>
	<td align="right">4.73</td>
	<td align="right">28.17</td>
	<td align="right">5.13</td>
	<td align="right">93,078.09</td>
	<td align="right">11,634.76</td>
	
</tr>
<tr class="udda">
	<th align="right">22:00</th>
	<td align="right">589.43</td>
	<td align="right">4.91</td>
	<td align="right">47,050,265.14</td>
	<td align="right">5.31</td>
	<td align="right">27.19</td>
	<td align="right">4.95</td>
	<td align="right">104,556.14</td>
	<td align="right">13,069.52</td>
	
</tr>
<tr class="jamn">
	<th align="right">23:00</th>
	<td align="right">368.00</td>
	<td align="right">3.07</td>
	<td align="right">23,095,926.00</td>
	<td align="right">2.61</td>
	<td align="right">26.40</td>
	<td align="right">4.81</td>
	<td align="right">51,324.28</td>
	<td align="right">6,415.53</td>
	
</tr>

</tbody>
</table>

