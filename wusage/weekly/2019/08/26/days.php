


<h2>Statistics for http://www.u5645470.fsdata.se, Week of 2019-8-26 to 2019-9-1</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Traffic by Day</h3>
<img src="d.png" width="468" height="351">
<br /><br />


<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Date</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	<th scope='col'>Bits per Sec</th>
	<th scope='col'>Bytes per Sec</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th align="right">2019-8-26</th>
	<td align="right">9,966	</td>
	<td align="right">17.27	</td>
	<td align="right">1,482,818,655	</td>
	<td align="right">15.23	</td>
	<td align="right">343	</td>
	<td align="right">13.24	</td>
	<td align="right">137,298.02	</td>
	<td align="right">17,162.25	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-8-27</th>
	<td align="right">8,969	</td>
	<td align="right">15.54	</td>
	<td align="right">1,474,183,505	</td>
	<td align="right">15.15	</td>
	<td align="right">413	</td>
	<td align="right">15.94	</td>
	<td align="right">136,498.47	</td>
	<td align="right">17,062.31	</td>
</tr>
<tr class="udda">
	<th align="right">2019-8-28</th>
	<td align="right">8,636	</td>
	<td align="right">14.96	</td>
	<td align="right">1,612,448,039	</td>
	<td align="right">16.57	</td>
	<td align="right">387	</td>
	<td align="right">14.94	</td>
	<td align="right">149,300.74	</td>
	<td align="right">18,662.59	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-8-29</th>
	<td align="right">9,128	</td>
	<td align="right">15.82	</td>
	<td align="right">1,734,991,489	</td>
	<td align="right">17.83	</td>
	<td align="right">372	</td>
	<td align="right">14.36	</td>
	<td align="right">160,647.36	</td>
	<td align="right">20,080.92	</td>
</tr>
<tr class="udda">
	<th align="right">2019-8-30</th>
	<td align="right">7,425	</td>
	<td align="right">12.86	</td>
	<td align="right">1,086,033,720	</td>
	<td align="right">11.16	</td>
	<td align="right">351	</td>
	<td align="right">13.55	</td>
	<td align="right">100,558.68	</td>
	<td align="right">12,569.83	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-8-31</th>
	<td align="right">6,392	</td>
	<td align="right">11.07	</td>
	<td align="right">1,101,075,099	</td>
	<td align="right">11.31	</td>
	<td align="right">359	</td>
	<td align="right">13.86	</td>
	<td align="right">101,951.40	</td>
	<td align="right">12,743.92	</td>
</tr>
<tr class="udda">
	<th align="right">2019-9-1</th>
	<td align="right">7,201	</td>
	<td align="right">12.48	</td>
	<td align="right">1,241,826,481	</td>
	<td align="right">12.76	</td>
	<td align="right">366	</td>
	<td align="right">14.13	</td>
	<td align="right">114,983.93	</td>
	<td align="right">14,372.99	</td>
</tr>

</tbody>
</table>

