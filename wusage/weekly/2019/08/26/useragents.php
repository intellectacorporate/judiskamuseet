


<h2>Statistics for http://www.u5645470.fsdata.se, Week of 2019-8-26 to 2019-9-1</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 10 of 199 Browsers (User Agents),
Sorted by Access Count

</h3>
<p></p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Product</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th class="hogerstalld">1</th>
	<td>
	Safari/537.36		
	</td>
	<td align="right">17,733</td>
	<td align="right">30.86</td>
	<td align="right">3,930,705,021</td>
	<td align="right">40.69</td>
	<td align="right">588</td>
	<td align="right">23.19</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	Safari/604.1		
	</td>
	<td align="right">13,458</td>
	<td align="right">23.42</td>
	<td align="right">2,613,797,075</td>
	<td align="right">27.06</td>
	<td align="right">465</td>
	<td align="right">18.35</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	Mozilla/5.0 		
	</td>
	<td align="right">7,165</td>
	<td align="right">12.47</td>
	<td align="right">847,290,210</td>
	<td align="right">8.77</td>
	<td align="right">207</td>
	<td align="right">8.17</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	Safari/605.1.15		
	</td>
	<td align="right">2,362</td>
	<td align="right">4.11</td>
	<td align="right">493,109,617</td>
	<td align="right">5.10</td>
	<td align="right">76</td>
	<td align="right">3.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	Safari/537.36 Edge/18.17763		
	</td>
	<td align="right">2,210</td>
	<td align="right">3.85</td>
	<td align="right">273,066,989</td>
	<td align="right">2.83</td>
	<td align="right">38</td>
	<td align="right">1.53</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	bingbot/2.0		
	</td>
	<td align="right">1,879</td>
	<td align="right">3.27</td>
	<td align="right">195,940,464</td>
	<td align="right">2.03</td>
	<td align="right">413</td>
	<td align="right">16.30</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	UptimeRobot/2.0		
	</td>
	<td align="right">1,440</td>
	<td align="right">2.51</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	Pingdom.com_bot_version_1.4_		
	</td>
	<td align="right">1,302</td>
	<td align="right">2.27</td>
	<td align="right">10,519,016</td>
	<td align="right">0.11</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	Safari/537.36 Edge/17.17134		
	</td>
	<td align="right">951</td>
	<td align="right">1.66</td>
	<td align="right">162,932,482</td>
	<td align="right">1.69</td>
	<td align="right">34</td>
	<td align="right">1.37</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">10</th>
	<td>
	Safari/537.36 (compatible; Googlebot/2.1;  http://www.google.com/bot.html)		
	</td>
	<td align="right">925</td>
	<td align="right">1.61</td>
	<td align="right">9,604,628</td>
	<td align="right">0.10</td>
	<td align="right">199</td>
	<td align="right">7.85</td>
</tr>

</tbody>
</table>

