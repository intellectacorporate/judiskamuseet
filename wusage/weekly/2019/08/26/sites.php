


<h2>Statistics for http://www.u5645470.fsdata.se, Week of 2019-8-26 to 2019-9-1</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 20 of 2,528 Visitor Sites,
Sorted by Access Count

</h3>
<p></p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Site</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="jamnudda">
	<th class="hogerstalld">1</th>
	<td>
	46.59.25.88		
	</td>
	<td align="right">2,061</td>
	<td align="right">3.57</td>
	<td align="right">275,157,137</td>
	<td align="right">2.83</td>
	<td align="right">38</td>
	<td align="right">1.50</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	63.143.42.243		
	</td>
	<td align="right">1,348</td>
	<td align="right">2.34</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	5.35.187.69		
	</td>
	<td align="right">1,030</td>
	<td align="right">1.78</td>
	<td align="right">29,642,473</td>
	<td align="right">0.30</td>
	<td align="right">9</td>
	<td align="right">0.37</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	195.196.62.48		
	</td>
	<td align="right">911</td>
	<td align="right">1.58</td>
	<td align="right">273,493,933</td>
	<td align="right">2.81</td>
	<td align="right">10</td>
	<td align="right">0.42</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	137.61.48.10		
	</td>
	<td align="right">499</td>
	<td align="right">0.86</td>
	<td align="right">40,457,341</td>
	<td align="right">0.42</td>
	<td align="right">4</td>
	<td align="right">0.17</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	89.221.250.27		
	</td>
	<td align="right">460</td>
	<td align="right">0.80</td>
	<td align="right">8,911</td>
	<td align="right">0.00</td>
	<td align="right">7</td>
	<td align="right">0.29</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	199.47.87.142		
	</td>
	<td align="right">379</td>
	<td align="right">0.66</td>
	<td align="right">3,291,174</td>
	<td align="right">0.03</td>
	<td align="right">1</td>
	<td align="right">0.04</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	193.11.31.230		
	</td>
	<td align="right">373</td>
	<td align="right">0.65</td>
	<td align="right">71,926,631</td>
	<td align="right">0.74</td>
	<td align="right">9</td>
	<td align="right">0.38</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	90.229.237.5		
	</td>
	<td align="right">348</td>
	<td align="right">0.60</td>
	<td align="right">35,179,411</td>
	<td align="right">0.36</td>
	<td align="right">5</td>
	<td align="right">0.21</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">10</th>
	<td>
	66.249.79.180		
	</td>
	<td align="right">304</td>
	<td align="right">0.53</td>
	<td align="right">4,973,029</td>
	<td align="right">0.05</td>
	<td align="right">51</td>
	<td align="right">1.98</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">11</th>
	<td>
	66.249.79.182		
	</td>
	<td align="right">280</td>
	<td align="right">0.49</td>
	<td align="right">4,833,872</td>
	<td align="right">0.05</td>
	<td align="right">54</td>
	<td align="right">2.11</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">12</th>
	<td>
	81.232.83.55		
	</td>
	<td align="right">244</td>
	<td align="right">0.42</td>
	<td align="right">24,117,462</td>
	<td align="right">0.25</td>
	<td align="right">4</td>
	<td align="right">0.17</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">13</th>
	<td>
	85.194.6.161		
	</td>
	<td align="right">240</td>
	<td align="right">0.42</td>
	<td align="right">107,873,713</td>
	<td align="right">1.11</td>
	<td align="right">8</td>
	<td align="right">0.33</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">14</th>
	<td>
	46.194.11.82		
	</td>
	<td align="right">231</td>
	<td align="right">0.40</td>
	<td align="right">25,989,316</td>
	<td align="right">0.27</td>
	<td align="right">6</td>
	<td align="right">0.25</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">15</th>
	<td>
	192.165.99.121		
	</td>
	<td align="right">225</td>
	<td align="right">0.39</td>
	<td align="right">73,357,445</td>
	<td align="right">0.75</td>
	<td align="right">5</td>
	<td align="right">0.21</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">16</th>
	<td>
	66.249.79.186		
	</td>
	<td align="right">203</td>
	<td align="right">0.35</td>
	<td align="right">13,582,411</td>
	<td align="right">0.14</td>
	<td align="right">38</td>
	<td align="right">1.48</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">17</th>
	<td>
	188.151.134.194		
	</td>
	<td align="right">198</td>
	<td align="right">0.34</td>
	<td align="right">40,514,709</td>
	<td align="right">0.42</td>
	<td align="right">1</td>
	<td align="right">0.04</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">18</th>
	<td>
	80.216.66.87		
	</td>
	<td align="right">187</td>
	<td align="right">0.32</td>
	<td align="right">32,792,124</td>
	<td align="right">0.34</td>
	<td align="right">3</td>
	<td align="right">0.13</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">19</th>
	<td>
	66.249.79.184		
	</td>
	<td align="right">182</td>
	<td align="right">0.32</td>
	<td align="right">2,128,510</td>
	<td align="right">0.02</td>
	<td align="right">40</td>
	<td align="right">1.56</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">20</th>
	<td>
	88.83.61.171		
	</td>
	<td align="right">178</td>
	<td align="right">0.31</td>
	<td align="right">42,880,651</td>
	<td align="right">0.44</td>
	<td align="right">3</td>
	<td align="right">0.13</td>
</tr>

</tbody>
</table>

