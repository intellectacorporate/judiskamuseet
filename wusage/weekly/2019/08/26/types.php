


<h2>Statistics for http://www.u5645470.fsdata.se, Week of 2019-8-26 to 2019-9-1</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 10 of 21 File Types (Extensions), 
Sorted by Access Count

</h3>
<p>Individual file types as determined by file extensions. All URLs that
do not contain an extension are counted as directories.</p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Type</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th class="hogerstalld">1</th>
	<td>
	js		
	</td>
	<td align="right">12,558</td>
	<td align="right">21.76</td>
	<td align="right">151,118,729</td>
	<td align="right">1.55</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	Directory (folder)		
	</td>
	<td align="right">12,312</td>
	<td align="right">21.33</td>
	<td align="right">120,949,283</td>
	<td align="right">1.24</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	jpg		
	</td>
	<td align="right">10,987</td>
	<td align="right">19.04</td>
	<td align="right">6,864,577,501</td>
	<td align="right">70.53</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	png		
	</td>
	<td align="right">9,255</td>
	<td align="right">16.04</td>
	<td align="right">243,730,915</td>
	<td align="right">2.50</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	css		
	</td>
	<td align="right">8,710</td>
	<td align="right">15.09</td>
	<td align="right">147,320,382</td>
	<td align="right">1.51</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	ttf		
	</td>
	<td align="right">1,174</td>
	<td align="right">2.03</td>
	<td align="right">104,265,218</td>
	<td align="right">1.07</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	php		
	</td>
	<td align="right">1,060</td>
	<td align="right">1.84</td>
	<td align="right">12,310,874</td>
	<td align="right">0.13</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	jpeg		
	</td>
	<td align="right">636</td>
	<td align="right">1.10</td>
	<td align="right">2,024,624,883</td>
	<td align="right">20.80</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	txt		
	</td>
	<td align="right">585</td>
	<td align="right">1.01</td>
	<td align="right">39,783</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">10</th>
	<td>
	gif		
	</td>
	<td align="right">205</td>
	<td align="right">0.36</td>
	<td align="right">874,852</td>
	<td align="right">0.01</td>
</tr>

</tbody>
</table>

