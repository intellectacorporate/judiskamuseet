


<h2>Statistics for http://www.u5645470.fsdata.se, Week of 2019-8-26 to 2019-9-1</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Traffic by Hour</h3>
<p>Figures are averages for that hour
	of each day in the time period analyzed.</p>
<img src="h.png" width="468" height="351">
<br /><br />
<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Hour</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	<th scope='col'>Bits per Sec</th>
	<th scope='col'>Bytes per Sec</th>
	
	</tr>
</thead>
<tbody>
<tr class="jamnudda">
	<th align="right">00:00</th>
	<td align="right">155.14</td>
	<td align="right">1.88</td>
	<td align="right">29,812,404.43</td>
	<td align="right">2.14</td>
	<td align="right">8.07</td>
	<td align="right">2.18</td>
	<td align="right">66,249.79</td>
	<td align="right">8,281.22</td>
	
</tr>
<tr class="jamn">
	<th align="right">01:00</th>
	<td align="right">85.86</td>
	<td align="right">1.04</td>
	<td align="right">7,376,613.29</td>
	<td align="right">0.53</td>
	<td align="right">8.85</td>
	<td align="right">2.39</td>
	<td align="right">16,392.47</td>
	<td align="right">2,049.06</td>
	
</tr>
<tr class="udda">
	<th align="right">02:00</th>
	<td align="right">102.14</td>
	<td align="right">1.24</td>
	<td align="right">11,931,220.86</td>
	<td align="right">0.86</td>
	<td align="right">7.61</td>
	<td align="right">2.05</td>
	<td align="right">26,513.82</td>
	<td align="right">3,314.23</td>
	
</tr>
<tr class="jamn">
	<th align="right">03:00</th>
	<td align="right">113.43</td>
	<td align="right">1.38</td>
	<td align="right">8,150,494.57</td>
	<td align="right">0.59</td>
	<td align="right">7.78</td>
	<td align="right">2.10</td>
	<td align="right">18,112.21</td>
	<td align="right">2,264.03</td>
	
</tr>
<tr class="udda">
	<th align="right">04:00</th>
	<td align="right">109.57</td>
	<td align="right">1.33</td>
	<td align="right">8,099,002.00</td>
	<td align="right">0.58</td>
	<td align="right">7.14</td>
	<td align="right">1.93</td>
	<td align="right">17,997.78</td>
	<td align="right">2,249.72</td>
	
</tr>
<tr class="jamn">
	<th align="right">05:00</th>
	<td align="right">96.57</td>
	<td align="right">1.17</td>
	<td align="right">9,136,901.71</td>
	<td align="right">0.66</td>
	<td align="right">6.84</td>
	<td align="right">1.85</td>
	<td align="right">20,304.23</td>
	<td align="right">2,538.03</td>
	
</tr>
<tr class="udda">
	<th align="right">06:00</th>
	<td align="right">114.43</td>
	<td align="right">1.39</td>
	<td align="right">14,183,914.43</td>
	<td align="right">1.02</td>
	<td align="right">8.70</td>
	<td align="right">2.35</td>
	<td align="right">31,519.81</td>
	<td align="right">3,939.98</td>
	
</tr>
<tr class="jamn">
	<th align="right">07:00</th>
	<td align="right">186.14</td>
	<td align="right">2.26</td>
	<td align="right">24,339,941.00</td>
	<td align="right">1.75</td>
	<td align="right">7.79</td>
	<td align="right">2.10</td>
	<td align="right">54,088.76</td>
	<td align="right">6,761.09</td>
	
</tr>
<tr class="udda">
	<th align="right">08:00</th>
	<td align="right">355.14</td>
	<td align="right">4.31</td>
	<td align="right">88,931,853.43</td>
	<td align="right">6.40</td>
	<td align="right">13.35</td>
	<td align="right">3.61</td>
	<td align="right">197,626.34</td>
	<td align="right">24,703.29</td>
	
</tr>
<tr class="jamn">
	<th align="right">09:00</th>
	<td align="right">593.29</td>
	<td align="right">7.20</td>
	<td align="right">106,507,603.00</td>
	<td align="right">7.66</td>
	<td align="right">21.74</td>
	<td align="right">5.87</td>
	<td align="right">236,683.56</td>
	<td align="right">29,585.45</td>
	
</tr>
<tr class="udda">
	<th align="right">10:00</th>
	<td align="right">621.43</td>
	<td align="right">7.54</td>
	<td align="right">109,180,955.86</td>
	<td align="right">7.85</td>
	<td align="right">24.70</td>
	<td align="right">6.67</td>
	<td align="right">242,624.35</td>
	<td align="right">30,328.04</td>
	
</tr>
<tr class="jamn">
	<th align="right">11:00</th>
	<td align="right">681.86</td>
	<td align="right">8.27</td>
	<td align="right">144,742,432.43</td>
	<td align="right">10.41</td>
	<td align="right">22.99</td>
	<td align="right">6.21</td>
	<td align="right">321,649.85</td>
	<td align="right">40,206.23</td>
	
</tr>
<tr class="udda">
	<th align="right">12:00</th>
	<td align="right">594.86</td>
	<td align="right">7.21</td>
	<td align="right">122,643,293.43</td>
	<td align="right">8.82</td>
	<td align="right">26.86</td>
	<td align="right">7.26</td>
	<td align="right">272,540.65</td>
	<td align="right">34,067.58</td>
	
</tr>
<tr class="jamn">
	<th align="right">13:00</th>
	<td align="right">677.43</td>
	<td align="right">8.22</td>
	<td align="right">110,959,766.14</td>
	<td align="right">7.98</td>
	<td align="right">25.65</td>
	<td align="right">6.93</td>
	<td align="right">246,577.26</td>
	<td align="right">30,822.16</td>
	
</tr>
<tr class="udda">
	<th align="right">14:00</th>
	<td align="right">619.00</td>
	<td align="right">7.51</td>
	<td align="right">91,107,005.57</td>
	<td align="right">6.55</td>
	<td align="right">24.22</td>
	<td align="right">6.54</td>
	<td align="right">202,460.01</td>
	<td align="right">25,307.50</td>
	
</tr>
<tr class="jamn">
	<th align="right">15:00</th>
	<td align="right">410.57</td>
	<td align="right">4.98</td>
	<td align="right">61,395,340.86</td>
	<td align="right">4.42</td>
	<td align="right">17.25</td>
	<td align="right">4.66</td>
	<td align="right">136,434.09</td>
	<td align="right">17,054.26</td>
	
</tr>
<tr class="udda">
	<th align="right">16:00</th>
	<td align="right">398.00</td>
	<td align="right">4.83</td>
	<td align="right">52,701,066.00</td>
	<td align="right">3.79</td>
	<td align="right">17.86</td>
	<td align="right">4.83</td>
	<td align="right">117,113.48</td>
	<td align="right">14,639.18</td>
	
</tr>
<tr class="jamn">
	<th align="right">17:00</th>
	<td align="right">366.57</td>
	<td align="right">4.45</td>
	<td align="right">62,935,905.57</td>
	<td align="right">4.53</td>
	<td align="right">17.27</td>
	<td align="right">4.66</td>
	<td align="right">139,857.57</td>
	<td align="right">17,482.20</td>
	
</tr>
<tr class="udda">
	<th align="right">18:00</th>
	<td align="right">421.14</td>
	<td align="right">5.11</td>
	<td align="right">57,482,970.43</td>
	<td align="right">4.13</td>
	<td align="right">16.22</td>
	<td align="right">4.38</td>
	<td align="right">127,739.93</td>
	<td align="right">15,967.49</td>
	
</tr>
<tr class="jamn">
	<th align="right">19:00</th>
	<td align="right">324.29</td>
	<td align="right">3.93</td>
	<td align="right">53,324,166.14</td>
	<td align="right">3.83</td>
	<td align="right">16.16</td>
	<td align="right">4.37</td>
	<td align="right">118,498.15</td>
	<td align="right">14,812.27</td>
	
</tr>
<tr class="udda">
	<th align="right">20:00</th>
	<td align="right">333.71</td>
	<td align="right">4.05</td>
	<td align="right">62,366,729.43</td>
	<td align="right">4.49</td>
	<td align="right">14.94</td>
	<td align="right">4.04</td>
	<td align="right">138,592.73</td>
	<td align="right">17,324.09</td>
	
</tr>
<tr class="jamn">
	<th align="right">21:00</th>
	<td align="right">367.00</td>
	<td align="right">4.45</td>
	<td align="right">66,819,947.29</td>
	<td align="right">4.81</td>
	<td align="right">18.03</td>
	<td align="right">4.87</td>
	<td align="right">148,488.77</td>
	<td align="right">18,561.10</td>
	
</tr>
<tr class="udda">
	<th align="right">22:00</th>
	<td align="right">303.86</td>
	<td align="right">3.69</td>
	<td align="right">47,558,591.71</td>
	<td align="right">3.42</td>
	<td align="right">13.07</td>
	<td align="right">3.53</td>
	<td align="right">105,685.76</td>
	<td align="right">13,210.72</td>
	
</tr>
<tr class="jamn">
	<th align="right">23:00</th>
	<td align="right">213.86</td>
	<td align="right">2.59</td>
	<td align="right">38,794,307.29</td>
	<td align="right">2.79</td>
	<td align="right">17.09</td>
	<td align="right">4.62</td>
	<td align="right">86,209.57</td>
	<td align="right">10,776.20</td>
	
</tr>

</tbody>
</table>

