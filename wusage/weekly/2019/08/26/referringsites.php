


<h2>Statistics for http://www.u5645470.fsdata.se, Week of 2019-8-26 to 2019-9-1</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 20 of 137 Referring Sites,
Sorted by Access Count

</h3>
<p></p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Referring Site</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th class="hogerstalld">1</th>
	<td>
	<a href="https://judiskamuseet.se">
	https://judiskamuseet.se</a>
	</td>
	<td align="right">40,782</td>
	<td align="right">88.88</td>
	<td align="right">8,564,688,756</td>
	<td align="right">95.74</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	<a href="http://judiskamuseet.se">
	http://judiskamuseet.se</a>
	</td>
	<td align="right">3,072</td>
	<td align="right">6.70</td>
	<td align="right">206,669,676</td>
	<td align="right">2.31</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	<a href="https://www.google.com">
	https://www.google.com</a>
	</td>
	<td align="right">964</td>
	<td align="right">2.10</td>
	<td align="right">91,727,294</td>
	<td align="right">1.03</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	<a href="https://www.google.se">
	https://www.google.se</a>
	</td>
	<td align="right">525</td>
	<td align="right">1.14</td>
	<td align="right">23,553,219</td>
	<td align="right">0.26</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	<a href="https://www.google.de">
	https://www.google.de</a>
	</td>
	<td align="right">100</td>
	<td align="right">0.22</td>
	<td align="right">1,511,198</td>
	<td align="right">0.02</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	<a href="http://www.google.com">
	http://www.google.com</a>
	</td>
	<td align="right">49</td>
	<td align="right">0.11</td>
	<td align="right">363,820</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	<a href="https://www.bing.com">
	https://www.bing.com</a>
	</td>
	<td align="right">42</td>
	<td align="right">0.09</td>
	<td align="right">10,450,395</td>
	<td align="right">0.12</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	<a href="http://www.judiska-museet.se">
	http://www.judiska-museet.se</a>
	</td>
	<td align="right">40</td>
	<td align="right">0.09</td>
	<td align="right">460</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	<a href="http://site.ru">
	http://site.ru</a>
	</td>
	<td align="right">36</td>
	<td align="right">0.08</td>
	<td align="right">1,515,005</td>
	<td align="right">0.02</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">10</th>
	<td>
	<a href="http://m.facebook.com">
	http://m.facebook.com</a>
	</td>
	<td align="right">34</td>
	<td align="right">0.07</td>
	<td align="right">397,686</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">11</th>
	<td>
	<a href="https://myactivity.google.com">
	https://myactivity.google.com</a>
	</td>
	<td align="right">8</td>
	<td align="right">0.02</td>
	<td align="right">10,289,631</td>
	<td align="right">0.12</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">12</th>
	<td>
	<a href="https://89.221.250.27:443">
	https://89.221.250.27:443</a>
	</td>
	<td align="right">7</td>
	<td align="right">0.02</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">13</th>
	<td>
	<a href="https://www.google.fr">
	https://www.google.fr</a>
	</td>
	<td align="right">7</td>
	<td align="right">0.02</td>
	<td align="right">6,207,374</td>
	<td align="right">0.07</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">14</th>
	<td>
	<a href="https://duckduckgo.com">
	https://duckduckgo.com</a>
	</td>
	<td align="right">6</td>
	<td align="right">0.01</td>
	<td align="right">91,866</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">15</th>
	<td>
	<a href="https://www.google.fi">
	https://www.google.fi</a>
	</td>
	<td align="right">6</td>
	<td align="right">0.01</td>
	<td align="right">84,233</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">16</th>
	<td>
	<a href="https://www.google.com.au">
	https://www.google.com.au</a>
	</td>
	<td align="right">5</td>
	<td align="right">0.01</td>
	<td align="right">37,421</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">17</th>
	<td>
	<a href="http://domain.com">
	http://domain.com</a>
	</td>
	<td align="right">5</td>
	<td align="right">0.01</td>
	<td align="right">8,115,903</td>
	<td align="right">0.09</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">18</th>
	<td>
	<a href="http://blogg.slaktingar.se">
	http://blogg.slaktingar.se</a>
	</td>
	<td align="right">5</td>
	<td align="right">0.01</td>
	<td align="right">16,171</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">19</th>
	<td>
	<a href="https://silastroy.com">
	https://silastroy.com</a>
	</td>
	<td align="right">4</td>
	<td align="right">0.01</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">20</th>
	<td>
	<a href="http://www.google.se">
	http://www.google.se</a>
	</td>
	<td align="right">4</td>
	<td align="right">0.01</td>
	<td align="right">16,044</td>
	<td align="right">0.00</td>
</tr>

</tbody>
</table>

