


<h2>Statistics for http://www.u5645470.fsdata.se, Week of 2019-8-12 to 2019-8-18</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Traffic by Day</h3>
<img src="d.png" width="468" height="351">
<br /><br />


<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Date</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	<th scope='col'>Bits per Sec</th>
	<th scope='col'>Bytes per Sec</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th align="right">2019-8-12</th>
	<td align="right">9,890	</td>
	<td align="right">19.98	</td>
	<td align="right">1,522,417,574	</td>
	<td align="right">17.82	</td>
	<td align="right">327	</td>
	<td align="right">14.27	</td>
	<td align="right">140,964.59	</td>
	<td align="right">17,620.57	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-8-13</th>
	<td align="right">7,644	</td>
	<td align="right">15.44	</td>
	<td align="right">1,432,328,755	</td>
	<td align="right">16.77	</td>
	<td align="right">334	</td>
	<td align="right">14.57	</td>
	<td align="right">132,623.03	</td>
	<td align="right">16,577.88	</td>
</tr>
<tr class="udda">
	<th align="right">2019-8-14</th>
	<td align="right">7,461	</td>
	<td align="right">15.07	</td>
	<td align="right">1,188,627,308	</td>
	<td align="right">13.92	</td>
	<td align="right">427	</td>
	<td align="right">18.63	</td>
	<td align="right">110,058.08	</td>
	<td align="right">13,757.26	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-8-15</th>
	<td align="right">6,362	</td>
	<td align="right">12.85	</td>
	<td align="right">1,056,809,952	</td>
	<td align="right">12.37	</td>
	<td align="right">378	</td>
	<td align="right">16.49	</td>
	<td align="right">97,852.77	</td>
	<td align="right">12,231.60	</td>
</tr>
<tr class="udda">
	<th align="right">2019-8-16</th>
	<td align="right">6,749	</td>
	<td align="right">13.63	</td>
	<td align="right">1,331,893,141	</td>
	<td align="right">15.59	</td>
	<td align="right">283	</td>
	<td align="right">12.35	</td>
	<td align="right">123,323.44	</td>
	<td align="right">15,415.43	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-8-17</th>
	<td align="right">4,928	</td>
	<td align="right">9.96	</td>
	<td align="right">743,361,592	</td>
	<td align="right">8.70	</td>
	<td align="right">251	</td>
	<td align="right">10.95	</td>
	<td align="right">68,829.78	</td>
	<td align="right">8,603.72	</td>
</tr>
<tr class="udda">
	<th align="right">2019-8-18</th>
	<td align="right">6,465	</td>
	<td align="right">13.06	</td>
	<td align="right">1,266,006,231	</td>
	<td align="right">14.82	</td>
	<td align="right">292	</td>
	<td align="right">12.74	</td>
	<td align="right">117,222.80	</td>
	<td align="right">14,652.85	</td>
</tr>

</tbody>
</table>

