


<h2>Statistics for http://www.u5645470.fsdata.se, Week of 2019-8-12 to 2019-8-18</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 10 of 166 Browsers (User Agents),
Sorted by Access Count

</h3>
<p></p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Product</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th class="hogerstalld">1</th>
	<td>
	Safari/537.36		
	</td>
	<td align="right">14,717</td>
	<td align="right">29.93</td>
	<td align="right">3,264,736,014</td>
	<td align="right">38.50</td>
	<td align="right">555</td>
	<td align="right">24.70</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	Safari/604.1		
	</td>
	<td align="right">12,009</td>
	<td align="right">24.42</td>
	<td align="right">2,222,994,379</td>
	<td align="right">26.22</td>
	<td align="right">442</td>
	<td align="right">19.68</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	Mozilla/5.0 		
	</td>
	<td align="right">5,263</td>
	<td align="right">10.70</td>
	<td align="right">1,024,282,766</td>
	<td align="right">12.08</td>
	<td align="right">315</td>
	<td align="right">14.02</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	Safari/537.36 Edge/18.17763		
	</td>
	<td align="right">3,137</td>
	<td align="right">6.38</td>
	<td align="right">186,634,819</td>
	<td align="right">2.20</td>
	<td align="right">15</td>
	<td align="right">0.67</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	Safari/605.1.15		
	</td>
	<td align="right">2,656</td>
	<td align="right">5.40</td>
	<td align="right">503,447,983</td>
	<td align="right">5.94</td>
	<td align="right">90</td>
	<td align="right">4.04</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	bingbot/2.0		
	</td>
	<td align="right">1,688</td>
	<td align="right">3.43</td>
	<td align="right">291,000,206</td>
	<td align="right">3.43</td>
	<td align="right">385</td>
	<td align="right">17.13</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	UptimeRobot/2.0		
	</td>
	<td align="right">1,286</td>
	<td align="right">2.61</td>
	<td align="right">88,127</td>
	<td align="right">0.00</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	Pingdom.com_bot_version_1.4_		
	</td>
	<td align="right">1,257</td>
	<td align="right">2.56</td>
	<td align="right">9,552,977</td>
	<td align="right">0.11</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	Safari/605.1		
	</td>
	<td align="right">830</td>
	<td align="right">1.69</td>
	<td align="right">153,182,500</td>
	<td align="right">1.81</td>
	<td align="right">32</td>
	<td align="right">1.45</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">10</th>
	<td>
	Safari/537.36 Edge/17.17134		
	</td>
	<td align="right">674</td>
	<td align="right">1.37</td>
	<td align="right">138,095,259</td>
	<td align="right">1.63</td>
	<td align="right">26</td>
	<td align="right">1.16</td>
</tr>

</tbody>
</table>

