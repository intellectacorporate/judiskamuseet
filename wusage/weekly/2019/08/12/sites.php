


<h2>Statistics for http://www.u5645470.fsdata.se, Week of 2019-8-12 to 2019-8-18</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 20 of 2,429 Visitor Sites,
Sorted by Access Count

</h3>
<p></p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Site</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="jamnudda">
	<th class="hogerstalld">1</th>
	<td>
	46.59.25.88		
	</td>
	<td align="right">3,786</td>
	<td align="right">7.65</td>
	<td align="right">300,186,491</td>
	<td align="right">3.51</td>
	<td align="right">39</td>
	<td align="right">1.74</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	63.143.42.243		
	</td>
	<td align="right">1,286</td>
	<td align="right">2.60</td>
	<td align="right">88,127</td>
	<td align="right">0.00</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	89.221.250.27		
	</td>
	<td align="right">473</td>
	<td align="right">0.96</td>
	<td align="right">9,235</td>
	<td align="right">0.00</td>
	<td align="right">9</td>
	<td align="right">0.43</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	90.229.237.5		
	</td>
	<td align="right">409</td>
	<td align="right">0.83</td>
	<td align="right">16,137,140</td>
	<td align="right">0.19</td>
	<td align="right">1</td>
	<td align="right">0.05</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	46.194.25.4		
	</td>
	<td align="right">221</td>
	<td align="right">0.45</td>
	<td align="right">27,228,557</td>
	<td align="right">0.32</td>
	<td align="right">7</td>
	<td align="right">0.34</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	213.50.33.196		
	</td>
	<td align="right">190</td>
	<td align="right">0.38</td>
	<td align="right">38,640,704</td>
	<td align="right">0.45</td>
	<td align="right">4</td>
	<td align="right">0.19</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	46.39.100.114		
	</td>
	<td align="right">184</td>
	<td align="right">0.37</td>
	<td align="right">91,460,346</td>
	<td align="right">1.07</td>
	<td align="right">3</td>
	<td align="right">0.14</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	82.196.178.67		
	</td>
	<td align="right">178</td>
	<td align="right">0.36</td>
	<td align="right">98,056,572</td>
	<td align="right">1.15</td>
	<td align="right">4</td>
	<td align="right">0.19</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	88.83.61.171		
	</td>
	<td align="right">167</td>
	<td align="right">0.34</td>
	<td align="right">53,597,481</td>
	<td align="right">0.63</td>
	<td align="right">2</td>
	<td align="right">0.10</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">10</th>
	<td>
	79.138.36.49		
	</td>
	<td align="right">155</td>
	<td align="right">0.31</td>
	<td align="right">61,964,763</td>
	<td align="right">0.73</td>
	<td align="right">4</td>
	<td align="right">0.19</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">11</th>
	<td>
	94.254.62.236		
	</td>
	<td align="right">151</td>
	<td align="right">0.31</td>
	<td align="right">31,127,088</td>
	<td align="right">0.36</td>
	<td align="right">3</td>
	<td align="right">0.14</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">12</th>
	<td>
	24.253.61.78		
	</td>
	<td align="right">145</td>
	<td align="right">0.29</td>
	<td align="right">8,603,537</td>
	<td align="right">0.10</td>
	<td align="right">5</td>
	<td align="right">0.23</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">13</th>
	<td>
	85.194.6.161		
	</td>
	<td align="right">142</td>
	<td align="right">0.29</td>
	<td align="right">28,039,367</td>
	<td align="right">0.33</td>
	<td align="right">7</td>
	<td align="right">0.33</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">14</th>
	<td>
	188.151.135.3		
	</td>
	<td align="right">140</td>
	<td align="right">0.28</td>
	<td align="right">15,566,482</td>
	<td align="right">0.18</td>
	<td align="right">6</td>
	<td align="right">0.28</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">15</th>
	<td>
	217.211.76.92		
	</td>
	<td align="right">137</td>
	<td align="right">0.28</td>
	<td align="right">54,033,425</td>
	<td align="right">0.63</td>
	<td align="right">2</td>
	<td align="right">0.09</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">16</th>
	<td>
	81.233.146.10		
	</td>
	<td align="right">133</td>
	<td align="right">0.27</td>
	<td align="right">22,083,427</td>
	<td align="right">0.26</td>
	<td align="right">3</td>
	<td align="right">0.14</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">17</th>
	<td>
	81.234.95.44		
	</td>
	<td align="right">132</td>
	<td align="right">0.27</td>
	<td align="right">27,122,148</td>
	<td align="right">0.32</td>
	<td align="right">3</td>
	<td align="right">0.14</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">18</th>
	<td>
	81.17.62.205		
	</td>
	<td align="right">130</td>
	<td align="right">0.26</td>
	<td align="right">976,839</td>
	<td align="right">0.01</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">19</th>
	<td>
	185.180.12.65		
	</td>
	<td align="right">128</td>
	<td align="right">0.26</td>
	<td align="right">933,416</td>
	<td align="right">0.01</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">20</th>
	<td>
	94.247.174.83		
	</td>
	<td align="right">128</td>
	<td align="right">0.26</td>
	<td align="right">962,065</td>
	<td align="right">0.01</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>

</tbody>
</table>

