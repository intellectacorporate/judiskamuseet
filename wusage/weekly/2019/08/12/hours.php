


<h2>Statistics for http://www.u5645470.fsdata.se, Week of 2019-8-12 to 2019-8-18</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Traffic by Hour</h3>
<p>Figures are averages for that hour
	of each day in the time period analyzed.</p>
<img src="h.png" width="468" height="351">
<br /><br />
<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Hour</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	<th scope='col'>Bits per Sec</th>
	<th scope='col'>Bytes per Sec</th>
	
	</tr>
</thead>
<tbody>
<tr class="jamnudda">
	<th align="right">00:00</th>
	<td align="right">151.14</td>
	<td align="right">2.14</td>
	<td align="right">20,330,212.43</td>
	<td align="right">1.67</td>
	<td align="right">5.44</td>
	<td align="right">1.66</td>
	<td align="right">45,178.25</td>
	<td align="right">5,647.28</td>
	
</tr>
<tr class="jamn">
	<th align="right">01:00</th>
	<td align="right">66.14</td>
	<td align="right">0.94</td>
	<td align="right">10,915,754.43</td>
	<td align="right">0.89</td>
	<td align="right">7.15</td>
	<td align="right">2.18</td>
	<td align="right">24,257.23</td>
	<td align="right">3,032.15</td>
	
</tr>
<tr class="udda">
	<th align="right">02:00</th>
	<td align="right">54.71</td>
	<td align="right">0.77</td>
	<td align="right">6,084,522.00</td>
	<td align="right">0.50</td>
	<td align="right">5.29</td>
	<td align="right">1.62</td>
	<td align="right">13,521.16</td>
	<td align="right">1,690.14</td>
	
</tr>
<tr class="jamn">
	<th align="right">03:00</th>
	<td align="right">92.00</td>
	<td align="right">1.30</td>
	<td align="right">5,164,351.71</td>
	<td align="right">0.42</td>
	<td align="right">6.20</td>
	<td align="right">1.89</td>
	<td align="right">11,476.34</td>
	<td align="right">1,434.54</td>
	
</tr>
<tr class="udda">
	<th align="right">04:00</th>
	<td align="right">96.71</td>
	<td align="right">1.37</td>
	<td align="right">8,122,262.14</td>
	<td align="right">0.67</td>
	<td align="right">7.82</td>
	<td align="right">2.39</td>
	<td align="right">18,049.47</td>
	<td align="right">2,256.18</td>
	
</tr>
<tr class="jamn">
	<th align="right">05:00</th>
	<td align="right">71.43</td>
	<td align="right">1.01</td>
	<td align="right">11,466,379.86</td>
	<td align="right">0.94</td>
	<td align="right">6.07</td>
	<td align="right">1.85</td>
	<td align="right">25,480.84</td>
	<td align="right">3,185.11</td>
	
</tr>
<tr class="udda">
	<th align="right">06:00</th>
	<td align="right">93.43</td>
	<td align="right">1.32</td>
	<td align="right">15,571,022.29</td>
	<td align="right">1.28</td>
	<td align="right">7.13</td>
	<td align="right">2.18</td>
	<td align="right">34,602.27</td>
	<td align="right">4,325.28</td>
	
</tr>
<tr class="jamn">
	<th align="right">07:00</th>
	<td align="right">191.14</td>
	<td align="right">2.70</td>
	<td align="right">29,157,271.43</td>
	<td align="right">2.39</td>
	<td align="right">9.35</td>
	<td align="right">2.85</td>
	<td align="right">64,793.94</td>
	<td align="right">8,099.24</td>
	
</tr>
<tr class="udda">
	<th align="right">08:00</th>
	<td align="right">308.43</td>
	<td align="right">4.36</td>
	<td align="right">39,698,255.71</td>
	<td align="right">3.25</td>
	<td align="right">13.84</td>
	<td align="right">4.23</td>
	<td align="right">88,218.35</td>
	<td align="right">11,027.29</td>
	
</tr>
<tr class="jamn">
	<th align="right">09:00</th>
	<td align="right">455.14</td>
	<td align="right">6.44</td>
	<td align="right">75,718,961.14</td>
	<td align="right">6.21</td>
	<td align="right">15.38</td>
	<td align="right">4.70</td>
	<td align="right">168,264.36</td>
	<td align="right">21,033.04</td>
	
</tr>
<tr class="udda">
	<th align="right">10:00</th>
	<td align="right">586.43</td>
	<td align="right">8.29</td>
	<td align="right">86,799,878.71</td>
	<td align="right">7.11</td>
	<td align="right">19.16</td>
	<td align="right">5.85</td>
	<td align="right">192,888.62</td>
	<td align="right">24,111.08</td>
	
</tr>
<tr class="jamn">
	<th align="right">11:00</th>
	<td align="right">509.43</td>
	<td align="right">7.20</td>
	<td align="right">105,428,578.71</td>
	<td align="right">8.64</td>
	<td align="right">19.33</td>
	<td align="right">5.90</td>
	<td align="right">234,285.73</td>
	<td align="right">29,285.72</td>
	
</tr>
<tr class="udda">
	<th align="right">12:00</th>
	<td align="right">432.57</td>
	<td align="right">6.12</td>
	<td align="right">64,010,488.57</td>
	<td align="right">5.25</td>
	<td align="right">19.17</td>
	<td align="right">5.86</td>
	<td align="right">142,245.53</td>
	<td align="right">17,780.69</td>
	
</tr>
<tr class="jamn">
	<th align="right">13:00</th>
	<td align="right">506.43</td>
	<td align="right">7.16</td>
	<td align="right">84,900,163.00</td>
	<td align="right">6.96</td>
	<td align="right">19.13</td>
	<td align="right">5.84</td>
	<td align="right">188,667.03</td>
	<td align="right">23,583.38</td>
	
</tr>
<tr class="udda">
	<th align="right">14:00</th>
	<td align="right">483.14</td>
	<td align="right">6.83</td>
	<td align="right">103,684,665.57</td>
	<td align="right">8.50</td>
	<td align="right">20.43</td>
	<td align="right">6.24</td>
	<td align="right">230,410.37</td>
	<td align="right">28,801.30</td>
	
</tr>
<tr class="jamn">
	<th align="right">15:00</th>
	<td align="right">490.71</td>
	<td align="right">6.94</td>
	<td align="right">81,706,146.86</td>
	<td align="right">6.70</td>
	<td align="right">19.32</td>
	<td align="right">5.90</td>
	<td align="right">181,569.22</td>
	<td align="right">22,696.15</td>
	
</tr>
<tr class="udda">
	<th align="right">16:00</th>
	<td align="right">370.00</td>
	<td align="right">5.23</td>
	<td align="right">80,523,619.00</td>
	<td align="right">6.60</td>
	<td align="right">17.93</td>
	<td align="right">5.48</td>
	<td align="right">178,941.38</td>
	<td align="right">22,367.67</td>
	
</tr>
<tr class="jamn">
	<th align="right">17:00</th>
	<td align="right">316.43</td>
	<td align="right">4.47</td>
	<td align="right">53,871,515.00</td>
	<td align="right">4.41</td>
	<td align="right">16.19</td>
	<td align="right">4.94</td>
	<td align="right">119,714.48</td>
	<td align="right">14,964.31</td>
	
</tr>
<tr class="udda">
	<th align="right">18:00</th>
	<td align="right">301.14</td>
	<td align="right">4.26</td>
	<td align="right">48,034,245.14</td>
	<td align="right">3.94</td>
	<td align="right">16.46</td>
	<td align="right">5.03</td>
	<td align="right">106,742.77</td>
	<td align="right">13,342.85</td>
	
</tr>
<tr class="jamn">
	<th align="right">19:00</th>
	<td align="right">276.29</td>
	<td align="right">3.91</td>
	<td align="right">48,487,155.14</td>
	<td align="right">3.97</td>
	<td align="right">12.43</td>
	<td align="right">3.80</td>
	<td align="right">107,749.23</td>
	<td align="right">13,468.65</td>
	
</tr>
<tr class="udda">
	<th align="right">20:00</th>
	<td align="right">257.57</td>
	<td align="right">3.64</td>
	<td align="right">53,336,399.57</td>
	<td align="right">4.37</td>
	<td align="right">10.68</td>
	<td align="right">3.26</td>
	<td align="right">118,525.33</td>
	<td align="right">14,815.67</td>
	
</tr>
<tr class="jamn">
	<th align="right">21:00</th>
	<td align="right">370.14</td>
	<td align="right">5.23</td>
	<td align="right">83,312,810.43</td>
	<td align="right">6.83</td>
	<td align="right">14.23</td>
	<td align="right">4.34</td>
	<td align="right">185,139.58</td>
	<td align="right">23,142.45</td>
	
</tr>
<tr class="udda">
	<th align="right">22:00</th>
	<td align="right">344.57</td>
	<td align="right">4.87</td>
	<td align="right">68,105,762.57</td>
	<td align="right">5.58</td>
	<td align="right">17.99</td>
	<td align="right">5.49</td>
	<td align="right">151,346.14</td>
	<td align="right">18,918.27</td>
	
</tr>
<tr class="jamn">
	<th align="right">23:00</th>
	<td align="right">246.14</td>
	<td align="right">3.48</td>
	<td align="right">35,775,943.29</td>
	<td align="right">2.93</td>
	<td align="right">21.32</td>
	<td align="right">6.51</td>
	<td align="right">79,502.10</td>
	<td align="right">9,937.76</td>
	
</tr>

</tbody>
</table>

