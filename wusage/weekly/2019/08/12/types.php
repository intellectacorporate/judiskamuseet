


<h2>Statistics for http://www.u5645470.fsdata.se, Week of 2019-8-12 to 2019-8-18</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 10 of 20 File Types (Extensions), 
Sorted by Access Count

</h3>
<p>Individual file types as determined by file extensions. All URLs that
do not contain an extension are counted as directories.</p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Type</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th class="hogerstalld">1</th>
	<td>
	Directory (folder)		
	</td>
	<td align="right">10,475</td>
	<td align="right">21.16</td>
	<td align="right">99,441,004</td>
	<td align="right">1.16</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	js		
	</td>
	<td align="right">10,442</td>
	<td align="right">21.10</td>
	<td align="right">136,487,428</td>
	<td align="right">1.60</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	jpg		
	</td>
	<td align="right">9,037</td>
	<td align="right">18.26</td>
	<td align="right">6,583,276,105</td>
	<td align="right">77.07</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	css		
	</td>
	<td align="right">7,430</td>
	<td align="right">15.01</td>
	<td align="right">132,924,975</td>
	<td align="right">1.56</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	png		
	</td>
	<td align="right">6,903</td>
	<td align="right">13.95</td>
	<td align="right">443,003,812</td>
	<td align="right">5.19</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	php		
	</td>
	<td align="right">2,625</td>
	<td align="right">5.30</td>
	<td align="right">50,577,981</td>
	<td align="right">0.59</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	ttf		
	</td>
	<td align="right">1,076</td>
	<td align="right">2.17</td>
	<td align="right">95,761,624</td>
	<td align="right">1.12</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	jpeg		
	</td>
	<td align="right">550</td>
	<td align="right">1.11</td>
	<td align="right">930,034,916</td>
	<td align="right">10.89</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	txt		
	</td>
	<td align="right">486</td>
	<td align="right">0.98</td>
	<td align="right">33,050</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">10</th>
	<td>
	svg		
	</td>
	<td align="right">150</td>
	<td align="right">0.30</td>
	<td align="right">2,474,353</td>
	<td align="right">0.03</td>
</tr>

</tbody>
</table>

