


<h2>Statistics for http://www.u5645470.fsdata.se, Week of 2019-8-12 to 2019-8-18</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 9 of 9 Operating Systems,
Sorted by Access Count

</h3>
<p></p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Product</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th class="hogerstalld">1</th>
	<td>
	Mac OS X		
	</td>
	<td align="right">38,062</td>
	<td align="right">77.39</td>
	<td align="right">7,128,827,402</td>
	<td align="right">84.07</td>
	<td align="right">1,363</td>
	<td align="right">59.50</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	Other		
	</td>
	<td align="right">6,884</td>
	<td align="right">14.00</td>
	<td align="right">553,110,262</td>
	<td align="right">6.52</td>
	<td align="right">654</td>
	<td align="right">28.56</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	Windows NT		
	</td>
	<td align="right">3,837</td>
	<td align="right">7.80</td>
	<td align="right">758,826,565</td>
	<td align="right">8.95</td>
	<td align="right">261</td>
	<td align="right">11.41</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	Linux		
	</td>
	<td align="right">319</td>
	<td align="right">0.65</td>
	<td align="right">13,353,225</td>
	<td align="right">0.16</td>
	<td align="right">9</td>
	<td align="right">0.43</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	Windows Codename Longhorn		
	</td>
	<td align="right">49</td>
	<td align="right">0.10</td>
	<td align="right">23,583,485</td>
	<td align="right">0.28</td>
	<td align="right">1</td>
	<td align="right">0.05</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	Windows XP		
	</td>
	<td align="right">15</td>
	<td align="right">0.03</td>
	<td align="right">1,666,170</td>
	<td align="right">0.02</td>
	<td align="right">1</td>
	<td align="right">0.05</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	Windows 2003		
	</td>
	<td align="right">6</td>
	<td align="right">0.01</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	Mac OS		
	</td>
	<td align="right">5</td>
	<td align="right">0.01</td>
	<td align="right">3,919</td>
	<td align="right">0.00</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	Windows 2000		
	</td>
	<td align="right">2</td>
	<td align="right">0.00</td>
	<td align="right">64,113</td>
	<td align="right">0.00</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>

</tbody>
</table>

