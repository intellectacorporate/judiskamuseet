


<h2>Statistics for http://www.u5645470.fsdata.se, Week of 2019-8-12 to 2019-8-18</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 20 of 112 Referring Sites,
Sorted by Access Count

</h3>
<p></p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Referring Site</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="jamnudda">
	<th class="hogerstalld">1</th>
	<td>
	<a href="https://judiskamuseet.se">
	https://judiskamuseet.se</a>
	</td>
	<td align="right">34,998</td>
	<td align="right">89.47</td>
	<td align="right">7,664,432,525</td>
	<td align="right">97.12</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	<a href="http://judiskamuseet.se">
	http://judiskamuseet.se</a>
	</td>
	<td align="right">2,372</td>
	<td align="right">6.06</td>
	<td align="right">115,272,437</td>
	<td align="right">1.46</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	<a href="https://www.google.com">
	https://www.google.com</a>
	</td>
	<td align="right">770</td>
	<td align="right">1.97</td>
	<td align="right">52,947,409</td>
	<td align="right">0.67</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	<a href="https://www.google.se">
	https://www.google.se</a>
	</td>
	<td align="right">454</td>
	<td align="right">1.16</td>
	<td align="right">28,519,800</td>
	<td align="right">0.36</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	<a href="https://www.google.de">
	https://www.google.de</a>
	</td>
	<td align="right">99</td>
	<td align="right">0.25</td>
	<td align="right">1,609,554</td>
	<td align="right">0.02</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	<a href="http://www.google.com">
	http://www.google.com</a>
	</td>
	<td align="right">84</td>
	<td align="right">0.21</td>
	<td align="right">694,756</td>
	<td align="right">0.01</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	<a href="https://www.bing.com">
	https://www.bing.com</a>
	</td>
	<td align="right">41</td>
	<td align="right">0.10</td>
	<td align="right">19,874,950</td>
	<td align="right">0.25</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	<a href="http://www.judiska-museet.se">
	http://www.judiska-museet.se</a>
	</td>
	<td align="right">22</td>
	<td align="right">0.06</td>
	<td align="right">499</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	<a href="http://m.facebook.com">
	http://m.facebook.com</a>
	</td>
	<td align="right">20</td>
	<td align="right">0.05</td>
	<td align="right">232,117</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">10</th>
	<td>
	<a href="https://duckduckgo.com">
	https://duckduckgo.com</a>
	</td>
	<td align="right">13</td>
	<td align="right">0.03</td>
	<td align="right">186,384</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">11</th>
	<td>
	<a href="http://www.stockholmmuseums.se">
	http://www.stockholmmuseums.se</a>
	</td>
	<td align="right">12</td>
	<td align="right">0.03</td>
	<td align="right">75,104</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">12</th>
	<td>
	<a href="https://89.221.250.27:443">
	https://89.221.250.27:443</a>
	</td>
	<td align="right">11</td>
	<td align="right">0.03</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">13</th>
	<td>
	<a href="http://l.instagram.com">
	http://l.instagram.com</a>
	</td>
	<td align="right">8</td>
	<td align="right">0.02</td>
	<td align="right">73,881</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">14</th>
	<td>
	<a href="http://l.facebook.com">
	http://l.facebook.com</a>
	</td>
	<td align="right">8</td>
	<td align="right">0.02</td>
	<td align="right">59,279</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">15</th>
	<td>
	<a href="android-app://com.google.android.gm">
	android-app://com.google.android.gm</a>
	</td>
	<td align="right">7</td>
	<td align="right">0.02</td>
	<td align="right">74,322</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">16</th>
	<td>
	<a href="https://sv.m.wikipedia.org">
	https://sv.m.wikipedia.org</a>
	</td>
	<td align="right">7</td>
	<td align="right">0.02</td>
	<td align="right">59,860</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">17</th>
	<td>
	<a href="https://www.facebook.com">
	https://www.facebook.com</a>
	</td>
	<td align="right">7</td>
	<td align="right">0.02</td>
	<td align="right">77,416</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">18</th>
	<td>
	<a href="https://herbcom.su">
	https://herbcom.su</a>
	</td>
	<td align="right">6</td>
	<td align="right">0.02</td>
	<td align="right">195,147</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">19</th>
	<td>
	<a href="https://myactivity.google.com">
	https://myactivity.google.com</a>
	</td>
	<td align="right">6</td>
	<td align="right">0.02</td>
	<td align="right">2,577,741</td>
	<td align="right">0.03</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">20</th>
	<td>
	<a href="https://www.youtube.com">
	https://www.youtube.com</a>
	</td>
	<td align="right">6</td>
	<td align="right">0.02</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>

</tbody>
</table>

