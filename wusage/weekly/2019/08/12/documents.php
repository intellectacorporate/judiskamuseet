

<script>
var componentsData = new Array(
        
        "*end*"
);
var componentsByPage = new Array();
var epochRange = "";
var sortedByAccesses = 0;
var sortedByBytes = 0;
var sortedByVisits = 0;
var alphabetized = 0;
var withBytes = 0;
var withVisits = 0;
var withDownloads = 0;
var subreportKeyword = "";
var subreportTitle = "";
var subreportAll = 0;
var subreportTop = 0;
var subreportTotal = 0;
var subreportPie = "";
var subreportGreenbarFlag = 0;
var pc = 0, ac = 0;
var dataEnd = 0;
var data;
var dataArrays = new Array(
	new Array(
		"Week of 2019-8-12 to 2019-8-18",	
		"*sorted-by-accesses*",
		"*bytes*",
		"*visits*",
		"*subreport*",
	"pages",
	"Documents",
	"*top*",
		30,
	
	"*total*",
	"2,207",
	"*pie*",
	"",
	"*row*",
	"1",
	"/",
	"*local-link*",
		"http://www.u5645470.fsdata.se/",
	
	"5,277",
	"10.66",
	"39,061,894",
		"0.46",
	"913",
		"2.16",
	"*row*",
	"2",
	"/wp-content/themes/Divi/core/admin/js/common.js",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/themes/Divi/core/admin/js/common.js",
	
	"1,165",
	"2.35",
	"645,910",
		"0.01",
	"1,210",
		"2.86",
	"*row*",
	"3",
	"/wp-content/themes/Divi/js/custom.min.js",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/themes/Divi/js/custom.min.js",
	
	"1,160",
	"2.34",
	"71,387,679",
		"0.84",
	"1,204",
		"2.84",
	"*row*",
	"4",
	"/wp-content/plugins/mailchimp-for-wp/assets/js/forms-api.min<br>\n.js",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/plugins/mailchimp-for-wp/assets/js/forms-api.min.js",
	
	"1,154",
	"2.33",
	"8,493,510",
		"0.10",
	"1,201",
		"2.84",
	"*row*",
	"5",
	"/wp-content/themes/divi-child/style.css",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/themes/divi-child/style.css",
	
	"1,153",
	"2.33",
	"9,701,148",
		"0.11",
	"1,193",
		"2.82",
	"*row*",
	"6",
	"/wp-content/themes/Divi/style.css",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/themes/Divi/style.css",
	
	"1,148",
	"2.32",
	"83,206,214",
		"0.97",
	"1,193",
		"2.82",
	"*row*",
	"7",
	"/wp-includes/css/dist/block-library/style.min.css",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-includes/css/dist/block-library/style.min.css",
	
	"1,141",
	"2.31",
	"5,403,090",
		"0.06",
	"1,163",
		"2.75",
	"*row*",
	"8",
	"/wp-includes/css/dashicons.min.css",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-includes/css/dashicons.min.css",
	
	"1,132",
	"2.29",
	"31,785,065",
		"0.37",
	"1,169",
		"2.76",
	"*row*",
	"9",
	"/wp-content/plugins/sitepress-multilingual-cms/templates/lan<br>\nguage-switchers/menu-item/style.css",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/plugins/sitepress-multilingual-cms/templates/language-switchers/menu-item/style.css",
	
	"1,130",
	"2.28",
	"154,412",
		"0.00",
	"1,171",
		"2.77",
	"*row*",
	"10",
	"/wp-content/plugins/wp-pagenavi/pagenavi-css.css",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/plugins/wp-pagenavi/pagenavi-css.css",
	
	"1,112",
	"2.25",
	"263,710",
		"0.00",
	"1,150",
		"2.72",
	"*row*",
	"11",
	"/wp-content/uploads/2017/06/Judiska_centr_svart_web-1.png",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/uploads/2017/06/Judiska_centr_svart_web-1.png",
	
	"1,103",
	"2.23",
	"9,967,930",
		"0.12",
	"1,132",
		"2.67",
	"*row*",
	"12",
	"/wp-includes/js/jquery/jquery.js",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-includes/js/jquery/jquery.js",
	
	"1,103",
	"2.23",
	"36,705,602",
		"0.43",
	"1,152",
		"2.72",
	"*row*",
	"13",
	"/wp-content/themes/divi-child/js/divi-child.js",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/themes/divi-child/js/divi-child.js",
	
	"1,100",
	"2.22",
	"980,131",
		"0.01",
	"1,152",
		"2.72",
	"*row*",
	"14",
	"/wp-includes/js/wp-embed.min.js",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-includes/js/wp-embed.min.js",
	
	"1,098",
	"2.22",
	"816,799",
		"0.01",
	"1,148",
		"2.71",
	"*row*",
	"15",
	"/wp-includes/js/jquery/jquery-migrate.min.js",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-includes/js/jquery/jquery-migrate.min.js",
	
	"1,091",
	"2.20",
	"4,363,260",
		"0.05",
	"1,136",
		"2.69",
	"*row*",
	"16",
	"/wp-content/themes/divi-child/img/Instagram.png",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/themes/divi-child/img/Instagram.png",
	
	"1,088",
	"2.20",
	"6,518,338",
		"0.08",
	"1,133",
		"2.68",
	"*row*",
	"17",
	"/wp-includes/js/wp-emoji-release.min.js",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-includes/js/wp-emoji-release.min.js",
	
	"1,083",
	"2.19",
	"4,936,296",
		"0.06",
	"1,129",
		"2.67",
	"*row*",
	"18",
	"/wp-content/themes/divi-child/img/facebook.png",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/themes/divi-child/img/facebook.png",
	
	"1,070",
	"2.16",
	"5,134,272",
		"0.06",
	"1,120",
		"2.65",
	"*row*",
	"19",
	"/wp-content/themes/Divi/core/admin/fonts/modules.ttf",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/themes/Divi/core/admin/fonts/modules.ttf",
	
	"1,066",
	"2.15",
	"95,634,000",
		"1.12",
	"1,127",
		"2.66",
	"*row*",
	"20",
	"/wp-content/themes/divi-child/img/location.png",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/themes/divi-child/img/location.png",
	
	"1,054",
	"2.13",
	"3,209,754",
		"0.04",
	"1,107",
		"2.62",
	"*row*",
	"21",
	"/wp-admin/admin-ajax.php",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-admin/admin-ajax.php",
	
	"1,042",
	"2.11",
	"19,134,470",
		"0.22",
	"27",
		"0.06",
	"*row*",
	"22",
	"/wp-login.php",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-login.php",
	
	"797",
	"1.61",
	"1,032,954",
		"0.01",
	"167",
		"0.39",
	"*row*",
	"23",
	"/wp-content/uploads/2019/05/cropped-Judiska_svart-2-32x32.pn<br>\ng",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/uploads/2019/05/cropped-Judiska_svart-2-32x32.png",
	
	"732",
	"1.48",
	"549,612",
		"0.01",
	"724",
		"1.71",
	"*row*",
	"24",
	"/wp-content/uploads/2019/05/cropped-Judiska_svart-2-192x192.<br>\npng",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/uploads/2019/05/cropped-Judiska_svart-2-192x192.png",
	
	"669",
	"1.35",
	"4,398,166",
		"0.05",
	"667",
		"1.58",
	"*row*",
	"25",
	"/wp-content/uploads/2019/06/till-webb_4-1.jpg",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/uploads/2019/06/till-webb_4-1.jpg",
	
	"665",
	"1.34",
	"66,550,614",
		"0.78",
	"702",
		"1.66",
	"*row*",
	"26",
	"/wp-content/uploads/2019/02/pngkey.com-instagram-png-21590-1<br>\n50x150.png",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/uploads/2019/02/pngkey.com-instagram-png-21590-150x150.png",
	
	"644",
	"1.30",
	"4,522,248",
		"0.05",
	"682",
		"1.61",
	"*row*",
	"27",
	"/wp-content/uploads/2019/06/sommarlov.jpg",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/uploads/2019/06/sommarlov.jpg",
	
	"633",
	"1.28",
	"5,568,537",
		"0.07",
	"673",
		"1.59",
	"*row*",
	"28",
	"/wp-content/uploads/2019/07/mönster.jpg",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/uploads/2019/07/mönster.jpg",
	
	"627",
	"1.27",
	"100,031,922",
		"1.17",
	"669",
		"1.58",
	"*row*",
	"29",
	"/wp-content/uploads/2019/06/DSF7700_edit.jpg",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/uploads/2019/06/DSF7700_edit.jpg",
	
	"562",
	"1.14",
	"194,967,461",
		"2.28",
	"599",
		"1.42",
	"*row*",
	"30",
	"/robots.txt",
	"*local-link*",
		"http://www.u5645470.fsdata.se/robots.txt",
	
	"486",
	"0.98",
	"33,050",
		"0.00",
	"175",
		"0.41",
	
		"*end*"
		)
	);

function incPC()
{
	pc++;
	if (pc == data.length) {
		if ((ac == dataArrays.length) || (dataArrays[ac] == "*end*")) {
			dataEnd = 1;
		} else {
			ac++;
			data = dataArrays[ac];
			pc = 0;
		}
	}	
}

function resetPC()
{
	pc = 0;
	ac = 0;
	if (dataArrays.length) {
		data = dataArrays[0];
		dataEnd = 0;
	} else {	
		dataEnd = 1;
	}
}

function compileReports()
{
	resetPC();
	compileDocuments();
}

function compileDocuments()
{
	var s = data[pc];
	epochRange = data[pc];
	incPC();
	while (dataEnd == 0) {
		var s = data[pc];
		if (s == "*sorted-by-accesses*") {
			sortedByAccesses = 1;
			incPC();
		} else if (s == "*sorted-by-bytes*") {
			sortedByBytes = 1;
			incPC();
		} else if (s == "*sorted-by-visits*") {
			sortedByVisits = 1;
			incPC();
		} else if (s == "*alphabetized*") {
			alphabetized = 1;
			incPC();
		} else if (s == "*bytes*") {
			withBytes = 1;
			incPC();
		} else if (s == "*visits*") {
			withVisits = 1;
			incPC();
		} else if (s == "*downloads*") {
			withDownloads = 1;
			incPC();
		} else {
			break;
		}
	}
	documentsHead();
	while (dataEnd == 0) {
		if (compileSubreport() == 0) {
			// No more subreports. That's OK 
			break;
		}	
	}
	documentsTail();
	return 1;
}
function compileSubreport()
{
	if (data[pc] != "*subreport*") {
		return 0;
	}
	incPC();	
	subreportKeyword = "";
	subreportTitle = "";
	subreportAll = 0;
	subreportTop = 0;
	subreportTotal = 0;
	subreportPie = "";
	subreportKeyword = data[pc];
	incPC();
	subreportTitle = data[pc];
	incPC();
	if (data[pc] == "*all*") {
		incPC();
		subreportAll = 1;
	}
	if (data[pc] == "*top*") {
		incPC();
		subreportTop = data[pc];
		incPC();
	}
	if (data[pc] == "*total*") {
		incPC();
		subreportTotal = data[pc];
		incPC();
	}
	if (data[pc] == "*pie*") {
		incPC();
		subreportPie = data[pc];
		incPC();
	}
	subreportHead();
	while (dataEnd == 0) {
		if (compileSubreportRow() == 0) {
			// No more subreport rows. That's OK 
			break;
		}
	}
	subreportTail();
	return 1;
}
var rowRank = 0;
var rowItem = "";
var rowLocalLink = "";
var rowTitle = "";
var rowReferrers = "";
var rowAccesses = 0;
var rowAccessesPer = 0;
var rowBytes = 0;
var rowBytesPer = 0;
var rowVisits = 0;
var rowVisitsPer = 0;
var rowDownloadsPer = 0;
var rowComponents = 0;

function compileSubreportRow()
{
	if (data[pc] != "*row*") {
		return 0;
	}
	rowRank = 0;
	rowItem = "";
	rowLocalLink = "";
	rowTitle = "";
	rowReferrers = "";
	rowComponents = 0;
	incPC();
	rowRank = data[pc];
	incPC();
	rowItem = data[pc];
	incPC();
	if (data[pc] == "*local-link*") {
		incPC();
		rowLocalLink = data[pc];
		incPC();
	}
	if (data[pc] == "*title*") {
		incPC();
		rowTitle = data[pc];
		incPC();
	}
	if (data[pc] == "*referrers*") {
		incPC();
		rowReferrers = data[pc];
		incPC();
	}
	if (data[pc] == "*components*") {
		if (compileComponentList() == 0) {
			return 0;
		}
	}		
	rowAccesses = data[pc];	
	incPC();
	rowAccessesPer = data[pc];	
	incPC();
	if (withBytes) {
		rowBytes = data[pc];
		incPC();
		rowBytesPer = data[pc];
		incPC();
	}
	if (withVisits) {
		rowVisits = data[pc];
		incPC();
		rowVisitsPer = data[pc];
		incPC();
	}
	if (withDownloads) {
		rowDownloadsPer = data[pc];
		incPC();
	}
	subreportRow();	
	return 1;
}

function compileComponentList()
{
	if (data[pc] != "*components*") {
		return 0;
	}
	incPC();
	var alreadyFirst = componentsByPage[rowItem + ".1"];
	while (dataEnd == 0) {
		if (data[pc] == "*end*") {
			incPC();
			return 1;
		}
		rowComponents++;
		var index = data[pc];
		incPC();
		if (!alreadyFirst) {
			addComponent(rowItem, index);
		}
	}
	return -1;
}

function listComponents(w, item)
{
	var i = 1;
	while (1) {
		var s = componentsByPage[item + "." + i];
		if (!s) {
			return;
		}
		var offset = s * 5;
		var name = componentsData[offset++];
		var accesses = componentsData[offset++];
		var bytes = componentsData[offset++];
		var visits = componentsData[offset++];
		var downloadsper = componentsData[offset++];
		w.document.writeln("<td>");
		w.document.writeln(name);
		w.document.writeln("</td>");
		w.document.writeln("<td align=right>");
		w.document.writeln(accesses);
		w.document.writeln("</td>");
		if (withBytes) {
			w.document.writeln("<td align=right>");
			w.document.writeln(bytes);
			w.document.writeln("</td>");
		}	
		if (withVisits) {
			w.document.writeln("<td align=right>");
			w.document.writeln(visits);
			w.document.writeln("</td>");
		}	
		if (withDownloads) {
			w.document.writeln("<td align=right>");
			w.document.writeln(downloadsper);
			w.document.writeln("</td>");
		}	
		w.document.writeln("</tr>");
		i++;
	}
}

function addComponent(item, index)
{
	var i = 1;
	while (1) {
		var s = componentsByPage[item + "." + i];
		if (!s) {
			break;
		}
		i++;
	}
	componentsByPage[item + "." + i] = index;
}

function wl(s)
{
	document.writeln(s);
}
function documentsHead()
{
	wl("<h2>" + epochRange + "</h2>");
	

wl("<h3>Menu</h3>");

wl("<div class='colmenu'>");
wl("<div class='column'>");
wl("<ul id='infolankar'>");
wl("<li><a href=\"../../../../index.php\">Calendar of Reports</a></li>");
wl("<li><a href=\"index.php\">Executive Summary and Totals</a></li>");
wl("<li><a href=\"hours.php\">Accesses by Hour</a></li>");
wl("<li><a href=\"days.php\">Accesses by Day</a></li>");
wl("<li><a href=\"documents.php\">Top Documents</a></li>");
wl("<li><a href=\"directories.php\">Documents by Directory</a></li>");
wl("<li><a href=\"types.php\">File Types (Extensions)</a></li>");

wl("</ul>");
wl("</div>");
wl("<div class='column'>");
wl("<ul id='infolankar'>");
wl("<li><a href=\"sites.php\">Top Visitor Sites</a></li>");
wl("<li><a href=\"useragents.php\">Top Web Browsers</a></li>");
wl("<li><a href=\"os.php\">Top Operating Systems</a></li>");
wl("<li><a href=\"referringsites.php\">Top Referring Sites</a></li>");
wl("<li><a href=\"notfound.php\">Documents Not Found</a></li>");
wl("<li><a href=\"resultcodes.php\">Accesses by Result Code</a></li>");

wl("</ul>");
wl("</div>");
wl("<div class='colmenufoot'></div>");
wl("</div>");

}
function documentsTail()
{
}
function subreportHead()
{
	wl("<h3><a name=\"" + subreportKeyword + "\">");
	if (subreportAll) {
		wl("All " + subreportTitle);
	} else {
		wl("Top " + subreportTop + " of " + subreportTotal + " " + subreportTitle + ", ");
	}
        if (sortedByAccesses) {
                wl("Sorted by Access Count");
        } else if (sortedByBytes) {
                wl("Sorted by Bytes");
        } else if (sortedByVisits) {
                wl("Sorted by Visits");
        }
        if (alphabetized) {
                wl("and Alphabetized");
        }
	wl("</a></h3><p></p>"); 

	if (subreportPie) {
		wl("<div align=\"center\">");
		wl(subreportPie);
		wl("</div>");
	}
	wl("<table cellspacing=\"0\" class=\"styladtabell\">");
	wl("<thead>");
	wl("<tr>");
	wl("<th scope=\"col\">Rank</th>");
	wl("<th scope=\"col\">Item</th>");
	wl("<th scope=\"col\">Accesses</th>");
	wl("<th scope=\"col\">%</th>");
	if (withBytes) {
		wl("<th scope=\"col\">Bytes</th>");
		wl("<th scope=\"col\">%</th>");
	}
	if (withVisits) {
		wl("<th scope=\"col\">Visits</th>");
		wl("<th scope=\"col\">%</th>");
	}
	wl("</tr>");
	wl("</thead>");
	wl("<tbody>");
}	
function subreportTail()
{
	wl("</tbody>");
	wl("</table>");
}
function subreportRow()
{
        var gc = "jamn";
        var dirLink = 0;
        if (subreportGreenbarFlag) {
                gc = "udda";
        }
        subreportGreenbarFlag = !subreportGreenbarFlag;
	wl("<tr class=\"" + gc + "\">");
	wl("<th class=\"hogerstalld\">" + rowRank + "</th>");
	wl("<td align=\"right\">");
	if (rowLocalLink) {
		wl("<a href=\"" + rowLocalLink + "\">");
	}
	if (rowTitle) {
		wl("<b>" + rowTitle + "</b><br>");
	}
	wl(rowItem);
	if (rowLocalLink) {
		wl("</a>");
	}	
	if (rowReferrers) {
		wl("<a href=\"" + rowReferrers + "\" target=\"referrers\">[Referrers]</a>");
	}
	wl("</td>");
	wl("<td align=\"right\">" + rowAccesses + "</td>");
	wl("<td align=\"right\">" + rowAccessesPer + "</td>");
	if (withBytes) {
		wl("<td align=\"right\">" + rowBytes + "</td>");
		wl("<td align=\"right\">" + rowBytesPer + "</td>");
	}
	if (withVisits) {
		wl("<td align=\"right\">" + rowVisits + "</td>");
		wl("<td align=\"right\">" + rowVisitsPer + "</td>");
	}
	if (withDownloads) {
		wl("<td align=\"right\">" + rowDownloadsPer + "</td>");
	}
	wl("</tr>");
}

compileReports(0);	
</script>
