


<h2>Statistics for http://www.u5645470.fsdata.se, Week of 2019-8-19 to 2019-8-25</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 20 of 2,820 Visitor Sites,
Sorted by Access Count

</h3>
<p></p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Site</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="jamnudda">
	<th class="hogerstalld">1</th>
	<td>
	46.59.25.88		
	</td>
	<td align="right">4,165</td>
	<td align="right">5.51</td>
	<td align="right">433,030,852</td>
	<td align="right">3.54</td>
	<td align="right">49</td>
	<td align="right">1.77</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	5.35.187.69		
	</td>
	<td align="right">2,455</td>
	<td align="right">3.25</td>
	<td align="right">101,537,647</td>
	<td align="right">0.83</td>
	<td align="right">4</td>
	<td align="right">0.15</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	63.143.42.243		
	</td>
	<td align="right">1,323</td>
	<td align="right">1.75</td>
	<td align="right">1</td>
	<td align="right">0.00</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	88.83.61.171		
	</td>
	<td align="right">510</td>
	<td align="right">0.67</td>
	<td align="right">215,144,894</td>
	<td align="right">1.76</td>
	<td align="right">9</td>
	<td align="right">0.34</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	95.216.20.54		
	</td>
	<td align="right">352</td>
	<td align="right">0.47</td>
	<td align="right">3,315,018</td>
	<td align="right">0.03</td>
	<td align="right">1</td>
	<td align="right">0.04</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	89.221.250.27		
	</td>
	<td align="right">339</td>
	<td align="right">0.45</td>
	<td align="right">30,344</td>
	<td align="right">0.00</td>
	<td align="right">7</td>
	<td align="right">0.27</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	138.201.36.87		
	</td>
	<td align="right">314</td>
	<td align="right">0.42</td>
	<td align="right">3,823,047</td>
	<td align="right">0.03</td>
	<td align="right">3</td>
	<td align="right">0.11</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	90.229.237.5		
	</td>
	<td align="right">307</td>
	<td align="right">0.41</td>
	<td align="right">52,384,174</td>
	<td align="right">0.43</td>
	<td align="right">9</td>
	<td align="right">0.34</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	66.249.64.18		
	</td>
	<td align="right">274</td>
	<td align="right">0.36</td>
	<td align="right">8,371,972</td>
	<td align="right">0.07</td>
	<td align="right">44</td>
	<td align="right">1.60</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">10</th>
	<td>
	82.196.178.67		
	</td>
	<td align="right">269</td>
	<td align="right">0.36</td>
	<td align="right">47,768,969</td>
	<td align="right">0.39</td>
	<td align="right">10</td>
	<td align="right">0.36</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">11</th>
	<td>
	207.244.157.10		
	</td>
	<td align="right">218</td>
	<td align="right">0.29</td>
	<td align="right">1,341,449</td>
	<td align="right">0.01</td>
	<td align="right">2</td>
	<td align="right">0.08</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">12</th>
	<td>
	81.234.166.203		
	</td>
	<td align="right">213</td>
	<td align="right">0.28</td>
	<td align="right">2,834,928</td>
	<td align="right">0.02</td>
	<td align="right">2</td>
	<td align="right">0.07</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">13</th>
	<td>
	90.231.218.243		
	</td>
	<td align="right">194</td>
	<td align="right">0.26</td>
	<td align="right">25,981,821</td>
	<td align="right">0.21</td>
	<td align="right">4</td>
	<td align="right">0.15</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">14</th>
	<td>
	66.249.64.19		
	</td>
	<td align="right">184</td>
	<td align="right">0.24</td>
	<td align="right">2,196,723</td>
	<td align="right">0.02</td>
	<td align="right">31</td>
	<td align="right">1.12</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">15</th>
	<td>
	213.113.65.93		
	</td>
	<td align="right">177</td>
	<td align="right">0.23</td>
	<td align="right">30,439,890</td>
	<td align="right">0.25</td>
	<td align="right">3</td>
	<td align="right">0.11</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">16</th>
	<td>
	37.46.169.77		
	</td>
	<td align="right">176</td>
	<td align="right">0.23</td>
	<td align="right">58,175,052</td>
	<td align="right">0.47</td>
	<td align="right">4</td>
	<td align="right">0.15</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">17</th>
	<td>
	84.216.177.156		
	</td>
	<td align="right">176</td>
	<td align="right">0.23</td>
	<td align="right">66,555,782</td>
	<td align="right">0.54</td>
	<td align="right">2</td>
	<td align="right">0.07</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">18</th>
	<td>
	94.255.219.157		
	</td>
	<td align="right">162</td>
	<td align="right">0.21</td>
	<td align="right">23,083,730</td>
	<td align="right">0.19</td>
	<td align="right">4</td>
	<td align="right">0.15</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">19</th>
	<td>
	66.249.64.29		
	</td>
	<td align="right">159</td>
	<td align="right">0.21</td>
	<td align="right">1,213,760</td>
	<td align="right">0.01</td>
	<td align="right">25</td>
	<td align="right">0.92</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">20</th>
	<td>
	185.154.111.24		
	</td>
	<td align="right">158</td>
	<td align="right">0.21</td>
	<td align="right">29,360,800</td>
	<td align="right">0.24</td>
	<td align="right">3</td>
	<td align="right">0.11</td>
</tr>

</tbody>
</table>

