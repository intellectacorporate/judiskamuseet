


<h2>Statistics for http://www.u5645470.fsdata.se, Week of 2019-8-19 to 2019-8-25</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Traffic by Day</h3>
<img src="d.png" width="468" height="351">
<br /><br />


<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Date</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	<th scope='col'>Bits per Sec</th>
	<th scope='col'>Bytes per Sec</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th align="right">2019-8-19</th>
	<td align="right">9,737	</td>
	<td align="right">12.88	</td>
	<td align="right">1,856,397,057	</td>
	<td align="right">15.16	</td>
	<td align="right">356	</td>
	<td align="right">12.66	</td>
	<td align="right">171,888.62	</td>
	<td align="right">21,486.08	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-8-20</th>
	<td align="right">7,354	</td>
	<td align="right">9.73	</td>
	<td align="right">1,294,166,952	</td>
	<td align="right">10.57	</td>
	<td align="right">290	</td>
	<td align="right">10.31	</td>
	<td align="right">119,830.27	</td>
	<td align="right">14,978.78	</td>
</tr>
<tr class="udda">
	<th align="right">2019-8-21</th>
	<td align="right">7,749	</td>
	<td align="right">10.25	</td>
	<td align="right">1,405,415,383	</td>
	<td align="right">11.47	</td>
	<td align="right">293	</td>
	<td align="right">10.42	</td>
	<td align="right">130,131.05	</td>
	<td align="right">16,266.38	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-8-22</th>
	<td align="right">22,868	</td>
	<td align="right">30.25	</td>
	<td align="right">3,614,401,645	</td>
	<td align="right">29.51	</td>
	<td align="right">790	</td>
	<td align="right">28.09	</td>
	<td align="right">334,666.82	</td>
	<td align="right">41,833.35	</td>
</tr>
<tr class="udda">
	<th align="right">2019-8-23</th>
	<td align="right">12,330	</td>
	<td align="right">16.31	</td>
	<td align="right">1,710,375,444	</td>
	<td align="right">13.96	</td>
	<td align="right">391	</td>
	<td align="right">13.90	</td>
	<td align="right">158,368.10	</td>
	<td align="right">19,796.01	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-8-24</th>
	<td align="right">8,149	</td>
	<td align="right">10.78	</td>
	<td align="right">1,272,619,921	</td>
	<td align="right">10.39	</td>
	<td align="right">340	</td>
	<td align="right">12.09	</td>
	<td align="right">117,835.18	</td>
	<td align="right">14,729.40	</td>
</tr>
<tr class="udda">
	<th align="right">2019-8-25</th>
	<td align="right">7,401	</td>
	<td align="right">9.79	</td>
	<td align="right">1,094,366,843	</td>
	<td align="right">8.94	</td>
	<td align="right">352	</td>
	<td align="right">12.52	</td>
	<td align="right">101,330.26	</td>
	<td align="right">12,666.28	</td>
</tr>

</tbody>
</table>

