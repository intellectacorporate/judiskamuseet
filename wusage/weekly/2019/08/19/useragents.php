


<h2>Statistics for http://www.u5645470.fsdata.se, Week of 2019-8-19 to 2019-8-25</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 10 of 197 Browsers (User Agents),
Sorted by Access Count

</h3>
<p></p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Product</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th class="hogerstalld">1</th>
	<td>
	Safari/537.36		
	</td>
	<td align="right">19,316</td>
	<td align="right">25.61</td>
	<td align="right">3,996,278,497</td>
	<td align="right">32.65</td>
	<td align="right">719</td>
	<td align="right">25.69</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	Safari/604.1		
	</td>
	<td align="right">18,865</td>
	<td align="right">25.01</td>
	<td align="right">3,387,151,292</td>
	<td align="right">27.67</td>
	<td align="right">619</td>
	<td align="right">22.13</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	Mozilla/5.0 		
	</td>
	<td align="right">13,154</td>
	<td align="right">17.44</td>
	<td align="right">1,771,150,785</td>
	<td align="right">14.47</td>
	<td align="right">281</td>
	<td align="right">10.04</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	Safari/537.36 Edge/18.17763		
	</td>
	<td align="right">4,278</td>
	<td align="right">5.67</td>
	<td align="right">481,999,101</td>
	<td align="right">3.94</td>
	<td align="right">39</td>
	<td align="right">1.43</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	Safari/605.1.15		
	</td>
	<td align="right">3,466</td>
	<td align="right">4.59</td>
	<td align="right">703,944,887</td>
	<td align="right">5.75</td>
	<td align="right">107</td>
	<td align="right">3.82</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	bingbot/2.0		
	</td>
	<td align="right">1,617</td>
	<td align="right">2.14</td>
	<td align="right">255,528,053</td>
	<td align="right">2.09</td>
	<td align="right">360</td>
	<td align="right">12.88</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	Safari/537.36 Edge/17.17134		
	</td>
	<td align="right">1,568</td>
	<td align="right">2.08</td>
	<td align="right">312,179,695</td>
	<td align="right">2.55</td>
	<td align="right">54</td>
	<td align="right">1.96</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	UptimeRobot/2.0		
	</td>
	<td align="right">1,324</td>
	<td align="right">1.76</td>
	<td align="right">15,947</td>
	<td align="right">0.00</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	Pingdom.com_bot_version_1.4_		
	</td>
	<td align="right">1,287</td>
	<td align="right">1.71</td>
	<td align="right">9,896,341</td>
	<td align="right">0.08</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">10</th>
	<td>
	Safari/537.36 Edge/18.18362		
	</td>
	<td align="right">1,190</td>
	<td align="right">1.58</td>
	<td align="right">172,532,827</td>
	<td align="right">1.41</td>
	<td align="right">32</td>
	<td align="right">1.14</td>
</tr>

</tbody>
</table>

