


<h2>Statistics for http://www.u5645470.fsdata.se, Week of 2019-8-19 to 2019-8-25</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 10 of 18 File Types (Extensions), 
Sorted by Access Count

</h3>
<p>Individual file types as determined by file extensions. All URLs that
do not contain an extension are counted as directories.</p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Type</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th class="hogerstalld">1</th>
	<td>
	js		
	</td>
	<td align="right">19,356</td>
	<td align="right">25.61</td>
	<td align="right">208,283,244</td>
	<td align="right">1.70</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	Directory (folder)		
	</td>
	<td align="right">13,048</td>
	<td align="right">17.26</td>
	<td align="right">130,894,643</td>
	<td align="right">1.07</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	jpg		
	</td>
	<td align="right">12,892</td>
	<td align="right">17.06</td>
	<td align="right">6,322,006,249</td>
	<td align="right">51.62</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	css		
	</td>
	<td align="right">12,491</td>
	<td align="right">16.53</td>
	<td align="right">191,017,093</td>
	<td align="right">1.56</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	png		
	</td>
	<td align="right">10,712</td>
	<td align="right">14.17</td>
	<td align="right">2,766,725,714</td>
	<td align="right">22.59</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	php		
	</td>
	<td align="right">3,196</td>
	<td align="right">4.23</td>
	<td align="right">55,909,358</td>
	<td align="right">0.46</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	ttf		
	</td>
	<td align="right">1,511</td>
	<td align="right">2.00</td>
	<td align="right">135,347,409</td>
	<td align="right">1.11</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	jpeg		
	</td>
	<td align="right">852</td>
	<td align="right">1.13</td>
	<td align="right">2,403,578,300</td>
	<td align="right">19.62</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	gif		
	</td>
	<td align="right">572</td>
	<td align="right">0.76</td>
	<td align="right">2,621,048</td>
	<td align="right">0.02</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">10</th>
	<td>
	txt		
	</td>
	<td align="right">510</td>
	<td align="right">0.67</td>
	<td align="right">34,804</td>
	<td align="right">0.00</td>
</tr>

</tbody>
</table>

