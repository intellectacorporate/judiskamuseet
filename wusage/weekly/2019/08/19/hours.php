


<h2>Statistics for http://www.u5645470.fsdata.se, Week of 2019-8-19 to 2019-8-25</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Traffic by Hour</h3>
<p>Figures are averages for that hour
	of each day in the time period analyzed.</p>
<img src="h.png" width="468" height="351">
<br /><br />
<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Hour</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	<th scope='col'>Bits per Sec</th>
	<th scope='col'>Bytes per Sec</th>
	
	</tr>
</thead>
<tbody>
<tr class="jamnudda">
	<th align="right">00:00</th>
	<td align="right">163.43</td>
	<td align="right">1.51</td>
	<td align="right">27,047,689.29</td>
	<td align="right">1.55</td>
	<td align="right">5.66</td>
	<td align="right">1.41</td>
	<td align="right">60,105.98</td>
	<td align="right">7,513.25</td>
	
</tr>
<tr class="jamn">
	<th align="right">01:00</th>
	<td align="right">78.29</td>
	<td align="right">0.72</td>
	<td align="right">6,547,520.00</td>
	<td align="right">0.37</td>
	<td align="right">7.53</td>
	<td align="right">1.87</td>
	<td align="right">14,550.04</td>
	<td align="right">1,818.76</td>
	
</tr>
<tr class="udda">
	<th align="right">02:00</th>
	<td align="right">95.71</td>
	<td align="right">0.89</td>
	<td align="right">10,650,200.00</td>
	<td align="right">0.61</td>
	<td align="right">4.96</td>
	<td align="right">1.23</td>
	<td align="right">23,667.11</td>
	<td align="right">2,958.39</td>
	
</tr>
<tr class="jamn">
	<th align="right">03:00</th>
	<td align="right">81.29</td>
	<td align="right">0.75</td>
	<td align="right">7,521,116.14</td>
	<td align="right">0.43</td>
	<td align="right">6.16</td>
	<td align="right">1.53</td>
	<td align="right">16,713.59</td>
	<td align="right">2,089.20</td>
	
</tr>
<tr class="udda">
	<th align="right">04:00</th>
	<td align="right">113.14</td>
	<td align="right">1.05</td>
	<td align="right">13,996,109.43</td>
	<td align="right">0.80</td>
	<td align="right">5.81</td>
	<td align="right">1.45</td>
	<td align="right">31,102.47</td>
	<td align="right">3,887.81</td>
	
</tr>
<tr class="jamn">
	<th align="right">05:00</th>
	<td align="right">90.57</td>
	<td align="right">0.84</td>
	<td align="right">3,415,575.57</td>
	<td align="right">0.20</td>
	<td align="right">4.90</td>
	<td align="right">1.22</td>
	<td align="right">7,590.17</td>
	<td align="right">948.77</td>
	
</tr>
<tr class="udda">
	<th align="right">06:00</th>
	<td align="right">104.57</td>
	<td align="right">0.97</td>
	<td align="right">19,799,961.29</td>
	<td align="right">1.13</td>
	<td align="right">5.84</td>
	<td align="right">1.45</td>
	<td align="right">43,999.91</td>
	<td align="right">5,499.99</td>
	
</tr>
<tr class="jamn">
	<th align="right">07:00</th>
	<td align="right">315.29</td>
	<td align="right">2.92</td>
	<td align="right">43,883,654.86</td>
	<td align="right">2.51</td>
	<td align="right">7.49</td>
	<td align="right">1.86</td>
	<td align="right">97,519.23</td>
	<td align="right">12,189.90</td>
	
</tr>
<tr class="udda">
	<th align="right">08:00</th>
	<td align="right">426.86</td>
	<td align="right">3.95</td>
	<td align="right">72,039,724.86</td>
	<td align="right">4.12</td>
	<td align="right">15.35</td>
	<td align="right">3.82</td>
	<td align="right">160,088.28</td>
	<td align="right">20,011.03</td>
	
</tr>
<tr class="jamn">
	<th align="right">09:00</th>
	<td align="right">641.57</td>
	<td align="right">5.94</td>
	<td align="right">113,273,751.43</td>
	<td align="right">6.47</td>
	<td align="right">22.19</td>
	<td align="right">5.52</td>
	<td align="right">251,719.45</td>
	<td align="right">31,464.93</td>
	
</tr>
<tr class="udda">
	<th align="right">10:00</th>
	<td align="right">625.86</td>
	<td align="right">5.80</td>
	<td align="right">133,995,779.86</td>
	<td align="right">7.66</td>
	<td align="right">22.82</td>
	<td align="right">5.68</td>
	<td align="right">297,768.40</td>
	<td align="right">37,221.05</td>
	
</tr>
<tr class="jamn">
	<th align="right">11:00</th>
	<td align="right">694.00</td>
	<td align="right">6.43</td>
	<td align="right">116,173,526.71</td>
	<td align="right">6.64</td>
	<td align="right">23.37</td>
	<td align="right">5.82</td>
	<td align="right">258,163.39</td>
	<td align="right">32,270.42</td>
	
</tr>
<tr class="udda">
	<th align="right">12:00</th>
	<td align="right">978.00</td>
	<td align="right">9.06</td>
	<td align="right">140,070,101.57</td>
	<td align="right">8.01</td>
	<td align="right">25.20</td>
	<td align="right">6.27</td>
	<td align="right">311,266.89</td>
	<td align="right">38,908.36</td>
	
</tr>
<tr class="jamn">
	<th align="right">13:00</th>
	<td align="right">1,000.29</td>
	<td align="right">9.26</td>
	<td align="right">164,669,529.86</td>
	<td align="right">9.41</td>
	<td align="right">38.96</td>
	<td align="right">9.70</td>
	<td align="right">365,932.29</td>
	<td align="right">45,741.54</td>
	
</tr>
<tr class="udda">
	<th align="right">14:00</th>
	<td align="right">851.57</td>
	<td align="right">7.89</td>
	<td align="right">162,335,886.71</td>
	<td align="right">9.28</td>
	<td align="right">31.40</td>
	<td align="right">7.82</td>
	<td align="right">360,746.41</td>
	<td align="right">45,093.30</td>
	
</tr>
<tr class="jamn">
	<th align="right">15:00</th>
	<td align="right">533.14</td>
	<td align="right">4.94</td>
	<td align="right">93,676,085.57</td>
	<td align="right">5.35</td>
	<td align="right">19.69</td>
	<td align="right">4.90</td>
	<td align="right">208,169.08</td>
	<td align="right">26,021.13</td>
	
</tr>
<tr class="udda">
	<th align="right">16:00</th>
	<td align="right">657.71</td>
	<td align="right">6.09</td>
	<td align="right">111,598,135.14</td>
	<td align="right">6.38</td>
	<td align="right">21.00</td>
	<td align="right">5.23</td>
	<td align="right">247,995.86</td>
	<td align="right">30,999.48</td>
	
</tr>
<tr class="jamn">
	<th align="right">17:00</th>
	<td align="right">752.29</td>
	<td align="right">6.97</td>
	<td align="right">72,717,155.29</td>
	<td align="right">4.16</td>
	<td align="right">18.38</td>
	<td align="right">4.58</td>
	<td align="right">161,593.68</td>
	<td align="right">20,199.21</td>
	
</tr>
<tr class="udda">
	<th align="right">18:00</th>
	<td align="right">501.57</td>
	<td align="right">4.64</td>
	<td align="right">80,175,007.43</td>
	<td align="right">4.58</td>
	<td align="right">18.55</td>
	<td align="right">4.62</td>
	<td align="right">178,166.68</td>
	<td align="right">22,270.84</td>
	
</tr>
<tr class="jamn">
	<th align="right">19:00</th>
	<td align="right">450.57</td>
	<td align="right">4.17</td>
	<td align="right">80,319,810.29</td>
	<td align="right">4.59</td>
	<td align="right">15.68</td>
	<td align="right">3.90</td>
	<td align="right">178,488.47</td>
	<td align="right">22,311.06</td>
	
</tr>
<tr class="udda">
	<th align="right">20:00</th>
	<td align="right">408.86</td>
	<td align="right">3.79</td>
	<td align="right">74,490,148.43</td>
	<td align="right">4.26</td>
	<td align="right">17.12</td>
	<td align="right">4.26</td>
	<td align="right">165,533.66</td>
	<td align="right">20,691.71</td>
	
</tr>
<tr class="jamn">
	<th align="right">21:00</th>
	<td align="right">487.29</td>
	<td align="right">4.51</td>
	<td align="right">78,059,506.57</td>
	<td align="right">4.46</td>
	<td align="right">18.51</td>
	<td align="right">4.61</td>
	<td align="right">173,465.57</td>
	<td align="right">21,683.20</td>
	
</tr>
<tr class="udda">
	<th align="right">22:00</th>
	<td align="right">466.57</td>
	<td align="right">4.32</td>
	<td align="right">75,871,756.00</td>
	<td align="right">4.34</td>
	<td align="right">21.04</td>
	<td align="right">5.24</td>
	<td align="right">168,603.90</td>
	<td align="right">21,075.49</td>
	
</tr>
<tr class="jamn">
	<th align="right">23:00</th>
	<td align="right">279.86</td>
	<td align="right">2.59</td>
	<td align="right">47,349,874.14</td>
	<td align="right">2.71</td>
	<td align="right">24.12</td>
	<td align="right">6.00</td>
	<td align="right">105,221.94</td>
	<td align="right">13,152.74</td>
	
</tr>

</tbody>
</table>

