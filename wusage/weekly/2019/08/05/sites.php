


<h2>Statistics for http://www.u5645470.fsdata.se, Week of 2019-8-5 to 2019-8-11</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 20 of 2,684 Visitor Sites,
Sorted by Access Count

</h3>
<p></p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Site</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="jamnudda">
	<th class="hogerstalld">1</th>
	<td>
	63.143.42.243		
	</td>
	<td align="right">1,291</td>
	<td align="right">2.51</td>
	<td align="right">14,799</td>
	<td align="right">0.00</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	5.35.187.69		
	</td>
	<td align="right">708</td>
	<td align="right">1.38</td>
	<td align="right">45,130,272</td>
	<td align="right">0.47</td>
	<td align="right">4</td>
	<td align="right">0.16</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	89.221.250.27		
	</td>
	<td align="right">524</td>
	<td align="right">1.02</td>
	<td align="right">10,237</td>
	<td align="right">0.00</td>
	<td align="right">8</td>
	<td align="right">0.33</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	46.59.25.88		
	</td>
	<td align="right">451</td>
	<td align="right">0.88</td>
	<td align="right">128,370,828</td>
	<td align="right">1.33</td>
	<td align="right">18</td>
	<td align="right">0.70</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	188.151.125.195		
	</td>
	<td align="right">372</td>
	<td align="right">0.72</td>
	<td align="right">173,597,743</td>
	<td align="right">1.79</td>
	<td align="right">10</td>
	<td align="right">0.41</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	79.138.36.49		
	</td>
	<td align="right">194</td>
	<td align="right">0.38</td>
	<td align="right">85,040,143</td>
	<td align="right">0.88</td>
	<td align="right">4</td>
	<td align="right">0.16</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	46.194.25.4		
	</td>
	<td align="right">193</td>
	<td align="right">0.38</td>
	<td align="right">20,275,735</td>
	<td align="right">0.21</td>
	<td align="right">6</td>
	<td align="right">0.25</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	80.216.24.80		
	</td>
	<td align="right">165</td>
	<td align="right">0.32</td>
	<td align="right">47,803,342</td>
	<td align="right">0.49</td>
	<td align="right">4</td>
	<td align="right">0.16</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	185.224.57.161		
	</td>
	<td align="right">163</td>
	<td align="right">0.32</td>
	<td align="right">21,863,355</td>
	<td align="right">0.23</td>
	<td align="right">7</td>
	<td align="right">0.29</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">10</th>
	<td>
	77.218.252.71		
	</td>
	<td align="right">163</td>
	<td align="right">0.32</td>
	<td align="right">51,528,653</td>
	<td align="right">0.53</td>
	<td align="right">3</td>
	<td align="right">0.13</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">11</th>
	<td>
	37.46.175.148		
	</td>
	<td align="right">159</td>
	<td align="right">0.31</td>
	<td align="right">34,614,338</td>
	<td align="right">0.36</td>
	<td align="right">3</td>
	<td align="right">0.12</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">12</th>
	<td>
	62.13.78.2		
	</td>
	<td align="right">144</td>
	<td align="right">0.28</td>
	<td align="right">76,155,927</td>
	<td align="right">0.79</td>
	<td align="right">4</td>
	<td align="right">0.16</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">13</th>
	<td>
	66.249.64.18		
	</td>
	<td align="right">138</td>
	<td align="right">0.27</td>
	<td align="right">4,410,148</td>
	<td align="right">0.05</td>
	<td align="right">16</td>
	<td align="right">0.62</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">14</th>
	<td>
	80.216.20.48		
	</td>
	<td align="right">132</td>
	<td align="right">0.26</td>
	<td align="right">21,918,281</td>
	<td align="right">0.23</td>
	<td align="right">6</td>
	<td align="right">0.25</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">15</th>
	<td>
	82.103.139.165		
	</td>
	<td align="right">129</td>
	<td align="right">0.25</td>
	<td align="right">976,590</td>
	<td align="right">0.01</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">16</th>
	<td>
	94.247.174.83		
	</td>
	<td align="right">129</td>
	<td align="right">0.25</td>
	<td align="right">1,006,150</td>
	<td align="right">0.01</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">17</th>
	<td>
	46.246.122.10		
	</td>
	<td align="right">128</td>
	<td align="right">0.25</td>
	<td align="right">961,758</td>
	<td align="right">0.01</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">18</th>
	<td>
	151.106.52.134		
	</td>
	<td align="right">127</td>
	<td align="right">0.25</td>
	<td align="right">961,453</td>
	<td align="right">0.01</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">19</th>
	<td>
	81.17.62.205		
	</td>
	<td align="right">125</td>
	<td align="right">0.24</td>
	<td align="right">961,782</td>
	<td align="right">0.01</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">20</th>
	<td>
	82.103.145.126		
	</td>
	<td align="right">124</td>
	<td align="right">0.24</td>
	<td align="right">961,647</td>
	<td align="right">0.01</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>

</tbody>
</table>

