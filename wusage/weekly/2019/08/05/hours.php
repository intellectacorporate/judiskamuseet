


<h2>Statistics for http://www.u5645470.fsdata.se, Week of 2019-8-5 to 2019-8-11</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Traffic by Hour</h3>
<p>Figures are averages for that hour
	of each day in the time period analyzed.</p>
<img src="h.png" width="468" height="351">
<br /><br />
<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Hour</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	<th scope='col'>Bits per Sec</th>
	<th scope='col'>Bytes per Sec</th>
	
	</tr>
</thead>
<tbody>
<tr class="jamnudda">
	<th align="right">00:00</th>
	<td align="right">142.57</td>
	<td align="right">1.94</td>
	<td align="right">20,080,771.43</td>
	<td align="right">1.45</td>
	<td align="right">7.02</td>
	<td align="right">1.85</td>
	<td align="right">44,623.94</td>
	<td align="right">5,577.99</td>
	
</tr>
<tr class="jamn">
	<th align="right">01:00</th>
	<td align="right">109.43</td>
	<td align="right">1.49</td>
	<td align="right">19,556,655.71</td>
	<td align="right">1.41</td>
	<td align="right">8.23</td>
	<td align="right">2.17</td>
	<td align="right">43,459.23</td>
	<td align="right">5,432.40</td>
	
</tr>
<tr class="udda">
	<th align="right">02:00</th>
	<td align="right">71.43</td>
	<td align="right">0.97</td>
	<td align="right">5,936,181.43</td>
	<td align="right">0.43</td>
	<td align="right">7.29</td>
	<td align="right">1.93</td>
	<td align="right">13,191.51</td>
	<td align="right">1,648.94</td>
	
</tr>
<tr class="jamn">
	<th align="right">03:00</th>
	<td align="right">98.00</td>
	<td align="right">1.33</td>
	<td align="right">9,586,826.43</td>
	<td align="right">0.69</td>
	<td align="right">7.47</td>
	<td align="right">1.97</td>
	<td align="right">21,304.06</td>
	<td align="right">2,663.01</td>
	
</tr>
<tr class="udda">
	<th align="right">04:00</th>
	<td align="right">82.57</td>
	<td align="right">1.12</td>
	<td align="right">5,128,486.43</td>
	<td align="right">0.37</td>
	<td align="right">6.68</td>
	<td align="right">1.76</td>
	<td align="right">11,396.64</td>
	<td align="right">1,424.58</td>
	
</tr>
<tr class="jamn">
	<th align="right">05:00</th>
	<td align="right">64.86</td>
	<td align="right">0.88</td>
	<td align="right">6,088,081.00</td>
	<td align="right">0.44</td>
	<td align="right">8.24</td>
	<td align="right">2.18</td>
	<td align="right">13,529.07</td>
	<td align="right">1,691.13</td>
	
</tr>
<tr class="udda">
	<th align="right">06:00</th>
	<td align="right">106.43</td>
	<td align="right">1.45</td>
	<td align="right">14,545,459.00</td>
	<td align="right">1.05</td>
	<td align="right">7.60</td>
	<td align="right">2.01</td>
	<td align="right">32,323.24</td>
	<td align="right">4,040.41</td>
	
</tr>
<tr class="jamn">
	<th align="right">07:00</th>
	<td align="right">179.43</td>
	<td align="right">2.44</td>
	<td align="right">33,901,912.43</td>
	<td align="right">2.45</td>
	<td align="right">8.86</td>
	<td align="right">2.34</td>
	<td align="right">75,337.58</td>
	<td align="right">9,417.20</td>
	
</tr>
<tr class="udda">
	<th align="right">08:00</th>
	<td align="right">283.29</td>
	<td align="right">3.85</td>
	<td align="right">37,584,235.00</td>
	<td align="right">2.72</td>
	<td align="right">12.89</td>
	<td align="right">3.40</td>
	<td align="right">83,520.52</td>
	<td align="right">10,440.07</td>
	
</tr>
<tr class="jamn">
	<th align="right">09:00</th>
	<td align="right">426.29</td>
	<td align="right">5.80</td>
	<td align="right">94,178,263.29</td>
	<td align="right">6.81</td>
	<td align="right">15.84</td>
	<td align="right">4.18</td>
	<td align="right">209,285.03</td>
	<td align="right">26,160.63</td>
	
</tr>
<tr class="udda">
	<th align="right">10:00</th>
	<td align="right">606.29</td>
	<td align="right">8.25</td>
	<td align="right">145,676,472.00</td>
	<td align="right">10.54</td>
	<td align="right">26.26</td>
	<td align="right">6.93</td>
	<td align="right">323,725.49</td>
	<td align="right">40,465.69</td>
	
</tr>
<tr class="jamn">
	<th align="right">11:00</th>
	<td align="right">460.29</td>
	<td align="right">6.26</td>
	<td align="right">81,743,912.14</td>
	<td align="right">5.91</td>
	<td align="right">24.73</td>
	<td align="right">6.53</td>
	<td align="right">181,653.14</td>
	<td align="right">22,706.64</td>
	
</tr>
<tr class="udda">
	<th align="right">12:00</th>
	<td align="right">520.71</td>
	<td align="right">7.08</td>
	<td align="right">114,921,416.43</td>
	<td align="right">8.31</td>
	<td align="right">21.46</td>
	<td align="right">5.67</td>
	<td align="right">255,380.93</td>
	<td align="right">31,922.62</td>
	
</tr>
<tr class="jamn">
	<th align="right">13:00</th>
	<td align="right">482.43</td>
	<td align="right">6.56</td>
	<td align="right">91,442,296.00</td>
	<td align="right">6.61</td>
	<td align="right">22.26</td>
	<td align="right">5.88</td>
	<td align="right">203,205.10</td>
	<td align="right">25,400.64</td>
	
</tr>
<tr class="udda">
	<th align="right">14:00</th>
	<td align="right">491.71</td>
	<td align="right">6.69</td>
	<td align="right">106,223,826.71</td>
	<td align="right">7.68</td>
	<td align="right">25.36</td>
	<td align="right">6.70</td>
	<td align="right">236,052.95</td>
	<td align="right">29,506.62</td>
	
</tr>
<tr class="jamn">
	<th align="right">15:00</th>
	<td align="right">391.29</td>
	<td align="right">5.32</td>
	<td align="right">74,546,017.00</td>
	<td align="right">5.39</td>
	<td align="right">21.93</td>
	<td align="right">5.79</td>
	<td align="right">165,657.82</td>
	<td align="right">20,707.23</td>
	
</tr>
<tr class="udda">
	<th align="right">16:00</th>
	<td align="right">482.71</td>
	<td align="right">6.57</td>
	<td align="right">83,125,946.43</td>
	<td align="right">6.01</td>
	<td align="right">18.96</td>
	<td align="right">5.01</td>
	<td align="right">184,724.33</td>
	<td align="right">23,090.54</td>
	
</tr>
<tr class="jamn">
	<th align="right">17:00</th>
	<td align="right">311.14</td>
	<td align="right">4.23</td>
	<td align="right">52,266,952.57</td>
	<td align="right">3.78</td>
	<td align="right">19.27</td>
	<td align="right">5.09</td>
	<td align="right">116,148.78</td>
	<td align="right">14,518.60</td>
	
</tr>
<tr class="udda">
	<th align="right">18:00</th>
	<td align="right">309.29</td>
	<td align="right">4.21</td>
	<td align="right">58,746,727.71</td>
	<td align="right">4.25</td>
	<td align="right">15.84</td>
	<td align="right">4.18</td>
	<td align="right">130,548.28</td>
	<td align="right">16,318.54</td>
	
</tr>
<tr class="jamn">
	<th align="right">19:00</th>
	<td align="right">297.71</td>
	<td align="right">4.05</td>
	<td align="right">53,015,501.57</td>
	<td align="right">3.83</td>
	<td align="right">16.02</td>
	<td align="right">4.23</td>
	<td align="right">117,812.23</td>
	<td align="right">14,726.53</td>
	
</tr>
<tr class="udda">
	<th align="right">20:00</th>
	<td align="right">439.14</td>
	<td align="right">5.97</td>
	<td align="right">86,355,758.86</td>
	<td align="right">6.25</td>
	<td align="right">18.51</td>
	<td align="right">4.89</td>
	<td align="right">191,901.69</td>
	<td align="right">23,987.71</td>
	
</tr>
<tr class="jamn">
	<th align="right">21:00</th>
	<td align="right">413.29</td>
	<td align="right">5.62</td>
	<td align="right">86,302,515.14</td>
	<td align="right">6.24</td>
	<td align="right">21.61</td>
	<td align="right">5.71</td>
	<td align="right">191,783.37</td>
	<td align="right">23,972.92</td>
	
</tr>
<tr class="udda">
	<th align="right">22:00</th>
	<td align="right">321.14</td>
	<td align="right">4.37</td>
	<td align="right">55,797,406.86</td>
	<td align="right">4.04</td>
	<td align="right">15.38</td>
	<td align="right">4.06</td>
	<td align="right">123,994.24</td>
	<td align="right">15,499.28</td>
	
</tr>
<tr class="jamn">
	<th align="right">23:00</th>
	<td align="right">258.71</td>
	<td align="right">3.52</td>
	<td align="right">45,889,237.14</td>
	<td align="right">3.32</td>
	<td align="right">20.99</td>
	<td align="right">5.54</td>
	<td align="right">101,976.08</td>
	<td align="right">12,747.01</td>
	
</tr>

</tbody>
</table>

