


<h2>Statistics for http://www.u5645470.fsdata.se, Week of 2019-8-5 to 2019-8-11</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 10 of 164 Browsers (User Agents),
Sorted by Access Count

</h3>
<p></p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Product</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th class="hogerstalld">1</th>
	<td>
	Safari/537.36		
	</td>
	<td align="right">14,957</td>
	<td align="right">29.11</td>
	<td align="right">3,351,211,232</td>
	<td align="right">34.64</td>
	<td align="right">581</td>
	<td align="right">21.96</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	Safari/604.1		
	</td>
	<td align="right">14,762</td>
	<td align="right">28.73</td>
	<td align="right">3,230,643,999</td>
	<td align="right">33.39</td>
	<td align="right">540</td>
	<td align="right">20.42</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	Mozilla/5.0 		
	</td>
	<td align="right">5,975</td>
	<td align="right">11.63</td>
	<td align="right">943,483,578</td>
	<td align="right">9.75</td>
	<td align="right">446</td>
	<td align="right">16.85</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	Safari/605.1.15		
	</td>
	<td align="right">2,651</td>
	<td align="right">5.16</td>
	<td align="right">638,555,645</td>
	<td align="right">6.60</td>
	<td align="right">97</td>
	<td align="right">3.69</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	bingbot/2.0		
	</td>
	<td align="right">1,885</td>
	<td align="right">3.67</td>
	<td align="right">262,179,330</td>
	<td align="right">2.71</td>
	<td align="right">414</td>
	<td align="right">15.65</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	UptimeRobot/2.0		
	</td>
	<td align="right">1,290</td>
	<td align="right">2.51</td>
	<td align="right">14,798</td>
	<td align="right">0.00</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	Pingdom.com_bot_version_1.4_		
	</td>
	<td align="right">1,280</td>
	<td align="right">2.49</td>
	<td align="right">9,794,800</td>
	<td align="right">0.10</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	Safari/537.36 Edge/17.17134		
	</td>
	<td align="right">922</td>
	<td align="right">1.79</td>
	<td align="right">201,568,660</td>
	<td align="right">2.08</td>
	<td align="right">33</td>
	<td align="right">1.27</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	Safari/602.1		
	</td>
	<td align="right">827</td>
	<td align="right">1.61</td>
	<td align="right">136,751,610</td>
	<td align="right">1.41</td>
	<td align="right">30</td>
	<td align="right">1.15</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">10</th>
	<td>
	Safari/605.1		
	</td>
	<td align="right">784</td>
	<td align="right">1.53</td>
	<td align="right">154,705,703</td>
	<td align="right">1.60</td>
	<td align="right">34</td>
	<td align="right">1.32</td>
</tr>

</tbody>
</table>

