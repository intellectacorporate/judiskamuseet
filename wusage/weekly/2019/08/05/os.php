


<h2>Statistics for http://www.u5645470.fsdata.se, Week of 2019-8-5 to 2019-8-11</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 8 of 8 Operating Systems,
Sorted by Access Count

</h3>
<p></p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Product</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th class="hogerstalld">1</th>
	<td>
	Mac OS X		
	</td>
	<td align="right">40,034</td>
	<td align="right">77.92</td>
	<td align="right">8,460,278,584</td>
	<td align="right">87.44</td>
	<td align="right">1,598</td>
	<td align="right">60.30</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	Other		
	</td>
	<td align="right">7,440</td>
	<td align="right">14.48</td>
	<td align="right">612,706,931</td>
	<td align="right">6.33</td>
	<td align="right">666</td>
	<td align="right">25.12</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	Windows NT		
	</td>
	<td align="right">2,717</td>
	<td align="right">5.29</td>
	<td align="right">593,301,271</td>
	<td align="right">6.13</td>
	<td align="right">101</td>
	<td align="right">3.82</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	Linux		
	</td>
	<td align="right">1,074</td>
	<td align="right">2.09</td>
	<td align="right">7,420,945</td>
	<td align="right">0.08</td>
	<td align="right">276</td>
	<td align="right">10.43</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	Windows Codename Longhorn		
	</td>
	<td align="right">49</td>
	<td align="right">0.10</td>
	<td align="right">777,230</td>
	<td align="right">0.01</td>
	<td align="right">3</td>
	<td align="right">0.12</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	Windows XP		
	</td>
	<td align="right">47</td>
	<td align="right">0.09</td>
	<td align="right">1,073,771</td>
	<td align="right">0.01</td>
	<td align="right">5</td>
	<td align="right">0.21</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	Windows 2003		
	</td>
	<td align="right">13</td>
	<td align="right">0.03</td>
	<td align="right">47,653</td>
	<td align="right">0.00</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	Mac OS		
	</td>
	<td align="right">2</td>
	<td align="right">0.00</td>
	<td align="right">40</td>
	<td align="right">0.00</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>

</tbody>
</table>

