


<h2>Statistics for http://www.u5645470.fsdata.se, Week of 2019-8-5 to 2019-8-11</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Traffic by Day</h3>
<img src="d.png" width="468" height="351">
<br /><br />


<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Date</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	<th scope='col'>Bits per Sec</th>
	<th scope='col'>Bytes per Sec</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th align="right">2019-8-5</th>
	<td align="right">7,784	</td>
	<td align="right">15.13	</td>
	<td align="right">1,337,661,936	</td>
	<td align="right">13.82	</td>
	<td align="right">354	</td>
	<td align="right">13.35	</td>
	<td align="right">123,857.59	</td>
	<td align="right">15,482.20	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-8-6</th>
	<td align="right">8,028	</td>
	<td align="right">15.60	</td>
	<td align="right">1,449,160,712	</td>
	<td align="right">14.97	</td>
	<td align="right">377	</td>
	<td align="right">14.22	</td>
	<td align="right">134,181.55	</td>
	<td align="right">16,772.69	</td>
</tr>
<tr class="udda">
	<th align="right">2019-8-7</th>
	<td align="right">8,501	</td>
	<td align="right">16.52	</td>
	<td align="right">1,651,635,044	</td>
	<td align="right">17.07	</td>
	<td align="right">397	</td>
	<td align="right">14.98	</td>
	<td align="right">152,929.17	</td>
	<td align="right">19,116.15	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-8-8</th>
	<td align="right">7,143	</td>
	<td align="right">13.88	</td>
	<td align="right">1,432,448,048	</td>
	<td align="right">14.80	</td>
	<td align="right">368	</td>
	<td align="right">13.88	</td>
	<td align="right">132,634.08	</td>
	<td align="right">16,579.26	</td>
</tr>
<tr class="udda">
	<th align="right">2019-8-9</th>
	<td align="right">6,129	</td>
	<td align="right">11.91	</td>
	<td align="right">1,121,045,987	</td>
	<td align="right">11.58	</td>
	<td align="right">393	</td>
	<td align="right">14.82	</td>
	<td align="right">103,800.55	</td>
	<td align="right">12,975.07	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-8-10</th>
	<td align="right">7,381	</td>
	<td align="right">14.35	</td>
	<td align="right">1,571,141,875	</td>
	<td align="right">16.23	</td>
	<td align="right">436	</td>
	<td align="right">16.45	</td>
	<td align="right">145,476.10	</td>
	<td align="right">18,184.51	</td>
</tr>
<tr class="udda">
	<th align="right">2019-8-11</th>
	<td align="right">6,485	</td>
	<td align="right">12.60	</td>
	<td align="right">1,115,392,409	</td>
	<td align="right">11.52	</td>
	<td align="right">326	</td>
	<td align="right">12.30	</td>
	<td align="right">103,277.07	</td>
	<td align="right">12,909.63	</td>
</tr>

</tbody>
</table>

