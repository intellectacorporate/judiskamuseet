


<h2>Statistics for http://www.u5645470.fsdata.se, Week of 2019-8-5 to 2019-8-11</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 20 of 136 Referring Sites,
Sorted by Access Count

</h3>
<p></p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Referring Site</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th class="hogerstalld">1</th>
	<td>
	<a href="https://judiskamuseet.se">
	https://judiskamuseet.se</a>
	</td>
	<td align="right">36,167</td>
	<td align="right">89.32</td>
	<td align="right">8,858,519,815</td>
	<td align="right">98.06</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	<a href="http://judiskamuseet.se">
	http://judiskamuseet.se</a>
	</td>
	<td align="right">2,379</td>
	<td align="right">5.88</td>
	<td align="right">49,351,511</td>
	<td align="right">0.55</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	<a href="https://www.google.com">
	https://www.google.com</a>
	</td>
	<td align="right">779</td>
	<td align="right">1.92</td>
	<td align="right">74,965,119</td>
	<td align="right">0.83</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	<a href="https://www.google.se">
	https://www.google.se</a>
	</td>
	<td align="right">546</td>
	<td align="right">1.35</td>
	<td align="right">29,006,400</td>
	<td align="right">0.32</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	<a href="https://www.google.de">
	https://www.google.de</a>
	</td>
	<td align="right">113</td>
	<td align="right">0.28</td>
	<td align="right">1,650,918</td>
	<td align="right">0.02</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	<a href="http://www.google.com">
	http://www.google.com</a>
	</td>
	<td align="right">88</td>
	<td align="right">0.22</td>
	<td align="right">694,673</td>
	<td align="right">0.01</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	<a href="https://www.bing.com">
	https://www.bing.com</a>
	</td>
	<td align="right">35</td>
	<td align="right">0.09</td>
	<td align="right">5,460,111</td>
	<td align="right">0.06</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	<a href="http://m.facebook.com">
	http://m.facebook.com</a>
	</td>
	<td align="right">31</td>
	<td align="right">0.08</td>
	<td align="right">397,915</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	<a href="https://www.google.co.uk">
	https://www.google.co.uk</a>
	</td>
	<td align="right">28</td>
	<td align="right">0.07</td>
	<td align="right">3,275,022</td>
	<td align="right">0.04</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">10</th>
	<td>
	<a href="http://www.judiska-museet.se">
	http://www.judiska-museet.se</a>
	</td>
	<td align="right">23</td>
	<td align="right">0.06</td>
	<td align="right">200</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">11</th>
	<td>
	<a href="https://myactivity.google.com">
	https://myactivity.google.com</a>
	</td>
	<td align="right">17</td>
	<td align="right">0.04</td>
	<td align="right">8,030,804</td>
	<td align="right">0.09</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">12</th>
	<td>
	<a href="http://www.stockholmmuseums.se">
	http://www.stockholmmuseums.se</a>
	</td>
	<td align="right">14</td>
	<td align="right">0.03</td>
	<td align="right">133,289</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">13</th>
	<td>
	<a href="https://developers.google.com">
	https://developers.google.com</a>
	</td>
	<td align="right">14</td>
	<td align="right">0.03</td>
	<td align="right">475,365</td>
	<td align="right">0.01</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">14</th>
	<td>
	<a href="http://l.instagram.com">
	http://l.instagram.com</a>
	</td>
	<td align="right">10</td>
	<td align="right">0.02</td>
	<td align="right">74,416</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">15</th>
	<td>
	<a href="https://yandex.ru">
	https://yandex.ru</a>
	</td>
	<td align="right">9</td>
	<td align="right">0.02</td>
	<td align="right">66,997</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">16</th>
	<td>
	<a href="https://www.facebook.com">
	https://www.facebook.com</a>
	</td>
	<td align="right">8</td>
	<td align="right">0.02</td>
	<td align="right">100,630</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">17</th>
	<td>
	<a href="https://www.google.ca">
	https://www.google.ca</a>
	</td>
	<td align="right">8</td>
	<td align="right">0.02</td>
	<td align="right">59,419</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">18</th>
	<td>
	<a href="https://m.facebook.com">
	https://m.facebook.com</a>
	</td>
	<td align="right">8</td>
	<td align="right">0.02</td>
	<td align="right">118,421</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">19</th>
	<td>
	<a href="https://www.google.com.au">
	https://www.google.com.au</a>
	</td>
	<td align="right">7</td>
	<td align="right">0.02</td>
	<td align="right">67,596</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">20</th>
	<td>
	<a href="http://www.google.se">
	http://www.google.se</a>
	</td>
	<td align="right">6</td>
	<td align="right">0.01</td>
	<td align="right">59,222</td>
	<td align="right">0.00</td>
</tr>

</tbody>
</table>

