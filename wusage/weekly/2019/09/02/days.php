


<h2>Statistics for http://www.u5645470.fsdata.se, Week of 2019-9-2 to 2019-9-8</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Traffic by Day</h3>
<img src="d.png" width="468" height="351">
<br /><br />


<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Date</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	<th scope='col'>Bits per Sec</th>
	<th scope='col'>Bytes per Sec</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th align="right">2019-9-2</th>
	<td align="right">10,840	</td>
	<td align="right">18.04	</td>
	<td align="right">1,533,387,025	</td>
	<td align="right">27.02	</td>
	<td align="right">394	</td>
	<td align="right">15.59	</td>
	<td align="right">141,980.28	</td>
	<td align="right">17,747.54	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-9-3</th>
	<td align="right">9,064	</td>
	<td align="right">15.08	</td>
	<td align="right">1,318,613,703	</td>
	<td align="right">23.23	</td>
	<td align="right">367	</td>
	<td align="right">14.52	</td>
	<td align="right">122,093.86	</td>
	<td align="right">15,261.73	</td>
</tr>
<tr class="udda">
	<th align="right">2019-9-4</th>
	<td align="right">7,915	</td>
	<td align="right">13.17	</td>
	<td align="right">543,539,979	</td>
	<td align="right">9.58	</td>
	<td align="right">349	</td>
	<td align="right">13.81	</td>
	<td align="right">50,327.78	</td>
	<td align="right">6,290.97	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-9-5</th>
	<td align="right">11,495	</td>
	<td align="right">19.13	</td>
	<td align="right">736,864,339	</td>
	<td align="right">12.98	</td>
	<td align="right">410	</td>
	<td align="right">16.22	</td>
	<td align="right">68,228.18	</td>
	<td align="right">8,528.52	</td>
</tr>
<tr class="udda">
	<th align="right">2019-9-6</th>
	<td align="right">7,545	</td>
	<td align="right">12.56	</td>
	<td align="right">504,339,366	</td>
	<td align="right">8.89	</td>
	<td align="right">344	</td>
	<td align="right">13.61	</td>
	<td align="right">46,698.09	</td>
	<td align="right">5,837.26	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-9-7</th>
	<td align="right">6,735	</td>
	<td align="right">11.21	</td>
	<td align="right">566,456,935	</td>
	<td align="right">9.98	</td>
	<td align="right">332	</td>
	<td align="right">13.13	</td>
	<td align="right">52,449.72	</td>
	<td align="right">6,556.21	</td>
</tr>
<tr class="udda">
	<th align="right">2019-9-8</th>
	<td align="right">6,500	</td>
	<td align="right">10.82	</td>
	<td align="right">472,653,692	</td>
	<td align="right">8.33	</td>
	<td align="right">332	</td>
	<td align="right">13.13	</td>
	<td align="right">43,764.23	</td>
	<td align="right">5,470.53	</td>
</tr>

</tbody>
</table>

