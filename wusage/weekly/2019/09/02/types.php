


<h2>Statistics for http://www.u5645470.fsdata.se, Week of 2019-9-2 to 2019-9-8</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 10 of 18 File Types (Extensions), 
Sorted by Access Count

</h3>
<p>Individual file types as determined by file extensions. All URLs that
do not contain an extension are counted as directories.</p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Type</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th class="hogerstalld">1</th>
	<td>
	js		
	</td>
	<td align="right">14,201</td>
	<td align="right">23.63</td>
	<td align="right">164,138,478</td>
	<td align="right">2.89</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	Directory (folder)		
	</td>
	<td align="right">11,685</td>
	<td align="right">19.44</td>
	<td align="right">129,913,161</td>
	<td align="right">2.29</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	jpg		
	</td>
	<td align="right">11,210</td>
	<td align="right">18.65</td>
	<td align="right">4,419,544,054</td>
	<td align="right">77.87</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	css		
	</td>
	<td align="right">8,984</td>
	<td align="right">14.95</td>
	<td align="right">162,046,826</td>
	<td align="right">2.86</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	png		
	</td>
	<td align="right">7,336</td>
	<td align="right">12.21</td>
	<td align="right">154,208,795</td>
	<td align="right">2.72</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	php		
	</td>
	<td align="right">3,816</td>
	<td align="right">6.35</td>
	<td align="right">52,004,137</td>
	<td align="right">0.92</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	ttf		
	</td>
	<td align="right">1,237</td>
	<td align="right">2.06</td>
	<td align="right">108,662,242</td>
	<td align="right">1.91</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	jpeg		
	</td>
	<td align="right">634</td>
	<td align="right">1.06</td>
	<td align="right">451,065,465</td>
	<td align="right">7.95</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	txt		
	</td>
	<td align="right">539</td>
	<td align="right">0.90</td>
	<td align="right">36,733</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">10</th>
	<td>
	svg		
	</td>
	<td align="right">140</td>
	<td align="right">0.23</td>
	<td align="right">2,688,881</td>
	<td align="right">0.05</td>
</tr>

</tbody>
</table>

