

<script>
var componentsData = new Array(
        
        "*end*"
);
var componentsByPage = new Array();
var epochRange = "";
var sortedByAccesses = 0;
var sortedByBytes = 0;
var sortedByVisits = 0;
var alphabetized = 0;
var withBytes = 0;
var withVisits = 0;
var withDownloads = 0;
var subreportKeyword = "";
var subreportTitle = "";
var subreportAll = 0;
var subreportTop = 0;
var subreportTotal = 0;
var subreportPie = "";
var subreportGreenbarFlag = 0;
var pc = 0, ac = 0;
var dataEnd = 0;
var data;
var dataArrays = new Array(
	new Array(
		"Week of 2019-9-2 to 2019-9-8",	
		"*sorted-by-accesses*",
		"*bytes*",
		"*visits*",
		"*subreport*",
	"pages",
	"Documents",
	"*top*",
		30,
	
	"*total*",
	"2,623",
	"*pie*",
	"",
	"*row*",
	"1",
	"/",
	"*local-link*",
		"http://www.u5645470.fsdata.se/",
	
	"5,386",
	"8.96",
	"47,776,990",
		"0.84",
	"750",
		"1.49",
	"*row*",
	"2",
	"/wp-admin/admin-ajax.php",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-admin/admin-ajax.php",
	
	"2,874",
	"4.78",
	"27,582,129",
		"0.49",
	"39",
		"0.08",
	"*row*",
	"3",
	"/wp-content/themes/divi-child/style.css",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/themes/divi-child/style.css",
	
	"1,342",
	"2.23",
	"10,882,770",
		"0.19",
	"1,390",
		"2.77",
	"*row*",
	"4",
	"/wp-includes/css/dashicons.min.css",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-includes/css/dashicons.min.css",
	
	"1,335",
	"2.22",
	"37,947,172",
		"0.67",
	"1,373",
		"2.73",
	"*row*",
	"5",
	"/wp-content/themes/Divi/style.css",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/themes/Divi/style.css",
	
	"1,326",
	"2.21",
	"102,637,593",
		"1.81",
	"1,368",
		"2.72",
	"*row*",
	"6",
	"/wp-includes/css/dist/block-library/style.min.css",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-includes/css/dist/block-library/style.min.css",
	
	"1,322",
	"2.20",
	"6,513,885",
		"0.11",
	"1,358",
		"2.70",
	"*row*",
	"7",
	"/wp-includes/js/wp-embed.min.js",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-includes/js/wp-embed.min.js",
	
	"1,315",
	"2.19",
	"990,774",
		"0.02",
	"1,361",
		"2.71",
	"*row*",
	"8",
	"/wp-content/themes/divi-child/js/divi-child.js",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/themes/divi-child/js/divi-child.js",
	
	"1,310",
	"2.18",
	"1,186,067",
		"0.02",
	"1,362",
		"2.71",
	"*row*",
	"9",
	"/wp-content/themes/Divi/core/admin/js/common.js",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/themes/Divi/core/admin/js/common.js",
	
	"1,308",
	"2.18",
	"733,985",
		"0.01",
	"1,357",
		"2.70",
	"*row*",
	"10",
	"/wp-content/themes/Divi/js/custom.min.js",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/themes/Divi/js/custom.min.js",
	
	"1,300",
	"2.16",
	"80,743,906",
		"1.42",
	"1,354",
		"2.69",
	"*row*",
	"11",
	"/wp-includes/js/wp-emoji-release.min.js",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-includes/js/wp-emoji-release.min.js",
	
	"1,297",
	"2.16",
	"6,031,502",
		"0.11",
	"1,345",
		"2.68",
	"*row*",
	"12",
	"/wp-content/plugins/mailchimp-for-wp/assets/js/forms-api.min<br>\n.js",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/plugins/mailchimp-for-wp/assets/js/forms-api.min.js",
	
	"1,291",
	"2.15",
	"9,619,999",
		"0.17",
	"1,338",
		"2.66",
	"*row*",
	"13",
	"/wp-content/plugins/sitepress-multilingual-cms/templates/lan<br>\nguage-switchers/menu-item/style.css",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/plugins/sitepress-multilingual-cms/templates/language-switchers/menu-item/style.css",
	
	"1,285",
	"2.14",
	"176,652",
		"0.00",
	"1,336",
		"2.66",
	"*row*",
	"14",
	"/wp-content/plugins/wp-smushit/app/assets/js/smush-lazy-load<br>\n.min.js",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/plugins/wp-smushit/app/assets/js/smush-lazy-load.min.js",
	
	"1,285",
	"2.14",
	"4,631,317",
		"0.08",
	"1,338",
		"2.66",
	"*row*",
	"15",
	"/wp-content/plugins/wp-pagenavi/pagenavi-css.css",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/plugins/wp-pagenavi/pagenavi-css.css",
	
	"1,270",
	"2.11",
	"302,636",
		"0.01",
	"1,319",
		"2.62",
	"*row*",
	"16",
	"/wp-includes/js/jquery/jquery-migrate.min.js",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-includes/js/jquery/jquery-migrate.min.js",
	
	"1,267",
	"2.11",
	"5,085,822",
		"0.09",
	"1,323",
		"2.63",
	"*row*",
	"17",
	"/wp-includes/js/jquery/jquery.js",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-includes/js/jquery/jquery.js",
	
	"1,263",
	"2.10",
	"42,506,164",
		"0.75",
	"1,313",
		"2.61",
	"*row*",
	"18",
	"/wp-content/uploads/2017/06/Judiska_centr_svart_web-1.png",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/uploads/2017/06/Judiska_centr_svart_web-1.png",
	
	"1,246",
	"2.07",
	"11,237,730",
		"0.20",
	"1,270",
		"2.53",
	"*row*",
	"19",
	"/wp-content/themes/Divi/core/admin/fonts/modules.ttf",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/themes/Divi/core/admin/fonts/modules.ttf",
	
	"1,219",
	"2.03",
	"108,477,065",
		"1.91",
	"1,268",
		"2.52",
	"*row*",
	"20",
	"/wp-content/uploads/2019/05/cropped-Judiska_svart-2-32x32.pn<br>\ng",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/uploads/2019/05/cropped-Judiska_svart-2-32x32.png",
	
	"942",
	"1.57",
	"706,104",
		"0.01",
	"909",
		"1.81",
	"*row*",
	"21",
	"/wp-content/themes/divi-child/img/location.png",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/themes/divi-child/img/location.png",
	
	"867",
	"1.44",
	"2,617,278",
		"0.05",
	"906",
		"1.80",
	"*row*",
	"22",
	"/wp-content/themes/divi-child/img/Instagram.png",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/themes/divi-child/img/Instagram.png",
	
	"834",
	"1.39",
	"4,961,641",
		"0.09",
	"872",
		"1.74",
	"*row*",
	"23",
	"/wp-content/themes/divi-child/img/facebook.png",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/themes/divi-child/img/facebook.png",
	
	"826",
	"1.37",
	"3,938,220",
		"0.07",
	"869",
		"1.73",
	"*row*",
	"24",
	"/wp-content/themes/divi-child/img/clock.png",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/themes/divi-child/img/clock.png",
	
	"800",
	"1.33",
	"1,804,650",
		"0.03",
	"839",
		"1.67",
	"*row*",
	"25",
	"/wp-content/uploads/2019/05/cropped-Judiska_svart-2-192x192.<br>\npng",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/uploads/2019/05/cropped-Judiska_svart-2-192x192.png",
	
	"655",
	"1.09",
	"4,338,100",
		"0.08",
	"647",
		"1.29",
	"*row*",
	"26",
	"/utforska/om-judendomen",
	"*local-link*",
		"http://www.u5645470.fsdata.se/utforska/om-judendomen",
	
	"570",
	"0.95",
	"9,101,475",
		"0.16",
	"337",
		"0.67",
	"*row*",
	"27",
	"/robots.txt",
	"*local-link*",
		"http://www.u5645470.fsdata.se/robots.txt",
	
	"539",
	"0.90",
	"36,733",
		"0.00",
	"236",
		"0.47",
	"*row*",
	"28",
	"/wp-content/uploads/2019/06/sommarlov.jpg",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/uploads/2019/06/sommarlov.jpg",
	
	"512",
	"0.85",
	"4,470,765",
		"0.08",
	"543",
		"1.08",
	"*row*",
	"29",
	"/wp-content/uploads/2019/07/annons.jpg",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/uploads/2019/07/annons.jpg",
	
	"501",
	"0.83",
	"23,205,924",
		"0.41",
	"523",
		"1.04",
	"*row*",
	"30",
	"/wp-content/uploads/2019/06/till-webb_4.jpg",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/uploads/2019/06/till-webb_4.jpg",
	
	"496",
	"0.83",
	"49,687,110",
		"0.88",
	"527",
		"1.05",
	
		"*end*"
		)
	);

function incPC()
{
	pc++;
	if (pc == data.length) {
		if ((ac == dataArrays.length) || (dataArrays[ac] == "*end*")) {
			dataEnd = 1;
		} else {
			ac++;
			data = dataArrays[ac];
			pc = 0;
		}
	}	
}

function resetPC()
{
	pc = 0;
	ac = 0;
	if (dataArrays.length) {
		data = dataArrays[0];
		dataEnd = 0;
	} else {	
		dataEnd = 1;
	}
}

function compileReports()
{
	resetPC();
	compileDocuments();
}

function compileDocuments()
{
	var s = data[pc];
	epochRange = data[pc];
	incPC();
	while (dataEnd == 0) {
		var s = data[pc];
		if (s == "*sorted-by-accesses*") {
			sortedByAccesses = 1;
			incPC();
		} else if (s == "*sorted-by-bytes*") {
			sortedByBytes = 1;
			incPC();
		} else if (s == "*sorted-by-visits*") {
			sortedByVisits = 1;
			incPC();
		} else if (s == "*alphabetized*") {
			alphabetized = 1;
			incPC();
		} else if (s == "*bytes*") {
			withBytes = 1;
			incPC();
		} else if (s == "*visits*") {
			withVisits = 1;
			incPC();
		} else if (s == "*downloads*") {
			withDownloads = 1;
			incPC();
		} else {
			break;
		}
	}
	documentsHead();
	while (dataEnd == 0) {
		if (compileSubreport() == 0) {
			// No more subreports. That's OK 
			break;
		}	
	}
	documentsTail();
	return 1;
}
function compileSubreport()
{
	if (data[pc] != "*subreport*") {
		return 0;
	}
	incPC();	
	subreportKeyword = "";
	subreportTitle = "";
	subreportAll = 0;
	subreportTop = 0;
	subreportTotal = 0;
	subreportPie = "";
	subreportKeyword = data[pc];
	incPC();
	subreportTitle = data[pc];
	incPC();
	if (data[pc] == "*all*") {
		incPC();
		subreportAll = 1;
	}
	if (data[pc] == "*top*") {
		incPC();
		subreportTop = data[pc];
		incPC();
	}
	if (data[pc] == "*total*") {
		incPC();
		subreportTotal = data[pc];
		incPC();
	}
	if (data[pc] == "*pie*") {
		incPC();
		subreportPie = data[pc];
		incPC();
	}
	subreportHead();
	while (dataEnd == 0) {
		if (compileSubreportRow() == 0) {
			// No more subreport rows. That's OK 
			break;
		}
	}
	subreportTail();
	return 1;
}
var rowRank = 0;
var rowItem = "";
var rowLocalLink = "";
var rowTitle = "";
var rowReferrers = "";
var rowAccesses = 0;
var rowAccessesPer = 0;
var rowBytes = 0;
var rowBytesPer = 0;
var rowVisits = 0;
var rowVisitsPer = 0;
var rowDownloadsPer = 0;
var rowComponents = 0;

function compileSubreportRow()
{
	if (data[pc] != "*row*") {
		return 0;
	}
	rowRank = 0;
	rowItem = "";
	rowLocalLink = "";
	rowTitle = "";
	rowReferrers = "";
	rowComponents = 0;
	incPC();
	rowRank = data[pc];
	incPC();
	rowItem = data[pc];
	incPC();
	if (data[pc] == "*local-link*") {
		incPC();
		rowLocalLink = data[pc];
		incPC();
	}
	if (data[pc] == "*title*") {
		incPC();
		rowTitle = data[pc];
		incPC();
	}
	if (data[pc] == "*referrers*") {
		incPC();
		rowReferrers = data[pc];
		incPC();
	}
	if (data[pc] == "*components*") {
		if (compileComponentList() == 0) {
			return 0;
		}
	}		
	rowAccesses = data[pc];	
	incPC();
	rowAccessesPer = data[pc];	
	incPC();
	if (withBytes) {
		rowBytes = data[pc];
		incPC();
		rowBytesPer = data[pc];
		incPC();
	}
	if (withVisits) {
		rowVisits = data[pc];
		incPC();
		rowVisitsPer = data[pc];
		incPC();
	}
	if (withDownloads) {
		rowDownloadsPer = data[pc];
		incPC();
	}
	subreportRow();	
	return 1;
}

function compileComponentList()
{
	if (data[pc] != "*components*") {
		return 0;
	}
	incPC();
	var alreadyFirst = componentsByPage[rowItem + ".1"];
	while (dataEnd == 0) {
		if (data[pc] == "*end*") {
			incPC();
			return 1;
		}
		rowComponents++;
		var index = data[pc];
		incPC();
		if (!alreadyFirst) {
			addComponent(rowItem, index);
		}
	}
	return -1;
}

function listComponents(w, item)
{
	var i = 1;
	while (1) {
		var s = componentsByPage[item + "." + i];
		if (!s) {
			return;
		}
		var offset = s * 5;
		var name = componentsData[offset++];
		var accesses = componentsData[offset++];
		var bytes = componentsData[offset++];
		var visits = componentsData[offset++];
		var downloadsper = componentsData[offset++];
		w.document.writeln("<td>");
		w.document.writeln(name);
		w.document.writeln("</td>");
		w.document.writeln("<td align=right>");
		w.document.writeln(accesses);
		w.document.writeln("</td>");
		if (withBytes) {
			w.document.writeln("<td align=right>");
			w.document.writeln(bytes);
			w.document.writeln("</td>");
		}	
		if (withVisits) {
			w.document.writeln("<td align=right>");
			w.document.writeln(visits);
			w.document.writeln("</td>");
		}	
		if (withDownloads) {
			w.document.writeln("<td align=right>");
			w.document.writeln(downloadsper);
			w.document.writeln("</td>");
		}	
		w.document.writeln("</tr>");
		i++;
	}
}

function addComponent(item, index)
{
	var i = 1;
	while (1) {
		var s = componentsByPage[item + "." + i];
		if (!s) {
			break;
		}
		i++;
	}
	componentsByPage[item + "." + i] = index;
}

function wl(s)
{
	document.writeln(s);
}
function documentsHead()
{
	wl("<h2>" + epochRange + "</h2>");
	

wl("<h3>Menu</h3>");

wl("<div class='colmenu'>");
wl("<div class='column'>");
wl("<ul id='infolankar'>");
wl("<li><a href=\"../../../../index.php\">Calendar of Reports</a></li>");
wl("<li><a href=\"index.php\">Executive Summary and Totals</a></li>");
wl("<li><a href=\"hours.php\">Accesses by Hour</a></li>");
wl("<li><a href=\"days.php\">Accesses by Day</a></li>");
wl("<li><a href=\"documents.php\">Top Documents</a></li>");
wl("<li><a href=\"directories.php\">Documents by Directory</a></li>");
wl("<li><a href=\"types.php\">File Types (Extensions)</a></li>");

wl("</ul>");
wl("</div>");
wl("<div class='column'>");
wl("<ul id='infolankar'>");
wl("<li><a href=\"sites.php\">Top Visitor Sites</a></li>");
wl("<li><a href=\"useragents.php\">Top Web Browsers</a></li>");
wl("<li><a href=\"os.php\">Top Operating Systems</a></li>");
wl("<li><a href=\"referringsites.php\">Top Referring Sites</a></li>");
wl("<li><a href=\"notfound.php\">Documents Not Found</a></li>");
wl("<li><a href=\"resultcodes.php\">Accesses by Result Code</a></li>");

wl("</ul>");
wl("</div>");
wl("<div class='colmenufoot'></div>");
wl("</div>");

}
function documentsTail()
{
}
function subreportHead()
{
	wl("<h3><a name=\"" + subreportKeyword + "\">");
	if (subreportAll) {
		wl("All " + subreportTitle);
	} else {
		wl("Top " + subreportTop + " of " + subreportTotal + " " + subreportTitle + ", ");
	}
        if (sortedByAccesses) {
                wl("Sorted by Access Count");
        } else if (sortedByBytes) {
                wl("Sorted by Bytes");
        } else if (sortedByVisits) {
                wl("Sorted by Visits");
        }
        if (alphabetized) {
                wl("and Alphabetized");
        }
	wl("</a></h3><p></p>"); 

	if (subreportPie) {
		wl("<div align=\"center\">");
		wl(subreportPie);
		wl("</div>");
	}
	wl("<table cellspacing=\"0\" class=\"styladtabell\">");
	wl("<thead>");
	wl("<tr>");
	wl("<th scope=\"col\">Rank</th>");
	wl("<th scope=\"col\">Item</th>");
	wl("<th scope=\"col\">Accesses</th>");
	wl("<th scope=\"col\">%</th>");
	if (withBytes) {
		wl("<th scope=\"col\">Bytes</th>");
		wl("<th scope=\"col\">%</th>");
	}
	if (withVisits) {
		wl("<th scope=\"col\">Visits</th>");
		wl("<th scope=\"col\">%</th>");
	}
	wl("</tr>");
	wl("</thead>");
	wl("<tbody>");
}	
function subreportTail()
{
	wl("</tbody>");
	wl("</table>");
}
function subreportRow()
{
        var gc = "jamn";
        var dirLink = 0;
        if (subreportGreenbarFlag) {
                gc = "udda";
        }
        subreportGreenbarFlag = !subreportGreenbarFlag;
	wl("<tr class=\"" + gc + "\">");
	wl("<th class=\"hogerstalld\">" + rowRank + "</th>");
	wl("<td align=\"right\">");
	if (rowLocalLink) {
		wl("<a href=\"" + rowLocalLink + "\">");
	}
	if (rowTitle) {
		wl("<b>" + rowTitle + "</b><br>");
	}
	wl(rowItem);
	if (rowLocalLink) {
		wl("</a>");
	}	
	if (rowReferrers) {
		wl("<a href=\"" + rowReferrers + "\" target=\"referrers\">[Referrers]</a>");
	}
	wl("</td>");
	wl("<td align=\"right\">" + rowAccesses + "</td>");
	wl("<td align=\"right\">" + rowAccessesPer + "</td>");
	if (withBytes) {
		wl("<td align=\"right\">" + rowBytes + "</td>");
		wl("<td align=\"right\">" + rowBytesPer + "</td>");
	}
	if (withVisits) {
		wl("<td align=\"right\">" + rowVisits + "</td>");
		wl("<td align=\"right\">" + rowVisitsPer + "</td>");
	}
	if (withDownloads) {
		wl("<td align=\"right\">" + rowDownloadsPer + "</td>");
	}
	wl("</tr>");
}

compileReports(0);	
</script>
