


<h2>Statistics for http://www.u5645470.fsdata.se, Week of 2019-9-2 to 2019-9-8</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 20 of 2,603 Visitor Sites,
Sorted by Access Count

</h3>
<p></p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Site</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="jamnudda">
	<th class="hogerstalld">1</th>
	<td>
	46.59.25.88		
	</td>
	<td align="right">4,998</td>
	<td align="right">8.32</td>
	<td align="right">225,738,245</td>
	<td align="right">3.98</td>
	<td align="right">44</td>
	<td align="right">1.76</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	5.35.187.69		
	</td>
	<td align="right">1,739</td>
	<td align="right">2.89</td>
	<td align="right">41,304,415</td>
	<td align="right">0.73</td>
	<td align="right">8</td>
	<td align="right">0.34</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	63.143.42.242		
	</td>
	<td align="right">1,291</td>
	<td align="right">2.15</td>
	<td align="right">82,248</td>
	<td align="right">0.00</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	85.228.32.74		
	</td>
	<td align="right">1,205</td>
	<td align="right">2.01</td>
	<td align="right">8,853,006</td>
	<td align="right">0.16</td>
	<td align="right">4</td>
	<td align="right">0.17</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	89.221.250.27		
	</td>
	<td align="right">486</td>
	<td align="right">0.81</td>
	<td align="right">9,371</td>
	<td align="right">0.00</td>
	<td align="right">10</td>
	<td align="right">0.43</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	79.142.244.252		
	</td>
	<td align="right">403</td>
	<td align="right">0.67</td>
	<td align="right">55,338,695</td>
	<td align="right">0.97</td>
	<td align="right">4</td>
	<td align="right">0.17</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	88.198.25.244		
	</td>
	<td align="right">317</td>
	<td align="right">0.53</td>
	<td align="right">3,942,764</td>
	<td align="right">0.07</td>
	<td align="right">1</td>
	<td align="right">0.04</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	217.140.112.221		
	</td>
	<td align="right">313</td>
	<td align="right">0.52</td>
	<td align="right">32,702,190</td>
	<td align="right">0.58</td>
	<td align="right">4</td>
	<td align="right">0.17</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	213.212.29.165		
	</td>
	<td align="right">302</td>
	<td align="right">0.50</td>
	<td align="right">13,827,115</td>
	<td align="right">0.24</td>
	<td align="right">3</td>
	<td align="right">0.13</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">10</th>
	<td>
	66.249.79.180		
	</td>
	<td align="right">287</td>
	<td align="right">0.48</td>
	<td align="right">3,304,014</td>
	<td align="right">0.06</td>
	<td align="right">35</td>
	<td align="right">1.41</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">11</th>
	<td>
	66.249.79.182		
	</td>
	<td align="right">219</td>
	<td align="right">0.36</td>
	<td align="right">2,935,329</td>
	<td align="right">0.05</td>
	<td align="right">31</td>
	<td align="right">1.24</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">12</th>
	<td>
	31.193.207.12		
	</td>
	<td align="right">215</td>
	<td align="right">0.36</td>
	<td align="right">58,107,138</td>
	<td align="right">1.02</td>
	<td align="right">5</td>
	<td align="right">0.21</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">13</th>
	<td>
	213.113.65.93		
	</td>
	<td align="right">204</td>
	<td align="right">0.34</td>
	<td align="right">21,088,243</td>
	<td align="right">0.37</td>
	<td align="right">2</td>
	<td align="right">0.09</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">14</th>
	<td>
	216.244.66.227		
	</td>
	<td align="right">187</td>
	<td align="right">0.31</td>
	<td align="right">29,411,308</td>
	<td align="right">0.52</td>
	<td align="right">77</td>
	<td align="right">3.08</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">15</th>
	<td>
	212.37.28.30		
	</td>
	<td align="right">187</td>
	<td align="right">0.31</td>
	<td align="right">20,713,193</td>
	<td align="right">0.36</td>
	<td align="right">7</td>
	<td align="right">0.30</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">16</th>
	<td>
	195.181.242.138		
	</td>
	<td align="right">186</td>
	<td align="right">0.31</td>
	<td align="right">1,650,579</td>
	<td align="right">0.03</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">17</th>
	<td>
	79.136.91.86		
	</td>
	<td align="right">184</td>
	<td align="right">0.31</td>
	<td align="right">23,375,539</td>
	<td align="right">0.41</td>
	<td align="right">7</td>
	<td align="right">0.30</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">18</th>
	<td>
	62.20.62.211		
	</td>
	<td align="right">184</td>
	<td align="right">0.31</td>
	<td align="right">25,577,348</td>
	<td align="right">0.45</td>
	<td align="right">7</td>
	<td align="right">0.30</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">19</th>
	<td>
	185.224.57.161		
	</td>
	<td align="right">183</td>
	<td align="right">0.30</td>
	<td align="right">9,090,425</td>
	<td align="right">0.16</td>
	<td align="right">5</td>
	<td align="right">0.22</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">20</th>
	<td>
	66.249.79.186		
	</td>
	<td align="right">150</td>
	<td align="right">0.25</td>
	<td align="right">2,848,090</td>
	<td align="right">0.05</td>
	<td align="right">22</td>
	<td align="right">0.90</td>
</tr>

</tbody>
</table>

