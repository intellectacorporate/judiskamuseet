


<h2>Statistics for http://www.u5645470.fsdata.se, Week of 2019-9-2 to 2019-9-8</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Traffic by Hour</h3>
<p>Figures are averages for that hour
	of each day in the time period analyzed.</p>
<img src="h.png" width="468" height="351">
<br /><br />
<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Hour</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	<th scope='col'>Bits per Sec</th>
	<th scope='col'>Bytes per Sec</th>
	
	</tr>
</thead>
<tbody>
<tr class="jamnudda">
	<th align="right">00:00</th>
	<td align="right">115.43</td>
	<td align="right">1.34</td>
	<td align="right">12,964,682.57</td>
	<td align="right">1.60</td>
	<td align="right">3.89</td>
	<td align="right">1.08</td>
	<td align="right">28,810.41</td>
	<td align="right">3,601.30</td>
	
</tr>
<tr class="jamn">
	<th align="right">01:00</th>
	<td align="right">83.43</td>
	<td align="right">0.97</td>
	<td align="right">4,897,973.43</td>
	<td align="right">0.60</td>
	<td align="right">7.32</td>
	<td align="right">2.03</td>
	<td align="right">10,884.39</td>
	<td align="right">1,360.55</td>
	
</tr>
<tr class="udda">
	<th align="right">02:00</th>
	<td align="right">69.57</td>
	<td align="right">0.81</td>
	<td align="right">3,527,513.57</td>
	<td align="right">0.44</td>
	<td align="right">5.46</td>
	<td align="right">1.51</td>
	<td align="right">7,838.92</td>
	<td align="right">979.86</td>
	
</tr>
<tr class="jamn">
	<th align="right">03:00</th>
	<td align="right">72.43</td>
	<td align="right">0.84</td>
	<td align="right">3,408,085.57</td>
	<td align="right">0.42</td>
	<td align="right">5.77</td>
	<td align="right">1.60</td>
	<td align="right">7,573.52</td>
	<td align="right">946.69</td>
	
</tr>
<tr class="udda">
	<th align="right">04:00</th>
	<td align="right">93.43</td>
	<td align="right">1.09</td>
	<td align="right">3,658,570.57</td>
	<td align="right">0.45</td>
	<td align="right">6.08</td>
	<td align="right">1.68</td>
	<td align="right">8,130.16</td>
	<td align="right">1,016.27</td>
	
</tr>
<tr class="jamn">
	<th align="right">05:00</th>
	<td align="right">68.43</td>
	<td align="right">0.80</td>
	<td align="right">7,810,193.00</td>
	<td align="right">0.96</td>
	<td align="right">5.43</td>
	<td align="right">1.50</td>
	<td align="right">17,355.98</td>
	<td align="right">2,169.50</td>
	
</tr>
<tr class="udda">
	<th align="right">06:00</th>
	<td align="right">104.14</td>
	<td align="right">1.21</td>
	<td align="right">9,854,903.00</td>
	<td align="right">1.22</td>
	<td align="right">5.47</td>
	<td align="right">1.51</td>
	<td align="right">21,899.78</td>
	<td align="right">2,737.47</td>
	
</tr>
<tr class="jamn">
	<th align="right">07:00</th>
	<td align="right">171.86</td>
	<td align="right">2.00</td>
	<td align="right">17,709,873.71</td>
	<td align="right">2.18</td>
	<td align="right">7.93</td>
	<td align="right">2.20</td>
	<td align="right">39,355.27</td>
	<td align="right">4,919.41</td>
	
</tr>
<tr class="udda">
	<th align="right">08:00</th>
	<td align="right">379.00</td>
	<td align="right">4.41</td>
	<td align="right">34,774,666.00</td>
	<td align="right">4.29</td>
	<td align="right">12.44</td>
	<td align="right">3.45</td>
	<td align="right">77,277.04</td>
	<td align="right">9,659.63</td>
	
</tr>
<tr class="jamn">
	<th align="right">09:00</th>
	<td align="right">712.43</td>
	<td align="right">8.30</td>
	<td align="right">64,048,830.71</td>
	<td align="right">7.90</td>
	<td align="right">23.26</td>
	<td align="right">6.44</td>
	<td align="right">142,330.73</td>
	<td align="right">17,791.34</td>
	
</tr>
<tr class="udda">
	<th align="right">10:00</th>
	<td align="right">732.29</td>
	<td align="right">8.53</td>
	<td align="right">76,379,883.14</td>
	<td align="right">9.42</td>
	<td align="right">25.80</td>
	<td align="right">7.14</td>
	<td align="right">169,733.07</td>
	<td align="right">21,216.63</td>
	
</tr>
<tr class="jamn">
	<th align="right">11:00</th>
	<td align="right">595.00</td>
	<td align="right">6.93</td>
	<td align="right">57,447,426.14</td>
	<td align="right">7.08</td>
	<td align="right">22.84</td>
	<td align="right">6.33</td>
	<td align="right">127,660.95</td>
	<td align="right">15,957.62</td>
	
</tr>
<tr class="udda">
	<th align="right">12:00</th>
	<td align="right">595.57</td>
	<td align="right">6.94</td>
	<td align="right">63,107,696.43</td>
	<td align="right">7.78</td>
	<td align="right">25.61</td>
	<td align="right">7.09</td>
	<td align="right">140,239.33</td>
	<td align="right">17,529.92</td>
	
</tr>
<tr class="jamn">
	<th align="right">13:00</th>
	<td align="right">599.00</td>
	<td align="right">6.98</td>
	<td align="right">69,468,163.57</td>
	<td align="right">8.57</td>
	<td align="right">22.67</td>
	<td align="right">6.28</td>
	<td align="right">154,373.70</td>
	<td align="right">19,296.71</td>
	
</tr>
<tr class="udda">
	<th align="right">14:00</th>
	<td align="right">623.43</td>
	<td align="right">7.26</td>
	<td align="right">65,263,210.14</td>
	<td align="right">8.05</td>
	<td align="right">22.05</td>
	<td align="right">6.11</td>
	<td align="right">145,029.36</td>
	<td align="right">18,128.67</td>
	
</tr>
<tr class="jamn">
	<th align="right">15:00</th>
	<td align="right">631.43</td>
	<td align="right">7.36</td>
	<td align="right">49,116,979.14</td>
	<td align="right">6.06</td>
	<td align="right">22.99</td>
	<td align="right">6.37</td>
	<td align="right">109,148.84</td>
	<td align="right">13,643.61</td>
	
</tr>
<tr class="udda">
	<th align="right">16:00</th>
	<td align="right">503.43</td>
	<td align="right">5.86</td>
	<td align="right">43,460,450.43</td>
	<td align="right">5.36</td>
	<td align="right">23.58</td>
	<td align="right">6.53</td>
	<td align="right">96,578.78</td>
	<td align="right">12,072.35</td>
	
</tr>
<tr class="jamn">
	<th align="right">17:00</th>
	<td align="right">350.43</td>
	<td align="right">4.08</td>
	<td align="right">24,025,920.14</td>
	<td align="right">2.96</td>
	<td align="right">16.58</td>
	<td align="right">4.59</td>
	<td align="right">53,390.93</td>
	<td align="right">6,673.87</td>
	
</tr>
<tr class="udda">
	<th align="right">18:00</th>
	<td align="right">331.00</td>
	<td align="right">3.86</td>
	<td align="right">41,021,066.57</td>
	<td align="right">5.06</td>
	<td align="right">15.25</td>
	<td align="right">4.22</td>
	<td align="right">91,157.93</td>
	<td align="right">11,394.74</td>
	
</tr>
<tr class="jamn">
	<th align="right">19:00</th>
	<td align="right">372.00</td>
	<td align="right">4.33</td>
	<td align="right">40,947,229.57</td>
	<td align="right">5.05</td>
	<td align="right">13.36</td>
	<td align="right">3.70</td>
	<td align="right">90,993.84</td>
	<td align="right">11,374.23</td>
	
</tr>
<tr class="udda">
	<th align="right">20:00</th>
	<td align="right">394.57</td>
	<td align="right">4.60</td>
	<td align="right">29,967,493.43</td>
	<td align="right">3.70</td>
	<td align="right">15.71</td>
	<td align="right">4.35</td>
	<td align="right">66,594.43</td>
	<td align="right">8,324.30</td>
	
</tr>
<tr class="jamn">
	<th align="right">21:00</th>
	<td align="right">378.29</td>
	<td align="right">4.41</td>
	<td align="right">28,145,374.29</td>
	<td align="right">3.47</td>
	<td align="right">18.05</td>
	<td align="right">5.00</td>
	<td align="right">62,545.28</td>
	<td align="right">7,818.16</td>
	
</tr>
<tr class="udda">
	<th align="right">22:00</th>
	<td align="right">367.86</td>
	<td align="right">4.28</td>
	<td align="right">29,742,473.14</td>
	<td align="right">3.67</td>
	<td align="right">14.91</td>
	<td align="right">4.13</td>
	<td align="right">66,094.38</td>
	<td align="right">8,261.80</td>
	
</tr>
<tr class="jamn">
	<th align="right">23:00</th>
	<td align="right">240.43</td>
	<td align="right">2.80</td>
	<td align="right">30,087,775.86</td>
	<td align="right">3.71</td>
	<td align="right">18.67</td>
	<td align="right">5.17</td>
	<td align="right">66,861.72</td>
	<td align="right">8,357.72</td>
	
</tr>

</tbody>
</table>

