


<h2>Statistics for http://www.u5645470.fsdata.se, Week of 2019-9-30 to 2019-10-6</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 20 of 3,969 Visitor Sites,
Sorted by Access Count

</h3>
<p></p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Site</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="jamnudda">
	<th class="hogerstalld">1</th>
	<td>
	188.151.113.17		
	</td>
	<td align="right">2,228</td>
	<td align="right">2.35</td>
	<td align="right">306,839,022</td>
	<td align="right">4.22</td>
	<td align="right">13</td>
	<td align="right">0.35</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	94.75.211.73		
	</td>
	<td align="right">1,567</td>
	<td align="right">1.65</td>
	<td align="right">12,857,922</td>
	<td align="right">0.18</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	151.106.52.134		
	</td>
	<td align="right">1,561</td>
	<td align="right">1.65</td>
	<td align="right">12,677,091</td>
	<td align="right">0.17</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	81.17.62.205		
	</td>
	<td align="right">1,559</td>
	<td align="right">1.65</td>
	<td align="right">12,743,054</td>
	<td align="right">0.18</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	185.93.3.92		
	</td>
	<td align="right">1,558</td>
	<td align="right">1.64</td>
	<td align="right">12,661,248</td>
	<td align="right">0.17</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	109.123.101.103		
	</td>
	<td align="right">1,557</td>
	<td align="right">1.64</td>
	<td align="right">12,511,739</td>
	<td align="right">0.17</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	85.195.116.134		
	</td>
	<td align="right">1,556</td>
	<td align="right">1.64</td>
	<td align="right">12,644,184</td>
	<td align="right">0.17</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	94.247.174.83		
	</td>
	<td align="right">1,556</td>
	<td align="right">1.64</td>
	<td align="right">12,693,571</td>
	<td align="right">0.17</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	185.246.208.82		
	</td>
	<td align="right">1,549</td>
	<td align="right">1.63</td>
	<td align="right">12,563,265</td>
	<td align="right">0.17</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">10</th>
	<td>
	185.152.65.167		
	</td>
	<td align="right">1,484</td>
	<td align="right">1.57</td>
	<td align="right">12,740,458</td>
	<td align="right">0.18</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">11</th>
	<td>
	46.59.25.88		
	</td>
	<td align="right">1,453</td>
	<td align="right">1.53</td>
	<td align="right">46,730,138</td>
	<td align="right">0.64</td>
	<td align="right">27</td>
	<td align="right">0.71</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">12</th>
	<td>
	63.143.42.242		
	</td>
	<td align="right">1,291</td>
	<td align="right">1.36</td>
	<td align="right">279,770</td>
	<td align="right">0.00</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">13</th>
	<td>
	213.212.29.165		
	</td>
	<td align="right">896</td>
	<td align="right">0.95</td>
	<td align="right">60,452,228</td>
	<td align="right">0.83</td>
	<td align="right">6</td>
	<td align="right">0.16</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">14</th>
	<td>
	82.103.139.165		
	</td>
	<td align="right">841</td>
	<td align="right">0.89</td>
	<td align="right">6,688,841</td>
	<td align="right">0.09</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">15</th>
	<td>
	185.70.76.23		
	</td>
	<td align="right">630</td>
	<td align="right">0.66</td>
	<td align="right">5,772,960</td>
	<td align="right">0.08</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">16</th>
	<td>
	89.221.250.27		
	</td>
	<td align="right">423</td>
	<td align="right">0.45</td>
	<td align="right">38,676,488</td>
	<td align="right">0.53</td>
	<td align="right">8</td>
	<td align="right">0.22</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">17</th>
	<td>
	159.69.189.213		
	</td>
	<td align="right">407</td>
	<td align="right">0.43</td>
	<td align="right">5,274,185</td>
	<td align="right">0.07</td>
	<td align="right">1</td>
	<td align="right">0.03</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">18</th>
	<td>
	5.150.247.74		
	</td>
	<td align="right">327</td>
	<td align="right">0.35</td>
	<td align="right">38,010,657</td>
	<td align="right">0.52</td>
	<td align="right">3</td>
	<td align="right">0.08</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">19</th>
	<td>
	185.213.154.239		
	</td>
	<td align="right">322</td>
	<td align="right">0.34</td>
	<td align="right">99,297,923</td>
	<td align="right">1.36</td>
	<td align="right">6</td>
	<td align="right">0.16</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">20</th>
	<td>
	62.20.62.209		
	</td>
	<td align="right">316</td>
	<td align="right">0.33</td>
	<td align="right">13,844,483</td>
	<td align="right">0.19</td>
	<td align="right">6</td>
	<td align="right">0.16</td>
</tr>

</tbody>
</table>

