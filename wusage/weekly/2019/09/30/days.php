


<h2>Statistics for http://www.u5645470.fsdata.se, Week of 2019-9-30 to 2019-10-6</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Traffic by Day</h3>
<img src="d.png" width="468" height="351">
<br /><br />


<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Date</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	<th scope='col'>Bits per Sec</th>
	<th scope='col'>Bytes per Sec</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th align="right">2019-9-30</th>
	<td align="right">12,730	</td>
	<td align="right">13.43	</td>
	<td align="right">795,805,719	</td>
	<td align="right">10.94	</td>
	<td align="right">511	</td>
	<td align="right">13.15	</td>
	<td align="right">73,685.71	</td>
	<td align="right">9,210.71	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-10-1</th>
	<td align="right">15,137	</td>
	<td align="right">15.97	</td>
	<td align="right">956,228,151	</td>
	<td align="right">13.14	</td>
	<td align="right">623	</td>
	<td align="right">16.04	</td>
	<td align="right">88,539.64	</td>
	<td align="right">11,067.46	</td>
</tr>
<tr class="udda">
	<th align="right">2019-10-2</th>
	<td align="right">17,959	</td>
	<td align="right">18.95	</td>
	<td align="right">2,249,946,357	</td>
	<td align="right">30.92	</td>
	<td align="right">497	</td>
	<td align="right">12.79	</td>
	<td align="right">208,328.37	</td>
	<td align="right">26,041.05	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-10-3</th>
	<td align="right">14,488	</td>
	<td align="right">15.29	</td>
	<td align="right">700,218,215	</td>
	<td align="right">9.62	</td>
	<td align="right">541	</td>
	<td align="right">13.93	</td>
	<td align="right">64,835.02	</td>
	<td align="right">8,104.38	</td>
</tr>
<tr class="udda">
	<th align="right">2019-10-4</th>
	<td align="right">15,011	</td>
	<td align="right">15.84	</td>
	<td align="right">1,156,831,774	</td>
	<td align="right">15.90	</td>
	<td align="right">662	</td>
	<td align="right">17.04	</td>
	<td align="right">107,114.05	</td>
	<td align="right">13,389.26	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-10-5</th>
	<td align="right">9,514	</td>
	<td align="right">10.04	</td>
	<td align="right">757,390,482	</td>
	<td align="right">10.41	</td>
	<td align="right">523	</td>
	<td align="right">13.46	</td>
	<td align="right">70,128.75	</td>
	<td align="right">8,766.09	</td>
</tr>
<tr class="udda">
	<th align="right">2019-10-6</th>
	<td align="right">9,926	</td>
	<td align="right">10.47	</td>
	<td align="right">660,205,965	</td>
	<td align="right">9.07	</td>
	<td align="right">528	</td>
	<td align="right">13.59	</td>
	<td align="right">61,130.18	</td>
	<td align="right">7,641.27	</td>
</tr>

</tbody>
</table>

