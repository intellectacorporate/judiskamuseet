


<h2>Statistics for http://www.u5645470.fsdata.se, Week of 2019-9-30 to 2019-10-6</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 10 of 21 File Types (Extensions), 
Sorted by Access Count

</h3>
<p>Individual file types as determined by file extensions. All URLs that
do not contain an extension are counted as directories.</p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Type</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th class="hogerstalld">1</th>
	<td>
	Directory (folder)		
	</td>
	<td align="right">27,043</td>
	<td align="right">28.54</td>
	<td align="right">258,101,259</td>
	<td align="right">3.55</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	js		
	</td>
	<td align="right">22,133</td>
	<td align="right">23.36</td>
	<td align="right">291,472,874</td>
	<td align="right">4.01</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	jpg		
	</td>
	<td align="right">14,738</td>
	<td align="right">15.55</td>
	<td align="right">4,154,510,337</td>
	<td align="right">57.09</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	css		
	</td>
	<td align="right">14,046</td>
	<td align="right">14.82</td>
	<td align="right">281,225,328</td>
	<td align="right">3.86</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	png		
	</td>
	<td align="right">10,745</td>
	<td align="right">11.34</td>
	<td align="right">285,748,011</td>
	<td align="right">3.93</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	php		
	</td>
	<td align="right">2,913</td>
	<td align="right">3.07</td>
	<td align="right">77,455,953</td>
	<td align="right">1.06</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	ttf		
	</td>
	<td align="right">1,985</td>
	<td align="right">2.09</td>
	<td align="right">123,157,782</td>
	<td align="right">1.69</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	txt		
	</td>
	<td align="right">553</td>
	<td align="right">0.58</td>
	<td align="right">37,260</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	jpeg		
	</td>
	<td align="right">163</td>
	<td align="right">0.17</td>
	<td align="right">70,719,115</td>
	<td align="right">0.97</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">10</th>
	<td>
	ico		
	</td>
	<td align="right">136</td>
	<td align="right">0.14</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>

</tbody>
</table>

