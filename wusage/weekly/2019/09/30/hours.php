


<h2>Statistics for http://www.u5645470.fsdata.se, Week of 2019-9-30 to 2019-10-6</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Traffic by Hour</h3>
<p>Figures are averages for that hour
	of each day in the time period analyzed.</p>
<img src="h.png" width="468" height="351">
<br /><br />
<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Hour</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	<th scope='col'>Bits per Sec</th>
	<th scope='col'>Bytes per Sec</th>
	
	</tr>
</thead>
<tbody>
<tr class="jamnudda">
	<th align="right">00:00</th>
	<td align="right">264.00</td>
	<td align="right">1.95</td>
	<td align="right">8,526,811.00</td>
	<td align="right">0.82</td>
	<td align="right">11.20</td>
	<td align="right">2.02</td>
	<td align="right">18,948.47</td>
	<td align="right">2,368.56</td>
	
</tr>
<tr class="jamn">
	<th align="right">01:00</th>
	<td align="right">191.57</td>
	<td align="right">1.42</td>
	<td align="right">6,879,527.57</td>
	<td align="right">0.66</td>
	<td align="right">10.47</td>
	<td align="right">1.89</td>
	<td align="right">15,287.84</td>
	<td align="right">1,910.98</td>
	
</tr>
<tr class="udda">
	<th align="right">02:00</th>
	<td align="right">151.71</td>
	<td align="right">1.12</td>
	<td align="right">5,986,863.86</td>
	<td align="right">0.58</td>
	<td align="right">7.74</td>
	<td align="right">1.40</td>
	<td align="right">13,304.14</td>
	<td align="right">1,663.02</td>
	
</tr>
<tr class="jamn">
	<th align="right">03:00</th>
	<td align="right">176.86</td>
	<td align="right">1.31</td>
	<td align="right">13,600,771.14</td>
	<td align="right">1.31</td>
	<td align="right">7.46</td>
	<td align="right">1.34</td>
	<td align="right">30,223.94</td>
	<td align="right">3,777.99</td>
	
</tr>
<tr class="udda">
	<th align="right">04:00</th>
	<td align="right">181.14</td>
	<td align="right">1.34</td>
	<td align="right">3,290,827.71</td>
	<td align="right">0.32</td>
	<td align="right">10.16</td>
	<td align="right">1.83</td>
	<td align="right">7,312.95</td>
	<td align="right">914.12</td>
	
</tr>
<tr class="jamn">
	<th align="right">05:00</th>
	<td align="right">188.14</td>
	<td align="right">1.39</td>
	<td align="right">3,601,400.86</td>
	<td align="right">0.35</td>
	<td align="right">10.79</td>
	<td align="right">1.94</td>
	<td align="right">8,003.11</td>
	<td align="right">1,000.39</td>
	
</tr>
<tr class="udda">
	<th align="right">06:00</th>
	<td align="right">198.29</td>
	<td align="right">1.46</td>
	<td align="right">7,855,336.43</td>
	<td align="right">0.76</td>
	<td align="right">11.09</td>
	<td align="right">2.00</td>
	<td align="right">17,456.30</td>
	<td align="right">2,182.04</td>
	
</tr>
<tr class="jamn">
	<th align="right">07:00</th>
	<td align="right">384.71</td>
	<td align="right">2.84</td>
	<td align="right">20,923,998.00</td>
	<td align="right">2.01</td>
	<td align="right">17.42</td>
	<td align="right">3.14</td>
	<td align="right">46,497.77</td>
	<td align="right">5,812.22</td>
	
</tr>
<tr class="udda">
	<th align="right">08:00</th>
	<td align="right">569.43</td>
	<td align="right">4.21</td>
	<td align="right">47,799,725.00</td>
	<td align="right">4.60</td>
	<td align="right">16.47</td>
	<td align="right">2.97</td>
	<td align="right">106,221.61</td>
	<td align="right">13,277.70</td>
	
</tr>
<tr class="jamn">
	<th align="right">09:00</th>
	<td align="right">1,019.14</td>
	<td align="right">7.53</td>
	<td align="right">77,854,712.00</td>
	<td align="right">7.49</td>
	<td align="right">32.24</td>
	<td align="right">5.81</td>
	<td align="right">173,010.47</td>
	<td align="right">21,626.31</td>
	
</tr>
<tr class="udda">
	<th align="right">10:00</th>
	<td align="right">1,024.14</td>
	<td align="right">7.57</td>
	<td align="right">84,286,582.43</td>
	<td align="right">8.11</td>
	<td align="right">40.85</td>
	<td align="right">7.36</td>
	<td align="right">187,303.52</td>
	<td align="right">23,412.94</td>
	
</tr>
<tr class="jamn">
	<th align="right">11:00</th>
	<td align="right">977.43</td>
	<td align="right">7.22</td>
	<td align="right">57,630,140.29</td>
	<td align="right">5.54</td>
	<td align="right">38.85</td>
	<td align="right">7.00</td>
	<td align="right">128,066.98</td>
	<td align="right">16,008.37</td>
	
</tr>
<tr class="udda">
	<th align="right">12:00</th>
	<td align="right">726.86</td>
	<td align="right">5.37</td>
	<td align="right">42,337,730.43</td>
	<td align="right">4.07</td>
	<td align="right">32.71</td>
	<td align="right">5.89</td>
	<td align="right">94,083.85</td>
	<td align="right">11,760.48</td>
	
</tr>
<tr class="jamn">
	<th align="right">13:00</th>
	<td align="right">880.43</td>
	<td align="right">6.50</td>
	<td align="right">65,727,398.43</td>
	<td align="right">6.32</td>
	<td align="right">31.60</td>
	<td align="right">5.69</td>
	<td align="right">146,060.89</td>
	<td align="right">18,257.61</td>
	
</tr>
<tr class="udda">
	<th align="right">14:00</th>
	<td align="right">857.00</td>
	<td align="right">6.33</td>
	<td align="right">50,924,092.57</td>
	<td align="right">4.90</td>
	<td align="right">35.82</td>
	<td align="right">6.45</td>
	<td align="right">113,164.65</td>
	<td align="right">14,145.58</td>
	
</tr>
<tr class="jamn">
	<th align="right">15:00</th>
	<td align="right">870.14</td>
	<td align="right">6.43</td>
	<td align="right">184,784,211.57</td>
	<td align="right">17.78</td>
	<td align="right">29.21</td>
	<td align="right">5.26</td>
	<td align="right">410,631.58</td>
	<td align="right">51,328.95</td>
	
</tr>
<tr class="udda">
	<th align="right">16:00</th>
	<td align="right">811.71</td>
	<td align="right">6.00</td>
	<td align="right">101,336,174.29</td>
	<td align="right">9.75</td>
	<td align="right">27.41</td>
	<td align="right">4.94</td>
	<td align="right">225,191.50</td>
	<td align="right">28,148.94</td>
	
</tr>
<tr class="jamn">
	<th align="right">17:00</th>
	<td align="right">611.71</td>
	<td align="right">4.52</td>
	<td align="right">41,910,836.00</td>
	<td align="right">4.03</td>
	<td align="right">21.94</td>
	<td align="right">3.95</td>
	<td align="right">93,135.19</td>
	<td align="right">11,641.90</td>
	
</tr>
<tr class="udda">
	<th align="right">18:00</th>
	<td align="right">566.57</td>
	<td align="right">4.19</td>
	<td align="right">37,941,011.86</td>
	<td align="right">3.65</td>
	<td align="right">27.27</td>
	<td align="right">4.91</td>
	<td align="right">84,313.36</td>
	<td align="right">10,539.17</td>
	
</tr>
<tr class="jamn">
	<th align="right">19:00</th>
	<td align="right">678.57</td>
	<td align="right">5.01</td>
	<td align="right">44,943,796.00</td>
	<td align="right">4.32</td>
	<td align="right">25.94</td>
	<td align="right">4.67</td>
	<td align="right">99,875.10</td>
	<td align="right">12,484.39</td>
	
</tr>
<tr class="udda">
	<th align="right">20:00</th>
	<td align="right">640.29</td>
	<td align="right">4.73</td>
	<td align="right">38,468,707.86</td>
	<td align="right">3.70</td>
	<td align="right">28.66</td>
	<td align="right">5.16</td>
	<td align="right">85,486.02</td>
	<td align="right">10,685.75</td>
	
</tr>
<tr class="jamn">
	<th align="right">21:00</th>
	<td align="right">616.57</td>
	<td align="right">4.55</td>
	<td align="right">37,670,428.00</td>
	<td align="right">3.62</td>
	<td align="right">28.73</td>
	<td align="right">5.18</td>
	<td align="right">83,712.06</td>
	<td align="right">10,464.01</td>
	
</tr>
<tr class="udda">
	<th align="right">22:00</th>
	<td align="right">586.57</td>
	<td align="right">4.33</td>
	<td align="right">37,530,077.00</td>
	<td align="right">3.61</td>
	<td align="right">24.87</td>
	<td align="right">4.48</td>
	<td align="right">83,400.17</td>
	<td align="right">10,425.02</td>
	
</tr>
<tr class="jamn">
	<th align="right">23:00</th>
	<td align="right">364.86</td>
	<td align="right">2.70</td>
	<td align="right">17,706,934.43</td>
	<td align="right">1.70</td>
	<td align="right">26.09</td>
	<td align="right">4.70</td>
	<td align="right">39,348.74</td>
	<td align="right">4,918.59</td>
	
</tr>

</tbody>
</table>

