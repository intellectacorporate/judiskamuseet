


<h2>Statistics for http://www.u5645470.fsdata.se, Week of 2019-9-30 to 2019-10-6</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 10 of 281 Browsers (User Agents),
Sorted by Access Count

</h3>
<p></p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Product</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th class="hogerstalld">1</th>
	<td>
	Safari/537.36		
	</td>
	<td align="right">31,926</td>
	<td align="right">33.70</td>
	<td align="right">2,532,474,669</td>
	<td align="right">34.81</td>
	<td align="right">1,096</td>
	<td align="right">28.22</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	Safari/604.1		
	</td>
	<td align="right">15,945</td>
	<td align="right">16.83</td>
	<td align="right">1,228,812,199</td>
	<td align="right">16.89</td>
	<td align="right">642</td>
	<td align="right">16.53</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	Pingdom.com_bot_version_1.4_		
	</td>
	<td align="right">15,416</td>
	<td align="right">16.27</td>
	<td align="right">126,554,331</td>
	<td align="right">1.74</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	Mozilla/5.0 		
	</td>
	<td align="right">9,610</td>
	<td align="right">10.14</td>
	<td align="right">679,197,762</td>
	<td align="right">9.34</td>
	<td align="right">806</td>
	<td align="right">20.75</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	Safari/605.1.15		
	</td>
	<td align="right">4,621</td>
	<td align="right">4.88</td>
	<td align="right">441,870,619</td>
	<td align="right">6.07</td>
	<td align="right">139</td>
	<td align="right">3.60</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	Safari/537.36 Edge/18.18362		
	</td>
	<td align="right">1,553</td>
	<td align="right">1.64</td>
	<td align="right">234,804,081</td>
	<td align="right">3.23</td>
	<td align="right">61</td>
	<td align="right">1.58</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	Safari/605.1		
	</td>
	<td align="right">1,500</td>
	<td align="right">1.58</td>
	<td align="right">112,868,779</td>
	<td align="right">1.55</td>
	<td align="right">53</td>
	<td align="right">1.39</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	bingbot/2.0		
	</td>
	<td align="right">1,440</td>
	<td align="right">1.52</td>
	<td align="right">156,251,805</td>
	<td align="right">2.15</td>
	<td align="right">317</td>
	<td align="right">8.16</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	UptimeRobot/2.0		
	</td>
	<td align="right">1,294</td>
	<td align="right">1.37</td>
	<td align="right">363,539</td>
	<td align="right">0.00</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">10</th>
	<td>
	Safari/537.36 Edge/18.17763		
	</td>
	<td align="right">842</td>
	<td align="right">0.89</td>
	<td align="right">1,039,841,866</td>
	<td align="right">14.29</td>
	<td align="right">21</td>
	<td align="right">0.54</td>
</tr>

</tbody>
</table>

