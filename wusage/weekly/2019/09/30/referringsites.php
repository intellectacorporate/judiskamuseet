


<h2>Statistics for http://www.u5645470.fsdata.se, Week of 2019-9-30 to 2019-10-6</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 20 of 140 Referring Sites,
Sorted by Access Count

</h3>
<p></p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Referring Site</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th class="hogerstalld">1</th>
	<td>
	<a href="https://judiskamuseet.se">
	https://judiskamuseet.se</a>
	</td>
	<td align="right">60,920</td>
	<td align="right">91.53</td>
	<td align="right">6,490,750,956</td>
	<td align="right">97.88</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	<a href="http://judiskamuseet.se">
	http://judiskamuseet.se</a>
	</td>
	<td align="right">2,705</td>
	<td align="right">4.06</td>
	<td align="right">50,264,665</td>
	<td align="right">0.76</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	<a href="https://www.google.com">
	https://www.google.com</a>
	</td>
	<td align="right">1,445</td>
	<td align="right">2.17</td>
	<td align="right">52,270,420</td>
	<td align="right">0.79</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	<a href="https://www.google.se">
	https://www.google.se</a>
	</td>
	<td align="right">596</td>
	<td align="right">0.90</td>
	<td align="right">20,499,811</td>
	<td align="right">0.31</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	<a href="http://instagram.com">
	http://instagram.com</a>
	</td>
	<td align="right">108</td>
	<td align="right">0.16</td>
	<td align="right">791,142</td>
	<td align="right">0.01</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	<a href="https://www.google.de">
	https://www.google.de</a>
	</td>
	<td align="right">99</td>
	<td align="right">0.15</td>
	<td align="right">1,219,848</td>
	<td align="right">0.02</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	<a href="http://judiskamuseet.se:80">
	http://judiskamuseet.se:80</a>
	</td>
	<td align="right">76</td>
	<td align="right">0.11</td>
	<td align="right">826,654</td>
	<td align="right">0.01</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	<a href="http://m.facebook.com">
	http://m.facebook.com</a>
	</td>
	<td align="right">57</td>
	<td align="right">0.09</td>
	<td align="right">666,325</td>
	<td align="right">0.01</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	<a href="https://www.bing.com">
	https://www.bing.com</a>
	</td>
	<td align="right">50</td>
	<td align="right">0.08</td>
	<td align="right">1,687,161</td>
	<td align="right">0.03</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">10</th>
	<td>
	<a href="http://www.google.com">
	http://www.google.com</a>
	</td>
	<td align="right">45</td>
	<td align="right">0.07</td>
	<td align="right">422,722</td>
	<td align="right">0.01</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">11</th>
	<td>
	<a href="https://www.netcraft.com">
	https://www.netcraft.com</a>
	</td>
	<td align="right">37</td>
	<td align="right">0.06</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">12</th>
	<td>
	<a href="http://site.ru">
	http://site.ru</a>
	</td>
	<td align="right">30</td>
	<td align="right">0.05</td>
	<td align="right">1,325,989</td>
	<td align="right">0.02</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">13</th>
	<td>
	<a href="https://r.search.yahoo.com">
	https://r.search.yahoo.com</a>
	</td>
	<td align="right">15</td>
	<td align="right">0.02</td>
	<td align="right">235,825</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">14</th>
	<td>
	<a href="https://www.facebook.com">
	https://www.facebook.com</a>
	</td>
	<td align="right">14</td>
	<td align="right">0.02</td>
	<td align="right">157,275</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">15</th>
	<td>
	<a href="http://www.judiska-museet.se">
	http://www.judiska-museet.se</a>
	</td>
	<td align="right">10</td>
	<td align="right">0.02</td>
	<td align="right">80</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">16</th>
	<td>
	<a href="https://myactivity.google.com">
	https://myactivity.google.com</a>
	</td>
	<td align="right">10</td>
	<td align="right">0.02</td>
	<td align="right">1,533,964</td>
	<td align="right">0.02</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">17</th>
	<td>
	<a href="http://www.google.se">
	http://www.google.se</a>
	</td>
	<td align="right">9</td>
	<td align="right">0.01</td>
	<td align="right">53,351</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">18</th>
	<td>
	<a href="fbapp://350685531728">
	fbapp://350685531728</a>
	</td>
	<td align="right">9</td>
	<td align="right">0.01</td>
	<td align="right">204,508</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">19</th>
	<td>
	<a href="https://zagadki.in.ua">
	https://zagadki.in.ua</a>
	</td>
	<td align="right">7</td>
	<td align="right">0.01</td>
	<td align="right">281,258</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">20</th>
	<td>
	<a href="https://l.instagram.com">
	https://l.instagram.com</a>
	</td>
	<td align="right">7</td>
	<td align="right">0.01</td>
	<td align="right">48,660</td>
	<td align="right">0.00</td>
</tr>

</tbody>
</table>

