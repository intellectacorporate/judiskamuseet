


<h2>Statistics for http://www.u5645470.fsdata.se, Week of 2019-9-23 to 2019-9-29</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 20 of 179 Referring Sites,
Sorted by Access Count

</h3>
<p></p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Referring Site</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th class="hogerstalld">1</th>
	<td>
	<a href="https://judiskamuseet.se">
	https://judiskamuseet.se</a>
	</td>
	<td align="right">61,131</td>
	<td align="right">90.76</td>
	<td align="right">4,188,519,107</td>
	<td align="right">96.67</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	<a href="http://judiskamuseet.se">
	http://judiskamuseet.se</a>
	</td>
	<td align="right">2,986</td>
	<td align="right">4.43</td>
	<td align="right">63,448,191</td>
	<td align="right">1.46</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	<a href="https://www.google.com">
	https://www.google.com</a>
	</td>
	<td align="right">1,662</td>
	<td align="right">2.47</td>
	<td align="right">42,708,277</td>
	<td align="right">0.99</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	<a href="https://www.google.se">
	https://www.google.se</a>
	</td>
	<td align="right">613</td>
	<td align="right">0.91</td>
	<td align="right">14,881,950</td>
	<td align="right">0.34</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	<a href="http://judiskamuseet.se:80">
	http://judiskamuseet.se:80</a>
	</td>
	<td align="right">98</td>
	<td align="right">0.15</td>
	<td align="right">1,065,459</td>
	<td align="right">0.02</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	<a href="https://judiskamuseet.se:443">
	https://judiskamuseet.se:443</a>
	</td>
	<td align="right">96</td>
	<td align="right">0.14</td>
	<td align="right">1,050,784</td>
	<td align="right">0.02</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	<a href="https://www.google.de">
	https://www.google.de</a>
	</td>
	<td align="right">93</td>
	<td align="right">0.14</td>
	<td align="right">1,231,363</td>
	<td align="right">0.03</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	<a href="http://www.google.com">
	http://www.google.com</a>
	</td>
	<td align="right">55</td>
	<td align="right">0.08</td>
	<td align="right">517,572</td>
	<td align="right">0.01</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	<a href="https://www.bing.com">
	https://www.bing.com</a>
	</td>
	<td align="right">51</td>
	<td align="right">0.08</td>
	<td align="right">6,217,447</td>
	<td align="right">0.14</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">10</th>
	<td>
	<a href="http://site.ru">
	http://site.ru</a>
	</td>
	<td align="right">44</td>
	<td align="right">0.07</td>
	<td align="right">1,523,239</td>
	<td align="right">0.04</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">11</th>
	<td>
	<a href="src='https://bes.belaterbewasthere.com">
	src='https://bes.belaterbewasthere.com</a>
	</td>
	<td align="right">28</td>
	<td align="right">0.04</td>
	<td align="right">86,181</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">12</th>
	<td>
	<a href="http://www.judiska-museet.se">
	http://www.judiska-museet.se</a>
	</td>
	<td align="right">26</td>
	<td align="right">0.04</td>
	<td align="right">31,296</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">13</th>
	<td>
	<a href="http://instagram.com">
	http://instagram.com</a>
	</td>
	<td align="right">25</td>
	<td align="right">0.04</td>
	<td align="right">179,920</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">14</th>
	<td>
	<a href="http://m.facebook.com">
	http://m.facebook.com</a>
	</td>
	<td align="right">19</td>
	<td align="right">0.03</td>
	<td align="right">184,452</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">15</th>
	<td>
	<a href="https://www.facebook.com">
	https://www.facebook.com</a>
	</td>
	<td align="right">10</td>
	<td align="right">0.01</td>
	<td align="right">96,216</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">16</th>
	<td>
	<a href="https://89.221.250.27:443">
	https://89.221.250.27:443</a>
	</td>
	<td align="right">10</td>
	<td align="right">0.01</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">17</th>
	<td>
	<a href="http://localhost">
	http://localhost</a>
	</td>
	<td align="right">9</td>
	<td align="right">0.01</td>
	<td align="right">735,957</td>
	<td align="right">0.02</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">18</th>
	<td>
	<a href="https://byvshie.su">
	https://byvshie.su</a>
	</td>
	<td align="right">8</td>
	<td align="right">0.01</td>
	<td align="right">467,376</td>
	<td align="right">0.01</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">19</th>
	<td>
	<a href="https://l.facebook.com">
	https://l.facebook.com</a>
	</td>
	<td align="right">8</td>
	<td align="right">0.01</td>
	<td align="right">16,643</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">20</th>
	<td>
	<a href="https://duckduckgo.com">
	https://duckduckgo.com</a>
	</td>
	<td align="right">8</td>
	<td align="right">0.01</td>
	<td align="right">94,038</td>
	<td align="right">0.00</td>
</tr>

</tbody>
</table>

