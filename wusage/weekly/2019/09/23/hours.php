


<h2>Statistics for http://www.u5645470.fsdata.se, Week of 2019-9-23 to 2019-9-29</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Traffic by Hour</h3>
<p>Figures are averages for that hour
	of each day in the time period analyzed.</p>
<img src="h.png" width="468" height="351">
<br /><br />
<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Hour</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	<th scope='col'>Bits per Sec</th>
	<th scope='col'>Bytes per Sec</th>
	
	</tr>
</thead>
<tbody>
<tr class="jamnudda">
	<th align="right">00:00</th>
	<td align="right">181.43</td>
	<td align="right">1.56</td>
	<td align="right">10,039,774.29</td>
	<td align="right">1.48</td>
	<td align="right">8.86</td>
	<td align="right">1.81</td>
	<td align="right">22,310.61</td>
	<td align="right">2,788.83</td>
	
</tr>
<tr class="jamn">
	<th align="right">01:00</th>
	<td align="right">145.57</td>
	<td align="right">1.26</td>
	<td align="right">7,172,390.29</td>
	<td align="right">1.06</td>
	<td align="right">10.23</td>
	<td align="right">2.09</td>
	<td align="right">15,938.65</td>
	<td align="right">1,992.33</td>
	
</tr>
<tr class="udda">
	<th align="right">02:00</th>
	<td align="right">77.00</td>
	<td align="right">0.66</td>
	<td align="right">3,280,858.14</td>
	<td align="right">0.48</td>
	<td align="right">7.93</td>
	<td align="right">1.62</td>
	<td align="right">7,290.80</td>
	<td align="right">911.35</td>
	
</tr>
<tr class="jamn">
	<th align="right">03:00</th>
	<td align="right">134.29</td>
	<td align="right">1.16</td>
	<td align="right">5,297,151.57</td>
	<td align="right">0.78</td>
	<td align="right">9.92</td>
	<td align="right">2.02</td>
	<td align="right">11,771.45</td>
	<td align="right">1,471.43</td>
	
</tr>
<tr class="udda">
	<th align="right">04:00</th>
	<td align="right">144.57</td>
	<td align="right">1.25</td>
	<td align="right">3,374,047.71</td>
	<td align="right">0.50</td>
	<td align="right">9.62</td>
	<td align="right">1.96</td>
	<td align="right">7,497.88</td>
	<td align="right">937.24</td>
	
</tr>
<tr class="jamn">
	<th align="right">05:00</th>
	<td align="right">137.86</td>
	<td align="right">1.19</td>
	<td align="right">4,629,968.43</td>
	<td align="right">0.68</td>
	<td align="right">8.55</td>
	<td align="right">1.74</td>
	<td align="right">10,288.82</td>
	<td align="right">1,286.10</td>
	
</tr>
<tr class="udda">
	<th align="right">06:00</th>
	<td align="right">166.14</td>
	<td align="right">1.43</td>
	<td align="right">6,185,826.57</td>
	<td align="right">0.91</td>
	<td align="right">9.16</td>
	<td align="right">1.87</td>
	<td align="right">13,746.28</td>
	<td align="right">1,718.29</td>
	
</tr>
<tr class="jamn">
	<th align="right">07:00</th>
	<td align="right">293.86</td>
	<td align="right">2.53</td>
	<td align="right">17,950,928.57</td>
	<td align="right">2.64</td>
	<td align="right">13.90</td>
	<td align="right">2.83</td>
	<td align="right">39,890.95</td>
	<td align="right">4,986.37</td>
	
</tr>
<tr class="udda">
	<th align="right">08:00</th>
	<td align="right">553.29</td>
	<td align="right">4.77</td>
	<td align="right">36,071,343.14</td>
	<td align="right">5.31</td>
	<td align="right">16.92</td>
	<td align="right">3.45</td>
	<td align="right">80,158.54</td>
	<td align="right">10,019.82</td>
	
</tr>
<tr class="jamn">
	<th align="right">09:00</th>
	<td align="right">744.14</td>
	<td align="right">6.42</td>
	<td align="right">42,064,913.71</td>
	<td align="right">6.19</td>
	<td align="right">27.44</td>
	<td align="right">5.59</td>
	<td align="right">93,477.59</td>
	<td align="right">11,684.70</td>
	
</tr>
<tr class="udda">
	<th align="right">10:00</th>
	<td align="right">1,063.14</td>
	<td align="right">9.17</td>
	<td align="right">70,592,547.71</td>
	<td align="right">10.39</td>
	<td align="right">32.61</td>
	<td align="right">6.64</td>
	<td align="right">156,872.33</td>
	<td align="right">19,609.04</td>
	
</tr>
<tr class="jamn">
	<th align="right">11:00</th>
	<td align="right">790.29</td>
	<td align="right">6.82</td>
	<td align="right">51,156,404.71</td>
	<td align="right">7.53</td>
	<td align="right">37.48</td>
	<td align="right">7.64</td>
	<td align="right">113,680.90</td>
	<td align="right">14,210.11</td>
	
</tr>
<tr class="udda">
	<th align="right">12:00</th>
	<td align="right">746.57</td>
	<td align="right">6.44</td>
	<td align="right">46,216,123.00</td>
	<td align="right">6.80</td>
	<td align="right">23.18</td>
	<td align="right">4.72</td>
	<td align="right">102,702.50</td>
	<td align="right">12,837.81</td>
	
</tr>
<tr class="jamn">
	<th align="right">13:00</th>
	<td align="right">917.86</td>
	<td align="right">7.92</td>
	<td align="right">52,101,960.86</td>
	<td align="right">7.67</td>
	<td align="right">34.92</td>
	<td align="right">7.12</td>
	<td align="right">115,782.14</td>
	<td align="right">14,472.77</td>
	
</tr>
<tr class="udda">
	<th align="right">14:00</th>
	<td align="right">710.14</td>
	<td align="right">6.13</td>
	<td align="right">42,371,240.14</td>
	<td align="right">6.24</td>
	<td align="right">31.85</td>
	<td align="right">6.49</td>
	<td align="right">94,158.31</td>
	<td align="right">11,769.79</td>
	
</tr>
<tr class="jamn">
	<th align="right">15:00</th>
	<td align="right">643.29</td>
	<td align="right">5.55</td>
	<td align="right">43,052,983.57</td>
	<td align="right">6.34</td>
	<td align="right">28.04</td>
	<td align="right">5.71</td>
	<td align="right">95,673.30</td>
	<td align="right">11,959.16</td>
	
</tr>
<tr class="udda">
	<th align="right">16:00</th>
	<td align="right">739.86</td>
	<td align="right">6.38</td>
	<td align="right">41,224,555.29</td>
	<td align="right">6.07</td>
	<td align="right">22.57</td>
	<td align="right">4.60</td>
	<td align="right">91,610.12</td>
	<td align="right">11,451.27</td>
	
</tr>
<tr class="jamn">
	<th align="right">17:00</th>
	<td align="right">646.43</td>
	<td align="right">5.58</td>
	<td align="right">38,668,635.14</td>
	<td align="right">5.69</td>
	<td align="right">25.18</td>
	<td align="right">5.13</td>
	<td align="right">85,930.30</td>
	<td align="right">10,741.29</td>
	
</tr>
<tr class="udda">
	<th align="right">18:00</th>
	<td align="right">516.14</td>
	<td align="right">4.45</td>
	<td align="right">32,656,578.71</td>
	<td align="right">4.81</td>
	<td align="right">27.02</td>
	<td align="right">5.51</td>
	<td align="right">72,570.17</td>
	<td align="right">9,071.27</td>
	
</tr>
<tr class="jamn">
	<th align="right">19:00</th>
	<td align="right">548.00</td>
	<td align="right">4.73</td>
	<td align="right">30,849,590.43</td>
	<td align="right">4.54</td>
	<td align="right">25.31</td>
	<td align="right">5.16</td>
	<td align="right">68,554.65</td>
	<td align="right">8,569.33</td>
	
</tr>
<tr class="udda">
	<th align="right">20:00</th>
	<td align="right">455.14</td>
	<td align="right">3.93</td>
	<td align="right">25,331,225.86</td>
	<td align="right">3.73</td>
	<td align="right">22.56</td>
	<td align="right">4.60</td>
	<td align="right">56,291.61</td>
	<td align="right">7,036.45</td>
	
</tr>
<tr class="jamn">
	<th align="right">21:00</th>
	<td align="right">487.00</td>
	<td align="right">4.20</td>
	<td align="right">29,569,719.57</td>
	<td align="right">4.35</td>
	<td align="right">18.15</td>
	<td align="right">3.70</td>
	<td align="right">65,710.49</td>
	<td align="right">8,213.81</td>
	
</tr>
<tr class="udda">
	<th align="right">22:00</th>
	<td align="right">464.57</td>
	<td align="right">4.01</td>
	<td align="right">24,317,865.00</td>
	<td align="right">3.58</td>
	<td align="right">18.14</td>
	<td align="right">3.70</td>
	<td align="right">54,039.70</td>
	<td align="right">6,754.96</td>
	
</tr>
<tr class="jamn">
	<th align="right">23:00</th>
	<td align="right">287.57</td>
	<td align="right">2.48</td>
	<td align="right">15,306,412.71</td>
	<td align="right">2.25</td>
	<td align="right">21.19</td>
	<td align="right">4.32</td>
	<td align="right">34,014.25</td>
	<td align="right">4,251.78</td>
	
</tr>

</tbody>
</table>

