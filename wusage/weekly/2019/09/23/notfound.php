


<h2>Statistics for http://www.u5645470.fsdata.se, Week of 2019-9-23 to 2019-9-29</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 10 of 301 Documents Not Found,
Sorted by Access Count

</h3>
<p></p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Document</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th class="hogerstalld">1</th>
	<td>
	/judiskamuseet.sql
	</td>
	<td align="right">31</td>
	<td align="right">4.79</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	/wp-json/oembed/1.0/embed
	</td>
	<td align="right">27</td>
	<td align="right">4.17</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	/wp-content/uploads/2017/09/synagogan-på-själagårdsgatan-19.jpg
	</td>
	<td align="right">18</td>
	<td align="right">2.78</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	/autodiscover/autodiscover.xml
	</td>
	<td align="right">18</td>
	<td align="right">2.78</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	/ads.txt
	</td>
	<td align="right">17</td>
	<td align="right">2.63</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	/wp-content/uploads/2017/09/tefillin-med-påse.jpg
	</td>
	<td align="right">13</td>
	<td align="right">2.01</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	/tracesofexistence
	</td>
	<td align="right">12</td>
	<td align="right">1.85</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	/backup.sql
	</td>
	<td align="right">9</td>
	<td align="right">1.39</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	//
	</td>
	<td align="right">8</td>
	<td align="right">1.24</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">10</th>
	<td>
	/wp-content/uploads/do.php
	</td>
	<td align="right">8</td>
	<td align="right">1.24</td>
</tr>

</tbody>
</table>

