


<h2>Statistics for http://www.u5645470.fsdata.se, Week of 2019-9-23 to 2019-9-29</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 10 of 217 Browsers (User Agents),
Sorted by Access Count

</h3>
<p></p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Product</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th class="hogerstalld">1</th>
	<td>
	Safari/537.36		
	</td>
	<td align="right">31,841</td>
	<td align="right">39.26</td>
	<td align="right">1,979,274,469</td>
	<td align="right">41.62</td>
	<td align="right">1,060</td>
	<td align="right">30.87</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	Safari/604.1		
	</td>
	<td align="right">16,151</td>
	<td align="right">19.91</td>
	<td align="right">999,717,627</td>
	<td align="right">21.02</td>
	<td align="right">534</td>
	<td align="right">15.57</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	Mozilla/5.0 		
	</td>
	<td align="right">8,998</td>
	<td align="right">11.09</td>
	<td align="right">533,902,041</td>
	<td align="right">11.23</td>
	<td align="right">532</td>
	<td align="right">15.51</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	Safari/605.1.15		
	</td>
	<td align="right">4,615</td>
	<td align="right">5.69</td>
	<td align="right">255,772,203</td>
	<td align="right">5.38</td>
	<td align="right">125</td>
	<td align="right">3.66</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	Safari/605.1		
	</td>
	<td align="right">2,015</td>
	<td align="right">2.48</td>
	<td align="right">168,354,020</td>
	<td align="right">3.54</td>
	<td align="right">59</td>
	<td align="right">1.74</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	bingbot/2.0		
	</td>
	<td align="right">1,920</td>
	<td align="right">2.37</td>
	<td align="right">168,228,984</td>
	<td align="right">3.54</td>
	<td align="right">439</td>
	<td align="right">12.80</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	Safari/537.36 Edge/18.18362		
	</td>
	<td align="right">1,898</td>
	<td align="right">2.34</td>
	<td align="right">156,633,241</td>
	<td align="right">3.29</td>
	<td align="right">59</td>
	<td align="right">1.74</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	UptimeRobot/2.0		
	</td>
	<td align="right">1,307</td>
	<td align="right">1.61</td>
	<td align="right">81,621</td>
	<td align="right">0.00</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	Pingdom.com_bot_version_1.4_		
	</td>
	<td align="right">1,274</td>
	<td align="right">1.57</td>
	<td align="right">10,279,399</td>
	<td align="right">0.22</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">10</th>
	<td>
	Safari/537.36 Edge/18.17763		
	</td>
	<td align="right">1,172</td>
	<td align="right">1.44</td>
	<td align="right">80,372,690</td>
	<td align="right">1.69</td>
	<td align="right">36</td>
	<td align="right">1.06</td>
</tr>

</tbody>
</table>

