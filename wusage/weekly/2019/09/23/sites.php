


<h2>Statistics for http://www.u5645470.fsdata.se, Week of 2019-9-23 to 2019-9-29</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 20 of 3,527 Visitor Sites,
Sorted by Access Count

</h3>
<p></p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Site</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="jamnudda">
	<th class="hogerstalld">1</th>
	<td>
	46.59.25.88		
	</td>
	<td align="right">1,415</td>
	<td align="right">1.74</td>
	<td align="right">91,503,269</td>
	<td align="right">1.92</td>
	<td align="right">36</td>
	<td align="right">1.05</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	63.143.42.242		
	</td>
	<td align="right">1,307</td>
	<td align="right">1.61</td>
	<td align="right">81,621</td>
	<td align="right">0.00</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	188.151.113.17		
	</td>
	<td align="right">757</td>
	<td align="right">0.93</td>
	<td align="right">24,096,602</td>
	<td align="right">0.51</td>
	<td align="right">5</td>
	<td align="right">0.16</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	54.197.234.188		
	</td>
	<td align="right">537</td>
	<td align="right">0.66</td>
	<td align="right">16,155,383</td>
	<td align="right">0.34</td>
	<td align="right">1</td>
	<td align="right">0.03</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	78.46.90.53		
	</td>
	<td align="right">487</td>
	<td align="right">0.60</td>
	<td align="right">2,954,376</td>
	<td align="right">0.06</td>
	<td align="right">1</td>
	<td align="right">0.03</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	195.22.74.51		
	</td>
	<td align="right">474</td>
	<td align="right">0.58</td>
	<td align="right">27,571,961</td>
	<td align="right">0.58</td>
	<td align="right">6</td>
	<td align="right">0.19</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	89.221.250.27		
	</td>
	<td align="right">435</td>
	<td align="right">0.54</td>
	<td align="right">8,500</td>
	<td align="right">0.00</td>
	<td align="right">7</td>
	<td align="right">0.22</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	185.6.9.220		
	</td>
	<td align="right">425</td>
	<td align="right">0.52</td>
	<td align="right">23,540,319</td>
	<td align="right">0.49</td>
	<td align="right">1</td>
	<td align="right">0.03</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	92.34.135.106		
	</td>
	<td align="right">327</td>
	<td align="right">0.40</td>
	<td align="right">42,551,670</td>
	<td align="right">0.89</td>
	<td align="right">6</td>
	<td align="right">0.19</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">10</th>
	<td>
	62.20.31.94		
	</td>
	<td align="right">317</td>
	<td align="right">0.39</td>
	<td align="right">13,508,294</td>
	<td align="right">0.28</td>
	<td align="right">6</td>
	<td align="right">0.19</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">11</th>
	<td>
	31.208.201.40		
	</td>
	<td align="right">294</td>
	<td align="right">0.36</td>
	<td align="right">10,733,693</td>
	<td align="right">0.23</td>
	<td align="right">3</td>
	<td align="right">0.09</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">12</th>
	<td>
	193.180.154.229		
	</td>
	<td align="right">263</td>
	<td align="right">0.32</td>
	<td align="right">14,811,682</td>
	<td align="right">0.31</td>
	<td align="right">8</td>
	<td align="right">0.25</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">13</th>
	<td>
	216.244.66.227		
	</td>
	<td align="right">250</td>
	<td align="right">0.31</td>
	<td align="right">5,536,702</td>
	<td align="right">0.12</td>
	<td align="right">60</td>
	<td align="right">1.77</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">14</th>
	<td>
	62.220.160.147		
	</td>
	<td align="right">226</td>
	<td align="right">0.28</td>
	<td align="right">11,538,411</td>
	<td align="right">0.24</td>
	<td align="right">6</td>
	<td align="right">0.19</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">15</th>
	<td>
	78.77.214.172		
	</td>
	<td align="right">212</td>
	<td align="right">0.26</td>
	<td align="right">16,815,945</td>
	<td align="right">0.35</td>
	<td align="right">2</td>
	<td align="right">0.06</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">16</th>
	<td>
	207.241.233.177		
	</td>
	<td align="right">211</td>
	<td align="right">0.26</td>
	<td align="right">14,074,934</td>
	<td align="right">0.30</td>
	<td align="right">8</td>
	<td align="right">0.25</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">17</th>
	<td>
	193.183.253.33		
	</td>
	<td align="right">210</td>
	<td align="right">0.26</td>
	<td align="right">11,036,271</td>
	<td align="right">0.23</td>
	<td align="right">6</td>
	<td align="right">0.19</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">18</th>
	<td>
	217.21.224.205		
	</td>
	<td align="right">191</td>
	<td align="right">0.24</td>
	<td align="right">11,728,878</td>
	<td align="right">0.25</td>
	<td align="right">5</td>
	<td align="right">0.16</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">19</th>
	<td>
	212.247.198.37		
	</td>
	<td align="right">190</td>
	<td align="right">0.23</td>
	<td align="right">25,768,602</td>
	<td align="right">0.54</td>
	<td align="right">6</td>
	<td align="right">0.19</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">20</th>
	<td>
	217.21.224.204		
	</td>
	<td align="right">186</td>
	<td align="right">0.23</td>
	<td align="right">11,344,123</td>
	<td align="right">0.24</td>
	<td align="right">4</td>
	<td align="right">0.12</td>
</tr>

</tbody>
</table>

