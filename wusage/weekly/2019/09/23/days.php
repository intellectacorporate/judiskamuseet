


<h2>Statistics for http://www.u5645470.fsdata.se, Week of 2019-9-23 to 2019-9-29</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Traffic by Day</h3>
<img src="d.png" width="468" height="351">
<br /><br />


<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Date</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	<th scope='col'>Bits per Sec</th>
	<th scope='col'>Bytes per Sec</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th align="right">2019-9-23</th>
	<td align="right">10,323	</td>
	<td align="right">12.72	</td>
	<td align="right">541,128,895	</td>
	<td align="right">11.38	</td>
	<td align="right">412	</td>
	<td align="right">11.99	</td>
	<td align="right">50,104.53	</td>
	<td align="right">6,263.07	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-9-24</th>
	<td align="right">12,364	</td>
	<td align="right">15.23	</td>
	<td align="right">780,063,452	</td>
	<td align="right">16.40	</td>
	<td align="right">470	</td>
	<td align="right">13.68	</td>
	<td align="right">72,228.10	</td>
	<td align="right">9,028.51	</td>
</tr>
<tr class="udda">
	<th align="right">2019-9-25</th>
	<td align="right">12,545	</td>
	<td align="right">15.46	</td>
	<td align="right">793,470,478	</td>
	<td align="right">16.68	</td>
	<td align="right">451	</td>
	<td align="right">13.13	</td>
	<td align="right">73,469.49	</td>
	<td align="right">9,183.69	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-9-26</th>
	<td align="right">11,447	</td>
	<td align="right">14.10	</td>
	<td align="right">690,742,488	</td>
	<td align="right">14.52	</td>
	<td align="right">488	</td>
	<td align="right">14.21	</td>
	<td align="right">63,957.64	</td>
	<td align="right">7,994.70	</td>
</tr>
<tr class="udda">
	<th align="right">2019-9-27</th>
	<td align="right">12,606	</td>
	<td align="right">15.53	</td>
	<td align="right">677,236,418	</td>
	<td align="right">14.24	</td>
	<td align="right">525	</td>
	<td align="right">15.28	</td>
	<td align="right">62,707.08	</td>
	<td align="right">7,838.38	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-9-28</th>
	<td align="right">10,513	</td>
	<td align="right">12.95	</td>
	<td align="right">607,747,911	</td>
	<td align="right">12.78	</td>
	<td align="right">536	</td>
	<td align="right">15.60	</td>
	<td align="right">56,272.95	</td>
	<td align="right">7,034.12	</td>
</tr>
<tr class="udda">
	<th align="right">2019-9-29</th>
	<td align="right">11,361	</td>
	<td align="right">14.00	</td>
	<td align="right">665,991,674	</td>
	<td align="right">14.00	</td>
	<td align="right">553	</td>
	<td align="right">16.10	</td>
	<td align="right">61,665.90	</td>
	<td align="right">7,708.24	</td>
</tr>

</tbody>
</table>

