


<h2>Statistics for http://www.u5645470.fsdata.se, Week of 2019-9-23 to 2019-9-29</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 10 of 12 Operating Systems,
Sorted by Access Count

</h3>
<p></p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Product</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th class="hogerstalld">1</th>
	<td>
	Mac OS X		
	</td>
	<td align="right">64,650</td>
	<td align="right">79.71</td>
	<td align="right">4,006,685,090</td>
	<td align="right">84.26</td>
	<td align="right">2,209</td>
	<td align="right">64.33</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	Other		
	</td>
	<td align="right">9,496</td>
	<td align="right">11.71</td>
	<td align="right">361,924,041</td>
	<td align="right">7.61</td>
	<td align="right">776</td>
	<td align="right">22.59</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	Windows NT		
	</td>
	<td align="right">5,249</td>
	<td align="right">6.47</td>
	<td align="right">348,873,036</td>
	<td align="right">7.34</td>
	<td align="right">176</td>
	<td align="right">5.15</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	Linux		
	</td>
	<td align="right">1,450</td>
	<td align="right">1.79</td>
	<td align="right">25,231,504</td>
	<td align="right">0.53</td>
	<td align="right">265</td>
	<td align="right">7.74</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	Windows XP		
	</td>
	<td align="right">164</td>
	<td align="right">0.20</td>
	<td align="right">6,282,871</td>
	<td align="right">0.13</td>
	<td align="right">4</td>
	<td align="right">0.12</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	Windows Codename Longhorn		
	</td>
	<td align="right">62</td>
	<td align="right">0.08</td>
	<td align="right">5,358,679</td>
	<td align="right">0.11</td>
	<td align="right">2</td>
	<td align="right">0.06</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	Windows 2003		
	</td>
	<td align="right">25</td>
	<td align="right">0.03</td>
	<td align="right">259,023</td>
	<td align="right">0.01</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	Windows 2000		
	</td>
	<td align="right">9</td>
	<td align="right">0.01</td>
	<td align="right">492,711</td>
	<td align="right">0.01</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	Unknown Unix system		
	</td>
	<td align="right">2</td>
	<td align="right">0.00</td>
	<td align="right">116,844</td>
	<td align="right">0.00</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">10</th>
	<td>
	Mac OS		
	</td>
	<td align="right">2</td>
	<td align="right">0.00</td>
	<td align="right">1,905</td>
	<td align="right">0.00</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>

</tbody>
</table>

