

<script>
var componentsData = new Array(
        
        "*end*"
);
var componentsByPage = new Array();
var epochRange = "";
var sortedByAccesses = 0;
var sortedByBytes = 0;
var sortedByVisits = 0;
var alphabetized = 0;
var withBytes = 0;
var withVisits = 0;
var withDownloads = 0;
var subreportKeyword = "";
var subreportTitle = "";
var subreportAll = 0;
var subreportTop = 0;
var subreportTotal = 0;
var subreportPie = "";
var subreportGreenbarFlag = 0;
var pc = 0, ac = 0;
var dataEnd = 0;
var data;
var dataArrays = new Array(
	new Array(
		"Week of 2019-9-23 to 2019-9-29",	
		"*sorted-by-accesses*",
		"*bytes*",
		"*visits*",
		"*subreport*",
	"pages",
	"Documents",
	"*top*",
		30,
	
	"*total*",
	"2,169",
	"*pie*",
	"",
	"*row*",
	"1",
	"/",
	"*local-link*",
		"http://www.u5645470.fsdata.se/",
	
	"5,869",
	"7.23",
	"51,645,458",
		"1.09",
	"929",
		"1.29",
	"*row*",
	"2",
	"/wp-content/plugins/the-events-calendar/common/src/resources<br>\n/css/reset.min.css",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/plugins/the-events-calendar/common/src/resources/css/reset.min.css",
	
	"1,919",
	"2.36",
	"2,071,990",
		"0.04",
	"1,908",
		"2.64",
	"*row*",
	"3",
	"/wp-content/plugins/the-events-calendar/common/src/resources<br>\n/css/common.min.css",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/plugins/the-events-calendar/common/src/resources/css/common.min.css",
	
	"1,900",
	"2.34",
	"10,637,410",
		"0.22",
	"1,911",
		"2.64",
	"*row*",
	"4",
	"/wp-content/plugins/the-events-calendar/common/src/resources<br>\n/css/tooltip.min.css",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/plugins/the-events-calendar/common/src/resources/css/tooltip.min.css",
	
	"1,897",
	"2.34",
	"1,070,163",
		"0.02",
	"1,914",
		"2.65",
	"*row*",
	"5",
	"/wp-content/plugins/the-events-calendar/common/src/resources<br>\n/js/tooltip.min.js",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/plugins/the-events-calendar/common/src/resources/js/tooltip.min.js",
	
	"1,883",
	"2.32",
	"484,395",
		"0.01",
	"1,894",
		"2.62",
	"*row*",
	"6",
	"/wp-content/themes/divi-child/style.css",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/themes/divi-child/style.css",
	
	"1,881",
	"2.32",
	"15,695,310",
		"0.33",
	"1,889",
		"2.61",
	"*row*",
	"7",
	"/wp-content/themes/Divi/core/admin/js/common.js",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/themes/Divi/core/admin/js/common.js",
	
	"1,881",
	"2.32",
	"1,055,786",
		"0.02",
	"1,902",
		"2.63",
	"*row*",
	"8",
	"/wp-content/themes/Divi/style.css",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/themes/Divi/style.css",
	
	"1,873",
	"2.31",
	"138,150,084",
		"2.90",
	"1,884",
		"2.61",
	"*row*",
	"9",
	"/wp-content/plugins/the-events-calendar/common/src/resources<br>\n/js/tribe-common.min.js",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/plugins/the-events-calendar/common/src/resources/js/tribe-common.min.js",
	
	"1,869",
	"2.30",
	"287,322",
		"0.01",
	"1,893",
		"2.62",
	"*row*",
	"10",
	"/wp-content/themes/Divi/js/custom.min.js",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/themes/Divi/js/custom.min.js",
	
	"1,869",
	"2.30",
	"122,127,184",
		"2.57",
	"1,890",
		"2.61",
	"*row*",
	"11",
	"/wp-content/plugins/wp-smushit/app/assets/js/smush-lazy-load<br>\n.min.js",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/plugins/wp-smushit/app/assets/js/smush-lazy-load.min.js",
	
	"1,862",
	"2.29",
	"6,709,846",
		"0.14",
	"1,887",
		"2.61",
	"*row*",
	"12",
	"/wp-includes/css/dashicons.min.css",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-includes/css/dashicons.min.css",
	
	"1,855",
	"2.29",
	"52,424,386",
		"1.10",
	"1,873",
		"2.59",
	"*row*",
	"13",
	"/wp-content/plugins/sitepress-multilingual-cms/templates/lan<br>\nguage-switchers/menu-item/style.css",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/plugins/sitepress-multilingual-cms/templates/language-switchers/menu-item/style.css",
	
	"1,845",
	"2.27",
	"254,197",
		"0.01",
	"1,859",
		"2.57",
	"*row*",
	"14",
	"/wp-includes/css/dist/block-library/style.min.css",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-includes/css/dist/block-library/style.min.css",
	
	"1,833",
	"2.26",
	"8,740,683",
		"0.18",
	"1,846",
		"2.55",
	"*row*",
	"15",
	"/wp-includes/js/jquery/jquery.js",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-includes/js/jquery/jquery.js",
	
	"1,831",
	"2.26",
	"61,416,269",
		"1.29",
	"1,849",
		"2.56",
	"*row*",
	"16",
	"/wp-content/plugins/wp-pagenavi/pagenavi-css.css",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/plugins/wp-pagenavi/pagenavi-css.css",
	
	"1,826",
	"2.25",
	"435,146",
		"0.01",
	"1,846",
		"2.55",
	"*row*",
	"17",
	"/wp-includes/js/jquery/jquery-migrate.min.js",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-includes/js/jquery/jquery-migrate.min.js",
	
	"1,818",
	"2.24",
	"7,317,648",
		"0.15",
	"1,832",
		"2.53",
	"*row*",
	"18",
	"/wp-includes/js/wp-embed.min.js",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-includes/js/wp-embed.min.js",
	
	"1,814",
	"2.24",
	"1,365,633",
		"0.03",
	"1,843",
		"2.55",
	"*row*",
	"19",
	"/wp-content/plugins/mailchimp-for-wp/assets/js/forms-api.min<br>\n.js",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/plugins/mailchimp-for-wp/assets/js/forms-api.min.js",
	
	"1,811",
	"2.23",
	"13,615,835",
		"0.29",
	"1,842",
		"2.55",
	"*row*",
	"20",
	"/wp-content/themes/divi-child/js/divi-child.js",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/themes/divi-child/js/divi-child.js",
	
	"1,808",
	"2.23",
	"1,615,968",
		"0.03",
	"1,831",
		"2.53",
	"*row*",
	"21",
	"/wp-includes/js/wp-emoji-release.min.js",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-includes/js/wp-emoji-release.min.js",
	
	"1,808",
	"2.23",
	"8,333,336",
		"0.18",
	"1,825",
		"2.52",
	"*row*",
	"22",
	"/wp-content/uploads/2017/06/Judiska_centr_svart_web-1.png",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/uploads/2017/06/Judiska_centr_svart_web-1.png",
	
	"1,800",
	"2.22",
	"16,289,720",
		"0.34",
	"1,786",
		"2.47",
	"*row*",
	"23",
	"/wp-content/themes/Divi/core/admin/fonts/modules.ttf",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/themes/Divi/core/admin/fonts/modules.ttf",
	
	"1,775",
	"2.19",
	"160,036,800",
		"3.36",
	"1,787",
		"2.47",
	"*row*",
	"24",
	"/wp-content/uploads/2019/05/cropped-Judiska_svart-2-32x32.pn<br>\ng",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/uploads/2019/05/cropped-Judiska_svart-2-32x32.png",
	
	"1,304",
	"1.61",
	"977,508",
		"0.02",
	"1,261",
		"1.74",
	"*row*",
	"25",
	"/wp-content/themes/divi-child/img/location.png",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/themes/divi-child/img/location.png",
	
	"1,167",
	"1.44",
	"3,539,586",
		"0.07",
	"1,196",
		"1.65",
	"*row*",
	"26",
	"/wp-content/themes/divi-child/img/Instagram.png",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/themes/divi-child/img/Instagram.png",
	
	"1,106",
	"1.36",
	"6,625,643",
		"0.14",
	"1,136",
		"1.57",
	"*row*",
	"27",
	"/wp-content/themes/divi-child/img/facebook.png",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/themes/divi-child/img/facebook.png",
	
	"1,103",
	"1.36",
	"5,275,270",
		"0.11",
	"1,138",
		"1.57",
	"*row*",
	"28",
	"/wp-content/themes/divi-child/img/clock.png",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/themes/divi-child/img/clock.png",
	
	"1,068",
	"1.32",
	"2,410,740",
		"0.05",
	"1,104",
		"1.53",
	"*row*",
	"29",
	"/wp-content/uploads/2019/05/cropped-Judiska_svart-2-192x192.<br>\npng",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/uploads/2019/05/cropped-Judiska_svart-2-192x192.png",
	
	"898",
	"1.11",
	"5,926,512",
		"0.12",
	"875",
		"1.21",
	"*row*",
	"30",
	"/utforska/om-judendomen",
	"*local-link*",
		"http://www.u5645470.fsdata.se/utforska/om-judendomen",
	
	"887",
	"1.09",
	"13,862,712",
		"0.29",
	"526",
		"0.73",
	
		"*end*"
		)
	);

function incPC()
{
	pc++;
	if (pc == data.length) {
		if ((ac == dataArrays.length) || (dataArrays[ac] == "*end*")) {
			dataEnd = 1;
		} else {
			ac++;
			data = dataArrays[ac];
			pc = 0;
		}
	}	
}

function resetPC()
{
	pc = 0;
	ac = 0;
	if (dataArrays.length) {
		data = dataArrays[0];
		dataEnd = 0;
	} else {	
		dataEnd = 1;
	}
}

function compileReports()
{
	resetPC();
	compileDocuments();
}

function compileDocuments()
{
	var s = data[pc];
	epochRange = data[pc];
	incPC();
	while (dataEnd == 0) {
		var s = data[pc];
		if (s == "*sorted-by-accesses*") {
			sortedByAccesses = 1;
			incPC();
		} else if (s == "*sorted-by-bytes*") {
			sortedByBytes = 1;
			incPC();
		} else if (s == "*sorted-by-visits*") {
			sortedByVisits = 1;
			incPC();
		} else if (s == "*alphabetized*") {
			alphabetized = 1;
			incPC();
		} else if (s == "*bytes*") {
			withBytes = 1;
			incPC();
		} else if (s == "*visits*") {
			withVisits = 1;
			incPC();
		} else if (s == "*downloads*") {
			withDownloads = 1;
			incPC();
		} else {
			break;
		}
	}
	documentsHead();
	while (dataEnd == 0) {
		if (compileSubreport() == 0) {
			// No more subreports. That's OK 
			break;
		}	
	}
	documentsTail();
	return 1;
}
function compileSubreport()
{
	if (data[pc] != "*subreport*") {
		return 0;
	}
	incPC();	
	subreportKeyword = "";
	subreportTitle = "";
	subreportAll = 0;
	subreportTop = 0;
	subreportTotal = 0;
	subreportPie = "";
	subreportKeyword = data[pc];
	incPC();
	subreportTitle = data[pc];
	incPC();
	if (data[pc] == "*all*") {
		incPC();
		subreportAll = 1;
	}
	if (data[pc] == "*top*") {
		incPC();
		subreportTop = data[pc];
		incPC();
	}
	if (data[pc] == "*total*") {
		incPC();
		subreportTotal = data[pc];
		incPC();
	}
	if (data[pc] == "*pie*") {
		incPC();
		subreportPie = data[pc];
		incPC();
	}
	subreportHead();
	while (dataEnd == 0) {
		if (compileSubreportRow() == 0) {
			// No more subreport rows. That's OK 
			break;
		}
	}
	subreportTail();
	return 1;
}
var rowRank = 0;
var rowItem = "";
var rowLocalLink = "";
var rowTitle = "";
var rowReferrers = "";
var rowAccesses = 0;
var rowAccessesPer = 0;
var rowBytes = 0;
var rowBytesPer = 0;
var rowVisits = 0;
var rowVisitsPer = 0;
var rowDownloadsPer = 0;
var rowComponents = 0;

function compileSubreportRow()
{
	if (data[pc] != "*row*") {
		return 0;
	}
	rowRank = 0;
	rowItem = "";
	rowLocalLink = "";
	rowTitle = "";
	rowReferrers = "";
	rowComponents = 0;
	incPC();
	rowRank = data[pc];
	incPC();
	rowItem = data[pc];
	incPC();
	if (data[pc] == "*local-link*") {
		incPC();
		rowLocalLink = data[pc];
		incPC();
	}
	if (data[pc] == "*title*") {
		incPC();
		rowTitle = data[pc];
		incPC();
	}
	if (data[pc] == "*referrers*") {
		incPC();
		rowReferrers = data[pc];
		incPC();
	}
	if (data[pc] == "*components*") {
		if (compileComponentList() == 0) {
			return 0;
		}
	}		
	rowAccesses = data[pc];	
	incPC();
	rowAccessesPer = data[pc];	
	incPC();
	if (withBytes) {
		rowBytes = data[pc];
		incPC();
		rowBytesPer = data[pc];
		incPC();
	}
	if (withVisits) {
		rowVisits = data[pc];
		incPC();
		rowVisitsPer = data[pc];
		incPC();
	}
	if (withDownloads) {
		rowDownloadsPer = data[pc];
		incPC();
	}
	subreportRow();	
	return 1;
}

function compileComponentList()
{
	if (data[pc] != "*components*") {
		return 0;
	}
	incPC();
	var alreadyFirst = componentsByPage[rowItem + ".1"];
	while (dataEnd == 0) {
		if (data[pc] == "*end*") {
			incPC();
			return 1;
		}
		rowComponents++;
		var index = data[pc];
		incPC();
		if (!alreadyFirst) {
			addComponent(rowItem, index);
		}
	}
	return -1;
}

function listComponents(w, item)
{
	var i = 1;
	while (1) {
		var s = componentsByPage[item + "." + i];
		if (!s) {
			return;
		}
		var offset = s * 5;
		var name = componentsData[offset++];
		var accesses = componentsData[offset++];
		var bytes = componentsData[offset++];
		var visits = componentsData[offset++];
		var downloadsper = componentsData[offset++];
		w.document.writeln("<td>");
		w.document.writeln(name);
		w.document.writeln("</td>");
		w.document.writeln("<td align=right>");
		w.document.writeln(accesses);
		w.document.writeln("</td>");
		if (withBytes) {
			w.document.writeln("<td align=right>");
			w.document.writeln(bytes);
			w.document.writeln("</td>");
		}	
		if (withVisits) {
			w.document.writeln("<td align=right>");
			w.document.writeln(visits);
			w.document.writeln("</td>");
		}	
		if (withDownloads) {
			w.document.writeln("<td align=right>");
			w.document.writeln(downloadsper);
			w.document.writeln("</td>");
		}	
		w.document.writeln("</tr>");
		i++;
	}
}

function addComponent(item, index)
{
	var i = 1;
	while (1) {
		var s = componentsByPage[item + "." + i];
		if (!s) {
			break;
		}
		i++;
	}
	componentsByPage[item + "." + i] = index;
}

function wl(s)
{
	document.writeln(s);
}
function documentsHead()
{
	wl("<h2>" + epochRange + "</h2>");
	

wl("<h3>Menu</h3>");

wl("<div class='colmenu'>");
wl("<div class='column'>");
wl("<ul id='infolankar'>");
wl("<li><a href=\"../../../../index.php\">Calendar of Reports</a></li>");
wl("<li><a href=\"index.php\">Executive Summary and Totals</a></li>");
wl("<li><a href=\"hours.php\">Accesses by Hour</a></li>");
wl("<li><a href=\"days.php\">Accesses by Day</a></li>");
wl("<li><a href=\"documents.php\">Top Documents</a></li>");
wl("<li><a href=\"directories.php\">Documents by Directory</a></li>");
wl("<li><a href=\"types.php\">File Types (Extensions)</a></li>");

wl("</ul>");
wl("</div>");
wl("<div class='column'>");
wl("<ul id='infolankar'>");
wl("<li><a href=\"sites.php\">Top Visitor Sites</a></li>");
wl("<li><a href=\"useragents.php\">Top Web Browsers</a></li>");
wl("<li><a href=\"os.php\">Top Operating Systems</a></li>");
wl("<li><a href=\"referringsites.php\">Top Referring Sites</a></li>");
wl("<li><a href=\"notfound.php\">Documents Not Found</a></li>");
wl("<li><a href=\"resultcodes.php\">Accesses by Result Code</a></li>");

wl("</ul>");
wl("</div>");
wl("<div class='colmenufoot'></div>");
wl("</div>");

}
function documentsTail()
{
}
function subreportHead()
{
	wl("<h3><a name=\"" + subreportKeyword + "\">");
	if (subreportAll) {
		wl("All " + subreportTitle);
	} else {
		wl("Top " + subreportTop + " of " + subreportTotal + " " + subreportTitle + ", ");
	}
        if (sortedByAccesses) {
                wl("Sorted by Access Count");
        } else if (sortedByBytes) {
                wl("Sorted by Bytes");
        } else if (sortedByVisits) {
                wl("Sorted by Visits");
        }
        if (alphabetized) {
                wl("and Alphabetized");
        }
	wl("</a></h3><p></p>"); 

	if (subreportPie) {
		wl("<div align=\"center\">");
		wl(subreportPie);
		wl("</div>");
	}
	wl("<table cellspacing=\"0\" class=\"styladtabell\">");
	wl("<thead>");
	wl("<tr>");
	wl("<th scope=\"col\">Rank</th>");
	wl("<th scope=\"col\">Item</th>");
	wl("<th scope=\"col\">Accesses</th>");
	wl("<th scope=\"col\">%</th>");
	if (withBytes) {
		wl("<th scope=\"col\">Bytes</th>");
		wl("<th scope=\"col\">%</th>");
	}
	if (withVisits) {
		wl("<th scope=\"col\">Visits</th>");
		wl("<th scope=\"col\">%</th>");
	}
	wl("</tr>");
	wl("</thead>");
	wl("<tbody>");
}	
function subreportTail()
{
	wl("</tbody>");
	wl("</table>");
}
function subreportRow()
{
        var gc = "jamn";
        var dirLink = 0;
        if (subreportGreenbarFlag) {
                gc = "udda";
        }
        subreportGreenbarFlag = !subreportGreenbarFlag;
	wl("<tr class=\"" + gc + "\">");
	wl("<th class=\"hogerstalld\">" + rowRank + "</th>");
	wl("<td align=\"right\">");
	if (rowLocalLink) {
		wl("<a href=\"" + rowLocalLink + "\">");
	}
	if (rowTitle) {
		wl("<b>" + rowTitle + "</b><br>");
	}
	wl(rowItem);
	if (rowLocalLink) {
		wl("</a>");
	}	
	if (rowReferrers) {
		wl("<a href=\"" + rowReferrers + "\" target=\"referrers\">[Referrers]</a>");
	}
	wl("</td>");
	wl("<td align=\"right\">" + rowAccesses + "</td>");
	wl("<td align=\"right\">" + rowAccessesPer + "</td>");
	if (withBytes) {
		wl("<td align=\"right\">" + rowBytes + "</td>");
		wl("<td align=\"right\">" + rowBytesPer + "</td>");
	}
	if (withVisits) {
		wl("<td align=\"right\">" + rowVisits + "</td>");
		wl("<td align=\"right\">" + rowVisitsPer + "</td>");
	}
	if (withDownloads) {
		wl("<td align=\"right\">" + rowDownloadsPer + "</td>");
	}
	wl("</tr>");
}

compileReports(0);	
</script>
