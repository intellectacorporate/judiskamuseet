


<h2>Statistics for http://www.u5645470.fsdata.se, Week of 2019-9-9 to 2019-9-15</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 20 of 3,014 Visitor Sites,
Sorted by Access Count

</h3>
<p></p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Site</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="jamnudda">
	<th class="hogerstalld">1</th>
	<td>
	46.59.25.88		
	</td>
	<td align="right">2,037</td>
	<td align="right">2.39</td>
	<td align="right">103,877,542</td>
	<td align="right">1.97</td>
	<td align="right">45</td>
	<td align="right">1.51</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	5.35.187.69		
	</td>
	<td align="right">1,973</td>
	<td align="right">2.32</td>
	<td align="right">34,165,738</td>
	<td align="right">0.65</td>
	<td align="right">3</td>
	<td align="right">0.11</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	188.151.113.17		
	</td>
	<td align="right">1,929</td>
	<td align="right">2.26</td>
	<td align="right">79,001,291</td>
	<td align="right">1.50</td>
	<td align="right">13</td>
	<td align="right">0.46</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	158.174.9.231		
	</td>
	<td align="right">1,843</td>
	<td align="right">2.16</td>
	<td align="right">44,767,751</td>
	<td align="right">0.85</td>
	<td align="right">8</td>
	<td align="right">0.28</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	63.143.42.242		
	</td>
	<td align="right">1,252</td>
	<td align="right">1.47</td>
	<td align="right">31,529</td>
	<td align="right">0.00</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	213.212.29.165		
	</td>
	<td align="right">1,182</td>
	<td align="right">1.39</td>
	<td align="right">51,756,749</td>
	<td align="right">0.98</td>
	<td align="right">6</td>
	<td align="right">0.21</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	90.229.237.5		
	</td>
	<td align="right">883</td>
	<td align="right">1.04</td>
	<td align="right">60,456,869</td>
	<td align="right">1.15</td>
	<td align="right">8</td>
	<td align="right">0.28</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	89.221.250.27		
	</td>
	<td align="right">535</td>
	<td align="right">0.63</td>
	<td align="right">10,669</td>
	<td align="right">0.00</td>
	<td align="right">7</td>
	<td align="right">0.25</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	79.142.244.252		
	</td>
	<td align="right">363</td>
	<td align="right">0.43</td>
	<td align="right">34,860,132</td>
	<td align="right">0.66</td>
	<td align="right">9</td>
	<td align="right">0.31</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">10</th>
	<td>
	195.22.74.51		
	</td>
	<td align="right">352</td>
	<td align="right">0.41</td>
	<td align="right">10,918,427</td>
	<td align="right">0.21</td>
	<td align="right">7</td>
	<td align="right">0.25</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">11</th>
	<td>
	185.6.9.220		
	</td>
	<td align="right">333</td>
	<td align="right">0.39</td>
	<td align="right">16,987,754</td>
	<td align="right">0.32</td>
	<td align="right">2</td>
	<td align="right">0.07</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">12</th>
	<td>
	212.107.129.217		
	</td>
	<td align="right">245</td>
	<td align="right">0.29</td>
	<td align="right">24,725,599</td>
	<td align="right">0.47</td>
	<td align="right">3</td>
	<td align="right">0.11</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">13</th>
	<td>
	195.196.62.48		
	</td>
	<td align="right">242</td>
	<td align="right">0.28</td>
	<td align="right">29,790,752</td>
	<td align="right">0.57</td>
	<td align="right">3</td>
	<td align="right">0.10</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">14</th>
	<td>
	217.21.226.228		
	</td>
	<td align="right">223</td>
	<td align="right">0.26</td>
	<td align="right">30,062,514</td>
	<td align="right">0.57</td>
	<td align="right">2</td>
	<td align="right">0.07</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">15</th>
	<td>
	212.181.100.64		
	</td>
	<td align="right">220</td>
	<td align="right">0.26</td>
	<td align="right">20,334,576</td>
	<td align="right">0.39</td>
	<td align="right">5</td>
	<td align="right">0.18</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">16</th>
	<td>
	212.181.100.63		
	</td>
	<td align="right">220</td>
	<td align="right">0.26</td>
	<td align="right">24,251,554</td>
	<td align="right">0.46</td>
	<td align="right">3</td>
	<td align="right">0.10</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">17</th>
	<td>
	193.182.18.51		
	</td>
	<td align="right">219</td>
	<td align="right">0.26</td>
	<td align="right">14,244,592</td>
	<td align="right">0.27</td>
	<td align="right">7</td>
	<td align="right">0.24</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">18</th>
	<td>
	94.246.101.230		
	</td>
	<td align="right">213</td>
	<td align="right">0.25</td>
	<td align="right">21,794,075</td>
	<td align="right">0.41</td>
	<td align="right">6</td>
	<td align="right">0.21</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">19</th>
	<td>
	88.131.3.207		
	</td>
	<td align="right">203</td>
	<td align="right">0.24</td>
	<td align="right">26,692,467</td>
	<td align="right">0.51</td>
	<td align="right">6</td>
	<td align="right">0.21</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">20</th>
	<td>
	193.235.91.61		
	</td>
	<td align="right">182</td>
	<td align="right">0.21</td>
	<td align="right">29,415,053</td>
	<td align="right">0.56</td>
	<td align="right">4</td>
	<td align="right">0.14</td>
</tr>

</tbody>
</table>

