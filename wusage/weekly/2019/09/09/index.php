





<h2>Statistics for http://www.u5645470.fsdata.se, Week of 2019-9-9 to 2019-9-15</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>
<h3>Important Totals</h3>
<p></p>
<table cellspacing='0' class='styladtabell'>
<thead><tr>
<th scope='col'>Item</th>
<th scope='col'>Accesses</th>
<th scope='col'>Bytes</th>
<th scope='col'>Visits</th>
<th scope='col'>Charts</th>
</tr>
</thead>
<tbody>
<tr class="udda">
	<td>Overall Accesses</td>
	<td align="right">&nbsp;85,194</td>
	<td align="right">&nbsp;5,271,660,153</td>
	<td align="right">&nbsp;3,022</td>
	<td align="right">&nbsp;<a href="../../../1.php">View Chart</a>
		
	</td>
	</tr>
<tr class="jamn">
	<td>Home Page Accesses</td>
	<td align="right">&nbsp;5,088</td>
	<td align="right">&nbsp;41,017,573</td>
	<td align="right">&nbsp;815</td>
	<td align="right">&nbsp;<a href="../../../3.php">View Chart</a>
		
	</td>
	</tr>
<tr class="udda">
	<td>Unique Visitors (Best Method)</td>
	<td align="right">&nbsp;2,046</td>
	<td align="right">&nbsp;</td>
	<td align="right">&nbsp;</td>
	<td align="right">&nbsp;<a href="../../../5.php">View Chart</a>
		
	</td>
	</tr>

</tbody>
</table>


<h3>Executive Summary</h3>
<p></p>
<p>2,046 unique visitors came to the site,
as determined by typical behavior of browsers with a non-rotating
IP address and including a projection of the true number of
visitors with rotating IP addresses.

</p>
<p>
Visitors came from 3,014 distinct Internet addresses.
</p>
<p>
The web server delivered 2,432 unique documents
one or more times each.
</p>
<p>
23 distinct types of documents were delivered.
</p>
<p>
There were 382 requests for documents which
did not exist on the web server.
</p>
<p>
The web server was linked to by one or more pages found on
189 distinct web sites.
</p>
<p>
Visitors used 205 distinct web browsers and
other web access programs to reach the web server.
</p>
<p>
Visitors used 10 distinct operating systems
on their computers.
</p>
