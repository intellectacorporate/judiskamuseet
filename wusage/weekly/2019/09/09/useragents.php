


<h2>Statistics for http://www.u5645470.fsdata.se, Week of 2019-9-9 to 2019-9-15</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 10 of 205 Browsers (User Agents),
Sorted by Access Count

</h3>
<p></p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Product</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th class="hogerstalld">1</th>
	<td>
	Safari/537.36		
	</td>
	<td align="right">33,223</td>
	<td align="right">39.01</td>
	<td align="right">2,243,613,298</td>
	<td align="right">42.57</td>
	<td align="right">1,023</td>
	<td align="right">33.84</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	Safari/604.1		
	</td>
	<td align="right">16,534</td>
	<td align="right">19.41</td>
	<td align="right">1,138,580,873</td>
	<td align="right">21.60</td>
	<td align="right">488</td>
	<td align="right">16.17</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	Mozilla/5.0 		
	</td>
	<td align="right">9,739</td>
	<td align="right">11.44</td>
	<td align="right">536,326,546</td>
	<td align="right">10.18</td>
	<td align="right">269</td>
	<td align="right">8.91</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	Safari/605.1.15		
	</td>
	<td align="right">7,094</td>
	<td align="right">8.33</td>
	<td align="right">310,993,145</td>
	<td align="right">5.90</td>
	<td align="right">97</td>
	<td align="right">3.21</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	Safari/537.36 Edge/17.17134		
	</td>
	<td align="right">2,219</td>
	<td align="right">2.61</td>
	<td align="right">125,559,815</td>
	<td align="right">2.38</td>
	<td align="right">59</td>
	<td align="right">1.96</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	Safari/605.1		
	</td>
	<td align="right">2,090</td>
	<td align="right">2.45</td>
	<td align="right">198,875,151</td>
	<td align="right">3.77</td>
	<td align="right">60</td>
	<td align="right">1.99</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	bingbot/2.0		
	</td>
	<td align="right">1,873</td>
	<td align="right">2.20</td>
	<td align="right">111,504,378</td>
	<td align="right">2.12</td>
	<td align="right">387</td>
	<td align="right">12.81</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	Safari/537.36 Edge/18.18362		
	</td>
	<td align="right">1,540</td>
	<td align="right">1.81</td>
	<td align="right">129,548,612</td>
	<td align="right">2.46</td>
	<td align="right">35</td>
	<td align="right">1.18</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	Pingdom.com_bot_version_1.4_		
	</td>
	<td align="right">1,254</td>
	<td align="right">1.47</td>
	<td align="right">10,325,865</td>
	<td align="right">0.20</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">10</th>
	<td>
	UptimeRobot/2.0		
	</td>
	<td align="right">1,251</td>
	<td align="right">1.47</td>
	<td align="right">31,528</td>
	<td align="right">0.00</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>

</tbody>
</table>

