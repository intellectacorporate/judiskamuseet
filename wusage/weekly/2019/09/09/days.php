


<h2>Statistics for http://www.u5645470.fsdata.se, Week of 2019-9-9 to 2019-9-15</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Traffic by Day</h3>
<img src="d.png" width="468" height="351">
<br /><br />


<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Date</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	<th scope='col'>Bits per Sec</th>
	<th scope='col'>Bytes per Sec</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th align="right">2019-9-9</th>
	<td align="right">11,090	</td>
	<td align="right">13.02	</td>
	<td align="right">580,690,801	</td>
	<td align="right">11.02	</td>
	<td align="right">322	</td>
	<td align="right">10.65	</td>
	<td align="right">53,767.67	</td>
	<td align="right">6,720.96	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-9-10</th>
	<td align="right">13,644	</td>
	<td align="right">16.02	</td>
	<td align="right">859,471,802	</td>
	<td align="right">16.30	</td>
	<td align="right">514	</td>
	<td align="right">17.00	</td>
	<td align="right">79,580.72	</td>
	<td align="right">9,947.59	</td>
</tr>
<tr class="udda">
	<th align="right">2019-9-11</th>
	<td align="right">11,635	</td>
	<td align="right">13.66	</td>
	<td align="right">750,098,756	</td>
	<td align="right">14.23	</td>
	<td align="right">409	</td>
	<td align="right">13.53	</td>
	<td align="right">69,453.59	</td>
	<td align="right">8,681.70	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-9-12</th>
	<td align="right">12,169	</td>
	<td align="right">14.28	</td>
	<td align="right">765,508,504	</td>
	<td align="right">14.52	</td>
	<td align="right">461	</td>
	<td align="right">15.25	</td>
	<td align="right">70,880.42	</td>
	<td align="right">8,860.05	</td>
</tr>
<tr class="udda">
	<th align="right">2019-9-13</th>
	<td align="right">18,796	</td>
	<td align="right">22.06	</td>
	<td align="right">1,001,699,706	</td>
	<td align="right">19.00	</td>
	<td align="right">614	</td>
	<td align="right">20.31	</td>
	<td align="right">92,749.97	</td>
	<td align="right">11,593.75	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-9-14</th>
	<td align="right">8,790	</td>
	<td align="right">10.32	</td>
	<td align="right">637,686,871	</td>
	<td align="right">12.10	</td>
	<td align="right">348	</td>
	<td align="right">11.51	</td>
	<td align="right">59,045.08	</td>
	<td align="right">7,380.64	</td>
</tr>
<tr class="udda">
	<th align="right">2019-9-15</th>
	<td align="right">9,070	</td>
	<td align="right">10.65	</td>
	<td align="right">676,503,713	</td>
	<td align="right">12.83	</td>
	<td align="right">355	</td>
	<td align="right">11.74	</td>
	<td align="right">62,639.23	</td>
	<td align="right">7,829.90	</td>
</tr>

</tbody>
</table>

