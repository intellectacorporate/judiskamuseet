


<h2>Statistics for http://www.u5645470.fsdata.se, Week of 2019-9-9 to 2019-9-15</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Traffic by Hour</h3>
<p>Figures are averages for that hour
	of each day in the time period analyzed.</p>
<img src="h.png" width="468" height="351">
<br /><br />
<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Hour</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	<th scope='col'>Bits per Sec</th>
	<th scope='col'>Bytes per Sec</th>
	
	</tr>
</thead>
<tbody>
<tr class="jamnudda">
	<th align="right">00:00</th>
	<td align="right">157.29</td>
	<td align="right">1.29</td>
	<td align="right">12,222,253.00</td>
	<td align="right">1.62</td>
	<td align="right">5.30</td>
	<td align="right">1.23</td>
	<td align="right">27,160.56</td>
	<td align="right">3,395.07</td>
	
</tr>
<tr class="jamn">
	<th align="right">01:00</th>
	<td align="right">134.00</td>
	<td align="right">1.10</td>
	<td align="right">5,201,040.14</td>
	<td align="right">0.69</td>
	<td align="right">8.65</td>
	<td align="right">2.00</td>
	<td align="right">11,557.87</td>
	<td align="right">1,444.73</td>
	
</tr>
<tr class="udda">
	<th align="right">02:00</th>
	<td align="right">112.57</td>
	<td align="right">0.92</td>
	<td align="right">7,497,475.71</td>
	<td align="right">1.00</td>
	<td align="right">7.27</td>
	<td align="right">1.68</td>
	<td align="right">16,661.06</td>
	<td align="right">2,082.63</td>
	
</tr>
<tr class="jamn">
	<th align="right">03:00</th>
	<td align="right">107.71</td>
	<td align="right">0.89</td>
	<td align="right">5,818,082.43</td>
	<td align="right">0.77</td>
	<td align="right">5.17</td>
	<td align="right">1.20</td>
	<td align="right">12,929.07</td>
	<td align="right">1,616.13</td>
	
</tr>
<tr class="udda">
	<th align="right">04:00</th>
	<td align="right">78.86</td>
	<td align="right">0.65</td>
	<td align="right">1,072,691.86</td>
	<td align="right">0.14</td>
	<td align="right">7.28</td>
	<td align="right">1.69</td>
	<td align="right">2,383.76</td>
	<td align="right">297.97</td>
	
</tr>
<tr class="jamn">
	<th align="right">05:00</th>
	<td align="right">94.57</td>
	<td align="right">0.78</td>
	<td align="right">4,157,744.43</td>
	<td align="right">0.55</td>
	<td align="right">5.47</td>
	<td align="right">1.27</td>
	<td align="right">9,239.43</td>
	<td align="right">1,154.93</td>
	
</tr>
<tr class="udda">
	<th align="right">06:00</th>
	<td align="right">311.71</td>
	<td align="right">2.56</td>
	<td align="right">17,688,783.86</td>
	<td align="right">2.35</td>
	<td align="right">6.97</td>
	<td align="right">1.62</td>
	<td align="right">39,308.41</td>
	<td align="right">4,913.55</td>
	
</tr>
<tr class="jamn">
	<th align="right">07:00</th>
	<td align="right">275.71</td>
	<td align="right">2.27</td>
	<td align="right">18,292,818.14</td>
	<td align="right">2.43</td>
	<td align="right">11.97</td>
	<td align="right">2.77</td>
	<td align="right">40,650.71</td>
	<td align="right">5,081.34</td>
	
</tr>
<tr class="udda">
	<th align="right">08:00</th>
	<td align="right">528.00</td>
	<td align="right">4.34</td>
	<td align="right">28,611,089.71</td>
	<td align="right">3.80</td>
	<td align="right">18.21</td>
	<td align="right">4.22</td>
	<td align="right">63,580.20</td>
	<td align="right">7,947.52</td>
	
</tr>
<tr class="jamn">
	<th align="right">09:00</th>
	<td align="right">824.86</td>
	<td align="right">6.78</td>
	<td align="right">44,469,045.43</td>
	<td align="right">5.90</td>
	<td align="right">23.94</td>
	<td align="right">5.54</td>
	<td align="right">98,820.10</td>
	<td align="right">12,352.51</td>
	
</tr>
<tr class="udda">
	<th align="right">10:00</th>
	<td align="right">964.29</td>
	<td align="right">7.92</td>
	<td align="right">65,627,389.86</td>
	<td align="right">8.71</td>
	<td align="right">25.64</td>
	<td align="right">5.94</td>
	<td align="right">145,838.64</td>
	<td align="right">18,229.83</td>
	
</tr>
<tr class="jamn">
	<th align="right">11:00</th>
	<td align="right">1,015.71</td>
	<td align="right">8.35</td>
	<td align="right">60,692,313.00</td>
	<td align="right">8.06</td>
	<td align="right">34.16</td>
	<td align="right">7.91</td>
	<td align="right">134,871.81</td>
	<td align="right">16,858.98</td>
	
</tr>
<tr class="udda">
	<th align="right">12:00</th>
	<td align="right">1,075.86</td>
	<td align="right">8.84</td>
	<td align="right">65,757,326.57</td>
	<td align="right">8.73</td>
	<td align="right">33.16</td>
	<td align="right">7.68</td>
	<td align="right">146,127.39</td>
	<td align="right">18,265.92</td>
	
</tr>
<tr class="jamn">
	<th align="right">13:00</th>
	<td align="right">898.43</td>
	<td align="right">7.38</td>
	<td align="right">52,434,570.86</td>
	<td align="right">6.96</td>
	<td align="right">33.02</td>
	<td align="right">7.65</td>
	<td align="right">116,521.27</td>
	<td align="right">14,565.16</td>
	
</tr>
<tr class="udda">
	<th align="right">14:00</th>
	<td align="right">779.29</td>
	<td align="right">6.40</td>
	<td align="right">44,881,991.00</td>
	<td align="right">5.96</td>
	<td align="right">27.39</td>
	<td align="right">6.34</td>
	<td align="right">99,737.76</td>
	<td align="right">12,467.22</td>
	
</tr>
<tr class="jamn">
	<th align="right">15:00</th>
	<td align="right">842.43</td>
	<td align="right">6.92</td>
	<td align="right">52,188,893.14</td>
	<td align="right">6.93</td>
	<td align="right">27.62</td>
	<td align="right">6.40</td>
	<td align="right">115,975.32</td>
	<td align="right">14,496.91</td>
	
</tr>
<tr class="udda">
	<th align="right">16:00</th>
	<td align="right">649.00</td>
	<td align="right">5.33</td>
	<td align="right">39,025,528.00</td>
	<td align="right">5.18</td>
	<td align="right">22.53</td>
	<td align="right">5.22</td>
	<td align="right">86,723.40</td>
	<td align="right">10,840.42</td>
	
</tr>
<tr class="jamn">
	<th align="right">17:00</th>
	<td align="right">568.86</td>
	<td align="right">4.67</td>
	<td align="right">38,691,972.29</td>
	<td align="right">5.14</td>
	<td align="right">19.99</td>
	<td align="right">4.63</td>
	<td align="right">85,982.16</td>
	<td align="right">10,747.77</td>
	
</tr>
<tr class="udda">
	<th align="right">18:00</th>
	<td align="right">442.71</td>
	<td align="right">3.64</td>
	<td align="right">26,928,086.86</td>
	<td align="right">3.58</td>
	<td align="right">19.08</td>
	<td align="right">4.42</td>
	<td align="right">59,840.19</td>
	<td align="right">7,480.02</td>
	
</tr>
<tr class="jamn">
	<th align="right">19:00</th>
	<td align="right">476.29</td>
	<td align="right">3.91</td>
	<td align="right">40,532,342.57</td>
	<td align="right">5.38</td>
	<td align="right">18.32</td>
	<td align="right">4.24</td>
	<td align="right">90,071.87</td>
	<td align="right">11,258.98</td>
	
</tr>
<tr class="udda">
	<th align="right">20:00</th>
	<td align="right">540.57</td>
	<td align="right">4.44</td>
	<td align="right">28,135,202.14</td>
	<td align="right">3.74</td>
	<td align="right">15.75</td>
	<td align="right">3.65</td>
	<td align="right">62,522.67</td>
	<td align="right">7,815.33</td>
	
</tr>
<tr class="jamn">
	<th align="right">21:00</th>
	<td align="right">651.86</td>
	<td align="right">5.36</td>
	<td align="right">44,448,876.43</td>
	<td align="right">5.90</td>
	<td align="right">19.43</td>
	<td align="right">4.50</td>
	<td align="right">98,775.28</td>
	<td align="right">12,346.91</td>
	
</tr>
<tr class="udda">
	<th align="right">22:00</th>
	<td align="right">403.14</td>
	<td align="right">3.31</td>
	<td align="right">32,607,653.43</td>
	<td align="right">4.33</td>
	<td align="right">18.75</td>
	<td align="right">4.34</td>
	<td align="right">72,461.45</td>
	<td align="right">9,057.68</td>
	
</tr>
<tr class="jamn">
	<th align="right">23:00</th>
	<td align="right">236.86</td>
	<td align="right">1.95</td>
	<td align="right">16,111,136.71</td>
	<td align="right">2.14</td>
	<td align="right">16.79</td>
	<td align="right">3.89</td>
	<td align="right">35,802.53</td>
	<td align="right">4,475.32</td>
	
</tr>

</tbody>
</table>

