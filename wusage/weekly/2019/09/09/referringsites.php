


<h2>Statistics for http://www.u5645470.fsdata.se, Week of 2019-9-9 to 2019-9-15</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 20 of 189 Referring Sites,
Sorted by Access Count

</h3>
<p></p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Referring Site</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th class="hogerstalld">1</th>
	<td>
	<a href="https://judiskamuseet.se">
	https://judiskamuseet.se</a>
	</td>
	<td align="right">68,341</td>
	<td align="right">92.55</td>
	<td align="right">4,705,564,824</td>
	<td align="right">97.05</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	<a href="http://judiskamuseet.se">
	http://judiskamuseet.se</a>
	</td>
	<td align="right">2,816</td>
	<td align="right">3.81</td>
	<td align="right">59,436,383</td>
	<td align="right">1.23</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	<a href="https://www.google.com">
	https://www.google.com</a>
	</td>
	<td align="right">1,300</td>
	<td align="right">1.76</td>
	<td align="right">45,139,817</td>
	<td align="right">0.93</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	<a href="https://www.google.se">
	https://www.google.se</a>
	</td>
	<td align="right">568</td>
	<td align="right">0.77</td>
	<td align="right">12,733,259</td>
	<td align="right">0.26</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	<a href="https://www.elegantthemes.com">
	https://www.elegantthemes.com</a>
	</td>
	<td align="right">95</td>
	<td align="right">0.13</td>
	<td align="right">5,082,026</td>
	<td align="right">0.10</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	<a href="https://www.google.de">
	https://www.google.de</a>
	</td>
	<td align="right">95</td>
	<td align="right">0.13</td>
	<td align="right">2,276,414</td>
	<td align="right">0.05</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	<a href="http://m.facebook.com">
	http://m.facebook.com</a>
	</td>
	<td align="right">88</td>
	<td align="right">0.12</td>
	<td align="right">987,070</td>
	<td align="right">0.02</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	<a href="https://www.bing.com">
	https://www.bing.com</a>
	</td>
	<td align="right">49</td>
	<td align="right">0.07</td>
	<td align="right">2,113,183</td>
	<td align="right">0.04</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	<a href="http://www.google.com">
	http://www.google.com</a>
	</td>
	<td align="right">43</td>
	<td align="right">0.06</td>
	<td align="right">377,392</td>
	<td align="right">0.01</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">10</th>
	<td>
	<a href="http://www.judiska-museet.se">
	http://www.judiska-museet.se</a>
	</td>
	<td align="right">20</td>
	<td align="right">0.03</td>
	<td align="right">14,524</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">11</th>
	<td>
	<a href="https://www.facebook.com">
	https://www.facebook.com</a>
	</td>
	<td align="right">19</td>
	<td align="right">0.03</td>
	<td align="right">207,462</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">12</th>
	<td>
	<a href="https://myactivity.google.com">
	https://myactivity.google.com</a>
	</td>
	<td align="right">16</td>
	<td align="right">0.02</td>
	<td align="right">2,744,497</td>
	<td align="right">0.06</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">13</th>
	<td>
	<a href="https://duckduckgo.com">
	https://duckduckgo.com</a>
	</td>
	<td align="right">13</td>
	<td align="right">0.02</td>
	<td align="right">196,439</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">14</th>
	<td>
	<a href="https://sv.wikipedia.org">
	https://sv.wikipedia.org</a>
	</td>
	<td align="right">9</td>
	<td align="right">0.01</td>
	<td align="right">64,194</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">15</th>
	<td>
	<a href="https://kanslinatet.lararforbundet.se">
	https://kanslinatet.lararforbundet.se</a>
	</td>
	<td align="right">7</td>
	<td align="right">0.01</td>
	<td align="right">92,181</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">16</th>
	<td>
	<a href="https://www.google.be">
	https://www.google.be</a>
	</td>
	<td align="right">7</td>
	<td align="right">0.01</td>
	<td align="right">38,610</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">17</th>
	<td>
	<a href="https://l.facebook.com">
	https://l.facebook.com</a>
	</td>
	<td align="right">7</td>
	<td align="right">0.01</td>
	<td align="right">69,643</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">18</th>
	<td>
	<a href="http://www.stockholmmuseums.se">
	http://www.stockholmmuseums.se</a>
	</td>
	<td align="right">7</td>
	<td align="right">0.01</td>
	<td align="right">50,255</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">19</th>
	<td>
	<a href="https://7ooo.ru">
	https://7ooo.ru</a>
	</td>
	<td align="right">7</td>
	<td align="right">0.01</td>
	<td align="right">321,676</td>
	<td align="right">0.01</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">20</th>
	<td>
	<a href="https://www.google.fi">
	https://www.google.fi</a>
	</td>
	<td align="right">6</td>
	<td align="right">0.01</td>
	<td align="right">91,682</td>
	<td align="right">0.00</td>
</tr>

</tbody>
</table>

