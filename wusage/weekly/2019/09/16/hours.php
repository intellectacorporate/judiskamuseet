


<h2>Statistics for http://www.u5645470.fsdata.se, Week of 2019-9-16 to 2019-9-22</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Traffic by Hour</h3>
<p>Figures are averages for that hour
	of each day in the time period analyzed.</p>
<img src="h.png" width="468" height="351">
<br /><br />
<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Hour</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	<th scope='col'>Bits per Sec</th>
	<th scope='col'>Bytes per Sec</th>
	
	</tr>
</thead>
<tbody>
<tr class="jamnudda">
	<th align="right">00:00</th>
	<td align="right">196.00</td>
	<td align="right">1.85</td>
	<td align="right">10,879,379.43</td>
	<td align="right">1.69</td>
	<td align="right">7.21</td>
	<td align="right">1.61</td>
	<td align="right">24,176.40</td>
	<td align="right">3,022.05</td>
	
</tr>
<tr class="jamn">
	<th align="right">01:00</th>
	<td align="right">102.00</td>
	<td align="right">0.96</td>
	<td align="right">4,743,860.29</td>
	<td align="right">0.74</td>
	<td align="right">7.48</td>
	<td align="right">1.67</td>
	<td align="right">10,541.91</td>
	<td align="right">1,317.74</td>
	
</tr>
<tr class="udda">
	<th align="right">02:00</th>
	<td align="right">95.86</td>
	<td align="right">0.91</td>
	<td align="right">2,652,625.00</td>
	<td align="right">0.41</td>
	<td align="right">9.18</td>
	<td align="right">2.05</td>
	<td align="right">5,894.72</td>
	<td align="right">736.84</td>
	
</tr>
<tr class="jamn">
	<th align="right">03:00</th>
	<td align="right">71.29</td>
	<td align="right">0.67</td>
	<td align="right">1,602,767.43</td>
	<td align="right">0.25</td>
	<td align="right">9.19</td>
	<td align="right">2.05</td>
	<td align="right">3,561.71</td>
	<td align="right">445.21</td>
	
</tr>
<tr class="udda">
	<th align="right">04:00</th>
	<td align="right">99.14</td>
	<td align="right">0.94</td>
	<td align="right">5,341,303.71</td>
	<td align="right">0.83</td>
	<td align="right">6.73</td>
	<td align="right">1.50</td>
	<td align="right">11,869.56</td>
	<td align="right">1,483.70</td>
	
</tr>
<tr class="jamn">
	<th align="right">05:00</th>
	<td align="right">106.00</td>
	<td align="right">1.00</td>
	<td align="right">3,109,442.14</td>
	<td align="right">0.48</td>
	<td align="right">7.03</td>
	<td align="right">1.57</td>
	<td align="right">6,909.87</td>
	<td align="right">863.73</td>
	
</tr>
<tr class="udda">
	<th align="right">06:00</th>
	<td align="right">138.86</td>
	<td align="right">1.31</td>
	<td align="right">5,918,190.86</td>
	<td align="right">0.92</td>
	<td align="right">6.86</td>
	<td align="right">1.53</td>
	<td align="right">13,151.54</td>
	<td align="right">1,643.94</td>
	
</tr>
<tr class="jamn">
	<th align="right">07:00</th>
	<td align="right">281.14</td>
	<td align="right">2.66</td>
	<td align="right">16,280,190.86</td>
	<td align="right">2.53</td>
	<td align="right">12.97</td>
	<td align="right">2.89</td>
	<td align="right">36,178.20</td>
	<td align="right">4,522.28</td>
	
</tr>
<tr class="udda">
	<th align="right">08:00</th>
	<td align="right">497.86</td>
	<td align="right">4.71</td>
	<td align="right">22,599,727.00</td>
	<td align="right">3.52</td>
	<td align="right">21.90</td>
	<td align="right">4.89</td>
	<td align="right">50,221.62</td>
	<td align="right">6,277.70</td>
	
</tr>
<tr class="jamn">
	<th align="right">09:00</th>
	<td align="right">802.14</td>
	<td align="right">7.59</td>
	<td align="right">51,347,527.43</td>
	<td align="right">7.99</td>
	<td align="right">25.86</td>
	<td align="right">5.77</td>
	<td align="right">114,105.62</td>
	<td align="right">14,263.20</td>
	
</tr>
<tr class="udda">
	<th align="right">10:00</th>
	<td align="right">827.00</td>
	<td align="right">7.82</td>
	<td align="right">54,281,671.00</td>
	<td align="right">8.45</td>
	<td align="right">30.21</td>
	<td align="right">6.74</td>
	<td align="right">120,625.94</td>
	<td align="right">15,078.24</td>
	
</tr>
<tr class="jamn">
	<th align="right">11:00</th>
	<td align="right">808.57</td>
	<td align="right">7.65</td>
	<td align="right">60,767,818.00</td>
	<td align="right">9.46</td>
	<td align="right">31.51</td>
	<td align="right">7.03</td>
	<td align="right">135,039.60</td>
	<td align="right">16,879.95</td>
	
</tr>
<tr class="udda">
	<th align="right">12:00</th>
	<td align="right">664.86</td>
	<td align="right">6.29</td>
	<td align="right">36,648,402.29</td>
	<td align="right">5.71</td>
	<td align="right">23.14</td>
	<td align="right">5.16</td>
	<td align="right">81,440.89</td>
	<td align="right">10,180.11</td>
	
</tr>
<tr class="jamn">
	<th align="right">13:00</th>
	<td align="right">710.43</td>
	<td align="right">6.72</td>
	<td align="right">45,674,080.57</td>
	<td align="right">7.11</td>
	<td align="right">27.12</td>
	<td align="right">6.05</td>
	<td align="right">101,497.96</td>
	<td align="right">12,687.24</td>
	
</tr>
<tr class="udda">
	<th align="right">14:00</th>
	<td align="right">772.14</td>
	<td align="right">7.30</td>
	<td align="right">42,051,140.57</td>
	<td align="right">6.55</td>
	<td align="right">28.01</td>
	<td align="right">6.25</td>
	<td align="right">93,446.98</td>
	<td align="right">11,680.87</td>
	
</tr>
<tr class="jamn">
	<th align="right">15:00</th>
	<td align="right">622.14</td>
	<td align="right">5.88</td>
	<td align="right">38,938,226.71</td>
	<td align="right">6.06</td>
	<td align="right">26.83</td>
	<td align="right">5.99</td>
	<td align="right">86,529.39</td>
	<td align="right">10,816.17</td>
	
</tr>
<tr class="udda">
	<th align="right">16:00</th>
	<td align="right">617.43</td>
	<td align="right">5.84</td>
	<td align="right">40,593,986.00</td>
	<td align="right">6.32</td>
	<td align="right">24.27</td>
	<td align="right">5.41</td>
	<td align="right">90,208.86</td>
	<td align="right">11,276.11</td>
	
</tr>
<tr class="jamn">
	<th align="right">17:00</th>
	<td align="right">546.14</td>
	<td align="right">5.17</td>
	<td align="right">38,497,782.29</td>
	<td align="right">5.99</td>
	<td align="right">21.33</td>
	<td align="right">4.76</td>
	<td align="right">85,550.63</td>
	<td align="right">10,693.83</td>
	
</tr>
<tr class="udda">
	<th align="right">18:00</th>
	<td align="right">438.29</td>
	<td align="right">4.15</td>
	<td align="right">21,396,456.14</td>
	<td align="right">3.33</td>
	<td align="right">19.05</td>
	<td align="right">4.25</td>
	<td align="right">47,547.68</td>
	<td align="right">5,943.46</td>
	
</tr>
<tr class="jamn">
	<th align="right">19:00</th>
	<td align="right">440.14</td>
	<td align="right">4.16</td>
	<td align="right">28,148,531.00</td>
	<td align="right">4.38</td>
	<td align="right">19.18</td>
	<td align="right">4.28</td>
	<td align="right">62,552.29</td>
	<td align="right">7,819.04</td>
	
</tr>
<tr class="udda">
	<th align="right">20:00</th>
	<td align="right">493.86</td>
	<td align="right">4.67</td>
	<td align="right">33,057,781.43</td>
	<td align="right">5.15</td>
	<td align="right">20.14</td>
	<td align="right">4.49</td>
	<td align="right">73,461.74</td>
	<td align="right">9,182.72</td>
	
</tr>
<tr class="jamn">
	<th align="right">21:00</th>
	<td align="right">432.14</td>
	<td align="right">4.09</td>
	<td align="right">28,887,195.14</td>
	<td align="right">4.50</td>
	<td align="right">19.20</td>
	<td align="right">4.28</td>
	<td align="right">64,193.77</td>
	<td align="right">8,024.22</td>
	
</tr>
<tr class="udda">
	<th align="right">22:00</th>
	<td align="right">516.43</td>
	<td align="right">4.88</td>
	<td align="right">32,926,033.43</td>
	<td align="right">5.13</td>
	<td align="right">19.96</td>
	<td align="right">4.45</td>
	<td align="right">73,168.96</td>
	<td align="right">9,146.12</td>
	
</tr>
<tr class="jamn">
	<th align="right">23:00</th>
	<td align="right">293.86</td>
	<td align="right">2.78</td>
	<td align="right">15,905,837.14</td>
	<td align="right">2.48</td>
	<td align="right">23.79</td>
	<td align="right">5.31</td>
	<td align="right">35,346.30</td>
	<td align="right">4,418.29</td>
	
</tr>

</tbody>
</table>

