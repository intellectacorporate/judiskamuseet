


<h2>Statistics for http://www.u5645470.fsdata.se, Week of 2019-9-16 to 2019-9-22</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 10 of 196 Browsers (User Agents),
Sorted by Access Count

</h3>
<p></p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Product</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th class="hogerstalld">1</th>
	<td>
	Safari/537.36		
	</td>
	<td align="right">26,512</td>
	<td align="right">35.86</td>
	<td align="right">1,734,349,432</td>
	<td align="right">38.62</td>
	<td align="right">863</td>
	<td align="right">27.58</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	Safari/604.1		
	</td>
	<td align="right">17,941</td>
	<td align="right">24.27</td>
	<td align="right">1,319,393,658</td>
	<td align="right">29.38</td>
	<td align="right">578</td>
	<td align="right">18.48</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	Mozilla/5.0 		
	</td>
	<td align="right">8,249</td>
	<td align="right">11.16</td>
	<td align="right">388,463,572</td>
	<td align="right">8.65</td>
	<td align="right">501</td>
	<td align="right">16.02</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	Safari/605.1.15		
	</td>
	<td align="right">4,935</td>
	<td align="right">6.67</td>
	<td align="right">278,257,773</td>
	<td align="right">6.20</td>
	<td align="right">110</td>
	<td align="right">3.54</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	bingbot/2.0		
	</td>
	<td align="right">1,705</td>
	<td align="right">2.31</td>
	<td align="right">120,711,672</td>
	<td align="right">2.69</td>
	<td align="right">416</td>
	<td align="right">13.30</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	Safari/537.36 Edge/17.17134		
	</td>
	<td align="right">1,548</td>
	<td align="right">2.09</td>
	<td align="right">81,518,207</td>
	<td align="right">1.82</td>
	<td align="right">28</td>
	<td align="right">0.92</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	Safari/605.1		
	</td>
	<td align="right">1,346</td>
	<td align="right">1.82</td>
	<td align="right">118,359,239</td>
	<td align="right">2.64</td>
	<td align="right">41</td>
	<td align="right">1.32</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	UptimeRobot/2.0		
	</td>
	<td align="right">1,292</td>
	<td align="right">1.75</td>
	<td align="right">330,027</td>
	<td align="right">0.01</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	Safari/537.36 Edge/18.18362		
	</td>
	<td align="right">1,272</td>
	<td align="right">1.72</td>
	<td align="right">92,154,506</td>
	<td align="right">2.05</td>
	<td align="right">37</td>
	<td align="right">1.19</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">10</th>
	<td>
	Pingdom.com_bot_version_1.4_		
	</td>
	<td align="right">1,253</td>
	<td align="right">1.69</td>
	<td align="right">10,147,839</td>
	<td align="right">0.23</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>

</tbody>
</table>

