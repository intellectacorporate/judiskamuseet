


<h2>Statistics for http://www.u5645470.fsdata.se, Week of 2019-9-16 to 2019-9-22</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Traffic by Day</h3>
<img src="d.png" width="468" height="351">
<br /><br />


<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Date</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	<th scope='col'>Bits per Sec</th>
	<th scope='col'>Bytes per Sec</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th align="right">2019-9-16</th>
	<td align="right">10,584	</td>
	<td align="right">14.30	</td>
	<td align="right">642,131,328	</td>
	<td align="right">14.28	</td>
	<td align="right">457	</td>
	<td align="right">14.57	</td>
	<td align="right">59,456.60	</td>
	<td align="right">7,432.08	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-9-17</th>
	<td align="right">12,059	</td>
	<td align="right">16.29	</td>
	<td align="right">793,722,908	</td>
	<td align="right">17.65	</td>
	<td align="right">464	</td>
	<td align="right">14.79	</td>
	<td align="right">73,492.86	</td>
	<td align="right">9,186.61	</td>
</tr>
<tr class="udda">
	<th align="right">2019-9-18</th>
	<td align="right">12,579	</td>
	<td align="right">16.99	</td>
	<td align="right">747,264,586	</td>
	<td align="right">16.62	</td>
	<td align="right">454	</td>
	<td align="right">14.47	</td>
	<td align="right">69,191.17	</td>
	<td align="right">8,648.90	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-9-19</th>
	<td align="right">14,180	</td>
	<td align="right">19.16	</td>
	<td align="right">758,683,512	</td>
	<td align="right">16.88	</td>
	<td align="right">552	</td>
	<td align="right">17.60	</td>
	<td align="right">70,248.47	</td>
	<td align="right">8,781.06	</td>
</tr>
<tr class="udda">
	<th align="right">2019-9-20</th>
	<td align="right">9,599	</td>
	<td align="right">12.97	</td>
	<td align="right">569,625,817	</td>
	<td align="right">12.67	</td>
	<td align="right">530	</td>
	<td align="right">16.90	</td>
	<td align="right">52,743.13	</td>
	<td align="right">6,592.89	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-9-21</th>
	<td align="right">7,035	</td>
	<td align="right">9.50	</td>
	<td align="right">404,108,728	</td>
	<td align="right">8.99	</td>
	<td align="right">332	</td>
	<td align="right">10.58	</td>
	<td align="right">37,417.47	</td>
	<td align="right">4,677.18	</td>
</tr>
<tr class="udda">
	<th align="right">2019-9-22</th>
	<td align="right">7,980	</td>
	<td align="right">10.78	</td>
	<td align="right">580,212,812	</td>
	<td align="right">12.91	</td>
	<td align="right">348	</td>
	<td align="right">11.09	</td>
	<td align="right">53,723.41	</td>
	<td align="right">6,715.43	</td>
</tr>

</tbody>
</table>

