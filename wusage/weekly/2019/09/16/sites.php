


<h2>Statistics for http://www.u5645470.fsdata.se, Week of 2019-9-16 to 2019-9-22</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 20 of 3,307 Visitor Sites,
Sorted by Access Count

</h3>
<p></p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Site</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="jamnudda">
	<th class="hogerstalld">1</th>
	<td>
	188.151.113.17		
	</td>
	<td align="right">1,614</td>
	<td align="right">2.18</td>
	<td align="right">46,522,649</td>
	<td align="right">1.03</td>
	<td align="right">10</td>
	<td align="right">0.34</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	46.59.25.88		
	</td>
	<td align="right">1,467</td>
	<td align="right">1.98</td>
	<td align="right">62,779,869</td>
	<td align="right">1.40</td>
	<td align="right">37</td>
	<td align="right">1.18</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	63.143.42.242		
	</td>
	<td align="right">1,293</td>
	<td align="right">1.75</td>
	<td align="right">330,028</td>
	<td align="right">0.01</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	213.212.29.165		
	</td>
	<td align="right">539</td>
	<td align="right">0.73</td>
	<td align="right">19,545,595</td>
	<td align="right">0.43</td>
	<td align="right">3</td>
	<td align="right">0.10</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	89.221.250.27		
	</td>
	<td align="right">448</td>
	<td align="right">0.61</td>
	<td align="right">8,609</td>
	<td align="right">0.00</td>
	<td align="right">8</td>
	<td align="right">0.27</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	185.6.9.220		
	</td>
	<td align="right">402</td>
	<td align="right">0.54</td>
	<td align="right">22,302,988</td>
	<td align="right">0.50</td>
	<td align="right">1</td>
	<td align="right">0.03</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	195.198.213.158		
	</td>
	<td align="right">366</td>
	<td align="right">0.49</td>
	<td align="right">14,819,652</td>
	<td align="right">0.33</td>
	<td align="right">8</td>
	<td align="right">0.27</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	195.22.74.51		
	</td>
	<td align="right">357</td>
	<td align="right">0.48</td>
	<td align="right">12,976,429</td>
	<td align="right">0.29</td>
	<td align="right">3</td>
	<td align="right">0.10</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	83.249.7.173		
	</td>
	<td align="right">298</td>
	<td align="right">0.40</td>
	<td align="right">36,983,286</td>
	<td align="right">0.82</td>
	<td align="right">5</td>
	<td align="right">0.17</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">10</th>
	<td>
	85.228.143.146		
	</td>
	<td align="right">240</td>
	<td align="right">0.32</td>
	<td align="right">24,116,687</td>
	<td align="right">0.54</td>
	<td align="right">2</td>
	<td align="right">0.07</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">11</th>
	<td>
	212.247.176.200		
	</td>
	<td align="right">215</td>
	<td align="right">0.29</td>
	<td align="right">10,387,197</td>
	<td align="right">0.23</td>
	<td align="right">2</td>
	<td align="right">0.07</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">12</th>
	<td>
	85.229.200.234		
	</td>
	<td align="right">190</td>
	<td align="right">0.26</td>
	<td align="right">8,847,265</td>
	<td align="right">0.20</td>
	<td align="right">3</td>
	<td align="right">0.10</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">13</th>
	<td>
	185.81.108.109		
	</td>
	<td align="right">190</td>
	<td align="right">0.26</td>
	<td align="right">12,864,471</td>
	<td align="right">0.29</td>
	<td align="right">4</td>
	<td align="right">0.13</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">14</th>
	<td>
	82.242.160.74		
	</td>
	<td align="right">183</td>
	<td align="right">0.25</td>
	<td align="right">86,710,993</td>
	<td align="right">1.93</td>
	<td align="right">2</td>
	<td align="right">0.07</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">15</th>
	<td>
	46.17.185.87		
	</td>
	<td align="right">183</td>
	<td align="right">0.25</td>
	<td align="right">4,895,723</td>
	<td align="right">0.11</td>
	<td align="right">2</td>
	<td align="right">0.07</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">16</th>
	<td>
	66.249.79.180		
	</td>
	<td align="right">180</td>
	<td align="right">0.24</td>
	<td align="right">2,134,082</td>
	<td align="right">0.05</td>
	<td align="right">26</td>
	<td align="right">0.85</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">17</th>
	<td>
	79.142.244.252		
	</td>
	<td align="right">177</td>
	<td align="right">0.24</td>
	<td align="right">24,698,168</td>
	<td align="right">0.55</td>
	<td align="right">3</td>
	<td align="right">0.10</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">18</th>
	<td>
	37.46.169.77		
	</td>
	<td align="right">170</td>
	<td align="right">0.23</td>
	<td align="right">24,536,329</td>
	<td align="right">0.55</td>
	<td align="right">2</td>
	<td align="right">0.07</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">19</th>
	<td>
	80.216.2.56		
	</td>
	<td align="right">168</td>
	<td align="right">0.23</td>
	<td align="right">15,347,921</td>
	<td align="right">0.34</td>
	<td align="right">4</td>
	<td align="right">0.14</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">20</th>
	<td>
	217.214.150.81		
	</td>
	<td align="right">168</td>
	<td align="right">0.23</td>
	<td align="right">22,347,142</td>
	<td align="right">0.50</td>
	<td align="right">4</td>
	<td align="right">0.14</td>
</tr>

</tbody>
</table>

