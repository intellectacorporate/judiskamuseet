


<h2>Statistics for http://www.u5645470.fsdata.se, Week of 2019-9-16 to 2019-9-22</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 10 of 20 File Types (Extensions), 
Sorted by Access Count

</h3>
<p>Individual file types as determined by file extensions. All URLs that
do not contain an extension are counted as directories.</p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Type</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th class="hogerstalld">1</th>
	<td>
	js		
	</td>
	<td align="right">20,722</td>
	<td align="right">28.00</td>
	<td align="right">210,383,296</td>
	<td align="right">4.68</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	css		
	</td>
	<td align="right">15,993</td>
	<td align="right">21.61</td>
	<td align="right">205,160,753</td>
	<td align="right">4.56</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	jpg		
	</td>
	<td align="right">12,205</td>
	<td align="right">16.49</td>
	<td align="right">3,261,551,035</td>
	<td align="right">72.55</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	Directory (folder)		
	</td>
	<td align="right">11,694</td>
	<td align="right">15.80</td>
	<td align="right">145,741,788</td>
	<td align="right">3.24</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	png		
	</td>
	<td align="right">8,583</td>
	<td align="right">11.60</td>
	<td align="right">197,946,024</td>
	<td align="right">4.40</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	php		
	</td>
	<td align="right">1,930</td>
	<td align="right">2.61</td>
	<td align="right">19,934,853</td>
	<td align="right">0.44</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	ttf		
	</td>
	<td align="right">1,613</td>
	<td align="right">2.18</td>
	<td align="right">141,988,489</td>
	<td align="right">3.16</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	txt		
	</td>
	<td align="right">537</td>
	<td align="right">0.73</td>
	<td align="right">36,386</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	jpeg		
	</td>
	<td align="right">302</td>
	<td align="right">0.41</td>
	<td align="right">195,411,478</td>
	<td align="right">4.35</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">10</th>
	<td>
	ico		
	</td>
	<td align="right">200</td>
	<td align="right">0.27</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>

</tbody>
</table>

