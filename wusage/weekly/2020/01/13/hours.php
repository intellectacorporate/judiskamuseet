


<h2>Statistics for http://www.u5645470.fsdata.se, Week of 2020-1-13 to 2020-1-19</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Traffic by Hour</h3>
<p>Figures are averages for that hour
	of each day in the time period analyzed.</p>
<img src="h.png" width="468" height="351">
<br /><br />
<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Hour</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	<th scope='col'>Bits per Sec</th>
	<th scope='col'>Bytes per Sec</th>
	
	</tr>
</thead>
<tbody>
<tr class="jamnudda">
	<th align="right">00:00</th>
	<td align="right">312.86</td>
	<td align="right">2.87</td>
	<td align="right">10,232,847.29</td>
	<td align="right">1.68</td>
	<td align="right">6.68</td>
	<td align="right">1.49</td>
	<td align="right">22,739.66</td>
	<td align="right">2,842.46</td>
	
</tr>
<tr class="jamn">
	<th align="right">01:00</th>
	<td align="right">295.86</td>
	<td align="right">2.71</td>
	<td align="right">10,962,933.14</td>
	<td align="right">1.80</td>
	<td align="right">10.82</td>
	<td align="right">2.42</td>
	<td align="right">24,362.07</td>
	<td align="right">3,045.26</td>
	
</tr>
<tr class="udda">
	<th align="right">02:00</th>
	<td align="right">255.43</td>
	<td align="right">2.34</td>
	<td align="right">5,700,747.43</td>
	<td align="right">0.93</td>
	<td align="right">9.40</td>
	<td align="right">2.10</td>
	<td align="right">12,668.33</td>
	<td align="right">1,583.54</td>
	
</tr>
<tr class="jamn">
	<th align="right">03:00</th>
	<td align="right">242.57</td>
	<td align="right">2.23</td>
	<td align="right">4,588,626.86</td>
	<td align="right">0.75</td>
	<td align="right">5.73</td>
	<td align="right">1.28</td>
	<td align="right">10,196.95</td>
	<td align="right">1,274.62</td>
	
</tr>
<tr class="udda">
	<th align="right">04:00</th>
	<td align="right">267.86</td>
	<td align="right">2.46</td>
	<td align="right">4,798,317.29</td>
	<td align="right">0.79</td>
	<td align="right">6.21</td>
	<td align="right">1.39</td>
	<td align="right">10,662.93</td>
	<td align="right">1,332.87</td>
	
</tr>
<tr class="jamn">
	<th align="right">05:00</th>
	<td align="right">237.57</td>
	<td align="right">2.18</td>
	<td align="right">3,359,140.71</td>
	<td align="right">0.55</td>
	<td align="right">7.47</td>
	<td align="right">1.67</td>
	<td align="right">7,464.76</td>
	<td align="right">933.09</td>
	
</tr>
<tr class="udda">
	<th align="right">06:00</th>
	<td align="right">282.57</td>
	<td align="right">2.59</td>
	<td align="right">7,828,030.86</td>
	<td align="right">1.28</td>
	<td align="right">6.99</td>
	<td align="right">1.56</td>
	<td align="right">17,395.62</td>
	<td align="right">2,174.45</td>
	
</tr>
<tr class="jamn">
	<th align="right">07:00</th>
	<td align="right">313.71</td>
	<td align="right">2.88</td>
	<td align="right">12,453,248.71</td>
	<td align="right">2.04</td>
	<td align="right">9.71</td>
	<td align="right">2.17</td>
	<td align="right">27,673.89</td>
	<td align="right">3,459.24</td>
	
</tr>
<tr class="udda">
	<th align="right">08:00</th>
	<td align="right">459.43</td>
	<td align="right">4.22</td>
	<td align="right">36,887,977.43</td>
	<td align="right">6.04</td>
	<td align="right">16.23</td>
	<td align="right">3.62</td>
	<td align="right">81,973.28</td>
	<td align="right">10,246.66</td>
	
</tr>
<tr class="jamn">
	<th align="right">09:00</th>
	<td align="right">712.14</td>
	<td align="right">6.53</td>
	<td align="right">49,092,820.57</td>
	<td align="right">8.04</td>
	<td align="right">28.32</td>
	<td align="right">6.32</td>
	<td align="right">109,095.16</td>
	<td align="right">13,636.89</td>
	
</tr>
<tr class="udda">
	<th align="right">10:00</th>
	<td align="right">680.43</td>
	<td align="right">6.24</td>
	<td align="right">46,127,815.14</td>
	<td align="right">7.56</td>
	<td align="right">31.61</td>
	<td align="right">7.06</td>
	<td align="right">102,506.26</td>
	<td align="right">12,813.28</td>
	
</tr>
<tr class="jamn">
	<th align="right">11:00</th>
	<td align="right">628.29</td>
	<td align="right">5.76</td>
	<td align="right">43,542,728.43</td>
	<td align="right">7.13</td>
	<td align="right">30.39</td>
	<td align="right">6.78</td>
	<td align="right">96,761.62</td>
	<td align="right">12,095.20</td>
	
</tr>
<tr class="udda">
	<th align="right">12:00</th>
	<td align="right">619.71</td>
	<td align="right">5.69</td>
	<td align="right">44,987,033.57</td>
	<td align="right">7.37</td>
	<td align="right">26.27</td>
	<td align="right">5.86</td>
	<td align="right">99,971.19</td>
	<td align="right">12,496.40</td>
	
</tr>
<tr class="jamn">
	<th align="right">13:00</th>
	<td align="right">612.43</td>
	<td align="right">5.62</td>
	<td align="right">39,681,778.14</td>
	<td align="right">6.50</td>
	<td align="right">30.39</td>
	<td align="right">6.78</td>
	<td align="right">88,181.73</td>
	<td align="right">11,022.72</td>
	
</tr>
<tr class="udda">
	<th align="right">14:00</th>
	<td align="right">679.29</td>
	<td align="right">6.23</td>
	<td align="right">42,812,249.86</td>
	<td align="right">7.01</td>
	<td align="right">26.85</td>
	<td align="right">5.99</td>
	<td align="right">95,138.33</td>
	<td align="right">11,892.29</td>
	
</tr>
<tr class="jamn">
	<th align="right">15:00</th>
	<td align="right">519.00</td>
	<td align="right">4.76</td>
	<td align="right">32,668,951.43</td>
	<td align="right">5.35</td>
	<td align="right">22.58</td>
	<td align="right">5.04</td>
	<td align="right">72,597.67</td>
	<td align="right">9,074.71</td>
	
</tr>
<tr class="udda">
	<th align="right">16:00</th>
	<td align="right">548.71</td>
	<td align="right">5.03</td>
	<td align="right">29,312,480.29</td>
	<td align="right">4.80</td>
	<td align="right">21.00</td>
	<td align="right">4.69</td>
	<td align="right">65,138.85</td>
	<td align="right">8,142.36</td>
	
</tr>
<tr class="jamn">
	<th align="right">17:00</th>
	<td align="right">492.57</td>
	<td align="right">4.52</td>
	<td align="right">28,065,993.43</td>
	<td align="right">4.60</td>
	<td align="right">24.65</td>
	<td align="right">5.50</td>
	<td align="right">62,368.87</td>
	<td align="right">7,796.11</td>
	
</tr>
<tr class="udda">
	<th align="right">18:00</th>
	<td align="right">474.57</td>
	<td align="right">4.35</td>
	<td align="right">26,295,361.57</td>
	<td align="right">4.31</td>
	<td align="right">18.93</td>
	<td align="right">4.23</td>
	<td align="right">58,434.14</td>
	<td align="right">7,304.27</td>
	
</tr>
<tr class="jamn">
	<th align="right">19:00</th>
	<td align="right">451.00</td>
	<td align="right">4.14</td>
	<td align="right">22,793,317.43</td>
	<td align="right">3.73</td>
	<td align="right">19.72</td>
	<td align="right">4.40</td>
	<td align="right">50,651.82</td>
	<td align="right">6,331.48</td>
	
</tr>
<tr class="udda">
	<th align="right">20:00</th>
	<td align="right">510.86</td>
	<td align="right">4.69</td>
	<td align="right">29,570,461.29</td>
	<td align="right">4.84</td>
	<td align="right">19.13</td>
	<td align="right">4.27</td>
	<td align="right">65,712.14</td>
	<td align="right">8,214.02</td>
	
</tr>
<tr class="jamn">
	<th align="right">21:00</th>
	<td align="right">430.71</td>
	<td align="right">3.95</td>
	<td align="right">26,412,502.14</td>
	<td align="right">4.33</td>
	<td align="right">21.64</td>
	<td align="right">4.83</td>
	<td align="right">58,694.45</td>
	<td align="right">7,336.81</td>
	
</tr>
<tr class="udda">
	<th align="right">22:00</th>
	<td align="right">452.86</td>
	<td align="right">4.16</td>
	<td align="right">27,738,609.86</td>
	<td align="right">4.54</td>
	<td align="right">17.03</td>
	<td align="right">3.80</td>
	<td align="right">61,641.36</td>
	<td align="right">7,705.17</td>
	
</tr>
<tr class="jamn">
	<th align="right">23:00</th>
	<td align="right">417.86</td>
	<td align="right">3.83</td>
	<td align="right">24,465,565.71</td>
	<td align="right">4.01</td>
	<td align="right">30.24</td>
	<td align="right">6.75</td>
	<td align="right">54,367.92</td>
	<td align="right">6,795.99</td>
	
</tr>

</tbody>
</table>

