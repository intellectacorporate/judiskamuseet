


<h2>Statistics for http://www.u5645470.fsdata.se, Week of 2020-1-13 to 2020-1-19</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 10 of 207 Browsers (User Agents),
Sorted by Access Count

</h3>
<p></p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Product</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th class="hogerstalld">1</th>
	<td>
	Pingdom.com_bot_version_1.4_		
	</td>
	<td align="right">29,438</td>
	<td align="right">38.64</td>
	<td align="right">240,034,592</td>
	<td align="right">5.63</td>
	<td align="right">45</td>
	<td align="right">1.46</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	Safari/537.36		
	</td>
	<td align="right">18,549</td>
	<td align="right">24.34</td>
	<td align="right">1,668,907,301</td>
	<td align="right">39.14</td>
	<td align="right">959</td>
	<td align="right">30.76</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	Safari/604.1		
	</td>
	<td align="right">9,118</td>
	<td align="right">11.97</td>
	<td align="right">902,209,741</td>
	<td align="right">21.16</td>
	<td align="right">566</td>
	<td align="right">18.16</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	Mozilla/5.0 		
	</td>
	<td align="right">3,979</td>
	<td align="right">5.22</td>
	<td align="right">273,314,248</td>
	<td align="right">6.41</td>
	<td align="right">382</td>
	<td align="right">12.27</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	Safari/605.1.15		
	</td>
	<td align="right">3,289</td>
	<td align="right">4.32</td>
	<td align="right">331,027,790</td>
	<td align="right">7.76</td>
	<td align="right">190</td>
	<td align="right">6.11</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	UptimeRobot/2.0		
	</td>
	<td align="right">1,226</td>
	<td align="right">1.61</td>
	<td align="right">103,628</td>
	<td align="right">0.00</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	Safari/537.36 Edge/18.18362		
	</td>
	<td align="right">838</td>
	<td align="right">1.10</td>
	<td align="right">85,050,119</td>
	<td align="right">1.99</td>
	<td align="right">32</td>
	<td align="right">1.03</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	bingbot/2.0		
	</td>
	<td align="right">767</td>
	<td align="right">1.01</td>
	<td align="right">49,887,820</td>
	<td align="right">1.17</td>
	<td align="right">186</td>
	<td align="right">5.98</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	facebookexternalhit/1.1 		
	</td>
	<td align="right">696</td>
	<td align="right">0.91</td>
	<td align="right">43,722,658</td>
	<td align="right">1.03</td>
	<td align="right">105</td>
	<td align="right">3.40</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">10</th>
	<td>
	Safari/537.36 Edge/18.17763		
	</td>
	<td align="right">689</td>
	<td align="right">0.90</td>
	<td align="right">99,718,577</td>
	<td align="right">2.34</td>
	<td align="right">25</td>
	<td align="right">0.82</td>
</tr>

</tbody>
</table>

