


<h2>Statistics for http://www.u5645470.fsdata.se, Week of 2020-1-13 to 2020-1-19</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>HTTP Result Codes, Sorted by Code</h3>
<p></p>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Code</th>
	<th scope='col'>Description</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th class="hogerstalld">103</th>
	<td></td>
	<td align="right">519</td>
	<td align="right">0.65</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">200</th>
	<td>OK</td>
	<td align="right">63,754</td>
	<td align="right">80.11</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">206</th>
	<td>Partial Content</td>
	<td align="right">412</td>
	<td align="right">0.52</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">301</th>
	<td>Moved Permanently</td>
	<td align="right">11,092</td>
	<td align="right">13.94</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">302</th>
	<td>Found</td>
	<td align="right">46</td>
	<td align="right">0.06</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">304</th>
	<td>Not Modified</td>
	<td align="right">984</td>
	<td align="right">1.24</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">400</th>
	<td>Bad Request</td>
	<td align="right">15</td>
	<td align="right">0.02</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">401</th>
	<td>Unauthorized</td>
	<td align="right">5</td>
	<td align="right">0.01</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">403</th>
	<td>Forbidden</td>
	<td align="right">27</td>
	<td align="right">0.03</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">404</th>
	<td>Not Found</td>
	<td align="right">2,389</td>
	<td align="right">3.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">405</th>
	<td>Method Not Allowed</td>
	<td align="right">4</td>
	<td align="right">0.01</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">500</th>
	<td>Internal Server Error</td>
	<td align="right">264</td>
	<td align="right">0.33</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">503</th>
	<td>Service Unavailable</td>
	<td align="right">75</td>
	<td align="right">0.09</td>
</tr>

</tbody>
</table>




