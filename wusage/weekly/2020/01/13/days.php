


<h2>Statistics for http://www.u5645470.fsdata.se, Week of 2020-1-13 to 2020-1-19</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Traffic by Day</h3>
<img src="d.png" width="468" height="351">
<br /><br />


<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Date</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	<th scope='col'>Bits per Sec</th>
	<th scope='col'>Bytes per Sec</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th align="right">2020-1-13</th>
	<td align="right">10,967	</td>
	<td align="right">14.38	</td>
	<td align="right">561,340,896	</td>
	<td align="right">13.14	</td>
	<td align="right">518	</td>
	<td align="right">16.52	</td>
	<td align="right">51,976.01	</td>
	<td align="right">6,497.00	</td>
</tr>
<tr class="jamn">
	<th align="right">2020-1-14</th>
	<td align="right">10,704	</td>
	<td align="right">14.03	</td>
	<td align="right">599,259,048	</td>
	<td align="right">14.03	</td>
	<td align="right">493	</td>
	<td align="right">15.72	</td>
	<td align="right">55,486.95	</td>
	<td align="right">6,935.87	</td>
</tr>
<tr class="udda">
	<th align="right">2020-1-15</th>
	<td align="right">12,578	</td>
	<td align="right">16.49	</td>
	<td align="right">699,468,895	</td>
	<td align="right">16.37	</td>
	<td align="right">554	</td>
	<td align="right">17.67	</td>
	<td align="right">64,765.64	</td>
	<td align="right">8,095.70	</td>
</tr>
<tr class="jamn">
	<th align="right">2020-1-16</th>
	<td align="right">11,641	</td>
	<td align="right">15.26	</td>
	<td align="right">710,863,603	</td>
	<td align="right">16.64	</td>
	<td align="right">435	</td>
	<td align="right">13.87	</td>
	<td align="right">65,820.70	</td>
	<td align="right">8,227.59	</td>
</tr>
<tr class="udda">
	<th align="right">2020-1-17</th>
	<td align="right">10,750	</td>
	<td align="right">14.09	</td>
	<td align="right">598,368,294	</td>
	<td align="right">14.00	</td>
	<td align="right">416	</td>
	<td align="right">13.27	</td>
	<td align="right">55,404.47	</td>
	<td align="right">6,925.56	</td>
</tr>
<tr class="jamn">
	<th align="right">2020-1-18</th>
	<td align="right">9,704	</td>
	<td align="right">12.72	</td>
	<td align="right">524,677,147	</td>
	<td align="right">12.28	</td>
	<td align="right">364	</td>
	<td align="right">11.61	</td>
	<td align="right">48,581.22	</td>
	<td align="right">6,072.65	</td>
</tr>
<tr class="udda">
	<th align="right">2020-1-19</th>
	<td align="right">9,944	</td>
	<td align="right">13.03	</td>
	<td align="right">578,678,887	</td>
	<td align="right">13.54	</td>
	<td align="right">356	</td>
	<td align="right">11.35	</td>
	<td align="right">53,581.38	</td>
	<td align="right">6,697.67	</td>
</tr>

</tbody>
</table>

