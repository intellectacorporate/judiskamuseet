


<h2>Statistics for http://www.u5645470.fsdata.se, Week of 2020-1-13 to 2020-1-19</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 10 of 20 File Types (Extensions), 
Sorted by Access Count

</h3>
<p>Individual file types as determined by file extensions. All URLs that
do not contain an extension are counted as directories.</p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Type</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th class="hogerstalld">1</th>
	<td>
	Directory (folder)		
	</td>
	<td align="right">30,145</td>
	<td align="right">39.51</td>
	<td align="right">258,185,583</td>
	<td align="right">6.04</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	png		
	</td>
	<td align="right">18,647</td>
	<td align="right">24.44</td>
	<td align="right">239,024,426</td>
	<td align="right">5.59</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	js		
	</td>
	<td align="right">10,496</td>
	<td align="right">13.76</td>
	<td align="right">219,634,515</td>
	<td align="right">5.14</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	jpg		
	</td>
	<td align="right">9,536</td>
	<td align="right">12.50</td>
	<td align="right">3,003,848,479</td>
	<td align="right">70.30</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	css		
	</td>
	<td align="right">3,994</td>
	<td align="right">5.24</td>
	<td align="right">216,236,096</td>
	<td align="right">5.06</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	php		
	</td>
	<td align="right">1,551</td>
	<td align="right">2.03</td>
	<td align="right">25,518,016</td>
	<td align="right">0.60</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	ttf		
	</td>
	<td align="right">1,410</td>
	<td align="right">1.85</td>
	<td align="right">52,972,509</td>
	<td align="right">1.24</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	txt		
	</td>
	<td align="right">294</td>
	<td align="right">0.39</td>
	<td align="right">19,139</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	jpeg		
	</td>
	<td align="right">78</td>
	<td align="right">0.10</td>
	<td align="right">143,876,272</td>
	<td align="right">3.37</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">10</th>
	<td>
	ico		
	</td>
	<td align="right">56</td>
	<td align="right">0.07</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>

</tbody>
</table>

