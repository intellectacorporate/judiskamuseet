


<h2>Statistics for http://www.u5645470.fsdata.se, Week of 2020-1-13 to 2020-1-19</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 20 of 144 Referring Sites,
Sorted by Access Count

</h3>
<p></p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Referring Site</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th class="hogerstalld">1</th>
	<td>
	<a href="https://judiskamuseet.se">
	https://judiskamuseet.se</a>
	</td>
	<td align="right">34,194</td>
	<td align="right">89.37</td>
	<td align="right">3,498,735,727</td>
	<td align="right">96.82</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	<a href="http://judiskamuseet.se">
	http://judiskamuseet.se</a>
	</td>
	<td align="right">1,768</td>
	<td align="right">4.62</td>
	<td align="right">19,903,592</td>
	<td align="right">0.55</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	<a href="https://www.google.com">
	https://www.google.com</a>
	</td>
	<td align="right">1,080</td>
	<td align="right">2.82</td>
	<td align="right">56,081,570</td>
	<td align="right">1.55</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	<a href="https://www.google.se">
	https://www.google.se</a>
	</td>
	<td align="right">534</td>
	<td align="right">1.40</td>
	<td align="right">17,967,190</td>
	<td align="right">0.50</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	<a href="http://m.facebook.com">
	http://m.facebook.com</a>
	</td>
	<td align="right">66</td>
	<td align="right">0.17</td>
	<td align="right">785,990</td>
	<td align="right">0.02</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	<a href="http://www.google.com">
	http://www.google.com</a>
	</td>
	<td align="right">63</td>
	<td align="right">0.16</td>
	<td align="right">493,459</td>
	<td align="right">0.01</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	<a href="https://webcache.googleusercontent.com">
	https://webcache.googleusercontent.com</a>
	</td>
	<td align="right">39</td>
	<td align="right">0.10</td>
	<td align="right">1,740,656</td>
	<td align="right">0.05</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	<a href="https://www.bing.com">
	https://www.bing.com</a>
	</td>
	<td align="right">32</td>
	<td align="right">0.08</td>
	<td align="right">3,684,288</td>
	<td align="right">0.10</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	<a href="http://www.judiska-museet.se">
	http://www.judiska-museet.se</a>
	</td>
	<td align="right">18</td>
	<td align="right">0.05</td>
	<td align="right">29,923</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">10</th>
	<td>
	<a href="https://docs.google.com">
	https://docs.google.com</a>
	</td>
	<td align="right">16</td>
	<td align="right">0.04</td>
	<td align="right">174,576</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">11</th>
	<td>
	<a href="https://bpro1.top">
	https://bpro1.top</a>
	</td>
	<td align="right">15</td>
	<td align="right">0.04</td>
	<td align="right">810,045</td>
	<td align="right">0.02</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">12</th>
	<td>
	<a href="https://myactivity.google.com">
	https://myactivity.google.com</a>
	</td>
	<td align="right">13</td>
	<td align="right">0.03</td>
	<td align="right">1,554,360</td>
	<td align="right">0.04</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">13</th>
	<td>
	<a href="https://meblieco.com">
	https://meblieco.com</a>
	</td>
	<td align="right">12</td>
	<td align="right">0.03</td>
	<td align="right">648,036</td>
	<td align="right">0.02</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">14</th>
	<td>
	<a href="https://70casino.online">
	https://70casino.online</a>
	</td>
	<td align="right">12</td>
	<td align="right">0.03</td>
	<td align="right">648,036</td>
	<td align="right">0.02</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">15</th>
	<td>
	<a href="https://www.facebook.com">
	https://www.facebook.com</a>
	</td>
	<td align="right">11</td>
	<td align="right">0.03</td>
	<td align="right">143,147</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">16</th>
	<td>
	<a href="https://furniturehomewares.com">
	https://furniturehomewares.com</a>
	</td>
	<td align="right">9</td>
	<td align="right">0.02</td>
	<td align="right">486,027</td>
	<td align="right">0.01</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">17</th>
	<td>
	<a href="http://greenshop.su">
	http://greenshop.su</a>
	</td>
	<td align="right">9</td>
	<td align="right">0.02</td>
	<td align="right">486,027</td>
	<td align="right">0.01</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">18</th>
	<td>
	<a href="https://duckduckgo.com">
	https://duckduckgo.com</a>
	</td>
	<td align="right">8</td>
	<td align="right">0.02</td>
	<td align="right">96,444</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">19</th>
	<td>
	<a href="https://www.kurbits.nu">
	https://www.kurbits.nu</a>
	</td>
	<td align="right">8</td>
	<td align="right">0.02</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">20</th>
	<td>
	<a href="https://www.google.fr">
	https://www.google.fr</a>
	</td>
	<td align="right">8</td>
	<td align="right">0.02</td>
	<td align="right">176,993</td>
	<td align="right">0.00</td>
</tr>

</tbody>
</table>

