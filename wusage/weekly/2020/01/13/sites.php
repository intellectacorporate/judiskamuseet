


<h2>Statistics for http://www.u5645470.fsdata.se, Week of 2020-1-13 to 2020-1-19</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 20 of 3,463 Visitor Sites,
Sorted by Access Count

</h3>
<p></p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Site</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="jamnudda">
	<th class="hogerstalld">1</th>
	<td>
	185.39.146.214		
	</td>
	<td align="right">2,968</td>
	<td align="right">3.89</td>
	<td align="right">23,952,490</td>
	<td align="right">0.56</td>
	<td align="right">7</td>
	<td align="right">0.25</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	94.75.211.74		
	</td>
	<td align="right">2,948</td>
	<td align="right">3.86</td>
	<td align="right">24,104,193</td>
	<td align="right">0.56</td>
	<td align="right">7</td>
	<td align="right">0.25</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	185.152.65.167		
	</td>
	<td align="right">2,939</td>
	<td align="right">3.85</td>
	<td align="right">24,024,427</td>
	<td align="right">0.56</td>
	<td align="right">7</td>
	<td align="right">0.25</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	82.103.139.165		
	</td>
	<td align="right">1,938</td>
	<td align="right">2.54</td>
	<td align="right">14,872,949</td>
	<td align="right">0.35</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	46.246.122.10		
	</td>
	<td align="right">1,934</td>
	<td align="right">2.54</td>
	<td align="right">14,843,341</td>
	<td align="right">0.35</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	109.123.101.103		
	</td>
	<td align="right">1,914</td>
	<td align="right">2.51</td>
	<td align="right">14,754,286</td>
	<td align="right">0.35</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	82.103.145.126		
	</td>
	<td align="right">1,868</td>
	<td align="right">2.45</td>
	<td align="right">15,259,993</td>
	<td align="right">0.36</td>
	<td align="right">5</td>
	<td align="right">0.18</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	46.59.25.88		
	</td>
	<td align="right">1,592</td>
	<td align="right">2.09</td>
	<td align="right">103,085,265</td>
	<td align="right">2.41</td>
	<td align="right">32</td>
	<td align="right">1.03</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	37.252.231.50		
	</td>
	<td align="right">1,268</td>
	<td align="right">1.66</td>
	<td align="right">10,408,511</td>
	<td align="right">0.24</td>
	<td align="right">4</td>
	<td align="right">0.14</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">10</th>
	<td>
	63.143.42.242		
	</td>
	<td align="right">1,226</td>
	<td align="right">1.61</td>
	<td align="right">103,628</td>
	<td align="right">0.00</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">11</th>
	<td>
	89.163.242.206		
	</td>
	<td align="right">1,098</td>
	<td align="right">1.44</td>
	<td align="right">8,398,277</td>
	<td align="right">0.20</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">12</th>
	<td>
	81.17.62.205		
	</td>
	<td align="right">1,076</td>
	<td align="right">1.41</td>
	<td align="right">8,740,738</td>
	<td align="right">0.20</td>
	<td align="right">3</td>
	<td align="right">0.11</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">13</th>
	<td>
	94.247.174.83		
	</td>
	<td align="right">1,061</td>
	<td align="right">1.39</td>
	<td align="right">8,602,772</td>
	<td align="right">0.20</td>
	<td align="right">3</td>
	<td align="right">0.11</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">14</th>
	<td>
	46.20.45.18		
	</td>
	<td align="right">1,008</td>
	<td align="right">1.32</td>
	<td align="right">9,142,560</td>
	<td align="right">0.21</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">15</th>
	<td>
	5.172.196.188		
	</td>
	<td align="right">1,008</td>
	<td align="right">1.32</td>
	<td align="right">9,142,560</td>
	<td align="right">0.21</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">16</th>
	<td>
	151.106.52.134		
	</td>
	<td align="right">808</td>
	<td align="right">1.06</td>
	<td align="right">6,517,443</td>
	<td align="right">0.15</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">17</th>
	<td>
	85.195.116.134		
	</td>
	<td align="right">800</td>
	<td align="right">1.05</td>
	<td align="right">6,598,993</td>
	<td align="right">0.15</td>
	<td align="right">2</td>
	<td align="right">0.07</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">18</th>
	<td>
	185.246.208.82		
	</td>
	<td align="right">797</td>
	<td align="right">1.04</td>
	<td align="right">6,758,657</td>
	<td align="right">0.16</td>
	<td align="right">3</td>
	<td align="right">0.11</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">19</th>
	<td>
	185.180.12.65		
	</td>
	<td align="right">729</td>
	<td align="right">0.96</td>
	<td align="right">6,612,030</td>
	<td align="right">0.15</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">20</th>
	<td>
	178.255.155.2		
	</td>
	<td align="right">707</td>
	<td align="right">0.93</td>
	<td align="right">5,524,615</td>
	<td align="right">0.13</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>

</tbody>
</table>

