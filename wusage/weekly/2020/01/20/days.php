


<h2>Statistics for http://www.u5645470.fsdata.se, Week of 2020-1-20 to 2020-1-26</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Traffic by Day</h3>
<img src="d.png" width="468" height="351">
<br /><br />


<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Date</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	<th scope='col'>Bits per Sec</th>
	<th scope='col'>Bytes per Sec</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th align="right">2020-1-20</th>
	<td align="right">0	</td>
	<td align="right">0.00	</td>
	<td align="right">0	</td>
	<td align="right">0.00	</td>
	<td align="right">0	</td>
	<td align="right">0.00	</td>
	<td align="right">0.00	</td>
	<td align="right">0.00	</td>
</tr>
<tr class="jamn">
	<th align="right">2020-1-21</th>
	<td align="right">0	</td>
	<td align="right">0.00	</td>
	<td align="right">0	</td>
	<td align="right">0.00	</td>
	<td align="right">0	</td>
	<td align="right">0.00	</td>
	<td align="right">0.00	</td>
	<td align="right">0.00	</td>
</tr>
<tr class="udda">
	<th align="right">2020-1-22</th>
	<td align="right">0	</td>
	<td align="right">0.00	</td>
	<td align="right">0	</td>
	<td align="right">0.00	</td>
	<td align="right">0	</td>
	<td align="right">0.00	</td>
	<td align="right">0.00	</td>
	<td align="right">0.00	</td>
</tr>
<tr class="jamn">
	<th align="right">2020-1-23</th>
	<td align="right">0	</td>
	<td align="right">0.00	</td>
	<td align="right">0	</td>
	<td align="right">0.00	</td>
	<td align="right">0	</td>
	<td align="right">0.00	</td>
	<td align="right">0.00	</td>
	<td align="right">0.00	</td>
</tr>
<tr class="udda">
	<th align="right">2020-1-24</th>
	<td align="right">0	</td>
	<td align="right">0.00	</td>
	<td align="right">0	</td>
	<td align="right">0.00	</td>
	<td align="right">0	</td>
	<td align="right">0.00	</td>
	<td align="right">0.00	</td>
	<td align="right">0.00	</td>
</tr>
<tr class="jamn">
	<th align="right">2020-1-25</th>
	<td align="right">0	</td>
	<td align="right">0.00	</td>
	<td align="right">0	</td>
	<td align="right">0.00	</td>
	<td align="right">0	</td>
	<td align="right">0.00	</td>
	<td align="right">0.00	</td>
	<td align="right">0.00	</td>
</tr>
<tr class="udda">
	<th align="right">2020-1-26</th>
	<td align="right">0	</td>
	<td align="right">0.00	</td>
	<td align="right">0	</td>
	<td align="right">0.00	</td>
	<td align="right">0	</td>
	<td align="right">0.00	</td>
	<td align="right">0.00	</td>
	<td align="right">0.00	</td>
</tr>

</tbody>
</table>

