


<h2>Statistics for http://www.u5645470.fsdata.se, Week of 2020-1-20 to 2020-1-26</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Traffic by Hour</h3>
<p>Figures are averages for that hour
	of each day in the time period analyzed.</p>
<img src="h.png" width="468" height="351">
<br /><br />
<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Hour</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	<th scope='col'>Bits per Sec</th>
	<th scope='col'>Bytes per Sec</th>
	
	</tr>
</thead>
<tbody>
<tr class="jamnudda">
	<th align="right">00:00</th>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	
</tr>
<tr class="jamn">
	<th align="right">01:00</th>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	
</tr>
<tr class="udda">
	<th align="right">02:00</th>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	
</tr>
<tr class="jamn">
	<th align="right">03:00</th>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	
</tr>
<tr class="udda">
	<th align="right">04:00</th>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	
</tr>
<tr class="jamn">
	<th align="right">05:00</th>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	
</tr>
<tr class="udda">
	<th align="right">06:00</th>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	
</tr>
<tr class="jamn">
	<th align="right">07:00</th>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	
</tr>
<tr class="udda">
	<th align="right">08:00</th>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	
</tr>
<tr class="jamn">
	<th align="right">09:00</th>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	
</tr>
<tr class="udda">
	<th align="right">10:00</th>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	
</tr>
<tr class="jamn">
	<th align="right">11:00</th>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	
</tr>
<tr class="udda">
	<th align="right">12:00</th>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	
</tr>
<tr class="jamn">
	<th align="right">13:00</th>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	
</tr>
<tr class="udda">
	<th align="right">14:00</th>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	
</tr>
<tr class="jamn">
	<th align="right">15:00</th>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	
</tr>
<tr class="udda">
	<th align="right">16:00</th>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	
</tr>
<tr class="jamn">
	<th align="right">17:00</th>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	
</tr>
<tr class="udda">
	<th align="right">18:00</th>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	
</tr>
<tr class="jamn">
	<th align="right">19:00</th>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	
</tr>
<tr class="udda">
	<th align="right">20:00</th>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	
</tr>
<tr class="jamn">
	<th align="right">21:00</th>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	
</tr>
<tr class="udda">
	<th align="right">22:00</th>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	
</tr>
<tr class="jamn">
	<th align="right">23:00</th>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	<td align="right">0.00</td>
	
</tr>

</tbody>
</table>

