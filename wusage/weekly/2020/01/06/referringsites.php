


<h2>Statistics for http://www.u5645470.fsdata.se, Week of 2020-1-6 to 2020-1-12</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 20 of 133 Referring Sites,
Sorted by Access Count

</h3>
<p></p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Referring Site</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th class="hogerstalld">1</th>
	<td>
	<a href="https://judiskamuseet.se">
	https://judiskamuseet.se</a>
	</td>
	<td align="right">29,299</td>
	<td align="right">88.74</td>
	<td align="right">3,315,577,976</td>
	<td align="right">97.02</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	<a href="http://judiskamuseet.se">
	http://judiskamuseet.se</a>
	</td>
	<td align="right">1,923</td>
	<td align="right">5.82</td>
	<td align="right">26,640,436</td>
	<td align="right">0.78</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	<a href="https://www.google.com">
	https://www.google.com</a>
	</td>
	<td align="right">838</td>
	<td align="right">2.54</td>
	<td align="right">30,204,360</td>
	<td align="right">0.88</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	<a href="https://www.google.se">
	https://www.google.se</a>
	</td>
	<td align="right">448</td>
	<td align="right">1.36</td>
	<td align="right">27,272,064</td>
	<td align="right">0.80</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	<a href="http://www.google.com">
	http://www.google.com</a>
	</td>
	<td align="right">65</td>
	<td align="right">0.20</td>
	<td align="right">491,938</td>
	<td align="right">0.01</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	<a href="https://www.bing.com">
	https://www.bing.com</a>
	</td>
	<td align="right">32</td>
	<td align="right">0.10</td>
	<td align="right">1,931,263</td>
	<td align="right">0.06</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	<a href="http://www.judiska-museet.se">
	http://www.judiska-museet.se</a>
	</td>
	<td align="right">13</td>
	<td align="right">0.04</td>
	<td align="right">200</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	<a href="https://www.google.de">
	https://www.google.de</a>
	</td>
	<td align="right">10</td>
	<td align="right">0.03</td>
	<td align="right">123,968</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	<a href="https://stop-nark.ru">
	https://stop-nark.ru</a>
	</td>
	<td align="right">9</td>
	<td align="right">0.03</td>
	<td align="right">485,955</td>
	<td align="right">0.01</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">10</th>
	<td>
	<a href="https://sauna-kursk.ru">
	https://sauna-kursk.ru</a>
	</td>
	<td align="right">9</td>
	<td align="right">0.03</td>
	<td align="right">486,003</td>
	<td align="right">0.01</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">11</th>
	<td>
	<a href="https://classroom.google.com">
	https://classroom.google.com</a>
	</td>
	<td align="right">9</td>
	<td align="right">0.03</td>
	<td align="right">58,090</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">12</th>
	<td>
	<a href="https://89.221.250.27:443">
	https://89.221.250.27:443</a>
	</td>
	<td align="right">9</td>
	<td align="right">0.03</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">13</th>
	<td>
	<a href="https://www.kurbits.nu">
	https://www.kurbits.nu</a>
	</td>
	<td align="right">7</td>
	<td align="right">0.02</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">14</th>
	<td>
	<a href="https://duckduckgo.com">
	https://duckduckgo.com</a>
	</td>
	<td align="right">7</td>
	<td align="right">0.02</td>
	<td align="right">103,759</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">15</th>
	<td>
	<a href="https://adpostmalta.com">
	https://adpostmalta.com</a>
	</td>
	<td align="right">6</td>
	<td align="right">0.02</td>
	<td align="right">324,018</td>
	<td align="right">0.01</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">16</th>
	<td>
	<a href="https://vksex.ru">
	https://vksex.ru</a>
	</td>
	<td align="right">6</td>
	<td align="right">0.02</td>
	<td align="right">324,018</td>
	<td align="right">0.01</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">17</th>
	<td>
	<a href="http://www.stockholmmuseums.se">
	http://www.stockholmmuseums.se</a>
	</td>
	<td align="right">6</td>
	<td align="right">0.02</td>
	<td align="right">44,510</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">18</th>
	<td>
	<a href="http://www.judiskamuseet.se">
	http://www.judiskamuseet.se</a>
	</td>
	<td align="right">6</td>
	<td align="right">0.02</td>
	<td align="right">206,541</td>
	<td align="right">0.01</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">19</th>
	<td>
	<a href="https://s-forum.biz">
	https://s-forum.biz</a>
	</td>
	<td align="right">6</td>
	<td align="right">0.02</td>
	<td align="right">324,018</td>
	<td align="right">0.01</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">20</th>
	<td>
	<a href="https://pornhive.org">
	https://pornhive.org</a>
	</td>
	<td align="right">6</td>
	<td align="right">0.02</td>
	<td align="right">323,994</td>
	<td align="right">0.01</td>
</tr>

</tbody>
</table>

