


<h2>Statistics for http://www.u5645470.fsdata.se, Week of 2020-1-6 to 2020-1-12</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 10 of 10 Operating Systems,
Sorted by Access Count

</h3>
<p></p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Product</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th class="hogerstalld">1</th>
	<td>
	Other		
	</td>
	<td align="right">37,257</td>
	<td align="right">51.01</td>
	<td align="right">707,515,680</td>
	<td align="right">16.61</td>
	<td align="right">897</td>
	<td align="right">26.53</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	Mac OS X		
	</td>
	<td align="right">30,707</td>
	<td align="right">42.04</td>
	<td align="right">3,185,595,083</td>
	<td align="right">74.78</td>
	<td align="right">1,812</td>
	<td align="right">53.58</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	Windows NT		
	</td>
	<td align="right">3,138</td>
	<td align="right">4.30</td>
	<td align="right">356,084,785</td>
	<td align="right">8.36</td>
	<td align="right">109</td>
	<td align="right">3.23</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	Linux		
	</td>
	<td align="right">1,733</td>
	<td align="right">2.37</td>
	<td align="right">3,315,455</td>
	<td align="right">0.08</td>
	<td align="right">552</td>
	<td align="right">16.35</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	Windows XP		
	</td>
	<td align="right">103</td>
	<td align="right">0.14</td>
	<td align="right">4,294,211</td>
	<td align="right">0.10</td>
	<td align="right">7</td>
	<td align="right">0.21</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	Windows 2000		
	</td>
	<td align="right">33</td>
	<td align="right">0.05</td>
	<td align="right">1,595,655</td>
	<td align="right">0.04</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	Windows 2003		
	</td>
	<td align="right">25</td>
	<td align="right">0.03</td>
	<td align="right">461,495</td>
	<td align="right">0.01</td>
	<td align="right">1</td>
	<td align="right">0.03</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	Windows Codename Longhorn		
	</td>
	<td align="right">22</td>
	<td align="right">0.03</td>
	<td align="right">155,852</td>
	<td align="right">0.00</td>
	<td align="right">2</td>
	<td align="right">0.07</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	Windows 95		
	</td>
	<td align="right">12</td>
	<td align="right">0.02</td>
	<td align="right">647,988</td>
	<td align="right">0.02</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">10</th>
	<td>
	Mac OS		
	</td>
	<td align="right">6</td>
	<td align="right">0.01</td>
	<td align="right">16,890</td>
	<td align="right">0.00</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>

</tbody>
</table>

