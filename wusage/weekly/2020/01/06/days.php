


<h2>Statistics for http://www.u5645470.fsdata.se, Week of 2020-1-6 to 2020-1-12</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Traffic by Day</h3>
<img src="d.png" width="468" height="351">
<br /><br />


<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Date</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	<th scope='col'>Bits per Sec</th>
	<th scope='col'>Bytes per Sec</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th align="right">2020-1-6</th>
	<td align="right">8,713	</td>
	<td align="right">11.91	</td>
	<td align="right">411,804,334	</td>
	<td align="right">9.62	</td>
	<td align="right">341	</td>
	<td align="right">10.08	</td>
	<td align="right">38,130.03	</td>
	<td align="right">4,766.25	</td>
</tr>
<tr class="jamn">
	<th align="right">2020-1-7</th>
	<td align="right">11,643	</td>
	<td align="right">15.92	</td>
	<td align="right">707,324,672	</td>
	<td align="right">16.53	</td>
	<td align="right">673	</td>
	<td align="right">19.90	</td>
	<td align="right">65,493.03	</td>
	<td align="right">8,186.63	</td>
</tr>
<tr class="udda">
	<th align="right">2020-1-8</th>
	<td align="right">10,993	</td>
	<td align="right">15.03	</td>
	<td align="right">589,966,854	</td>
	<td align="right">13.79	</td>
	<td align="right">609	</td>
	<td align="right">18.01	</td>
	<td align="right">54,626.56	</td>
	<td align="right">6,828.32	</td>
</tr>
<tr class="jamn">
	<th align="right">2020-1-9</th>
	<td align="right">11,740	</td>
	<td align="right">16.05	</td>
	<td align="right">869,819,317	</td>
	<td align="right">20.32	</td>
	<td align="right">495	</td>
	<td align="right">14.64	</td>
	<td align="right">80,538.83	</td>
	<td align="right">10,067.35	</td>
</tr>
<tr class="udda">
	<th align="right">2020-1-10</th>
	<td align="right">10,959	</td>
	<td align="right">14.98	</td>
	<td align="right">713,890,961	</td>
	<td align="right">16.68	</td>
	<td align="right">476	</td>
	<td align="right">14.07	</td>
	<td align="right">66,101.01	</td>
	<td align="right">8,262.63	</td>
</tr>
<tr class="jamn">
	<th align="right">2020-1-11</th>
	<td align="right">9,010	</td>
	<td align="right">12.32	</td>
	<td align="right">432,809,212	</td>
	<td align="right">10.11	</td>
	<td align="right">416	</td>
	<td align="right">12.30	</td>
	<td align="right">40,074.93	</td>
	<td align="right">5,009.37	</td>
</tr>
<tr class="udda">
	<th align="right">2020-1-12</th>
	<td align="right">10,078	</td>
	<td align="right">13.78	</td>
	<td align="right">553,996,570	</td>
	<td align="right">12.95	</td>
	<td align="right">372	</td>
	<td align="right">11.00	</td>
	<td align="right">51,295.98	</td>
	<td align="right">6,412.00	</td>
</tr>

</tbody>
</table>

