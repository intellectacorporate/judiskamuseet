


<h2>Statistics for http://www.u5645470.fsdata.se, Week of 2020-1-6 to 2020-1-12</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 10 of 21 File Types (Extensions), 
Sorted by Access Count

</h3>
<p>Individual file types as determined by file extensions. All URLs that
do not contain an extension are counted as directories.</p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Type</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th class="hogerstalld">1</th>
	<td>
	Directory (folder)		
	</td>
	<td align="right">30,978</td>
	<td align="right">42.36</td>
	<td align="right">267,183,166</td>
	<td align="right">6.24</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	png		
	</td>
	<td align="right">17,223</td>
	<td align="right">23.55</td>
	<td align="right">200,265,098</td>
	<td align="right">4.68</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	jpg		
	</td>
	<td align="right">8,840</td>
	<td align="right">12.09</td>
	<td align="right">3,174,519,503</td>
	<td align="right">74.18</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	js		
	</td>
	<td align="right">8,714</td>
	<td align="right">11.91</td>
	<td align="right">173,915,355</td>
	<td align="right">4.06</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	css		
	</td>
	<td align="right">3,430</td>
	<td align="right">4.69</td>
	<td align="right">178,564,449</td>
	<td align="right">4.17</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	php		
	</td>
	<td align="right">2,285</td>
	<td align="right">3.12</td>
	<td align="right">16,551,207</td>
	<td align="right">0.39</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	ttf		
	</td>
	<td align="right">1,035</td>
	<td align="right">1.42</td>
	<td align="right">38,761,461</td>
	<td align="right">0.91</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	txt		
	</td>
	<td align="right">352</td>
	<td align="right">0.48</td>
	<td align="right">23,120</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	jpeg		
	</td>
	<td align="right">116</td>
	<td align="right">0.16</td>
	<td align="right">184,821,142</td>
	<td align="right">4.32</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">10</th>
	<td>
	eot		
	</td>
	<td align="right">58</td>
	<td align="right">0.08</td>
	<td align="right">2,175,986</td>
	<td align="right">0.05</td>
</tr>

</tbody>
</table>

