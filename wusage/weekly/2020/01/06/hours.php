


<h2>Statistics for http://www.u5645470.fsdata.se, Week of 2020-1-6 to 2020-1-12</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Traffic by Hour</h3>
<p>Figures are averages for that hour
	of each day in the time period analyzed.</p>
<img src="h.png" width="468" height="351">
<br /><br />
<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Hour</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	<th scope='col'>Bits per Sec</th>
	<th scope='col'>Bytes per Sec</th>
	
	</tr>
</thead>
<tbody>
<tr class="jamnudda">
	<th align="right">00:00</th>
	<td align="right">359.00</td>
	<td align="right">3.44</td>
	<td align="right">23,139,098.71</td>
	<td align="right">3.78</td>
	<td align="right">10.18</td>
	<td align="right">2.11</td>
	<td align="right">51,420.22</td>
	<td align="right">6,427.53</td>
	
</tr>
<tr class="jamn">
	<th align="right">01:00</th>
	<td align="right">313.29</td>
	<td align="right">3.00</td>
	<td align="right">12,350,481.00</td>
	<td align="right">2.02</td>
	<td align="right">14.74</td>
	<td align="right">3.05</td>
	<td align="right">27,445.51</td>
	<td align="right">3,430.69</td>
	
</tr>
<tr class="udda">
	<th align="right">02:00</th>
	<td align="right">262.71</td>
	<td align="right">2.51</td>
	<td align="right">4,667,815.43</td>
	<td align="right">0.76</td>
	<td align="right">8.89</td>
	<td align="right">1.84</td>
	<td align="right">10,372.92</td>
	<td align="right">1,296.62</td>
	
</tr>
<tr class="jamn">
	<th align="right">03:00</th>
	<td align="right">276.00</td>
	<td align="right">2.64</td>
	<td align="right">5,149,308.57</td>
	<td align="right">0.84</td>
	<td align="right">10.25</td>
	<td align="right">2.12</td>
	<td align="right">11,442.91</td>
	<td align="right">1,430.36</td>
	
</tr>
<tr class="udda">
	<th align="right">04:00</th>
	<td align="right">275.71</td>
	<td align="right">2.64</td>
	<td align="right">8,877,881.86</td>
	<td align="right">1.45</td>
	<td align="right">10.86</td>
	<td align="right">2.25</td>
	<td align="right">19,728.63</td>
	<td align="right">2,466.08</td>
	
</tr>
<tr class="jamn">
	<th align="right">05:00</th>
	<td align="right">281.71</td>
	<td align="right">2.70</td>
	<td align="right">10,840,834.00</td>
	<td align="right">1.77</td>
	<td align="right">15.72</td>
	<td align="right">3.25</td>
	<td align="right">24,090.74</td>
	<td align="right">3,011.34</td>
	
</tr>
<tr class="udda">
	<th align="right">06:00</th>
	<td align="right">288.57</td>
	<td align="right">2.76</td>
	<td align="right">12,588,200.29</td>
	<td align="right">2.06</td>
	<td align="right">13.24</td>
	<td align="right">2.74</td>
	<td align="right">27,973.78</td>
	<td align="right">3,496.72</td>
	
</tr>
<tr class="jamn">
	<th align="right">07:00</th>
	<td align="right">334.43</td>
	<td align="right">3.20</td>
	<td align="right">16,036,730.14</td>
	<td align="right">2.62</td>
	<td align="right">12.95</td>
	<td align="right">2.68</td>
	<td align="right">35,637.18</td>
	<td align="right">4,454.65</td>
	
</tr>
<tr class="udda">
	<th align="right">08:00</th>
	<td align="right">419.29</td>
	<td align="right">4.01</td>
	<td align="right">26,094,893.14</td>
	<td align="right">4.27</td>
	<td align="right">16.05</td>
	<td align="right">3.32</td>
	<td align="right">57,988.65</td>
	<td align="right">7,248.58</td>
	
</tr>
<tr class="jamn">
	<th align="right">09:00</th>
	<td align="right">584.57</td>
	<td align="right">5.60</td>
	<td align="right">41,764,511.86</td>
	<td align="right">6.83</td>
	<td align="right">20.06</td>
	<td align="right">4.15</td>
	<td align="right">92,810.03</td>
	<td align="right">11,601.25</td>
	
</tr>
<tr class="udda">
	<th align="right">10:00</th>
	<td align="right">569.43</td>
	<td align="right">5.45</td>
	<td align="right">36,360,649.43</td>
	<td align="right">5.95</td>
	<td align="right">27.31</td>
	<td align="right">5.65</td>
	<td align="right">80,801.44</td>
	<td align="right">10,100.18</td>
	
</tr>
<tr class="jamn">
	<th align="right">11:00</th>
	<td align="right">599.71</td>
	<td align="right">5.74</td>
	<td align="right">55,558,069.14</td>
	<td align="right">9.09</td>
	<td align="right">27.77</td>
	<td align="right">5.75</td>
	<td align="right">123,462.38</td>
	<td align="right">15,432.80</td>
	
</tr>
<tr class="udda">
	<th align="right">12:00</th>
	<td align="right">528.14</td>
	<td align="right">5.05</td>
	<td align="right">40,237,428.43</td>
	<td align="right">6.58</td>
	<td align="right">23.17</td>
	<td align="right">4.80</td>
	<td align="right">89,416.51</td>
	<td align="right">11,177.06</td>
	
</tr>
<tr class="jamn">
	<th align="right">13:00</th>
	<td align="right">547.14</td>
	<td align="right">5.24</td>
	<td align="right">40,101,885.71</td>
	<td align="right">6.56</td>
	<td align="right">24.76</td>
	<td align="right">5.13</td>
	<td align="right">89,115.30</td>
	<td align="right">11,139.41</td>
	
</tr>
<tr class="udda">
	<th align="right">14:00</th>
	<td align="right">585.14</td>
	<td align="right">5.60</td>
	<td align="right">30,219,889.43</td>
	<td align="right">4.94</td>
	<td align="right">25.85</td>
	<td align="right">5.35</td>
	<td align="right">67,155.31</td>
	<td align="right">8,394.41</td>
	
</tr>
<tr class="jamn">
	<th align="right">15:00</th>
	<td align="right">507.86</td>
	<td align="right">4.86</td>
	<td align="right">35,081,384.00</td>
	<td align="right">5.74</td>
	<td align="right">25.29</td>
	<td align="right">5.23</td>
	<td align="right">77,958.63</td>
	<td align="right">9,744.83</td>
	
</tr>
<tr class="udda">
	<th align="right">16:00</th>
	<td align="right">553.43</td>
	<td align="right">5.30</td>
	<td align="right">35,997,896.57</td>
	<td align="right">5.89</td>
	<td align="right">24.58</td>
	<td align="right">5.09</td>
	<td align="right">79,995.33</td>
	<td align="right">9,999.42</td>
	
</tr>
<tr class="jamn">
	<th align="right">17:00</th>
	<td align="right">522.14</td>
	<td align="right">5.00</td>
	<td align="right">23,073,779.14</td>
	<td align="right">3.77</td>
	<td align="right">24.86</td>
	<td align="right">5.14</td>
	<td align="right">51,275.06</td>
	<td align="right">6,409.38</td>
	
</tr>
<tr class="udda">
	<th align="right">18:00</th>
	<td align="right">436.71</td>
	<td align="right">4.18</td>
	<td align="right">24,620,166.57</td>
	<td align="right">4.03</td>
	<td align="right">22.84</td>
	<td align="right">4.73</td>
	<td align="right">54,711.48</td>
	<td align="right">6,838.94</td>
	
</tr>
<tr class="jamn">
	<th align="right">19:00</th>
	<td align="right">497.86</td>
	<td align="right">4.77</td>
	<td align="right">33,638,937.86</td>
	<td align="right">5.50</td>
	<td align="right">24.37</td>
	<td align="right">5.04</td>
	<td align="right">74,753.20</td>
	<td align="right">9,344.15</td>
	
</tr>
<tr class="udda">
	<th align="right">20:00</th>
	<td align="right">473.57</td>
	<td align="right">4.53</td>
	<td align="right">27,704,130.14</td>
	<td align="right">4.53</td>
	<td align="right">22.64</td>
	<td align="right">4.68</td>
	<td align="right">61,564.73</td>
	<td align="right">7,695.59</td>
	
</tr>
<tr class="jamn">
	<th align="right">21:00</th>
	<td align="right">448.00</td>
	<td align="right">4.29</td>
	<td align="right">24,488,817.57</td>
	<td align="right">4.01</td>
	<td align="right">25.93</td>
	<td align="right">5.37</td>
	<td align="right">54,419.59</td>
	<td align="right">6,802.45</td>
	
</tr>
<tr class="udda">
	<th align="right">22:00</th>
	<td align="right">434.43</td>
	<td align="right">4.16</td>
	<td align="right">26,514,922.29</td>
	<td align="right">4.34</td>
	<td align="right">21.33</td>
	<td align="right">4.41</td>
	<td align="right">58,922.05</td>
	<td align="right">7,365.26</td>
	
</tr>
<tr class="jamn">
	<th align="right">23:00</th>
	<td align="right">349.14</td>
	<td align="right">3.34</td>
	<td align="right">16,265,420.14</td>
	<td align="right">2.66</td>
	<td align="right">29.49</td>
	<td align="right">6.10</td>
	<td align="right">36,145.38</td>
	<td align="right">4,518.17</td>
	
</tr>

</tbody>
</table>

