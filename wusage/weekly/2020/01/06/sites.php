


<h2>Statistics for http://www.u5645470.fsdata.se, Week of 2020-1-6 to 2020-1-12</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 20 of 3,647 Visitor Sites,
Sorted by Access Count

</h3>
<p></p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Site</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="jamnudda">
	<th class="hogerstalld">1</th>
	<td>
	94.247.174.83		
	</td>
	<td align="right">2,944</td>
	<td align="right">4.03</td>
	<td align="right">24,085,605</td>
	<td align="right">0.56</td>
	<td align="right">8</td>
	<td align="right">0.24</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	94.75.211.74		
	</td>
	<td align="right">2,936</td>
	<td align="right">4.01</td>
	<td align="right">24,138,802</td>
	<td align="right">0.56</td>
	<td align="right">8</td>
	<td align="right">0.24</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	185.152.65.167		
	</td>
	<td align="right">2,930</td>
	<td align="right">4.01</td>
	<td align="right">24,100,131</td>
	<td align="right">0.56</td>
	<td align="right">8</td>
	<td align="right">0.24</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	82.103.145.126		
	</td>
	<td align="right">2,906</td>
	<td align="right">3.97</td>
	<td align="right">23,877,877</td>
	<td align="right">0.56</td>
	<td align="right">8</td>
	<td align="right">0.24</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	185.39.146.214		
	</td>
	<td align="right">2,862</td>
	<td align="right">3.91</td>
	<td align="right">23,355,272</td>
	<td align="right">0.55</td>
	<td align="right">8</td>
	<td align="right">0.24</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	185.136.156.82		
	</td>
	<td align="right">1,942</td>
	<td align="right">2.66</td>
	<td align="right">14,957,941</td>
	<td align="right">0.35</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	46.246.122.10		
	</td>
	<td align="right">1,939</td>
	<td align="right">2.65</td>
	<td align="right">14,943,113</td>
	<td align="right">0.35</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	109.123.101.103		
	</td>
	<td align="right">1,937</td>
	<td align="right">2.65</td>
	<td align="right">14,972,615</td>
	<td align="right">0.35</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	82.103.139.165		
	</td>
	<td align="right">1,831</td>
	<td align="right">2.50</td>
	<td align="right">14,275,640</td>
	<td align="right">0.33</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">10</th>
	<td>
	46.59.25.88		
	</td>
	<td align="right">1,762</td>
	<td align="right">2.41</td>
	<td align="right">112,826,752</td>
	<td align="right">2.64</td>
	<td align="right">28</td>
	<td align="right">0.85</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">11</th>
	<td>
	37.252.231.50		
	</td>
	<td align="right">1,705</td>
	<td align="right">2.33</td>
	<td align="right">14,098,808</td>
	<td align="right">0.33</td>
	<td align="right">5</td>
	<td align="right">0.17</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">12</th>
	<td>
	81.17.62.205		
	</td>
	<td align="right">1,222</td>
	<td align="right">1.67</td>
	<td align="right">9,971,647</td>
	<td align="right">0.23</td>
	<td align="right">3</td>
	<td align="right">0.10</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">13</th>
	<td>
	63.143.42.242		
	</td>
	<td align="right">1,215</td>
	<td align="right">1.66</td>
	<td align="right">14,824</td>
	<td align="right">0.00</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">14</th>
	<td>
	151.106.52.134		
	</td>
	<td align="right">1,008</td>
	<td align="right">1.38</td>
	<td align="right">9,142,560</td>
	<td align="right">0.21</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">15</th>
	<td>
	185.246.208.82		
	</td>
	<td align="right">1,008</td>
	<td align="right">1.38</td>
	<td align="right">9,142,560</td>
	<td align="right">0.21</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">16</th>
	<td>
	46.20.45.18		
	</td>
	<td align="right">962</td>
	<td align="right">1.32</td>
	<td align="right">8,725,340</td>
	<td align="right">0.20</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">17</th>
	<td>
	85.119.133.32		
	</td>
	<td align="right">928</td>
	<td align="right">1.27</td>
	<td align="right">134,133,703</td>
	<td align="right">3.13</td>
	<td align="right">19</td>
	<td align="right">0.58</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">18</th>
	<td>
	95.141.32.46		
	</td>
	<td align="right">580</td>
	<td align="right">0.79</td>
	<td align="right">5,260,600</td>
	<td align="right">0.12</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">19</th>
	<td>
	5.172.196.188		
	</td>
	<td align="right">500</td>
	<td align="right">0.68</td>
	<td align="right">4,409,614</td>
	<td align="right">0.10</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">20</th>
	<td>
	66.249.64.174		
	</td>
	<td align="right">494</td>
	<td align="right">0.68</td>
	<td align="right">8,552,118</td>
	<td align="right">0.20</td>
	<td align="right">101</td>
	<td align="right">3.00</td>
</tr>

</tbody>
</table>

