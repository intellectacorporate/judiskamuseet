


<h2>Statistics for http://www.u5645470.fsdata.se, Week of 2020-1-6 to 2020-1-12</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 10 of 194 Browsers (User Agents),
Sorted by Access Count

</h3>
<p></p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Product</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th class="hogerstalld">1</th>
	<td>
	Pingdom.com_bot_version_1.4_		
	</td>
	<td align="right">29,390</td>
	<td align="right">40.24</td>
	<td align="right">240,900,832</td>
	<td align="right">5.66</td>
	<td align="right">52</td>
	<td align="right">1.56</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	Safari/537.36		
	</td>
	<td align="right">14,928</td>
	<td align="right">20.44</td>
	<td align="right">1,478,831,151</td>
	<td align="right">34.72</td>
	<td align="right">736</td>
	<td align="right">21.92</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	Safari/604.1		
	</td>
	<td align="right">7,624</td>
	<td align="right">10.44</td>
	<td align="right">813,180,554</td>
	<td align="right">19.09</td>
	<td align="right">459</td>
	<td align="right">13.67</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	Mozilla/5.0 		
	</td>
	<td align="right">5,312</td>
	<td align="right">7.27</td>
	<td align="right">420,257,105</td>
	<td align="right">9.87</td>
	<td align="right">694</td>
	<td align="right">20.66</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	Safari/605.1.15		
	</td>
	<td align="right">2,487</td>
	<td align="right">3.41</td>
	<td align="right">269,993,500</td>
	<td align="right">6.34</td>
	<td align="right">137</td>
	<td align="right">4.09</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	bingbot/2.0		
	</td>
	<td align="right">1,257</td>
	<td align="right">1.72</td>
	<td align="right">110,189,624</td>
	<td align="right">2.59</td>
	<td align="right">306</td>
	<td align="right">9.11</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	UptimeRobot/2.0		
	</td>
	<td align="right">1,215</td>
	<td align="right">1.66</td>
	<td align="right">14,824</td>
	<td align="right">0.00</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	Safari/537.36 (compatible; Googlebot/2.1;  http://www.google.com/bot.html)		
	</td>
	<td align="right">1,095</td>
	<td align="right">1.50</td>
	<td align="right">11,756,827</td>
	<td align="right">0.28</td>
	<td align="right">250</td>
	<td align="right">7.45</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	Safari/537.36 Edge/18.18362		
	</td>
	<td align="right">961</td>
	<td align="right">1.32</td>
	<td align="right">159,679,407</td>
	<td align="right">3.75</td>
	<td align="right">43</td>
	<td align="right">1.29</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">10</th>
	<td>
	Safari/537.36 Edge/18.17763		
	</td>
	<td align="right">588</td>
	<td align="right">0.81</td>
	<td align="right">76,026,432</td>
	<td align="right">1.78</td>
	<td align="right">20</td>
	<td align="right">0.61</td>
</tr>

</tbody>
</table>

