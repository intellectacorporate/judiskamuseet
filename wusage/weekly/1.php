


<h2>Statistics for http://www.u5645470.fsdata.se, Week of 2017-6-26 to 2020-1-26</h2>

	<h3>History from 2017-6-26 to 2020-1-26</h3>
	
	<p><img src="1t.png" width="468" height="351"></p>
	<table cellspacing='0' class='styladtabell'>
	<caption>Overall Accesses</caption>
	<thead>
	<tr>
	<th scope="col" class='centrerad'>Report Date</th>
	<th scope="col" class='centrerad'>Accesses</th>
	<th scope="col" class='centrerad'>Bytes</th>
	<th scope="col" class='centrerad'>Visits</th>
	
	</tr>
        </thead>
	<tbody>
	<tr class="jamn">
	<th align="left">Total</th>
	<td align="right">6,289,297</td>
	<td align="right">1,092,773,978,293</td>
	<td align="right">289,654</td>	
	
	</tr>
	<tr class="udda">
	<th align="left">Average</th>
	<td align="right">46,587</td>
	<td align="right">8,094,622,061</td>
	<td align="right">2,145</td>
	
	</tr>
	<tr class="jamn">
	<th align="left">Latest</th>
	<td align="right">0.00</td>
	<td align="right">0</td>
	<td align="right">0</td>
	
	</tr>
	<tr class="udda">
	<th align=left>
	17-06-26
	</th>
	<td align=right>2</td>
	<td align=right>669</td>
	<td align=right>0</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	17-07-03
	</th>
	<td align=right>59,007</td>
	<td align=right>5,868,094,561</td>
	<td align=right>1,653</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	17-07-10
	</th>
	<td align=right>28,716</td>
	<td align=right>3,053,355,946</td>
	<td align=right>969</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	17-07-17
	</th>
	<td align=right>21,797</td>
	<td align=right>2,297,764,306</td>
	<td align=right>917</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	17-07-24
	</th>
	<td align=right>21,865</td>
	<td align=right>2,135,943,599</td>
	<td align=right>861</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	17-07-31
	</th>
	<td align=right>16,988</td>
	<td align=right>1,683,673,008</td>
	<td align=right>779</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	17-08-07
	</th>
	<td align=right>27,078</td>
	<td align=right>3,253,871,628</td>
	<td align=right>1,140</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	17-08-14
	</th>
	<td align=right>29,984</td>
	<td align=right>4,217,089,068</td>
	<td align=right>1,373</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	17-08-21
	</th>
	<td align=right>26,078</td>
	<td align=right>3,732,135,568</td>
	<td align=right>2,219</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	17-08-28
	</th>
	<td align=right>42,289</td>
	<td align=right>7,108,906,072</td>
	<td align=right>1,871</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	17-09-04
	</th>
	<td align=right>45,880</td>
	<td align=right>8,299,582,552</td>
	<td align=right>2,050</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	17-09-11
	</th>
	<td align=right>53,485</td>
	<td align=right>9,479,680,633</td>
	<td align=right>2,242</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	17-09-18
	</th>
	<td align=right>62,413</td>
	<td align=right>10,061,748,126</td>
	<td align=right>2,639</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	17-09-25
	</th>
	<td align=right>67,200</td>
	<td align=right>12,643,967,078</td>
	<td align=right>2,602</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	17-10-02
	</th>
	<td align=right>42,059</td>
	<td align=right>7,769,003,966</td>
	<td align=right>1,716</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	17-10-09
	</th>
	<td align=right>47,356</td>
	<td align=right>9,370,477,171</td>
	<td align=right>2,027</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	17-10-16
	</th>
	<td align=right>53,602</td>
	<td align=right>10,768,536,834</td>
	<td align=right>2,232</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	17-10-23
	</th>
	<td align=right>47,201</td>
	<td align=right>10,169,653,012</td>
	<td align=right>2,004</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	17-10-30
	</th>
	<td align=right>31,181</td>
	<td align=right>5,430,254,416</td>
	<td align=right>1,381</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	17-11-06
	</th>
	<td align=right>36,238</td>
	<td align=right>7,018,114,824</td>
	<td align=right>1,527</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	17-11-13
	</th>
	<td align=right>31,608</td>
	<td align=right>6,748,081,075</td>
	<td align=right>1,403</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	17-11-20
	</th>
	<td align=right>37,327</td>
	<td align=right>8,721,298,767</td>
	<td align=right>1,606</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	17-11-27
	</th>
	<td align=right>30,223</td>
	<td align=right>5,627,324,049</td>
	<td align=right>1,426</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	17-12-04
	</th>
	<td align=right>35,877</td>
	<td align=right>6,572,674,348</td>
	<td align=right>1,730</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	17-12-11
	</th>
	<td align=right>38,523</td>
	<td align=right>7,237,007,680</td>
	<td align=right>1,919</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	17-12-18
	</th>
	<td align=right>25,984</td>
	<td align=right>4,133,516,249</td>
	<td align=right>1,259</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	17-12-25
	</th>
	<td align=right>16,280</td>
	<td align=right>2,671,461,626</td>
	<td align=right>770</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	18-01-01
	</th>
	<td align=right>17,982</td>
	<td align=right>3,417,297,654</td>
	<td align=right>784</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	18-01-08
	</th>
	<td align=right>24,162</td>
	<td align=right>5,414,379,508</td>
	<td align=right>960</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	18-01-15
	</th>
	<td align=right>37,727</td>
	<td align=right>6,365,605,995</td>
	<td align=right>1,766</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	18-01-22
	</th>
	<td align=right>39,534</td>
	<td align=right>8,009,526,128</td>
	<td align=right>1,670</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	18-01-29
	</th>
	<td align=right>39,730</td>
	<td align=right>7,004,645,019</td>
	<td align=right>1,666</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	18-02-05
	</th>
	<td align=right>38,344</td>
	<td align=right>6,819,583,526</td>
	<td align=right>2,001</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	18-02-12
	</th>
	<td align=right>31,535</td>
	<td align=right>5,440,618,642</td>
	<td align=right>1,684</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	18-02-19
	</th>
	<td align=right>30,888</td>
	<td align=right>4,695,336,993</td>
	<td align=right>1,555</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	18-02-26
	</th>
	<td align=right>31,718</td>
	<td align=right>4,976,928,175</td>
	<td align=right>1,568</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	18-03-05
	</th>
	<td align=right>59,697</td>
	<td align=right>7,893,710,972</td>
	<td align=right>2,222</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	18-03-12
	</th>
	<td align=right>52,672</td>
	<td align=right>11,034,982,476</td>
	<td align=right>2,384</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	18-03-19
	</th>
	<td align=right>36,843</td>
	<td align=right>7,839,792,725</td>
	<td align=right>1,675</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	18-03-26
	</th>
	<td align=right>34,809</td>
	<td align=right>5,507,498,261</td>
	<td align=right>1,798</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	18-04-02
	</th>
	<td align=right>32,647</td>
	<td align=right>5,543,263,074</td>
	<td align=right>1,285</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	18-04-09
	</th>
	<td align=right>29,850</td>
	<td align=right>5,723,567,800</td>
	<td align=right>1,685</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	18-04-16
	</th>
	<td align=right>41,672</td>
	<td align=right>8,886,212,800</td>
	<td align=right>2,207</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	18-04-23
	</th>
	<td align=right>35,969</td>
	<td align=right>7,765,064,344</td>
	<td align=right>1,576</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	18-04-30
	</th>
	<td align=right>43,538</td>
	<td align=right>8,092,662,912</td>
	<td align=right>1,762</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	18-05-07
	</th>
	<td align=right>27,463</td>
	<td align=right>6,043,399,569</td>
	<td align=right>1,232</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	18-05-14
	</th>
	<td align=right>31,705</td>
	<td align=right>6,897,882,055</td>
	<td align=right>1,699</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	18-05-21
	</th>
	<td align=right>45,950</td>
	<td align=right>7,406,453,245</td>
	<td align=right>2,088</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	18-05-28
	</th>
	<td align=right>30,187</td>
	<td align=right>6,178,414,092</td>
	<td align=right>1,814</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	18-06-04
	</th>
	<td align=right>26,768</td>
	<td align=right>5,965,808,136</td>
	<td align=right>1,596</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	18-06-11
	</th>
	<td align=right>41,897</td>
	<td align=right>8,380,908,513</td>
	<td align=right>2,565</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	18-06-18
	</th>
	<td align=right>55,903</td>
	<td align=right>7,361,366,211</td>
	<td align=right>3,559</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	18-06-25
	</th>
	<td align=right>32,958</td>
	<td align=right>6,240,255,359</td>
	<td align=right>1,939</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	18-07-02
	</th>
	<td align=right>25,035</td>
	<td align=right>5,589,661,982</td>
	<td align=right>1,612</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	18-07-09
	</th>
	<td align=right>23,184</td>
	<td align=right>4,500,610,214</td>
	<td align=right>1,546</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	18-07-16
	</th>
	<td align=right>19,041</td>
	<td align=right>4,635,485,887</td>
	<td align=right>1,354</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	18-07-23
	</th>
	<td align=right>24,249</td>
	<td align=right>5,575,423,011</td>
	<td align=right>1,152</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	18-07-30
	</th>
	<td align=right>19,314</td>
	<td align=right>4,341,351,093</td>
	<td align=right>1,556</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	18-08-06
	</th>
	<td align=right>32,056</td>
	<td align=right>7,436,242,557</td>
	<td align=right>2,980</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	18-08-13
	</th>
	<td align=right>25,974</td>
	<td align=right>7,414,315,002</td>
	<td align=right>1,351</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	18-08-20
	</th>
	<td align=right>32,115</td>
	<td align=right>8,452,773,003</td>
	<td align=right>1,576</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	18-08-27
	</th>
	<td align=right>26,100</td>
	<td align=right>7,142,419,342</td>
	<td align=right>1,800</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	18-09-03
	</th>
	<td align=right>27,352</td>
	<td align=right>6,201,785,625</td>
	<td align=right>1,667</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	18-09-10
	</th>
	<td align=right>30,285</td>
	<td align=right>7,603,145,284</td>
	<td align=right>1,660</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	18-09-17
	</th>
	<td align=right>32,138</td>
	<td align=right>6,237,604,590</td>
	<td align=right>1,515</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	18-09-24
	</th>
	<td align=right>30,318</td>
	<td align=right>6,491,897,762</td>
	<td align=right>1,655</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	18-10-01
	</th>
	<td align=right>30,219</td>
	<td align=right>6,758,291,246</td>
	<td align=right>2,057</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	18-10-08
	</th>
	<td align=right>28,611</td>
	<td align=right>6,269,509,295</td>
	<td align=right>1,502</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	18-10-15
	</th>
	<td align=right>36,131</td>
	<td align=right>7,751,958,446</td>
	<td align=right>1,685</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	18-10-22
	</th>
	<td align=right>30,046</td>
	<td align=right>7,874,332,673</td>
	<td align=right>1,731</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	18-10-29
	</th>
	<td align=right>26,807</td>
	<td align=right>5,925,324,519</td>
	<td align=right>1,450</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	18-11-05
	</th>
	<td align=right>32,683</td>
	<td align=right>7,939,454,943</td>
	<td align=right>1,733</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	18-11-12
	</th>
	<td align=right>32,456</td>
	<td align=right>8,375,011,691</td>
	<td align=right>1,761</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	18-11-19
	</th>
	<td align=right>38,723</td>
	<td align=right>8,391,990,483</td>
	<td align=right>1,882</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	18-11-26
	</th>
	<td align=right>38,086</td>
	<td align=right>9,045,040,600</td>
	<td align=right>1,623</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	18-12-03
	</th>
	<td align=right>32,849</td>
	<td align=right>9,764,602,042</td>
	<td align=right>1,542</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	18-12-10
	</th>
	<td align=right>45,425</td>
	<td align=right>10,956,490,181</td>
	<td align=right>1,681</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	18-12-17
	</th>
	<td align=right>46,579</td>
	<td align=right>10,335,959,567</td>
	<td align=right>1,597</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	18-12-24
	</th>
	<td align=right>20,945</td>
	<td align=right>5,521,315,263</td>
	<td align=right>1,039</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	18-12-31
	</th>
	<td align=right>20,779</td>
	<td align=right>5,852,759,145</td>
	<td align=right>1,104</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	19-01-07
	</th>
	<td align=right>42,525</td>
	<td align=right>17,942,047,883</td>
	<td align=right>1,680</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	19-01-14
	</th>
	<td align=right>28,943</td>
	<td align=right>9,445,598,612</td>
	<td align=right>1,395</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	19-01-21
	</th>
	<td align=right>37,851</td>
	<td align=right>13,286,007,398</td>
	<td align=right>1,833</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	19-01-28
	</th>
	<td align=right>46,318</td>
	<td align=right>12,645,892,692</td>
	<td align=right>1,857</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	19-02-04
	</th>
	<td align=right>38,191</td>
	<td align=right>10,010,486,552</td>
	<td align=right>1,803</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	19-02-11
	</th>
	<td align=right>29,602</td>
	<td align=right>8,421,595,445</td>
	<td align=right>1,566</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	19-02-18
	</th>
	<td align=right>30,910</td>
	<td align=right>8,250,102,688</td>
	<td align=right>1,582</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	19-02-25
	</th>
	<td align=right>25,775</td>
	<td align=right>8,198,923,520</td>
	<td align=right>1,544</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	19-03-04
	</th>
	<td align=right>34,421</td>
	<td align=right>10,356,470,470</td>
	<td align=right>1,926</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	19-03-11
	</th>
	<td align=right>34,152</td>
	<td align=right>11,031,869,512</td>
	<td align=right>1,842</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	19-03-18
	</th>
	<td align=right>34,216</td>
	<td align=right>10,425,422,345</td>
	<td align=right>2,092</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	19-03-25
	</th>
	<td align=right>43,017</td>
	<td align=right>10,929,027,415</td>
	<td align=right>2,459</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	19-04-01
	</th>
	<td align=right>40,962</td>
	<td align=right>12,708,139,500</td>
	<td align=right>2,237</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	19-04-08
	</th>
	<td align=right>75,603</td>
	<td align=right>17,804,151,612</td>
	<td align=right>3,392</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	19-04-15
	</th>
	<td align=right>29,872</td>
	<td align=right>10,124,568,648</td>
	<td align=right>1,750</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	19-04-22
	</th>
	<td align=right>50,252</td>
	<td align=right>15,698,079,368</td>
	<td align=right>2,533</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	19-04-29
	</th>
	<td align=right>38,299</td>
	<td align=right>12,033,123,011</td>
	<td align=right>2,009</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	19-05-06
	</th>
	<td align=right>52,297</td>
	<td align=right>15,047,151,360</td>
	<td align=right>2,976</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	19-05-13
	</th>
	<td align=right>55,421</td>
	<td align=right>17,942,337,266</td>
	<td align=right>2,416</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	19-05-20
	</th>
	<td align=right>67,286</td>
	<td align=right>14,438,568,392</td>
	<td align=right>2,501</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	19-05-27
	</th>
	<td align=right>103,502</td>
	<td align=right>21,962,697,732</td>
	<td align=right>3,317</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	19-06-03
	</th>
	<td align=right>232,852</td>
	<td align=right>57,394,956,060</td>
	<td align=right>6,412</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	19-06-10
	</th>
	<td align=right>157,756</td>
	<td align=right>23,667,311,892</td>
	<td align=right>5,174</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	19-06-17
	</th>
	<td align=right>71,605</td>
	<td align=right>10,510,638,090</td>
	<td align=right>2,972</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	19-06-24
	</th>
	<td align=right>66,899</td>
	<td align=right>10,661,523,832</td>
	<td align=right>3,209</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	19-07-01
	</th>
	<td align=right>77,939</td>
	<td align=right>12,977,180,731</td>
	<td align=right>3,334</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	19-07-08
	</th>
	<td align=right>57,081</td>
	<td align=right>9,994,283,303</td>
	<td align=right>2,707</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	19-07-15
	</th>
	<td align=right>46,772</td>
	<td align=right>9,073,685,699</td>
	<td align=right>2,368</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	<a target="_top" href="2019/07/22/index.php">
	19-07-22</a>
	
	</th>
	<td align=right>57,639</td>
	<td align=right>7,710,581,016</td>
	<td align=right>3,170</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	<a target="_top" href="2019/07/29/index.php">
	19-07-29</a>
	
	</th>
	<td align=right>49,442</td>
	<td align=right>8,226,288,648</td>
	<td align=right>2,395</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	<a target="_top" href="2019/08/05/index.php">
	19-08-05</a>
	
	</th>
	<td align=right>51,451</td>
	<td align=right>9,678,486,011</td>
	<td align=right>2,651</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	<a target="_top" href="2019/08/12/index.php">
	19-08-12</a>
	
	</th>
	<td align=right>49,499</td>
	<td align=right>8,541,444,553</td>
	<td align=right>2,292</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	<a target="_top" href="2019/08/19/index.php">
	19-08-19</a>
	
	</th>
	<td align=right>75,588</td>
	<td align=right>12,247,743,245</td>
	<td align=right>2,812</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	<a target="_top" href="2019/08/26/index.php">
	19-08-26</a>
	
	</th>
	<td align=right>57,717</td>
	<td align=right>9,733,376,988</td>
	<td align=right>2,591</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	<a target="_top" href="2019/09/02/index.php">
	19-09-02</a>
	
	</th>
	<td align=right>60,094</td>
	<td align=right>5,675,855,039</td>
	<td align=right>2,528</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	<a target="_top" href="2019/09/09/index.php">
	19-09-09</a>
	
	</th>
	<td align=right>85,194</td>
	<td align=right>5,271,660,153</td>
	<td align=right>3,023</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	<a target="_top" href="2019/09/16/index.php">
	19-09-16</a>
	
	</th>
	<td align=right>74,016</td>
	<td align=right>4,495,749,691</td>
	<td align=right>3,137</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	<a target="_top" href="2019/09/23/index.php">
	19-09-23</a>
	
	</th>
	<td align=right>81,159</td>
	<td align=right>4,756,381,316</td>
	<td align=right>3,435</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	<a target="_top" href="2019/09/30/index.php">
	19-09-30</a>
	
	</th>
	<td align=right>94,765</td>
	<td align=right>7,276,626,663</td>
	<td align=right>3,885</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	<a target="_top" href="2019/10/07/index.php">
	19-10-07</a>
	
	</th>
	<td align=right>84,100</td>
	<td align=right>6,418,445,767</td>
	<td align=right>4,075</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	<a target="_top" href="2019/10/14/index.php">
	19-10-14</a>
	
	</th>
	<td align=right>84,030</td>
	<td align=right>6,200,352,526</td>
	<td align=right>3,841</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	<a target="_top" href="2019/10/21/index.php">
	19-10-21</a>
	
	</th>
	<td align=right>76,378</td>
	<td align=right>5,355,061,485</td>
	<td align=right>3,771</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	<a target="_top" href="2019/10/28/index.php">
	19-10-28</a>
	
	</th>
	<td align=right>78,918</td>
	<td align=right>5,866,550,202</td>
	<td align=right>3,551</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	<a target="_top" href="2019/11/04/index.php">
	19-11-04</a>
	
	</th>
	<td align=right>77,106</td>
	<td align=right>5,569,479,716</td>
	<td align=right>3,886</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	<a target="_top" href="2019/11/11/index.php">
	19-11-11</a>
	
	</th>
	<td align=right>78,802</td>
	<td align=right>5,611,066,112</td>
	<td align=right>4,015</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	<a target="_top" href="2019/11/18/index.php">
	19-11-18</a>
	
	</th>
	<td align=right>81,914</td>
	<td align=right>5,118,796,076</td>
	<td align=right>3,946</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	<a target="_top" href="2019/11/25/index.php">
	19-11-25</a>
	
	</th>
	<td align=right>85,807</td>
	<td align=right>5,269,597,755</td>
	<td align=right>3,878</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	<a target="_top" href="2019/12/02/index.php">
	19-12-02</a>
	
	</th>
	<td align=right>90,418</td>
	<td align=right>4,865,617,693</td>
	<td align=right>4,380</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	<a target="_top" href="2019/12/09/index.php">
	19-12-09</a>
	
	</th>
	<td align=right>80,968</td>
	<td align=right>4,543,667,723</td>
	<td align=right>3,557</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	<a target="_top" href="2019/12/16/index.php">
	19-12-16</a>
	
	</th>
	<td align=right>71,133</td>
	<td align=right>3,212,680,807</td>
	<td align=right>3,116</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	<a target="_top" href="2019/12/23/index.php">
	19-12-23</a>
	
	</th>
	<td align=right>64,416</td>
	<td align=right>2,977,846,984</td>
	<td align=right>2,782</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	<a target="_top" href="2019/12/30/index.php">
	19-12-30</a>
	
	</th>
	<td align=right>63,111</td>
	<td align=right>3,253,375,945</td>
	<td align=right>2,667</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	<a target="_top" href="2020/01/06/index.php">
	20-01-06</a>
	
	</th>
	<td align=right>73,136</td>
	<td align=right>4,279,611,920</td>
	<td align=right>3,382</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	<a target="_top" href="2020/01/13/index.php">
	20-01-13</a>
	
	</th>
	<td align=right>76,288</td>
	<td align=right>4,272,656,770</td>
	<td align=right>3,136</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	<a target="_top" href="2020/01/20/index.php">
	20-01-20</a>
	
	</th>
	<td align=right>0.00</td>
	<td align=right>0.00</td>
	<td align=right>0.00</td>
	
	</tr>

	</tbody>

	</table>
	