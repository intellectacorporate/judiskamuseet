


<h2>Statistics for http://www.u5645470.fsdata.se, Week of 2017-6-26 to 2020-1-26</h2>

	<h3>History from 2017-6-26 to 2020-1-26</h3>
	
	<p><img src="3t.png" width="468" height="351"></p>
	<table cellspacing='0' class='styladtabell'>
	<caption>Home Page Accesses</caption>
	<thead>
	<tr>
	<th scope="col" class='centrerad'>Report Date</th>
	<th scope="col" class='centrerad'>Accesses</th>
	<th scope="col" class='centrerad'>Bytes</th>
	<th scope="col" class='centrerad'>Visits</th>
	
	</tr>
        </thead>
	<tbody>
	<tr class="jamn">
	<th align="left">Total</th>
	<td align="right">914,523</td>
	<td align="right">7,095,490,179</td>
	<td align="right">153,671</td>	
	
	</tr>
	<tr class="udda">
	<th align="left">Average</th>
	<td align="right">6,774</td>
	<td align="right">52,559,186</td>
	<td align="right">1,138</td>
	
	</tr>
	<tr class="jamn">
	<th align="left">Latest</th>
	<td align="right">0.00</td>
	<td align="right">0</td>
	<td align="right">0</td>
	
	</tr>
	<tr class="jamnudda">
	<th align=left>
	17-06-26
	</th>
	<td align=right>2</td>
	<td align=right>669</td>
	<td align=right>0</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	17-07-03
	</th>
	<td align=right>3,894</td>
	<td align=right>28,771,974</td>
	<td align=right>767</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	17-07-10
	</th>
	<td align=right>5,281</td>
	<td align=right>31,838,444</td>
	<td align=right>732</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	17-07-17
	</th>
	<td align=right>3,968</td>
	<td align=right>27,426,474</td>
	<td align=right>673</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	17-07-24
	</th>
	<td align=right>3,621</td>
	<td align=right>28,719,400</td>
	<td align=right>619</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	17-07-31
	</th>
	<td align=right>3,547</td>
	<td align=right>23,547,808</td>
	<td align=right>540</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	17-08-07
	</th>
	<td align=right>4,069</td>
	<td align=right>29,671,286</td>
	<td align=right>792</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	17-08-14
	</th>
	<td align=right>4,374</td>
	<td align=right>28,915,928</td>
	<td align=right>1,059</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	17-08-21
	</th>
	<td align=right>5,024</td>
	<td align=right>37,602,857</td>
	<td align=right>1,029</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	17-08-28
	</th>
	<td align=right>5,273</td>
	<td align=right>57,380,852</td>
	<td align=right>1,661</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	17-09-04
	</th>
	<td align=right>5,569</td>
	<td align=right>65,732,821</td>
	<td align=right>1,774</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	17-09-11
	</th>
	<td align=right>5,360</td>
	<td align=right>58,543,674</td>
	<td align=right>1,813</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	17-09-18
	</th>
	<td align=right>5,668</td>
	<td align=right>66,676,871</td>
	<td align=right>2,105</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	17-09-25
	</th>
	<td align=right>5,951</td>
	<td align=right>58,715,697</td>
	<td align=right>2,154</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	17-10-02
	</th>
	<td align=right>4,548</td>
	<td align=right>20,090,949</td>
	<td align=right>1,353</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	17-10-09
	</th>
	<td align=right>4,768</td>
	<td align=right>20,480,608</td>
	<td align=right>1,613</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	17-10-16
	</th>
	<td align=right>4,851</td>
	<td align=right>19,080,731</td>
	<td align=right>1,645</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	17-10-23
	</th>
	<td align=right>4,676</td>
	<td align=right>19,287,351</td>
	<td align=right>1,428</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	17-10-30
	</th>
	<td align=right>3,742</td>
	<td align=right>18,888,077</td>
	<td align=right>916</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	17-11-06
	</th>
	<td align=right>4,057</td>
	<td align=right>20,712,000</td>
	<td align=right>1,108</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	17-11-13
	</th>
	<td align=right>3,940</td>
	<td align=right>18,724,204</td>
	<td align=right>1,039</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	17-11-20
	</th>
	<td align=right>4,231</td>
	<td align=right>23,776,427</td>
	<td align=right>1,262</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	17-11-27
	</th>
	<td align=right>4,105</td>
	<td align=right>20,731,236</td>
	<td align=right>1,020</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	17-12-04
	</th>
	<td align=right>4,152</td>
	<td align=right>20,156,316</td>
	<td align=right>1,192</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	17-12-11
	</th>
	<td align=right>4,280</td>
	<td align=right>21,785,046</td>
	<td align=right>1,279</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	17-12-18
	</th>
	<td align=right>3,728</td>
	<td align=right>20,503,926</td>
	<td align=right>782</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	17-12-25
	</th>
	<td align=right>3,272</td>
	<td align=right>17,266,862</td>
	<td align=right>480</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	18-01-01
	</th>
	<td align=right>3,256</td>
	<td align=right>17,267,032</td>
	<td align=right>531</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	18-01-08
	</th>
	<td align=right>3,683</td>
	<td align=right>19,671,214</td>
	<td align=right>732</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	18-01-15
	</th>
	<td align=right>4,066</td>
	<td align=right>15,310,474</td>
	<td align=right>1,152</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	18-01-22
	</th>
	<td align=right>4,385</td>
	<td align=right>22,378,040</td>
	<td align=right>1,174</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	18-01-29
	</th>
	<td align=right>5,272</td>
	<td align=right>38,650,238</td>
	<td align=right>1,298</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	18-02-05
	</th>
	<td align=right>5,464</td>
	<td align=right>42,599,703</td>
	<td align=right>1,392</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	18-02-12
	</th>
	<td align=right>4,999</td>
	<td align=right>36,283,000</td>
	<td align=right>1,154</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	18-02-19
	</th>
	<td align=right>4,862</td>
	<td align=right>29,404,263</td>
	<td align=right>1,050</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	18-02-26
	</th>
	<td align=right>4,565</td>
	<td align=right>25,765,246</td>
	<td align=right>977</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	18-03-05
	</th>
	<td align=right>5,668</td>
	<td align=right>31,373,233</td>
	<td align=right>1,646</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	18-03-12
	</th>
	<td align=right>5,836</td>
	<td align=right>30,275,766</td>
	<td align=right>1,672</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	18-03-19
	</th>
	<td align=right>4,712</td>
	<td align=right>25,342,160</td>
	<td align=right>1,139</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	18-03-26
	</th>
	<td align=right>4,330</td>
	<td align=right>16,726,611</td>
	<td align=right>888</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	18-04-02
	</th>
	<td align=right>4,208</td>
	<td align=right>16,447,002</td>
	<td align=right>820</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	18-04-09
	</th>
	<td align=right>3,796</td>
	<td align=right>14,157,430</td>
	<td align=right>869</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	18-04-16
	</th>
	<td align=right>3,888</td>
	<td align=right>18,350,340</td>
	<td align=right>1,138</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	18-04-23
	</th>
	<td align=right>3,886</td>
	<td align=right>18,902,697</td>
	<td align=right>1,066</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	18-04-30
	</th>
	<td align=right>4,036</td>
	<td align=right>17,983,581</td>
	<td align=right>1,129</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	18-05-07
	</th>
	<td align=right>3,951</td>
	<td align=right>22,674,360</td>
	<td align=right>804</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	18-05-14
	</th>
	<td align=right>4,220</td>
	<td align=right>43,234,268</td>
	<td align=right>1,172</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	18-05-21
	</th>
	<td align=right>4,682</td>
	<td align=right>60,729,547</td>
	<td align=right>1,404</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	18-05-28
	</th>
	<td align=right>4,403</td>
	<td align=right>52,357,689</td>
	<td align=right>1,247</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	18-06-04
	</th>
	<td align=right>3,947</td>
	<td align=right>42,698,492</td>
	<td align=right>1,047</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	18-06-11
	</th>
	<td align=right>4,359</td>
	<td align=right>39,930,320</td>
	<td align=right>1,469</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	18-06-18
	</th>
	<td align=right>4,849</td>
	<td align=right>41,911,957</td>
	<td align=right>2,187</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	18-06-25
	</th>
	<td align=right>4,020</td>
	<td align=right>38,036,019</td>
	<td align=right>1,176</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	18-07-02
	</th>
	<td align=right>3,863</td>
	<td align=right>43,833,267</td>
	<td align=right>1,017</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	18-07-09
	</th>
	<td align=right>3,409</td>
	<td align=right>34,852,860</td>
	<td align=right>971</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	18-07-16
	</th>
	<td align=right>3,412</td>
	<td align=right>35,735,327</td>
	<td align=right>874</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	18-07-23
	</th>
	<td align=right>3,796</td>
	<td align=right>30,457,723</td>
	<td align=right>696</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	18-07-30
	</th>
	<td align=right>3,798</td>
	<td align=right>45,957,071</td>
	<td align=right>1,035</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	18-08-06
	</th>
	<td align=right>5,132</td>
	<td align=right>126,443,357</td>
	<td align=right>2,345</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	18-08-13
	</th>
	<td align=right>3,605</td>
	<td align=right>26,730,178</td>
	<td align=right>800</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	18-08-20
	</th>
	<td align=right>4,192</td>
	<td align=right>34,091,347</td>
	<td align=right>937</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	18-08-27
	</th>
	<td align=right>4,027</td>
	<td align=right>38,871,782</td>
	<td align=right>1,024</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	18-09-03
	</th>
	<td align=right>3,929</td>
	<td align=right>31,317,028</td>
	<td align=right>995</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	18-09-10
	</th>
	<td align=right>4,272</td>
	<td align=right>34,164,915</td>
	<td align=right>1,056</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	18-09-17
	</th>
	<td align=right>4,066</td>
	<td align=right>36,008,214</td>
	<td align=right>1,027</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	18-09-24
	</th>
	<td align=right>4,054</td>
	<td align=right>36,198,576</td>
	<td align=right>1,087</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	18-10-01
	</th>
	<td align=right>4,436</td>
	<td align=right>64,269,705</td>
	<td align=right>1,525</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	18-10-08
	</th>
	<td align=right>3,783</td>
	<td align=right>33,591,974</td>
	<td align=right>1,053</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	18-10-15
	</th>
	<td align=right>4,822</td>
	<td align=right>29,152,949</td>
	<td align=right>1,119</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	18-10-22
	</th>
	<td align=right>4,159</td>
	<td align=right>33,934,942</td>
	<td align=right>1,122</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	18-10-29
	</th>
	<td align=right>3,608</td>
	<td align=right>34,135,349</td>
	<td align=right>942</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	18-11-05
	</th>
	<td align=right>4,066</td>
	<td align=right>34,014,732</td>
	<td align=right>1,151</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	18-11-12
	</th>
	<td align=right>3,634</td>
	<td align=right>26,785,515</td>
	<td align=right>1,060</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	18-11-19
	</th>
	<td align=right>4,777</td>
	<td align=right>32,656,616</td>
	<td align=right>961</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	18-11-26
	</th>
	<td align=right>4,336</td>
	<td align=right>30,401,747</td>
	<td align=right>1,045</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	18-12-03
	</th>
	<td align=right>4,555</td>
	<td align=right>31,012,397</td>
	<td align=right>960</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	18-12-10
	</th>
	<td align=right>5,496</td>
	<td align=right>44,512,527</td>
	<td align=right>1,068</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	18-12-17
	</th>
	<td align=right>4,168</td>
	<td align=right>34,239,978</td>
	<td align=right>825</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	18-12-24
	</th>
	<td align=right>3,466</td>
	<td align=right>19,446,274</td>
	<td align=right>570</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	18-12-31
	</th>
	<td align=right>3,180</td>
	<td align=right>19,116,455</td>
	<td align=right>553</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	19-01-07
	</th>
	<td align=right>3,856</td>
	<td align=right>28,903,097</td>
	<td align=right>1,027</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	19-01-14
	</th>
	<td align=right>3,488</td>
	<td align=right>24,945,936</td>
	<td align=right>849</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	19-01-21
	</th>
	<td align=right>4,069</td>
	<td align=right>27,616,178</td>
	<td align=right>1,136</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	19-01-28
	</th>
	<td align=right>4,507</td>
	<td align=right>34,137,769</td>
	<td align=right>1,148</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	19-02-04
	</th>
	<td align=right>4,456</td>
	<td align=right>25,977,496</td>
	<td align=right>1,156</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	19-02-11
	</th>
	<td align=right>4,474</td>
	<td align=right>23,403,494</td>
	<td align=right>976</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	19-02-18
	</th>
	<td align=right>4,319</td>
	<td align=right>24,058,996</td>
	<td align=right>931</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	19-02-25
	</th>
	<td align=right>3,992</td>
	<td align=right>25,991,076</td>
	<td align=right>828</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	19-03-04
	</th>
	<td align=right>4,241</td>
	<td align=right>23,835,863</td>
	<td align=right>1,046</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	19-03-11
	</th>
	<td align=right>4,397</td>
	<td align=right>25,807,170</td>
	<td align=right>1,100</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	19-03-18
	</th>
	<td align=right>3,958</td>
	<td align=right>24,917,039</td>
	<td align=right>1,088</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	19-03-25
	</th>
	<td align=right>3,856</td>
	<td align=right>20,358,258</td>
	<td align=right>1,552</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	19-04-01
	</th>
	<td align=right>4,319</td>
	<td align=right>25,523,548</td>
	<td align=right>1,419</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	19-04-08
	</th>
	<td align=right>5,935</td>
	<td align=right>33,861,654</td>
	<td align=right>2,416</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	19-04-15
	</th>
	<td align=right>4,188</td>
	<td align=right>23,679,356</td>
	<td align=right>952</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	19-04-22
	</th>
	<td align=right>5,238</td>
	<td align=right>29,377,333</td>
	<td align=right>1,612</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	19-04-29
	</th>
	<td align=right>4,652</td>
	<td align=right>26,128,839</td>
	<td align=right>1,296</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	19-05-06
	</th>
	<td align=right>5,097</td>
	<td align=right>30,939,529</td>
	<td align=right>1,758</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	19-05-13
	</th>
	<td align=right>4,056</td>
	<td align=right>60,105,799</td>
	<td align=right>1,323</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	19-05-20
	</th>
	<td align=right>4,718</td>
	<td align=right>48,328,242</td>
	<td align=right>1,196</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	19-05-27
	</th>
	<td align=right>6,730</td>
	<td align=right>48,067,004</td>
	<td align=right>1,985</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	19-06-03
	</th>
	<td align=right>10,769</td>
	<td align=right>90,363,458</td>
	<td align=right>3,688</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	19-06-10
	</th>
	<td align=right>6,743</td>
	<td align=right>64,082,069</td>
	<td align=right>2,023</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	19-06-17
	</th>
	<td align=right>5,129</td>
	<td align=right>43,436,696</td>
	<td align=right>1,184</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	19-06-24
	</th>
	<td align=right>5,916</td>
	<td align=right>50,209,976</td>
	<td align=right>1,266</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	19-07-01
	</th>
	<td align=right>6,809</td>
	<td align=right>53,041,129</td>
	<td align=right>1,461</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	19-07-08
	</th>
	<td align=right>6,114</td>
	<td align=right>46,030,370</td>
	<td align=right>1,119</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	19-07-15
	</th>
	<td align=right>5,702</td>
	<td align=right>40,447,559</td>
	<td align=right>1,077</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	<a target="_top" href="2019/07/22/index.php">
	19-07-22</a>
	
	</th>
	<td align=right>5,269</td>
	<td align=right>37,668,108</td>
	<td align=right>861</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	<a target="_top" href="2019/07/29/index.php">
	19-07-29</a>
	
	</th>
	<td align=right>5,233</td>
	<td align=right>39,984,473</td>
	<td align=right>932</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	<a target="_top" href="2019/08/05/index.php">
	19-08-05</a>
	
	</th>
	<td align=right>5,633</td>
	<td align=right>43,633,837</td>
	<td align=right>1,039</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	<a target="_top" href="2019/08/12/index.php">
	19-08-12</a>
	
	</th>
	<td align=right>5,277</td>
	<td align=right>39,061,894</td>
	<td align=right>913</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	<a target="_top" href="2019/08/19/index.php">
	19-08-19</a>
	
	</th>
	<td align=right>5,364</td>
	<td align=right>41,599,767</td>
	<td align=right>855</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	<a target="_top" href="2019/08/26/index.php">
	19-08-26</a>
	
	</th>
	<td align=right>5,639</td>
	<td align=right>49,006,574</td>
	<td align=right>826</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	<a target="_top" href="2019/09/02/index.php">
	19-09-02</a>
	
	</th>
	<td align=right>5,386</td>
	<td align=right>47,776,990</td>
	<td align=right>750</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	<a target="_top" href="2019/09/09/index.php">
	19-09-09</a>
	
	</th>
	<td align=right>5,088</td>
	<td align=right>41,017,573</td>
	<td align=right>815</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	<a target="_top" href="2019/09/16/index.php">
	19-09-16</a>
	
	</th>
	<td align=right>5,107</td>
	<td align=right>41,358,850</td>
	<td align=right>811</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	<a target="_top" href="2019/09/23/index.php">
	19-09-23</a>
	
	</th>
	<td align=right>5,869</td>
	<td align=right>51,645,458</td>
	<td align=right>929</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	<a target="_top" href="2019/09/30/index.php">
	19-09-30</a>
	
	</th>
	<td align=right>19,788</td>
	<td align=right>161,415,769</td>
	<td align=right>1,066</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	<a target="_top" href="2019/10/07/index.php">
	19-10-07</a>
	
	</th>
	<td align=right>24,207</td>
	<td align=right>198,530,640</td>
	<td align=right>1,384</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	<a target="_top" href="2019/10/14/index.php">
	19-10-14</a>
	
	</th>
	<td align=right>23,876</td>
	<td align=right>196,503,890</td>
	<td align=right>1,323</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	<a target="_top" href="2019/10/21/index.php">
	19-10-21</a>
	
	</th>
	<td align=right>23,299</td>
	<td align=right>194,018,779</td>
	<td align=right>1,127</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	<a target="_top" href="2019/10/28/index.php">
	19-10-28</a>
	
	</th>
	<td align=right>23,718</td>
	<td align=right>197,752,733</td>
	<td align=right>1,209</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	<a target="_top" href="2019/11/04/index.php">
	19-11-04</a>
	
	</th>
	<td align=right>23,274</td>
	<td align=right>192,076,593</td>
	<td align=right>1,112</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	<a target="_top" href="2019/11/11/index.php">
	19-11-11</a>
	
	</th>
	<td align=right>23,355</td>
	<td align=right>197,946,180</td>
	<td align=right>1,044</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	<a target="_top" href="2019/11/18/index.php">
	19-11-18</a>
	
	</th>
	<td align=right>24,407</td>
	<td align=right>211,504,064</td>
	<td align=right>1,176</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	<a target="_top" href="2019/11/25/index.php">
	19-11-25</a>
	
	</th>
	<td align=right>24,596</td>
	<td align=right>207,433,187</td>
	<td align=right>1,181</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	<a target="_top" href="2019/12/02/index.php">
	19-12-02</a>
	
	</th>
	<td align=right>25,283</td>
	<td align=right>205,825,123</td>
	<td align=right>1,182</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	<a target="_top" href="2019/12/09/index.php">
	19-12-09</a>
	
	</th>
	<td align=right>24,746</td>
	<td align=right>195,734,719</td>
	<td align=right>1,072</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	<a target="_top" href="2019/12/16/index.php">
	19-12-16</a>
	
	</th>
	<td align=right>23,727</td>
	<td align=right>175,311,168</td>
	<td align=right>867</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	<a target="_top" href="2019/12/23/index.php">
	19-12-23</a>
	
	</th>
	<td align=right>23,521</td>
	<td align=right>173,454,081</td>
	<td align=right>788</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	<a target="_top" href="2019/12/30/index.php">
	19-12-30</a>
	
	</th>
	<td align=right>23,157</td>
	<td align=right>172,446,697</td>
	<td align=right>844</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	<a target="_top" href="2020/01/06/index.php">
	20-01-06</a>
	
	</th>
	<td align=right>23,281</td>
	<td align=right>177,089,900</td>
	<td align=right>955</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	<a target="_top" href="2020/01/13/index.php">
	20-01-13</a>
	
	</th>
	<td align=right>23,360</td>
	<td align=right>176,696,224</td>
	<td align=right>946</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	<a target="_top" href="2020/01/20/index.php">
	20-01-20</a>
	
	</th>
	<td align=right>0.00</td>
	<td align=right>0.00</td>
	<td align=right>0.00</td>
	
	</tr>

	</tbody>

	</table>
	