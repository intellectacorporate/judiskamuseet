


<h2>Statistics for http://www.u5645470.fsdata.se, Week of 2017-6-26 to 2020-1-26</h2>

	<h3>History from 2017-6-26 to 2020-1-26</h3>
	
	<p><img src="5t.png" width="468" height="351"></p>
	<table cellspacing='0' class='styladtabell'>
	<caption>Unique Visitors (Best Method)</caption>
	<thead>
	<tr>
	<th scope="col" class='centrerad'>Report Date</th>
	<th scope="col" class='centrerad'>Accesses</th>
	
	</tr>
        </thead>
	<tbody>
	<tr class="jamn">
	<th align="left">Total</th>
	<td align="right">216,848</td>
	
	</tr>
	<tr class="udda">
	<th align="left">Average</th>
	<td align="right">1,606</td>
	
	</tr>
	<tr class="jamn">
	<th align="left">Latest</th>
	<td align="right">0.00</td>
	
	</tr>
	<tr class="jamnudda">
	<th align=left>
	17-07-03
	</th>
	<td align=right>1,328</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	17-07-10
	</th>
	<td align=right>918</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	17-07-17
	</th>
	<td align=right>818</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	17-07-24
	</th>
	<td align=right>779</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	17-07-31
	</th>
	<td align=right>703</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	17-08-07
	</th>
	<td align=right>992</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	17-08-14
	</th>
	<td align=right>1,157</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	17-08-21
	</th>
	<td align=right>2,007</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	17-08-28
	</th>
	<td align=right>1,572</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	17-09-04
	</th>
	<td align=right>1,718</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	17-09-11
	</th>
	<td align=right>1,688</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	17-09-18
	</th>
	<td align=right>2,011</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	17-09-25
	</th>
	<td align=right>2,012</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	17-10-02
	</th>
	<td align=right>1,305</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	17-10-09
	</th>
	<td align=right>1,591</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	17-10-16
	</th>
	<td align=right>1,642</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	17-10-23
	</th>
	<td align=right>1,349</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	17-10-30
	</th>
	<td align=right>1,150</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	17-11-06
	</th>
	<td align=right>1,136</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	17-11-13
	</th>
	<td align=right>1,053</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	17-11-20
	</th>
	<td align=right>1,241</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	17-11-27
	</th>
	<td align=right>1,089</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	17-12-04
	</th>
	<td align=right>1,453</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	17-12-11
	</th>
	<td align=right>1,590</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	17-12-18
	</th>
	<td align=right>928</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	17-12-25
	</th>
	<td align=right>579</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	18-01-01
	</th>
	<td align=right>617</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	18-01-08
	</th>
	<td align=right>775</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	18-01-15
	</th>
	<td align=right>1,450</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	18-01-22
	</th>
	<td align=right>1,325</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	18-01-29
	</th>
	<td align=right>1,207</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	18-02-05
	</th>
	<td align=right>1,440</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	18-02-12
	</th>
	<td align=right>1,219</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	18-02-19
	</th>
	<td align=right>1,187</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	18-02-26
	</th>
	<td align=right>1,232</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	18-03-05
	</th>
	<td align=right>1,624</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	18-03-12
	</th>
	<td align=right>1,678</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	18-03-19
	</th>
	<td align=right>1,152</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	18-03-26
	</th>
	<td align=right>1,156</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	18-04-02
	</th>
	<td align=right>988</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	18-04-09
	</th>
	<td align=right>1,183</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	18-04-16
	</th>
	<td align=right>1,422</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	18-04-23
	</th>
	<td align=right>1,177</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	18-04-30
	</th>
	<td align=right>1,271</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	18-05-07
	</th>
	<td align=right>928</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	18-05-14
	</th>
	<td align=right>1,272</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	18-05-21
	</th>
	<td align=right>1,544</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	18-05-28
	</th>
	<td align=right>1,403</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	18-06-04
	</th>
	<td align=right>1,261</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	18-06-11
	</th>
	<td align=right>1,857</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	18-06-18
	</th>
	<td align=right>2,843</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	18-06-25
	</th>
	<td align=right>1,414</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	18-07-02
	</th>
	<td align=right>1,203</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	18-07-09
	</th>
	<td align=right>1,226</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	18-07-16
	</th>
	<td align=right>1,054</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	18-07-23
	</th>
	<td align=right>831</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	18-07-30
	</th>
	<td align=right>1,209</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	18-08-06
	</th>
	<td align=right>2,570</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	18-08-13
	</th>
	<td align=right>991</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	18-08-20
	</th>
	<td align=right>1,021</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	18-08-27
	</th>
	<td align=right>1,197</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	18-09-03
	</th>
	<td align=right>1,043</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	18-09-10
	</th>
	<td align=right>1,015</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	18-09-17
	</th>
	<td align=right>1,102</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	18-09-24
	</th>
	<td align=right>1,177</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	18-10-01
	</th>
	<td align=right>1,640</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	18-10-08
	</th>
	<td align=right>1,145</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	18-10-15
	</th>
	<td align=right>1,235</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	18-10-22
	</th>
	<td align=right>1,215</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	18-10-29
	</th>
	<td align=right>1,055</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	18-11-05
	</th>
	<td align=right>1,233</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	18-11-12
	</th>
	<td align=right>1,301</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	18-11-19
	</th>
	<td align=right>1,046</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	18-11-26
	</th>
	<td align=right>1,141</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	18-12-03
	</th>
	<td align=right>1,049</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	18-12-10
	</th>
	<td align=right>1,193</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	18-12-17
	</th>
	<td align=right>1,042</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	18-12-24
	</th>
	<td align=right>736</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	18-12-31
	</th>
	<td align=right>763</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	19-01-07
	</th>
	<td align=right>1,254</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	19-01-14
	</th>
	<td align=right>1,016</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	19-01-21
	</th>
	<td align=right>1,302</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	19-01-28
	</th>
	<td align=right>1,344</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	19-02-04
	</th>
	<td align=right>1,299</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	19-02-11
	</th>
	<td align=right>1,140</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	19-02-18
	</th>
	<td align=right>1,141</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	19-02-25
	</th>
	<td align=right>957</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	19-03-04
	</th>
	<td align=right>1,168</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	19-03-11
	</th>
	<td align=right>1,213</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	19-03-18
	</th>
	<td align=right>1,411</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	19-03-25
	</th>
	<td align=right>1,856</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	19-04-01
	</th>
	<td align=right>1,427</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	19-04-08
	</th>
	<td align=right>2,603</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	19-04-15
	</th>
	<td align=right>1,215</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	19-04-22
	</th>
	<td align=right>1,814</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	19-04-29
	</th>
	<td align=right>1,383</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	19-05-06
	</th>
	<td align=right>2,065</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	19-05-13
	</th>
	<td align=right>1,658</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	19-05-20
	</th>
	<td align=right>1,723</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	19-05-27
	</th>
	<td align=right>2,373</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	19-06-03
	</th>
	<td align=right>4,596</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	19-06-10
	</th>
	<td align=right>3,434</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	19-06-17
	</th>
	<td align=right>1,906</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	19-06-24
	</th>
	<td align=right>2,011</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	19-07-01
	</th>
	<td align=right>2,178</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	19-07-08
	</th>
	<td align=right>1,830</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	19-07-15
	</th>
	<td align=right>1,557</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	<a target="_top" href="2019/07/22/index.php">
	19-07-22</a>
	
	</th>
	<td align=right>2,357</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	<a target="_top" href="2019/07/29/index.php">
	19-07-29</a>
	
	</th>
	<td align=right>1,656</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	<a target="_top" href="2019/08/05/index.php">
	19-08-05</a>
	
	</th>
	<td align=right>1,856</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	<a target="_top" href="2019/08/12/index.php">
	19-08-12</a>
	
	</th>
	<td align=right>1,653</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	<a target="_top" href="2019/08/19/index.php">
	19-08-19</a>
	
	</th>
	<td align=right>1,996</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	<a target="_top" href="2019/08/26/index.php">
	19-08-26</a>
	
	</th>
	<td align=right>1,653</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	<a target="_top" href="2019/09/02/index.php">
	19-09-02</a>
	
	</th>
	<td align=right>1,681</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	<a target="_top" href="2019/09/09/index.php">
	19-09-09</a>
	
	</th>
	<td align=right>2,046</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	<a target="_top" href="2019/09/16/index.php">
	19-09-16</a>
	
	</th>
	<td align=right>2,222</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	<a target="_top" href="2019/09/23/index.php">
	19-09-23</a>
	
	</th>
	<td align=right>2,427</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	<a target="_top" href="2019/09/30/index.php">
	19-09-30</a>
	
	</th>
	<td align=right>3,325</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	<a target="_top" href="2019/10/07/index.php">
	19-10-07</a>
	
	</th>
	<td align=right>3,652</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	<a target="_top" href="2019/10/14/index.php">
	19-10-14</a>
	
	</th>
	<td align=right>3,208</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	<a target="_top" href="2019/10/21/index.php">
	19-10-21</a>
	
	</th>
	<td align=right>3,416</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	<a target="_top" href="2019/10/28/index.php">
	19-10-28</a>
	
	</th>
	<td align=right>3,364</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	<a target="_top" href="2019/11/04/index.php">
	19-11-04</a>
	
	</th>
	<td align=right>3,452</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	<a target="_top" href="2019/11/11/index.php">
	19-11-11</a>
	
	</th>
	<td align=right>3,639</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	<a target="_top" href="2019/11/18/index.php">
	19-11-18</a>
	
	</th>
	<td align=right>3,161</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	<a target="_top" href="2019/11/25/index.php">
	19-11-25</a>
	
	</th>
	<td align=right>2,863</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	<a target="_top" href="2019/12/02/index.php">
	19-12-02</a>
	
	</th>
	<td align=right>3,702</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	<a target="_top" href="2019/12/09/index.php">
	19-12-09</a>
	
	</th>
	<td align=right>3,052</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	<a target="_top" href="2019/12/16/index.php">
	19-12-16</a>
	
	</th>
	<td align=right>2,668</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	<a target="_top" href="2019/12/23/index.php">
	19-12-23</a>
	
	</th>
	<td align=right>2,137</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	<a target="_top" href="2019/12/30/index.php">
	19-12-30</a>
	
	</th>
	<td align=right>1,882</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	<a target="_top" href="2020/01/06/index.php">
	20-01-06</a>
	
	</th>
	<td align=right>2,343</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	<a target="_top" href="2020/01/13/index.php">
	20-01-13</a>
	
	</th>
	<td align=right>2,632</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	<a target="_top" href="2020/01/20/index.php">
	20-01-20</a>
	
	</th>
	<td align=right>0.00</td>
	
	</tr>

	</tbody>

	</table>
	