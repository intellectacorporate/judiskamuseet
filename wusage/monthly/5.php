


<h2>Statistics for http://www.u5645470.fsdata.se, Month of 2017-6-1 to 2020-1-31</h2>

	<h3>History from 2017-6-1 to 2020-1-31</h3>
	<p>Values for completed time periods are final. Values for the 
	reporting period currently in progress are projections, except
	for user-calculated functions, which are not scaled.</p>
	
	<p><img src="5t.png" width="468" height="351"></p>
	<table cellspacing='0' class='styladtabell'>
	<caption>Unique Visitors (Best Method)</caption>
	<thead>
	<tr>
	<th scope="col" class='centrerad'>Report Date</th>
	<th scope="col" class='centrerad'>Accesses</th>
	
	</tr>
        </thead>
	<tbody>
	<tr class="jamn">
	<th align="left">Total</th>
	<td align="right">186,916</td>
	
	</tr>
	<tr class="udda">
	<th align="left">Average</th>
	<td align="right">5,751</td>
	
	</tr>
	<tr class="jamn">
	<th align="left">Latest</th>
	<td align="right">8,189.45</td>
	
	</tr>
	<tr class="udda">
	<th align=left>
	17-07-01
	</th>
	<td align=right>3,688</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	17-08-01
	</th>
	<td align=right>5,102</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	17-09-01
	</th>
	<td align=right>7,269</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	17-10-01
	</th>
	<td align=right>5,454</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	17-11-01
	</th>
	<td align=right>4,212</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	17-12-01
	</th>
	<td align=right>4,481</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	18-01-01
	</th>
	<td align=right>4,200</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	18-02-01
	</th>
	<td align=right>4,323</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	18-03-01
	</th>
	<td align=right>4,972</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	18-04-01
	</th>
	<td align=right>4,498</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	18-05-01
	</th>
	<td align=right>4,864</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	18-06-01
	</th>
	<td align=right>7,228</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	18-07-01
	</th>
	<td align=right>4,413</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	<a target="_top" href="2018/08/01/index.php">
	18-08-01</a>
	
	</th>
	<td align=right>5,968</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	<a target="_top" href="2018/09/01/index.php">
	18-09-01</a>
	
	</th>
	<td align=right>4,110</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	<a target="_top" href="2018/10/01/index.php">
	18-10-01</a>
	
	</th>
	<td align=right>5,215</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	<a target="_top" href="2018/11/01/index.php">
	18-11-01</a>
	
	</th>
	<td align=right>4,348</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	<a target="_top" href="2018/12/01/index.php">
	18-12-01</a>
	
	</th>
	<td align=right>3,894</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	<a target="_top" href="2019/01/01/index.php">
	19-01-01</a>
	
	</th>
	<td align=right>4,420</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	<a target="_top" href="2019/02/01/index.php">
	19-02-01</a>
	
	</th>
	<td align=right>4,072</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	<a target="_top" href="2019/03/01/index.php">
	19-03-01</a>
	
	</th>
	<td align=right>5,231</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	<a target="_top" href="2019/04/01/index.php">
	19-04-01</a>
	
	</th>
	<td align=right>6,528</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	<a target="_top" href="2019/05/01/index.php">
	19-05-01</a>
	
	</th>
	<td align=right>6,946</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	<a target="_top" href="2019/06/01/index.php">
	19-06-01</a>
	
	</th>
	<td align=right>10,920</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	<a target="_top" href="2019/07/01/index.php">
	19-07-01</a>
	
	</th>
	<td align=right>7,636</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	<a target="_top" href="2019/08/01/index.php">
	19-08-01</a>
	
	</th>
	<td align=right>6,746</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	<a target="_top" href="2019/09/01/index.php">
	19-09-01</a>
	
	</th>
	<td align=right>7,780</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	<a target="_top" href="2019/10/01/index.php">
	19-10-01</a>
	
	</th>
	<td align=right>12,370</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	<a target="_top" href="2019/11/01/index.php">
	19-11-01</a>
	
	</th>
	<td align=right>11,095</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	<a target="_top" href="2019/12/01/index.php">
	19-12-01</a>
	
	</th>
	<td align=right>9,361</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	<a target="_top" href="2020/01/01/index.php">
	20-01-01</a>
	
	</th>
	<td align=right>8,189.45</td>
	
	</tr>

	</tbody>

	</table>
	