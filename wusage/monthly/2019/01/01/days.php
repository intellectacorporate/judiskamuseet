


<h2>Statistics for http://www.u5645470.fsdata.se, Month of 2019-1-1 to 2019-1-31</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Traffic by Day</h3>
<p>The continuous line represents a 7-day moving average.</p>
<img src="d.png" width="468" height="351">
<br /><br />


<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Date</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	<th scope='col'>Bits per Sec</th>
	<th scope='col'>Bytes per Sec</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th align="right">2019-1-1</th>
	<td align="right">2,860	</td>
	<td align="right">1.81	</td>
	<td align="right">713,540,555	</td>
	<td align="right">1.29	</td>
	<td align="right">159	</td>
	<td align="right">2.25	</td>
	<td align="right">66,068.57	</td>
	<td align="right">8,258.57	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-1-2</th>
	<td align="right">3,439	</td>
	<td align="right">2.18	</td>
	<td align="right">1,188,831,566	</td>
	<td align="right">2.16	</td>
	<td align="right">174	</td>
	<td align="right">2.46	</td>
	<td align="right">110,077.00	</td>
	<td align="right">13,759.62	</td>
</tr>
<tr class="udda">
	<th align="right">2019-1-3</th>
	<td align="right">3,418	</td>
	<td align="right">2.17	</td>
	<td align="right">1,149,843,284	</td>
	<td align="right">2.09	</td>
	<td align="right">192	</td>
	<td align="right">2.72	</td>
	<td align="right">106,466.97	</td>
	<td align="right">13,308.37	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-1-4</th>
	<td align="right">3,252	</td>
	<td align="right">2.06	</td>
	<td align="right">928,615,469	</td>
	<td align="right">1.68	</td>
	<td align="right">147	</td>
	<td align="right">2.08	</td>
	<td align="right">85,982.91	</td>
	<td align="right">10,747.86	</td>
</tr>
<tr class="udda">
	<th align="right">2019-1-5</th>
	<td align="right">2,474	</td>
	<td align="right">1.57	</td>
	<td align="right">562,371,844	</td>
	<td align="right">1.02	</td>
	<td align="right">139	</td>
	<td align="right">1.97	</td>
	<td align="right">52,071.47	</td>
	<td align="right">6,508.93	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-1-6</th>
	<td align="right">2,866	</td>
	<td align="right">1.82	</td>
	<td align="right">812,436,200	</td>
	<td align="right">1.47	</td>
	<td align="right">178	</td>
	<td align="right">2.52	</td>
	<td align="right">75,225.57	</td>
	<td align="right">9,403.20	</td>
</tr>
<tr class="udda">
	<th align="right">2019-1-7</th>
	<td align="right">4,908	</td>
	<td align="right">3.11	</td>
	<td align="right">1,607,699,656	</td>
	<td align="right">2.92	</td>
	<td align="right">146	</td>
	<td align="right">2.07	</td>
	<td align="right">148,861.08	</td>
	<td align="right">18,607.63	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-1-8</th>
	<td align="right">12,901	</td>
	<td align="right">8.18	</td>
	<td align="right">5,408,784,911	</td>
	<td align="right">9.81	</td>
	<td align="right">384	</td>
	<td align="right">5.43	</td>
	<td align="right">500,813.42	</td>
	<td align="right">62,601.68	</td>
</tr>
<tr class="udda">
	<th align="right">2019-1-9</th>
	<td align="right">7,834	</td>
	<td align="right">4.97	</td>
	<td align="right">4,156,807,344	</td>
	<td align="right">7.54	</td>
	<td align="right">297	</td>
	<td align="right">4.20	</td>
	<td align="right">384,889.57	</td>
	<td align="right">48,111.20	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-1-10</th>
	<td align="right">6,256	</td>
	<td align="right">3.97	</td>
	<td align="right">2,884,186,484	</td>
	<td align="right">5.23	</td>
	<td align="right">263	</td>
	<td align="right">3.72	</td>
	<td align="right">267,054.30	</td>
	<td align="right">33,381.79	</td>
</tr>
<tr class="udda">
	<th align="right">2019-1-11</th>
	<td align="right">3,836	</td>
	<td align="right">2.43	</td>
	<td align="right">1,710,322,691	</td>
	<td align="right">3.10	</td>
	<td align="right">210	</td>
	<td align="right">2.97	</td>
	<td align="right">158,363.21	</td>
	<td align="right">19,795.40	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-1-12</th>
	<td align="right">3,835	</td>
	<td align="right">2.43	</td>
	<td align="right">1,153,180,963	</td>
	<td align="right">2.09	</td>
	<td align="right">214	</td>
	<td align="right">3.03	</td>
	<td align="right">106,776.02	</td>
	<td align="right">13,347.00	</td>
</tr>
<tr class="udda">
	<th align="right">2019-1-13</th>
	<td align="right">2,955	</td>
	<td align="right">1.87	</td>
	<td align="right">1,021,065,834	</td>
	<td align="right">1.85	</td>
	<td align="right">166	</td>
	<td align="right">2.35	</td>
	<td align="right">94,543.13	</td>
	<td align="right">11,817.89	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-1-14</th>
	<td align="right">4,236	</td>
	<td align="right">2.68	</td>
	<td align="right">1,677,764,663	</td>
	<td align="right">3.04	</td>
	<td align="right">209	</td>
	<td align="right">2.96	</td>
	<td align="right">155,348.58	</td>
	<td align="right">19,418.57	</td>
</tr>
<tr class="udda">
	<th align="right">2019-1-15</th>
	<td align="right">4,588	</td>
	<td align="right">2.91	</td>
	<td align="right">1,441,424,687	</td>
	<td align="right">2.61	</td>
	<td align="right">207	</td>
	<td align="right">2.93	</td>
	<td align="right">133,465.25	</td>
	<td align="right">16,683.16	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-1-16</th>
	<td align="right">4,615	</td>
	<td align="right">2.93	</td>
	<td align="right">1,555,878,812	</td>
	<td align="right">2.82	</td>
	<td align="right">243	</td>
	<td align="right">3.44	</td>
	<td align="right">144,062.85	</td>
	<td align="right">18,007.86	</td>
</tr>
<tr class="udda">
	<th align="right">2019-1-17</th>
	<td align="right">6,265	</td>
	<td align="right">3.97	</td>
	<td align="right">1,773,684,856	</td>
	<td align="right">3.22	</td>
	<td align="right">214	</td>
	<td align="right">3.03	</td>
	<td align="right">164,230.08	</td>
	<td align="right">20,528.76	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-1-18</th>
	<td align="right">2,938	</td>
	<td align="right">1.86	</td>
	<td align="right">753,698,739	</td>
	<td align="right">1.37	</td>
	<td align="right">179	</td>
	<td align="right">2.53	</td>
	<td align="right">69,786.92	</td>
	<td align="right">8,723.37	</td>
</tr>
<tr class="udda">
	<th align="right">2019-1-19</th>
	<td align="right">2,713	</td>
	<td align="right">1.72	</td>
	<td align="right">864,776,009	</td>
	<td align="right">1.57	</td>
	<td align="right">139	</td>
	<td align="right">1.97	</td>
	<td align="right">80,071.85	</td>
	<td align="right">10,008.98	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-1-20</th>
	<td align="right">3,588	</td>
	<td align="right">2.27	</td>
	<td align="right">1,378,370,846	</td>
	<td align="right">2.50	</td>
	<td align="right">204	</td>
	<td align="right">2.89	</td>
	<td align="right">127,626.93	</td>
	<td align="right">15,953.37	</td>
</tr>
<tr class="udda">
	<th align="right">2019-1-21</th>
	<td align="right">4,860	</td>
	<td align="right">3.08	</td>
	<td align="right">1,440,702,067	</td>
	<td align="right">2.61	</td>
	<td align="right">251	</td>
	<td align="right">3.55	</td>
	<td align="right">133,398.34	</td>
	<td align="right">16,674.79	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-1-22</th>
	<td align="right">5,281	</td>
	<td align="right">3.35	</td>
	<td align="right">1,986,950,830	</td>
	<td align="right">3.60	</td>
	<td align="right">260	</td>
	<td align="right">3.68	</td>
	<td align="right">183,976.93	</td>
	<td align="right">22,997.12	</td>
</tr>
<tr class="udda">
	<th align="right">2019-1-23</th>
	<td align="right">6,156	</td>
	<td align="right">3.90	</td>
	<td align="right">2,213,402,091	</td>
	<td align="right">4.01	</td>
	<td align="right">276	</td>
	<td align="right">3.90	</td>
	<td align="right">204,944.64	</td>
	<td align="right">25,618.08	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-1-24</th>
	<td align="right">5,725	</td>
	<td align="right">3.63	</td>
	<td align="right">2,225,922,346	</td>
	<td align="right">4.04	</td>
	<td align="right">285	</td>
	<td align="right">4.03	</td>
	<td align="right">206,103.92	</td>
	<td align="right">25,762.99	</td>
</tr>
<tr class="udda">
	<th align="right">2019-1-25</th>
	<td align="right">5,701	</td>
	<td align="right">3.61	</td>
	<td align="right">2,159,639,105	</td>
	<td align="right">3.92	</td>
	<td align="right">233	</td>
	<td align="right">3.30	</td>
	<td align="right">199,966.58	</td>
	<td align="right">24,995.82	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-1-26</th>
	<td align="right">3,844	</td>
	<td align="right">2.44	</td>
	<td align="right">1,210,280,757	</td>
	<td align="right">2.20	</td>
	<td align="right">208	</td>
	<td align="right">2.94	</td>
	<td align="right">112,063.03	</td>
	<td align="right">14,007.88	</td>
</tr>
<tr class="udda">
	<th align="right">2019-1-27</th>
	<td align="right">6,284	</td>
	<td align="right">3.98	</td>
	<td align="right">2,049,110,202	</td>
	<td align="right">3.72	</td>
	<td align="right">320	</td>
	<td align="right">4.53	</td>
	<td align="right">189,732.43	</td>
	<td align="right">23,716.55	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-1-28</th>
	<td align="right">9,684	</td>
	<td align="right">6.14	</td>
	<td align="right">2,480,355,342	</td>
	<td align="right">4.50	</td>
	<td align="right">355	</td>
	<td align="right">5.02	</td>
	<td align="right">229,662.53	</td>
	<td align="right">28,707.82	</td>
</tr>
<tr class="udda">
	<th align="right">2019-1-29</th>
	<td align="right">6,796	</td>
	<td align="right">4.31	</td>
	<td align="right">2,059,640,763	</td>
	<td align="right">3.74	</td>
	<td align="right">331	</td>
	<td align="right">4.68	</td>
	<td align="right">190,707.48	</td>
	<td align="right">23,838.43	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-1-30</th>
	<td align="right">6,208	</td>
	<td align="right">3.93	</td>
	<td align="right">2,818,101,853	</td>
	<td align="right">5.11	</td>
	<td align="right">245	</td>
	<td align="right">3.47	</td>
	<td align="right">260,935.36	</td>
	<td align="right">32,616.92	</td>
</tr>
<tr class="udda">
	<th align="right">2019-1-31</th>
	<td align="right">7,450	</td>
	<td align="right">4.72	</td>
	<td align="right">1,741,063,823	</td>
	<td align="right">3.16	</td>
	<td align="right">242	</td>
	<td align="right">3.42	</td>
	<td align="right">161,209.61	</td>
	<td align="right">20,151.20	</td>
</tr>

</tbody>
</table>

