


<h2>Statistics for http://www.u5645470.fsdata.se, Month of 2019-1-1 to 2019-1-31</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Traffic by Hour</h3>
<p>Figures are averages for that hour
	of each day in the time period analyzed.</p>
<img src="h.png" width="468" height="351">
<br /><br />
<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Hour</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	<th scope='col'>Bits per Sec</th>
	<th scope='col'>Bytes per Sec</th>
	
	</tr>
</thead>
<tbody>
<tr class="jamnudda">
	<th align="right">00:00</th>
	<td align="right">102.68</td>
	<td align="right">2.02</td>
	<td align="right">32,017,835.94</td>
	<td align="right">1.80</td>
	<td align="right">2.93</td>
	<td align="right">1.28</td>
	<td align="right">71,150.75</td>
	<td align="right">8,893.84</td>
	
</tr>
<tr class="jamn">
	<th align="right">01:00</th>
	<td align="right">72.10</td>
	<td align="right">1.42</td>
	<td align="right">18,911,239.55</td>
	<td align="right">1.06</td>
	<td align="right">4.57</td>
	<td align="right">2.00</td>
	<td align="right">42,024.98</td>
	<td align="right">5,253.12</td>
	
</tr>
<tr class="udda">
	<th align="right">02:00</th>
	<td align="right">56.81</td>
	<td align="right">1.12</td>
	<td align="right">9,856,574.45</td>
	<td align="right">0.55</td>
	<td align="right">4.22</td>
	<td align="right">1.85</td>
	<td align="right">21,903.50</td>
	<td align="right">2,737.94</td>
	
</tr>
<tr class="jamn">
	<th align="right">03:00</th>
	<td align="right">55.06</td>
	<td align="right">1.08</td>
	<td align="right">4,571,940.74</td>
	<td align="right">0.26</td>
	<td align="right">4.21</td>
	<td align="right">1.85</td>
	<td align="right">10,159.87</td>
	<td align="right">1,269.98</td>
	
</tr>
<tr class="udda">
	<th align="right">04:00</th>
	<td align="right">52.74</td>
	<td align="right">1.04</td>
	<td align="right">5,540,636.74</td>
	<td align="right">0.31</td>
	<td align="right">4.01</td>
	<td align="right">1.76</td>
	<td align="right">12,312.53</td>
	<td align="right">1,539.07</td>
	
</tr>
<tr class="jamn">
	<th align="right">05:00</th>
	<td align="right">56.42</td>
	<td align="right">1.11</td>
	<td align="right">9,328,248.84</td>
	<td align="right">0.52</td>
	<td align="right">3.60</td>
	<td align="right">1.58</td>
	<td align="right">20,729.44</td>
	<td align="right">2,591.18</td>
	
</tr>
<tr class="udda">
	<th align="right">06:00</th>
	<td align="right">61.65</td>
	<td align="right">1.21</td>
	<td align="right">12,181,308.87</td>
	<td align="right">0.68</td>
	<td align="right">4.30</td>
	<td align="right">1.88</td>
	<td align="right">27,069.58</td>
	<td align="right">3,383.70</td>
	
</tr>
<tr class="jamn">
	<th align="right">07:00</th>
	<td align="right">79.65</td>
	<td align="right">1.56</td>
	<td align="right">13,371,361.00</td>
	<td align="right">0.75</td>
	<td align="right">3.99</td>
	<td align="right">1.75</td>
	<td align="right">29,714.14</td>
	<td align="right">3,714.27</td>
	
</tr>
<tr class="udda">
	<th align="right">08:00</th>
	<td align="right">212.42</td>
	<td align="right">4.17</td>
	<td align="right">82,855,609.65</td>
	<td align="right">4.66</td>
	<td align="right">7.58</td>
	<td align="right">3.32</td>
	<td align="right">184,123.58</td>
	<td align="right">23,015.45</td>
	
</tr>
<tr class="jamn">
	<th align="right">09:00</th>
	<td align="right">281.71</td>
	<td align="right">5.54</td>
	<td align="right">103,725,649.19</td>
	<td align="right">5.83</td>
	<td align="right">12.79</td>
	<td align="right">5.61</td>
	<td align="right">230,501.44</td>
	<td align="right">28,812.68</td>
	
</tr>
<tr class="udda">
	<th align="right">10:00</th>
	<td align="right">382.58</td>
	<td align="right">7.52</td>
	<td align="right">152,523,527.39</td>
	<td align="right">8.58</td>
	<td align="right">15.29</td>
	<td align="right">6.70</td>
	<td align="right">338,941.17</td>
	<td align="right">42,367.65</td>
	
</tr>
<tr class="jamn">
	<th align="right">11:00</th>
	<td align="right">407.13</td>
	<td align="right">8.00</td>
	<td align="right">167,850,014.61</td>
	<td align="right">9.44</td>
	<td align="right">16.07</td>
	<td align="right">7.05</td>
	<td align="right">373,000.03</td>
	<td align="right">46,625.00</td>
	
</tr>
<tr class="udda">
	<th align="right">12:00</th>
	<td align="right">340.39</td>
	<td align="right">6.69</td>
	<td align="right">135,156,449.03</td>
	<td align="right">7.60</td>
	<td align="right">14.95</td>
	<td align="right">6.55</td>
	<td align="right">300,347.66</td>
	<td align="right">37,543.46</td>
	
</tr>
<tr class="jamn">
	<th align="right">13:00</th>
	<td align="right">369.26</td>
	<td align="right">7.26</td>
	<td align="right">160,624,719.03</td>
	<td align="right">9.03</td>
	<td align="right">14.00</td>
	<td align="right">6.14</td>
	<td align="right">356,943.82</td>
	<td align="right">44,617.98</td>
	
</tr>
<tr class="udda">
	<th align="right">14:00</th>
	<td align="right">374.74</td>
	<td align="right">7.36</td>
	<td align="right">135,365,591.94</td>
	<td align="right">7.61</td>
	<td align="right">13.79</td>
	<td align="right">6.04</td>
	<td align="right">300,812.43</td>
	<td align="right">37,601.55</td>
	
</tr>
<tr class="jamn">
	<th align="right">15:00</th>
	<td align="right">383.48</td>
	<td align="right">7.54</td>
	<td align="right">134,152,029.32</td>
	<td align="right">7.54</td>
	<td align="right">13.14</td>
	<td align="right">5.76</td>
	<td align="right">298,115.62</td>
	<td align="right">37,264.45</td>
	
</tr>
<tr class="udda">
	<th align="right">16:00</th>
	<td align="right">329.68</td>
	<td align="right">6.48</td>
	<td align="right">109,026,195.97</td>
	<td align="right">6.13</td>
	<td align="right">14.62</td>
	<td align="right">6.41</td>
	<td align="right">242,280.44</td>
	<td align="right">30,285.05</td>
	
</tr>
<tr class="jamn">
	<th align="right">17:00</th>
	<td align="right">235.45</td>
	<td align="right">4.63</td>
	<td align="right">73,610,348.29</td>
	<td align="right">4.14</td>
	<td align="right">12.32</td>
	<td align="right">5.40</td>
	<td align="right">163,578.55</td>
	<td align="right">20,447.32</td>
	
</tr>
<tr class="udda">
	<th align="right">18:00</th>
	<td align="right">265.32</td>
	<td align="right">5.21</td>
	<td align="right">92,039,723.90</td>
	<td align="right">5.18</td>
	<td align="right">10.86</td>
	<td align="right">4.76</td>
	<td align="right">204,532.72</td>
	<td align="right">25,566.59</td>
	
</tr>
<tr class="jamn">
	<th align="right">19:00</th>
	<td align="right">226.52</td>
	<td align="right">4.45</td>
	<td align="right">77,017,444.35</td>
	<td align="right">4.33</td>
	<td align="right">10.88</td>
	<td align="right">4.77</td>
	<td align="right">171,149.88</td>
	<td align="right">21,393.73</td>
	
</tr>
<tr class="udda">
	<th align="right">20:00</th>
	<td align="right">209.48</td>
	<td align="right">4.12</td>
	<td align="right">69,521,769.77</td>
	<td align="right">3.91</td>
	<td align="right">10.26</td>
	<td align="right">4.50</td>
	<td align="right">154,492.82</td>
	<td align="right">19,311.60</td>
	
</tr>
<tr class="jamn">
	<th align="right">21:00</th>
	<td align="right">224.29</td>
	<td align="right">4.41</td>
	<td align="right">88,490,340.65</td>
	<td align="right">4.98</td>
	<td align="right">9.88</td>
	<td align="right">4.33</td>
	<td align="right">196,645.20</td>
	<td align="right">24,580.65</td>
	
</tr>
<tr class="udda">
	<th align="right">22:00</th>
	<td align="right">178.23</td>
	<td align="right">3.50</td>
	<td align="right">50,903,923.00</td>
	<td align="right">2.86</td>
	<td align="right">10.15</td>
	<td align="right">4.45</td>
	<td align="right">113,119.83</td>
	<td align="right">14,139.98</td>
	
</tr>
<tr class="jamn">
	<th align="right">23:00</th>
	<td align="right">131.45</td>
	<td align="right">2.58</td>
	<td align="right">39,694,762.68</td>
	<td align="right">2.23</td>
	<td align="right">9.68</td>
	<td align="right">4.25</td>
	<td align="right">88,210.58</td>
	<td align="right">11,026.32</td>
	
</tr>

</tbody>
</table>

