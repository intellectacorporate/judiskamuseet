


<h2>Statistics for http://www.u5645470.fsdata.se, Month of 2019-1-1 to 2019-1-31</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 20 of 6,586 Visitor Sites,
Sorted by Access Count

</h3>
<p></p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Site</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="jamnudda">
	<th class="hogerstalld">1</th>
	<td>
	46.59.25.88		
	</td>
	<td align="right">12,380</td>
	<td align="right">7.85</td>
	<td align="right">4,717,071,739</td>
	<td align="right">8.56</td>
	<td align="right">81</td>
	<td align="right">1.15</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	63.143.42.244		
	</td>
	<td align="right">6,442</td>
	<td align="right">4.08</td>
	<td align="right">65,630</td>
	<td align="right">0.00</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	89.221.250.27		
	</td>
	<td align="right">6,174</td>
	<td align="right">3.91</td>
	<td align="right">121,530</td>
	<td align="right">0.00</td>
	<td align="right">88</td>
	<td align="right">1.25</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	46.182.207.79		
	</td>
	<td align="right">1,004</td>
	<td align="right">0.64</td>
	<td align="right">143,949,955</td>
	<td align="right">0.26</td>
	<td align="right">7</td>
	<td align="right">0.11</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	23.101.169.3		
	</td>
	<td align="right">767</td>
	<td align="right">0.49</td>
	<td align="right">11,867,130</td>
	<td align="right">0.02</td>
	<td align="right">51</td>
	<td align="right">0.73</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	195.3.144.185		
	</td>
	<td align="right">743</td>
	<td align="right">0.47</td>
	<td align="right">12,324,001</td>
	<td align="right">0.02</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	95.195.216.179		
	</td>
	<td align="right">732</td>
	<td align="right">0.46</td>
	<td align="right">23,349,835</td>
	<td align="right">0.04</td>
	<td align="right">1</td>
	<td align="right">0.02</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	81.170.246.9		
	</td>
	<td align="right">685</td>
	<td align="right">0.43</td>
	<td align="right">179,350,601</td>
	<td align="right">0.33</td>
	<td align="right">7</td>
	<td align="right">0.11</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	144.217.171.229		
	</td>
	<td align="right">667</td>
	<td align="right">0.42</td>
	<td align="right">219,518,197</td>
	<td align="right">0.40</td>
	<td align="right">3</td>
	<td align="right">0.05</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">10</th>
	<td>
	82.209.180.94		
	</td>
	<td align="right">612</td>
	<td align="right">0.39</td>
	<td align="right">62,093,454</td>
	<td align="right">0.11</td>
	<td align="right">4</td>
	<td align="right">0.06</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">11</th>
	<td>
	90.229.237.5		
	</td>
	<td align="right">577</td>
	<td align="right">0.37</td>
	<td align="right">28,642,965</td>
	<td align="right">0.05</td>
	<td align="right">10</td>
	<td align="right">0.16</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">12</th>
	<td>
	66.249.66.214		
	</td>
	<td align="right">574</td>
	<td align="right">0.36</td>
	<td align="right">44,773,872</td>
	<td align="right">0.08</td>
	<td align="right">95</td>
	<td align="right">1.35</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">13</th>
	<td>
	66.249.66.216		
	</td>
	<td align="right">472</td>
	<td align="right">0.30</td>
	<td align="right">20,955,480</td>
	<td align="right">0.04</td>
	<td align="right">77</td>
	<td align="right">1.10</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">14</th>
	<td>
	122.49.28.47		
	</td>
	<td align="right">402</td>
	<td align="right">0.25</td>
	<td align="right">62,052,481</td>
	<td align="right">0.11</td>
	<td align="right">9</td>
	<td align="right">0.14</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">15</th>
	<td>
	134.25.0.196		
	</td>
	<td align="right">392</td>
	<td align="right">0.25</td>
	<td align="right">302,810,392</td>
	<td align="right">0.55</td>
	<td align="right">6</td>
	<td align="right">0.09</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">16</th>
	<td>
	66.249.66.220		
	</td>
	<td align="right">359</td>
	<td align="right">0.23</td>
	<td align="right">6,559,394</td>
	<td align="right">0.01</td>
	<td align="right">40</td>
	<td align="right">0.57</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">17</th>
	<td>
	155.4.131.233		
	</td>
	<td align="right">355</td>
	<td align="right">0.23</td>
	<td align="right">106,968,259</td>
	<td align="right">0.19</td>
	<td align="right">10</td>
	<td align="right">0.15</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">18</th>
	<td>
	188.92.74.189		
	</td>
	<td align="right">350</td>
	<td align="right">0.22</td>
	<td align="right">5,801,734</td>
	<td align="right">0.01</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">19</th>
	<td>
	216.244.66.227		
	</td>
	<td align="right">350</td>
	<td align="right">0.22</td>
	<td align="right">278,791,725</td>
	<td align="right">0.51</td>
	<td align="right">56</td>
	<td align="right">0.80</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">20</th>
	<td>
	66.249.66.218		
	</td>
	<td align="right">337</td>
	<td align="right">0.21</td>
	<td align="right">21,927,179</td>
	<td align="right">0.04</td>
	<td align="right">57</td>
	<td align="right">0.82</td>
</tr>

</tbody>
</table>

