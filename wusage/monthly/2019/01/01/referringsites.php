


<h2>Statistics for http://www.u5645470.fsdata.se, Month of 2019-1-1 to 2019-1-31</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 20 of 307 Referring Sites,
Sorted by Access Count

</h3>
<p></p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Referring Site</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th class="hogerstalld">1</th>
	<td>
	<a href="http://judiskamuseet.se">
	http://judiskamuseet.se</a>
	</td>
	<td align="right">76,719</td>
	<td align="right">55.21</td>
	<td align="right">26,145,058,027</td>
	<td align="right">76.47</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	<a href="https://judiskamuseet.se">
	https://judiskamuseet.se</a>
	</td>
	<td align="right">56,129</td>
	<td align="right">40.39</td>
	<td align="right">7,337,963,494</td>
	<td align="right">21.46</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	<a href="https://www.google.com">
	https://www.google.com</a>
	</td>
	<td align="right">2,312</td>
	<td align="right">1.66</td>
	<td align="right">414,479,146</td>
	<td align="right">1.21</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	<a href="https://www.google.se">
	https://www.google.se</a>
	</td>
	<td align="right">1,332</td>
	<td align="right">0.96</td>
	<td align="right">146,731,049</td>
	<td align="right">0.43</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	<a href="http://judiska-museet.se">
	http://judiska-museet.se</a>
	</td>
	<td align="right">363</td>
	<td align="right">0.26</td>
	<td align="right">5,926,770</td>
	<td align="right">0.02</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	<a href="https://www.google.de">
	https://www.google.de</a>
	</td>
	<td align="right">344</td>
	<td align="right">0.25</td>
	<td align="right">3,852,853</td>
	<td align="right">0.01</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	<a href="http://site.ru">
	http://site.ru</a>
	</td>
	<td align="right">157</td>
	<td align="right">0.11</td>
	<td align="right">6,437,365</td>
	<td align="right">0.02</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	<a href="http://www.judiskamuseet.se">
	http://www.judiskamuseet.se</a>
	</td>
	<td align="right">134</td>
	<td align="right">0.10</td>
	<td align="right">1,921,596</td>
	<td align="right">0.01</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	<a href="http://m.facebook.com">
	http://m.facebook.com</a>
	</td>
	<td align="right">102</td>
	<td align="right">0.07</td>
	<td align="right">1,317,556</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">10</th>
	<td>
	<a href="http://www.judiska-museet.se">
	http://www.judiska-museet.se</a>
	</td>
	<td align="right">65</td>
	<td align="right">0.05</td>
	<td align="right">1,466,785</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">11</th>
	<td>
	<a href="http://www.google.com">
	http://www.google.com</a>
	</td>
	<td align="right">57</td>
	<td align="right">0.04</td>
	<td align="right">605,557</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">12</th>
	<td>
	<a href="https://89.221.250.27:443">
	https://89.221.250.27:443</a>
	</td>
	<td align="right">49</td>
	<td align="right">0.04</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">13</th>
	<td>
	<a href="https://www.bing.com">
	https://www.bing.com</a>
	</td>
	<td align="right">47</td>
	<td align="right">0.03</td>
	<td align="right">93,963,151</td>
	<td align="right">0.27</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">14</th>
	<td>
	<a href="https://www.facebook.com">
	https://www.facebook.com</a>
	</td>
	<td align="right">46</td>
	<td align="right">0.03</td>
	<td align="right">516,646</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">15</th>
	<td>
	<a href="http://www.bing.com">
	http://www.bing.com</a>
	</td>
	<td align="right">31</td>
	<td align="right">0.02</td>
	<td align="right">406,288</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">16</th>
	<td>
	<a href="https://duckduckgo.com">
	https://duckduckgo.com</a>
	</td>
	<td align="right">26</td>
	<td align="right">0.02</td>
	<td align="right">392,736</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">17</th>
	<td>
	<a href="https://r.search.yahoo.com">
	https://r.search.yahoo.com</a>
	</td>
	<td align="right">20</td>
	<td align="right">0.01</td>
	<td align="right">296,659</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">18</th>
	<td>
	<a href="https://sv.m.wikipedia.org">
	https://sv.m.wikipedia.org</a>
	</td>
	<td align="right">17</td>
	<td align="right">0.01</td>
	<td align="right">182,506</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">19</th>
	<td>
	<a href="http://l.facebook.com">
	http://l.facebook.com</a>
	</td>
	<td align="right">14</td>
	<td align="right">0.01</td>
	<td align="right">140,550</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">20</th>
	<td>
	<a href="http://www.minoritet.se">
	http://www.minoritet.se</a>
	</td>
	<td align="right">13</td>
	<td align="right">0.01</td>
	<td align="right">195,703</td>
	<td align="right">0.00</td>
</tr>

</tbody>
</table>

