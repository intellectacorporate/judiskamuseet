


<h2>Statistics for http://www.u5645470.fsdata.se, Month of 2019-1-1 to 2019-1-31</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 10 of 570 Browsers (User Agents),
Sorted by Access Count

</h3>
<p></p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Product</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th class="hogerstalld">1</th>
	<td>
	Safari/537.36		
	</td>
	<td align="right">52,242</td>
	<td align="right">33.25</td>
	<td align="right">22,654,817,916</td>
	<td align="right">41.23</td>
	<td align="right">2,228</td>
	<td align="right">31.59</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	Safari/604.1		
	</td>
	<td align="right">24,496</td>
	<td align="right">15.59</td>
	<td align="right">9,669,441,104</td>
	<td align="right">17.60</td>
	<td align="right">1,009</td>
	<td align="right">14.31</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	Mozilla/5.0 		
	</td>
	<td align="right">17,048</td>
	<td align="right">10.85</td>
	<td align="right">6,369,205,283</td>
	<td align="right">11.59</td>
	<td align="right">639</td>
	<td align="right">9.06</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	Safari/537.36 Edge/17.17134		
	</td>
	<td align="right">13,515</td>
	<td align="right">8.60</td>
	<td align="right">6,074,305,236</td>
	<td align="right">11.05</td>
	<td align="right">179</td>
	<td align="right">2.55</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	Safari/605.1.15		
	</td>
	<td align="right">6,787</td>
	<td align="right">4.32</td>
	<td align="right">3,046,618,524</td>
	<td align="right">5.54</td>
	<td align="right">232</td>
	<td align="right">3.30</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	UptimeRobot/2.0		
	</td>
	<td align="right">6,434</td>
	<td align="right">4.10</td>
	<td align="right">98,848</td>
	<td align="right">0.00</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	bingbot/2.0		
	</td>
	<td align="right">5,067</td>
	<td align="right">3.23</td>
	<td align="right">319,967,100</td>
	<td align="right">0.58</td>
	<td align="right">916</td>
	<td align="right">12.99</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	WordPress/5.0.3		
	</td>
	<td align="right">4,135</td>
	<td align="right">2.63</td>
	<td align="right">81,420</td>
	<td align="right">0.00</td>
	<td align="right">61</td>
	<td align="right">0.87</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	Safari/605.1		
	</td>
	<td align="right">2,960</td>
	<td align="right">1.88</td>
	<td align="right">1,227,216,056</td>
	<td align="right">2.23</td>
	<td align="right">121</td>
	<td align="right">1.73</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">10</th>
	<td>
	WordPress/5.0.2		
	</td>
	<td align="right">2,034</td>
	<td align="right">1.29</td>
	<td align="right">40,105</td>
	<td align="right">0.00</td>
	<td align="right">27</td>
	<td align="right">0.38</td>
</tr>

</tbody>
</table>

