


<h2>Statistics for http://www.u5645470.fsdata.se, Month of 2019-1-1 to 2019-1-31</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 10 of 22 File Types (Extensions), 
Sorted by Access Count

</h3>
<p>Individual file types as determined by file extensions. All URLs that
do not contain an extension are counted as directories.</p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Type</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th class="hogerstalld">1</th>
	<td>
	jpg		
	</td>
	<td align="right">42,559</td>
	<td align="right">26.98</td>
	<td align="right">51,777,066,955</td>
	<td align="right">93.92</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	js		
	</td>
	<td align="right">37,109</td>
	<td align="right">23.52</td>
	<td align="right">478,271,096</td>
	<td align="right">0.87</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	Directory (folder)		
	</td>
	<td align="right">31,891</td>
	<td align="right">20.21</td>
	<td align="right">343,612,175</td>
	<td align="right">0.62</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	css		
	</td>
	<td align="right">21,902</td>
	<td align="right">13.88</td>
	<td align="right">440,998,467</td>
	<td align="right">0.80</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	php		
	</td>
	<td align="right">9,761</td>
	<td align="right">6.19</td>
	<td align="right">207,661,890</td>
	<td align="right">0.38</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	png		
	</td>
	<td align="right">7,123</td>
	<td align="right">4.51</td>
	<td align="right">1,010,162,356</td>
	<td align="right">1.83</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	ttf		
	</td>
	<td align="right">3,655</td>
	<td align="right">2.32</td>
	<td align="right">332,879,760</td>
	<td align="right">0.60</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	txt		
	</td>
	<td align="right">2,344</td>
	<td align="right">1.49</td>
	<td align="right">160,490</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	svg		
	</td>
	<td align="right">397</td>
	<td align="right">0.25</td>
	<td align="right">12,402,783</td>
	<td align="right">0.02</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">10</th>
	<td>
	gif		
	</td>
	<td align="right">343</td>
	<td align="right">0.22</td>
	<td align="right">1,352,943</td>
	<td align="right">0.00</td>
</tr>

</tbody>
</table>

