


<h2>Statistics for http://www.u5645470.fsdata.se, Month of 2019-3-1 to 2019-3-31</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Traffic by Day</h3>
<p>The continuous line represents a 7-day moving average.</p>
<img src="d.png" width="468" height="351">
<br /><br />


<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Date</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	<th scope='col'>Bits per Sec</th>
	<th scope='col'>Bytes per Sec</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th align="right">2019-3-1</th>
	<td align="right">3,982	</td>
	<td align="right">2.56	</td>
	<td align="right">1,358,293,237	</td>
	<td align="right">2.98	</td>
	<td align="right">218	</td>
	<td align="right">2.43	</td>
	<td align="right">125,767.89	</td>
	<td align="right">15,720.99	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-3-2</th>
	<td align="right">3,176	</td>
	<td align="right">2.04	</td>
	<td align="right">894,445,150	</td>
	<td align="right">1.96	</td>
	<td align="right">241	</td>
	<td align="right">2.69	</td>
	<td align="right">82,819.00	</td>
	<td align="right">10,352.37	</td>
</tr>
<tr class="udda">
	<th align="right">2019-3-3</th>
	<td align="right">2,514	</td>
	<td align="right">1.62	</td>
	<td align="right">649,394,971	</td>
	<td align="right">1.42	</td>
	<td align="right">183	</td>
	<td align="right">2.04	</td>
	<td align="right">60,129.16	</td>
	<td align="right">7,516.15	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-3-4</th>
	<td align="right">5,876	</td>
	<td align="right">3.78	</td>
	<td align="right">1,811,307,766	</td>
	<td align="right">3.97	</td>
	<td align="right">339	</td>
	<td align="right">3.78	</td>
	<td align="right">167,713.68	</td>
	<td align="right">20,964.21	</td>
</tr>
<tr class="udda">
	<th align="right">2019-3-5</th>
	<td align="right">4,418	</td>
	<td align="right">2.84	</td>
	<td align="right">1,439,491,455	</td>
	<td align="right">3.15	</td>
	<td align="right">301	</td>
	<td align="right">3.36	</td>
	<td align="right">133,286.25	</td>
	<td align="right">16,660.78	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-3-6</th>
	<td align="right">4,449	</td>
	<td align="right">2.86	</td>
	<td align="right">1,246,973,433	</td>
	<td align="right">2.73	</td>
	<td align="right">280	</td>
	<td align="right">3.12	</td>
	<td align="right">115,460.50	</td>
	<td align="right">14,432.56	</td>
</tr>
<tr class="udda">
	<th align="right">2019-3-7</th>
	<td align="right">5,930	</td>
	<td align="right">3.81	</td>
	<td align="right">2,081,200,079	</td>
	<td align="right">4.56	</td>
	<td align="right">248	</td>
	<td align="right">2.77	</td>
	<td align="right">192,703.71	</td>
	<td align="right">24,087.96	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-3-8</th>
	<td align="right">6,257	</td>
	<td align="right">4.02	</td>
	<td align="right">1,594,835,105	</td>
	<td align="right">3.49	</td>
	<td align="right">326	</td>
	<td align="right">3.64	</td>
	<td align="right">147,669.92	</td>
	<td align="right">18,458.74	</td>
</tr>
<tr class="udda">
	<th align="right">2019-3-9</th>
	<td align="right">3,970	</td>
	<td align="right">2.55	</td>
	<td align="right">934,159,854	</td>
	<td align="right">2.05	</td>
	<td align="right">222	</td>
	<td align="right">2.48	</td>
	<td align="right">86,496.28	</td>
	<td align="right">10,812.04	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-3-10</th>
	<td align="right">3,521	</td>
	<td align="right">2.26	</td>
	<td align="right">1,248,502,778	</td>
	<td align="right">2.74	</td>
	<td align="right">210	</td>
	<td align="right">2.34	</td>
	<td align="right">115,602.11	</td>
	<td align="right">14,450.26	</td>
</tr>
<tr class="udda">
	<th align="right">2019-3-11</th>
	<td align="right">4,294	</td>
	<td align="right">2.76	</td>
	<td align="right">1,314,043,562	</td>
	<td align="right">2.88	</td>
	<td align="right">233	</td>
	<td align="right">2.60	</td>
	<td align="right">121,670.70	</td>
	<td align="right">15,208.84	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-3-12</th>
	<td align="right">6,109	</td>
	<td align="right">3.93	</td>
	<td align="right">2,153,336,909	</td>
	<td align="right">4.72	</td>
	<td align="right">322	</td>
	<td align="right">3.59	</td>
	<td align="right">199,383.05	</td>
	<td align="right">24,922.88	</td>
</tr>
<tr class="udda">
	<th align="right">2019-3-13</th>
	<td align="right">7,215	</td>
	<td align="right">4.64	</td>
	<td align="right">2,607,705,470	</td>
	<td align="right">5.71	</td>
	<td align="right">330	</td>
	<td align="right">3.68	</td>
	<td align="right">241,454.21	</td>
	<td align="right">30,181.78	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-3-14</th>
	<td align="right">5,211	</td>
	<td align="right">3.35	</td>
	<td align="right">1,614,280,877	</td>
	<td align="right">3.54	</td>
	<td align="right">304	</td>
	<td align="right">3.39	</td>
	<td align="right">149,470.45	</td>
	<td align="right">18,683.81	</td>
</tr>
<tr class="udda">
	<th align="right">2019-3-15</th>
	<td align="right">4,312	</td>
	<td align="right">2.77	</td>
	<td align="right">1,412,000,284	</td>
	<td align="right">3.09	</td>
	<td align="right">241	</td>
	<td align="right">2.69	</td>
	<td align="right">130,740.77	</td>
	<td align="right">16,342.60	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-3-16</th>
	<td align="right">2,746	</td>
	<td align="right">1.77	</td>
	<td align="right">778,927,552	</td>
	<td align="right">1.71	</td>
	<td align="right">184	</td>
	<td align="right">2.05	</td>
	<td align="right">72,122.92	</td>
	<td align="right">9,015.37	</td>
</tr>
<tr class="udda">
	<th align="right">2019-3-17</th>
	<td align="right">4,265	</td>
	<td align="right">2.74	</td>
	<td align="right">1,151,574,858	</td>
	<td align="right">2.52	</td>
	<td align="right">228	</td>
	<td align="right">2.54	</td>
	<td align="right">106,627.30	</td>
	<td align="right">13,328.41	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-3-18</th>
	<td align="right">4,728	</td>
	<td align="right">3.04	</td>
	<td align="right">1,610,129,333	</td>
	<td align="right">3.53	</td>
	<td align="right">278	</td>
	<td align="right">3.10	</td>
	<td align="right">149,086.05	</td>
	<td align="right">18,635.76	</td>
</tr>
<tr class="udda">
	<th align="right">2019-3-19</th>
	<td align="right">5,582	</td>
	<td align="right">3.59	</td>
	<td align="right">1,586,588,585	</td>
	<td align="right">3.48	</td>
	<td align="right">260	</td>
	<td align="right">2.90	</td>
	<td align="right">146,906.35	</td>
	<td align="right">18,363.29	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-3-20</th>
	<td align="right">6,433	</td>
	<td align="right">4.14	</td>
	<td align="right">2,405,524,079	</td>
	<td align="right">5.27	</td>
	<td align="right">299	</td>
	<td align="right">3.34	</td>
	<td align="right">222,733.71	</td>
	<td align="right">27,841.71	</td>
</tr>
<tr class="udda">
	<th align="right">2019-3-21</th>
	<td align="right">5,639	</td>
	<td align="right">3.63	</td>
	<td align="right">1,632,439,057	</td>
	<td align="right">3.58	</td>
	<td align="right">359	</td>
	<td align="right">4.01	</td>
	<td align="right">151,151.76	</td>
	<td align="right">18,893.97	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-3-22</th>
	<td align="right">5,163	</td>
	<td align="right">3.32	</td>
	<td align="right">1,548,908,063	</td>
	<td align="right">3.39	</td>
	<td align="right">470	</td>
	<td align="right">5.24	</td>
	<td align="right">143,417.41	</td>
	<td align="right">17,927.18	</td>
</tr>
<tr class="udda">
	<th align="right">2019-3-23</th>
	<td align="right">2,965	</td>
	<td align="right">1.91	</td>
	<td align="right">765,152,524	</td>
	<td align="right">1.68	</td>
	<td align="right">199	</td>
	<td align="right">2.22	</td>
	<td align="right">70,847.46	</td>
	<td align="right">8,855.93	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-3-24</th>
	<td align="right">3,706	</td>
	<td align="right">2.38	</td>
	<td align="right">876,680,704	</td>
	<td align="right">1.92	</td>
	<td align="right">227	</td>
	<td align="right">2.53	</td>
	<td align="right">81,174.14	</td>
	<td align="right">10,146.77	</td>
</tr>
<tr class="udda">
	<th align="right">2019-3-25</th>
	<td align="right">1	</td>
	<td align="right">0.00	</td>
	<td align="right">1	</td>
	<td align="right">0.00	</td>
	<td align="right">0	</td>
	<td align="right">0.00	</td>
	<td align="right">0.00	</td>
	<td align="right">0.00	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-3-26</th>
	<td align="right">14,734	</td>
	<td align="right">9.48	</td>
	<td align="right">3,208,004,267	</td>
	<td align="right">7.03	</td>
	<td align="right">786	</td>
	<td align="right">8.77	</td>
	<td align="right">297,037.43	</td>
	<td align="right">37,129.68	</td>
</tr>
<tr class="udda">
	<th align="right">2019-3-27</th>
	<td align="right">9,214	</td>
	<td align="right">5.93	</td>
	<td align="right">2,268,275,668	</td>
	<td align="right">4.97	</td>
	<td align="right">581	</td>
	<td align="right">6.48	</td>
	<td align="right">210,025.52	</td>
	<td align="right">26,253.19	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-3-28</th>
	<td align="right">6,829	</td>
	<td align="right">4.39	</td>
	<td align="right">2,125,323,385	</td>
	<td align="right">4.66	</td>
	<td align="right">377	</td>
	<td align="right">4.21	</td>
	<td align="right">196,789.20	</td>
	<td align="right">24,598.65	</td>
</tr>
<tr class="udda">
	<th align="right">2019-3-29</th>
	<td align="right">4,904	</td>
	<td align="right">3.15	</td>
	<td align="right">1,295,232,578	</td>
	<td align="right">2.84	</td>
	<td align="right">278	</td>
	<td align="right">3.10	</td>
	<td align="right">119,928.94	</td>
	<td align="right">14,991.12	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-3-30</th>
	<td align="right">3,188	</td>
	<td align="right">2.05	</td>
	<td align="right">841,531,939	</td>
	<td align="right">1.84	</td>
	<td align="right">211	</td>
	<td align="right">2.35	</td>
	<td align="right">77,919.62	</td>
	<td align="right">9,739.95	</td>
</tr>
<tr class="udda">
	<th align="right">2019-3-31</th>
	<td align="right">4,147	</td>
	<td align="right">2.67	</td>
	<td align="right">1,190,659,577	</td>
	<td align="right">2.61	</td>
	<td align="right">226	</td>
	<td align="right">2.52	</td>
	<td align="right">110,246.26	</td>
	<td align="right">13,780.78	</td>
</tr>

</tbody>
</table>

