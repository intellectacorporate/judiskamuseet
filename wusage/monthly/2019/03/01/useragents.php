


<h2>Statistics for http://www.u5645470.fsdata.se, Month of 2019-3-1 to 2019-3-31</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 10 of 360 Browsers (User Agents),
Sorted by Access Count

</h3>
<p></p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Product</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th class="hogerstalld">1</th>
	<td>
	Safari/537.36		
	</td>
	<td align="right">58,534</td>
	<td align="right">37.75</td>
	<td align="right">22,440,290,493</td>
	<td align="right">49.23</td>
	<td align="right">3,107</td>
	<td align="right">34.82</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	Safari/604.1		
	</td>
	<td align="right">20,209</td>
	<td align="right">13.03</td>
	<td align="right">7,050,062,831</td>
	<td align="right">15.47</td>
	<td align="right">977</td>
	<td align="right">10.95</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	Mozilla/5.0 		
	</td>
	<td align="right">19,667</td>
	<td align="right">12.68</td>
	<td align="right">5,330,565,804</td>
	<td align="right">11.70</td>
	<td align="right">1,358</td>
	<td align="right">15.22</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	UptimeRobot/2.0		
	</td>
	<td align="right">7,519</td>
	<td align="right">4.85</td>
	<td align="right">294,283</td>
	<td align="right">0.00</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	Safari/537.36 Edge/17.17134		
	</td>
	<td align="right">7,168</td>
	<td align="right">4.62</td>
	<td align="right">2,025,769,972</td>
	<td align="right">4.44</td>
	<td align="right">153</td>
	<td align="right">1.72</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	Safari/605.1.15		
	</td>
	<td align="right">6,164</td>
	<td align="right">3.97</td>
	<td align="right">2,998,348,455</td>
	<td align="right">6.58</td>
	<td align="right">281</td>
	<td align="right">3.15</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	bingbot/2.0		
	</td>
	<td align="right">5,197</td>
	<td align="right">3.35</td>
	<td align="right">850,016,300</td>
	<td align="right">1.86</td>
	<td align="right">1,014</td>
	<td align="right">11.37</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	WordPress/5.0.4		
	</td>
	<td align="right">2,481</td>
	<td align="right">1.60</td>
	<td align="right">48,675</td>
	<td align="right">0.00</td>
	<td align="right">49</td>
	<td align="right">0.55</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	Safari/537.36 &#091;FB_IAB/FB4A;FBAV/212.0.0.28.110;&#093;		
	</td>
	<td align="right">2,135</td>
	<td align="right">1.38</td>
	<td align="right">469,160,879</td>
	<td align="right">1.03</td>
	<td align="right">113</td>
	<td align="right">1.28</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">10</th>
	<td>
	WordPress/5.0.3		
	</td>
	<td align="right">2,087</td>
	<td align="right">1.35</td>
	<td align="right">40,949</td>
	<td align="right">0.00</td>
	<td align="right">49</td>
	<td align="right">0.55</td>
</tr>

</tbody>
</table>

