


<h2>Statistics for http://www.u5645470.fsdata.se, Month of 2019-3-1 to 2019-3-31</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 20 of 7,698 Visitor Sites,
Sorted by Access Count

</h3>
<p></p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Site</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="jamnudda">
	<th class="hogerstalld">1</th>
	<td>
	63.143.42.244		
	</td>
	<td align="right">7,528</td>
	<td align="right">4.84</td>
	<td align="right">279,601</td>
	<td align="right">0.00</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	89.221.250.27		
	</td>
	<td align="right">5,979</td>
	<td align="right">3.85</td>
	<td align="right">117,467</td>
	<td align="right">0.00</td>
	<td align="right">110</td>
	<td align="right">1.24</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	46.59.25.88		
	</td>
	<td align="right">4,718</td>
	<td align="right">3.03</td>
	<td align="right">1,239,218,348</td>
	<td align="right">2.71</td>
	<td align="right">71</td>
	<td align="right">0.80</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	185.15.93.69		
	</td>
	<td align="right">2,293</td>
	<td align="right">1.47</td>
	<td align="right">39,099,201</td>
	<td align="right">0.09</td>
	<td align="right">167</td>
	<td align="right">1.87</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	148.160.250.14		
	</td>
	<td align="right">1,542</td>
	<td align="right">0.99</td>
	<td align="right">1,162,800,150</td>
	<td align="right">2.55</td>
	<td align="right">26</td>
	<td align="right">0.30</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	83.241.181.2		
	</td>
	<td align="right">883</td>
	<td align="right">0.57</td>
	<td align="right">206,582,156</td>
	<td align="right">0.45</td>
	<td align="right">25</td>
	<td align="right">0.28</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	90.229.237.5		
	</td>
	<td align="right">852</td>
	<td align="right">0.55</td>
	<td align="right">109,322,281</td>
	<td align="right">0.24</td>
	<td align="right">12</td>
	<td align="right">0.14</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	83.241.181.3		
	</td>
	<td align="right">832</td>
	<td align="right">0.54</td>
	<td align="right">250,173,590</td>
	<td align="right">0.55</td>
	<td align="right">27</td>
	<td align="right">0.30</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	83.241.181.4		
	</td>
	<td align="right">778</td>
	<td align="right">0.50</td>
	<td align="right">165,045,572</td>
	<td align="right">0.36</td>
	<td align="right">28</td>
	<td align="right">0.32</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">10</th>
	<td>
	195.3.144.185		
	</td>
	<td align="right">703</td>
	<td align="right">0.45</td>
	<td align="right">10,536,750</td>
	<td align="right">0.02</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">11</th>
	<td>
	23.100.232.233		
	</td>
	<td align="right">670</td>
	<td align="right">0.43</td>
	<td align="right">10,459,091</td>
	<td align="right">0.02</td>
	<td align="right">42</td>
	<td align="right">0.48</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">12</th>
	<td>
	unknown		
	</td>
	<td align="right">626</td>
	<td align="right">0.40</td>
	<td align="right">3,166,968</td>
	<td align="right">0.01</td>
	<td align="right">183</td>
	<td align="right">2.05</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">13</th>
	<td>
	216.244.66.227		
	</td>
	<td align="right">601</td>
	<td align="right">0.39</td>
	<td align="right">16,003,785</td>
	<td align="right">0.04</td>
	<td align="right">73</td>
	<td align="right">0.82</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">14</th>
	<td>
	109.102.111.71		
	</td>
	<td align="right">583</td>
	<td align="right">0.37</td>
	<td align="right">22,749,923</td>
	<td align="right">0.05</td>
	<td align="right">1</td>
	<td align="right">0.01</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">15</th>
	<td>
	17.58.99.252		
	</td>
	<td align="right">501</td>
	<td align="right">0.32</td>
	<td align="right">4,217,793</td>
	<td align="right">0.01</td>
	<td align="right">128</td>
	<td align="right">1.43</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">16</th>
	<td>
	66.249.64.30		
	</td>
	<td align="right">477</td>
	<td align="right">0.31</td>
	<td align="right">4,411,518</td>
	<td align="right">0.01</td>
	<td align="right">91</td>
	<td align="right">1.03</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">17</th>
	<td>
	66.249.64.29		
	</td>
	<td align="right">475</td>
	<td align="right">0.31</td>
	<td align="right">3,906,591</td>
	<td align="right">0.01</td>
	<td align="right">74</td>
	<td align="right">0.83</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">18</th>
	<td>
	217.115.50.82		
	</td>
	<td align="right">453</td>
	<td align="right">0.29</td>
	<td align="right">127,819,290</td>
	<td align="right">0.28</td>
	<td align="right">13</td>
	<td align="right">0.15</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">19</th>
	<td>
	66.249.64.18		
	</td>
	<td align="right">372</td>
	<td align="right">0.24</td>
	<td align="right">3,822,646</td>
	<td align="right">0.01</td>
	<td align="right">56</td>
	<td align="right">0.63</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">20</th>
	<td>
	46.17.185.87		
	</td>
	<td align="right">370</td>
	<td align="right">0.24</td>
	<td align="right">122,073,042</td>
	<td align="right">0.27</td>
	<td align="right">4</td>
	<td align="right">0.05</td>
</tr>

</tbody>
</table>

