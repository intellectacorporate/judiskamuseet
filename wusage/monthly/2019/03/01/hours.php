


<h2>Statistics for http://www.u5645470.fsdata.se, Month of 2019-3-1 to 2019-3-31</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Traffic by Hour</h3>
<p>Figures are averages for that hour
	of each day in the time period analyzed.</p>
<img src="h.png" width="468" height="351">
<br /><br />
<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Hour</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	<th scope='col'>Bits per Sec</th>
	<th scope='col'>Bytes per Sec</th>
	
	</tr>
</thead>
<tbody>
<tr class="jamnudda">
	<th align="right">00:00</th>
	<td align="right">109.03</td>
	<td align="right">2.17</td>
	<td align="right">17,776,244.03</td>
	<td align="right">1.21</td>
	<td align="right">4.44</td>
	<td align="right">1.53</td>
	<td align="right">39,502.76</td>
	<td align="right">4,937.85</td>
	
</tr>
<tr class="jamn">
	<th align="right">01:00</th>
	<td align="right">75.16</td>
	<td align="right">1.50</td>
	<td align="right">12,430,631.77</td>
	<td align="right">0.84</td>
	<td align="right">6.86</td>
	<td align="right">2.37</td>
	<td align="right">27,623.63</td>
	<td align="right">3,452.95</td>
	
</tr>
<tr class="udda">
	<th align="right">02:00</th>
	<td align="right">73.68</td>
	<td align="right">1.47</td>
	<td align="right">10,407,913.77</td>
	<td align="right">0.71</td>
	<td align="right">6.97</td>
	<td align="right">2.41</td>
	<td align="right">23,128.70</td>
	<td align="right">2,891.09</td>
	
</tr>
<tr class="jamn">
	<th align="right">03:00</th>
	<td align="right">93.10</td>
	<td align="right">1.86</td>
	<td align="right">9,528,310.19</td>
	<td align="right">0.65</td>
	<td align="right">5.53</td>
	<td align="right">1.91</td>
	<td align="right">21,174.02</td>
	<td align="right">2,646.75</td>
	
</tr>
<tr class="udda">
	<th align="right">04:00</th>
	<td align="right">71.97</td>
	<td align="right">1.43</td>
	<td align="right">9,803,690.32</td>
	<td align="right">0.67</td>
	<td align="right">6.68</td>
	<td align="right">2.31</td>
	<td align="right">21,785.98</td>
	<td align="right">2,723.25</td>
	
</tr>
<tr class="jamn">
	<th align="right">05:00</th>
	<td align="right">78.35</td>
	<td align="right">1.56</td>
	<td align="right">8,997,706.06</td>
	<td align="right">0.61</td>
	<td align="right">7.10</td>
	<td align="right">2.46</td>
	<td align="right">19,994.90</td>
	<td align="right">2,499.36</td>
	
</tr>
<tr class="udda">
	<th align="right">06:00</th>
	<td align="right">89.39</td>
	<td align="right">1.78</td>
	<td align="right">12,031,700.52</td>
	<td align="right">0.82</td>
	<td align="right">7.54</td>
	<td align="right">2.61</td>
	<td align="right">26,737.11</td>
	<td align="right">3,342.14</td>
	
</tr>
<tr class="jamn">
	<th align="right">07:00</th>
	<td align="right">114.90</td>
	<td align="right">2.29</td>
	<td align="right">24,381,776.35</td>
	<td align="right">1.66</td>
	<td align="right">8.33</td>
	<td align="right">2.88</td>
	<td align="right">54,181.73</td>
	<td align="right">6,772.72</td>
	
</tr>
<tr class="udda">
	<th align="right">08:00</th>
	<td align="right">223.81</td>
	<td align="right">4.46</td>
	<td align="right">71,858,713.58</td>
	<td align="right">4.88</td>
	<td align="right">9.54</td>
	<td align="right">3.30</td>
	<td align="right">159,686.03</td>
	<td align="right">19,960.75</td>
	
</tr>
<tr class="jamn">
	<th align="right">09:00</th>
	<td align="right">295.74</td>
	<td align="right">5.90</td>
	<td align="right">107,050,432.68</td>
	<td align="right">7.27</td>
	<td align="right">16.73</td>
	<td align="right">5.79</td>
	<td align="right">237,889.85</td>
	<td align="right">29,736.23</td>
	
</tr>
<tr class="udda">
	<th align="right">10:00</th>
	<td align="right">323.10</td>
	<td align="right">6.44</td>
	<td align="right">104,433,628.87</td>
	<td align="right">7.09</td>
	<td align="right">16.07</td>
	<td align="right">5.56</td>
	<td align="right">232,074.73</td>
	<td align="right">29,009.34</td>
	
</tr>
<tr class="jamn">
	<th align="right">11:00</th>
	<td align="right">285.32</td>
	<td align="right">5.69</td>
	<td align="right">107,718,311.52</td>
	<td align="right">7.32</td>
	<td align="right">15.93</td>
	<td align="right">5.51</td>
	<td align="right">239,374.03</td>
	<td align="right">29,921.75</td>
	
</tr>
<tr class="udda">
	<th align="right">12:00</th>
	<td align="right">290.55</td>
	<td align="right">5.79</td>
	<td align="right">92,515,132.29</td>
	<td align="right">6.28</td>
	<td align="right">15.34</td>
	<td align="right">5.31</td>
	<td align="right">205,589.18</td>
	<td align="right">25,698.65</td>
	
</tr>
<tr class="jamn">
	<th align="right">13:00</th>
	<td align="right">348.39</td>
	<td align="right">6.95</td>
	<td align="right">134,495,649.65</td>
	<td align="right">9.13</td>
	<td align="right">17.11</td>
	<td align="right">5.92</td>
	<td align="right">298,879.22</td>
	<td align="right">37,359.90</td>
	
</tr>
<tr class="udda">
	<th align="right">14:00</th>
	<td align="right">321.45</td>
	<td align="right">6.41</td>
	<td align="right">116,672,691.48</td>
	<td align="right">7.92</td>
	<td align="right">16.41</td>
	<td align="right">5.68</td>
	<td align="right">259,272.65</td>
	<td align="right">32,409.08</td>
	
</tr>
<tr class="jamn">
	<th align="right">15:00</th>
	<td align="right">372.87</td>
	<td align="right">7.43</td>
	<td align="right">124,325,286.13</td>
	<td align="right">8.44</td>
	<td align="right">17.62</td>
	<td align="right">6.09</td>
	<td align="right">276,278.41</td>
	<td align="right">34,534.80</td>
	
</tr>
<tr class="udda">
	<th align="right">16:00</th>
	<td align="right">328.29</td>
	<td align="right">6.55</td>
	<td align="right">83,513,658.19</td>
	<td align="right">5.67</td>
	<td align="right">17.26</td>
	<td align="right">5.97</td>
	<td align="right">185,585.91</td>
	<td align="right">23,198.24</td>
	
</tr>
<tr class="jamn">
	<th align="right">17:00</th>
	<td align="right">277.68</td>
	<td align="right">5.54</td>
	<td align="right">71,781,645.97</td>
	<td align="right">4.88</td>
	<td align="right">16.49</td>
	<td align="right">5.71</td>
	<td align="right">159,514.77</td>
	<td align="right">19,939.35</td>
	
</tr>
<tr class="udda">
	<th align="right">18:00</th>
	<td align="right">231.03</td>
	<td align="right">4.61</td>
	<td align="right">62,735,558.90</td>
	<td align="right">4.26</td>
	<td align="right">13.77</td>
	<td align="right">4.76</td>
	<td align="right">139,412.35</td>
	<td align="right">17,426.54</td>
	
</tr>
<tr class="jamn">
	<th align="right">19:00</th>
	<td align="right">230.39</td>
	<td align="right">4.59</td>
	<td align="right">68,047,711.55</td>
	<td align="right">4.62</td>
	<td align="right">12.96</td>
	<td align="right">4.48</td>
	<td align="right">151,217.14</td>
	<td align="right">18,902.14</td>
	
</tr>
<tr class="udda">
	<th align="right">20:00</th>
	<td align="right">216.39</td>
	<td align="right">4.31</td>
	<td align="right">61,826,393.26</td>
	<td align="right">4.20</td>
	<td align="right">12.57</td>
	<td align="right">4.35</td>
	<td align="right">137,391.99</td>
	<td align="right">17,174.00</td>
	
</tr>
<tr class="jamn">
	<th align="right">21:00</th>
	<td align="right">206.94</td>
	<td align="right">4.13</td>
	<td align="right">59,372,203.81</td>
	<td align="right">4.03</td>
	<td align="right">11.61</td>
	<td align="right">4.02</td>
	<td align="right">131,938.23</td>
	<td align="right">16,492.28</td>
	
</tr>
<tr class="udda">
	<th align="right">22:00</th>
	<td align="right">194.81</td>
	<td align="right">3.88</td>
	<td align="right">58,614,957.29</td>
	<td align="right">3.98</td>
	<td align="right">11.97</td>
	<td align="right">4.14</td>
	<td align="right">130,255.46</td>
	<td align="right">16,281.93</td>
	
</tr>
<tr class="jamn">
	<th align="right">23:00</th>
	<td align="right">163.10</td>
	<td align="right">3.25</td>
	<td align="right">42,096,926.00</td>
	<td align="right">2.86</td>
	<td align="right">14.25</td>
	<td align="right">4.93</td>
	<td align="right">93,548.72</td>
	<td align="right">11,693.59</td>
	
</tr>

</tbody>
</table>

