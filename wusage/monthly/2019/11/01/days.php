


<h2>Statistics for http://www.u5645470.fsdata.se, Month of 2019-11-1 to 2019-11-30</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Traffic by Day</h3>
<p>The continuous line represents a 7-day moving average.</p>
<img src="d.png" width="468" height="351">
<br /><br />


<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Date</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	<th scope='col'>Bits per Sec</th>
	<th scope='col'>Bytes per Sec</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th align="right">2019-11-1</th>
	<td align="right">15,407	</td>
	<td align="right">4.42	</td>
	<td align="right">1,132,110,123	</td>
	<td align="right">4.82	</td>
	<td align="right">697	</td>
	<td align="right">4.14	</td>
	<td align="right">104,825.01	</td>
	<td align="right">13,103.13	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-11-2</th>
	<td align="right">10,724	</td>
	<td align="right">3.08	</td>
	<td align="right">624,568,453	</td>
	<td align="right">2.66	</td>
	<td align="right">469	</td>
	<td align="right">2.79	</td>
	<td align="right">57,830.41	</td>
	<td align="right">7,228.80	</td>
</tr>
<tr class="udda">
	<th align="right">2019-11-3</th>
	<td align="right">9,976	</td>
	<td align="right">2.86	</td>
	<td align="right">799,166,460	</td>
	<td align="right">3.40	</td>
	<td align="right">465	</td>
	<td align="right">2.76	</td>
	<td align="right">73,996.89	</td>
	<td align="right">9,249.61	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-11-4</th>
	<td align="right">11,254	</td>
	<td align="right">3.23	</td>
	<td align="right">707,213,035	</td>
	<td align="right">3.01	</td>
	<td align="right">599	</td>
	<td align="right">3.56	</td>
	<td align="right">65,482.69	</td>
	<td align="right">8,185.34	</td>
</tr>
<tr class="udda">
	<th align="right">2019-11-5</th>
	<td align="right">11,956	</td>
	<td align="right">3.43	</td>
	<td align="right">905,179,091	</td>
	<td align="right">3.85	</td>
	<td align="right">571	</td>
	<td align="right">3.39	</td>
	<td align="right">83,812.88	</td>
	<td align="right">10,476.61	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-11-6</th>
	<td align="right">11,096	</td>
	<td align="right">3.18	</td>
	<td align="right">913,602,391	</td>
	<td align="right">3.89	</td>
	<td align="right">524	</td>
	<td align="right">3.11	</td>
	<td align="right">84,592.81	</td>
	<td align="right">10,574.10	</td>
</tr>
<tr class="udda">
	<th align="right">2019-11-7</th>
	<td align="right">11,142	</td>
	<td align="right">3.20	</td>
	<td align="right">714,124,976	</td>
	<td align="right">3.04	</td>
	<td align="right">611	</td>
	<td align="right">3.63	</td>
	<td align="right">66,122.68	</td>
	<td align="right">8,265.34	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-11-8</th>
	<td align="right">9,400	</td>
	<td align="right">2.70	</td>
	<td align="right">710,299,175	</td>
	<td align="right">3.02	</td>
	<td align="right">483	</td>
	<td align="right">2.87	</td>
	<td align="right">65,768.44	</td>
	<td align="right">8,221.06	</td>
</tr>
<tr class="udda">
	<th align="right">2019-11-9</th>
	<td align="right">9,861	</td>
	<td align="right">2.83	</td>
	<td align="right">725,961,188	</td>
	<td align="right">3.09	</td>
	<td align="right">478	</td>
	<td align="right">2.84	</td>
	<td align="right">67,218.63	</td>
	<td align="right">8,402.33	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-11-10</th>
	<td align="right">12,397	</td>
	<td align="right">3.56	</td>
	<td align="right">893,099,860	</td>
	<td align="right">3.80	</td>
	<td align="right">620	</td>
	<td align="right">3.68	</td>
	<td align="right">82,694.43	</td>
	<td align="right">10,336.80	</td>
</tr>
<tr class="udda">
	<th align="right">2019-11-11</th>
	<td align="right">12,899	</td>
	<td align="right">3.70	</td>
	<td align="right">861,938,220	</td>
	<td align="right">3.67	</td>
	<td align="right">535	</td>
	<td align="right">3.18	</td>
	<td align="right">79,809.09	</td>
	<td align="right">9,976.14	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-11-12</th>
	<td align="right">11,471	</td>
	<td align="right">3.29	</td>
	<td align="right">896,549,681	</td>
	<td align="right">3.82	</td>
	<td align="right">545	</td>
	<td align="right">3.24	</td>
	<td align="right">83,013.86	</td>
	<td align="right">10,376.73	</td>
</tr>
<tr class="udda">
	<th align="right">2019-11-13</th>
	<td align="right">11,545	</td>
	<td align="right">3.31	</td>
	<td align="right">807,912,367	</td>
	<td align="right">3.44	</td>
	<td align="right">664	</td>
	<td align="right">3.94	</td>
	<td align="right">74,806.70	</td>
	<td align="right">9,350.84	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-11-14</th>
	<td align="right">13,201	</td>
	<td align="right">3.79	</td>
	<td align="right">889,781,685	</td>
	<td align="right">3.79	</td>
	<td align="right">721	</td>
	<td align="right">4.28	</td>
	<td align="right">82,387.19	</td>
	<td align="right">10,298.40	</td>
</tr>
<tr class="udda">
	<th align="right">2019-11-15</th>
	<td align="right">11,145	</td>
	<td align="right">3.20	</td>
	<td align="right">794,748,350	</td>
	<td align="right">3.38	</td>
	<td align="right">559	</td>
	<td align="right">3.32	</td>
	<td align="right">73,587.81	</td>
	<td align="right">9,198.48	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-11-16</th>
	<td align="right">8,970	</td>
	<td align="right">2.57	</td>
	<td align="right">713,101,191	</td>
	<td align="right">3.04	</td>
	<td align="right">416	</td>
	<td align="right">2.47	</td>
	<td align="right">66,027.89	</td>
	<td align="right">8,253.49	</td>
</tr>
<tr class="udda">
	<th align="right">2019-11-17</th>
	<td align="right">9,571	</td>
	<td align="right">2.75	</td>
	<td align="right">647,034,618	</td>
	<td align="right">2.75	</td>
	<td align="right">575	</td>
	<td align="right">3.41	</td>
	<td align="right">59,910.61	</td>
	<td align="right">7,488.83	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-11-18</th>
	<td align="right">11,482	</td>
	<td align="right">3.29	</td>
	<td align="right">816,618,789	</td>
	<td align="right">3.48	</td>
	<td align="right">616	</td>
	<td align="right">3.66	</td>
	<td align="right">75,612.85	</td>
	<td align="right">9,451.61	</td>
</tr>
<tr class="udda">
	<th align="right">2019-11-19</th>
	<td align="right">10,989	</td>
	<td align="right">3.15	</td>
	<td align="right">740,635,226	</td>
	<td align="right">3.15	</td>
	<td align="right">602	</td>
	<td align="right">3.58	</td>
	<td align="right">68,577.34	</td>
	<td align="right">8,572.17	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-11-20</th>
	<td align="right">11,233	</td>
	<td align="right">3.22	</td>
	<td align="right">725,512,475	</td>
	<td align="right">3.09	</td>
	<td align="right">502	</td>
	<td align="right">2.98	</td>
	<td align="right">67,177.08	</td>
	<td align="right">8,397.14	</td>
</tr>
<tr class="udda">
	<th align="right">2019-11-21</th>
	<td align="right">14,055	</td>
	<td align="right">4.03	</td>
	<td align="right">868,924,259	</td>
	<td align="right">3.70	</td>
	<td align="right">641	</td>
	<td align="right">3.81	</td>
	<td align="right">80,455.95	</td>
	<td align="right">10,056.99	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-11-22</th>
	<td align="right">12,407	</td>
	<td align="right">3.56	</td>
	<td align="right">789,340,173	</td>
	<td align="right">3.36	</td>
	<td align="right">541	</td>
	<td align="right">3.21	</td>
	<td align="right">73,087.05	</td>
	<td align="right">9,135.88	</td>
</tr>
<tr class="udda">
	<th align="right">2019-11-23</th>
	<td align="right">10,158	</td>
	<td align="right">2.91	</td>
	<td align="right">515,756,129	</td>
	<td align="right">2.20	</td>
	<td align="right">515	</td>
	<td align="right">3.06	</td>
	<td align="right">47,755.20	</td>
	<td align="right">5,969.40	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-11-24</th>
	<td align="right">11,590	</td>
	<td align="right">3.33	</td>
	<td align="right">662,009,025	</td>
	<td align="right">2.82	</td>
	<td align="right">529	</td>
	<td align="right">3.14	</td>
	<td align="right">61,297.13	</td>
	<td align="right">7,662.14	</td>
</tr>
<tr class="udda">
	<th align="right">2019-11-25</th>
	<td align="right">14,151	</td>
	<td align="right">4.06	</td>
	<td align="right">960,536,515	</td>
	<td align="right">4.09	</td>
	<td align="right">618	</td>
	<td align="right">3.67	</td>
	<td align="right">88,938.57	</td>
	<td align="right">11,117.32	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-11-26</th>
	<td align="right">13,630	</td>
	<td align="right">3.91	</td>
	<td align="right">920,598,943	</td>
	<td align="right">3.92	</td>
	<td align="right">560	</td>
	<td align="right">3.33	</td>
	<td align="right">85,240.64	</td>
	<td align="right">10,655.08	</td>
</tr>
<tr class="udda">
	<th align="right">2019-11-27</th>
	<td align="right">14,709	</td>
	<td align="right">4.22	</td>
	<td align="right">919,788,861	</td>
	<td align="right">3.92	</td>
	<td align="right">652	</td>
	<td align="right">3.87	</td>
	<td align="right">85,165.64	</td>
	<td align="right">10,645.70	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-11-28</th>
	<td align="right">12,230	</td>
	<td align="right">3.51	</td>
	<td align="right">724,785,409	</td>
	<td align="right">3.09	</td>
	<td align="right">594	</td>
	<td align="right">3.53	</td>
	<td align="right">67,109.76	</td>
	<td align="right">8,388.72	</td>
</tr>
<tr class="udda">
	<th align="right">2019-11-29</th>
	<td align="right">10,534	</td>
	<td align="right">3.02	</td>
	<td align="right">610,602,340	</td>
	<td align="right">2.60	</td>
	<td align="right">474	</td>
	<td align="right">2.82	</td>
	<td align="right">56,537.25	</td>
	<td align="right">7,067.16	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-11-30</th>
	<td align="right">9,372	</td>
	<td align="right">2.69	</td>
	<td align="right">497,133,870	</td>
	<td align="right">2.12	</td>
	<td align="right">462	</td>
	<td align="right">2.74	</td>
	<td align="right">46,030.91	</td>
	<td align="right">5,753.86	</td>
</tr>

</tbody>
</table>

