


<h2>Statistics for http://www.u5645470.fsdata.se, Month of 2019-11-1 to 2019-11-30</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 10 of 31 File Types (Extensions), 
Sorted by Access Count

</h3>
<p>Individual file types as determined by file extensions. All URLs that
do not contain an extension are counted as directories.</p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Type</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th class="hogerstalld">1</th>
	<td>
	Directory (folder)		
	</td>
	<td align="right">137,788</td>
	<td align="right">39.53</td>
	<td align="right">1,317,519,842</td>
	<td align="right">5.61</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	js		
	</td>
	<td align="right">57,904</td>
	<td align="right">16.61</td>
	<td align="right">1,142,196,864</td>
	<td align="right">4.86</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	jpg		
	</td>
	<td align="right">54,905</td>
	<td align="right">15.75</td>
	<td align="right">16,827,681,317</td>
	<td align="right">71.64</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	png		
	</td>
	<td align="right">53,217</td>
	<td align="right">15.27</td>
	<td align="right">954,003,350</td>
	<td align="right">4.06</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	css		
	</td>
	<td align="right">20,990</td>
	<td align="right">6.02</td>
	<td align="right">1,220,098,419</td>
	<td align="right">5.19</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	php		
	</td>
	<td align="right">10,504</td>
	<td align="right">3.01</td>
	<td align="right">143,823,825</td>
	<td align="right">0.61</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	ttf		
	</td>
	<td align="right">7,655</td>
	<td align="right">2.20</td>
	<td align="right">287,289,641</td>
	<td align="right">1.22</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	jpeg		
	</td>
	<td align="right">2,732</td>
	<td align="right">0.78</td>
	<td align="right">1,147,095,350</td>
	<td align="right">4.88</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	txt		
	</td>
	<td align="right">1,794</td>
	<td align="right">0.51</td>
	<td align="right">117,947</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">10</th>
	<td>
	ico		
	</td>
	<td align="right">251</td>
	<td align="right">0.07</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>

</tbody>
</table>

