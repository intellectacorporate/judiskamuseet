


<h2>Statistics for http://www.u5645470.fsdata.se, Month of 2019-11-1 to 2019-11-30</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 10 of 13 Operating Systems,
Sorted by Access Count

</h3>
<p></p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Product</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th class="hogerstalld">1</th>
	<td>
	Mac OS X		
	</td>
	<td align="right">199,092</td>
	<td align="right">57.23</td>
	<td align="right">19,617,393,915</td>
	<td align="right">83.93</td>
	<td align="right">10,791</td>
	<td align="right">64.09</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	Other		
	</td>
	<td align="right">128,318</td>
	<td align="right">36.89</td>
	<td align="right">2,511,869,982</td>
	<td align="right">10.75</td>
	<td align="right">3,342</td>
	<td align="right">19.85</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	Windows NT		
	</td>
	<td align="right">11,714</td>
	<td align="right">3.37</td>
	<td align="right">1,101,337,738</td>
	<td align="right">4.71</td>
	<td align="right">562</td>
	<td align="right">3.34</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	Linux		
	</td>
	<td align="right">7,722</td>
	<td align="right">2.22</td>
	<td align="right">77,053,542</td>
	<td align="right">0.33</td>
	<td align="right">2,120</td>
	<td align="right">12.59</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	Windows XP		
	</td>
	<td align="right">442</td>
	<td align="right">0.13</td>
	<td align="right">22,440,854</td>
	<td align="right">0.10</td>
	<td align="right">12</td>
	<td align="right">0.07</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	Windows Codename Longhorn		
	</td>
	<td align="right">294</td>
	<td align="right">0.08</td>
	<td align="right">31,106,310</td>
	<td align="right">0.13</td>
	<td align="right">9</td>
	<td align="right">0.06</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	Windows 2003		
	</td>
	<td align="right">130</td>
	<td align="right">0.04</td>
	<td align="right">3,841,749</td>
	<td align="right">0.02</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	Windows 2000		
	</td>
	<td align="right">55</td>
	<td align="right">0.02</td>
	<td align="right">2,903,022</td>
	<td align="right">0.01</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	Windows 95		
	</td>
	<td align="right">41</td>
	<td align="right">0.01</td>
	<td align="right">2,459,692</td>
	<td align="right">0.01</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">10</th>
	<td>
	Windows Me		
	</td>
	<td align="right">17</td>
	<td align="right">0.00</td>
	<td align="right">938,133</td>
	<td align="right">0.00</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>

</tbody>
</table>

