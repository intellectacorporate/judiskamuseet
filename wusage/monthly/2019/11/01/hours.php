


<h2>Statistics for http://www.u5645470.fsdata.se, Month of 2019-11-1 to 2019-11-30</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Traffic by Hour</h3>
<p>Figures are averages for that hour
	of each day in the time period analyzed.</p>
<img src="h.png" width="468" height="351">
<br /><br />
<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Hour</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	<th scope='col'>Bits per Sec</th>
	<th scope='col'>Bytes per Sec</th>
	
	</tr>
</thead>
<tbody>
<tr class="jamnudda">
	<th align="right">00:00</th>
	<td align="right">303.13</td>
	<td align="right">2.61</td>
	<td align="right">14,494,009.27</td>
	<td align="right">1.85</td>
	<td align="right">9.08</td>
	<td align="right">1.62</td>
	<td align="right">32,208.91</td>
	<td align="right">4,026.11</td>
	
</tr>
<tr class="jamn">
	<th align="right">01:00</th>
	<td align="right">252.47</td>
	<td align="right">2.17</td>
	<td align="right">10,986,612.13</td>
	<td align="right">1.40</td>
	<td align="right">12.29</td>
	<td align="right">2.19</td>
	<td align="right">24,414.69</td>
	<td align="right">3,051.84</td>
	
</tr>
<tr class="udda">
	<th align="right">02:00</th>
	<td align="right">254.03</td>
	<td align="right">2.19</td>
	<td align="right">7,820,175.83</td>
	<td align="right">1.00</td>
	<td align="right">10.61</td>
	<td align="right">1.89</td>
	<td align="right">17,378.17</td>
	<td align="right">2,172.27</td>
	
</tr>
<tr class="jamn">
	<th align="right">03:00</th>
	<td align="right">230.60</td>
	<td align="right">1.98</td>
	<td align="right">5,097,059.67</td>
	<td align="right">0.65</td>
	<td align="right">10.97</td>
	<td align="right">1.96</td>
	<td align="right">11,326.80</td>
	<td align="right">1,415.85</td>
	
</tr>
<tr class="udda">
	<th align="right">04:00</th>
	<td align="right">235.40</td>
	<td align="right">2.03</td>
	<td align="right">6,078,799.87</td>
	<td align="right">0.78</td>
	<td align="right">10.43</td>
	<td align="right">1.86</td>
	<td align="right">13,508.44</td>
	<td align="right">1,688.56</td>
	
</tr>
<tr class="jamn">
	<th align="right">05:00</th>
	<td align="right">228.53</td>
	<td align="right">1.97</td>
	<td align="right">6,507,770.00</td>
	<td align="right">0.83</td>
	<td align="right">11.59</td>
	<td align="right">2.07</td>
	<td align="right">14,461.71</td>
	<td align="right">1,807.71</td>
	
</tr>
<tr class="udda">
	<th align="right">06:00</th>
	<td align="right">269.37</td>
	<td align="right">2.32</td>
	<td align="right">10,097,126.87</td>
	<td align="right">1.29</td>
	<td align="right">11.78</td>
	<td align="right">2.10</td>
	<td align="right">22,438.06</td>
	<td align="right">2,804.76</td>
	
</tr>
<tr class="jamn">
	<th align="right">07:00</th>
	<td align="right">326.60</td>
	<td align="right">2.81</td>
	<td align="right">20,062,684.60</td>
	<td align="right">2.56</td>
	<td align="right">14.68</td>
	<td align="right">2.62</td>
	<td align="right">44,583.74</td>
	<td align="right">5,572.97</td>
	
</tr>
<tr class="udda">
	<th align="right">08:00</th>
	<td align="right">442.13</td>
	<td align="right">3.81</td>
	<td align="right">29,877,775.50</td>
	<td align="right">3.82</td>
	<td align="right">19.27</td>
	<td align="right">3.43</td>
	<td align="right">66,395.06</td>
	<td align="right">8,299.38</td>
	
</tr>
<tr class="jamn">
	<th align="right">09:00</th>
	<td align="right">675.07</td>
	<td align="right">5.81</td>
	<td align="right">52,730,800.97</td>
	<td align="right">6.73</td>
	<td align="right">32.47</td>
	<td align="right">5.79</td>
	<td align="right">117,179.56</td>
	<td align="right">14,647.44</td>
	
</tr>
<tr class="udda">
	<th align="right">10:00</th>
	<td align="right">736.20</td>
	<td align="right">6.34</td>
	<td align="right">59,011,026.33</td>
	<td align="right">7.54</td>
	<td align="right">31.90</td>
	<td align="right">5.68</td>
	<td align="right">131,135.61</td>
	<td align="right">16,391.95</td>
	
</tr>
<tr class="jamn">
	<th align="right">11:00</th>
	<td align="right">701.23</td>
	<td align="right">6.04</td>
	<td align="right">58,137,407.03</td>
	<td align="right">7.43</td>
	<td align="right">34.13</td>
	<td align="right">6.08</td>
	<td align="right">129,194.24</td>
	<td align="right">16,149.28</td>
	
</tr>
<tr class="udda">
	<th align="right">12:00</th>
	<td align="right">600.80</td>
	<td align="right">5.17</td>
	<td align="right">44,979,396.67</td>
	<td align="right">5.74</td>
	<td align="right">31.02</td>
	<td align="right">5.53</td>
	<td align="right">99,954.21</td>
	<td align="right">12,494.28</td>
	
</tr>
<tr class="jamn">
	<th align="right">13:00</th>
	<td align="right">718.17</td>
	<td align="right">6.18</td>
	<td align="right">55,941,209.70</td>
	<td align="right">7.14</td>
	<td align="right">32.54</td>
	<td align="right">5.80</td>
	<td align="right">124,313.80</td>
	<td align="right">15,539.22</td>
	
</tr>
<tr class="udda">
	<th align="right">14:00</th>
	<td align="right">636.10</td>
	<td align="right">5.47</td>
	<td align="right">46,524,437.13</td>
	<td align="right">5.94</td>
	<td align="right">30.83</td>
	<td align="right">5.49</td>
	<td align="right">103,387.64</td>
	<td align="right">12,923.45</td>
	
</tr>
<tr class="jamn">
	<th align="right">15:00</th>
	<td align="right">614.43</td>
	<td align="right">5.29</td>
	<td align="right">47,534,382.87</td>
	<td align="right">6.07</td>
	<td align="right">28.21</td>
	<td align="right">5.03</td>
	<td align="right">105,631.96</td>
	<td align="right">13,204.00</td>
	
</tr>
<tr class="udda">
	<th align="right">16:00</th>
	<td align="right">697.70</td>
	<td align="right">6.01</td>
	<td align="right">48,638,773.60</td>
	<td align="right">6.21</td>
	<td align="right">27.92</td>
	<td align="right">4.97</td>
	<td align="right">108,086.16</td>
	<td align="right">13,510.77</td>
	
</tr>
<tr class="jamn">
	<th align="right">17:00</th>
	<td align="right">577.00</td>
	<td align="right">4.97</td>
	<td align="right">39,595,965.67</td>
	<td align="right">5.06</td>
	<td align="right">30.68</td>
	<td align="right">5.47</td>
	<td align="right">87,991.03</td>
	<td align="right">10,998.88</td>
	
</tr>
<tr class="udda">
	<th align="right">18:00</th>
	<td align="right">555.63</td>
	<td align="right">4.78</td>
	<td align="right">38,411,272.00</td>
	<td align="right">4.91</td>
	<td align="right">30.03</td>
	<td align="right">5.35</td>
	<td align="right">85,358.38</td>
	<td align="right">10,669.80</td>
	
</tr>
<tr class="jamn">
	<th align="right">19:00</th>
	<td align="right">536.03</td>
	<td align="right">4.61</td>
	<td align="right">40,473,064.13</td>
	<td align="right">5.17</td>
	<td align="right">27.86</td>
	<td align="right">4.96</td>
	<td align="right">89,940.14</td>
	<td align="right">11,242.52</td>
	
</tr>
<tr class="udda">
	<th align="right">20:00</th>
	<td align="right">536.83</td>
	<td align="right">4.62</td>
	<td align="right">36,622,191.67</td>
	<td align="right">4.68</td>
	<td align="right">28.60</td>
	<td align="right">5.10</td>
	<td align="right">81,382.65</td>
	<td align="right">10,172.83</td>
	
</tr>
<tr class="jamn">
	<th align="right">21:00</th>
	<td align="right">575.40</td>
	<td align="right">4.95</td>
	<td align="right">42,590,209.63</td>
	<td align="right">5.44</td>
	<td align="right">27.51</td>
	<td align="right">4.90</td>
	<td align="right">94,644.91</td>
	<td align="right">11,830.61</td>
	
</tr>
<tr class="udda">
	<th align="right">22:00</th>
	<td align="right">515.53</td>
	<td align="right">4.44</td>
	<td align="right">37,151,462.97</td>
	<td align="right">4.75</td>
	<td align="right">27.43</td>
	<td align="right">4.89</td>
	<td align="right">82,558.81</td>
	<td align="right">10,319.85</td>
	
</tr>
<tr class="jamn">
	<th align="right">23:00</th>
	<td align="right">400.10</td>
	<td align="right">3.44</td>
	<td align="right">23,590,815.17</td>
	<td align="right">3.01</td>
	<td align="right">29.42</td>
	<td align="right">5.24</td>
	<td align="right">52,424.03</td>
	<td align="right">6,553.00</td>
	
</tr>

</tbody>
</table>

