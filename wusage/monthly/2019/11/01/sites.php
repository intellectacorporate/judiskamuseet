


<h2>Statistics for http://www.u5645470.fsdata.se, Month of 2019-11-1 to 2019-11-30</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 20 of 13,206 Visitor Sites,
Sorted by Access Count

</h3>
<p></p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Site</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th class="hogerstalld">1</th>
	<td>
	83.170.113.210		
	</td>
	<td align="right">9,388</td>
	<td align="right">2.69</td>
	<td align="right">80,594,906</td>
	<td align="right">0.34</td>
	<td align="right">11</td>
	<td align="right">0.07</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	94.247.174.83		
	</td>
	<td align="right">9,275</td>
	<td align="right">2.66</td>
	<td align="right">79,408,650</td>
	<td align="right">0.34</td>
	<td align="right">11</td>
	<td align="right">0.07</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	85.195.116.134		
	</td>
	<td align="right">8,272</td>
	<td align="right">2.37</td>
	<td align="right">70,520,094</td>
	<td align="right">0.30</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	185.136.156.82		
	</td>
	<td align="right">8,265</td>
	<td align="right">2.37</td>
	<td align="right">70,519,358</td>
	<td align="right">0.30</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	185.39.146.215		
	</td>
	<td align="right">8,253</td>
	<td align="right">2.37</td>
	<td align="right">70,437,145</td>
	<td align="right">0.30</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	94.75.211.74		
	</td>
	<td align="right">7,965</td>
	<td align="right">2.29</td>
	<td align="right">67,910,178</td>
	<td align="right">0.29</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	46.59.25.88		
	</td>
	<td align="right">7,080</td>
	<td align="right">2.03</td>
	<td align="right">493,917,242</td>
	<td align="right">2.10</td>
	<td align="right">177</td>
	<td align="right">1.05</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	46.20.45.18		
	</td>
	<td align="right">6,929</td>
	<td align="right">1.99</td>
	<td align="right">59,412,853</td>
	<td align="right">0.25</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	95.141.32.46		
	</td>
	<td align="right">5,371</td>
	<td align="right">1.54</td>
	<td align="right">46,359,755</td>
	<td align="right">0.20</td>
	<td align="right">6</td>
	<td align="right">0.04</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">10</th>
	<td>
	63.143.42.242		
	</td>
	<td align="right">5,351</td>
	<td align="right">1.54</td>
	<td align="right">83,174</td>
	<td align="right">0.00</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">11</th>
	<td>
	185.246.208.82		
	</td>
	<td align="right">4,774</td>
	<td align="right">1.37</td>
	<td align="right">41,760,350</td>
	<td align="right">0.18</td>
	<td align="right">7</td>
	<td align="right">0.05</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">12</th>
	<td>
	178.255.153.2		
	</td>
	<td align="right">3,808</td>
	<td align="right">1.09</td>
	<td align="right">32,838,363</td>
	<td align="right">0.14</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">13</th>
	<td>
	82.103.136.16		
	</td>
	<td align="right">3,561</td>
	<td align="right">1.02</td>
	<td align="right">31,043,905</td>
	<td align="right">0.13</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">14</th>
	<td>
	46.246.122.10		
	</td>
	<td align="right">3,528</td>
	<td align="right">1.01</td>
	<td align="right">29,967,349</td>
	<td align="right">0.13</td>
	<td align="right">4</td>
	<td align="right">0.03</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">15</th>
	<td>
	81.17.62.205		
	</td>
	<td align="right">3,055</td>
	<td align="right">0.88</td>
	<td align="right">25,647,616</td>
	<td align="right">0.11</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">16</th>
	<td>
	185.93.3.92		
	</td>
	<td align="right">2,695</td>
	<td align="right">0.77</td>
	<td align="right">23,633,049</td>
	<td align="right">0.10</td>
	<td align="right">5</td>
	<td align="right">0.03</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">17</th>
	<td>
	90.229.237.5		
	</td>
	<td align="right">2,322</td>
	<td align="right">0.67</td>
	<td align="right">55,857,551</td>
	<td align="right">0.24</td>
	<td align="right">23</td>
	<td align="right">0.14</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">18</th>
	<td>
	185.152.65.167		
	</td>
	<td align="right">2,259</td>
	<td align="right">0.65</td>
	<td align="right">19,144,294</td>
	<td align="right">0.08</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">19</th>
	<td>
	193.183.253.33		
	</td>
	<td align="right">1,871</td>
	<td align="right">0.54</td>
	<td align="right">146,834,002</td>
	<td align="right">0.63</td>
	<td align="right">44</td>
	<td align="right">0.26</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">20</th>
	<td>
	185.39.146.214		
	</td>
	<td align="right">1,362</td>
	<td align="right">0.39</td>
	<td align="right">12,353,340</td>
	<td align="right">0.05</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>

</tbody>
</table>

