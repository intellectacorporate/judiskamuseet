


<h2>Statistics for http://www.u5645470.fsdata.se, Month of 2019-7-1 to 2019-7-31</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Traffic by Day</h3>
<p>The continuous line represents a 7-day moving average.</p>
<img src="d.png" width="468" height="351">
<br /><br />


<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Date</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	<th scope='col'>Bits per Sec</th>
	<th scope='col'>Bytes per Sec</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th align="right">2019-7-1</th>
	<td align="right">14,262	</td>
	<td align="right">5.39	</td>
	<td align="right">1,787,733,978	</td>
	<td align="right">4.07	</td>
	<td align="right">602	</td>
	<td align="right">4.72	</td>
	<td align="right">165,530.92	</td>
	<td align="right">20,691.37	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-7-2</th>
	<td align="right">14,294	</td>
	<td align="right">5.40	</td>
	<td align="right">2,296,409,495	</td>
	<td align="right">5.22	</td>
	<td align="right">545	</td>
	<td align="right">4.28	</td>
	<td align="right">212,630.51	</td>
	<td align="right">26,578.81	</td>
</tr>
<tr class="udda">
	<th align="right">2019-7-3</th>
	<td align="right">10,551	</td>
	<td align="right">3.99	</td>
	<td align="right">2,175,708,007	</td>
	<td align="right">4.95	</td>
	<td align="right">495	</td>
	<td align="right">3.88	</td>
	<td align="right">201,454.45	</td>
	<td align="right">25,181.81	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-7-4</th>
	<td align="right">10,017	</td>
	<td align="right">3.79	</td>
	<td align="right">1,581,754,798	</td>
	<td align="right">3.60	</td>
	<td align="right">405	</td>
	<td align="right">3.18	</td>
	<td align="right">146,458.78	</td>
	<td align="right">18,307.35	</td>
</tr>
<tr class="udda">
	<th align="right">2019-7-5</th>
	<td align="right">10,162	</td>
	<td align="right">3.84	</td>
	<td align="right">1,800,245,263	</td>
	<td align="right">4.10	</td>
	<td align="right">450	</td>
	<td align="right">3.53	</td>
	<td align="right">166,689.38	</td>
	<td align="right">20,836.17	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-7-6</th>
	<td align="right">8,557	</td>
	<td align="right">3.23	</td>
	<td align="right">1,398,721,708	</td>
	<td align="right">3.18	</td>
	<td align="right">406	</td>
	<td align="right">3.18	</td>
	<td align="right">129,511.27	</td>
	<td align="right">16,188.91	</td>
</tr>
<tr class="udda">
	<th align="right">2019-7-7</th>
	<td align="right">10,096	</td>
	<td align="right">3.82	</td>
	<td align="right">1,936,607,482	</td>
	<td align="right">4.41	</td>
	<td align="right">431	</td>
	<td align="right">3.38	</td>
	<td align="right">179,315.51	</td>
	<td align="right">22,414.44	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-7-8</th>
	<td align="right">9,125	</td>
	<td align="right">3.45	</td>
	<td align="right">1,473,473,257	</td>
	<td align="right">3.35	</td>
	<td align="right">450	</td>
	<td align="right">3.53	</td>
	<td align="right">136,432.71	</td>
	<td align="right">17,054.09	</td>
</tr>
<tr class="udda">
	<th align="right">2019-7-9</th>
	<td align="right">10,415	</td>
	<td align="right">3.94	</td>
	<td align="right">2,040,679,975	</td>
	<td align="right">4.64	</td>
	<td align="right">465	</td>
	<td align="right">3.65	</td>
	<td align="right">188,951.85	</td>
	<td align="right">23,618.98	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-7-10</th>
	<td align="right">8,251	</td>
	<td align="right">3.12	</td>
	<td align="right">1,276,934,353	</td>
	<td align="right">2.90	</td>
	<td align="right">373	</td>
	<td align="right">2.93	</td>
	<td align="right">118,234.66	</td>
	<td align="right">14,779.33	</td>
</tr>
<tr class="udda">
	<th align="right">2019-7-11</th>
	<td align="right">8,869	</td>
	<td align="right">3.35	</td>
	<td align="right">1,706,103,153	</td>
	<td align="right">3.88	</td>
	<td align="right">406	</td>
	<td align="right">3.18	</td>
	<td align="right">157,972.51	</td>
	<td align="right">19,746.56	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-7-12</th>
	<td align="right">7,505	</td>
	<td align="right">2.84	</td>
	<td align="right">1,251,484,997	</td>
	<td align="right">2.85	</td>
	<td align="right">343	</td>
	<td align="right">2.69	</td>
	<td align="right">115,878.24	</td>
	<td align="right">14,484.78	</td>
</tr>
<tr class="udda">
	<th align="right">2019-7-13</th>
	<td align="right">7,263	</td>
	<td align="right">2.75	</td>
	<td align="right">1,408,653,961	</td>
	<td align="right">3.20	</td>
	<td align="right">365	</td>
	<td align="right">2.86	</td>
	<td align="right">130,430.92	</td>
	<td align="right">16,303.87	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-7-14</th>
	<td align="right">5,653	</td>
	<td align="right">2.14	</td>
	<td align="right">836,953,607	</td>
	<td align="right">1.90	</td>
	<td align="right">305	</td>
	<td align="right">2.39	</td>
	<td align="right">77,495.70	</td>
	<td align="right">9,686.96	</td>
</tr>
<tr class="udda">
	<th align="right">2019-7-15</th>
	<td align="right">7,292	</td>
	<td align="right">2.76	</td>
	<td align="right">1,337,397,798	</td>
	<td align="right">3.04	</td>
	<td align="right">353	</td>
	<td align="right">2.77	</td>
	<td align="right">123,833.13	</td>
	<td align="right">15,479.14	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-7-16</th>
	<td align="right">8,431	</td>
	<td align="right">3.19	</td>
	<td align="right">1,657,682,371	</td>
	<td align="right">3.77	</td>
	<td align="right">422	</td>
	<td align="right">3.31	</td>
	<td align="right">153,489.11	</td>
	<td align="right">19,186.14	</td>
</tr>
<tr class="udda">
	<th align="right">2019-7-17</th>
	<td align="right">8,251	</td>
	<td align="right">3.12	</td>
	<td align="right">1,611,373,890	</td>
	<td align="right">3.67	</td>
	<td align="right">385	</td>
	<td align="right">3.02	</td>
	<td align="right">149,201.29	</td>
	<td align="right">18,650.16	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-7-18</th>
	<td align="right">6,475	</td>
	<td align="right">2.45	</td>
	<td align="right">1,112,012,375	</td>
	<td align="right">2.53	</td>
	<td align="right">325	</td>
	<td align="right">2.55	</td>
	<td align="right">102,964.11	</td>
	<td align="right">12,870.51	</td>
</tr>
<tr class="udda">
	<th align="right">2019-7-19</th>
	<td align="right">5,549	</td>
	<td align="right">2.10	</td>
	<td align="right">1,015,741,197	</td>
	<td align="right">2.31	</td>
	<td align="right">307	</td>
	<td align="right">2.41	</td>
	<td align="right">94,050.11	</td>
	<td align="right">11,756.26	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-7-20</th>
	<td align="right">5,298	</td>
	<td align="right">2.00	</td>
	<td align="right">1,420,529,001	</td>
	<td align="right">3.23	</td>
	<td align="right">275	</td>
	<td align="right">2.16	</td>
	<td align="right">131,530.46	</td>
	<td align="right">16,441.31	</td>
</tr>
<tr class="udda">
	<th align="right">2019-7-21</th>
	<td align="right">5,476	</td>
	<td align="right">2.07	</td>
	<td align="right">918,949,067	</td>
	<td align="right">2.09	</td>
	<td align="right">301	</td>
	<td align="right">2.36	</td>
	<td align="right">85,087.88	</td>
	<td align="right">10,635.98	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-7-22</th>
	<td align="right">16,003	</td>
	<td align="right">6.05	</td>
	<td align="right">1,894,627,565	</td>
	<td align="right">4.31	</td>
	<td align="right">698	</td>
	<td align="right">5.48	</td>
	<td align="right">175,428.48	</td>
	<td align="right">21,928.56	</td>
</tr>
<tr class="udda">
	<th align="right">2019-7-23</th>
	<td align="right">10,348	</td>
	<td align="right">3.91	</td>
	<td align="right">1,392,058,869	</td>
	<td align="right">3.17	</td>
	<td align="right">413	</td>
	<td align="right">3.24	</td>
	<td align="right">128,894.34	</td>
	<td align="right">16,111.79	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-7-24</th>
	<td align="right">6,179	</td>
	<td align="right">2.34	</td>
	<td align="right">1,004,411,296	</td>
	<td align="right">2.28	</td>
	<td align="right">334	</td>
	<td align="right">2.62	</td>
	<td align="right">93,001.05	</td>
	<td align="right">11,625.13	</td>
</tr>
<tr class="udda">
	<th align="right">2019-7-25</th>
	<td align="right">6,178	</td>
	<td align="right">2.34	</td>
	<td align="right">957,022,873	</td>
	<td align="right">2.18	</td>
	<td align="right">272	</td>
	<td align="right">2.13	</td>
	<td align="right">88,613.23	</td>
	<td align="right">11,076.65	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-7-26</th>
	<td align="right">8,414	</td>
	<td align="right">3.18	</td>
	<td align="right">939,407,403	</td>
	<td align="right">2.14	</td>
	<td align="right">929	</td>
	<td align="right">7.29	</td>
	<td align="right">86,982.17	</td>
	<td align="right">10,872.77	</td>
</tr>
<tr class="udda">
	<th align="right">2019-7-27</th>
	<td align="right">5,474	</td>
	<td align="right">2.07	</td>
	<td align="right">741,333,530	</td>
	<td align="right">1.69	</td>
	<td align="right">259	</td>
	<td align="right">2.03	</td>
	<td align="right">68,641.99	</td>
	<td align="right">8,580.25	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-7-28</th>
	<td align="right">5,043	</td>
	<td align="right">1.91	</td>
	<td align="right">781,719,480	</td>
	<td align="right">1.78	</td>
	<td align="right">265	</td>
	<td align="right">2.08	</td>
	<td align="right">72,381.43	</td>
	<td align="right">9,047.68	</td>
</tr>
<tr class="udda">
	<th align="right">2019-7-29</th>
	<td align="right">7,949	</td>
	<td align="right">3.00	</td>
	<td align="right">1,215,260,033	</td>
	<td align="right">2.76	</td>
	<td align="right">354	</td>
	<td align="right">2.78	</td>
	<td align="right">112,524.08	</td>
	<td align="right">14,065.51	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-7-30</th>
	<td align="right">8,775	</td>
	<td align="right">3.32	</td>
	<td align="right">1,532,632,015	</td>
	<td align="right">3.49	</td>
	<td align="right">407	</td>
	<td align="right">3.19	</td>
	<td align="right">141,910.37	</td>
	<td align="right">17,738.80	</td>
</tr>
<tr class="udda">
	<th align="right">2019-7-31</th>
	<td align="right">8,381	</td>
	<td align="right">3.17	</td>
	<td align="right">1,456,968,062	</td>
	<td align="right">3.31	</td>
	<td align="right">408	</td>
	<td align="right">3.20	</td>
	<td align="right">134,904.45	</td>
	<td align="right">16,863.06	</td>
</tr>

</tbody>
</table>

