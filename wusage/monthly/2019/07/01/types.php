


<h2>Statistics for http://www.u5645470.fsdata.se, Month of 2019-7-1 to 2019-7-31</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 10 of 24 File Types (Extensions), 
Sorted by Access Count

</h3>
<p>Individual file types as determined by file extensions. All URLs that
do not contain an extension are counted as directories.</p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Type</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th class="hogerstalld">1</th>
	<td>
	js		
	</td>
	<td align="right">55,791</td>
	<td align="right">21.09</td>
	<td align="right">744,234,475</td>
	<td align="right">1.69</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	Directory (folder)		
	</td>
	<td align="right">53,442</td>
	<td align="right">20.20</td>
	<td align="right">496,260,043</td>
	<td align="right">1.13</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	css		
	</td>
	<td align="right">47,246</td>
	<td align="right">17.86</td>
	<td align="right">738,782,331</td>
	<td align="right">1.68</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	jpg		
	</td>
	<td align="right">46,327</td>
	<td align="right">17.51</td>
	<td align="right">37,631,776,266</td>
	<td align="right">85.60</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	png		
	</td>
	<td align="right">37,539</td>
	<td align="right">14.19</td>
	<td align="right">1,890,593,310</td>
	<td align="right">4.30</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	php		
	</td>
	<td align="right">12,065</td>
	<td align="right">4.56</td>
	<td align="right">173,864,705</td>
	<td align="right">0.40</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	ttf		
	</td>
	<td align="right">6,058</td>
	<td align="right">2.29</td>
	<td align="right">537,361,321</td>
	<td align="right">1.22</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	txt		
	</td>
	<td align="right">3,150</td>
	<td align="right">1.19</td>
	<td align="right">214,992</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	jpeg		
	</td>
	<td align="right">892</td>
	<td align="right">0.34</td>
	<td align="right">738,445,260</td>
	<td align="right">1.68</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">10</th>
	<td>
	ico		
	</td>
	<td align="right">576</td>
	<td align="right">0.22</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>

</tbody>
</table>

