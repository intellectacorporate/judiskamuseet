


<h2>Statistics for http://www.u5645470.fsdata.se, Month of 2019-7-1 to 2019-7-31</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 20 of 359 Referring Sites,
Sorted by Access Count

</h3>
<p></p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Referring Site</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="jamnudda">
	<th class="hogerstalld">1</th>
	<td>
	<a href="https://judiskamuseet.se">
	https://judiskamuseet.se</a>
	</td>
	<td align="right">186,725</td>
	<td align="right">89.37</td>
	<td align="right">39,137,833,043</td>
	<td align="right">97.80</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	<a href="http://judiskamuseet.se">
	http://judiskamuseet.se</a>
	</td>
	<td align="right">12,631</td>
	<td align="right">6.05</td>
	<td align="right">488,635,938</td>
	<td align="right">1.22</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	<a href="https://www.google.com">
	https://www.google.com</a>
	</td>
	<td align="right">3,757</td>
	<td align="right">1.80</td>
	<td align="right">160,755,628</td>
	<td align="right">0.40</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	<a href="https://www.google.se">
	https://www.google.se</a>
	</td>
	<td align="right">2,430</td>
	<td align="right">1.16</td>
	<td align="right">111,451,284</td>
	<td align="right">0.28</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	<a href="http://m.facebook.com">
	http://m.facebook.com</a>
	</td>
	<td align="right">476</td>
	<td align="right">0.23</td>
	<td align="right">5,815,997</td>
	<td align="right">0.01</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	<a href="https://www.google.de">
	https://www.google.de</a>
	</td>
	<td align="right">435</td>
	<td align="right">0.21</td>
	<td align="right">6,044,605</td>
	<td align="right">0.02</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	<a href="http://www.google.com">
	http://www.google.com</a>
	</td>
	<td align="right">391</td>
	<td align="right">0.19</td>
	<td align="right">3,208,983</td>
	<td align="right">0.01</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	<a href="https://www.bing.com">
	https://www.bing.com</a>
	</td>
	<td align="right">214</td>
	<td align="right">0.10</td>
	<td align="right">75,011,177</td>
	<td align="right">0.19</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	<a href="https://www.facebook.com">
	https://www.facebook.com</a>
	</td>
	<td align="right">97</td>
	<td align="right">0.05</td>
	<td align="right">1,172,001</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">10</th>
	<td>
	<a href="https://m.facebook.com">
	https://m.facebook.com</a>
	</td>
	<td align="right">82</td>
	<td align="right">0.04</td>
	<td align="right">1,116,238</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">11</th>
	<td>
	<a href="http://www.judiska-museet.se">
	http://www.judiska-museet.se</a>
	</td>
	<td align="right">76</td>
	<td align="right">0.04</td>
	<td align="right">15,908</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">12</th>
	<td>
	<a href="http://site.ru">
	http://site.ru</a>
	</td>
	<td align="right">72</td>
	<td align="right">0.03</td>
	<td align="right">2,844,311</td>
	<td align="right">0.01</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">13</th>
	<td>
	<a href="https://translate.googleusercontent.com">
	https://translate.googleusercontent.com</a>
	</td>
	<td align="right">47</td>
	<td align="right">0.02</td>
	<td align="right">4,536,684</td>
	<td align="right">0.01</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">14</th>
	<td>
	<a href="https://r.search.yahoo.com">
	https://r.search.yahoo.com</a>
	</td>
	<td align="right">41</td>
	<td align="right">0.02</td>
	<td align="right">552,349</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">15</th>
	<td>
	<a href="https://duckduckgo.com">
	https://duckduckgo.com</a>
	</td>
	<td align="right">41</td>
	<td align="right">0.02</td>
	<td align="right">579,783</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">16</th>
	<td>
	<a href="http://l.instagram.com">
	http://l.instagram.com</a>
	</td>
	<td align="right">38</td>
	<td align="right">0.02</td>
	<td align="right">229,410</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">17</th>
	<td>
	<a href="https://89.221.250.27:443">
	https://89.221.250.27:443</a>
	</td>
	<td align="right">33</td>
	<td align="right">0.02</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">18</th>
	<td>
	<a href="https://l.instagram.com">
	https://l.instagram.com</a>
	</td>
	<td align="right">32</td>
	<td align="right">0.02</td>
	<td align="right">230,864</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">19</th>
	<td>
	<a href="https://sv.wikipedia.org">
	https://sv.wikipedia.org</a>
	</td>
	<td align="right">30</td>
	<td align="right">0.01</td>
	<td align="right">274,484</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">20</th>
	<td>
	<a href="http://www.stockholmmuseums.se">
	http://www.stockholmmuseums.se</a>
	</td>
	<td align="right">30</td>
	<td align="right">0.01</td>
	<td align="right">234,056</td>
	<td align="right">0.00</td>
</tr>

</tbody>
</table>

