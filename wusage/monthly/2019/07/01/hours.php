


<h2>Statistics for http://www.u5645470.fsdata.se, Month of 2019-7-1 to 2019-7-31</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Traffic by Hour</h3>
<p>Figures are averages for that hour
	of each day in the time period analyzed.</p>
<img src="h.png" width="468" height="351">
<br /><br />
<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Hour</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	<th scope='col'>Bits per Sec</th>
	<th scope='col'>Bytes per Sec</th>
	
	</tr>
</thead>
<tbody>
<tr class="jamnudda">
	<th align="right">00:00</th>
	<td align="right">178.65</td>
	<td align="right">2.09</td>
	<td align="right">35,554,046.26</td>
	<td align="right">2.51</td>
	<td align="right">6.95</td>
	<td align="right">1.69</td>
	<td align="right">79,008.99</td>
	<td align="right">9,876.12</td>
	
</tr>
<tr class="jamn">
	<th align="right">01:00</th>
	<td align="right">119.03</td>
	<td align="right">1.39</td>
	<td align="right">20,270,118.16</td>
	<td align="right">1.43</td>
	<td align="right">9.13</td>
	<td align="right">2.22</td>
	<td align="right">45,044.71</td>
	<td align="right">5,630.59</td>
	
</tr>
<tr class="udda">
	<th align="right">02:00</th>
	<td align="right">95.16</td>
	<td align="right">1.12</td>
	<td align="right">13,018,634.29</td>
	<td align="right">0.92</td>
	<td align="right">6.84</td>
	<td align="right">1.66</td>
	<td align="right">28,930.30</td>
	<td align="right">3,616.29</td>
	
</tr>
<tr class="jamn">
	<th align="right">03:00</th>
	<td align="right">88.55</td>
	<td align="right">1.04</td>
	<td align="right">14,105,398.94</td>
	<td align="right">0.99</td>
	<td align="right">7.19</td>
	<td align="right">1.75</td>
	<td align="right">31,345.33</td>
	<td align="right">3,918.17</td>
	
</tr>
<tr class="udda">
	<th align="right">04:00</th>
	<td align="right">94.13</td>
	<td align="right">1.10</td>
	<td align="right">11,103,362.94</td>
	<td align="right">0.78</td>
	<td align="right">7.29</td>
	<td align="right">1.77</td>
	<td align="right">24,674.14</td>
	<td align="right">3,084.27</td>
	
</tr>
<tr class="jamn">
	<th align="right">05:00</th>
	<td align="right">114.13</td>
	<td align="right">1.34</td>
	<td align="right">16,591,909.45</td>
	<td align="right">1.17</td>
	<td align="right">8.87</td>
	<td align="right">2.16</td>
	<td align="right">36,870.91</td>
	<td align="right">4,608.86</td>
	
</tr>
<tr class="udda">
	<th align="right">06:00</th>
	<td align="right">119.35</td>
	<td align="right">1.40</td>
	<td align="right">12,342,357.61</td>
	<td align="right">0.87</td>
	<td align="right">8.67</td>
	<td align="right">2.11</td>
	<td align="right">27,427.46</td>
	<td align="right">3,428.43</td>
	
</tr>
<tr class="jamn">
	<th align="right">07:00</th>
	<td align="right">247.16</td>
	<td align="right">2.90</td>
	<td align="right">32,609,190.77</td>
	<td align="right">2.30</td>
	<td align="right">10.31</td>
	<td align="right">2.51</td>
	<td align="right">72,464.87</td>
	<td align="right">9,058.11</td>
	
</tr>
<tr class="udda">
	<th align="right">08:00</th>
	<td align="right">413.87</td>
	<td align="right">4.85</td>
	<td align="right">85,816,641.23</td>
	<td align="right">6.05</td>
	<td align="right">14.73</td>
	<td align="right">3.58</td>
	<td align="right">190,703.65</td>
	<td align="right">23,837.96</td>
	
</tr>
<tr class="jamn">
	<th align="right">09:00</th>
	<td align="right">553.29</td>
	<td align="right">6.48</td>
	<td align="right">107,131,717.87</td>
	<td align="right">7.55</td>
	<td align="right">19.50</td>
	<td align="right">4.74</td>
	<td align="right">238,070.48</td>
	<td align="right">29,758.81</td>
	
</tr>
<tr class="udda">
	<th align="right">10:00</th>
	<td align="right">650.61</td>
	<td align="right">7.62</td>
	<td align="right">118,892,542.19</td>
	<td align="right">8.38</td>
	<td align="right">24.68</td>
	<td align="right">6.00</td>
	<td align="right">264,205.65</td>
	<td align="right">33,025.71</td>
	
</tr>
<tr class="jamn">
	<th align="right">11:00</th>
	<td align="right">636.42</td>
	<td align="right">7.46</td>
	<td align="right">104,765,430.48</td>
	<td align="right">7.39</td>
	<td align="right">27.20</td>
	<td align="right">6.62</td>
	<td align="right">232,812.07</td>
	<td align="right">29,101.51</td>
	
</tr>
<tr class="udda">
	<th align="right">12:00</th>
	<td align="right">553.19</td>
	<td align="right">6.48</td>
	<td align="right">93,937,248.03</td>
	<td align="right">6.62</td>
	<td align="right">24.95</td>
	<td align="right">6.07</td>
	<td align="right">208,749.44</td>
	<td align="right">26,093.68</td>
	
</tr>
<tr class="jamn">
	<th align="right">13:00</th>
	<td align="right">519.32</td>
	<td align="right">6.09</td>
	<td align="right">80,224,356.58</td>
	<td align="right">5.66</td>
	<td align="right">24.61</td>
	<td align="right">5.98</td>
	<td align="right">178,276.35</td>
	<td align="right">22,284.54</td>
	
</tr>
<tr class="udda">
	<th align="right">14:00</th>
	<td align="right">521.35</td>
	<td align="right">6.11</td>
	<td align="right">83,042,075.45</td>
	<td align="right">5.86</td>
	<td align="right">22.86</td>
	<td align="right">5.56</td>
	<td align="right">184,537.95</td>
	<td align="right">23,067.24</td>
	
</tr>
<tr class="jamn">
	<th align="right">15:00</th>
	<td align="right">536.16</td>
	<td align="right">6.28</td>
	<td align="right">77,170,554.55</td>
	<td align="right">5.44</td>
	<td align="right">24.38</td>
	<td align="right">5.93</td>
	<td align="right">171,490.12</td>
	<td align="right">21,436.27</td>
	
</tr>
<tr class="udda">
	<th align="right">16:00</th>
	<td align="right">417.39</td>
	<td align="right">4.89</td>
	<td align="right">68,180,340.61</td>
	<td align="right">4.81</td>
	<td align="right">21.62</td>
	<td align="right">5.26</td>
	<td align="right">151,511.87</td>
	<td align="right">18,938.98</td>
	
</tr>
<tr class="jamn">
	<th align="right">17:00</th>
	<td align="right">378.52</td>
	<td align="right">4.44</td>
	<td align="right">62,310,787.48</td>
	<td align="right">4.39</td>
	<td align="right">20.28</td>
	<td align="right">4.93</td>
	<td align="right">138,468.42</td>
	<td align="right">17,308.55</td>
	
</tr>
<tr class="udda">
	<th align="right">18:00</th>
	<td align="right">367.81</td>
	<td align="right">4.31</td>
	<td align="right">70,077,292.61</td>
	<td align="right">4.94</td>
	<td align="right">18.41</td>
	<td align="right">4.48</td>
	<td align="right">155,727.32</td>
	<td align="right">19,465.91</td>
	
</tr>
<tr class="jamn">
	<th align="right">19:00</th>
	<td align="right">413.84</td>
	<td align="right">4.85</td>
	<td align="right">69,756,926.81</td>
	<td align="right">4.92</td>
	<td align="right">18.66</td>
	<td align="right">4.54</td>
	<td align="right">155,015.39</td>
	<td align="right">19,376.92</td>
	
</tr>
<tr class="udda">
	<th align="right">20:00</th>
	<td align="right">372.42</td>
	<td align="right">4.36</td>
	<td align="right">60,207,254.52</td>
	<td align="right">4.25</td>
	<td align="right">20.16</td>
	<td align="right">4.90</td>
	<td align="right">133,793.90</td>
	<td align="right">16,724.24</td>
	
</tr>
<tr class="jamn">
	<th align="right">21:00</th>
	<td align="right">451.55</td>
	<td align="right">5.29</td>
	<td align="right">66,753,321.35</td>
	<td align="right">4.71</td>
	<td align="right">20.43</td>
	<td align="right">4.97</td>
	<td align="right">148,340.71</td>
	<td align="right">18,542.59</td>
	
</tr>
<tr class="udda">
	<th align="right">22:00</th>
	<td align="right">370.35</td>
	<td align="right">4.34</td>
	<td align="right">60,739,549.39</td>
	<td align="right">4.28</td>
	<td align="right">20.87</td>
	<td align="right">5.08</td>
	<td align="right">134,976.78</td>
	<td align="right">16,872.10</td>
	
</tr>
<tr class="jamn">
	<th align="right">23:00</th>
	<td align="right">321.16</td>
	<td align="right">3.76</td>
	<td align="right">53,482,518.52</td>
	<td align="right">3.77</td>
	<td align="right">22.65</td>
	<td align="right">5.51</td>
	<td align="right">118,850.04</td>
	<td align="right">14,856.26</td>
	
</tr>

</tbody>
</table>

