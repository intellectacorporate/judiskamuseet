


<h2>Statistics for http://www.u5645470.fsdata.se, Month of 2019-7-1 to 2019-7-31</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 20 of 10,217 Visitor Sites,
Sorted by Access Count

</h3>
<p></p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Site</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="jamnudda">
	<th class="hogerstalld">1</th>
	<td>
	46.59.25.88		
	</td>
	<td align="right">10,443</td>
	<td align="right">3.95</td>
	<td align="right">1,267,030,833</td>
	<td align="right">2.88</td>
	<td align="right">132</td>
	<td align="right">1.04</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	63.143.42.243		
	</td>
	<td align="right">5,692</td>
	<td align="right">2.15</td>
	<td align="right">310,094</td>
	<td align="right">0.00</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	5.35.187.69		
	</td>
	<td align="right">2,819</td>
	<td align="right">1.07</td>
	<td align="right">92,146,247</td>
	<td align="right">0.21</td>
	<td align="right">7</td>
	<td align="right">0.06</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	89.221.250.27		
	</td>
	<td align="right">2,249</td>
	<td align="right">0.85</td>
	<td align="right">44,091</td>
	<td align="right">0.00</td>
	<td align="right">40</td>
	<td align="right">0.32</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	81.170.246.9		
	</td>
	<td align="right">1,399</td>
	<td align="right">0.53</td>
	<td align="right">91,227,507</td>
	<td align="right">0.21</td>
	<td align="right">5</td>
	<td align="right">0.04</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	66.249.64.18		
	</td>
	<td align="right">762</td>
	<td align="right">0.29</td>
	<td align="right">22,605,886</td>
	<td align="right">0.05</td>
	<td align="right">125</td>
	<td align="right">0.98</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	216.244.66.227		
	</td>
	<td align="right">685</td>
	<td align="right">0.26</td>
	<td align="right">389,273,554</td>
	<td align="right">0.89</td>
	<td align="right">152</td>
	<td align="right">1.20</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	85.228.32.74		
	</td>
	<td align="right">634</td>
	<td align="right">0.24</td>
	<td align="right">16,033,465</td>
	<td align="right">0.04</td>
	<td align="right">4</td>
	<td align="right">0.03</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	66.249.64.19		
	</td>
	<td align="right">592</td>
	<td align="right">0.22</td>
	<td align="right">19,357,702</td>
	<td align="right">0.04</td>
	<td align="right">127</td>
	<td align="right">1.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">10</th>
	<td>
	51.75.14.187		
	</td>
	<td align="right">574</td>
	<td align="right">0.22</td>
	<td align="right">3,503,515</td>
	<td align="right">0.01</td>
	<td align="right">2</td>
	<td align="right">0.02</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">11</th>
	<td>
	95.195.145.67		
	</td>
	<td align="right">528</td>
	<td align="right">0.20</td>
	<td align="right">6,204,307</td>
	<td align="right">0.01</td>
	<td align="right">1</td>
	<td align="right">0.01</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">12</th>
	<td>
	82.103.145.126		
	</td>
	<td align="right">513</td>
	<td align="right">0.19</td>
	<td align="right">3,837,258</td>
	<td align="right">0.01</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">13</th>
	<td>
	85.25.210.234		
	</td>
	<td align="right">511</td>
	<td align="right">0.19</td>
	<td align="right">96,368,396</td>
	<td align="right">0.22</td>
	<td align="right">178</td>
	<td align="right">1.40</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">14</th>
	<td>
	185.15.93.69		
	</td>
	<td align="right">482</td>
	<td align="right">0.18</td>
	<td align="right">34,712,573</td>
	<td align="right">0.08</td>
	<td align="right">24</td>
	<td align="right">0.20</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">15</th>
	<td>
	185.246.208.82		
	</td>
	<td align="right">474</td>
	<td align="right">0.18</td>
	<td align="right">3,481,659</td>
	<td align="right">0.01</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">16</th>
	<td>
	109.104.25.125		
	</td>
	<td align="right">469</td>
	<td align="right">0.18</td>
	<td align="right">21,095,665</td>
	<td align="right">0.05</td>
	<td align="right">4</td>
	<td align="right">0.03</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">17</th>
	<td>
	213.212.29.165		
	</td>
	<td align="right">463</td>
	<td align="right">0.18</td>
	<td align="right">143,235,313</td>
	<td align="right">0.33</td>
	<td align="right">13</td>
	<td align="right">0.11</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">18</th>
	<td>
	83.233.67.83		
	</td>
	<td align="right">458</td>
	<td align="right">0.17</td>
	<td align="right">55,112,011</td>
	<td align="right">0.13</td>
	<td align="right">14</td>
	<td align="right">0.11</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">19</th>
	<td>
	81.17.62.205		
	</td>
	<td align="right">455</td>
	<td align="right">0.17</td>
	<td align="right">3,388,157</td>
	<td align="right">0.01</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">20</th>
	<td>
	94.75.211.74		
	</td>
	<td align="right">453</td>
	<td align="right">0.17</td>
	<td align="right">3,413,604</td>
	<td align="right">0.01</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>

</tbody>
</table>

