


<h2>Statistics for http://www.u5645470.fsdata.se, Month of 2019-7-1 to 2019-7-31</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 10 of 338 Browsers (User Agents),
Sorted by Access Count

</h3>
<p></p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Product</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th class="hogerstalld">1</th>
	<td>
	Safari/604.1		
	</td>
	<td align="right">72,871</td>
	<td align="right">27.63</td>
	<td align="right">13,880,524,375</td>
	<td align="right">31.71</td>
	<td align="right">2,642</td>
	<td align="right">20.91</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	Safari/537.36		
	</td>
	<td align="right">69,465</td>
	<td align="right">26.34</td>
	<td align="right">13,878,520,518</td>
	<td align="right">31.70</td>
	<td align="right">2,610</td>
	<td align="right">20.66</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	Mozilla/5.0 		
	</td>
	<td align="right">38,615</td>
	<td align="right">14.64</td>
	<td align="right">4,518,532,416</td>
	<td align="right">10.32</td>
	<td align="right">2,098</td>
	<td align="right">16.61</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	Safari/537.36 Edge/18.17763		
	</td>
	<td align="right">10,148</td>
	<td align="right">3.85</td>
	<td align="right">829,111,283</td>
	<td align="right">1.89</td>
	<td align="right">60</td>
	<td align="right">0.48</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	Safari/605.1.15		
	</td>
	<td align="right">9,941</td>
	<td align="right">3.77</td>
	<td align="right">2,120,284,670</td>
	<td align="right">4.84</td>
	<td align="right">356</td>
	<td align="right">2.82</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	bingbot/2.0		
	</td>
	<td align="right">7,557</td>
	<td align="right">2.87</td>
	<td align="right">1,169,014,302</td>
	<td align="right">2.67</td>
	<td align="right">1,728</td>
	<td align="right">13.67</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	Safari/537.36 Edge/17.17134		
	</td>
	<td align="right">7,493</td>
	<td align="right">2.84</td>
	<td align="right">2,314,175,780</td>
	<td align="right">5.29</td>
	<td align="right">229</td>
	<td align="right">1.82</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	UptimeRobot/2.0		
	</td>
	<td align="right">5,700</td>
	<td align="right">2.16</td>
	<td align="right">450,667</td>
	<td align="right">0.00</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	Pingdom.com_bot_version_1.4_		
	</td>
	<td align="right">5,647</td>
	<td align="right">2.14</td>
	<td align="right">41,764,712</td>
	<td align="right">0.10</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">10</th>
	<td>
	Safari/605.1		
	</td>
	<td align="right">4,097</td>
	<td align="right">1.55</td>
	<td align="right">757,722,970</td>
	<td align="right">1.73</td>
	<td align="right">150</td>
	<td align="right">1.19</td>
</tr>

</tbody>
</table>

