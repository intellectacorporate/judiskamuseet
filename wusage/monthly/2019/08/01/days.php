


<h2>Statistics for http://www.u5645470.fsdata.se, Month of 2019-8-1 to 2019-8-31</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Traffic by Day</h3>
<p>The continuous line represents a 7-day moving average.</p>
<img src="d.png" width="468" height="351">
<br /><br />


<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Date</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	<th scope='col'>Bits per Sec</th>
	<th scope='col'>Bytes per Sec</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th align="right">2019-8-1</th>
	<td align="right">6,724	</td>
	<td align="right">2.67	</td>
	<td align="right">1,072,083,351	</td>
	<td align="right">2.49	</td>
	<td align="right">363	</td>
	<td align="right">3.24	</td>
	<td align="right">99,266.98	</td>
	<td align="right">12,408.37	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-8-2</th>
	<td align="right">6,267	</td>
	<td align="right">2.49	</td>
	<td align="right">1,107,221,815	</td>
	<td align="right">2.58	</td>
	<td align="right">314	</td>
	<td align="right">2.80	</td>
	<td align="right">102,520.54	</td>
	<td align="right">12,815.07	</td>
</tr>
<tr class="udda">
	<th align="right">2019-8-3</th>
	<td align="right">5,744	</td>
	<td align="right">2.28	</td>
	<td align="right">1,009,671,414	</td>
	<td align="right">2.35	</td>
	<td align="right">271	</td>
	<td align="right">2.42	</td>
	<td align="right">93,488.09	</td>
	<td align="right">11,686.01	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-8-4</th>
	<td align="right">5,602	</td>
	<td align="right">2.23	</td>
	<td align="right">832,451,958	</td>
	<td align="right">1.94	</td>
	<td align="right">278	</td>
	<td align="right">2.48	</td>
	<td align="right">77,078.88	</td>
	<td align="right">9,634.86	</td>
</tr>
<tr class="udda">
	<th align="right">2019-8-5</th>
	<td align="right">7,784	</td>
	<td align="right">3.10	</td>
	<td align="right">1,337,661,936	</td>
	<td align="right">3.11	</td>
	<td align="right">354	</td>
	<td align="right">3.16	</td>
	<td align="right">123,857.59	</td>
	<td align="right">15,482.20	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-8-6</th>
	<td align="right">8,028	</td>
	<td align="right">3.19	</td>
	<td align="right">1,449,160,712	</td>
	<td align="right">3.37	</td>
	<td align="right">377	</td>
	<td align="right">3.36	</td>
	<td align="right">134,181.55	</td>
	<td align="right">16,772.69	</td>
</tr>
<tr class="udda">
	<th align="right">2019-8-7</th>
	<td align="right">8,501	</td>
	<td align="right">3.38	</td>
	<td align="right">1,651,635,044	</td>
	<td align="right">3.84	</td>
	<td align="right">397	</td>
	<td align="right">3.54	</td>
	<td align="right">152,929.17	</td>
	<td align="right">19,116.15	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-8-8</th>
	<td align="right">7,143	</td>
	<td align="right">2.84	</td>
	<td align="right">1,432,448,048	</td>
	<td align="right">3.33	</td>
	<td align="right">368	</td>
	<td align="right">3.28	</td>
	<td align="right">132,634.08	</td>
	<td align="right">16,579.26	</td>
</tr>
<tr class="udda">
	<th align="right">2019-8-9</th>
	<td align="right">6,129	</td>
	<td align="right">2.44	</td>
	<td align="right">1,121,045,987	</td>
	<td align="right">2.61	</td>
	<td align="right">393	</td>
	<td align="right">3.51	</td>
	<td align="right">103,800.55	</td>
	<td align="right">12,975.07	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-8-10</th>
	<td align="right">7,381	</td>
	<td align="right">2.94	</td>
	<td align="right">1,571,141,875	</td>
	<td align="right">3.66	</td>
	<td align="right">436	</td>
	<td align="right">3.89	</td>
	<td align="right">145,476.10	</td>
	<td align="right">18,184.51	</td>
</tr>
<tr class="udda">
	<th align="right">2019-8-11</th>
	<td align="right">6,485	</td>
	<td align="right">2.58	</td>
	<td align="right">1,115,392,409	</td>
	<td align="right">2.60	</td>
	<td align="right">326	</td>
	<td align="right">2.91	</td>
	<td align="right">103,277.07	</td>
	<td align="right">12,909.63	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-8-12</th>
	<td align="right">9,890	</td>
	<td align="right">3.93	</td>
	<td align="right">1,522,417,574	</td>
	<td align="right">3.54	</td>
	<td align="right">327	</td>
	<td align="right">2.92	</td>
	<td align="right">140,964.59	</td>
	<td align="right">17,620.57	</td>
</tr>
<tr class="udda">
	<th align="right">2019-8-13</th>
	<td align="right">7,644	</td>
	<td align="right">3.04	</td>
	<td align="right">1,432,328,755	</td>
	<td align="right">3.33	</td>
	<td align="right">334	</td>
	<td align="right">2.98	</td>
	<td align="right">132,623.03	</td>
	<td align="right">16,577.88	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-8-14</th>
	<td align="right">7,461	</td>
	<td align="right">2.97	</td>
	<td align="right">1,188,627,308	</td>
	<td align="right">2.77	</td>
	<td align="right">427	</td>
	<td align="right">3.81	</td>
	<td align="right">110,058.08	</td>
	<td align="right">13,757.26	</td>
</tr>
<tr class="udda">
	<th align="right">2019-8-15</th>
	<td align="right">6,362	</td>
	<td align="right">2.53	</td>
	<td align="right">1,056,809,952	</td>
	<td align="right">2.46	</td>
	<td align="right">378	</td>
	<td align="right">3.37	</td>
	<td align="right">97,852.77	</td>
	<td align="right">12,231.60	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-8-16</th>
	<td align="right">6,749	</td>
	<td align="right">2.68	</td>
	<td align="right">1,331,893,141	</td>
	<td align="right">3.10	</td>
	<td align="right">283	</td>
	<td align="right">2.53	</td>
	<td align="right">123,323.44	</td>
	<td align="right">15,415.43	</td>
</tr>
<tr class="udda">
	<th align="right">2019-8-17</th>
	<td align="right">4,928	</td>
	<td align="right">1.96	</td>
	<td align="right">743,361,592	</td>
	<td align="right">1.73	</td>
	<td align="right">251	</td>
	<td align="right">2.24	</td>
	<td align="right">68,829.78	</td>
	<td align="right">8,603.72	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-8-18</th>
	<td align="right">6,465	</td>
	<td align="right">2.57	</td>
	<td align="right">1,266,006,231	</td>
	<td align="right">2.95	</td>
	<td align="right">292	</td>
	<td align="right">2.61	</td>
	<td align="right">117,222.80	</td>
	<td align="right">14,652.85	</td>
</tr>
<tr class="udda">
	<th align="right">2019-8-19</th>
	<td align="right">9,737	</td>
	<td align="right">3.87	</td>
	<td align="right">1,856,397,057	</td>
	<td align="right">4.32	</td>
	<td align="right">356	</td>
	<td align="right">3.18	</td>
	<td align="right">171,888.62	</td>
	<td align="right">21,486.08	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-8-20</th>
	<td align="right">7,354	</td>
	<td align="right">2.93	</td>
	<td align="right">1,294,166,952	</td>
	<td align="right">3.01	</td>
	<td align="right">290	</td>
	<td align="right">2.59	</td>
	<td align="right">119,830.27	</td>
	<td align="right">14,978.78	</td>
</tr>
<tr class="udda">
	<th align="right">2019-8-21</th>
	<td align="right">7,749	</td>
	<td align="right">3.08	</td>
	<td align="right">1,405,415,383	</td>
	<td align="right">3.27	</td>
	<td align="right">293	</td>
	<td align="right">2.61	</td>
	<td align="right">130,131.05	</td>
	<td align="right">16,266.38	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-8-22</th>
	<td align="right">22,868	</td>
	<td align="right">9.10	</td>
	<td align="right">3,614,401,645	</td>
	<td align="right">8.41	</td>
	<td align="right">790	</td>
	<td align="right">7.05	</td>
	<td align="right">334,666.82	</td>
	<td align="right">41,833.35	</td>
</tr>
<tr class="udda">
	<th align="right">2019-8-23</th>
	<td align="right">12,330	</td>
	<td align="right">4.90	</td>
	<td align="right">1,710,375,444	</td>
	<td align="right">3.98	</td>
	<td align="right">391	</td>
	<td align="right">3.49	</td>
	<td align="right">158,368.10	</td>
	<td align="right">19,796.01	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-8-24</th>
	<td align="right">8,149	</td>
	<td align="right">3.24	</td>
	<td align="right">1,272,619,921	</td>
	<td align="right">2.96	</td>
	<td align="right">340	</td>
	<td align="right">3.03	</td>
	<td align="right">117,835.18	</td>
	<td align="right">14,729.40	</td>
</tr>
<tr class="udda">
	<th align="right">2019-8-25</th>
	<td align="right">7,401	</td>
	<td align="right">2.94	</td>
	<td align="right">1,094,366,843	</td>
	<td align="right">2.55	</td>
	<td align="right">352	</td>
	<td align="right">3.14	</td>
	<td align="right">101,330.26	</td>
	<td align="right">12,666.28	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-8-26</th>
	<td align="right">9,966	</td>
	<td align="right">3.96	</td>
	<td align="right">1,482,818,655	</td>
	<td align="right">3.45	</td>
	<td align="right">343	</td>
	<td align="right">3.06	</td>
	<td align="right">137,298.02	</td>
	<td align="right">17,162.25	</td>
</tr>
<tr class="udda">
	<th align="right">2019-8-27</th>
	<td align="right">8,969	</td>
	<td align="right">3.57	</td>
	<td align="right">1,474,183,505	</td>
	<td align="right">3.43	</td>
	<td align="right">413	</td>
	<td align="right">3.69	</td>
	<td align="right">136,498.47	</td>
	<td align="right">17,062.31	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-8-28</th>
	<td align="right">8,636	</td>
	<td align="right">3.44	</td>
	<td align="right">1,612,448,039	</td>
	<td align="right">3.75	</td>
	<td align="right">387	</td>
	<td align="right">3.45	</td>
	<td align="right">149,300.74	</td>
	<td align="right">18,662.59	</td>
</tr>
<tr class="udda">
	<th align="right">2019-8-29</th>
	<td align="right">9,128	</td>
	<td align="right">3.63	</td>
	<td align="right">1,734,991,489	</td>
	<td align="right">4.04	</td>
	<td align="right">372	</td>
	<td align="right">3.32	</td>
	<td align="right">160,647.36	</td>
	<td align="right">20,080.92	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-8-30</th>
	<td align="right">7,425	</td>
	<td align="right">2.95	</td>
	<td align="right">1,086,033,720	</td>
	<td align="right">2.53	</td>
	<td align="right">351	</td>
	<td align="right">3.13	</td>
	<td align="right">100,558.68	</td>
	<td align="right">12,569.83	</td>
</tr>
<tr class="udda">
	<th align="right">2019-8-31</th>
	<td align="right">6,392	</td>
	<td align="right">2.54	</td>
	<td align="right">1,101,075,099	</td>
	<td align="right">2.56	</td>
	<td align="right">359	</td>
	<td align="right">3.20	</td>
	<td align="right">101,951.40	</td>
	<td align="right">12,743.92	</td>
</tr>

</tbody>
</table>

