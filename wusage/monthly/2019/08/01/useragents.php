


<h2>Statistics for http://www.u5645470.fsdata.se, Month of 2019-8-1 to 2019-8-31</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 10 of 361 Browsers (User Agents),
Sorted by Access Count

</h3>
<p></p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Product</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th class="hogerstalld">1</th>
	<td>
	Safari/537.36		
	</td>
	<td align="right">71,109</td>
	<td align="right">28.36</td>
	<td align="right">15,338,858,431</td>
	<td align="right">35.75</td>
	<td align="right">2,639</td>
	<td align="right">23.69</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	Safari/604.1		
	</td>
	<td align="right">63,588</td>
	<td align="right">25.36</td>
	<td align="right">12,329,836,281</td>
	<td align="right">28.74</td>
	<td align="right">2,247</td>
	<td align="right">20.16</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	Mozilla/5.0 		
	</td>
	<td align="right">33,339</td>
	<td align="right">13.30</td>
	<td align="right">4,923,946,555</td>
	<td align="right">11.48</td>
	<td align="right">1,341</td>
	<td align="right">12.04</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	Safari/605.1.15		
	</td>
	<td align="right">11,887</td>
	<td align="right">4.74</td>
	<td align="right">2,468,633,147</td>
	<td align="right">5.75</td>
	<td align="right">401</td>
	<td align="right">3.60</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	Safari/537.36 Edge/18.17763		
	</td>
	<td align="right">9,772</td>
	<td align="right">3.90</td>
	<td align="right">972,649,934</td>
	<td align="right">2.27</td>
	<td align="right">98</td>
	<td align="right">0.88</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	bingbot/2.0		
	</td>
	<td align="right">7,725</td>
	<td align="right">3.08</td>
	<td align="right">1,069,069,387</td>
	<td align="right">2.49</td>
	<td align="right">1,699</td>
	<td align="right">15.25</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	UptimeRobot/2.0		
	</td>
	<td align="right">5,867</td>
	<td align="right">2.34</td>
	<td align="right">118,872</td>
	<td align="right">0.00</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	Pingdom.com_bot_version_1.4_		
	</td>
	<td align="right">5,657</td>
	<td align="right">2.26</td>
	<td align="right">43,748,856</td>
	<td align="right">0.10</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	Safari/537.36 Edge/17.17134		
	</td>
	<td align="right">4,394</td>
	<td align="right">1.75</td>
	<td align="right">889,281,740</td>
	<td align="right">2.07</td>
	<td align="right">159</td>
	<td align="right">1.43</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">10</th>
	<td>
	Safari/605.1		
	</td>
	<td align="right">3,583</td>
	<td align="right">1.43</td>
	<td align="right">683,254,364</td>
	<td align="right">1.59</td>
	<td align="right">135</td>
	<td align="right">1.21</td>
</tr>

</tbody>
</table>

