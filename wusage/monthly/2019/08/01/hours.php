


<h2>Statistics for http://www.u5645470.fsdata.se, Month of 2019-8-1 to 2019-8-31</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Traffic by Hour</h3>
<p>Figures are averages for that hour
	of each day in the time period analyzed.</p>
<img src="h.png" width="468" height="351">
<br /><br />
<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Hour</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	<th scope='col'>Bits per Sec</th>
	<th scope='col'>Bytes per Sec</th>
	
	</tr>
</thead>
<tbody>
<tr class="jamnudda">
	<th align="right">00:00</th>
	<td align="right">157.81</td>
	<td align="right">1.95</td>
	<td align="right">25,504,510.81</td>
	<td align="right">1.84</td>
	<td align="right">6.50</td>
	<td align="right">1.80</td>
	<td align="right">56,676.69</td>
	<td align="right">7,084.59</td>
	
</tr>
<tr class="jamn">
	<th align="right">01:00</th>
	<td align="right">85.94</td>
	<td align="right">1.06</td>
	<td align="right">10,491,063.90</td>
	<td align="right">0.76</td>
	<td align="right">8.48</td>
	<td align="right">2.35</td>
	<td align="right">23,313.48</td>
	<td align="right">2,914.18</td>
	
</tr>
<tr class="udda">
	<th align="right">02:00</th>
	<td align="right">98.32</td>
	<td align="right">1.21</td>
	<td align="right">11,060,922.03</td>
	<td align="right">0.80</td>
	<td align="right">5.87</td>
	<td align="right">1.62</td>
	<td align="right">24,579.83</td>
	<td align="right">3,072.48</td>
	
</tr>
<tr class="jamn">
	<th align="right">03:00</th>
	<td align="right">103.26</td>
	<td align="right">1.27</td>
	<td align="right">11,269,990.45</td>
	<td align="right">0.81</td>
	<td align="right">6.74</td>
	<td align="right">1.87</td>
	<td align="right">25,044.42</td>
	<td align="right">3,130.55</td>
	
</tr>
<tr class="udda">
	<th align="right">04:00</th>
	<td align="right">95.39</td>
	<td align="right">1.18</td>
	<td align="right">8,295,904.81</td>
	<td align="right">0.60</td>
	<td align="right">6.93</td>
	<td align="right">1.92</td>
	<td align="right">18,435.34</td>
	<td align="right">2,304.42</td>
	
</tr>
<tr class="jamn">
	<th align="right">05:00</th>
	<td align="right">79.45</td>
	<td align="right">0.98</td>
	<td align="right">7,522,652.77</td>
	<td align="right">0.54</td>
	<td align="right">6.58</td>
	<td align="right">1.82</td>
	<td align="right">16,717.01</td>
	<td align="right">2,089.63</td>
	
</tr>
<tr class="udda">
	<th align="right">06:00</th>
	<td align="right">103.55</td>
	<td align="right">1.28</td>
	<td align="right">15,455,676.03</td>
	<td align="right">1.11</td>
	<td align="right">7.12</td>
	<td align="right">1.97</td>
	<td align="right">34,345.95</td>
	<td align="right">4,293.24</td>
	
</tr>
<tr class="jamn">
	<th align="right">07:00</th>
	<td align="right">209.77</td>
	<td align="right">2.59</td>
	<td align="right">33,186,239.10</td>
	<td align="right">2.39</td>
	<td align="right">8.15</td>
	<td align="right">2.25</td>
	<td align="right">73,747.20</td>
	<td align="right">9,218.40</td>
	
</tr>
<tr class="udda">
	<th align="right">08:00</th>
	<td align="right">343.81</td>
	<td align="right">4.24</td>
	<td align="right">58,934,300.52</td>
	<td align="right">4.25</td>
	<td align="right">13.33</td>
	<td align="right">3.69</td>
	<td align="right">130,965.11</td>
	<td align="right">16,370.64</td>
	
</tr>
<tr class="jamn">
	<th align="right">09:00</th>
	<td align="right">514.19</td>
	<td align="right">6.34</td>
	<td align="right">96,013,659.48</td>
	<td align="right">6.93</td>
	<td align="right">17.83</td>
	<td align="right">4.93</td>
	<td align="right">213,363.69</td>
	<td align="right">26,670.46</td>
	
</tr>
<tr class="udda">
	<th align="right">10:00</th>
	<td align="right">594.35</td>
	<td align="right">7.33</td>
	<td align="right">113,294,995.39</td>
	<td align="right">8.17</td>
	<td align="right">22.82</td>
	<td align="right">6.31</td>
	<td align="right">251,766.66</td>
	<td align="right">31,470.83</td>
	
</tr>
<tr class="jamn">
	<th align="right">11:00</th>
	<td align="right">558.39</td>
	<td align="right">6.89</td>
	<td align="right">105,038,127.42</td>
	<td align="right">7.58</td>
	<td align="right">22.42</td>
	<td align="right">6.20</td>
	<td align="right">233,418.06</td>
	<td align="right">29,177.26</td>
	
</tr>
<tr class="udda">
	<th align="right">12:00</th>
	<td align="right">605.35</td>
	<td align="right">7.46</td>
	<td align="right">104,577,654.42</td>
	<td align="right">7.54</td>
	<td align="right">22.58</td>
	<td align="right">6.25</td>
	<td align="right">232,394.79</td>
	<td align="right">29,049.35</td>
	
</tr>
<tr class="jamn">
	<th align="right">13:00</th>
	<td align="right">627.94</td>
	<td align="right">7.74</td>
	<td align="right">108,438,887.16</td>
	<td align="right">7.82</td>
	<td align="right">25.29</td>
	<td align="right">7.00</td>
	<td align="right">240,975.30</td>
	<td align="right">30,121.91</td>
	
</tr>
<tr class="udda">
	<th align="right">14:00</th>
	<td align="right">570.13</td>
	<td align="right">7.03</td>
	<td align="right">106,952,654.32</td>
	<td align="right">7.71</td>
	<td align="right">23.79</td>
	<td align="right">6.58</td>
	<td align="right">237,672.57</td>
	<td align="right">29,709.07</td>
	
</tr>
<tr class="jamn">
	<th align="right">15:00</th>
	<td align="right">455.45</td>
	<td align="right">5.62</td>
	<td align="right">79,350,678.32</td>
	<td align="right">5.72</td>
	<td align="right">19.49</td>
	<td align="right">5.39</td>
	<td align="right">176,334.84</td>
	<td align="right">22,041.86</td>
	
</tr>
<tr class="udda">
	<th align="right">16:00</th>
	<td align="right">463.42</td>
	<td align="right">5.71</td>
	<td align="right">78,333,912.90</td>
	<td align="right">5.65</td>
	<td align="right">18.78</td>
	<td align="right">5.20</td>
	<td align="right">174,075.36</td>
	<td align="right">21,759.42</td>
	
</tr>
<tr class="jamn">
	<th align="right">17:00</th>
	<td align="right">417.03</td>
	<td align="right">5.14</td>
	<td align="right">57,023,497.61</td>
	<td align="right">4.11</td>
	<td align="right">17.48</td>
	<td align="right">4.84</td>
	<td align="right">126,718.88</td>
	<td align="right">15,839.86</td>
	
</tr>
<tr class="udda">
	<th align="right">18:00</th>
	<td align="right">366.81</td>
	<td align="right">4.52</td>
	<td align="right">59,990,931.26</td>
	<td align="right">4.33</td>
	<td align="right">16.83</td>
	<td align="right">4.65</td>
	<td align="right">133,313.18</td>
	<td align="right">16,664.15</td>
	
</tr>
<tr class="jamn">
	<th align="right">19:00</th>
	<td align="right">331.74</td>
	<td align="right">4.09</td>
	<td align="right">57,592,130.58</td>
	<td align="right">4.15</td>
	<td align="right">15.07</td>
	<td align="right">4.17</td>
	<td align="right">127,982.51</td>
	<td align="right">15,997.81</td>
	
</tr>
<tr class="udda">
	<th align="right">20:00</th>
	<td align="right">341.39</td>
	<td align="right">4.21</td>
	<td align="right">65,357,479.90</td>
	<td align="right">4.71</td>
	<td align="right">14.64</td>
	<td align="right">4.05</td>
	<td align="right">145,238.84</td>
	<td align="right">18,154.86</td>
	
</tr>
<tr class="jamn">
	<th align="right">21:00</th>
	<td align="right">397.39</td>
	<td align="right">4.90</td>
	<td align="right">72,837,494.71</td>
	<td align="right">5.25</td>
	<td align="right">17.73</td>
	<td align="right">4.90</td>
	<td align="right">161,861.10</td>
	<td align="right">20,232.64</td>
	
</tr>
<tr class="udda">
	<th align="right">22:00</th>
	<td align="right">343.10</td>
	<td align="right">4.23</td>
	<td align="right">59,836,295.42</td>
	<td align="right">4.32</td>
	<td align="right">15.90</td>
	<td align="right">4.40</td>
	<td align="right">132,969.55</td>
	<td align="right">16,621.19</td>
	
</tr>
<tr class="jamn">
	<th align="right">23:00</th>
	<td align="right">245.42</td>
	<td align="right">3.03</td>
	<td align="right">40,113,013.39</td>
	<td align="right">2.89</td>
	<td align="right">21.15</td>
	<td align="right">5.85</td>
	<td align="right">89,140.03</td>
	<td align="right">11,142.50</td>
	
</tr>

</tbody>
</table>

