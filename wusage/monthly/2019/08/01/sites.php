


<h2>Statistics for http://www.u5645470.fsdata.se, Month of 2019-8-1 to 2019-8-31</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 20 of 9,210 Visitor Sites,
Sorted by Access Count

</h3>
<p></p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Site</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="jamnudda">
	<th class="hogerstalld">1</th>
	<td>
	46.59.25.88		
	</td>
	<td align="right">10,696</td>
	<td align="right">4.25</td>
	<td align="right">1,245,595,936</td>
	<td align="right">2.90</td>
	<td align="right">158</td>
	<td align="right">1.41</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	63.143.42.243		
	</td>
	<td align="right">5,867</td>
	<td align="right">2.33</td>
	<td align="right">102,927</td>
	<td align="right">0.00</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	5.35.187.69		
	</td>
	<td align="right">4,193</td>
	<td align="right">1.67</td>
	<td align="right">176,310,392</td>
	<td align="right">0.41</td>
	<td align="right">18</td>
	<td align="right">0.16</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	89.221.250.27		
	</td>
	<td align="right">2,102</td>
	<td align="right">0.84</td>
	<td align="right">64,809</td>
	<td align="right">0.00</td>
	<td align="right">37</td>
	<td align="right">0.33</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	90.229.237.5		
	</td>
	<td align="right">1,064</td>
	<td align="right">0.42</td>
	<td align="right">103,700,725</td>
	<td align="right">0.24</td>
	<td align="right">16</td>
	<td align="right">0.15</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	195.196.62.48		
	</td>
	<td align="right">959</td>
	<td align="right">0.38</td>
	<td align="right">281,257,680</td>
	<td align="right">0.65</td>
	<td align="right">14</td>
	<td align="right">0.13</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	88.83.61.171		
	</td>
	<td align="right">860</td>
	<td align="right">0.34</td>
	<td align="right">321,115,930</td>
	<td align="right">0.75</td>
	<td align="right">15</td>
	<td align="right">0.14</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	66.249.64.18		
	</td>
	<td align="right">663</td>
	<td align="right">0.26</td>
	<td align="right">28,040,658</td>
	<td align="right">0.07</td>
	<td align="right">102</td>
	<td align="right">0.92</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	81.17.62.205		
	</td>
	<td align="right">565</td>
	<td align="right">0.22</td>
	<td align="right">4,339,742</td>
	<td align="right">0.01</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">10</th>
	<td>
	151.106.52.134		
	</td>
	<td align="right">556</td>
	<td align="right">0.22</td>
	<td align="right">4,315,385</td>
	<td align="right">0.01</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">11</th>
	<td>
	82.103.139.165		
	</td>
	<td align="right">542</td>
	<td align="right">0.22</td>
	<td align="right">4,215,446</td>
	<td align="right">0.01</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">12</th>
	<td>
	52.5.190.19		
	</td>
	<td align="right">506</td>
	<td align="right">0.20</td>
	<td align="right">75,895,337</td>
	<td align="right">0.18</td>
	<td align="right">1</td>
	<td align="right">0.01</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">13</th>
	<td>
	85.194.6.161		
	</td>
	<td align="right">499</td>
	<td align="right">0.20</td>
	<td align="right">155,687,327</td>
	<td align="right">0.36</td>
	<td align="right">18</td>
	<td align="right">0.16</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">14</th>
	<td>
	137.61.48.10		
	</td>
	<td align="right">499</td>
	<td align="right">0.20</td>
	<td align="right">40,457,341</td>
	<td align="right">0.09</td>
	<td align="right">4</td>
	<td align="right">0.04</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">15</th>
	<td>
	66.249.64.19		
	</td>
	<td align="right">486</td>
	<td align="right">0.19</td>
	<td align="right">10,127,763</td>
	<td align="right">0.02</td>
	<td align="right">89</td>
	<td align="right">0.79</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">16</th>
	<td>
	5.172.196.188		
	</td>
	<td align="right">481</td>
	<td align="right">0.19</td>
	<td align="right">3,677,424</td>
	<td align="right">0.01</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">17</th>
	<td>
	188.151.125.195		
	</td>
	<td align="right">469</td>
	<td align="right">0.19</td>
	<td align="right">207,626,937</td>
	<td align="right">0.48</td>
	<td align="right">14</td>
	<td align="right">0.13</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">18</th>
	<td>
	37.9.113.109		
	</td>
	<td align="right">456</td>
	<td align="right">0.18</td>
	<td align="right">3,527,232</td>
	<td align="right">0.01</td>
	<td align="right">122</td>
	<td align="right">1.09</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">19</th>
	<td>
	82.196.178.67		
	</td>
	<td align="right">447</td>
	<td align="right">0.18</td>
	<td align="right">145,825,541</td>
	<td align="right">0.34</td>
	<td align="right">14</td>
	<td align="right">0.13</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">20</th>
	<td>
	185.180.12.65		
	</td>
	<td align="right">435</td>
	<td align="right">0.17</td>
	<td align="right">3,353,796</td>
	<td align="right">0.01</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>

</tbody>
</table>

