


<h2>Statistics for http://www.u5645470.fsdata.se, Month of 2019-2-1 to 2019-2-28</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 10 of 469 Browsers (User Agents),
Sorted by Access Count

</h3>
<p></p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Product</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th class="hogerstalld">1</th>
	<td>
	Safari/537.36		
	</td>
	<td align="right">44,007</td>
	<td align="right">33.71</td>
	<td align="right">16,627,884,522</td>
	<td align="right">46.88</td>
	<td align="right">2,266</td>
	<td align="right">34.84</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	Safari/604.1		
	</td>
	<td align="right">17,842</td>
	<td align="right">13.67</td>
	<td align="right">5,377,143,799</td>
	<td align="right">15.16</td>
	<td align="right">851</td>
	<td align="right">13.08</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	Mozilla/5.0 		
	</td>
	<td align="right">15,373</td>
	<td align="right">11.77</td>
	<td align="right">4,503,918,562</td>
	<td align="right">12.70</td>
	<td align="right">533</td>
	<td align="right">8.20</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	Safari/537.36 Edge/17.17134		
	</td>
	<td align="right">10,515</td>
	<td align="right">8.05</td>
	<td align="right">2,276,752,764</td>
	<td align="right">6.42</td>
	<td align="right">170</td>
	<td align="right">2.62</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	UptimeRobot/2.0		
	</td>
	<td align="right">7,088</td>
	<td align="right">5.43</td>
	<td align="right">101,837</td>
	<td align="right">0.00</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	WordPress/5.0.3		
	</td>
	<td align="right">6,111</td>
	<td align="right">4.68</td>
	<td align="right">120,444</td>
	<td align="right">0.00</td>
	<td align="right">116</td>
	<td align="right">1.78</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	Safari/605.1.15		
	</td>
	<td align="right">4,977</td>
	<td align="right">3.81</td>
	<td align="right">2,053,780,256</td>
	<td align="right">5.79</td>
	<td align="right">217</td>
	<td align="right">3.35</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	bingbot/2.0		
	</td>
	<td align="right">4,309</td>
	<td align="right">3.30</td>
	<td align="right">463,862,852</td>
	<td align="right">1.31</td>
	<td align="right">935</td>
	<td align="right">14.37</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	Safari/605.1		
	</td>
	<td align="right">1,906</td>
	<td align="right">1.46</td>
	<td align="right">636,541,399</td>
	<td align="right">1.79</td>
	<td align="right">82</td>
	<td align="right">1.27</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">10</th>
	<td>
	Safari/602.1		
	</td>
	<td align="right">1,546</td>
	<td align="right">1.18</td>
	<td align="right">416,558,093</td>
	<td align="right">1.17</td>
	<td align="right">79</td>
	<td align="right">1.22</td>
</tr>

</tbody>
</table>

