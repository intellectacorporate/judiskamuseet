


<h2>Statistics for http://www.u5645470.fsdata.se, Month of 2019-2-1 to 2019-2-28</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 20 of 6,483 Visitor Sites,
Sorted by Access Count

</h3>
<p></p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Site</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th class="hogerstalld">1</th>
	<td>
	46.59.25.88		
	</td>
	<td align="right">8,776</td>
	<td align="right">6.70</td>
	<td align="right">1,409,454,722</td>
	<td align="right">3.97</td>
	<td align="right">75</td>
	<td align="right">1.16</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	63.143.42.244		
	</td>
	<td align="right">7,100</td>
	<td align="right">5.42</td>
	<td align="right">72,752</td>
	<td align="right">0.00</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	89.221.250.27		
	</td>
	<td align="right">6,112</td>
	<td align="right">4.67</td>
	<td align="right">120,445</td>
	<td align="right">0.00</td>
	<td align="right">116</td>
	<td align="right">1.78</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	46.182.207.79		
	</td>
	<td align="right">1,171</td>
	<td align="right">0.89</td>
	<td align="right">122,676,866</td>
	<td align="right">0.35</td>
	<td align="right">6</td>
	<td align="right">0.10</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	23.101.169.3		
	</td>
	<td align="right">888</td>
	<td align="right">0.68</td>
	<td align="right">13,614,265</td>
	<td align="right">0.04</td>
	<td align="right">58</td>
	<td align="right">0.89</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	195.3.144.185		
	</td>
	<td align="right">842</td>
	<td align="right">0.64</td>
	<td align="right">12,514,786</td>
	<td align="right">0.04</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	81.218.202.180		
	</td>
	<td align="right">818</td>
	<td align="right">0.62</td>
	<td align="right">48,176,911</td>
	<td align="right">0.14</td>
	<td align="right">5</td>
	<td align="right">0.08</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	51.15.15.54		
	</td>
	<td align="right">765</td>
	<td align="right">0.58</td>
	<td align="right">3,030,936</td>
	<td align="right">0.01</td>
	<td align="right">15</td>
	<td align="right">0.24</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	185.15.93.69		
	</td>
	<td align="right">727</td>
	<td align="right">0.56</td>
	<td align="right">12,364,136</td>
	<td align="right">0.03</td>
	<td align="right">55</td>
	<td align="right">0.85</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">10</th>
	<td>
	195.196.62.48		
	</td>
	<td align="right">463</td>
	<td align="right">0.35</td>
	<td align="right">207,470,832</td>
	<td align="right">0.58</td>
	<td align="right">14</td>
	<td align="right">0.22</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">11</th>
	<td>
	193.47.167.198		
	</td>
	<td align="right">457</td>
	<td align="right">0.35</td>
	<td align="right">44,553,944</td>
	<td align="right">0.13</td>
	<td align="right">5</td>
	<td align="right">0.08</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">12</th>
	<td>
	216.244.66.227		
	</td>
	<td align="right">455</td>
	<td align="right">0.35</td>
	<td align="right">91,338,166</td>
	<td align="right">0.26</td>
	<td align="right">92</td>
	<td align="right">1.42</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">13</th>
	<td>
	51.75.14.187		
	</td>
	<td align="right">443</td>
	<td align="right">0.34</td>
	<td align="right">2,693,152</td>
	<td align="right">0.01</td>
	<td align="right">1</td>
	<td align="right">0.02</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">14</th>
	<td>
	188.92.74.189		
	</td>
	<td align="right">441</td>
	<td align="right">0.34</td>
	<td align="right">6,516,309</td>
	<td align="right">0.02</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">15</th>
	<td>
	155.4.131.233		
	</td>
	<td align="right">385</td>
	<td align="right">0.29</td>
	<td align="right">110,266,717</td>
	<td align="right">0.31</td>
	<td align="right">13</td>
	<td align="right">0.21</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">16</th>
	<td>
	122.49.28.47		
	</td>
	<td align="right">342</td>
	<td align="right">0.26</td>
	<td align="right">73,623,618</td>
	<td align="right">0.21</td>
	<td align="right">11</td>
	<td align="right">0.17</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">17</th>
	<td>
	193.47.167.199		
	</td>
	<td align="right">314</td>
	<td align="right">0.24</td>
	<td align="right">48,605,953</td>
	<td align="right">0.14</td>
	<td align="right">5</td>
	<td align="right">0.08</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">18</th>
	<td>
	80.86.78.45		
	</td>
	<td align="right">310</td>
	<td align="right">0.24</td>
	<td align="right">123,203,762</td>
	<td align="right">0.35</td>
	<td align="right">9</td>
	<td align="right">0.15</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">19</th>
	<td>
	195.178.161.7		
	</td>
	<td align="right">293</td>
	<td align="right">0.22</td>
	<td align="right">14,016,576</td>
	<td align="right">0.04</td>
	<td align="right">7</td>
	<td align="right">0.12</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">20</th>
	<td>
	17.58.99.252		
	</td>
	<td align="right">291</td>
	<td align="right">0.22</td>
	<td align="right">1,969,713</td>
	<td align="right">0.01</td>
	<td align="right">66</td>
	<td align="right">1.02</td>
</tr>

</tbody>
</table>

