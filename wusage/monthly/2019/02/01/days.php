


<h2>Statistics for http://www.u5645470.fsdata.se, Month of 2019-2-1 to 2019-2-28</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Traffic by Day</h3>
<p>The continuous line represents a 7-day moving average.</p>
<img src="d.png" width="468" height="351">
<br /><br />


<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Date</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	<th scope='col'>Bits per Sec</th>
	<th scope='col'>Bytes per Sec</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th align="right">2019-2-1</th>
	<td align="right">8,740	</td>
	<td align="right">6.67	</td>
	<td align="right">1,690,250,870	</td>
	<td align="right">4.76	</td>
	<td align="right">279	</td>
	<td align="right">4.27	</td>
	<td align="right">156,504.71	</td>
	<td align="right">19,563.09	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-2-2</th>
	<td align="right">3,758	</td>
	<td align="right">2.87	</td>
	<td align="right">976,163,651	</td>
	<td align="right">2.75	</td>
	<td align="right">194	</td>
	<td align="right">2.97	</td>
	<td align="right">90,385.52	</td>
	<td align="right">11,298.19	</td>
</tr>
<tr class="udda">
	<th align="right">2019-2-3</th>
	<td align="right">3,682	</td>
	<td align="right">2.81	</td>
	<td align="right">880,316,390	</td>
	<td align="right">2.48	</td>
	<td align="right">211	</td>
	<td align="right">3.23	</td>
	<td align="right">81,510.78	</td>
	<td align="right">10,188.85	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-2-4</th>
	<td align="right">8,226	</td>
	<td align="right">6.28	</td>
	<td align="right">2,140,807,337	</td>
	<td align="right">6.03	</td>
	<td align="right">236	</td>
	<td align="right">3.61	</td>
	<td align="right">198,222.90	</td>
	<td align="right">24,777.86	</td>
</tr>
<tr class="udda">
	<th align="right">2019-2-5</th>
	<td align="right">5,499	</td>
	<td align="right">4.20	</td>
	<td align="right">1,627,328,624	</td>
	<td align="right">4.58	</td>
	<td align="right">272	</td>
	<td align="right">4.16	</td>
	<td align="right">150,678.58	</td>
	<td align="right">18,834.82	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-2-6</th>
	<td align="right">6,539	</td>
	<td align="right">4.99	</td>
	<td align="right">1,536,020,549	</td>
	<td align="right">4.32	</td>
	<td align="right">323	</td>
	<td align="right">4.94	</td>
	<td align="right">142,224.12	</td>
	<td align="right">17,778.02	</td>
</tr>
<tr class="udda">
	<th align="right">2019-2-7</th>
	<td align="right">5,615	</td>
	<td align="right">4.29	</td>
	<td align="right">1,526,988,195	</td>
	<td align="right">4.30	</td>
	<td align="right">276	</td>
	<td align="right">4.22	</td>
	<td align="right">141,387.80	</td>
	<td align="right">17,673.47	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-2-8</th>
	<td align="right">3,752	</td>
	<td align="right">2.86	</td>
	<td align="right">940,536,393	</td>
	<td align="right">2.65	</td>
	<td align="right">231	</td>
	<td align="right">3.53	</td>
	<td align="right">87,086.70	</td>
	<td align="right">10,885.84	</td>
</tr>
<tr class="udda">
	<th align="right">2019-2-9</th>
	<td align="right">3,934	</td>
	<td align="right">3.00	</td>
	<td align="right">833,081,489	</td>
	<td align="right">2.35	</td>
	<td align="right">213	</td>
	<td align="right">3.26	</td>
	<td align="right">77,137.17	</td>
	<td align="right">9,642.15	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-2-10</th>
	<td align="right">4,626	</td>
	<td align="right">3.53	</td>
	<td align="right">1,405,723,965	</td>
	<td align="right">3.96	</td>
	<td align="right">252	</td>
	<td align="right">3.85	</td>
	<td align="right">130,159.63	</td>
	<td align="right">16,269.95	</td>
</tr>
<tr class="udda">
	<th align="right">2019-2-11</th>
	<td align="right">4,524	</td>
	<td align="right">3.45	</td>
	<td align="right">1,387,094,024	</td>
	<td align="right">3.90	</td>
	<td align="right">261	</td>
	<td align="right">3.99	</td>
	<td align="right">128,434.63	</td>
	<td align="right">16,054.33	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-2-12</th>
	<td align="right">4,920	</td>
	<td align="right">3.76	</td>
	<td align="right">1,436,663,476	</td>
	<td align="right">4.04	</td>
	<td align="right">268	</td>
	<td align="right">4.10	</td>
	<td align="right">133,024.40	</td>
	<td align="right">16,628.05	</td>
</tr>
<tr class="udda">
	<th align="right">2019-2-13</th>
	<td align="right">5,300	</td>
	<td align="right">4.05	</td>
	<td align="right">1,278,785,988	</td>
	<td align="right">3.60	</td>
	<td align="right">278	</td>
	<td align="right">4.25	</td>
	<td align="right">118,406.11	</td>
	<td align="right">14,800.76	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-2-14</th>
	<td align="right">4,833	</td>
	<td align="right">3.69	</td>
	<td align="right">1,317,873,814	</td>
	<td align="right">3.71	</td>
	<td align="right">226	</td>
	<td align="right">3.46	</td>
	<td align="right">122,025.35	</td>
	<td align="right">15,253.17	</td>
</tr>
<tr class="udda">
	<th align="right">2019-2-15</th>
	<td align="right">3,688	</td>
	<td align="right">2.82	</td>
	<td align="right">1,145,717,545	</td>
	<td align="right">3.23	</td>
	<td align="right">198	</td>
	<td align="right">3.03	</td>
	<td align="right">106,084.96	</td>
	<td align="right">13,260.62	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-2-16</th>
	<td align="right">3,222	</td>
	<td align="right">2.46	</td>
	<td align="right">871,939,562	</td>
	<td align="right">2.45	</td>
	<td align="right">150	</td>
	<td align="right">2.29	</td>
	<td align="right">80,735.14	</td>
	<td align="right">10,091.89	</td>
</tr>
<tr class="udda">
	<th align="right">2019-2-17</th>
	<td align="right">3,115	</td>
	<td align="right">2.38	</td>
	<td align="right">983,521,036	</td>
	<td align="right">2.77	</td>
	<td align="right">185	</td>
	<td align="right">2.83	</td>
	<td align="right">91,066.76	</td>
	<td align="right">11,383.35	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-2-18</th>
	<td align="right">5,017	</td>
	<td align="right">3.83	</td>
	<td align="right">1,460,738,029	</td>
	<td align="right">4.11	</td>
	<td align="right">220	</td>
	<td align="right">3.37	</td>
	<td align="right">135,253.52	</td>
	<td align="right">16,906.69	</td>
</tr>
<tr class="udda">
	<th align="right">2019-2-19</th>
	<td align="right">4,187	</td>
	<td align="right">3.20	</td>
	<td align="right">1,301,209,035	</td>
	<td align="right">3.66	</td>
	<td align="right">224	</td>
	<td align="right">3.43	</td>
	<td align="right">120,482.32	</td>
	<td align="right">15,060.29	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-2-20</th>
	<td align="right">5,987	</td>
	<td align="right">4.57	</td>
	<td align="right">1,551,603,449	</td>
	<td align="right">4.37	</td>
	<td align="right">244	</td>
	<td align="right">3.73	</td>
	<td align="right">143,666.99	</td>
	<td align="right">17,958.37	</td>
</tr>
<tr class="udda">
	<th align="right">2019-2-21</th>
	<td align="right">4,824	</td>
	<td align="right">3.68	</td>
	<td align="right">1,150,637,344	</td>
	<td align="right">3.24	</td>
	<td align="right">254	</td>
	<td align="right">3.89	</td>
	<td align="right">106,540.49	</td>
	<td align="right">13,317.56	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-2-22</th>
	<td align="right">4,647	</td>
	<td align="right">3.55	</td>
	<td align="right">1,062,888,542	</td>
	<td align="right">2.99	</td>
	<td align="right">298	</td>
	<td align="right">4.56	</td>
	<td align="right">98,415.61	</td>
	<td align="right">12,301.95	</td>
</tr>
<tr class="udda">
	<th align="right">2019-2-23</th>
	<td align="right">3,272	</td>
	<td align="right">2.50	</td>
	<td align="right">780,267,439	</td>
	<td align="right">2.20	</td>
	<td align="right">162	</td>
	<td align="right">2.48	</td>
	<td align="right">72,246.99	</td>
	<td align="right">9,030.87	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-2-24</th>
	<td align="right">2,976	</td>
	<td align="right">2.27	</td>
	<td align="right">942,758,850	</td>
	<td align="right">2.65	</td>
	<td align="right">180	</td>
	<td align="right">2.75	</td>
	<td align="right">87,292.49	</td>
	<td align="right">10,911.56	</td>
</tr>
<tr class="udda">
	<th align="right">2019-2-25</th>
	<td align="right">3,756	</td>
	<td align="right">2.87	</td>
	<td align="right">1,162,054,302	</td>
	<td align="right">3.27	</td>
	<td align="right">204	</td>
	<td align="right">3.12	</td>
	<td align="right">107,597.62	</td>
	<td align="right">13,449.70	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-2-26</th>
	<td align="right">4,631	</td>
	<td align="right">3.54	</td>
	<td align="right">1,451,792,524	</td>
	<td align="right">4.09	</td>
	<td align="right">243	</td>
	<td align="right">3.72	</td>
	<td align="right">134,425.23	</td>
	<td align="right">16,803.15	</td>
</tr>
<tr class="udda">
	<th align="right">2019-2-27</th>
	<td align="right">3,671	</td>
	<td align="right">2.80	</td>
	<td align="right">1,401,167,635	</td>
	<td align="right">3.94	</td>
	<td align="right">207	</td>
	<td align="right">3.17	</td>
	<td align="right">129,737.74	</td>
	<td align="right">16,217.22	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-2-28</th>
	<td align="right">4,045	</td>
	<td align="right">3.09	</td>
	<td align="right">1,281,775,701	</td>
	<td align="right">3.61	</td>
	<td align="right">248	</td>
	<td align="right">3.79	</td>
	<td align="right">118,682.94	</td>
	<td align="right">14,835.37	</td>
</tr>

</tbody>
</table>

