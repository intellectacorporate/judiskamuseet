


<h2>Statistics for http://www.u5645470.fsdata.se, Month of 2019-2-1 to 2019-2-28</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Traffic by Hour</h3>
<p>Figures are averages for that hour
	of each day in the time period analyzed.</p>
<img src="h.png" width="468" height="351">
<br /><br />
<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Hour</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	<th scope='col'>Bits per Sec</th>
	<th scope='col'>Bytes per Sec</th>
	
	</tr>
</thead>
<tbody>
<tr class="jamnudda">
	<th align="right">00:00</th>
	<td align="right">89.89</td>
	<td align="right">1.92</td>
	<td align="right">21,876,822.04</td>
	<td align="right">1.72</td>
	<td align="right">3.34</td>
	<td align="right">1.43</td>
	<td align="right">48,615.16</td>
	<td align="right">6,076.90</td>
	
</tr>
<tr class="jamn">
	<th align="right">01:00</th>
	<td align="right">70.11</td>
	<td align="right">1.50</td>
	<td align="right">12,307,442.36</td>
	<td align="right">0.97</td>
	<td align="right">4.57</td>
	<td align="right">1.96</td>
	<td align="right">27,349.87</td>
	<td align="right">3,418.73</td>
	
</tr>
<tr class="udda">
	<th align="right">02:00</th>
	<td align="right">73.71</td>
	<td align="right">1.58</td>
	<td align="right">13,769,850.61</td>
	<td align="right">1.09</td>
	<td align="right">4.31</td>
	<td align="right">1.85</td>
	<td align="right">30,599.67</td>
	<td align="right">3,824.96</td>
	
</tr>
<tr class="jamn">
	<th align="right">03:00</th>
	<td align="right">69.54</td>
	<td align="right">1.49</td>
	<td align="right">7,591,364.50</td>
	<td align="right">0.60</td>
	<td align="right">4.83</td>
	<td align="right">2.07</td>
	<td align="right">16,869.70</td>
	<td align="right">2,108.71</td>
	
</tr>
<tr class="udda">
	<th align="right">04:00</th>
	<td align="right">65.29</td>
	<td align="right">1.40</td>
	<td align="right">7,099,904.25</td>
	<td align="right">0.56</td>
	<td align="right">4.57</td>
	<td align="right">1.96</td>
	<td align="right">15,777.57</td>
	<td align="right">1,972.20</td>
	
</tr>
<tr class="jamn">
	<th align="right">05:00</th>
	<td align="right">72.79</td>
	<td align="right">1.56</td>
	<td align="right">5,661,397.04</td>
	<td align="right">0.45</td>
	<td align="right">4.45</td>
	<td align="right">1.91</td>
	<td align="right">12,580.88</td>
	<td align="right">1,572.61</td>
	
</tr>
<tr class="udda">
	<th align="right">06:00</th>
	<td align="right">64.21</td>
	<td align="right">1.37</td>
	<td align="right">7,507,368.96</td>
	<td align="right">0.59</td>
	<td align="right">5.44</td>
	<td align="right">2.33</td>
	<td align="right">16,683.04</td>
	<td align="right">2,085.38</td>
	
</tr>
<tr class="jamn">
	<th align="right">07:00</th>
	<td align="right">92.96</td>
	<td align="right">1.99</td>
	<td align="right">19,280,856.36</td>
	<td align="right">1.52</td>
	<td align="right">5.29</td>
	<td align="right">2.27</td>
	<td align="right">42,846.35</td>
	<td align="right">5,355.79</td>
	
</tr>
<tr class="udda">
	<th align="right">08:00</th>
	<td align="right">190.96</td>
	<td align="right">4.08</td>
	<td align="right">50,695,458.96</td>
	<td align="right">4.00</td>
	<td align="right">7.54</td>
	<td align="right">3.23</td>
	<td align="right">112,656.58</td>
	<td align="right">14,082.07</td>
	
</tr>
<tr class="jamn">
	<th align="right">09:00</th>
	<td align="right">303.89</td>
	<td align="right">6.50</td>
	<td align="right">77,869,915.18</td>
	<td align="right">6.14</td>
	<td align="right">12.46</td>
	<td align="right">5.34</td>
	<td align="right">173,044.26</td>
	<td align="right">21,630.53</td>
	
</tr>
<tr class="udda">
	<th align="right">10:00</th>
	<td align="right">346.07</td>
	<td align="right">7.40</td>
	<td align="right">103,065,566.18</td>
	<td align="right">8.12</td>
	<td align="right">13.00</td>
	<td align="right">5.57</td>
	<td align="right">229,034.59</td>
	<td align="right">28,629.32</td>
	
</tr>
<tr class="jamn">
	<th align="right">11:00</th>
	<td align="right">301.71</td>
	<td align="right">6.45</td>
	<td align="right">99,190,167.71</td>
	<td align="right">7.82</td>
	<td align="right">14.10</td>
	<td align="right">6.04</td>
	<td align="right">220,422.59</td>
	<td align="right">27,552.82</td>
	
</tr>
<tr class="udda">
	<th align="right">12:00</th>
	<td align="right">274.82</td>
	<td align="right">5.87</td>
	<td align="right">89,706,732.11</td>
	<td align="right">7.07</td>
	<td align="right">13.53</td>
	<td align="right">5.80</td>
	<td align="right">199,348.29</td>
	<td align="right">24,918.54</td>
	
</tr>
<tr class="jamn">
	<th align="right">13:00</th>
	<td align="right">393.25</td>
	<td align="right">8.41</td>
	<td align="right">118,460,108.11</td>
	<td align="right">9.34</td>
	<td align="right">15.95</td>
	<td align="right">6.83</td>
	<td align="right">263,244.68</td>
	<td align="right">32,905.59</td>
	
</tr>
<tr class="udda">
	<th align="right">14:00</th>
	<td align="right">317.61</td>
	<td align="right">6.79</td>
	<td align="right">91,861,122.50</td>
	<td align="right">7.24</td>
	<td align="right">14.91</td>
	<td align="right">6.39</td>
	<td align="right">204,135.83</td>
	<td align="right">25,516.98</td>
	
</tr>
<tr class="jamn">
	<th align="right">15:00</th>
	<td align="right">254.79</td>
	<td align="right">5.45</td>
	<td align="right">70,751,360.04</td>
	<td align="right">5.58</td>
	<td align="right">12.32</td>
	<td align="right">5.28</td>
	<td align="right">157,225.24</td>
	<td align="right">19,653.16</td>
	
</tr>
<tr class="udda">
	<th align="right">16:00</th>
	<td align="right">262.32</td>
	<td align="right">5.61</td>
	<td align="right">79,499,828.68</td>
	<td align="right">6.27</td>
	<td align="right">13.17</td>
	<td align="right">5.64</td>
	<td align="right">176,666.29</td>
	<td align="right">22,083.29</td>
	
</tr>
<tr class="jamn">
	<th align="right">17:00</th>
	<td align="right">238.64</td>
	<td align="right">5.10</td>
	<td align="right">60,803,678.18</td>
	<td align="right">4.79</td>
	<td align="right">12.13</td>
	<td align="right">5.19</td>
	<td align="right">135,119.28</td>
	<td align="right">16,889.91</td>
	
</tr>
<tr class="udda">
	<th align="right">18:00</th>
	<td align="right">215.64</td>
	<td align="right">4.61</td>
	<td align="right">55,347,275.39</td>
	<td align="right">4.36</td>
	<td align="right">12.02</td>
	<td align="right">5.15</td>
	<td align="right">122,993.95</td>
	<td align="right">15,374.24</td>
	
</tr>
<tr class="jamn">
	<th align="right">19:00</th>
	<td align="right">252.04</td>
	<td align="right">5.39</td>
	<td align="right">80,858,655.14</td>
	<td align="right">6.37</td>
	<td align="right">12.58</td>
	<td align="right">5.39</td>
	<td align="right">179,685.90</td>
	<td align="right">22,460.74</td>
	
</tr>
<tr class="udda">
	<th align="right">20:00</th>
	<td align="right">206.25</td>
	<td align="right">4.41</td>
	<td align="right">53,531,275.00</td>
	<td align="right">4.22</td>
	<td align="right">10.96</td>
	<td align="right">4.69</td>
	<td align="right">118,958.39</td>
	<td align="right">14,869.80</td>
	
</tr>
<tr class="jamn">
	<th align="right">21:00</th>
	<td align="right">190.89</td>
	<td align="right">4.08</td>
	<td align="right">49,195,444.25</td>
	<td align="right">3.88</td>
	<td align="right">9.91</td>
	<td align="right">4.24</td>
	<td align="right">109,323.21</td>
	<td align="right">13,665.40</td>
	
</tr>
<tr class="udda">
	<th align="right">22:00</th>
	<td align="right">193.11</td>
	<td align="right">4.13</td>
	<td align="right">62,291,140.75</td>
	<td align="right">4.91</td>
	<td align="right">10.69</td>
	<td align="right">4.58</td>
	<td align="right">138,424.76</td>
	<td align="right">17,303.09</td>
	
</tr>
<tr class="jamn">
	<th align="right">23:00</th>
	<td align="right">137.57</td>
	<td align="right">2.94</td>
	<td align="right">30,552,471.36</td>
	<td align="right">2.41</td>
	<td align="right">11.38</td>
	<td align="right">4.88</td>
	<td align="right">67,894.38</td>
	<td align="right">8,486.80</td>
	
</tr>

</tbody>
</table>

