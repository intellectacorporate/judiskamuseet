


<h2>Statistics for http://www.u5645470.fsdata.se, Month of 2019-2-1 to 2019-2-28</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 9 of 9 Operating Systems,
Sorted by Access Count

</h3>
<p></p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Product</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th class="hogerstalld">1</th>
	<td>
	Mac OS X		
	</td>
	<td align="right">91,626</td>
	<td align="right">70.18</td>
	<td align="right">30,065,767,491</td>
	<td align="right">84.76</td>
	<td align="right">4,360</td>
	<td align="right">66.71</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	Other		
	</td>
	<td align="right">26,554</td>
	<td align="right">20.34</td>
	<td align="right">1,823,130,149</td>
	<td align="right">5.14</td>
	<td align="right">1,715</td>
	<td align="right">26.24</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	Windows NT		
	</td>
	<td align="right">8,822</td>
	<td align="right">6.76</td>
	<td align="right">3,248,667,688</td>
	<td align="right">9.16</td>
	<td align="right">344</td>
	<td align="right">5.27</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	Windows Codename Longhorn		
	</td>
	<td align="right">1,828</td>
	<td align="right">1.40</td>
	<td align="right">109,946,734</td>
	<td align="right">0.31</td>
	<td align="right">80</td>
	<td align="right">1.24</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	Linux		
	</td>
	<td align="right">1,350</td>
	<td align="right">1.03</td>
	<td align="right">191,714,097</td>
	<td align="right">0.54</td>
	<td align="right">17</td>
	<td align="right">0.28</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	Windows XP		
	</td>
	<td align="right">321</td>
	<td align="right">0.25</td>
	<td align="right">31,932,145</td>
	<td align="right">0.09</td>
	<td align="right">17</td>
	<td align="right">0.27</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	Windows 2003		
	</td>
	<td align="right">40</td>
	<td align="right">0.03</td>
	<td align="right">50,948</td>
	<td align="right">0.00</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	Windows 2000		
	</td>
	<td align="right">17</td>
	<td align="right">0.01</td>
	<td align="right">335,195</td>
	<td align="right">0.00</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	Windows Me		
	</td>
	<td align="right">1</td>
	<td align="right">0.00</td>
	<td align="right">20</td>
	<td align="right">0.00</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>

</tbody>
</table>

