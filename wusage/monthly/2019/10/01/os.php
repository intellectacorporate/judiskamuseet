


<h2>Statistics for http://www.u5645470.fsdata.se, Month of 2019-10-1 to 2019-10-31</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 10 of 18 Operating Systems,
Sorted by Access Count

</h3>
<p></p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Product</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th class="hogerstalld">1</th>
	<td>
	Mac OS X		
	</td>
	<td align="right">227,794</td>
	<td align="right">61.75</td>
	<td align="right">23,701,209,543</td>
	<td align="right">85.51</td>
	<td align="right">11,262</td>
	<td align="right">66.32</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	Other		
	</td>
	<td align="right">115,978</td>
	<td align="right">31.44</td>
	<td align="right">2,274,829,784</td>
	<td align="right">8.21</td>
	<td align="right">3,209</td>
	<td align="right">18.90</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	Windows NT		
	</td>
	<td align="right">16,046</td>
	<td align="right">4.35</td>
	<td align="right">1,591,134,965</td>
	<td align="right">5.74</td>
	<td align="right">700</td>
	<td align="right">4.13</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	Linux		
	</td>
	<td align="right">7,512</td>
	<td align="right">2.04</td>
	<td align="right">87,861,148</td>
	<td align="right">0.32</td>
	<td align="right">1,787</td>
	<td align="right">10.53</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	Windows XP		
	</td>
	<td align="right">1,079</td>
	<td align="right">0.29</td>
	<td align="right">33,494,762</td>
	<td align="right">0.12</td>
	<td align="right">13</td>
	<td align="right">0.08</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	Windows Codename Longhorn		
	</td>
	<td align="right">189</td>
	<td align="right">0.05</td>
	<td align="right">17,670,224</td>
	<td align="right">0.06</td>
	<td align="right">6</td>
	<td align="right">0.04</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	Windows 2000		
	</td>
	<td align="right">116</td>
	<td align="right">0.03</td>
	<td align="right">5,814,717</td>
	<td align="right">0.02</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	Windows 2003		
	</td>
	<td align="right">73</td>
	<td align="right">0.02</td>
	<td align="right">1,794,812</td>
	<td align="right">0.01</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	Windows 95		
	</td>
	<td align="right">41</td>
	<td align="right">0.01</td>
	<td align="right">2,002,357</td>
	<td align="right">0.01</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">10</th>
	<td>
	Mac OS		
	</td>
	<td align="right">19</td>
	<td align="right">0.01</td>
	<td align="right">15,880</td>
	<td align="right">0.00</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>

</tbody>
</table>

