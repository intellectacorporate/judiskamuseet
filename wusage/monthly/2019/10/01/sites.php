


<h2>Statistics for http://www.u5645470.fsdata.se, Month of 2019-10-1 to 2019-10-31</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 20 of 12,452 Visitor Sites,
Sorted by Access Count

</h3>
<p></p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Site</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="jamnudda">
	<th class="hogerstalld">1</th>
	<td>
	46.59.25.88		
	</td>
	<td align="right">8,495</td>
	<td align="right">2.30</td>
	<td align="right">622,094,374</td>
	<td align="right">2.24</td>
	<td align="right">171</td>
	<td align="right">1.01</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	85.195.116.134		
	</td>
	<td align="right">8,474</td>
	<td align="right">2.29</td>
	<td align="right">70,375,438</td>
	<td align="right">0.25</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	185.93.3.92		
	</td>
	<td align="right">8,341</td>
	<td align="right">2.26</td>
	<td align="right">69,553,309</td>
	<td align="right">0.25</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	94.75.211.73		
	</td>
	<td align="right">7,932</td>
	<td align="right">2.15</td>
	<td align="right">65,881,001</td>
	<td align="right">0.24</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	185.246.208.82		
	</td>
	<td align="right">6,456</td>
	<td align="right">1.75</td>
	<td align="right">53,499,195</td>
	<td align="right">0.19</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	63.143.42.242		
	</td>
	<td align="right">5,726</td>
	<td align="right">1.55</td>
	<td align="right">214,368</td>
	<td align="right">0.00</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	151.106.52.134		
	</td>
	<td align="right">5,588</td>
	<td align="right">1.51</td>
	<td align="right">46,221,802</td>
	<td align="right">0.17</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	81.17.62.205		
	</td>
	<td align="right">5,582</td>
	<td align="right">1.51</td>
	<td align="right">46,287,995</td>
	<td align="right">0.17</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	185.152.65.167		
	</td>
	<td align="right">5,075</td>
	<td align="right">1.37</td>
	<td align="right">42,682,090</td>
	<td align="right">0.15</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">10</th>
	<td>
	82.103.139.165		
	</td>
	<td align="right">4,121</td>
	<td align="right">1.12</td>
	<td align="right">34,010,430</td>
	<td align="right">0.12</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">11</th>
	<td>
	94.247.174.83		
	</td>
	<td align="right">3,896</td>
	<td align="right">1.05</td>
	<td align="right">32,141,112</td>
	<td align="right">0.12</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">12</th>
	<td>
	185.180.12.65		
	</td>
	<td align="right">3,337</td>
	<td align="right">0.90</td>
	<td align="right">27,708,716</td>
	<td align="right">0.10</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">13</th>
	<td>
	188.151.113.17		
	</td>
	<td align="right">3,067</td>
	<td align="right">0.83</td>
	<td align="right">695,197,321</td>
	<td align="right">2.50</td>
	<td align="right">35</td>
	<td align="right">0.21</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">14</th>
	<td>
	169.51.2.22		
	</td>
	<td align="right">3,035</td>
	<td align="right">0.82</td>
	<td align="right">25,190,902</td>
	<td align="right">0.09</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">15</th>
	<td>
	82.103.136.16		
	</td>
	<td align="right">2,927</td>
	<td align="right">0.79</td>
	<td align="right">24,202,651</td>
	<td align="right">0.09</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">16</th>
	<td>
	46.246.122.10		
	</td>
	<td align="right">2,911</td>
	<td align="right">0.79</td>
	<td align="right">24,186,266</td>
	<td align="right">0.09</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">17</th>
	<td>
	185.39.146.215		
	</td>
	<td align="right">2,903</td>
	<td align="right">0.79</td>
	<td align="right">24,202,177</td>
	<td align="right">0.09</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">18</th>
	<td>
	185.136.156.82		
	</td>
	<td align="right">2,897</td>
	<td align="right">0.78</td>
	<td align="right">24,186,092</td>
	<td align="right">0.09</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">19</th>
	<td>
	83.170.113.210		
	</td>
	<td align="right">2,810</td>
	<td align="right">0.76</td>
	<td align="right">23,332,192</td>
	<td align="right">0.08</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">20</th>
	<td>
	185.15.93.69		
	</td>
	<td align="right">2,641</td>
	<td align="right">0.72</td>
	<td align="right">81,802,984</td>
	<td align="right">0.29</td>
	<td align="right">265</td>
	<td align="right">1.56</td>
</tr>

</tbody>
</table>

