


<h2>Statistics for http://www.u5645470.fsdata.se, Month of 2019-10-1 to 2019-10-31</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Traffic by Hour</h3>
<p>Figures are averages for that hour
	of each day in the time period analyzed.</p>
<img src="h.png" width="468" height="351">
<br /><br />
<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Hour</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	<th scope='col'>Bits per Sec</th>
	<th scope='col'>Bytes per Sec</th>
	
	</tr>
</thead>
<tbody>
<tr class="jamnudda">
	<th align="right">00:00</th>
	<td align="right">283.74</td>
	<td align="right">2.38</td>
	<td align="right">14,106,338.74</td>
	<td align="right">1.57</td>
	<td align="right">9.49</td>
	<td align="right">1.73</td>
	<td align="right">31,347.42</td>
	<td align="right">3,918.43</td>
	
</tr>
<tr class="jamn">
	<th align="right">01:00</th>
	<td align="right">225.97</td>
	<td align="right">1.90</td>
	<td align="right">8,893,313.84</td>
	<td align="right">0.99</td>
	<td align="right">11.18</td>
	<td align="right">2.04</td>
	<td align="right">19,762.92</td>
	<td align="right">2,470.36</td>
	
</tr>
<tr class="udda">
	<th align="right">02:00</th>
	<td align="right">199.16</td>
	<td align="right">1.67</td>
	<td align="right">6,557,836.00</td>
	<td align="right">0.73</td>
	<td align="right">8.43</td>
	<td align="right">1.54</td>
	<td align="right">14,572.97</td>
	<td align="right">1,821.62</td>
	
</tr>
<tr class="jamn">
	<th align="right">03:00</th>
	<td align="right">199.74</td>
	<td align="right">1.68</td>
	<td align="right">6,522,973.26</td>
	<td align="right">0.73</td>
	<td align="right">8.52</td>
	<td align="right">1.56</td>
	<td align="right">14,495.50</td>
	<td align="right">1,811.94</td>
	
</tr>
<tr class="udda">
	<th align="right">04:00</th>
	<td align="right">212.55</td>
	<td align="right">1.78</td>
	<td align="right">5,987,112.48</td>
	<td align="right">0.67</td>
	<td align="right">9.33</td>
	<td align="right">1.70</td>
	<td align="right">13,304.69</td>
	<td align="right">1,663.09</td>
	
</tr>
<tr class="jamn">
	<th align="right">05:00</th>
	<td align="right">214.58</td>
	<td align="right">1.80</td>
	<td align="right">5,225,989.32</td>
	<td align="right">0.58</td>
	<td align="right">10.99</td>
	<td align="right">2.01</td>
	<td align="right">11,613.31</td>
	<td align="right">1,451.66</td>
	
</tr>
<tr class="udda">
	<th align="right">06:00</th>
	<td align="right">259.03</td>
	<td align="right">2.17</td>
	<td align="right">12,726,241.90</td>
	<td align="right">1.42</td>
	<td align="right">11.42</td>
	<td align="right">2.09</td>
	<td align="right">28,280.54</td>
	<td align="right">3,535.07</td>
	
</tr>
<tr class="jamn">
	<th align="right">07:00</th>
	<td align="right">331.48</td>
	<td align="right">2.78</td>
	<td align="right">19,099,855.45</td>
	<td align="right">2.13</td>
	<td align="right">14.46</td>
	<td align="right">2.64</td>
	<td align="right">42,444.12</td>
	<td align="right">5,305.52</td>
	
</tr>
<tr class="udda">
	<th align="right">08:00</th>
	<td align="right">539.71</td>
	<td align="right">4.53</td>
	<td align="right">39,320,517.87</td>
	<td align="right">4.39</td>
	<td align="right">20.21</td>
	<td align="right">3.69</td>
	<td align="right">87,378.93</td>
	<td align="right">10,922.37</td>
	
</tr>
<tr class="jamn">
	<th align="right">09:00</th>
	<td align="right">772.35</td>
	<td align="right">6.48</td>
	<td align="right">62,322,594.68</td>
	<td align="right">6.96</td>
	<td align="right">31.32</td>
	<td align="right">5.72</td>
	<td align="right">138,494.65</td>
	<td align="right">17,311.83</td>
	
</tr>
<tr class="udda">
	<th align="right">10:00</th>
	<td align="right">841.77</td>
	<td align="right">7.07</td>
	<td align="right">72,768,900.87</td>
	<td align="right">8.12</td>
	<td align="right">37.41</td>
	<td align="right">6.83</td>
	<td align="right">161,708.67</td>
	<td align="right">20,213.58</td>
	
</tr>
<tr class="jamn">
	<th align="right">11:00</th>
	<td align="right">764.61</td>
	<td align="right">6.42</td>
	<td align="right">59,803,283.68</td>
	<td align="right">6.68</td>
	<td align="right">35.43</td>
	<td align="right">6.47</td>
	<td align="right">132,896.19</td>
	<td align="right">16,612.02</td>
	
</tr>
<tr class="udda">
	<th align="right">12:00</th>
	<td align="right">666.97</td>
	<td align="right">5.60</td>
	<td align="right">53,176,415.61</td>
	<td align="right">5.94</td>
	<td align="right">31.45</td>
	<td align="right">5.74</td>
	<td align="right">118,169.81</td>
	<td align="right">14,771.23</td>
	
</tr>
<tr class="jamn">
	<th align="right">13:00</th>
	<td align="right">708.77</td>
	<td align="right">5.95</td>
	<td align="right">55,241,233.42</td>
	<td align="right">6.17</td>
	<td align="right">32.89</td>
	<td align="right">6.00</td>
	<td align="right">122,758.30</td>
	<td align="right">15,344.79</td>
	
</tr>
<tr class="udda">
	<th align="right">14:00</th>
	<td align="right">725.84</td>
	<td align="right">6.09</td>
	<td align="right">67,797,851.65</td>
	<td align="right">7.57</td>
	<td align="right">30.77</td>
	<td align="right">5.62</td>
	<td align="right">150,661.89</td>
	<td align="right">18,832.74</td>
	
</tr>
<tr class="jamn">
	<th align="right">15:00</th>
	<td align="right">659.35</td>
	<td align="right">5.53</td>
	<td align="right">81,721,360.06</td>
	<td align="right">9.12</td>
	<td align="right">29.15</td>
	<td align="right">5.32</td>
	<td align="right">181,603.02</td>
	<td align="right">22,700.38</td>
	
</tr>
<tr class="udda">
	<th align="right">16:00</th>
	<td align="right">657.45</td>
	<td align="right">5.52</td>
	<td align="right">61,557,094.61</td>
	<td align="right">6.87</td>
	<td align="right">28.15</td>
	<td align="right">5.14</td>
	<td align="right">136,793.54</td>
	<td align="right">17,099.19</td>
	
</tr>
<tr class="jamn">
	<th align="right">17:00</th>
	<td align="right">563.13</td>
	<td align="right">4.73</td>
	<td align="right">41,558,704.81</td>
	<td align="right">4.64</td>
	<td align="right">26.78</td>
	<td align="right">4.89</td>
	<td align="right">92,352.68</td>
	<td align="right">11,544.08</td>
	
</tr>
<tr class="udda">
	<th align="right">18:00</th>
	<td align="right">564.68</td>
	<td align="right">4.74</td>
	<td align="right">40,207,732.35</td>
	<td align="right">4.49</td>
	<td align="right">27.42</td>
	<td align="right">5.01</td>
	<td align="right">89,350.52</td>
	<td align="right">11,168.81</td>
	
</tr>
<tr class="jamn">
	<th align="right">19:00</th>
	<td align="right">550.74</td>
	<td align="right">4.62</td>
	<td align="right">41,135,869.65</td>
	<td align="right">4.59</td>
	<td align="right">25.56</td>
	<td align="right">4.67</td>
	<td align="right">91,413.04</td>
	<td align="right">11,426.63</td>
	
</tr>
<tr class="udda">
	<th align="right">20:00</th>
	<td align="right">548.94</td>
	<td align="right">4.61</td>
	<td align="right">40,746,337.97</td>
	<td align="right">4.55</td>
	<td align="right">27.24</td>
	<td align="right">4.97</td>
	<td align="right">90,547.42</td>
	<td align="right">11,318.43</td>
	
</tr>
<tr class="jamn">
	<th align="right">21:00</th>
	<td align="right">545.48</td>
	<td align="right">4.58</td>
	<td align="right">40,315,223.84</td>
	<td align="right">4.50</td>
	<td align="right">26.62</td>
	<td align="right">4.86</td>
	<td align="right">89,589.39</td>
	<td align="right">11,198.67</td>
	
</tr>
<tr class="udda">
	<th align="right">22:00</th>
	<td align="right">518.65</td>
	<td align="right">4.35</td>
	<td align="right">36,268,446.42</td>
	<td align="right">4.05</td>
	<td align="right">26.67</td>
	<td align="right">4.87</td>
	<td align="right">80,596.55</td>
	<td align="right">10,074.57</td>
	
</tr>
<tr class="jamn">
	<th align="right">23:00</th>
	<td align="right">359.94</td>
	<td align="right">3.02</td>
	<td align="right">22,596,380.81</td>
	<td align="right">2.52</td>
	<td align="right">26.88</td>
	<td align="right">4.91</td>
	<td align="right">50,214.18</td>
	<td align="right">6,276.77</td>
	
</tr>

</tbody>
</table>

