


<h2>Statistics for http://www.u5645470.fsdata.se, Month of 2019-10-1 to 2019-10-31</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Traffic by Day</h3>
<p>The continuous line represents a 7-day moving average.</p>
<img src="d.png" width="468" height="351">
<br /><br />


<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Date</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	<th scope='col'>Bits per Sec</th>
	<th scope='col'>Bytes per Sec</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th align="right">2019-10-1</th>
	<td align="right">15,137	</td>
	<td align="right">4.10	</td>
	<td align="right">956,228,151	</td>
	<td align="right">3.44	</td>
	<td align="right">623	</td>
	<td align="right">3.67	</td>
	<td align="right">88,539.64	</td>
	<td align="right">11,067.46	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-10-2</th>
	<td align="right">17,959	</td>
	<td align="right">4.86	</td>
	<td align="right">2,249,946,357	</td>
	<td align="right">8.10	</td>
	<td align="right">497	</td>
	<td align="right">2.93	</td>
	<td align="right">208,328.37	</td>
	<td align="right">26,041.05	</td>
</tr>
<tr class="udda">
	<th align="right">2019-10-3</th>
	<td align="right">14,488	</td>
	<td align="right">3.92	</td>
	<td align="right">700,218,215	</td>
	<td align="right">2.52	</td>
	<td align="right">541	</td>
	<td align="right">3.19	</td>
	<td align="right">64,835.02	</td>
	<td align="right">8,104.38	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-10-4</th>
	<td align="right">15,011	</td>
	<td align="right">4.06	</td>
	<td align="right">1,156,831,774	</td>
	<td align="right">4.17	</td>
	<td align="right">662	</td>
	<td align="right">3.90	</td>
	<td align="right">107,114.05	</td>
	<td align="right">13,389.26	</td>
</tr>
<tr class="udda">
	<th align="right">2019-10-5</th>
	<td align="right">9,514	</td>
	<td align="right">2.58	</td>
	<td align="right">757,390,482	</td>
	<td align="right">2.73	</td>
	<td align="right">523	</td>
	<td align="right">3.08	</td>
	<td align="right">70,128.75	</td>
	<td align="right">8,766.09	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-10-6</th>
	<td align="right">9,926	</td>
	<td align="right">2.69	</td>
	<td align="right">660,205,965	</td>
	<td align="right">2.38	</td>
	<td align="right">528	</td>
	<td align="right">3.11	</td>
	<td align="right">61,130.18	</td>
	<td align="right">7,641.27	</td>
</tr>
<tr class="udda">
	<th align="right">2019-10-7</th>
	<td align="right">12,197	</td>
	<td align="right">3.30	</td>
	<td align="right">934,828,185	</td>
	<td align="right">3.37	</td>
	<td align="right">590	</td>
	<td align="right">3.47	</td>
	<td align="right">86,558.17	</td>
	<td align="right">10,819.77	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-10-8</th>
	<td align="right">12,338	</td>
	<td align="right">3.34	</td>
	<td align="right">889,266,312	</td>
	<td align="right">3.20	</td>
	<td align="right">606	</td>
	<td align="right">3.57	</td>
	<td align="right">82,339.47	</td>
	<td align="right">10,292.43	</td>
</tr>
<tr class="udda">
	<th align="right">2019-10-9</th>
	<td align="right">12,586	</td>
	<td align="right">3.41	</td>
	<td align="right">1,047,359,609	</td>
	<td align="right">3.77	</td>
	<td align="right">498	</td>
	<td align="right">2.93	</td>
	<td align="right">96,977.74	</td>
	<td align="right">12,122.22	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-10-10</th>
	<td align="right">13,184	</td>
	<td align="right">3.57	</td>
	<td align="right">1,184,753,049	</td>
	<td align="right">4.27	</td>
	<td align="right">648	</td>
	<td align="right">3.82	</td>
	<td align="right">109,699.36	</td>
	<td align="right">13,712.42	</td>
</tr>
<tr class="udda">
	<th align="right">2019-10-11</th>
	<td align="right">13,067	</td>
	<td align="right">3.54	</td>
	<td align="right">905,689,866	</td>
	<td align="right">3.26	</td>
	<td align="right">663	</td>
	<td align="right">3.90	</td>
	<td align="right">83,860.17	</td>
	<td align="right">10,482.52	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-10-12</th>
	<td align="right">10,427	</td>
	<td align="right">2.82	</td>
	<td align="right">733,794,926	</td>
	<td align="right">2.64	</td>
	<td align="right">561	</td>
	<td align="right">3.30	</td>
	<td align="right">67,943.97	</td>
	<td align="right">8,493.00	</td>
</tr>
<tr class="udda">
	<th align="right">2019-10-13</th>
	<td align="right">10,301	</td>
	<td align="right">2.79	</td>
	<td align="right">722,753,820	</td>
	<td align="right">2.60	</td>
	<td align="right">509	</td>
	<td align="right">3.00	</td>
	<td align="right">66,921.65	</td>
	<td align="right">8,365.21	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-10-14</th>
	<td align="right">11,841	</td>
	<td align="right">3.21	</td>
	<td align="right">816,860,548	</td>
	<td align="right">2.94	</td>
	<td align="right">516	</td>
	<td align="right">3.04	</td>
	<td align="right">75,635.24	</td>
	<td align="right">9,454.40	</td>
</tr>
<tr class="udda">
	<th align="right">2019-10-15</th>
	<td align="right">11,585	</td>
	<td align="right">3.14	</td>
	<td align="right">830,525,306	</td>
	<td align="right">2.99	</td>
	<td align="right">529	</td>
	<td align="right">3.12	</td>
	<td align="right">76,900.49	</td>
	<td align="right">9,612.56	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-10-16</th>
	<td align="right">12,432	</td>
	<td align="right">3.37	</td>
	<td align="right">1,009,401,185	</td>
	<td align="right">3.64	</td>
	<td align="right">567	</td>
	<td align="right">3.34	</td>
	<td align="right">93,463.07	</td>
	<td align="right">11,682.88	</td>
</tr>
<tr class="udda">
	<th align="right">2019-10-17</th>
	<td align="right">15,900	</td>
	<td align="right">4.30	</td>
	<td align="right">1,091,865,755	</td>
	<td align="right">3.93	</td>
	<td align="right">830	</td>
	<td align="right">4.89	</td>
	<td align="right">101,098.68	</td>
	<td align="right">12,637.34	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-10-18</th>
	<td align="right">11,580	</td>
	<td align="right">3.14	</td>
	<td align="right">910,604,461	</td>
	<td align="right">3.28	</td>
	<td align="right">529	</td>
	<td align="right">3.12	</td>
	<td align="right">84,315.23	</td>
	<td align="right">10,539.40	</td>
</tr>
<tr class="udda">
	<th align="right">2019-10-19</th>
	<td align="right">9,977	</td>
	<td align="right">2.70	</td>
	<td align="right">745,791,322	</td>
	<td align="right">2.69	</td>
	<td align="right">416	</td>
	<td align="right">2.45	</td>
	<td align="right">69,054.75	</td>
	<td align="right">8,631.84	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-10-20</th>
	<td align="right">10,715	</td>
	<td align="right">2.90	</td>
	<td align="right">795,303,949	</td>
	<td align="right">2.86	</td>
	<td align="right">454	</td>
	<td align="right">2.67	</td>
	<td align="right">73,639.25	</td>
	<td align="right">9,204.91	</td>
</tr>
<tr class="udda">
	<th align="right">2019-10-21</th>
	<td align="right">10,772	</td>
	<td align="right">2.92	</td>
	<td align="right">705,651,821	</td>
	<td align="right">2.54	</td>
	<td align="right">558	</td>
	<td align="right">3.29	</td>
	<td align="right">65,338.13	</td>
	<td align="right">8,167.27	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-10-22</th>
	<td align="right">11,937	</td>
	<td align="right">3.23	</td>
	<td align="right">839,756,417	</td>
	<td align="right">3.02	</td>
	<td align="right">581	</td>
	<td align="right">3.42	</td>
	<td align="right">77,755.22	</td>
	<td align="right">9,719.40	</td>
</tr>
<tr class="udda">
	<th align="right">2019-10-23</th>
	<td align="right">11,932	</td>
	<td align="right">3.23	</td>
	<td align="right">857,831,835	</td>
	<td align="right">3.09	</td>
	<td align="right">678	</td>
	<td align="right">3.99	</td>
	<td align="right">79,428.87	</td>
	<td align="right">9,928.61	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-10-24</th>
	<td align="right">13,500	</td>
	<td align="right">3.66	</td>
	<td align="right">996,202,457	</td>
	<td align="right">3.59	</td>
	<td align="right">647	</td>
	<td align="right">3.81	</td>
	<td align="right">92,240.97	</td>
	<td align="right">11,530.12	</td>
</tr>
<tr class="udda">
	<th align="right">2019-10-25</th>
	<td align="right">9,847	</td>
	<td align="right">2.67	</td>
	<td align="right">665,389,179	</td>
	<td align="right">2.40	</td>
	<td align="right">442	</td>
	<td align="right">2.60	</td>
	<td align="right">61,610.11	</td>
	<td align="right">7,701.26	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-10-26</th>
	<td align="right">9,178	</td>
	<td align="right">2.48	</td>
	<td align="right">664,953,994	</td>
	<td align="right">2.39	</td>
	<td align="right">358	</td>
	<td align="right">2.11	</td>
	<td align="right">61,569.81	</td>
	<td align="right">7,696.23	</td>
</tr>
<tr class="udda">
	<th align="right">2019-10-27</th>
	<td align="right">9,212	</td>
	<td align="right">2.49	</td>
	<td align="right">625,275,782	</td>
	<td align="right">2.25	</td>
	<td align="right">507	</td>
	<td align="right">2.99	</td>
	<td align="right">57,895.91	</td>
	<td align="right">7,236.99	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-10-28</th>
	<td align="right">10,529	</td>
	<td align="right">2.85	</td>
	<td align="right">717,035,066	</td>
	<td align="right">2.58	</td>
	<td align="right">545	</td>
	<td align="right">3.21	</td>
	<td align="right">66,392.14	</td>
	<td align="right">8,299.02	</td>
</tr>
<tr class="udda">
	<th align="right">2019-10-29</th>
	<td align="right">10,656	</td>
	<td align="right">2.89	</td>
	<td align="right">786,717,361	</td>
	<td align="right">2.83	</td>
	<td align="right">463	</td>
	<td align="right">2.73	</td>
	<td align="right">72,844.20	</td>
	<td align="right">9,105.53	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-10-30</th>
	<td align="right">10,952	</td>
	<td align="right">2.97	</td>
	<td align="right">906,425,079	</td>
	<td align="right">3.26	</td>
	<td align="right">471	</td>
	<td align="right">2.77	</td>
	<td align="right">83,928.25	</td>
	<td align="right">10,491.03	</td>
</tr>
<tr class="udda">
	<th align="right">2019-10-31</th>
	<td align="right">10,674	</td>
	<td align="right">2.89	</td>
	<td align="right">900,527,660	</td>
	<td align="right">3.24	</td>
	<td align="right">441	</td>
	<td align="right">2.60	</td>
	<td align="right">83,382.19	</td>
	<td align="right">10,422.77	</td>
</tr>

</tbody>
</table>

