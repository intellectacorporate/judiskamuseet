


<h2>Statistics for http://www.u5645470.fsdata.se, Month of 2019-10-1 to 2019-10-31</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 20 of 395 Referring Sites,
Sorted by Access Count

</h3>
<p></p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Referring Site</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th class="hogerstalld">1</th>
	<td>
	<a href="https://judiskamuseet.se">
	https://judiskamuseet.se</a>
	</td>
	<td align="right">204,986</td>
	<td align="right">89.71</td>
	<td align="right">24,547,146,838</td>
	<td align="right">97.63</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	<a href="http://judiskamuseet.se">
	http://judiskamuseet.se</a>
	</td>
	<td align="right">10,200</td>
	<td align="right">4.46</td>
	<td align="right">178,756,623</td>
	<td align="right">0.71</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	<a href="https://www.google.com">
	https://www.google.com</a>
	</td>
	<td align="right">5,618</td>
	<td align="right">2.46</td>
	<td align="right">210,953,129</td>
	<td align="right">0.84</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	<a href="https://www.google.se">
	https://www.google.se</a>
	</td>
	<td align="right">2,493</td>
	<td align="right">1.09</td>
	<td align="right">85,840,299</td>
	<td align="right">0.34</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	<a href="https://judiskamuseet.se:443">
	https://judiskamuseet.se:443</a>
	</td>
	<td align="right">1,091</td>
	<td align="right">0.48</td>
	<td align="right">12,714,445</td>
	<td align="right">0.05</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	<a href="https://www.google.de">
	https://www.google.de</a>
	</td>
	<td align="right">571</td>
	<td align="right">0.25</td>
	<td align="right">8,900,570</td>
	<td align="right">0.04</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	<a href="http://www.google.com">
	http://www.google.com</a>
	</td>
	<td align="right">338</td>
	<td align="right">0.15</td>
	<td align="right">2,645,913</td>
	<td align="right">0.01</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	<a href="http://m.facebook.com">
	http://m.facebook.com</a>
	</td>
	<td align="right">295</td>
	<td align="right">0.13</td>
	<td align="right">3,611,337</td>
	<td align="right">0.01</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	<a href="https://www.bing.com">
	https://www.bing.com</a>
	</td>
	<td align="right">248</td>
	<td align="right">0.11</td>
	<td align="right">17,624,116</td>
	<td align="right">0.07</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">10</th>
	<td>
	<a href="http://instagram.com">
	http://instagram.com</a>
	</td>
	<td align="right">166</td>
	<td align="right">0.07</td>
	<td align="right">1,282,860</td>
	<td align="right">0.01</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">11</th>
	<td>
	<a href="http://judiskamuseet.se:80">
	http://judiskamuseet.se:80</a>
	</td>
	<td align="right">126</td>
	<td align="right">0.06</td>
	<td align="right">1,462,478</td>
	<td align="right">0.01</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">12</th>
	<td>
	<a href="http://www.judiska-museet.se">
	http://www.judiska-museet.se</a>
	</td>
	<td align="right">101</td>
	<td align="right">0.04</td>
	<td align="right">3,231,011</td>
	<td align="right">0.01</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">13</th>
	<td>
	<a href="http://site.ru">
	http://site.ru</a>
	</td>
	<td align="right">94</td>
	<td align="right">0.04</td>
	<td align="right">3,992,622</td>
	<td align="right">0.02</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">14</th>
	<td>
	<a href="http://www.sverigesmuseer.se">
	http://www.sverigesmuseer.se</a>
	</td>
	<td align="right">91</td>
	<td align="right">0.04</td>
	<td align="right">1,017,593</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">15</th>
	<td>
	<a href="https://www.elegantthemes.com">
	https://www.elegantthemes.com</a>
	</td>
	<td align="right">91</td>
	<td align="right">0.04</td>
	<td align="right">4,767,885</td>
	<td align="right">0.02</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">16</th>
	<td>
	<a href="https://www.netcraft.com">
	https://www.netcraft.com</a>
	</td>
	<td align="right">60</td>
	<td align="right">0.03</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">17</th>
	<td>
	<a href="https://myactivity.google.com">
	https://myactivity.google.com</a>
	</td>
	<td align="right">48</td>
	<td align="right">0.02</td>
	<td align="right">8,011,290</td>
	<td align="right">0.03</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">18</th>
	<td>
	<a href="https://www.facebook.com">
	https://www.facebook.com</a>
	</td>
	<td align="right">45</td>
	<td align="right">0.02</td>
	<td align="right">562,237</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">19</th>
	<td>
	<a href="https://duckduckgo.com">
	https://duckduckgo.com</a>
	</td>
	<td align="right">37</td>
	<td align="right">0.02</td>
	<td align="right">526,340</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">20</th>
	<td>
	<a href="https://m.facebook.com">
	https://m.facebook.com</a>
	</td>
	<td align="right">36</td>
	<td align="right">0.02</td>
	<td align="right">465,638</td>
	<td align="right">0.00</td>
</tr>

</tbody>
</table>

