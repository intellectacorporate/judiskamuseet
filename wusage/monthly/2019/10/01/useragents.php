


<h2>Statistics for http://www.u5645470.fsdata.se, Month of 2019-10-1 to 2019-10-31</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 10 of 599 Browsers (User Agents),
Sorted by Access Count

</h3>
<p></p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Product</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th class="hogerstalld">1</th>
	<td>
	Safari/537.36		
	</td>
	<td align="right">108,651</td>
	<td align="right">29.45</td>
	<td align="right">10,695,077,652</td>
	<td align="right">38.59</td>
	<td align="right">5,141</td>
	<td align="right">30.37</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	Pingdom.com_bot_version_1.4_		
	</td>
	<td align="right">84,689</td>
	<td align="right">22.96</td>
	<td align="right">703,961,944</td>
	<td align="right">2.54</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	Safari/604.1		
	</td>
	<td align="right">54,913</td>
	<td align="right">14.89</td>
	<td align="right">5,721,715,933</td>
	<td align="right">20.64</td>
	<td align="right">2,738</td>
	<td align="right">16.18</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	Mozilla/5.0 		
	</td>
	<td align="right">31,564</td>
	<td align="right">8.56</td>
	<td align="right">2,520,246,162</td>
	<td align="right">9.09</td>
	<td align="right">2,932</td>
	<td align="right">17.33</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	Safari/605.1.15		
	</td>
	<td align="right">19,172</td>
	<td align="right">5.20</td>
	<td align="right">1,986,993,773</td>
	<td align="right">7.17</td>
	<td align="right">841</td>
	<td align="right">4.97</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	Safari/537.36 Edge/18.18362		
	</td>
	<td align="right">7,692</td>
	<td align="right">2.09</td>
	<td align="right">900,586,442</td>
	<td align="right">3.25</td>
	<td align="right">286</td>
	<td align="right">1.69</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	Safari/605.1		
	</td>
	<td align="right">5,936</td>
	<td align="right">1.61</td>
	<td align="right">712,059,134</td>
	<td align="right">2.57</td>
	<td align="right">275</td>
	<td align="right">1.63</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	UptimeRobot/2.0		
	</td>
	<td align="right">5,726</td>
	<td align="right">1.55</td>
	<td align="right">214,368</td>
	<td align="right">0.00</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	bingbot/2.0		
	</td>
	<td align="right">5,512</td>
	<td align="right">1.49</td>
	<td align="right">339,625,312</td>
	<td align="right">1.23</td>
	<td align="right">1,121</td>
	<td align="right">6.62</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">10</th>
	<td>
	Safari/537.36 (compatible; Googlebot/2.1;  http://www.google.com/bot.html)		
	</td>
	<td align="right">2,833</td>
	<td align="right">0.77</td>
	<td align="right">39,013,888</td>
	<td align="right">0.14</td>
	<td align="right">561</td>
	<td align="right">3.32</td>
</tr>

</tbody>
</table>

