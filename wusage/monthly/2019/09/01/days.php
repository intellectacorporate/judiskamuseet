


<h2>Statistics for http://www.u5645470.fsdata.se, Month of 2019-9-1 to 2019-9-30</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Traffic by Day</h3>
<p>The continuous line represents a 7-day moving average.</p>
<img src="d.png" width="468" height="351">
<br /><br />


<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Date</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	<th scope='col'>Bits per Sec</th>
	<th scope='col'>Bytes per Sec</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th align="right">2019-9-1</th>
	<td align="right">7,201	</td>
	<td align="right">2.25	</td>
	<td align="right">1,241,826,481	</td>
	<td align="right">5.58	</td>
	<td align="right">366	</td>
	<td align="right">2.82	</td>
	<td align="right">114,983.93	</td>
	<td align="right">14,372.99	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-9-2</th>
	<td align="right">10,840	</td>
	<td align="right">3.38	</td>
	<td align="right">1,533,387,025	</td>
	<td align="right">6.90	</td>
	<td align="right">394	</td>
	<td align="right">3.03	</td>
	<td align="right">141,980.28	</td>
	<td align="right">17,747.54	</td>
</tr>
<tr class="udda">
	<th align="right">2019-9-3</th>
	<td align="right">9,064	</td>
	<td align="right">2.83	</td>
	<td align="right">1,318,613,703	</td>
	<td align="right">5.93	</td>
	<td align="right">367	</td>
	<td align="right">2.82	</td>
	<td align="right">122,093.86	</td>
	<td align="right">15,261.73	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-9-4</th>
	<td align="right">7,915	</td>
	<td align="right">2.47	</td>
	<td align="right">543,539,979	</td>
	<td align="right">2.44	</td>
	<td align="right">349	</td>
	<td align="right">2.68	</td>
	<td align="right">50,327.78	</td>
	<td align="right">6,290.97	</td>
</tr>
<tr class="udda">
	<th align="right">2019-9-5</th>
	<td align="right">11,495	</td>
	<td align="right">3.59	</td>
	<td align="right">736,864,339	</td>
	<td align="right">3.31	</td>
	<td align="right">410	</td>
	<td align="right">3.15	</td>
	<td align="right">68,228.18	</td>
	<td align="right">8,528.52	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-9-6</th>
	<td align="right">7,545	</td>
	<td align="right">2.35	</td>
	<td align="right">504,339,366	</td>
	<td align="right">2.27	</td>
	<td align="right">344	</td>
	<td align="right">2.65	</td>
	<td align="right">46,698.09	</td>
	<td align="right">5,837.26	</td>
</tr>
<tr class="udda">
	<th align="right">2019-9-7</th>
	<td align="right">6,735	</td>
	<td align="right">2.10	</td>
	<td align="right">566,456,935	</td>
	<td align="right">2.55	</td>
	<td align="right">332	</td>
	<td align="right">2.55	</td>
	<td align="right">52,449.72	</td>
	<td align="right">6,556.21	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-9-8</th>
	<td align="right">6,500	</td>
	<td align="right">2.03	</td>
	<td align="right">472,653,692	</td>
	<td align="right">2.13	</td>
	<td align="right">332	</td>
	<td align="right">2.55	</td>
	<td align="right">43,764.23	</td>
	<td align="right">5,470.53	</td>
</tr>
<tr class="udda">
	<th align="right">2019-9-9</th>
	<td align="right">11,090	</td>
	<td align="right">3.46	</td>
	<td align="right">580,690,801	</td>
	<td align="right">2.61	</td>
	<td align="right">322	</td>
	<td align="right">2.48	</td>
	<td align="right">53,767.67	</td>
	<td align="right">6,720.96	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-9-10</th>
	<td align="right">13,644	</td>
	<td align="right">4.26	</td>
	<td align="right">859,471,802	</td>
	<td align="right">3.87	</td>
	<td align="right">514	</td>
	<td align="right">3.95	</td>
	<td align="right">79,580.72	</td>
	<td align="right">9,947.59	</td>
</tr>
<tr class="udda">
	<th align="right">2019-9-11</th>
	<td align="right">11,635	</td>
	<td align="right">3.63	</td>
	<td align="right">750,098,756	</td>
	<td align="right">3.37	</td>
	<td align="right">409	</td>
	<td align="right">3.15	</td>
	<td align="right">69,453.59	</td>
	<td align="right">8,681.70	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-9-12</th>
	<td align="right">12,169	</td>
	<td align="right">3.80	</td>
	<td align="right">765,508,504	</td>
	<td align="right">3.44	</td>
	<td align="right">461	</td>
	<td align="right">3.55	</td>
	<td align="right">70,880.42	</td>
	<td align="right">8,860.05	</td>
</tr>
<tr class="udda">
	<th align="right">2019-9-13</th>
	<td align="right">18,796	</td>
	<td align="right">5.87	</td>
	<td align="right">1,001,699,706	</td>
	<td align="right">4.50	</td>
	<td align="right">614	</td>
	<td align="right">4.72	</td>
	<td align="right">92,749.97	</td>
	<td align="right">11,593.75	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-9-14</th>
	<td align="right">8,790	</td>
	<td align="right">2.74	</td>
	<td align="right">637,686,871	</td>
	<td align="right">2.87	</td>
	<td align="right">348	</td>
	<td align="right">2.68	</td>
	<td align="right">59,045.08	</td>
	<td align="right">7,380.64	</td>
</tr>
<tr class="udda">
	<th align="right">2019-9-15</th>
	<td align="right">9,070	</td>
	<td align="right">2.83	</td>
	<td align="right">676,503,713	</td>
	<td align="right">3.04	</td>
	<td align="right">355	</td>
	<td align="right">2.73	</td>
	<td align="right">62,639.23	</td>
	<td align="right">7,829.90	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-9-16</th>
	<td align="right">10,584	</td>
	<td align="right">3.30	</td>
	<td align="right">642,131,328	</td>
	<td align="right">2.89	</td>
	<td align="right">457	</td>
	<td align="right">3.52	</td>
	<td align="right">59,456.60	</td>
	<td align="right">7,432.08	</td>
</tr>
<tr class="udda">
	<th align="right">2019-9-17</th>
	<td align="right">12,059	</td>
	<td align="right">3.76	</td>
	<td align="right">793,722,908	</td>
	<td align="right">3.57	</td>
	<td align="right">464	</td>
	<td align="right">3.57	</td>
	<td align="right">73,492.86	</td>
	<td align="right">9,186.61	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-9-18</th>
	<td align="right">12,579	</td>
	<td align="right">3.93	</td>
	<td align="right">747,264,586	</td>
	<td align="right">3.36	</td>
	<td align="right">454	</td>
	<td align="right">3.49	</td>
	<td align="right">69,191.17	</td>
	<td align="right">8,648.90	</td>
</tr>
<tr class="udda">
	<th align="right">2019-9-19</th>
	<td align="right">14,180	</td>
	<td align="right">4.43	</td>
	<td align="right">758,683,512	</td>
	<td align="right">3.41	</td>
	<td align="right">552	</td>
	<td align="right">4.25	</td>
	<td align="right">70,248.47	</td>
	<td align="right">8,781.06	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-9-20</th>
	<td align="right">9,599	</td>
	<td align="right">3.00	</td>
	<td align="right">569,625,817	</td>
	<td align="right">2.56	</td>
	<td align="right">530	</td>
	<td align="right">4.08	</td>
	<td align="right">52,743.13	</td>
	<td align="right">6,592.89	</td>
</tr>
<tr class="udda">
	<th align="right">2019-9-21</th>
	<td align="right">7,035	</td>
	<td align="right">2.20	</td>
	<td align="right">404,108,728	</td>
	<td align="right">1.82	</td>
	<td align="right">332	</td>
	<td align="right">2.55	</td>
	<td align="right">37,417.47	</td>
	<td align="right">4,677.18	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-9-22</th>
	<td align="right">7,980	</td>
	<td align="right">2.49	</td>
	<td align="right">580,212,812	</td>
	<td align="right">2.61	</td>
	<td align="right">348	</td>
	<td align="right">2.68	</td>
	<td align="right">53,723.41	</td>
	<td align="right">6,715.43	</td>
</tr>
<tr class="udda">
	<th align="right">2019-9-23</th>
	<td align="right">10,323	</td>
	<td align="right">3.22	</td>
	<td align="right">541,128,895	</td>
	<td align="right">2.43	</td>
	<td align="right">412	</td>
	<td align="right">3.17	</td>
	<td align="right">50,104.53	</td>
	<td align="right">6,263.07	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-9-24</th>
	<td align="right">12,364	</td>
	<td align="right">3.86	</td>
	<td align="right">780,063,452	</td>
	<td align="right">3.51	</td>
	<td align="right">470	</td>
	<td align="right">3.62	</td>
	<td align="right">72,228.10	</td>
	<td align="right">9,028.51	</td>
</tr>
<tr class="udda">
	<th align="right">2019-9-25</th>
	<td align="right">12,545	</td>
	<td align="right">3.92	</td>
	<td align="right">793,470,478	</td>
	<td align="right">3.57	</td>
	<td align="right">451	</td>
	<td align="right">3.47	</td>
	<td align="right">73,469.49	</td>
	<td align="right">9,183.69	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-9-26</th>
	<td align="right">11,447	</td>
	<td align="right">3.57	</td>
	<td align="right">690,742,488	</td>
	<td align="right">3.11	</td>
	<td align="right">488	</td>
	<td align="right">3.75	</td>
	<td align="right">63,957.64	</td>
	<td align="right">7,994.70	</td>
</tr>
<tr class="udda">
	<th align="right">2019-9-27</th>
	<td align="right">12,606	</td>
	<td align="right">3.93	</td>
	<td align="right">677,236,418	</td>
	<td align="right">3.05	</td>
	<td align="right">525	</td>
	<td align="right">4.04	</td>
	<td align="right">62,707.08	</td>
	<td align="right">7,838.38	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-9-28</th>
	<td align="right">10,513	</td>
	<td align="right">3.28	</td>
	<td align="right">607,747,911	</td>
	<td align="right">2.73	</td>
	<td align="right">536	</td>
	<td align="right">4.12	</td>
	<td align="right">56,272.95	</td>
	<td align="right">7,034.12	</td>
</tr>
<tr class="udda">
	<th align="right">2019-9-29</th>
	<td align="right">11,361	</td>
	<td align="right">3.55	</td>
	<td align="right">665,991,674	</td>
	<td align="right">2.99	</td>
	<td align="right">553	</td>
	<td align="right">4.25	</td>
	<td align="right">61,665.90	</td>
	<td align="right">7,708.24	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-9-30</th>
	<td align="right">12,730	</td>
	<td align="right">3.97	</td>
	<td align="right">795,805,719	</td>
	<td align="right">3.58	</td>
	<td align="right">511	</td>
	<td align="right">3.93	</td>
	<td align="right">73,685.71	</td>
	<td align="right">9,210.71	</td>
</tr>

</tbody>
</table>

