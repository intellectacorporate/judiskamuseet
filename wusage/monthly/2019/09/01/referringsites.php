


<h2>Statistics for http://www.u5645470.fsdata.se, Month of 2019-9-1 to 2019-9-30</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 20 of 441 Referring Sites,
Sorted by Access Count

</h3>
<p></p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Referring Site</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th class="hogerstalld">1</th>
	<td>
	<a href="https://judiskamuseet.se">
	https://judiskamuseet.se</a>
	</td>
	<td align="right">247,079</td>
	<td align="right">91.26</td>
	<td align="right">19,556,503,170</td>
	<td align="right">96.97</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	<a href="http://judiskamuseet.se">
	http://judiskamuseet.se</a>
	</td>
	<td align="right">11,474</td>
	<td align="right">4.24</td>
	<td align="right">212,906,042</td>
	<td align="right">1.06</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	<a href="https://www.google.com">
	https://www.google.com</a>
	</td>
	<td align="right">5,957</td>
	<td align="right">2.20</td>
	<td align="right">219,995,994</td>
	<td align="right">1.09</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	<a href="https://www.google.se">
	https://www.google.se</a>
	</td>
	<td align="right">2,652</td>
	<td align="right">0.98</td>
	<td align="right">69,883,134</td>
	<td align="right">0.35</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	<a href="https://www.google.de">
	https://www.google.de</a>
	</td>
	<td align="right">408</td>
	<td align="right">0.15</td>
	<td align="right">6,617,623</td>
	<td align="right">0.03</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	<a href="http://m.facebook.com">
	http://m.facebook.com</a>
	</td>
	<td align="right">312</td>
	<td align="right">0.12</td>
	<td align="right">3,523,253</td>
	<td align="right">0.02</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	<a href="https://www.bing.com">
	https://www.bing.com</a>
	</td>
	<td align="right">242</td>
	<td align="right">0.09</td>
	<td align="right">22,245,241</td>
	<td align="right">0.11</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	<a href="http://www.google.com">
	http://www.google.com</a>
	</td>
	<td align="right">199</td>
	<td align="right">0.07</td>
	<td align="right">1,711,326</td>
	<td align="right">0.01</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	<a href="http://judiskamuseet.se:80">
	http://judiskamuseet.se:80</a>
	</td>
	<td align="right">112</td>
	<td align="right">0.04</td>
	<td align="right">1,218,965</td>
	<td align="right">0.01</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">10</th>
	<td>
	<a href="http://www.judiska-museet.se">
	http://www.judiska-museet.se</a>
	</td>
	<td align="right">102</td>
	<td align="right">0.04</td>
	<td align="right">46,600</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">11</th>
	<td>
	<a href="https://judiskamuseet.se:443">
	https://judiskamuseet.se:443</a>
	</td>
	<td align="right">96</td>
	<td align="right">0.04</td>
	<td align="right">1,050,784</td>
	<td align="right">0.01</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">12</th>
	<td>
	<a href="https://www.elegantthemes.com">
	https://www.elegantthemes.com</a>
	</td>
	<td align="right">95</td>
	<td align="right">0.04</td>
	<td align="right">5,082,026</td>
	<td align="right">0.03</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">13</th>
	<td>
	<a href="https://www.facebook.com">
	https://www.facebook.com</a>
	</td>
	<td align="right">78</td>
	<td align="right">0.03</td>
	<td align="right">1,216,771</td>
	<td align="right">0.01</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">14</th>
	<td>
	<a href="https://sms7.schoolsoft.se">
	https://sms7.schoolsoft.se</a>
	</td>
	<td align="right">72</td>
	<td align="right">0.03</td>
	<td align="right">961,771</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">15</th>
	<td>
	<a href="https://myactivity.google.com">
	https://myactivity.google.com</a>
	</td>
	<td align="right">48</td>
	<td align="right">0.02</td>
	<td align="right">15,487,683</td>
	<td align="right">0.08</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">16</th>
	<td>
	<a href="http://instagram.com">
	http://instagram.com</a>
	</td>
	<td align="right">46</td>
	<td align="right">0.02</td>
	<td align="right">313,199</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">17</th>
	<td>
	<a href="http://site.ru">
	http://site.ru</a>
	</td>
	<td align="right">45</td>
	<td align="right">0.02</td>
	<td align="right">1,566,706</td>
	<td align="right">0.01</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">18</th>
	<td>
	<a href="http://www.google.com.hk">
	http://www.google.com.hk</a>
	</td>
	<td align="right">45</td>
	<td align="right">0.02</td>
	<td align="right">10,624</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">19</th>
	<td>
	<a href="https://m.facebook.com">
	https://m.facebook.com</a>
	</td>
	<td align="right">43</td>
	<td align="right">0.02</td>
	<td align="right">550,639</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">20</th>
	<td>
	<a href="https://sms1.schoolsoft.se">
	https://sms1.schoolsoft.se</a>
	</td>
	<td align="right">39</td>
	<td align="right">0.01</td>
	<td align="right">556,514</td>
	<td align="right">0.00</td>
</tr>

</tbody>
</table>

