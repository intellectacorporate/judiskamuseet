


<h2>Statistics for http://www.u5645470.fsdata.se, Month of 2019-9-1 to 2019-9-30</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Traffic by Hour</h3>
<p>Figures are averages for that hour
	of each day in the time period analyzed.</p>
<img src="h.png" width="468" height="351">
<br /><br />
<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Hour</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	<th scope='col'>Bits per Sec</th>
	<th scope='col'>Bytes per Sec</th>
	
	</tr>
</thead>
<tbody>
<tr class="jamnudda">
	<th align="right">00:00</th>
	<td align="right">163.60</td>
	<td align="right">1.53</td>
	<td align="right">11,827,810.33</td>
	<td align="right">1.60</td>
	<td align="right">6.58</td>
	<td align="right">1.52</td>
	<td align="right">26,284.02</td>
	<td align="right">3,285.50</td>
	
</tr>
<tr class="jamn">
	<th align="right">01:00</th>
	<td align="right">116.93</td>
	<td align="right">1.09</td>
	<td align="right">6,017,472.13</td>
	<td align="right">0.81</td>
	<td align="right">8.22</td>
	<td align="right">1.90</td>
	<td align="right">13,372.16</td>
	<td align="right">1,671.52</td>
	
</tr>
<tr class="udda">
	<th align="right">02:00</th>
	<td align="right">87.40</td>
	<td align="right">0.82</td>
	<td align="right">4,547,481.47</td>
	<td align="right">0.61</td>
	<td align="right">7.33</td>
	<td align="right">1.69</td>
	<td align="right">10,105.51</td>
	<td align="right">1,263.19</td>
	
</tr>
<tr class="jamn">
	<th align="right">03:00</th>
	<td align="right">94.10</td>
	<td align="right">0.88</td>
	<td align="right">4,129,302.07</td>
	<td align="right">0.56</td>
	<td align="right">7.37</td>
	<td align="right">1.70</td>
	<td align="right">9,176.23</td>
	<td align="right">1,147.03</td>
	
</tr>
<tr class="udda">
	<th align="right">04:00</th>
	<td align="right">101.93</td>
	<td align="right">0.95</td>
	<td align="right">3,309,739.57</td>
	<td align="right">0.45</td>
	<td align="right">7.30</td>
	<td align="right">1.68</td>
	<td align="right">7,354.98</td>
	<td align="right">919.37</td>
	
</tr>
<tr class="jamn">
	<th align="right">05:00</th>
	<td align="right">100.63</td>
	<td align="right">0.94</td>
	<td align="right">4,881,503.63</td>
	<td align="right">0.66</td>
	<td align="right">6.57</td>
	<td align="right">1.52</td>
	<td align="right">10,847.79</td>
	<td align="right">1,355.97</td>
	
</tr>
<tr class="udda">
	<th align="right">06:00</th>
	<td align="right">173.43</td>
	<td align="right">1.62</td>
	<td align="right">9,548,049.03</td>
	<td align="right">1.29</td>
	<td align="right">7.14</td>
	<td align="right">1.65</td>
	<td align="right">21,217.89</td>
	<td align="right">2,652.24</td>
	
</tr>
<tr class="jamn">
	<th align="right">07:00</th>
	<td align="right">257.57</td>
	<td align="right">2.41</td>
	<td align="right">17,712,987.10</td>
	<td align="right">2.39</td>
	<td align="right">11.52</td>
	<td align="right">2.66</td>
	<td align="right">39,362.19</td>
	<td align="right">4,920.27</td>
	
</tr>
<tr class="udda">
	<th align="right">08:00</th>
	<td align="right">475.30</td>
	<td align="right">4.45</td>
	<td align="right">31,281,759.67</td>
	<td align="right">4.22</td>
	<td align="right">17.07</td>
	<td align="right">3.94</td>
	<td align="right">69,515.02</td>
	<td align="right">8,689.38</td>
	
</tr>
<tr class="jamn">
	<th align="right">09:00</th>
	<td align="right">755.03</td>
	<td align="right">7.07</td>
	<td align="right">49,580,342.63</td>
	<td align="right">6.69</td>
	<td align="right">25.22</td>
	<td align="right">5.82</td>
	<td align="right">110,178.54</td>
	<td align="right">13,772.32</td>
	
</tr>
<tr class="udda">
	<th align="right">10:00</th>
	<td align="right">860.90</td>
	<td align="right">8.06</td>
	<td align="right">65,426,476.93</td>
	<td align="right">8.83</td>
	<td align="right">27.82</td>
	<td align="right">6.42</td>
	<td align="right">145,392.17</td>
	<td align="right">18,174.02</td>
	
</tr>
<tr class="jamn">
	<th align="right">11:00</th>
	<td align="right">798.57</td>
	<td align="right">7.48</td>
	<td align="right">58,883,545.83</td>
	<td align="right">7.94</td>
	<td align="right">30.97</td>
	<td align="right">7.15</td>
	<td align="right">130,852.32</td>
	<td align="right">16,356.54</td>
	
</tr>
<tr class="udda">
	<th align="right">12:00</th>
	<td align="right">757.97</td>
	<td align="right">7.10</td>
	<td align="right">54,115,698.27</td>
	<td align="right">7.30</td>
	<td align="right">26.25</td>
	<td align="right">6.06</td>
	<td align="right">120,257.11</td>
	<td align="right">15,032.14</td>
	
</tr>
<tr class="jamn">
	<th align="right">13:00</th>
	<td align="right">779.07</td>
	<td align="right">7.29</td>
	<td align="right">56,452,914.73</td>
	<td align="right">7.62</td>
	<td align="right">29.41</td>
	<td align="right">6.79</td>
	<td align="right">125,450.92</td>
	<td align="right">15,681.37</td>
	
</tr>
<tr class="udda">
	<th align="right">14:00</th>
	<td align="right">723.23</td>
	<td align="right">6.77</td>
	<td align="right">51,381,873.40</td>
	<td align="right">6.93</td>
	<td align="right">27.47</td>
	<td align="right">6.34</td>
	<td align="right">114,181.94</td>
	<td align="right">14,272.74</td>
	
</tr>
<tr class="jamn">
	<th align="right">15:00</th>
	<td align="right">681.37</td>
	<td align="right">6.38</td>
	<td align="right">47,294,741.97</td>
	<td align="right">6.38</td>
	<td align="right">26.44</td>
	<td align="right">6.10</td>
	<td align="right">105,099.43</td>
	<td align="right">13,137.43</td>
	
</tr>
<tr class="udda">
	<th align="right">16:00</th>
	<td align="right">619.13</td>
	<td align="right">5.80</td>
	<td align="right">41,762,477.03</td>
	<td align="right">5.63</td>
	<td align="right">23.05</td>
	<td align="right">5.32</td>
	<td align="right">92,805.50</td>
	<td align="right">11,600.69</td>
	
</tr>
<tr class="jamn">
	<th align="right">17:00</th>
	<td align="right">542.20</td>
	<td align="right">5.08</td>
	<td align="right">36,738,672.23</td>
	<td align="right">4.96</td>
	<td align="right">21.17</td>
	<td align="right">4.89</td>
	<td align="right">81,641.49</td>
	<td align="right">10,205.19</td>
	
</tr>
<tr class="udda">
	<th align="right">18:00</th>
	<td align="right">450.50</td>
	<td align="right">4.22</td>
	<td align="right">31,882,389.33</td>
	<td align="right">4.30</td>
	<td align="right">20.65</td>
	<td align="right">4.77</td>
	<td align="right">70,849.75</td>
	<td align="right">8,856.22</td>
	
</tr>
<tr class="jamn">
	<th align="right">19:00</th>
	<td align="right">463.57</td>
	<td align="right">4.34</td>
	<td align="right">35,710,365.97</td>
	<td align="right">4.82</td>
	<td align="right">19.31</td>
	<td align="right">4.46</td>
	<td align="right">79,356.37</td>
	<td align="right">9,919.55</td>
	
</tr>
<tr class="udda">
	<th align="right">20:00</th>
	<td align="right">486.33</td>
	<td align="right">4.55</td>
	<td align="right">32,642,308.23</td>
	<td align="right">4.40</td>
	<td align="right">19.09</td>
	<td align="right">4.41</td>
	<td align="right">72,538.46</td>
	<td align="right">9,067.31</td>
	
</tr>
<tr class="jamn">
	<th align="right">21:00</th>
	<td align="right">487.80</td>
	<td align="right">4.57</td>
	<td align="right">35,555,201.60</td>
	<td align="right">4.80</td>
	<td align="right">19.14</td>
	<td align="right">4.42</td>
	<td align="right">79,011.56</td>
	<td align="right">9,876.44</td>
	
</tr>
<tr class="udda">
	<th align="right">22:00</th>
	<td align="right">439.70</td>
	<td align="right">4.12</td>
	<td align="right">30,916,341.77</td>
	<td align="right">4.17</td>
	<td align="right">18.00</td>
	<td align="right">4.15</td>
	<td align="right">68,702.98</td>
	<td align="right">8,587.87</td>
	
</tr>
<tr class="jamn">
	<th align="right">23:00</th>
	<td align="right">263.53</td>
	<td align="right">2.47</td>
	<td align="right">19,643,158.37</td>
	<td align="right">2.65</td>
	<td align="right">20.23</td>
	<td align="right">4.67</td>
	<td align="right">43,651.46</td>
	<td align="right">5,456.43</td>
	
</tr>

</tbody>
</table>

