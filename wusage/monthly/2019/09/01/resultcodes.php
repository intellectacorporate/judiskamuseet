


<h2>Statistics for http://www.u5645470.fsdata.se, Month of 2019-9-1 to 2019-9-30</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>HTTP Result Codes, Sorted by Code</h3>
<p></p>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Code</th>
	<th scope='col'>Description</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th class="hogerstalld">0</th>
	<td></td>
	<td align="right">23</td>
	<td align="right">0.01</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">103</th>
	<td></td>
	<td align="right">5,042</td>
	<td align="right">1.53</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">200</th>
	<td>OK</td>
	<td align="right">299,331</td>
	<td align="right">90.63</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">206</th>
	<td>Partial Content</td>
	<td align="right">234</td>
	<td align="right">0.07</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">301</th>
	<td>Moved Permanently</td>
	<td align="right">12,235</td>
	<td align="right">3.70</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">302</th>
	<td>Found</td>
	<td align="right">488</td>
	<td align="right">0.15</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">304</th>
	<td>Not Modified</td>
	<td align="right">8,106</td>
	<td align="right">2.45</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">400</th>
	<td>Bad Request</td>
	<td align="right">90</td>
	<td align="right">0.03</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">401</th>
	<td>Unauthorized</td>
	<td align="right">5</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">403</th>
	<td>Forbidden</td>
	<td align="right">232</td>
	<td align="right">0.07</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">404</th>
	<td>Not Found</td>
	<td align="right">2,208</td>
	<td align="right">0.67</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">405</th>
	<td>Method Not Allowed</td>
	<td align="right">22</td>
	<td align="right">0.01</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">409</th>
	<td>Conflict</td>
	<td align="right">1</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">500</th>
	<td>Internal Server Error</td>
	<td align="right">1,007</td>
	<td align="right">0.30</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">503</th>
	<td>Service Unavailable</td>
	<td align="right">1,236</td>
	<td align="right">0.37</td>
</tr>

</tbody>
</table>




