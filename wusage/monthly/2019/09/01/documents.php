

<script>
var componentsData = new Array(
        
        "*end*"
);
var componentsByPage = new Array();
var epochRange = "";
var sortedByAccesses = 0;
var sortedByBytes = 0;
var sortedByVisits = 0;
var alphabetized = 0;
var withBytes = 0;
var withVisits = 0;
var withDownloads = 0;
var subreportKeyword = "";
var subreportTitle = "";
var subreportAll = 0;
var subreportTop = 0;
var subreportTotal = 0;
var subreportPie = "";
var subreportGreenbarFlag = 0;
var pc = 0, ac = 0;
var dataEnd = 0;
var data;
var dataArrays = new Array(
	new Array(
		"Month of 2019-9-1 to 2019-9-30",	
		"*sorted-by-accesses*",
		"*bytes*",
		"*visits*",
		"*subreport*",
	"pages",
	"Documents",
	"*top*",
		30,
	
	"*total*",
	"3,785",
	"*pie*",
	"",
	"*row*",
	"1",
	"/",
	"*local-link*",
		"http://www.u5645470.fsdata.se/",
	
	"22,976",
	"7.17",
	"193,090,661",
		"0.87",
	"3,527",
		"1.26",
	"*row*",
	"2",
	"/wp-content/themes/Divi/core/admin/js/common.js",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/themes/Divi/core/admin/js/common.js",
	
	"7,189",
	"2.24",
	"3,970,994",
		"0.02",
	"7,247",
		"2.58",
	"*row*",
	"3",
	"/wp-content/themes/Divi/js/custom.min.js",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/themes/Divi/js/custom.min.js",
	
	"7,131",
	"2.23",
	"456,125,059",
		"2.05",
	"7,212",
		"2.57",
	"*row*",
	"4",
	"/wp-content/themes/divi-child/style.css",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/themes/divi-child/style.css",
	
	"7,102",
	"2.22",
	"58,775,976",
		"0.26",
	"7,161",
		"2.55",
	"*row*",
	"5",
	"/wp-content/themes/Divi/style.css",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/themes/Divi/style.css",
	
	"7,090",
	"2.21",
	"520,266,714",
		"2.34",
	"7,154",
		"2.55",
	"*row*",
	"6",
	"/wp-includes/css/dashicons.min.css",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-includes/css/dashicons.min.css",
	
	"6,939",
	"2.17",
	"196,749,169",
		"0.88",
	"7,022",
		"2.50",
	"*row*",
	"7",
	"/wp-includes/css/dist/block-library/style.min.css",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-includes/css/dist/block-library/style.min.css",
	
	"6,854",
	"2.14",
	"33,126,912",
		"0.15",
	"6,925",
		"2.47",
	"*row*",
	"8",
	"/wp-content/plugins/sitepress-multilingual-cms/templates/lan<br>\nguage-switchers/menu-item/style.css",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/plugins/sitepress-multilingual-cms/templates/language-switchers/menu-item/style.css",
	
	"6,835",
	"2.13",
	"937,297",
		"0.00",
	"6,940",
		"2.47",
	"*row*",
	"9",
	"/wp-includes/js/wp-embed.min.js",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-includes/js/wp-embed.min.js",
	
	"6,822",
	"2.13",
	"5,131,173",
		"0.02",
	"6,932",
		"2.47",
	"*row*",
	"10",
	"/wp-content/themes/divi-child/js/divi-child.js",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/themes/divi-child/js/divi-child.js",
	
	"6,804",
	"2.12",
	"6,092,952",
		"0.03",
	"6,921",
		"2.46",
	"*row*",
	"11",
	"/wp-content/plugins/wp-smushit/app/assets/js/smush-lazy-load<br>\n.min.js",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/plugins/wp-smushit/app/assets/js/smush-lazy-load.min.js",
	
	"6,804",
	"2.12",
	"24,370,187",
		"0.11",
	"6,908",
		"2.46",
	"*row*",
	"12",
	"/wp-content/plugins/mailchimp-for-wp/assets/js/forms-api.min<br>\n.js",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/plugins/mailchimp-for-wp/assets/js/forms-api.min.js",
	
	"6,800",
	"2.12",
	"50,855,177",
		"0.23",
	"6,915",
		"2.46",
	"*row*",
	"13",
	"/wp-includes/js/jquery/jquery.js",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-includes/js/jquery/jquery.js",
	
	"6,786",
	"2.12",
	"227,450,902",
		"1.02",
	"6,893",
		"2.45",
	"*row*",
	"14",
	"/wp-includes/js/wp-emoji-release.min.js",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-includes/js/wp-emoji-release.min.js",
	
	"6,750",
	"2.11",
	"31,211,664",
		"0.14",
	"6,839",
		"2.44",
	"*row*",
	"15",
	"/wp-content/plugins/wp-pagenavi/pagenavi-css.css",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/plugins/wp-pagenavi/pagenavi-css.css",
	
	"6,748",
	"2.11",
	"1,604,821",
		"0.01",
	"6,850",
		"2.44",
	"*row*",
	"16",
	"/wp-includes/js/jquery/jquery-migrate.min.js",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-includes/js/jquery/jquery-migrate.min.js",
	
	"6,733",
	"2.10",
	"27,036,780",
		"0.12",
	"6,842",
		"2.44",
	"*row*",
	"17",
	"/wp-content/uploads/2017/06/Judiska_centr_svart_web-1.png",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/uploads/2017/06/Judiska_centr_svart_web-1.png",
	
	"6,698",
	"2.09",
	"60,360,850",
		"0.27",
	"6,650",
		"2.37",
	"*row*",
	"18",
	"/wp-content/themes/Divi/core/admin/fonts/modules.ttf",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/themes/Divi/core/admin/fonts/modules.ttf",
	
	"6,685",
	"2.09",
	"594,419,147",
		"2.67",
	"6,781",
		"2.41",
	"*row*",
	"19",
	"/wp-content/plugins/the-events-calendar/common/src/resources<br>\n/css/reset.min.css",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/plugins/the-events-calendar/common/src/resources/css/reset.min.css",
	
	"5,781",
	"1.80",
	"6,058,916",
		"0.03",
	"5,709",
		"2.03",
	"*row*",
	"20",
	"/wp-content/plugins/the-events-calendar/common/src/resources<br>\n/css/common.min.css",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/plugins/the-events-calendar/common/src/resources/css/common.min.css",
	
	"5,720",
	"1.79",
	"31,410,240",
		"0.14",
	"5,719",
		"2.04",
	"*row*",
	"21",
	"/wp-content/plugins/the-events-calendar/common/src/resources<br>\n/css/tooltip.min.css",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/plugins/the-events-calendar/common/src/resources/css/tooltip.min.css",
	
	"5,718",
	"1.78",
	"3,146,493",
		"0.01",
	"5,721",
		"2.04",
	"*row*",
	"22",
	"/wp-content/plugins/the-events-calendar/common/src/resources<br>\n/js/tooltip.min.js",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/plugins/the-events-calendar/common/src/resources/js/tooltip.min.js",
	
	"5,678",
	"1.77",
	"1,422,194",
		"0.01",
	"5,684",
		"2.02",
	"*row*",
	"23",
	"/wp-content/plugins/the-events-calendar/common/src/resources<br>\n/js/tribe-common.min.js",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/plugins/the-events-calendar/common/src/resources/js/tribe-common.min.js",
	
	"5,670",
	"1.77",
	"855,153",
		"0.00",
	"5,693",
		"2.03",
	"*row*",
	"24",
	"/wp-content/uploads/2019/05/cropped-Judiska_svart-2-32x32.pn<br>\ng",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/uploads/2019/05/cropped-Judiska_svart-2-32x32.png",
	
	"4,785",
	"1.49",
	"3,592,512",
		"0.02",
	"4,609",
		"1.64",
	"*row*",
	"25",
	"/wp-admin/admin-ajax.php",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-admin/admin-ajax.php",
	
	"4,635",
	"1.45",
	"93,802,052",
		"0.42",
	"120",
		"0.04",
	"*row*",
	"26",
	"/wp-content/themes/divi-child/img/location.png",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/themes/divi-child/img/location.png",
	
	"4,462",
	"1.39",
	"13,507,842",
		"0.06",
	"4,580",
		"1.63",
	"*row*",
	"27",
	"/wp-content/themes/divi-child/img/Instagram.png",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/themes/divi-child/img/Instagram.png",
	
	"4,269",
	"1.33",
	"25,487,201",
		"0.11",
	"4,393",
		"1.56",
	"*row*",
	"28",
	"/wp-content/themes/divi-child/img/facebook.png",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/themes/divi-child/img/facebook.png",
	
	"4,235",
	"1.32",
	"20,255,092",
		"0.09",
	"4,382",
		"1.56",
	"*row*",
	"29",
	"/wp-content/themes/divi-child/img/clock.png",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/themes/divi-child/img/clock.png",
	
	"4,139",
	"1.29",
	"9,341,050",
		"0.04",
	"4,282",
		"1.52",
	"*row*",
	"30",
	"/wp-content/uploads/2019/05/cropped-Judiska_svart-2-192x192.<br>\npng",
	"*local-link*",
		"http://www.u5645470.fsdata.se/wp-content/uploads/2019/05/cropped-Judiska_svart-2-192x192.png",
	
	"3,423",
	"1.07",
	"22,578,142",
		"0.10",
	"3,344",
		"1.19",
	
		"*end*"
		)
	);

function incPC()
{
	pc++;
	if (pc == data.length) {
		if ((ac == dataArrays.length) || (dataArrays[ac] == "*end*")) {
			dataEnd = 1;
		} else {
			ac++;
			data = dataArrays[ac];
			pc = 0;
		}
	}	
}

function resetPC()
{
	pc = 0;
	ac = 0;
	if (dataArrays.length) {
		data = dataArrays[0];
		dataEnd = 0;
	} else {	
		dataEnd = 1;
	}
}

function compileReports()
{
	resetPC();
	compileDocuments();
}

function compileDocuments()
{
	var s = data[pc];
	epochRange = data[pc];
	incPC();
	while (dataEnd == 0) {
		var s = data[pc];
		if (s == "*sorted-by-accesses*") {
			sortedByAccesses = 1;
			incPC();
		} else if (s == "*sorted-by-bytes*") {
			sortedByBytes = 1;
			incPC();
		} else if (s == "*sorted-by-visits*") {
			sortedByVisits = 1;
			incPC();
		} else if (s == "*alphabetized*") {
			alphabetized = 1;
			incPC();
		} else if (s == "*bytes*") {
			withBytes = 1;
			incPC();
		} else if (s == "*visits*") {
			withVisits = 1;
			incPC();
		} else if (s == "*downloads*") {
			withDownloads = 1;
			incPC();
		} else {
			break;
		}
	}
	documentsHead();
	while (dataEnd == 0) {
		if (compileSubreport() == 0) {
			// No more subreports. That's OK 
			break;
		}	
	}
	documentsTail();
	return 1;
}
function compileSubreport()
{
	if (data[pc] != "*subreport*") {
		return 0;
	}
	incPC();	
	subreportKeyword = "";
	subreportTitle = "";
	subreportAll = 0;
	subreportTop = 0;
	subreportTotal = 0;
	subreportPie = "";
	subreportKeyword = data[pc];
	incPC();
	subreportTitle = data[pc];
	incPC();
	if (data[pc] == "*all*") {
		incPC();
		subreportAll = 1;
	}
	if (data[pc] == "*top*") {
		incPC();
		subreportTop = data[pc];
		incPC();
	}
	if (data[pc] == "*total*") {
		incPC();
		subreportTotal = data[pc];
		incPC();
	}
	if (data[pc] == "*pie*") {
		incPC();
		subreportPie = data[pc];
		incPC();
	}
	subreportHead();
	while (dataEnd == 0) {
		if (compileSubreportRow() == 0) {
			// No more subreport rows. That's OK 
			break;
		}
	}
	subreportTail();
	return 1;
}
var rowRank = 0;
var rowItem = "";
var rowLocalLink = "";
var rowTitle = "";
var rowReferrers = "";
var rowAccesses = 0;
var rowAccessesPer = 0;
var rowBytes = 0;
var rowBytesPer = 0;
var rowVisits = 0;
var rowVisitsPer = 0;
var rowDownloadsPer = 0;
var rowComponents = 0;

function compileSubreportRow()
{
	if (data[pc] != "*row*") {
		return 0;
	}
	rowRank = 0;
	rowItem = "";
	rowLocalLink = "";
	rowTitle = "";
	rowReferrers = "";
	rowComponents = 0;
	incPC();
	rowRank = data[pc];
	incPC();
	rowItem = data[pc];
	incPC();
	if (data[pc] == "*local-link*") {
		incPC();
		rowLocalLink = data[pc];
		incPC();
	}
	if (data[pc] == "*title*") {
		incPC();
		rowTitle = data[pc];
		incPC();
	}
	if (data[pc] == "*referrers*") {
		incPC();
		rowReferrers = data[pc];
		incPC();
	}
	if (data[pc] == "*components*") {
		if (compileComponentList() == 0) {
			return 0;
		}
	}		
	rowAccesses = data[pc];	
	incPC();
	rowAccessesPer = data[pc];	
	incPC();
	if (withBytes) {
		rowBytes = data[pc];
		incPC();
		rowBytesPer = data[pc];
		incPC();
	}
	if (withVisits) {
		rowVisits = data[pc];
		incPC();
		rowVisitsPer = data[pc];
		incPC();
	}
	if (withDownloads) {
		rowDownloadsPer = data[pc];
		incPC();
	}
	subreportRow();	
	return 1;
}

function compileComponentList()
{
	if (data[pc] != "*components*") {
		return 0;
	}
	incPC();
	var alreadyFirst = componentsByPage[rowItem + ".1"];
	while (dataEnd == 0) {
		if (data[pc] == "*end*") {
			incPC();
			return 1;
		}
		rowComponents++;
		var index = data[pc];
		incPC();
		if (!alreadyFirst) {
			addComponent(rowItem, index);
		}
	}
	return -1;
}

function listComponents(w, item)
{
	var i = 1;
	while (1) {
		var s = componentsByPage[item + "." + i];
		if (!s) {
			return;
		}
		var offset = s * 5;
		var name = componentsData[offset++];
		var accesses = componentsData[offset++];
		var bytes = componentsData[offset++];
		var visits = componentsData[offset++];
		var downloadsper = componentsData[offset++];
		w.document.writeln("<td>");
		w.document.writeln(name);
		w.document.writeln("</td>");
		w.document.writeln("<td align=right>");
		w.document.writeln(accesses);
		w.document.writeln("</td>");
		if (withBytes) {
			w.document.writeln("<td align=right>");
			w.document.writeln(bytes);
			w.document.writeln("</td>");
		}	
		if (withVisits) {
			w.document.writeln("<td align=right>");
			w.document.writeln(visits);
			w.document.writeln("</td>");
		}	
		if (withDownloads) {
			w.document.writeln("<td align=right>");
			w.document.writeln(downloadsper);
			w.document.writeln("</td>");
		}	
		w.document.writeln("</tr>");
		i++;
	}
}

function addComponent(item, index)
{
	var i = 1;
	while (1) {
		var s = componentsByPage[item + "." + i];
		if (!s) {
			break;
		}
		i++;
	}
	componentsByPage[item + "." + i] = index;
}

function wl(s)
{
	document.writeln(s);
}
function documentsHead()
{
	wl("<h2>" + epochRange + "</h2>");
	

wl("<h3>Menu</h3>");

wl("<div class='colmenu'>");
wl("<div class='column'>");
wl("<ul id='infolankar'>");
wl("<li><a href=\"../../../../index.php\">Calendar of Reports</a></li>");
wl("<li><a href=\"index.php\">Executive Summary and Totals</a></li>");
wl("<li><a href=\"hours.php\">Accesses by Hour</a></li>");
wl("<li><a href=\"days.php\">Accesses by Day</a></li>");
wl("<li><a href=\"documents.php\">Top Documents</a></li>");
wl("<li><a href=\"directories.php\">Documents by Directory</a></li>");
wl("<li><a href=\"types.php\">File Types (Extensions)</a></li>");

wl("</ul>");
wl("</div>");
wl("<div class='column'>");
wl("<ul id='infolankar'>");
wl("<li><a href=\"sites.php\">Top Visitor Sites</a></li>");
wl("<li><a href=\"useragents.php\">Top Web Browsers</a></li>");
wl("<li><a href=\"os.php\">Top Operating Systems</a></li>");
wl("<li><a href=\"referringsites.php\">Top Referring Sites</a></li>");
wl("<li><a href=\"notfound.php\">Documents Not Found</a></li>");
wl("<li><a href=\"resultcodes.php\">Accesses by Result Code</a></li>");

wl("</ul>");
wl("</div>");
wl("<div class='colmenufoot'></div>");
wl("</div>");

}
function documentsTail()
{
}
function subreportHead()
{
	wl("<h3><a name=\"" + subreportKeyword + "\">");
	if (subreportAll) {
		wl("All " + subreportTitle);
	} else {
		wl("Top " + subreportTop + " of " + subreportTotal + " " + subreportTitle + ", ");
	}
        if (sortedByAccesses) {
                wl("Sorted by Access Count");
        } else if (sortedByBytes) {
                wl("Sorted by Bytes");
        } else if (sortedByVisits) {
                wl("Sorted by Visits");
        }
        if (alphabetized) {
                wl("and Alphabetized");
        }
	wl("</a></h3><p></p>"); 

	if (subreportPie) {
		wl("<div align=\"center\">");
		wl(subreportPie);
		wl("</div>");
	}
	wl("<table cellspacing=\"0\" class=\"styladtabell\">");
	wl("<thead>");
	wl("<tr>");
	wl("<th scope=\"col\">Rank</th>");
	wl("<th scope=\"col\">Item</th>");
	wl("<th scope=\"col\">Accesses</th>");
	wl("<th scope=\"col\">%</th>");
	if (withBytes) {
		wl("<th scope=\"col\">Bytes</th>");
		wl("<th scope=\"col\">%</th>");
	}
	if (withVisits) {
		wl("<th scope=\"col\">Visits</th>");
		wl("<th scope=\"col\">%</th>");
	}
	wl("</tr>");
	wl("</thead>");
	wl("<tbody>");
}	
function subreportTail()
{
	wl("</tbody>");
	wl("</table>");
}
function subreportRow()
{
        var gc = "jamn";
        var dirLink = 0;
        if (subreportGreenbarFlag) {
                gc = "udda";
        }
        subreportGreenbarFlag = !subreportGreenbarFlag;
	wl("<tr class=\"" + gc + "\">");
	wl("<th class=\"hogerstalld\">" + rowRank + "</th>");
	wl("<td align=\"right\">");
	if (rowLocalLink) {
		wl("<a href=\"" + rowLocalLink + "\">");
	}
	if (rowTitle) {
		wl("<b>" + rowTitle + "</b><br>");
	}
	wl(rowItem);
	if (rowLocalLink) {
		wl("</a>");
	}	
	if (rowReferrers) {
		wl("<a href=\"" + rowReferrers + "\" target=\"referrers\">[Referrers]</a>");
	}
	wl("</td>");
	wl("<td align=\"right\">" + rowAccesses + "</td>");
	wl("<td align=\"right\">" + rowAccessesPer + "</td>");
	if (withBytes) {
		wl("<td align=\"right\">" + rowBytes + "</td>");
		wl("<td align=\"right\">" + rowBytesPer + "</td>");
	}
	if (withVisits) {
		wl("<td align=\"right\">" + rowVisits + "</td>");
		wl("<td align=\"right\">" + rowVisitsPer + "</td>");
	}
	if (withDownloads) {
		wl("<td align=\"right\">" + rowDownloadsPer + "</td>");
	}
	wl("</tr>");
}

compileReports(0);	
</script>
