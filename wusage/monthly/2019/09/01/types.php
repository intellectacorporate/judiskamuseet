


<h2>Statistics for http://www.u5645470.fsdata.se, Month of 2019-9-1 to 2019-9-30</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 10 of 24 File Types (Extensions), 
Sorted by Access Count

</h3>
<p>Individual file types as determined by file extensions. All URLs that
do not contain an extension are counted as directories.</p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Type</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th class="hogerstalld">1</th>
	<td>
	js		
	</td>
	<td align="right">87,424</td>
	<td align="right">27.29</td>
	<td align="right">902,352,841</td>
	<td align="right">4.06</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	css		
	</td>
	<td align="right">64,621</td>
	<td align="right">20.17</td>
	<td align="right">871,085,551</td>
	<td align="right">3.92</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	Directory (folder)		
	</td>
	<td align="right">53,959</td>
	<td align="right">16.84</td>
	<td align="right">642,522,333</td>
	<td align="right">2.89</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	jpg		
	</td>
	<td align="right">53,765</td>
	<td align="right">16.78</td>
	<td align="right">16,439,367,349</td>
	<td align="right">73.93</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	png		
	</td>
	<td align="right">37,537</td>
	<td align="right">11.72</td>
	<td align="right">918,982,604</td>
	<td align="right">4.13</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	php		
	</td>
	<td align="right">10,249</td>
	<td align="right">3.20</td>
	<td align="right">200,270,133</td>
	<td align="right">0.90</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	ttf		
	</td>
	<td align="right">6,745</td>
	<td align="right">2.11</td>
	<td align="right">595,158,560</td>
	<td align="right">2.68</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	txt		
	</td>
	<td align="right">2,392</td>
	<td align="right">0.75</td>
	<td align="right">162,641</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	jpeg		
	</td>
	<td align="right">1,917</td>
	<td align="right">0.60</td>
	<td align="right">1,387,464,175</td>
	<td align="right">6.24</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">10</th>
	<td>
	ico		
	</td>
	<td align="right">499</td>
	<td align="right">0.16</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>

</tbody>
</table>

