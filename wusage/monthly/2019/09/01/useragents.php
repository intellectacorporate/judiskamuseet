


<h2>Statistics for http://www.u5645470.fsdata.se, Month of 2019-9-1 to 2019-9-30</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 10 of 430 Browsers (User Agents),
Sorted by Access Count

</h3>
<p></p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Product</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th class="hogerstalld">1</th>
	<td>
	Safari/537.36		
	</td>
	<td align="right">118,295</td>
	<td align="right">36.98</td>
	<td align="right">9,064,447,021</td>
	<td align="right">40.91</td>
	<td align="right">3,897</td>
	<td align="right">30.12</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	Safari/604.1		
	</td>
	<td align="right">65,863</td>
	<td align="right">20.59</td>
	<td align="right">5,296,193,267</td>
	<td align="right">23.91</td>
	<td align="right">2,158</td>
	<td align="right">16.68</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	Mozilla/5.0 		
	</td>
	<td align="right">36,652</td>
	<td align="right">11.46</td>
	<td align="right">2,110,469,810</td>
	<td align="right">9.53</td>
	<td align="right">1,679</td>
	<td align="right">12.98</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	Safari/605.1.15		
	</td>
	<td align="right">21,216</td>
	<td align="right">6.63</td>
	<td align="right">1,240,729,020</td>
	<td align="right">5.60</td>
	<td align="right">462</td>
	<td align="right">3.57</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	bingbot/2.0		
	</td>
	<td align="right">7,595</td>
	<td align="right">2.37</td>
	<td align="right">628,293,546</td>
	<td align="right">2.84</td>
	<td align="right">1,729</td>
	<td align="right">13.36</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	Safari/605.1		
	</td>
	<td align="right">6,986</td>
	<td align="right">2.18</td>
	<td align="right">653,478,293</td>
	<td align="right">2.95</td>
	<td align="right">217</td>
	<td align="right">1.68</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	Safari/537.36 Edge/17.17134		
	</td>
	<td align="right">6,204</td>
	<td align="right">1.94</td>
	<td align="right">344,884,150</td>
	<td align="right">1.56</td>
	<td align="right">154</td>
	<td align="right">1.20</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	Safari/537.36 Edge/18.17763		
	</td>
	<td align="right">6,140</td>
	<td align="right">1.92</td>
	<td align="right">359,457,559</td>
	<td align="right">1.62</td>
	<td align="right">102</td>
	<td align="right">0.79</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	Safari/537.36 Edge/18.18362		
	</td>
	<td align="right">5,836</td>
	<td align="right">1.82</td>
	<td align="right">498,857,381</td>
	<td align="right">2.25</td>
	<td align="right">166</td>
	<td align="right">1.29</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">10</th>
	<td>
	UptimeRobot/2.0		
	</td>
	<td align="right">5,540</td>
	<td align="right">1.73</td>
	<td align="right">813,487</td>
	<td align="right">0.00</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>

</tbody>
</table>

