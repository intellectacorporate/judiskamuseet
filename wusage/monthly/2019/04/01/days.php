


<h2>Statistics for http://www.u5645470.fsdata.se, Month of 2019-4-1 to 2019-4-30</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Traffic by Day</h3>
<p>The continuous line represents a 7-day moving average.</p>
<img src="d.png" width="468" height="351">
<br /><br />


<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Date</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	<th scope='col'>Bits per Sec</th>
	<th scope='col'>Bytes per Sec</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th align="right">2019-4-1</th>
	<td align="right">6,146	</td>
	<td align="right">2.96	</td>
	<td align="right">1,948,662,205	</td>
	<td align="right">3.22	</td>
	<td align="right">349	</td>
	<td align="right">3.31	</td>
	<td align="right">180,431.69	</td>
	<td align="right">22,553.96	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-4-2</th>
	<td align="right">4,853	</td>
	<td align="right">2.34	</td>
	<td align="right">1,423,231,307	</td>
	<td align="right">2.35	</td>
	<td align="right">287	</td>
	<td align="right">2.73	</td>
	<td align="right">131,780.68	</td>
	<td align="right">16,472.58	</td>
</tr>
<tr class="udda">
	<th align="right">2019-4-3</th>
	<td align="right">6,696	</td>
	<td align="right">3.22	</td>
	<td align="right">2,313,572,300	</td>
	<td align="right">3.83	</td>
	<td align="right">336	</td>
	<td align="right">3.19	</td>
	<td align="right">214,219.66	</td>
	<td align="right">26,777.46	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-4-4</th>
	<td align="right">8,013	</td>
	<td align="right">3.86	</td>
	<td align="right">2,330,021,748	</td>
	<td align="right">3.86	</td>
	<td align="right">453	</td>
	<td align="right">4.30	</td>
	<td align="right">215,742.75	</td>
	<td align="right">26,967.84	</td>
</tr>
<tr class="udda">
	<th align="right">2019-4-5</th>
	<td align="right">6,241	</td>
	<td align="right">3.00	</td>
	<td align="right">1,925,255,716	</td>
	<td align="right">3.19	</td>
	<td align="right">350	</td>
	<td align="right">3.32	</td>
	<td align="right">178,264.42	</td>
	<td align="right">22,283.05	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-4-6</th>
	<td align="right">4,174	</td>
	<td align="right">2.01	</td>
	<td align="right">1,416,991,939	</td>
	<td align="right">2.34	</td>
	<td align="right">223	</td>
	<td align="right">2.12	</td>
	<td align="right">131,202.96	</td>
	<td align="right">16,400.37	</td>
</tr>
<tr class="udda">
	<th align="right">2019-4-7</th>
	<td align="right">4,839	</td>
	<td align="right">2.33	</td>
	<td align="right">1,350,404,285	</td>
	<td align="right">2.23	</td>
	<td align="right">239	</td>
	<td align="right">2.27	</td>
	<td align="right">125,037.43	</td>
	<td align="right">15,629.68	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-4-8</th>
	<td align="right">9,646	</td>
	<td align="right">4.64	</td>
	<td align="right">2,097,305,018	</td>
	<td align="right">3.47	</td>
	<td align="right">467	</td>
	<td align="right">4.43	</td>
	<td align="right">194,194.91	</td>
	<td align="right">24,274.36	</td>
</tr>
<tr class="udda">
	<th align="right">2019-4-9</th>
	<td align="right">6,731	</td>
	<td align="right">3.24	</td>
	<td align="right">1,856,826,920	</td>
	<td align="right">3.07	</td>
	<td align="right">349	</td>
	<td align="right">3.31	</td>
	<td align="right">171,928.42	</td>
	<td align="right">21,491.05	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-4-10</th>
	<td align="right">5,545	</td>
	<td align="right">2.67	</td>
	<td align="right">1,592,765,421	</td>
	<td align="right">2.64	</td>
	<td align="right">331	</td>
	<td align="right">3.14	</td>
	<td align="right">147,478.28	</td>
	<td align="right">18,434.78	</td>
</tr>
<tr class="udda">
	<th align="right">2019-4-11</th>
	<td align="right">10,894	</td>
	<td align="right">5.25	</td>
	<td align="right">2,630,486,714	</td>
	<td align="right">4.35	</td>
	<td align="right">425	</td>
	<td align="right">4.04	</td>
	<td align="right">243,563.58	</td>
	<td align="right">30,445.45	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-4-12</th>
	<td align="right">31,779	</td>
	<td align="right">15.30	</td>
	<td align="right">6,384,033,496	</td>
	<td align="right">10.56	</td>
	<td align="right">1,212	</td>
	<td align="right">11.51	</td>
	<td align="right">591,114.21	</td>
	<td align="right">73,889.28	</td>
</tr>
<tr class="udda">
	<th align="right">2019-4-13</th>
	<td align="right">6,342	</td>
	<td align="right">3.05	</td>
	<td align="right">1,867,687,790	</td>
	<td align="right">3.09	</td>
	<td align="right">328	</td>
	<td align="right">3.11	</td>
	<td align="right">172,934.05	</td>
	<td align="right">21,616.76	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-4-14</th>
	<td align="right">4,666	</td>
	<td align="right">2.25	</td>
	<td align="right">1,375,046,253	</td>
	<td align="right">2.28	</td>
	<td align="right">280	</td>
	<td align="right">2.66	</td>
	<td align="right">127,319.10	</td>
	<td align="right">15,914.89	</td>
</tr>
<tr class="udda">
	<th align="right">2019-4-15</th>
	<td align="right">5,093	</td>
	<td align="right">2.45	</td>
	<td align="right">1,578,243,546	</td>
	<td align="right">2.61	</td>
	<td align="right">291	</td>
	<td align="right">2.76	</td>
	<td align="right">146,133.66	</td>
	<td align="right">18,266.71	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-4-16</th>
	<td align="right">5,285	</td>
	<td align="right">2.54	</td>
	<td align="right">2,352,069,539	</td>
	<td align="right">3.89	</td>
	<td align="right">303	</td>
	<td align="right">2.88	</td>
	<td align="right">217,784.22	</td>
	<td align="right">27,223.03	</td>
</tr>
<tr class="udda">
	<th align="right">2019-4-17</th>
	<td align="right">4,331	</td>
	<td align="right">2.09	</td>
	<td align="right">1,486,811,834	</td>
	<td align="right">2.46	</td>
	<td align="right">260	</td>
	<td align="right">2.47	</td>
	<td align="right">137,667.76	</td>
	<td align="right">17,208.47	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-4-18</th>
	<td align="right">3,494	</td>
	<td align="right">1.68	</td>
	<td align="right">1,243,661,148	</td>
	<td align="right">2.06	</td>
	<td align="right">239	</td>
	<td align="right">2.27	</td>
	<td align="right">115,153.81	</td>
	<td align="right">14,394.23	</td>
</tr>
<tr class="udda">
	<th align="right">2019-4-19</th>
	<td align="right">4,141	</td>
	<td align="right">1.99	</td>
	<td align="right">1,349,783,817	</td>
	<td align="right">2.23	</td>
	<td align="right">245	</td>
	<td align="right">2.33	</td>
	<td align="right">124,979.98	</td>
	<td align="right">15,622.50	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-4-20</th>
	<td align="right">3,148	</td>
	<td align="right">1.52	</td>
	<td align="right">752,205,251	</td>
	<td align="right">1.24	</td>
	<td align="right">188	</td>
	<td align="right">1.79	</td>
	<td align="right">69,648.63	</td>
	<td align="right">8,706.08	</td>
</tr>
<tr class="udda">
	<th align="right">2019-4-21</th>
	<td align="right">4,380	</td>
	<td align="right">2.11	</td>
	<td align="right">1,361,793,513	</td>
	<td align="right">2.25	</td>
	<td align="right">224	</td>
	<td align="right">2.13	</td>
	<td align="right">126,091.99	</td>
	<td align="right">15,761.50	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-4-22</th>
	<td align="right">4,537	</td>
	<td align="right">2.18	</td>
	<td align="right">1,193,286,122	</td>
	<td align="right">1.97	</td>
	<td align="right">254	</td>
	<td align="right">2.41	</td>
	<td align="right">110,489.46	</td>
	<td align="right">13,811.18	</td>
</tr>
<tr class="udda">
	<th align="right">2019-4-23</th>
	<td align="right">9,726	</td>
	<td align="right">4.68	</td>
	<td align="right">2,957,586,647	</td>
	<td align="right">4.89	</td>
	<td align="right">451	</td>
	<td align="right">4.28	</td>
	<td align="right">273,850.62	</td>
	<td align="right">34,231.33	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-4-24</th>
	<td align="right">10,631	</td>
	<td align="right">5.12	</td>
	<td align="right">2,897,450,453	</td>
	<td align="right">4.79	</td>
	<td align="right">473	</td>
	<td align="right">4.49	</td>
	<td align="right">268,282.45	</td>
	<td align="right">33,535.31	</td>
</tr>
<tr class="udda">
	<th align="right">2019-4-25</th>
	<td align="right">8,533	</td>
	<td align="right">4.11	</td>
	<td align="right">3,105,376,595	</td>
	<td align="right">5.14	</td>
	<td align="right">398	</td>
	<td align="right">3.78	</td>
	<td align="right">287,534.87	</td>
	<td align="right">35,941.86	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-4-26</th>
	<td align="right">5,577	</td>
	<td align="right">2.69	</td>
	<td align="right">1,820,254,349	</td>
	<td align="right">3.01	</td>
	<td align="right">297	</td>
	<td align="right">2.82	</td>
	<td align="right">168,542.07	</td>
	<td align="right">21,067.76	</td>
</tr>
<tr class="udda">
	<th align="right">2019-4-27</th>
	<td align="right">6,154	</td>
	<td align="right">2.96	</td>
	<td align="right">2,212,975,087	</td>
	<td align="right">3.66	</td>
	<td align="right">325	</td>
	<td align="right">3.09	</td>
	<td align="right">204,905.10	</td>
	<td align="right">25,613.14	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-4-28</th>
	<td align="right">5,094	</td>
	<td align="right">2.45	</td>
	<td align="right">1,511,150,115	</td>
	<td align="right">2.50	</td>
	<td align="right">335	</td>
	<td align="right">3.18	</td>
	<td align="right">139,921.31	</td>
	<td align="right">17,490.16	</td>
</tr>
<tr class="udda">
	<th align="right">2019-4-29</th>
	<td align="right">6,043	</td>
	<td align="right">2.91	</td>
	<td align="right">2,309,374,716	</td>
	<td align="right">3.82	</td>
	<td align="right">334	</td>
	<td align="right">3.17	</td>
	<td align="right">213,830.99	</td>
	<td align="right">26,728.87	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-4-30</th>
	<td align="right">4,968	</td>
	<td align="right">2.39	</td>
	<td align="right">1,795,599,240	</td>
	<td align="right">2.97	</td>
	<td align="right">285	</td>
	<td align="right">2.71	</td>
	<td align="right">166,259.19	</td>
	<td align="right">20,782.40	</td>
</tr>

</tbody>
</table>

