


<h2>Statistics for http://www.u5645470.fsdata.se, Month of 2019-4-1 to 2019-4-30</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Traffic by Hour</h3>
<p>Figures are averages for that hour
	of each day in the time period analyzed.</p>
<img src="h.png" width="468" height="351">
<br /><br />
<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Hour</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	<th scope='col'>Bits per Sec</th>
	<th scope='col'>Bytes per Sec</th>
	
	</tr>
</thead>
<tbody>
<tr class="jamnudda">
	<th align="right">00:00</th>
	<td align="right">138.20</td>
	<td align="right">2.00</td>
	<td align="right">30,994,544.47</td>
	<td align="right">1.54</td>
	<td align="right">4.93</td>
	<td align="right">1.40</td>
	<td align="right">68,876.77</td>
	<td align="right">8,609.60</td>
	
</tr>
<tr class="jamn">
	<th align="right">01:00</th>
	<td align="right">105.87</td>
	<td align="right">1.53</td>
	<td align="right">22,377,750.73</td>
	<td align="right">1.11</td>
	<td align="right">6.13</td>
	<td align="right">1.75</td>
	<td align="right">49,728.33</td>
	<td align="right">6,216.04</td>
	
</tr>
<tr class="udda">
	<th align="right">02:00</th>
	<td align="right">90.03</td>
	<td align="right">1.30</td>
	<td align="right">16,348,042.10</td>
	<td align="right">0.81</td>
	<td align="right">6.04</td>
	<td align="right">1.72</td>
	<td align="right">36,328.98</td>
	<td align="right">4,541.12</td>
	
</tr>
<tr class="jamn">
	<th align="right">03:00</th>
	<td align="right">78.23</td>
	<td align="right">1.13</td>
	<td align="right">9,190,774.20</td>
	<td align="right">0.46</td>
	<td align="right">6.57</td>
	<td align="right">1.87</td>
	<td align="right">20,423.94</td>
	<td align="right">2,552.99</td>
	
</tr>
<tr class="udda">
	<th align="right">04:00</th>
	<td align="right">82.17</td>
	<td align="right">1.19</td>
	<td align="right">14,503,397.37</td>
	<td align="right">0.72</td>
	<td align="right">5.86</td>
	<td align="right">1.67</td>
	<td align="right">32,229.77</td>
	<td align="right">4,028.72</td>
	
</tr>
<tr class="jamn">
	<th align="right">05:00</th>
	<td align="right">99.60</td>
	<td align="right">1.44</td>
	<td align="right">12,830,724.87</td>
	<td align="right">0.64</td>
	<td align="right">6.33</td>
	<td align="right">1.80</td>
	<td align="right">28,512.72</td>
	<td align="right">3,564.09</td>
	
</tr>
<tr class="udda">
	<th align="right">06:00</th>
	<td align="right">253.20</td>
	<td align="right">3.66</td>
	<td align="right">35,375,105.53</td>
	<td align="right">1.76</td>
	<td align="right">9.12</td>
	<td align="right">2.60</td>
	<td align="right">78,611.35</td>
	<td align="right">9,826.42</td>
	
</tr>
<tr class="jamn">
	<th align="right">07:00</th>
	<td align="right">367.40</td>
	<td align="right">5.31</td>
	<td align="right">59,547,742.37</td>
	<td align="right">2.96</td>
	<td align="right">18.70</td>
	<td align="right">5.33</td>
	<td align="right">132,328.32</td>
	<td align="right">16,541.04</td>
	
</tr>
<tr class="udda">
	<th align="right">08:00</th>
	<td align="right">433.80</td>
	<td align="right">6.27</td>
	<td align="right">105,813,267.00</td>
	<td align="right">5.25</td>
	<td align="right">17.89</td>
	<td align="right">5.10</td>
	<td align="right">235,140.59</td>
	<td align="right">29,392.57</td>
	
</tr>
<tr class="jamn">
	<th align="right">09:00</th>
	<td align="right">462.70</td>
	<td align="right">6.68</td>
	<td align="right">137,963,302.03</td>
	<td align="right">6.85</td>
	<td align="right">23.92</td>
	<td align="right">6.81</td>
	<td align="right">306,585.12</td>
	<td align="right">38,323.14</td>
	
</tr>
<tr class="udda">
	<th align="right">10:00</th>
	<td align="right">543.57</td>
	<td align="right">7.85</td>
	<td align="right">168,716,187.67</td>
	<td align="right">8.37</td>
	<td align="right">24.44</td>
	<td align="right">6.96</td>
	<td align="right">374,924.86</td>
	<td align="right">46,865.61</td>
	
</tr>
<tr class="jamn">
	<th align="right">11:00</th>
	<td align="right">393.67</td>
	<td align="right">5.69</td>
	<td align="right">133,680,300.23</td>
	<td align="right">6.64</td>
	<td align="right">23.11</td>
	<td align="right">6.58</td>
	<td align="right">297,067.33</td>
	<td align="right">37,133.42</td>
	
</tr>
<tr class="udda">
	<th align="right">12:00</th>
	<td align="right">384.60</td>
	<td align="right">5.56</td>
	<td align="right">135,496,799.67</td>
	<td align="right">6.73</td>
	<td align="right">16.46</td>
	<td align="right">4.69</td>
	<td align="right">301,104.00</td>
	<td align="right">37,638.00</td>
	
</tr>
<tr class="jamn">
	<th align="right">13:00</th>
	<td align="right">418.40</td>
	<td align="right">6.04</td>
	<td align="right">138,333,054.17</td>
	<td align="right">6.87</td>
	<td align="right">21.22</td>
	<td align="right">6.05</td>
	<td align="right">307,406.79</td>
	<td align="right">38,425.85</td>
	
</tr>
<tr class="udda">
	<th align="right">14:00</th>
	<td align="right">424.67</td>
	<td align="right">6.13</td>
	<td align="right">132,028,078.73</td>
	<td align="right">6.55</td>
	<td align="right">20.10</td>
	<td align="right">5.72</td>
	<td align="right">293,395.73</td>
	<td align="right">36,674.47</td>
	
</tr>
<tr class="jamn">
	<th align="right">15:00</th>
	<td align="right">324.53</td>
	<td align="right">4.69</td>
	<td align="right">93,675,934.50</td>
	<td align="right">4.65</td>
	<td align="right">16.50</td>
	<td align="right">4.70</td>
	<td align="right">208,168.74</td>
	<td align="right">26,021.09</td>
	
</tr>
<tr class="udda">
	<th align="right">16:00</th>
	<td align="right">303.67</td>
	<td align="right">4.39</td>
	<td align="right">113,142,561.43</td>
	<td align="right">5.62</td>
	<td align="right">16.58</td>
	<td align="right">4.72</td>
	<td align="right">251,427.91</td>
	<td align="right">31,428.49</td>
	
</tr>
<tr class="jamn">
	<th align="right">17:00</th>
	<td align="right">288.20</td>
	<td align="right">4.16</td>
	<td align="right">93,527,196.57</td>
	<td align="right">4.64</td>
	<td align="right">14.64</td>
	<td align="right">4.17</td>
	<td align="right">207,838.21</td>
	<td align="right">25,979.78</td>
	
</tr>
<tr class="udda">
	<th align="right">18:00</th>
	<td align="right">333.60</td>
	<td align="right">4.82</td>
	<td align="right">101,790,872.23</td>
	<td align="right">5.05</td>
	<td align="right">15.41</td>
	<td align="right">4.39</td>
	<td align="right">226,201.94</td>
	<td align="right">28,275.24</td>
	
</tr>
<tr class="jamn">
	<th align="right">19:00</th>
	<td align="right">290.90</td>
	<td align="right">4.20</td>
	<td align="right">95,085,783.00</td>
	<td align="right">4.72</td>
	<td align="right">14.68</td>
	<td align="right">4.18</td>
	<td align="right">211,301.74</td>
	<td align="right">26,412.72</td>
	
</tr>
<tr class="udda">
	<th align="right">20:00</th>
	<td align="right">286.97</td>
	<td align="right">4.14</td>
	<td align="right">82,988,088.40</td>
	<td align="right">4.12</td>
	<td align="right">14.29</td>
	<td align="right">4.07</td>
	<td align="right">184,417.97</td>
	<td align="right">23,052.25</td>
	
</tr>
<tr class="jamn">
	<th align="right">21:00</th>
	<td align="right">361.60</td>
	<td align="right">5.22</td>
	<td align="right">122,183,658.83</td>
	<td align="right">6.06</td>
	<td align="right">17.19</td>
	<td align="right">4.90</td>
	<td align="right">271,519.24</td>
	<td align="right">33,939.91</td>
	
</tr>
<tr class="udda">
	<th align="right">22:00</th>
	<td align="right">261.00</td>
	<td align="right">3.77</td>
	<td align="right">93,537,788.30</td>
	<td align="right">4.64</td>
	<td align="right">15.11</td>
	<td align="right">4.30</td>
	<td align="right">207,861.75</td>
	<td align="right">25,982.72</td>
	
</tr>
<tr class="jamn">
	<th align="right">23:00</th>
	<td align="right">196.77</td>
	<td align="right">2.84</td>
	<td align="right">65,532,815.07</td>
	<td align="right">3.25</td>
	<td align="right">15.80</td>
	<td align="right">4.50</td>
	<td align="right">145,628.48</td>
	<td align="right">18,203.56</td>
	
</tr>

</tbody>
</table>

