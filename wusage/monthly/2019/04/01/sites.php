


<h2>Statistics for http://www.u5645470.fsdata.se, Month of 2019-4-1 to 2019-4-30</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 20 of 8,407 Visitor Sites,
Sorted by Access Count

</h3>
<p></p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Site</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th class="hogerstalld">1</th>
	<td>
	63.143.42.244		
	</td>
	<td align="right">7,728</td>
	<td align="right">3.72</td>
	<td align="right">114,494</td>
	<td align="right">0.00</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	89.221.250.27		
	</td>
	<td align="right">7,251</td>
	<td align="right">3.49</td>
	<td align="right">142,843</td>
	<td align="right">0.00</td>
	<td align="right">115</td>
	<td align="right">1.09</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	46.59.25.88		
	</td>
	<td align="right">5,196</td>
	<td align="right">2.50</td>
	<td align="right">1,944,273,393</td>
	<td align="right">3.22</td>
	<td align="right">77</td>
	<td align="right">0.73</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	185.15.93.69		
	</td>
	<td align="right">2,616</td>
	<td align="right">1.26</td>
	<td align="right">44,692,833</td>
	<td align="right">0.07</td>
	<td align="right">184</td>
	<td align="right">1.76</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	167.114.124.133		
	</td>
	<td align="right">1,483</td>
	<td align="right">0.71</td>
	<td align="right">487,596,136</td>
	<td align="right">0.81</td>
	<td align="right">6</td>
	<td align="right">0.06</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	90.229.237.5		
	</td>
	<td align="right">1,388</td>
	<td align="right">0.67</td>
	<td align="right">71,469,534</td>
	<td align="right">0.12</td>
	<td align="right">11</td>
	<td align="right">0.11</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	5.35.187.69		
	</td>
	<td align="right">1,386</td>
	<td align="right">0.67</td>
	<td align="right">383,193,725</td>
	<td align="right">0.63</td>
	<td align="right">9</td>
	<td align="right">0.09</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	unknown		
	</td>
	<td align="right">674</td>
	<td align="right">0.32</td>
	<td align="right">3,331,857</td>
	<td align="right">0.01</td>
	<td align="right">207</td>
	<td align="right">1.97</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	79.142.244.252		
	</td>
	<td align="right">640</td>
	<td align="right">0.31</td>
	<td align="right">420,960,638</td>
	<td align="right">0.70</td>
	<td align="right">20</td>
	<td align="right">0.19</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">10</th>
	<td>
	216.244.66.227		
	</td>
	<td align="right">634</td>
	<td align="right">0.31</td>
	<td align="right">393,406,050</td>
	<td align="right">0.65</td>
	<td align="right">108</td>
	<td align="right">1.03</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">11</th>
	<td>
	109.102.111.67		
	</td>
	<td align="right">552</td>
	<td align="right">0.27</td>
	<td align="right">9,330,910</td>
	<td align="right">0.02</td>
	<td align="right">3</td>
	<td align="right">0.03</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">12</th>
	<td>
	80.68.102.230		
	</td>
	<td align="right">463</td>
	<td align="right">0.22</td>
	<td align="right">172,089,559</td>
	<td align="right">0.28</td>
	<td align="right">24</td>
	<td align="right">0.23</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">13</th>
	<td>
	17.58.99.252		
	</td>
	<td align="right">433</td>
	<td align="right">0.21</td>
	<td align="right">3,370,289</td>
	<td align="right">0.01</td>
	<td align="right">102</td>
	<td align="right">0.97</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">14</th>
	<td>
	194.0.67.51		
	</td>
	<td align="right">424</td>
	<td align="right">0.20</td>
	<td align="right">102,612,960</td>
	<td align="right">0.17</td>
	<td align="right">15</td>
	<td align="right">0.14</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">15</th>
	<td>
	212.112.56.83		
	</td>
	<td align="right">399</td>
	<td align="right">0.19</td>
	<td align="right">14,729,078</td>
	<td align="right">0.02</td>
	<td align="right">1</td>
	<td align="right">0.01</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">16</th>
	<td>
	85.202.82.116		
	</td>
	<td align="right">388</td>
	<td align="right">0.19</td>
	<td align="right">8,940,503</td>
	<td align="right">0.01</td>
	<td align="right">5</td>
	<td align="right">0.05</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">17</th>
	<td>
	23.100.232.233		
	</td>
	<td align="right">384</td>
	<td align="right">0.18</td>
	<td align="right">5,936,996</td>
	<td align="right">0.01</td>
	<td align="right">26</td>
	<td align="right">0.25</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">18</th>
	<td>
	193.188.156.131		
	</td>
	<td align="right">382</td>
	<td align="right">0.18</td>
	<td align="right">317,088,900</td>
	<td align="right">0.52</td>
	<td align="right">11</td>
	<td align="right">0.11</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">19</th>
	<td>
	62.20.62.208		
	</td>
	<td align="right">379</td>
	<td align="right">0.18</td>
	<td align="right">81,130,578</td>
	<td align="right">0.13</td>
	<td align="right">20</td>
	<td align="right">0.19</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">20</th>
	<td>
	66.249.79.186		
	</td>
	<td align="right">324</td>
	<td align="right">0.16</td>
	<td align="right">4,009,591</td>
	<td align="right">0.01</td>
	<td align="right">46</td>
	<td align="right">0.44</td>
</tr>

</tbody>
</table>

