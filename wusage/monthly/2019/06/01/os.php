


<h2>Statistics for http://www.u5645470.fsdata.se, Month of 2019-6-1 to 2019-6-30</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 10 of 13 Operating Systems,
Sorted by Access Count

</h3>
<p></p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Product</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th class="hogerstalld">1</th>
	<td>
	Mac OS X		
	</td>
	<td align="right">474,761</td>
	<td align="right">85.76</td>
	<td align="right">95,753,715,619</td>
	<td align="right">88.97</td>
	<td align="right">13,851</td>
	<td align="right">74.04</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	Other		
	</td>
	<td align="right">35,668</td>
	<td align="right">6.44</td>
	<td align="right">3,308,479,262</td>
	<td align="right">3.07</td>
	<td align="right">3,355</td>
	<td align="right">17.93</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	Windows NT		
	</td>
	<td align="right">34,818</td>
	<td align="right">6.29</td>
	<td align="right">8,192,002,487</td>
	<td align="right">7.61</td>
	<td align="right">854</td>
	<td align="right">4.57</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	Linux		
	</td>
	<td align="right">6,243</td>
	<td align="right">1.13</td>
	<td align="right">227,652,122</td>
	<td align="right">0.21</td>
	<td align="right">599</td>
	<td align="right">3.20</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	Windows Codename Longhorn		
	</td>
	<td align="right">1,312</td>
	<td align="right">0.24</td>
	<td align="right">80,534,801</td>
	<td align="right">0.07</td>
	<td align="right">27</td>
	<td align="right">0.15</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	Windows XP		
	</td>
	<td align="right">619</td>
	<td align="right">0.11</td>
	<td align="right">67,761,933</td>
	<td align="right">0.06</td>
	<td align="right">20</td>
	<td align="right">0.11</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	Windows 2003		
	</td>
	<td align="right">99</td>
	<td align="right">0.02</td>
	<td align="right">69,225</td>
	<td align="right">0.00</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	Windows 2000		
	</td>
	<td align="right">45</td>
	<td align="right">0.01</td>
	<td align="right">77,533</td>
	<td align="right">0.00</td>
	<td align="right">1</td>
	<td align="right">0.01</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	Windows 95		
	</td>
	<td align="right">30</td>
	<td align="right">0.01</td>
	<td align="right">8,814</td>
	<td align="right">0.00</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">10</th>
	<td>
	Windows Me		
	</td>
	<td align="right">13</td>
	<td align="right">0.00</td>
	<td align="right">15,566</td>
	<td align="right">0.00</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>

</tbody>
</table>

