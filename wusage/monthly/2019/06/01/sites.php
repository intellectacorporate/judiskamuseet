


<h2>Statistics for http://www.u5645470.fsdata.se, Month of 2019-6-1 to 2019-6-30</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 20 of 14,455 Visitor Sites,
Sorted by Access Count

</h3>
<p></p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Site</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th class="hogerstalld">1</th>
	<td>
	46.59.25.88		
	</td>
	<td align="right">17,046</td>
	<td align="right">3.07</td>
	<td align="right">1,772,983,574</td>
	<td align="right">1.65</td>
	<td align="right">203</td>
	<td align="right">1.09</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	5.35.187.69		
	</td>
	<td align="right">10,392</td>
	<td align="right">1.87</td>
	<td align="right">729,565,568</td>
	<td align="right">0.68</td>
	<td align="right">32</td>
	<td align="right">0.17</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	81.170.246.9		
	</td>
	<td align="right">7,402</td>
	<td align="right">1.33</td>
	<td align="right">272,130,456</td>
	<td align="right">0.25</td>
	<td align="right">14</td>
	<td align="right">0.08</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	63.143.42.243		
	</td>
	<td align="right">5,570</td>
	<td align="right">1.00</td>
	<td align="right">543,546</td>
	<td align="right">0.00</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	66.249.64.18		
	</td>
	<td align="right">2,451</td>
	<td align="right">0.44</td>
	<td align="right">110,317,079</td>
	<td align="right">0.10</td>
	<td align="right">350</td>
	<td align="right">1.88</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	66.249.64.19		
	</td>
	<td align="right">1,590</td>
	<td align="right">0.29</td>
	<td align="right">76,031,859</td>
	<td align="right">0.07</td>
	<td align="right">311</td>
	<td align="right">1.67</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	31.28.163.157		
	</td>
	<td align="right">1,585</td>
	<td align="right">0.29</td>
	<td align="right">418,307,874</td>
	<td align="right">0.39</td>
	<td align="right">1</td>
	<td align="right">0.01</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	213.212.29.165		
	</td>
	<td align="right">1,550</td>
	<td align="right">0.28</td>
	<td align="right">72,769,447</td>
	<td align="right">0.07</td>
	<td align="right">11</td>
	<td align="right">0.06</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	193.188.156.131		
	</td>
	<td align="right">1,511</td>
	<td align="right">0.27</td>
	<td align="right">418,525,812</td>
	<td align="right">0.39</td>
	<td align="right">34</td>
	<td align="right">0.19</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">10</th>
	<td>
	109.104.25.125		
	</td>
	<td align="right">1,267</td>
	<td align="right">0.23</td>
	<td align="right">125,803,192</td>
	<td align="right">0.12</td>
	<td align="right">27</td>
	<td align="right">0.15</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">11</th>
	<td>
	52.5.190.19		
	</td>
	<td align="right">1,183</td>
	<td align="right">0.21</td>
	<td align="right">169,532,429</td>
	<td align="right">0.16</td>
	<td align="right">2</td>
	<td align="right">0.01</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">12</th>
	<td>
	216.244.66.227		
	</td>
	<td align="right">1,132</td>
	<td align="right">0.20</td>
	<td align="right">10,098,943</td>
	<td align="right">0.01</td>
	<td align="right">209</td>
	<td align="right">1.12</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">13</th>
	<td>
	66.249.64.20		
	</td>
	<td align="right">1,071</td>
	<td align="right">0.19</td>
	<td align="right">39,870,231</td>
	<td align="right">0.04</td>
	<td align="right">215</td>
	<td align="right">1.15</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">14</th>
	<td>
	188.151.134.188		
	</td>
	<td align="right">995</td>
	<td align="right">0.18</td>
	<td align="right">161,747,674</td>
	<td align="right">0.15</td>
	<td align="right">26</td>
	<td align="right">0.14</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">15</th>
	<td>
	94.137.110.158		
	</td>
	<td align="right">915</td>
	<td align="right">0.17</td>
	<td align="right">187,552,163</td>
	<td align="right">0.17</td>
	<td align="right">21</td>
	<td align="right">0.11</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">16</th>
	<td>
	89.221.250.27		
	</td>
	<td align="right">843</td>
	<td align="right">0.15</td>
	<td align="right">60,707</td>
	<td align="right">0.00</td>
	<td align="right">18</td>
	<td align="right">0.10</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">17</th>
	<td>
	95.199.4.93		
	</td>
	<td align="right">715</td>
	<td align="right">0.13</td>
	<td align="right">22,931,562</td>
	<td align="right">0.02</td>
	<td align="right">2</td>
	<td align="right">0.01</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">18</th>
	<td>
	185.224.57.161		
	</td>
	<td align="right">713</td>
	<td align="right">0.13</td>
	<td align="right">35,105,746</td>
	<td align="right">0.03</td>
	<td align="right">23</td>
	<td align="right">0.12</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">19</th>
	<td>
	158.174.225.83		
	</td>
	<td align="right">672</td>
	<td align="right">0.12</td>
	<td align="right">44,183,703</td>
	<td align="right">0.04</td>
	<td align="right">4</td>
	<td align="right">0.02</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">20</th>
	<td>
	134.25.0.133		
	</td>
	<td align="right">670</td>
	<td align="right">0.12</td>
	<td align="right">315,514,362</td>
	<td align="right">0.29</td>
	<td align="right">14</td>
	<td align="right">0.08</td>
</tr>

</tbody>
</table>

