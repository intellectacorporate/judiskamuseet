


<h2>Statistics for http://www.u5645470.fsdata.se, Month of 2019-6-1 to 2019-6-30</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 10 of 487 Browsers (User Agents),
Sorted by Access Count

</h3>
<p></p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Product</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th class="hogerstalld">1</th>
	<td>
	Safari/537.36		
	</td>
	<td align="right">158,808</td>
	<td align="right">28.69</td>
	<td align="right">37,454,266,919</td>
	<td align="right">34.80</td>
	<td align="right">4,983</td>
	<td align="right">26.70</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	Safari/604.1		
	</td>
	<td align="right">158,355</td>
	<td align="right">28.60</td>
	<td align="right">31,907,425,350</td>
	<td align="right">29.65</td>
	<td align="right">4,310</td>
	<td align="right">23.09</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	Mozilla/5.0 		
	</td>
	<td align="right">84,194</td>
	<td align="right">15.21</td>
	<td align="right">14,324,012,149</td>
	<td align="right">13.31</td>
	<td align="right">2,416</td>
	<td align="right">12.95</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	Safari/605.1.15		
	</td>
	<td align="right">27,234</td>
	<td align="right">4.92</td>
	<td align="right">7,499,233,475</td>
	<td align="right">6.97</td>
	<td align="right">653</td>
	<td align="right">3.50</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	Safari/537.36 Edge/18.17763		
	</td>
	<td align="right">23,024</td>
	<td align="right">4.16</td>
	<td align="right">2,029,256,991</td>
	<td align="right">1.89</td>
	<td align="right">161</td>
	<td align="right">0.87</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	Safari/537.36 Edge/17.17134		
	</td>
	<td align="right">16,252</td>
	<td align="right">2.94</td>
	<td align="right">2,646,926,004</td>
	<td align="right">2.46</td>
	<td align="right">415</td>
	<td align="right">2.23</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	Safari/605.1		
	</td>
	<td align="right">9,524</td>
	<td align="right">1.72</td>
	<td align="right">2,190,681,051</td>
	<td align="right">2.04</td>
	<td align="right">271</td>
	<td align="right">1.46</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	Safari/602.1		
	</td>
	<td align="right">8,265</td>
	<td align="right">1.49</td>
	<td align="right">1,558,171,939</td>
	<td align="right">1.45</td>
	<td align="right">230</td>
	<td align="right">1.24</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	bingbot/2.0		
	</td>
	<td align="right">8,001</td>
	<td align="right">1.45</td>
	<td align="right">790,969,639</td>
	<td align="right">0.73</td>
	<td align="right">1,727</td>
	<td align="right">9.26</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">10</th>
	<td>
	UptimeRobot/2.0		
	</td>
	<td align="right">5,573</td>
	<td align="right">1.01</td>
	<td align="right">638,457</td>
	<td align="right">0.00</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>

</tbody>
</table>

