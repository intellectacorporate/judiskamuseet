


<h2>Statistics for http://www.u5645470.fsdata.se, Month of 2019-6-1 to 2019-6-30</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 20 of 469 Referring Sites,
Sorted by Access Count

</h3>
<p></p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Referring Site</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th class="hogerstalld">1</th>
	<td>
	<a href="https://judiskamuseet.se">
	https://judiskamuseet.se</a>
	</td>
	<td align="right">401,858</td>
	<td align="right">93.91</td>
	<td align="right">91,592,242,708</td>
	<td align="right">98.98</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	<a href="http://judiskamuseet.se">
	http://judiskamuseet.se</a>
	</td>
	<td align="right">9,165</td>
	<td align="right">2.14</td>
	<td align="right">248,789,781</td>
	<td align="right">0.27</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	<a href="https://www.google.com">
	https://www.google.com</a>
	</td>
	<td align="right">6,653</td>
	<td align="right">1.55</td>
	<td align="right">320,950,223</td>
	<td align="right">0.35</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	<a href="https://www.google.se">
	https://www.google.se</a>
	</td>
	<td align="right">4,597</td>
	<td align="right">1.07</td>
	<td align="right">151,742,594</td>
	<td align="right">0.16</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	<a href="http://www.google.com">
	http://www.google.com</a>
	</td>
	<td align="right">590</td>
	<td align="right">0.14</td>
	<td align="right">5,095,131</td>
	<td align="right">0.01</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	<a href="http://m.facebook.com">
	http://m.facebook.com</a>
	</td>
	<td align="right">443</td>
	<td align="right">0.10</td>
	<td align="right">4,783,739</td>
	<td align="right">0.01</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	<a href="https://www.google.de">
	https://www.google.de</a>
	</td>
	<td align="right">408</td>
	<td align="right">0.10</td>
	<td align="right">8,116,066</td>
	<td align="right">0.01</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	<a href="https://www.bing.com">
	https://www.bing.com</a>
	</td>
	<td align="right">354</td>
	<td align="right">0.08</td>
	<td align="right">86,464,101</td>
	<td align="right">0.09</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	<a href="https://us15.admin.mailchimp.com">
	https://us15.admin.mailchimp.com</a>
	</td>
	<td align="right">193</td>
	<td align="right">0.05</td>
	<td align="right">6,229,437</td>
	<td align="right">0.01</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">10</th>
	<td>
	<a href="http://instagram.com">
	http://instagram.com</a>
	</td>
	<td align="right">178</td>
	<td align="right">0.04</td>
	<td align="right">2,325,072</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">11</th>
	<td>
	<a href="http://www.judiska-museet.se">
	http://www.judiska-museet.se</a>
	</td>
	<td align="right">141</td>
	<td align="right">0.03</td>
	<td align="right">5,650,478</td>
	<td align="right">0.01</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">12</th>
	<td>
	<a href="http://site.ru">
	http://site.ru</a>
	</td>
	<td align="right">138</td>
	<td align="right">0.03</td>
	<td align="right">2,453,384</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">13</th>
	<td>
	<a href="https://www.elegantthemes.com">
	https://www.elegantthemes.com</a>
	</td>
	<td align="right">134</td>
	<td align="right">0.03</td>
	<td align="right">10,314,271</td>
	<td align="right">0.01</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">14</th>
	<td>
	<a href="http://skola.karlstad.se">
	http://skola.karlstad.se</a>
	</td>
	<td align="right">112</td>
	<td align="right">0.03</td>
	<td align="right">429,760</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">15</th>
	<td>
	<a href="https://duckduckgo.com">
	https://duckduckgo.com</a>
	</td>
	<td align="right">93</td>
	<td align="right">0.02</td>
	<td align="right">1,369,049</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">16</th>
	<td>
	<a href="https://l.instagram.com">
	https://l.instagram.com</a>
	</td>
	<td align="right">92</td>
	<td align="right">0.02</td>
	<td align="right">410,097</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">17</th>
	<td>
	<a href="https://www.facebook.com">
	https://www.facebook.com</a>
	</td>
	<td align="right">92</td>
	<td align="right">0.02</td>
	<td align="right">940,974</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">18</th>
	<td>
	<a href="http://l.facebook.com">
	http://l.facebook.com</a>
	</td>
	<td align="right">88</td>
	<td align="right">0.02</td>
	<td align="right">458,885</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">19</th>
	<td>
	<a href="http://l.instagram.com">
	http://l.instagram.com</a>
	</td>
	<td align="right">80</td>
	<td align="right">0.02</td>
	<td align="right">303,545</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">20</th>
	<td>
	<a href="https://sv.m.wikipedia.org">
	https://sv.m.wikipedia.org</a>
	</td>
	<td align="right">73</td>
	<td align="right">0.02</td>
	<td align="right">446,587</td>
	<td align="right">0.00</td>
</tr>

</tbody>
</table>

