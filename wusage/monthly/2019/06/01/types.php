


<h2>Statistics for http://www.u5645470.fsdata.se, Month of 2019-6-1 to 2019-6-30</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 10 of 30 File Types (Extensions), 
Sorted by Access Count

</h3>
<p>Individual file types as determined by file extensions. All URLs that
do not contain an extension are counted as directories.</p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Type</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th class="hogerstalld">1</th>
	<td>
	js		
	</td>
	<td align="right">125,045</td>
	<td align="right">22.55</td>
	<td align="right">1,390,091,776</td>
	<td align="right">1.29</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	png		
	</td>
	<td align="right">122,221</td>
	<td align="right">22.04</td>
	<td align="right">9,638,164,192</td>
	<td align="right">8.95</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	jpg		
	</td>
	<td align="right">105,798</td>
	<td align="right">19.08</td>
	<td align="right">93,078,422,664</td>
	<td align="right">86.40</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	css		
	</td>
	<td align="right">94,373</td>
	<td align="right">17.02</td>
	<td align="right">1,294,167,507</td>
	<td align="right">1.20</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	Directory (folder)		
	</td>
	<td align="right">72,958</td>
	<td align="right">13.16</td>
	<td align="right">708,471,057</td>
	<td align="right">0.66</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	php		
	</td>
	<td align="right">11,856</td>
	<td align="right">2.14</td>
	<td align="right">199,776,664</td>
	<td align="right">0.19</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	ttf		
	</td>
	<td align="right">11,342</td>
	<td align="right">2.05</td>
	<td align="right">984,970,781</td>
	<td align="right">0.91</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	txt		
	</td>
	<td align="right">3,938</td>
	<td align="right">0.71</td>
	<td align="right">460,730</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	gif		
	</td>
	<td align="right">3,225</td>
	<td align="right">0.58</td>
	<td align="right">13,890,078</td>
	<td align="right">0.01</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">10</th>
	<td>
	ico		
	</td>
	<td align="right">1,613</td>
	<td align="right">0.29</td>
	<td align="right">2,369</td>
	<td align="right">0.00</td>
</tr>

</tbody>
</table>

