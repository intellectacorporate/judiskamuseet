


<h2>Statistics for http://www.u5645470.fsdata.se, Month of 2019-6-1 to 2019-6-30</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Traffic by Day</h3>
<p>The continuous line represents a 7-day moving average.</p>
<img src="d.png" width="468" height="351">
<br /><br />


<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Date</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	<th scope='col'>Bits per Sec</th>
	<th scope='col'>Bytes per Sec</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th align="right">2019-6-1</th>
	<td align="right">12,357	</td>
	<td align="right">2.23	</td>
	<td align="right">2,555,266,651	</td>
	<td align="right">2.37	</td>
	<td align="right">555	</td>
	<td align="right">2.97	</td>
	<td align="right">236,598.76	</td>
	<td align="right">29,574.85	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-6-2</th>
	<td align="right">13,013	</td>
	<td align="right">2.35	</td>
	<td align="right">2,940,707,159	</td>
	<td align="right">2.73	</td>
	<td align="right">387	</td>
	<td align="right">2.07	</td>
	<td align="right">272,287.70	</td>
	<td align="right">34,035.96	</td>
</tr>
<tr class="udda">
	<th align="right">2019-6-3</th>
	<td align="right">35,929	</td>
	<td align="right">6.48	</td>
	<td align="right">7,326,006,703	</td>
	<td align="right">6.80	</td>
	<td align="right">935	</td>
	<td align="right">5.00	</td>
	<td align="right">678,333.95	</td>
	<td align="right">84,791.74	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-6-4</th>
	<td align="right">29,150	</td>
	<td align="right">5.26	</td>
	<td align="right">6,495,592,921	</td>
	<td align="right">6.03	</td>
	<td align="right">739	</td>
	<td align="right">3.95	</td>
	<td align="right">601,443.79	</td>
	<td align="right">75,180.47	</td>
</tr>
<tr class="udda">
	<th align="right">2019-6-5</th>
	<td align="right">48,327	</td>
	<td align="right">8.72	</td>
	<td align="right">10,961,671,835	</td>
	<td align="right">10.18	</td>
	<td align="right">1,260	</td>
	<td align="right">6.73	</td>
	<td align="right">1,014,969.61	</td>
	<td align="right">126,871.20	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-6-6</th>
	<td align="right">45,912	</td>
	<td align="right">8.28	</td>
	<td align="right">10,952,616,341	</td>
	<td align="right">10.17	</td>
	<td align="right">1,171	</td>
	<td align="right">6.26	</td>
	<td align="right">1,014,131.14	</td>
	<td align="right">126,766.39	</td>
</tr>
<tr class="udda">
	<th align="right">2019-6-7</th>
	<td align="right">29,269	</td>
	<td align="right">5.28	</td>
	<td align="right">7,128,847,937	</td>
	<td align="right">6.62	</td>
	<td align="right">769	</td>
	<td align="right">4.11	</td>
	<td align="right">660,078.51	</td>
	<td align="right">82,509.81	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-6-8</th>
	<td align="right">19,317	</td>
	<td align="right">3.48	</td>
	<td align="right">6,121,196,372	</td>
	<td align="right">5.68	</td>
	<td align="right">678	</td>
	<td align="right">3.62	</td>
	<td align="right">566,777.44	</td>
	<td align="right">70,847.18	</td>
</tr>
<tr class="udda">
	<th align="right">2019-6-9</th>
	<td align="right">24,948	</td>
	<td align="right">4.50	</td>
	<td align="right">8,409,023,951	</td>
	<td align="right">7.81	</td>
	<td align="right">860	</td>
	<td align="right">4.60	</td>
	<td align="right">778,613.33	</td>
	<td align="right">97,326.67	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-6-10</th>
	<td align="right">34,266	</td>
	<td align="right">6.18	</td>
	<td align="right">4,721,052,410	</td>
	<td align="right">4.38	</td>
	<td align="right">851	</td>
	<td align="right">4.55	</td>
	<td align="right">437,134.48	</td>
	<td align="right">54,641.81	</td>
</tr>
<tr class="udda">
	<th align="right">2019-6-11</th>
	<td align="right">33,574	</td>
	<td align="right">6.06	</td>
	<td align="right">4,460,400,729	</td>
	<td align="right">4.14	</td>
	<td align="right">1,056	</td>
	<td align="right">5.64	</td>
	<td align="right">413,000.07	</td>
	<td align="right">51,625.01	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-6-12</th>
	<td align="right">26,050	</td>
	<td align="right">4.70	</td>
	<td align="right">4,205,915,667	</td>
	<td align="right">3.90	</td>
	<td align="right">870	</td>
	<td align="right">4.65	</td>
	<td align="right">389,436.64	</td>
	<td align="right">48,679.58	</td>
</tr>
<tr class="udda">
	<th align="right">2019-6-13</th>
	<td align="right">18,771	</td>
	<td align="right">3.39	</td>
	<td align="right">3,303,765,186	</td>
	<td align="right">3.07	</td>
	<td align="right">639	</td>
	<td align="right">3.42	</td>
	<td align="right">305,904.18	</td>
	<td align="right">38,238.02	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-6-14</th>
	<td align="right">17,214	</td>
	<td align="right">3.10	</td>
	<td align="right">2,694,323,869	</td>
	<td align="right">2.50	</td>
	<td align="right">565	</td>
	<td align="right">3.02	</td>
	<td align="right">249,474.43	</td>
	<td align="right">31,184.30	</td>
</tr>
<tr class="udda">
	<th align="right">2019-6-15</th>
	<td align="right">13,923	</td>
	<td align="right">2.51	</td>
	<td align="right">2,299,877,980	</td>
	<td align="right">2.13	</td>
	<td align="right">574	</td>
	<td align="right">3.07	</td>
	<td align="right">212,951.66	</td>
	<td align="right">26,618.96	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-6-16</th>
	<td align="right">13,958	</td>
	<td align="right">2.52	</td>
	<td align="right">1,981,976,051	</td>
	<td align="right">1.84	</td>
	<td align="right">619	</td>
	<td align="right">3.31	</td>
	<td align="right">183,516.30	</td>
	<td align="right">22,939.54	</td>
</tr>
<tr class="udda">
	<th align="right">2019-6-17</th>
	<td align="right">12,619	</td>
	<td align="right">2.28	</td>
	<td align="right">1,886,811,063	</td>
	<td align="right">1.75	</td>
	<td align="right">514	</td>
	<td align="right">2.75	</td>
	<td align="right">174,704.73	</td>
	<td align="right">21,838.09	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-6-18</th>
	<td align="right">18,568	</td>
	<td align="right">3.35	</td>
	<td align="right">2,422,225,720	</td>
	<td align="right">2.25	</td>
	<td align="right">560	</td>
	<td align="right">2.99	</td>
	<td align="right">224,280.16	</td>
	<td align="right">28,035.02	</td>
</tr>
<tr class="udda">
	<th align="right">2019-6-19</th>
	<td align="right">12,966	</td>
	<td align="right">2.34	</td>
	<td align="right">1,941,470,413	</td>
	<td align="right">1.80	</td>
	<td align="right">491	</td>
	<td align="right">2.62	</td>
	<td align="right">179,765.78	</td>
	<td align="right">22,470.72	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-6-20</th>
	<td align="right">6,719	</td>
	<td align="right">1.21	</td>
	<td align="right">1,005,227,832	</td>
	<td align="right">0.93	</td>
	<td align="right">329	</td>
	<td align="right">1.76	</td>
	<td align="right">93,076.65	</td>
	<td align="right">11,634.58	</td>
</tr>
<tr class="udda">
	<th align="right">2019-6-21</th>
	<td align="right">5,675	</td>
	<td align="right">1.02	</td>
	<td align="right">761,731,998	</td>
	<td align="right">0.71	</td>
	<td align="right">314	</td>
	<td align="right">1.68	</td>
	<td align="right">70,530.74	</td>
	<td align="right">8,816.34	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-6-22</th>
	<td align="right">7,613	</td>
	<td align="right">1.37	</td>
	<td align="right">1,219,839,585	</td>
	<td align="right">1.13	</td>
	<td align="right">379	</td>
	<td align="right">2.03	</td>
	<td align="right">112,948.11	</td>
	<td align="right">14,118.51	</td>
</tr>
<tr class="udda">
	<th align="right">2019-6-23</th>
	<td align="right">7,445	</td>
	<td align="right">1.34	</td>
	<td align="right">1,273,331,479	</td>
	<td align="right">1.18	</td>
	<td align="right">385	</td>
	<td align="right">2.06	</td>
	<td align="right">117,901.06	</td>
	<td align="right">14,737.63	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-6-24</th>
	<td align="right">8,934	</td>
	<td align="right">1.61	</td>
	<td align="right">1,423,615,311	</td>
	<td align="right">1.32	</td>
	<td align="right">369	</td>
	<td align="right">1.97	</td>
	<td align="right">131,816.23	</td>
	<td align="right">16,477.03	</td>
</tr>
<tr class="udda">
	<th align="right">2019-6-25</th>
	<td align="right">9,337	</td>
	<td align="right">1.68	</td>
	<td align="right">1,828,712,312	</td>
	<td align="right">1.70	</td>
	<td align="right">403	</td>
	<td align="right">2.15	</td>
	<td align="right">169,325.21	</td>
	<td align="right">21,165.65	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-6-26</th>
	<td align="right">10,593	</td>
	<td align="right">1.91	</td>
	<td align="right">1,690,331,103	</td>
	<td align="right">1.57	</td>
	<td align="right">453	</td>
	<td align="right">2.42	</td>
	<td align="right">156,512.14	</td>
	<td align="right">19,564.02	</td>
</tr>
<tr class="udda">
	<th align="right">2019-6-27</th>
	<td align="right">15,187	</td>
	<td align="right">2.74	</td>
	<td align="right">2,451,946,404	</td>
	<td align="right">2.28	</td>
	<td align="right">708	</td>
	<td align="right">3.78	</td>
	<td align="right">227,032.07	</td>
	<td align="right">28,379.01	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-6-28</th>
	<td align="right">9,374	</td>
	<td align="right">1.69	</td>
	<td align="right">1,251,516,108	</td>
	<td align="right">1.16	</td>
	<td align="right">461	</td>
	<td align="right">2.46	</td>
	<td align="right">115,881.12	</td>
	<td align="right">14,485.14	</td>
</tr>
<tr class="udda">
	<th align="right">2019-6-29</th>
	<td align="right">7,125	</td>
	<td align="right">1.28	</td>
	<td align="right">1,067,645,466	</td>
	<td align="right">0.99	</td>
	<td align="right">404	</td>
	<td align="right">2.16	</td>
	<td align="right">98,856.06	</td>
	<td align="right">12,357.01	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-6-30</th>
	<td align="right">6,349	</td>
	<td align="right">1.15	</td>
	<td align="right">947,757,128	</td>
	<td align="right">0.88	</td>
	<td align="right">411	</td>
	<td align="right">2.20	</td>
	<td align="right">87,755.29	</td>
	<td align="right">10,969.41	</td>
</tr>

</tbody>
</table>

