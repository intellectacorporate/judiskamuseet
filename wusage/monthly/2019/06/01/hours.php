


<h2>Statistics for http://www.u5645470.fsdata.se, Month of 2019-6-1 to 2019-6-30</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Traffic by Hour</h3>
<p>Figures are averages for that hour
	of each day in the time period analyzed.</p>
<img src="h.png" width="468" height="351">
<br /><br />
<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Hour</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	<th scope='col'>Bits per Sec</th>
	<th scope='col'>Bytes per Sec</th>
	
	</tr>
</thead>
<tbody>
<tr class="jamnudda">
	<th align="right">00:00</th>
	<td align="right">364.40</td>
	<td align="right">1.97</td>
	<td align="right">81,527,961.70</td>
	<td align="right">2.27</td>
	<td align="right">10.81</td>
	<td align="right">1.73</td>
	<td align="right">181,173.25</td>
	<td align="right">22,646.66</td>
	
</tr>
<tr class="jamn">
	<th align="right">01:00</th>
	<td align="right">191.77</td>
	<td align="right">1.04</td>
	<td align="right">38,258,107.70</td>
	<td align="right">1.07</td>
	<td align="right">11.52</td>
	<td align="right">1.85</td>
	<td align="right">85,018.02</td>
	<td align="right">10,627.25</td>
	
</tr>
<tr class="udda">
	<th align="right">02:00</th>
	<td align="right">149.70</td>
	<td align="right">0.81</td>
	<td align="right">26,715,708.83</td>
	<td align="right">0.74</td>
	<td align="right">9.48</td>
	<td align="right">1.52</td>
	<td align="right">59,368.24</td>
	<td align="right">7,421.03</td>
	
</tr>
<tr class="jamn">
	<th align="right">03:00</th>
	<td align="right">116.27</td>
	<td align="right">0.63</td>
	<td align="right">13,614,690.77</td>
	<td align="right">0.38</td>
	<td align="right">9.20</td>
	<td align="right">1.48</td>
	<td align="right">30,254.87</td>
	<td align="right">3,781.86</td>
	
</tr>
<tr class="udda">
	<th align="right">04:00</th>
	<td align="right">132.97</td>
	<td align="right">0.72</td>
	<td align="right">15,807,802.47</td>
	<td align="right">0.44</td>
	<td align="right">8.33</td>
	<td align="right">1.34</td>
	<td align="right">35,128.45</td>
	<td align="right">4,391.06</td>
	
</tr>
<tr class="jamn">
	<th align="right">05:00</th>
	<td align="right">176.40</td>
	<td align="right">0.95</td>
	<td align="right">33,967,012.47</td>
	<td align="right">0.95</td>
	<td align="right">10.04</td>
	<td align="right">1.61</td>
	<td align="right">75,482.25</td>
	<td align="right">9,435.28</td>
	
</tr>
<tr class="udda">
	<th align="right">06:00</th>
	<td align="right">284.07</td>
	<td align="right">1.54</td>
	<td align="right">52,029,537.93</td>
	<td align="right">1.45</td>
	<td align="right">11.85</td>
	<td align="right">1.90</td>
	<td align="right">115,621.20</td>
	<td align="right">14,452.65</td>
	
</tr>
<tr class="jamn">
	<th align="right">07:00</th>
	<td align="right">492.73</td>
	<td align="right">2.67</td>
	<td align="right">104,613,805.90</td>
	<td align="right">2.91</td>
	<td align="right">16.89</td>
	<td align="right">2.71</td>
	<td align="right">232,475.12</td>
	<td align="right">29,059.39</td>
	
</tr>
<tr class="udda">
	<th align="right">08:00</th>
	<td align="right">961.60</td>
	<td align="right">5.20</td>
	<td align="right">171,983,012.93</td>
	<td align="right">4.79</td>
	<td align="right">24.99</td>
	<td align="right">4.01</td>
	<td align="right">382,184.47</td>
	<td align="right">47,773.06</td>
	
</tr>
<tr class="jamn">
	<th align="right">09:00</th>
	<td align="right">1,273.10</td>
	<td align="right">6.89</td>
	<td align="right">268,716,278.90</td>
	<td align="right">7.48</td>
	<td align="right">35.01</td>
	<td align="right">5.61</td>
	<td align="right">597,147.29</td>
	<td align="right">74,643.41</td>
	
</tr>
<tr class="udda">
	<th align="right">10:00</th>
	<td align="right">1,385.40</td>
	<td align="right">7.50</td>
	<td align="right">297,554,615.57</td>
	<td align="right">8.29</td>
	<td align="right">40.12</td>
	<td align="right">6.43</td>
	<td align="right">661,232.48</td>
	<td align="right">82,654.06</td>
	
</tr>
<tr class="jamn">
	<th align="right">11:00</th>
	<td align="right">1,227.30</td>
	<td align="right">6.64</td>
	<td align="right">273,800,083.77</td>
	<td align="right">7.62</td>
	<td align="right">39.18</td>
	<td align="right">6.28</td>
	<td align="right">608,444.63</td>
	<td align="right">76,055.58</td>
	
</tr>
<tr class="udda">
	<th align="right">12:00</th>
	<td align="right">1,176.90</td>
	<td align="right">6.37</td>
	<td align="right">219,062,675.33</td>
	<td align="right">6.10</td>
	<td align="right">35.33</td>
	<td align="right">5.67</td>
	<td align="right">486,805.95</td>
	<td align="right">60,850.74</td>
	
</tr>
<tr class="jamn">
	<th align="right">13:00</th>
	<td align="right">1,234.90</td>
	<td align="right">6.68</td>
	<td align="right">254,751,266.73</td>
	<td align="right">7.09</td>
	<td align="right">39.06</td>
	<td align="right">6.26</td>
	<td align="right">566,113.93</td>
	<td align="right">70,764.24</td>
	
</tr>
<tr class="udda">
	<th align="right">14:00</th>
	<td align="right">1,343.13</td>
	<td align="right">7.27</td>
	<td align="right">222,063,392.63</td>
	<td align="right">6.18</td>
	<td align="right">35.12</td>
	<td align="right">5.63</td>
	<td align="right">493,474.21</td>
	<td align="right">61,684.28</td>
	
</tr>
<tr class="jamn">
	<th align="right">15:00</th>
	<td align="right">1,236.80</td>
	<td align="right">6.69</td>
	<td align="right">212,575,676.37</td>
	<td align="right">5.92</td>
	<td align="right">40.08</td>
	<td align="right">6.43</td>
	<td align="right">472,390.39</td>
	<td align="right">59,048.80</td>
	
</tr>
<tr class="udda">
	<th align="right">16:00</th>
	<td align="right">1,012.57</td>
	<td align="right">5.48</td>
	<td align="right">165,237,861.80</td>
	<td align="right">4.60</td>
	<td align="right">33.97</td>
	<td align="right">5.45</td>
	<td align="right">367,195.25</td>
	<td align="right">45,899.41</td>
	
</tr>
<tr class="jamn">
	<th align="right">17:00</th>
	<td align="right">934.33</td>
	<td align="right">5.06</td>
	<td align="right">186,974,886.73</td>
	<td align="right">5.21</td>
	<td align="right">33.83</td>
	<td align="right">5.42</td>
	<td align="right">415,499.75</td>
	<td align="right">51,937.47</td>
	
</tr>
<tr class="udda">
	<th align="right">18:00</th>
	<td align="right">885.17</td>
	<td align="right">4.79</td>
	<td align="right">167,021,068.93</td>
	<td align="right">4.65</td>
	<td align="right">29.55</td>
	<td align="right">4.74</td>
	<td align="right">371,157.93</td>
	<td align="right">46,394.74</td>
	
</tr>
<tr class="jamn">
	<th align="right">19:00</th>
	<td align="right">712.93</td>
	<td align="right">3.86</td>
	<td align="right">132,714,903.37</td>
	<td align="right">3.70</td>
	<td align="right">27.37</td>
	<td align="right">4.39</td>
	<td align="right">294,922.01</td>
	<td align="right">36,865.25</td>
	
</tr>
<tr class="udda">
	<th align="right">20:00</th>
	<td align="right">790.97</td>
	<td align="right">4.28</td>
	<td align="right">153,242,671.03</td>
	<td align="right">4.27</td>
	<td align="right">28.66</td>
	<td align="right">4.60</td>
	<td align="right">340,539.27</td>
	<td align="right">42,567.41</td>
	
</tr>
<tr class="jamn">
	<th align="right">21:00</th>
	<td align="right">989.23</td>
	<td align="right">5.35</td>
	<td align="right">193,416,923.10</td>
	<td align="right">5.39</td>
	<td align="right">27.75</td>
	<td align="right">4.45</td>
	<td align="right">429,815.38</td>
	<td align="right">53,726.92</td>
	
</tr>
<tr class="udda">
	<th align="right">22:00</th>
	<td align="right">825.50</td>
	<td align="right">4.47</td>
	<td align="right">179,782,225.53</td>
	<td align="right">5.01</td>
	<td align="right">32.33</td>
	<td align="right">5.18</td>
	<td align="right">399,516.06</td>
	<td align="right">49,939.51</td>
	
</tr>
<tr class="jamn">
	<th align="right">23:00</th>
	<td align="right">584.60</td>
	<td align="right">3.16</td>
	<td align="right">125,581,285.63</td>
	<td align="right">3.50</td>
	<td align="right">33.14</td>
	<td align="right">5.31</td>
	<td align="right">279,069.52</td>
	<td align="right">34,883.69</td>
	
</tr>

</tbody>
</table>

