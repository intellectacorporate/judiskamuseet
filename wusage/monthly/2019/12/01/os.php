


<h2>Statistics for http://www.u5645470.fsdata.se, Month of 2019-12-1 to 2019-12-31</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 10 of 16 Operating Systems,
Sorted by Access Count

</h3>
<p></p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Product</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th class="hogerstalld">1</th>
	<td>
	Other		
	</td>
	<td align="right">165,582</td>
	<td align="right">49.69</td>
	<td align="right">2,510,000,146</td>
	<td align="right">14.95</td>
	<td align="right">3,217</td>
	<td align="right">21.23</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	Mac OS X		
	</td>
	<td align="right">147,888</td>
	<td align="right">44.38</td>
	<td align="right">13,266,144,997</td>
	<td align="right">79.01</td>
	<td align="right">8,578</td>
	<td align="right">56.60</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	Linux		
	</td>
	<td align="right">10,079</td>
	<td align="right">3.02</td>
	<td align="right">114,263,380</td>
	<td align="right">0.68</td>
	<td align="right">2,858</td>
	<td align="right">18.86</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	Windows NT		
	</td>
	<td align="right">8,555</td>
	<td align="right">2.57</td>
	<td align="right">839,130,140</td>
	<td align="right">5.00</td>
	<td align="right">476</td>
	<td align="right">3.15</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	Windows XP		
	</td>
	<td align="right">632</td>
	<td align="right">0.19</td>
	<td align="right">33,834,804</td>
	<td align="right">0.20</td>
	<td align="right">18</td>
	<td align="right">0.12</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	Windows Codename Longhorn		
	</td>
	<td align="right">196</td>
	<td align="right">0.06</td>
	<td align="right">17,479,877</td>
	<td align="right">0.10</td>
	<td align="right">6</td>
	<td align="right">0.05</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	Windows 2003		
	</td>
	<td align="right">128</td>
	<td align="right">0.04</td>
	<td align="right">2,938,170</td>
	<td align="right">0.02</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	Windows 2000		
	</td>
	<td align="right">65</td>
	<td align="right">0.02</td>
	<td align="right">3,327,593</td>
	<td align="right">0.02</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	Windows 95		
	</td>
	<td align="right">39</td>
	<td align="right">0.01</td>
	<td align="right">2,378,673</td>
	<td align="right">0.01</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">10</th>
	<td>
	Windows Me		
	</td>
	<td align="right">11</td>
	<td align="right">0.00</td>
	<td align="right">494,713</td>
	<td align="right">0.00</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>

</tbody>
</table>

