


<h2>Statistics for http://www.u5645470.fsdata.se, Month of 2019-12-1 to 2019-12-31</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Traffic by Hour</h3>
<p>Figures are averages for that hour
	of each day in the time period analyzed.</p>
<img src="h.png" width="468" height="351">
<br /><br />
<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Hour</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	<th scope='col'>Bits per Sec</th>
	<th scope='col'>Bytes per Sec</th>
	
	</tr>
</thead>
<tbody>
<tr class="jamnudda">
	<th align="right">00:00</th>
	<td align="right">347.68</td>
	<td align="right">3.23</td>
	<td align="right">12,281,502.00</td>
	<td align="right">2.26</td>
	<td align="right">10.24</td>
	<td align="right">2.10</td>
	<td align="right">27,292.23</td>
	<td align="right">3,411.53</td>
	
</tr>
<tr class="jamn">
	<th align="right">01:00</th>
	<td align="right">292.81</td>
	<td align="right">2.72</td>
	<td align="right">9,620,242.39</td>
	<td align="right">1.77</td>
	<td align="right">12.09</td>
	<td align="right">2.47</td>
	<td align="right">21,378.32</td>
	<td align="right">2,672.29</td>
	
</tr>
<tr class="udda">
	<th align="right">02:00</th>
	<td align="right">296.35</td>
	<td align="right">2.75</td>
	<td align="right">6,666,270.19</td>
	<td align="right">1.23</td>
	<td align="right">10.50</td>
	<td align="right">2.15</td>
	<td align="right">14,813.93</td>
	<td align="right">1,851.74</td>
	
</tr>
<tr class="jamn">
	<th align="right">03:00</th>
	<td align="right">279.32</td>
	<td align="right">2.60</td>
	<td align="right">5,810,189.26</td>
	<td align="right">1.07</td>
	<td align="right">11.25</td>
	<td align="right">2.30</td>
	<td align="right">12,911.53</td>
	<td align="right">1,613.94</td>
	
</tr>
<tr class="udda">
	<th align="right">04:00</th>
	<td align="right">262.52</td>
	<td align="right">2.44</td>
	<td align="right">5,212,326.10</td>
	<td align="right">0.96</td>
	<td align="right">10.09</td>
	<td align="right">2.06</td>
	<td align="right">11,582.95</td>
	<td align="right">1,447.87</td>
	
</tr>
<tr class="jamn">
	<th align="right">05:00</th>
	<td align="right">273.42</td>
	<td align="right">2.54</td>
	<td align="right">5,021,162.55</td>
	<td align="right">0.92</td>
	<td align="right">10.51</td>
	<td align="right">2.15</td>
	<td align="right">11,158.14</td>
	<td align="right">1,394.77</td>
	
</tr>
<tr class="udda">
	<th align="right">06:00</th>
	<td align="right">290.74</td>
	<td align="right">2.70</td>
	<td align="right">7,438,319.26</td>
	<td align="right">1.37</td>
	<td align="right">10.35</td>
	<td align="right">2.12</td>
	<td align="right">16,529.60</td>
	<td align="right">2,066.20</td>
	
</tr>
<tr class="jamn">
	<th align="right">07:00</th>
	<td align="right">315.97</td>
	<td align="right">2.94</td>
	<td align="right">8,722,731.52</td>
	<td align="right">1.61</td>
	<td align="right">14.27</td>
	<td align="right">2.92</td>
	<td align="right">19,383.85</td>
	<td align="right">2,422.98</td>
	
</tr>
<tr class="udda">
	<th align="right">08:00</th>
	<td align="right">414.48</td>
	<td align="right">3.85</td>
	<td align="right">19,506,944.87</td>
	<td align="right">3.59</td>
	<td align="right">16.73</td>
	<td align="right">3.42</td>
	<td align="right">43,348.77</td>
	<td align="right">5,418.60</td>
	
</tr>
<tr class="jamn">
	<th align="right">09:00</th>
	<td align="right">549.55</td>
	<td align="right">5.11</td>
	<td align="right">31,828,776.26</td>
	<td align="right">5.86</td>
	<td align="right">27.85</td>
	<td align="right">5.70</td>
	<td align="right">70,730.61</td>
	<td align="right">8,841.33</td>
	
</tr>
<tr class="udda">
	<th align="right">10:00</th>
	<td align="right">595.65</td>
	<td align="right">5.53</td>
	<td align="right">38,293,718.16</td>
	<td align="right">7.05</td>
	<td align="right">27.44</td>
	<td align="right">5.61</td>
	<td align="right">85,097.15</td>
	<td align="right">10,637.14</td>
	
</tr>
<tr class="jamn">
	<th align="right">11:00</th>
	<td align="right">584.10</td>
	<td align="right">5.43</td>
	<td align="right">36,065,972.35</td>
	<td align="right">6.64</td>
	<td align="right">28.14</td>
	<td align="right">5.76</td>
	<td align="right">80,146.61</td>
	<td align="right">10,018.33</td>
	
</tr>
<tr class="udda">
	<th align="right">12:00</th>
	<td align="right">528.03</td>
	<td align="right">4.91</td>
	<td align="right">31,532,348.19</td>
	<td align="right">5.81</td>
	<td align="right">23.63</td>
	<td align="right">4.83</td>
	<td align="right">70,071.88</td>
	<td align="right">8,758.99</td>
	
</tr>
<tr class="jamn">
	<th align="right">13:00</th>
	<td align="right">583.74</td>
	<td align="right">5.42</td>
	<td align="right">33,608,146.65</td>
	<td align="right">6.19</td>
	<td align="right">24.54</td>
	<td align="right">5.02</td>
	<td align="right">74,684.77</td>
	<td align="right">9,335.60</td>
	
</tr>
<tr class="udda">
	<th align="right">14:00</th>
	<td align="right">575.00</td>
	<td align="right">5.34</td>
	<td align="right">35,829,057.32</td>
	<td align="right">6.60</td>
	<td align="right">26.83</td>
	<td align="right">5.49</td>
	<td align="right">79,620.13</td>
	<td align="right">9,952.52</td>
	
</tr>
<tr class="jamn">
	<th align="right">15:00</th>
	<td align="right">556.52</td>
	<td align="right">5.17</td>
	<td align="right">32,151,165.94</td>
	<td align="right">5.92</td>
	<td align="right">25.03</td>
	<td align="right">5.12</td>
	<td align="right">71,447.04</td>
	<td align="right">8,930.88</td>
	
</tr>
<tr class="udda">
	<th align="right">16:00</th>
	<td align="right">529.55</td>
	<td align="right">4.92</td>
	<td align="right">28,631,765.26</td>
	<td align="right">5.27</td>
	<td align="right">21.95</td>
	<td align="right">4.49</td>
	<td align="right">63,626.15</td>
	<td align="right">7,953.27</td>
	
</tr>
<tr class="jamn">
	<th align="right">17:00</th>
	<td align="right">497.42</td>
	<td align="right">4.62</td>
	<td align="right">26,846,946.23</td>
	<td align="right">4.94</td>
	<td align="right">24.30</td>
	<td align="right">4.97</td>
	<td align="right">59,659.88</td>
	<td align="right">7,457.49</td>
	
</tr>
<tr class="udda">
	<th align="right">18:00</th>
	<td align="right">508.90</td>
	<td align="right">4.73</td>
	<td align="right">28,211,190.23</td>
	<td align="right">5.19</td>
	<td align="right">23.98</td>
	<td align="right">4.90</td>
	<td align="right">62,691.53</td>
	<td align="right">7,836.44</td>
	
</tr>
<tr class="jamn">
	<th align="right">19:00</th>
	<td align="right">517.65</td>
	<td align="right">4.81</td>
	<td align="right">31,519,263.26</td>
	<td align="right">5.80</td>
	<td align="right">24.40</td>
	<td align="right">4.99</td>
	<td align="right">70,042.81</td>
	<td align="right">8,755.35</td>
	
</tr>
<tr class="udda">
	<th align="right">20:00</th>
	<td align="right">514.77</td>
	<td align="right">4.78</td>
	<td align="right">28,140,126.06</td>
	<td align="right">5.18</td>
	<td align="right">25.23</td>
	<td align="right">5.16</td>
	<td align="right">62,533.61</td>
	<td align="right">7,816.70</td>
	
</tr>
<tr class="jamn">
	<th align="right">21:00</th>
	<td align="right">530.87</td>
	<td align="right">4.93</td>
	<td align="right">33,248,852.42</td>
	<td align="right">6.12</td>
	<td align="right">25.70</td>
	<td align="right">5.26</td>
	<td align="right">73,886.34</td>
	<td align="right">9,235.79</td>
	
</tr>
<tr class="udda">
	<th align="right">22:00</th>
	<td align="right">490.26</td>
	<td align="right">4.56</td>
	<td align="right">27,077,294.42</td>
	<td align="right">4.99</td>
	<td align="right">23.15</td>
	<td align="right">4.74</td>
	<td align="right">60,171.77</td>
	<td align="right">7,521.47</td>
	
</tr>
<tr class="jamn">
	<th align="right">23:00</th>
	<td align="right">427.42</td>
	<td align="right">3.97</td>
	<td align="right">19,865,353.61</td>
	<td align="right">3.66</td>
	<td align="right">30.69</td>
	<td align="right">6.28</td>
	<td align="right">44,145.23</td>
	<td align="right">5,518.15</td>
	
</tr>

</tbody>
</table>

