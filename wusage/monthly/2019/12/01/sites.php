


<h2>Statistics for http://www.u5645470.fsdata.se, Month of 2019-12-1 to 2019-12-31</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 20 of 11,763 Visitor Sites,
Sorted by Access Count

</h3>
<p></p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Site</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="jamnudda">
	<th class="hogerstalld">1</th>
	<td>
	46.246.122.10		
	</td>
	<td align="right">11,541</td>
	<td align="right">3.46</td>
	<td align="right">94,777,369</td>
	<td align="right">0.56</td>
	<td align="right">31</td>
	<td align="right">0.21</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	185.93.3.92		
	</td>
	<td align="right">10,578</td>
	<td align="right">3.17</td>
	<td align="right">87,292,343</td>
	<td align="right">0.52</td>
	<td align="right">29</td>
	<td align="right">0.19</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	85.195.116.134		
	</td>
	<td align="right">9,475</td>
	<td align="right">2.84</td>
	<td align="right">75,860,587</td>
	<td align="right">0.45</td>
	<td align="right">16</td>
	<td align="right">0.11</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	94.75.211.74		
	</td>
	<td align="right">9,143</td>
	<td align="right">2.74</td>
	<td align="right">71,698,027</td>
	<td align="right">0.43</td>
	<td align="right">4</td>
	<td align="right">0.03</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	185.136.156.82		
	</td>
	<td align="right">8,620</td>
	<td align="right">2.58</td>
	<td align="right">67,480,287</td>
	<td align="right">0.40</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	82.103.145.126		
	</td>
	<td align="right">7,647</td>
	<td align="right">2.29</td>
	<td align="right">61,432,405</td>
	<td align="right">0.36</td>
	<td align="right">5</td>
	<td align="right">0.04</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	82.103.139.165		
	</td>
	<td align="right">7,618</td>
	<td align="right">2.28</td>
	<td align="right">59,519,561</td>
	<td align="right">0.35</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	94.247.174.83		
	</td>
	<td align="right">7,560</td>
	<td align="right">2.27</td>
	<td align="right">62,961,608</td>
	<td align="right">0.37</td>
	<td align="right">20</td>
	<td align="right">0.14</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	46.59.25.88		
	</td>
	<td align="right">6,418</td>
	<td align="right">1.92</td>
	<td align="right">407,102,770</td>
	<td align="right">2.42</td>
	<td align="right">124</td>
	<td align="right">0.82</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">10</th>
	<td>
	63.143.42.242		
	</td>
	<td align="right">6,034</td>
	<td align="right">1.81</td>
	<td align="right">104,480</td>
	<td align="right">0.00</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">11</th>
	<td>
	185.70.76.23		
	</td>
	<td align="right">5,373</td>
	<td align="right">1.61</td>
	<td align="right">43,411,501</td>
	<td align="right">0.26</td>
	<td align="right">7</td>
	<td align="right">0.05</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">12</th>
	<td>
	37.252.231.50		
	</td>
	<td align="right">5,338</td>
	<td align="right">1.60</td>
	<td align="right">44,375,480</td>
	<td align="right">0.26</td>
	<td align="right">11</td>
	<td align="right">0.08</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">13</th>
	<td>
	185.180.12.65		
	</td>
	<td align="right">4,912</td>
	<td align="right">1.47</td>
	<td align="right">43,029,417</td>
	<td align="right">0.26</td>
	<td align="right">5</td>
	<td align="right">0.04</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">14</th>
	<td>
	185.246.208.82		
	</td>
	<td align="right">4,898</td>
	<td align="right">1.47</td>
	<td align="right">37,595,187</td>
	<td align="right">0.22</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">15</th>
	<td>
	46.20.45.18		
	</td>
	<td align="right">4,666</td>
	<td align="right">1.40</td>
	<td align="right">35,783,199</td>
	<td align="right">0.21</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">16</th>
	<td>
	151.106.52.134		
	</td>
	<td align="right">4,443</td>
	<td align="right">1.33</td>
	<td align="right">40,298,010</td>
	<td align="right">0.24</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">17</th>
	<td>
	82.103.136.16		
	</td>
	<td align="right">4,429</td>
	<td align="right">1.33</td>
	<td align="right">40,171,030</td>
	<td align="right">0.24</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">18</th>
	<td>
	185.39.146.214		
	</td>
	<td align="right">3,875</td>
	<td align="right">1.16</td>
	<td align="right">35,146,250</td>
	<td align="right">0.21</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">19</th>
	<td>
	185.39.146.215		
	</td>
	<td align="right">3,633</td>
	<td align="right">1.09</td>
	<td align="right">29,058,073</td>
	<td align="right">0.17</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">20</th>
	<td>
	89.163.242.206		
	</td>
	<td align="right">3,507</td>
	<td align="right">1.05</td>
	<td align="right">31,808,490</td>
	<td align="right">0.19</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>

</tbody>
</table>

