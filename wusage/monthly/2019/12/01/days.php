


<h2>Statistics for http://www.u5645470.fsdata.se, Month of 2019-12-1 to 2019-12-31</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Traffic by Day</h3>
<p>The continuous line represents a 7-day moving average.</p>
<img src="d.png" width="468" height="351">
<br /><br />


<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Date</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	<th scope='col'>Bits per Sec</th>
	<th scope='col'>Bytes per Sec</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th align="right">2019-12-1</th>
	<td align="right">11,181	</td>
	<td align="right">3.35	</td>
	<td align="right">636,151,817	</td>
	<td align="right">3.78	</td>
	<td align="right">518	</td>
	<td align="right">3.42	</td>
	<td align="right">58,902.95	</td>
	<td align="right">7,362.87	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-12-2</th>
	<td align="right">13,094	</td>
	<td align="right">3.92	</td>
	<td align="right">742,293,112	</td>
	<td align="right">4.41	</td>
	<td align="right">606	</td>
	<td align="right">4.00	</td>
	<td align="right">68,730.84	</td>
	<td align="right">8,591.36	</td>
</tr>
<tr class="udda">
	<th align="right">2019-12-3</th>
	<td align="right">16,907	</td>
	<td align="right">5.07	</td>
	<td align="right">1,026,304,577	</td>
	<td align="right">6.10	</td>
	<td align="right">875	</td>
	<td align="right">5.77	</td>
	<td align="right">95,028.20	</td>
	<td align="right">11,878.53	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-12-4</th>
	<td align="right">15,136	</td>
	<td align="right">4.54	</td>
	<td align="right">878,668,308	</td>
	<td align="right">5.22	</td>
	<td align="right">736	</td>
	<td align="right">4.86	</td>
	<td align="right">81,358.18	</td>
	<td align="right">10,169.77	</td>
</tr>
<tr class="udda">
	<th align="right">2019-12-5</th>
	<td align="right">12,732	</td>
	<td align="right">3.82	</td>
	<td align="right">613,430,839	</td>
	<td align="right">3.64	</td>
	<td align="right">566	</td>
	<td align="right">3.73	</td>
	<td align="right">56,799.15	</td>
	<td align="right">7,099.89	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-12-6</th>
	<td align="right">11,260	</td>
	<td align="right">3.37	</td>
	<td align="right">600,116,642	</td>
	<td align="right">3.56	</td>
	<td align="right">471	</td>
	<td align="right">3.11	</td>
	<td align="right">55,566.36	</td>
	<td align="right">6,945.79	</td>
</tr>
<tr class="udda">
	<th align="right">2019-12-7</th>
	<td align="right">10,690	</td>
	<td align="right">3.20	</td>
	<td align="right">529,728,609	</td>
	<td align="right">3.15	</td>
	<td align="right">601	</td>
	<td align="right">3.97	</td>
	<td align="right">49,048.95	</td>
	<td align="right">6,131.12	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-12-8</th>
	<td align="right">10,599	</td>
	<td align="right">3.18	</td>
	<td align="right">475,075,606	</td>
	<td align="right">2.82	</td>
	<td align="right">525	</td>
	<td align="right">3.46	</td>
	<td align="right">43,988.48	</td>
	<td align="right">5,498.56	</td>
</tr>
<tr class="udda">
	<th align="right">2019-12-9</th>
	<td align="right">13,005	</td>
	<td align="right">3.90	</td>
	<td align="right">729,721,752	</td>
	<td align="right">4.33	</td>
	<td align="right">607	</td>
	<td align="right">4.00	</td>
	<td align="right">67,566.83	</td>
	<td align="right">8,445.85	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-12-10</th>
	<td align="right">12,064	</td>
	<td align="right">3.62	</td>
	<td align="right">638,643,300	</td>
	<td align="right">3.79	</td>
	<td align="right">528	</td>
	<td align="right">3.48	</td>
	<td align="right">59,133.64	</td>
	<td align="right">7,391.70	</td>
</tr>
<tr class="udda">
	<th align="right">2019-12-11</th>
	<td align="right">12,601	</td>
	<td align="right">3.78	</td>
	<td align="right">684,374,619	</td>
	<td align="right">4.06	</td>
	<td align="right">543	</td>
	<td align="right">3.58	</td>
	<td align="right">63,368.02	</td>
	<td align="right">7,921.00	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-12-12</th>
	<td align="right">12,812	</td>
	<td align="right">3.84	</td>
	<td align="right">807,342,712	</td>
	<td align="right">4.80	</td>
	<td align="right">566	</td>
	<td align="right">3.73	</td>
	<td align="right">74,753.95	</td>
	<td align="right">9,344.24	</td>
</tr>
<tr class="udda">
	<th align="right">2019-12-13</th>
	<td align="right">11,482	</td>
	<td align="right">3.44	</td>
	<td align="right">671,671,359	</td>
	<td align="right">3.99	</td>
	<td align="right">494	</td>
	<td align="right">3.26	</td>
	<td align="right">62,191.79	</td>
	<td align="right">7,773.97	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-12-14</th>
	<td align="right">8,781	</td>
	<td align="right">2.63	</td>
	<td align="right">468,114,776	</td>
	<td align="right">2.78	</td>
	<td align="right">369	</td>
	<td align="right">2.43	</td>
	<td align="right">43,343.96	</td>
	<td align="right">5,418.00	</td>
</tr>
<tr class="udda">
	<th align="right">2019-12-15</th>
	<td align="right">10,223	</td>
	<td align="right">3.06	</td>
	<td align="right">543,799,205	</td>
	<td align="right">3.23	</td>
	<td align="right">450	</td>
	<td align="right">2.97	</td>
	<td align="right">50,351.78	</td>
	<td align="right">6,293.97	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-12-16</th>
	<td align="right">10,996	</td>
	<td align="right">3.30	</td>
	<td align="right">591,840,214	</td>
	<td align="right">3.52	</td>
	<td align="right">487	</td>
	<td align="right">3.21	</td>
	<td align="right">54,800.02	</td>
	<td align="right">6,850.00	</td>
</tr>
<tr class="udda">
	<th align="right">2019-12-17</th>
	<td align="right">11,588	</td>
	<td align="right">3.47	</td>
	<td align="right">501,354,466	</td>
	<td align="right">2.98	</td>
	<td align="right">508	</td>
	<td align="right">3.35	</td>
	<td align="right">46,421.71	</td>
	<td align="right">5,802.71	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-12-18</th>
	<td align="right">10,692	</td>
	<td align="right">3.20	</td>
	<td align="right">503,063,275	</td>
	<td align="right">2.99	</td>
	<td align="right">501	</td>
	<td align="right">3.31	</td>
	<td align="right">46,579.93	</td>
	<td align="right">5,822.49	</td>
</tr>
<tr class="udda">
	<th align="right">2019-12-19</th>
	<td align="right">10,671	</td>
	<td align="right">3.20	</td>
	<td align="right">489,522,291	</td>
	<td align="right">2.91	</td>
	<td align="right">466	</td>
	<td align="right">3.07	</td>
	<td align="right">45,326.14	</td>
	<td align="right">5,665.77	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-12-20</th>
	<td align="right">10,330	</td>
	<td align="right">3.10	</td>
	<td align="right">424,405,766	</td>
	<td align="right">2.52	</td>
	<td align="right">426	</td>
	<td align="right">2.81	</td>
	<td align="right">39,296.83	</td>
	<td align="right">4,912.10	</td>
</tr>
<tr class="udda">
	<th align="right">2019-12-21</th>
	<td align="right">8,212	</td>
	<td align="right">2.46	</td>
	<td align="right">330,474,668	</td>
	<td align="right">1.96	</td>
	<td align="right">369	</td>
	<td align="right">2.43	</td>
	<td align="right">30,599.51	</td>
	<td align="right">3,824.94	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-12-22</th>
	<td align="right">8,644	</td>
	<td align="right">2.59	</td>
	<td align="right">372,020,127	</td>
	<td align="right">2.21	</td>
	<td align="right">359	</td>
	<td align="right">2.37	</td>
	<td align="right">34,446.31	</td>
	<td align="right">4,305.79	</td>
</tr>
<tr class="udda">
	<th align="right">2019-12-23</th>
	<td align="right">9,377	</td>
	<td align="right">2.81	</td>
	<td align="right">463,280,911	</td>
	<td align="right">2.75	</td>
	<td align="right">315	</td>
	<td align="right">2.08	</td>
	<td align="right">42,896.38	</td>
	<td align="right">5,362.05	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-12-24</th>
	<td align="right">8,873	</td>
	<td align="right">2.66	</td>
	<td align="right">387,548,607	</td>
	<td align="right">2.30	</td>
	<td align="right">395	</td>
	<td align="right">2.61	</td>
	<td align="right">35,884.13	</td>
	<td align="right">4,485.52	</td>
</tr>
<tr class="udda">
	<th align="right">2019-12-25</th>
	<td align="right">7,978	</td>
	<td align="right">2.39	</td>
	<td align="right">243,176,488	</td>
	<td align="right">1.44	</td>
	<td align="right">366	</td>
	<td align="right">2.41	</td>
	<td align="right">22,516.34	</td>
	<td align="right">2,814.54	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-12-26</th>
	<td align="right">9,127	</td>
	<td align="right">2.74	</td>
	<td align="right">438,593,319	</td>
	<td align="right">2.60	</td>
	<td align="right">347	</td>
	<td align="right">2.29	</td>
	<td align="right">40,610.49	</td>
	<td align="right">5,076.31	</td>
</tr>
<tr class="udda">
	<th align="right">2019-12-27</th>
	<td align="right">9,739	</td>
	<td align="right">2.92	</td>
	<td align="right">467,087,447	</td>
	<td align="right">2.77	</td>
	<td align="right">493	</td>
	<td align="right">3.25	</td>
	<td align="right">43,248.84	</td>
	<td align="right">5,406.10	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-12-28</th>
	<td align="right">10,121	</td>
	<td align="right">3.03	</td>
	<td align="right">538,836,987	</td>
	<td align="right">3.20	</td>
	<td align="right">495	</td>
	<td align="right">3.27	</td>
	<td align="right">49,892.31	</td>
	<td align="right">6,236.54	</td>
</tr>
<tr class="udda">
	<th align="right">2019-12-29</th>
	<td align="right">9,201	</td>
	<td align="right">2.76	</td>
	<td align="right">439,323,225	</td>
	<td align="right">2.61	</td>
	<td align="right">371	</td>
	<td align="right">2.45	</td>
	<td align="right">40,678.08	</td>
	<td align="right">5,084.76	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-12-30</th>
	<td align="right">7,897	</td>
	<td align="right">2.37	</td>
	<td align="right">301,611,407	</td>
	<td align="right">1.79	</td>
	<td align="right">413	</td>
	<td align="right">2.72	</td>
	<td align="right">27,926.98	</td>
	<td align="right">3,490.87	</td>
</tr>
<tr class="udda">
	<th align="right">2019-12-31</th>
	<td align="right">7,631	</td>
	<td align="right">2.29	</td>
	<td align="right">299,443,168	</td>
	<td align="right">1.78	</td>
	<td align="right">391	</td>
	<td align="right">2.58	</td>
	<td align="right">27,726.22	</td>
	<td align="right">3,465.78	</td>
</tr>

</tbody>
</table>

