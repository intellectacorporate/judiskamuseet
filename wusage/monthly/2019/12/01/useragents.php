


<h2>Statistics for http://www.u5645470.fsdata.se, Month of 2019-12-1 to 2019-12-31</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 10 of 398 Browsers (User Agents),
Sorted by Access Count

</h3>
<p></p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Product</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th class="hogerstalld">1</th>
	<td>
	Pingdom.com_bot_version_1.4_		
	</td>
	<td align="right">130,931</td>
	<td align="right">39.29</td>
	<td align="right">1,076,851,995</td>
	<td align="right">6.41</td>
	<td align="right">134</td>
	<td align="right">0.89</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	Safari/537.36		
	</td>
	<td align="right">69,063</td>
	<td align="right">20.73</td>
	<td align="right">6,092,168,888</td>
	<td align="right">36.28</td>
	<td align="right">3,548</td>
	<td align="right">23.48</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	Safari/604.1		
	</td>
	<td align="right">40,728</td>
	<td align="right">12.22</td>
	<td align="right">3,962,258,986</td>
	<td align="right">23.60</td>
	<td align="right">2,435</td>
	<td align="right">16.11</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	Mozilla/5.0 		
	</td>
	<td align="right">22,712</td>
	<td align="right">6.82</td>
	<td align="right">1,336,648,048</td>
	<td align="right">7.96</td>
	<td align="right">3,520</td>
	<td align="right">23.29</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	Safari/605.1.15		
	</td>
	<td align="right">13,420</td>
	<td align="right">4.03</td>
	<td align="right">1,186,712,946</td>
	<td align="right">7.07</td>
	<td align="right">768</td>
	<td align="right">5.09</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	UptimeRobot/2.0		
	</td>
	<td align="right">6,038</td>
	<td align="right">1.81</td>
	<td align="right">274,988</td>
	<td align="right">0.00</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	bingbot/2.0		
	</td>
	<td align="right">4,793</td>
	<td align="right">1.44</td>
	<td align="right">228,309,994</td>
	<td align="right">1.36</td>
	<td align="right">1,054</td>
	<td align="right">6.98</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	curl/7.54.0		
	</td>
	<td align="right">4,789</td>
	<td align="right">1.44</td>
	<td align="right">49,240,788</td>
	<td align="right">0.29</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	Safari/537.36 Edge/18.18362		
	</td>
	<td align="right">3,379</td>
	<td align="right">1.01</td>
	<td align="right">379,198,560</td>
	<td align="right">2.26</td>
	<td align="right">134</td>
	<td align="right">0.89</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">10</th>
	<td>
	Safari/537.36 (compatible; Googlebot/2.1;  http://www.google.com/bot.html)		
	</td>
	<td align="right">3,300</td>
	<td align="right">0.99</td>
	<td align="right">34,972,574</td>
	<td align="right">0.21</td>
	<td align="right">715</td>
	<td align="right">4.73</td>
</tr>

</tbody>
</table>

