


<h2>Statistics for http://www.u5645470.fsdata.se, Month of 2019-5-1 to 2019-5-31</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Traffic by Day</h3>
<p>The continuous line represents a 7-day moving average.</p>
<img src="d.png" width="468" height="351">
<br /><br />


<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Date</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	<th scope='col'>Bits per Sec</th>
	<th scope='col'>Bytes per Sec</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th align="right">2019-5-1</th>
	<td align="right">4,648	</td>
	<td align="right">1.66	</td>
	<td align="right">1,294,338,564	</td>
	<td align="right">1.80	</td>
	<td align="right">274	</td>
	<td align="right">2.35	</td>
	<td align="right">119,846.16	</td>
	<td align="right">14,980.77	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-5-2</th>
	<td align="right">7,609	</td>
	<td align="right">2.71	</td>
	<td align="right">1,966,791,368	</td>
	<td align="right">2.74	</td>
	<td align="right">336	</td>
	<td align="right">2.88	</td>
	<td align="right">182,110.31	</td>
	<td align="right">22,763.79	</td>
</tr>
<tr class="udda">
	<th align="right">2019-5-3</th>
	<td align="right">6,916	</td>
	<td align="right">2.47	</td>
	<td align="right">2,476,322,726	</td>
	<td align="right">3.45	</td>
	<td align="right">316	</td>
	<td align="right">2.71	</td>
	<td align="right">229,289.14	</td>
	<td align="right">28,661.14	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-5-4</th>
	<td align="right">3,202	</td>
	<td align="right">1.14	</td>
	<td align="right">811,183,760	</td>
	<td align="right">1.13	</td>
	<td align="right">205	</td>
	<td align="right">1.76	</td>
	<td align="right">75,109.61	</td>
	<td align="right">9,388.70	</td>
</tr>
<tr class="udda">
	<th align="right">2019-5-5</th>
	<td align="right">4,913	</td>
	<td align="right">1.75	</td>
	<td align="right">1,379,512,637	</td>
	<td align="right">1.92	</td>
	<td align="right">259	</td>
	<td align="right">2.22	</td>
	<td align="right">127,732.65	</td>
	<td align="right">15,966.58	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-5-6</th>
	<td align="right">9,224	</td>
	<td align="right">3.29	</td>
	<td align="right">3,007,970,931	</td>
	<td align="right">4.19	</td>
	<td align="right">429	</td>
	<td align="right">3.68	</td>
	<td align="right">278,515.83	</td>
	<td align="right">34,814.48	</td>
</tr>
<tr class="udda">
	<th align="right">2019-5-7</th>
	<td align="right">7,499	</td>
	<td align="right">2.67	</td>
	<td align="right">2,267,794,689	</td>
	<td align="right">3.16	</td>
	<td align="right">355	</td>
	<td align="right">3.05	</td>
	<td align="right">209,980.99	</td>
	<td align="right">26,247.62	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-5-8</th>
	<td align="right">9,192	</td>
	<td align="right">3.28	</td>
	<td align="right">2,384,775,554	</td>
	<td align="right">3.32	</td>
	<td align="right">445	</td>
	<td align="right">3.82	</td>
	<td align="right">220,812.55	</td>
	<td align="right">27,601.57	</td>
</tr>
<tr class="udda">
	<th align="right">2019-5-9</th>
	<td align="right">10,693	</td>
	<td align="right">3.81	</td>
	<td align="right">2,927,836,168	</td>
	<td align="right">4.08	</td>
	<td align="right">712	</td>
	<td align="right">6.11	</td>
	<td align="right">271,095.94	</td>
	<td align="right">33,886.99	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-5-10</th>
	<td align="right">6,916	</td>
	<td align="right">2.47	</td>
	<td align="right">2,223,166,737	</td>
	<td align="right">3.10	</td>
	<td align="right">431	</td>
	<td align="right">3.70	</td>
	<td align="right">205,848.77	</td>
	<td align="right">25,731.10	</td>
</tr>
<tr class="udda">
	<th align="right">2019-5-11</th>
	<td align="right">4,507	</td>
	<td align="right">1.61	</td>
	<td align="right">1,239,669,078	</td>
	<td align="right">1.73	</td>
	<td align="right">305	</td>
	<td align="right">2.62	</td>
	<td align="right">114,784.17	</td>
	<td align="right">14,348.02	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-5-12</th>
	<td align="right">4,266	</td>
	<td align="right">1.52	</td>
	<td align="right">995,938,203	</td>
	<td align="right">1.39	</td>
	<td align="right">299	</td>
	<td align="right">2.56	</td>
	<td align="right">92,216.50	</td>
	<td align="right">11,527.06	</td>
</tr>
<tr class="udda">
	<th align="right">2019-5-13</th>
	<td align="right">5,993	</td>
	<td align="right">2.14	</td>
	<td align="right">2,089,771,049	</td>
	<td align="right">2.91	</td>
	<td align="right">314	</td>
	<td align="right">2.69	</td>
	<td align="right">193,497.32	</td>
	<td align="right">24,187.16	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-5-14</th>
	<td align="right">10,043	</td>
	<td align="right">3.58	</td>
	<td align="right">3,493,956,564	</td>
	<td align="right">4.86	</td>
	<td align="right">405	</td>
	<td align="right">3.47	</td>
	<td align="right">323,514.50	</td>
	<td align="right">40,439.31	</td>
</tr>
<tr class="udda">
	<th align="right">2019-5-15</th>
	<td align="right">9,819	</td>
	<td align="right">3.50	</td>
	<td align="right">3,215,908,737	</td>
	<td align="right">4.48	</td>
	<td align="right">415	</td>
	<td align="right">3.56	</td>
	<td align="right">297,769.33	</td>
	<td align="right">37,221.17	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-5-16</th>
	<td align="right">10,330	</td>
	<td align="right">3.68	</td>
	<td align="right">3,430,041,955	</td>
	<td align="right">4.78	</td>
	<td align="right">388	</td>
	<td align="right">3.33	</td>
	<td align="right">317,596.48	</td>
	<td align="right">39,699.56	</td>
</tr>
<tr class="udda">
	<th align="right">2019-5-17</th>
	<td align="right">8,814	</td>
	<td align="right">3.14	</td>
	<td align="right">2,521,974,889	</td>
	<td align="right">3.51	</td>
	<td align="right">401	</td>
	<td align="right">3.44	</td>
	<td align="right">233,516.19	</td>
	<td align="right">29,189.52	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-5-18</th>
	<td align="right">5,239	</td>
	<td align="right">1.87	</td>
	<td align="right">1,686,977,952	</td>
	<td align="right">2.35	</td>
	<td align="right">246	</td>
	<td align="right">2.11	</td>
	<td align="right">156,201.66	</td>
	<td align="right">19,525.21	</td>
</tr>
<tr class="udda">
	<th align="right">2019-5-19</th>
	<td align="right">5,183	</td>
	<td align="right">1.85	</td>
	<td align="right">1,503,706,120	</td>
	<td align="right">2.09	</td>
	<td align="right">247	</td>
	<td align="right">2.12	</td>
	<td align="right">139,232.05	</td>
	<td align="right">17,404.01	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-5-20</th>
	<td align="right">7,124	</td>
	<td align="right">2.54	</td>
	<td align="right">2,348,985,602	</td>
	<td align="right">3.27	</td>
	<td align="right">388	</td>
	<td align="right">3.33	</td>
	<td align="right">217,498.67	</td>
	<td align="right">27,187.33	</td>
</tr>
<tr class="udda">
	<th align="right">2019-5-21</th>
	<td align="right">13,332	</td>
	<td align="right">4.75	</td>
	<td align="right">2,511,931,540	</td>
	<td align="right">3.50	</td>
	<td align="right">400	</td>
	<td align="right">3.43	</td>
	<td align="right">232,586.25	</td>
	<td align="right">29,073.28	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-5-22</th>
	<td align="right">15,894	</td>
	<td align="right">5.67	</td>
	<td align="right">2,680,666,168	</td>
	<td align="right">3.73	</td>
	<td align="right">370	</td>
	<td align="right">3.17	</td>
	<td align="right">248,209.83	</td>
	<td align="right">31,026.23	</td>
</tr>
<tr class="udda">
	<th align="right">2019-5-23</th>
	<td align="right">11,664	</td>
	<td align="right">4.16	</td>
	<td align="right">2,193,266,899	</td>
	<td align="right">3.05	</td>
	<td align="right">366	</td>
	<td align="right">3.14	</td>
	<td align="right">203,080.27	</td>
	<td align="right">25,385.03	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-5-24</th>
	<td align="right">6,745	</td>
	<td align="right">2.41	</td>
	<td align="right">1,740,737,817	</td>
	<td align="right">2.42	</td>
	<td align="right">337	</td>
	<td align="right">2.89	</td>
	<td align="right">161,179.43	</td>
	<td align="right">20,147.43	</td>
</tr>
<tr class="udda">
	<th align="right">2019-5-25</th>
	<td align="right">5,060	</td>
	<td align="right">1.80	</td>
	<td align="right">1,138,709,044	</td>
	<td align="right">1.59	</td>
	<td align="right">259	</td>
	<td align="right">2.22	</td>
	<td align="right">105,436.02	</td>
	<td align="right">13,179.50	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-5-26</th>
	<td align="right">7,467	</td>
	<td align="right">2.66	</td>
	<td align="right">1,824,271,322	</td>
	<td align="right">2.54	</td>
	<td align="right">381	</td>
	<td align="right">3.27	</td>
	<td align="right">168,914.01	</td>
	<td align="right">21,114.25	</td>
</tr>
<tr class="udda">
	<th align="right">2019-5-27</th>
	<td align="right">15,386	</td>
	<td align="right">5.49	</td>
	<td align="right">2,765,659,503	</td>
	<td align="right">3.85	</td>
	<td align="right">494	</td>
	<td align="right">4.24	</td>
	<td align="right">256,079.58	</td>
	<td align="right">32,009.95	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-5-28</th>
	<td align="right">13,101	</td>
	<td align="right">4.67	</td>
	<td align="right">3,547,730,942	</td>
	<td align="right">4.94	</td>
	<td align="right">543	</td>
	<td align="right">4.66	</td>
	<td align="right">328,493.61	</td>
	<td align="right">41,061.70	</td>
</tr>
<tr class="udda">
	<th align="right">2019-5-29</th>
	<td align="right">20,062	</td>
	<td align="right">7.15	</td>
	<td align="right">4,276,996,201	</td>
	<td align="right">5.95	</td>
	<td align="right">486	</td>
	<td align="right">4.17	</td>
	<td align="right">396,018.17	</td>
	<td align="right">49,502.27	</td>
</tr>
<tr class="jamn">
	<th align="right">2019-5-30</th>
	<td align="right">10,531	</td>
	<td align="right">3.76	</td>
	<td align="right">3,160,916,647	</td>
	<td align="right">4.40	</td>
	<td align="right">452	</td>
	<td align="right">3.88	</td>
	<td align="right">292,677.47	</td>
	<td align="right">36,584.68	</td>
</tr>
<tr class="udda">
	<th align="right">2019-5-31</th>
	<td align="right">19,052	</td>
	<td align="right">6.79	</td>
	<td align="right">2,715,420,629	</td>
	<td align="right">3.78	</td>
	<td align="right">400	</td>
	<td align="right">3.43	</td>
	<td align="right">251,427.84	</td>
	<td align="right">31,428.48	</td>
</tr>

</tbody>
</table>

