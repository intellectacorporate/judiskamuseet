


<h2>Statistics for http://www.u5645470.fsdata.se, Month of 2019-5-1 to 2019-5-31</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 10 of 429 Browsers (User Agents),
Sorted by Access Count

</h3>
<p></p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Product</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th class="hogerstalld">1</th>
	<td>
	Safari/537.36		
	</td>
	<td align="right">84,031</td>
	<td align="right">30.01</td>
	<td align="right">28,428,511,470</td>
	<td align="right">39.59</td>
	<td align="right">3,613</td>
	<td align="right">31.04</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	Mozilla/5.0 		
	</td>
	<td align="right">54,031</td>
	<td align="right">19.30</td>
	<td align="right">9,655,418,732</td>
	<td align="right">13.45</td>
	<td align="right">1,848</td>
	<td align="right">15.88</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	Safari/604.1		
	</td>
	<td align="right">44,495</td>
	<td align="right">15.89</td>
	<td align="right">13,914,093,917</td>
	<td align="right">19.38</td>
	<td align="right">1,892</td>
	<td align="right">16.25</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	Safari/537.36 Edge/18.17763		
	</td>
	<td align="right">23,811</td>
	<td align="right">8.50</td>
	<td align="right">3,579,668,180</td>
	<td align="right">4.99</td>
	<td align="right">125</td>
	<td align="right">1.08</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	Safari/605.1.15		
	</td>
	<td align="right">10,570</td>
	<td align="right">3.78</td>
	<td align="right">5,092,546,859</td>
	<td align="right">7.09</td>
	<td align="right">407</td>
	<td align="right">3.50</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	Safari/537.36 Edge/17.17134		
	</td>
	<td align="right">8,649</td>
	<td align="right">3.09</td>
	<td align="right">2,675,274,336</td>
	<td align="right">3.73</td>
	<td align="right">177</td>
	<td align="right">1.52</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	UptimeRobot/2.0		
	</td>
	<td align="right">6,985</td>
	<td align="right">2.49</td>
	<td align="right">817,144</td>
	<td align="right">0.00</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	Safari/605.1		
	</td>
	<td align="right">5,831</td>
	<td align="right">2.08</td>
	<td align="right">1,733,484,937</td>
	<td align="right">2.41</td>
	<td align="right">247</td>
	<td align="right">2.12</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	bingbot/2.0		
	</td>
	<td align="right">5,304</td>
	<td align="right">1.89</td>
	<td align="right">638,410,609</td>
	<td align="right">0.89</td>
	<td align="right">1,025</td>
	<td align="right">8.81</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">10</th>
	<td>
	WordPress/5.1.1		
	</td>
	<td align="right">4,164</td>
	<td align="right">1.49</td>
	<td align="right">71,265</td>
	<td align="right">0.00</td>
	<td align="right">73</td>
	<td align="right">0.63</td>
</tr>

</tbody>
</table>

