


<h2>Statistics for http://www.u5645470.fsdata.se, Month of 2019-5-1 to 2019-5-31</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 20 of 9,561 Visitor Sites,
Sorted by Access Count

</h3>
<p></p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Site</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="jamnudda">
	<th class="hogerstalld">1</th>
	<td>
	46.59.25.88		
	</td>
	<td align="right">27,062</td>
	<td align="right">9.65</td>
	<td align="right">3,371,812,550</td>
	<td align="right">4.69</td>
	<td align="right">132</td>
	<td align="right">1.14</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	5.35.187.69		
	</td>
	<td align="right">22,156</td>
	<td align="right">7.90</td>
	<td align="right">770,525,990</td>
	<td align="right">1.07</td>
	<td align="right">39</td>
	<td align="right">0.34</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	63.143.42.243		
	</td>
	<td align="right">5,615</td>
	<td align="right">2.00</td>
	<td align="right">604,944</td>
	<td align="right">0.00</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	89.221.250.27		
	</td>
	<td align="right">4,851</td>
	<td align="right">1.73</td>
	<td align="right">54,693</td>
	<td align="right">0.00</td>
	<td align="right">85</td>
	<td align="right">0.73</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	unknown		
	</td>
	<td align="right">1,542</td>
	<td align="right">0.55</td>
	<td align="right">51,185,946</td>
	<td align="right">0.07</td>
	<td align="right">117</td>
	<td align="right">1.01</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	81.170.246.9		
	</td>
	<td align="right">1,377</td>
	<td align="right">0.49</td>
	<td align="right">399,636,966</td>
	<td align="right">0.56</td>
	<td align="right">16</td>
	<td align="right">0.14</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	63.143.42.244		
	</td>
	<td align="right">1,367</td>
	<td align="right">0.49</td>
	<td align="right">84,165</td>
	<td align="right">0.00</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	213.212.29.165		
	</td>
	<td align="right">1,235</td>
	<td align="right">0.44</td>
	<td align="right">56,586,222</td>
	<td align="right">0.08</td>
	<td align="right">4</td>
	<td align="right">0.04</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	79.142.244.252		
	</td>
	<td align="right">969</td>
	<td align="right">0.35</td>
	<td align="right">717,124,827</td>
	<td align="right">1.00</td>
	<td align="right">38</td>
	<td align="right">0.33</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">10</th>
	<td>
	66.249.79.186		
	</td>
	<td align="right">909</td>
	<td align="right">0.32</td>
	<td align="right">43,236,864</td>
	<td align="right">0.06</td>
	<td align="right">130</td>
	<td align="right">1.12</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">11</th>
	<td>
	185.15.93.69		
	</td>
	<td align="right">831</td>
	<td align="right">0.30</td>
	<td align="right">12,650,798</td>
	<td align="right">0.02</td>
	<td align="right">47</td>
	<td align="right">0.40</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">12</th>
	<td>
	66.249.79.180		
	</td>
	<td align="right">755</td>
	<td align="right">0.27</td>
	<td align="right">24,830,643</td>
	<td align="right">0.03</td>
	<td align="right">111</td>
	<td align="right">0.95</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">13</th>
	<td>
	34.227.118.221		
	</td>
	<td align="right">732</td>
	<td align="right">0.26</td>
	<td align="right">246,484,910</td>
	<td align="right">0.34</td>
	<td align="right">3</td>
	<td align="right">0.03</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">14</th>
	<td>
	17.58.99.252		
	</td>
	<td align="right">725</td>
	<td align="right">0.26</td>
	<td align="right">7,295,972</td>
	<td align="right">0.01</td>
	<td align="right">186</td>
	<td align="right">1.60</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">15</th>
	<td>
	66.249.79.188		
	</td>
	<td align="right">675</td>
	<td align="right">0.24</td>
	<td align="right">88,293,779</td>
	<td align="right">0.12</td>
	<td align="right">111</td>
	<td align="right">0.95</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">16</th>
	<td>
	23.100.232.233		
	</td>
	<td align="right">666</td>
	<td align="right">0.24</td>
	<td align="right">10,136,420</td>
	<td align="right">0.01</td>
	<td align="right">46</td>
	<td align="right">0.40</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">17</th>
	<td>
	194.111.119.48		
	</td>
	<td align="right">654</td>
	<td align="right">0.23</td>
	<td align="right">953,016,320</td>
	<td align="right">1.33</td>
	<td align="right">12</td>
	<td align="right">0.10</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">18</th>
	<td>
	216.244.66.227		
	</td>
	<td align="right">640</td>
	<td align="right">0.23</td>
	<td align="right">114,860,347</td>
	<td align="right">0.16</td>
	<td align="right">107</td>
	<td align="right">0.92</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">19</th>
	<td>
	66.249.79.182		
	</td>
	<td align="right">590</td>
	<td align="right">0.21</td>
	<td align="right">66,602,445</td>
	<td align="right">0.09</td>
	<td align="right">89</td>
	<td align="right">0.77</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">20</th>
	<td>
	212.37.28.30		
	</td>
	<td align="right">545</td>
	<td align="right">0.19</td>
	<td align="right">169,070,839</td>
	<td align="right">0.24</td>
	<td align="right">17</td>
	<td align="right">0.15</td>
</tr>

</tbody>
</table>

