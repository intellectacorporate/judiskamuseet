


<h2>Statistics for http://www.u5645470.fsdata.se, Month of 2019-5-1 to 2019-5-31</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 10 of 13 Operating Systems,
Sorted by Access Count

</h3>
<p></p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Product</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th class="hogerstalld">1</th>
	<td>
	Mac OS X		
	</td>
	<td align="right">228,908</td>
	<td align="right">81.76</td>
	<td align="right">63,230,400,404</td>
	<td align="right">88.06</td>
	<td align="right">8,134</td>
	<td align="right">69.77</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	Other		
	</td>
	<td align="right">30,752</td>
	<td align="right">10.98</td>
	<td align="right">2,574,099,310</td>
	<td align="right">3.58</td>
	<td align="right">2,137</td>
	<td align="right">18.33</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	Windows NT		
	</td>
	<td align="right">13,576</td>
	<td align="right">4.85</td>
	<td align="right">5,691,787,319</td>
	<td align="right">7.93</td>
	<td align="right">487</td>
	<td align="right">4.18</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	Linux		
	</td>
	<td align="right">5,121</td>
	<td align="right">1.83</td>
	<td align="right">208,477,629</td>
	<td align="right">0.29</td>
	<td align="right">740</td>
	<td align="right">6.35</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	Windows Codename Longhorn		
	</td>
	<td align="right">1,208</td>
	<td align="right">0.43</td>
	<td align="right">96,637,821</td>
	<td align="right">0.13</td>
	<td align="right">145</td>
	<td align="right">1.25</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	Windows XP		
	</td>
	<td align="right">278</td>
	<td align="right">0.10</td>
	<td align="right">4,118,605</td>
	<td align="right">0.01</td>
	<td align="right">13</td>
	<td align="right">0.11</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	Windows 2003		
	</td>
	<td align="right">68</td>
	<td align="right">0.02</td>
	<td align="right">22,716</td>
	<td align="right">0.00</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	Windows 2000		
	</td>
	<td align="right">42</td>
	<td align="right">0.02</td>
	<td align="right">199,156</td>
	<td align="right">0.00</td>
	<td align="right">1</td>
	<td align="right">0.01</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	Windows 98		
	</td>
	<td align="right">10</td>
	<td align="right">0.00</td>
	<td align="right">64,392</td>
	<td align="right">0.00</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">10</th>
	<td>
	Windows 95		
	</td>
	<td align="right">9</td>
	<td align="right">0.00</td>
	<td align="right">2,538</td>
	<td align="right">0.00</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>

</tbody>
</table>

