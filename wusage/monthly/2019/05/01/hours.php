


<h2>Statistics for http://www.u5645470.fsdata.se, Month of 2019-5-1 to 2019-5-31</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Traffic by Hour</h3>
<p>Figures are averages for that hour
	of each day in the time period analyzed.</p>
<img src="h.png" width="468" height="351">
<br /><br />
<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Hour</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	<th scope='col'>Bits per Sec</th>
	<th scope='col'>Bytes per Sec</th>
	
	</tr>
</thead>
<tbody>
<tr class="jamnudda">
	<th align="right">00:00</th>
	<td align="right">156.00</td>
	<td align="right">1.72</td>
	<td align="right">39,687,968.87</td>
	<td align="right">1.71</td>
	<td align="right">6.97</td>
	<td align="right">1.85</td>
	<td align="right">88,195.49</td>
	<td align="right">11,024.44</td>
	
</tr>
<tr class="jamn">
	<th align="right">01:00</th>
	<td align="right">85.45</td>
	<td align="right">0.94</td>
	<td align="right">14,655,936.39</td>
	<td align="right">0.63</td>
	<td align="right">6.86</td>
	<td align="right">1.82</td>
	<td align="right">32,568.75</td>
	<td align="right">4,071.09</td>
	
</tr>
<tr class="udda">
	<th align="right">02:00</th>
	<td align="right">83.77</td>
	<td align="right">0.93</td>
	<td align="right">12,180,254.03</td>
	<td align="right">0.53</td>
	<td align="right">6.54</td>
	<td align="right">1.74</td>
	<td align="right">27,067.23</td>
	<td align="right">3,383.40</td>
	
</tr>
<tr class="jamn">
	<th align="right">03:00</th>
	<td align="right">108.65</td>
	<td align="right">1.20</td>
	<td align="right">10,197,358.94</td>
	<td align="right">0.44</td>
	<td align="right">6.49</td>
	<td align="right">1.73</td>
	<td align="right">22,660.80</td>
	<td align="right">2,832.60</td>
	
</tr>
<tr class="udda">
	<th align="right">04:00</th>
	<td align="right">89.74</td>
	<td align="right">0.99</td>
	<td align="right">9,166,077.32</td>
	<td align="right">0.40</td>
	<td align="right">6.85</td>
	<td align="right">1.82</td>
	<td align="right">20,369.06</td>
	<td align="right">2,546.13</td>
	
</tr>
<tr class="jamn">
	<th align="right">05:00</th>
	<td align="right">92.94</td>
	<td align="right">1.03</td>
	<td align="right">17,099,566.32</td>
	<td align="right">0.74</td>
	<td align="right">6.09</td>
	<td align="right">1.62</td>
	<td align="right">37,999.04</td>
	<td align="right">4,749.88</td>
	
</tr>
<tr class="udda">
	<th align="right">06:00</th>
	<td align="right">117.52</td>
	<td align="right">1.30</td>
	<td align="right">21,306,767.94</td>
	<td align="right">0.92</td>
	<td align="right">8.45</td>
	<td align="right">2.25</td>
	<td align="right">47,348.37</td>
	<td align="right">5,918.55</td>
	
</tr>
<tr class="jamn">
	<th align="right">07:00</th>
	<td align="right">212.52</td>
	<td align="right">2.35</td>
	<td align="right">54,333,750.13</td>
	<td align="right">2.35</td>
	<td align="right">8.96</td>
	<td align="right">2.38</td>
	<td align="right">120,741.67</td>
	<td align="right">15,092.71</td>
	
</tr>
<tr class="udda">
	<th align="right">08:00</th>
	<td align="right">436.45</td>
	<td align="right">4.82</td>
	<td align="right">109,313,603.84</td>
	<td align="right">4.72</td>
	<td align="right">14.01</td>
	<td align="right">3.73</td>
	<td align="right">242,919.12</td>
	<td align="right">30,364.89</td>
	
</tr>
<tr class="jamn">
	<th align="right">09:00</th>
	<td align="right">573.06</td>
	<td align="right">6.34</td>
	<td align="right">147,557,371.71</td>
	<td align="right">6.37</td>
	<td align="right">21.67</td>
	<td align="right">5.76</td>
	<td align="right">327,905.27</td>
	<td align="right">40,988.16</td>
	
</tr>
<tr class="udda">
	<th align="right">10:00</th>
	<td align="right">714.29</td>
	<td align="right">7.90</td>
	<td align="right">199,643,267.13</td>
	<td align="right">8.62</td>
	<td align="right">23.88</td>
	<td align="right">6.35</td>
	<td align="right">443,651.70</td>
	<td align="right">55,456.46</td>
	
</tr>
<tr class="jamn">
	<th align="right">11:00</th>
	<td align="right">627.77</td>
	<td align="right">6.94</td>
	<td align="right">173,609,820.39</td>
	<td align="right">7.49</td>
	<td align="right">21.66</td>
	<td align="right">5.76</td>
	<td align="right">385,799.60</td>
	<td align="right">48,224.95</td>
	
</tr>
<tr class="udda">
	<th align="right">12:00</th>
	<td align="right">539.06</td>
	<td align="right">5.96</td>
	<td align="right">133,007,671.26</td>
	<td align="right">5.74</td>
	<td align="right">21.10</td>
	<td align="right">5.61</td>
	<td align="right">295,572.60</td>
	<td align="right">36,946.58</td>
	
</tr>
<tr class="jamn">
	<th align="right">13:00</th>
	<td align="right">738.52</td>
	<td align="right">8.16</td>
	<td align="right">169,814,008.19</td>
	<td align="right">7.33</td>
	<td align="right">24.04</td>
	<td align="right">6.39</td>
	<td align="right">377,364.46</td>
	<td align="right">47,170.56</td>
	
</tr>
<tr class="udda">
	<th align="right">14:00</th>
	<td align="right">632.45</td>
	<td align="right">6.99</td>
	<td align="right">176,401,763.19</td>
	<td align="right">7.61</td>
	<td align="right">21.03</td>
	<td align="right">5.59</td>
	<td align="right">392,003.92</td>
	<td align="right">49,000.49</td>
	
</tr>
<tr class="jamn">
	<th align="right">15:00</th>
	<td align="right">672.32</td>
	<td align="right">7.43</td>
	<td align="right">163,268,130.74</td>
	<td align="right">7.05</td>
	<td align="right">20.88</td>
	<td align="right">5.55</td>
	<td align="right">362,818.07</td>
	<td align="right">45,352.26</td>
	
</tr>
<tr class="udda">
	<th align="right">16:00</th>
	<td align="right">539.29</td>
	<td align="right">5.96</td>
	<td align="right">132,203,579.45</td>
	<td align="right">5.71</td>
	<td align="right">20.63</td>
	<td align="right">5.49</td>
	<td align="right">293,785.73</td>
	<td align="right">36,723.22</td>
	
</tr>
<tr class="jamn">
	<th align="right">17:00</th>
	<td align="right">511.26</td>
	<td align="right">5.65</td>
	<td align="right">112,464,349.42</td>
	<td align="right">4.85</td>
	<td align="right">19.34</td>
	<td align="right">5.14</td>
	<td align="right">249,920.78</td>
	<td align="right">31,240.10</td>
	
</tr>
<tr class="udda">
	<th align="right">18:00</th>
	<td align="right">410.13</td>
	<td align="right">4.53</td>
	<td align="right">114,643,064.39</td>
	<td align="right">4.95</td>
	<td align="right">19.84</td>
	<td align="right">5.28</td>
	<td align="right">254,762.37</td>
	<td align="right">31,845.30</td>
	
</tr>
<tr class="jamn">
	<th align="right">19:00</th>
	<td align="right">364.10</td>
	<td align="right">4.02</td>
	<td align="right">105,764,271.26</td>
	<td align="right">4.56</td>
	<td align="right">18.03</td>
	<td align="right">4.79</td>
	<td align="right">235,031.71</td>
	<td align="right">29,378.96</td>
	
</tr>
<tr class="udda">
	<th align="right">20:00</th>
	<td align="right">369.68</td>
	<td align="right">4.09</td>
	<td align="right">115,148,481.45</td>
	<td align="right">4.97</td>
	<td align="right">18.50</td>
	<td align="right">4.92</td>
	<td align="right">255,885.51</td>
	<td align="right">31,985.69</td>
	
</tr>
<tr class="jamn">
	<th align="right">21:00</th>
	<td align="right">405.00</td>
	<td align="right">4.48</td>
	<td align="right">115,952,285.84</td>
	<td align="right">5.00</td>
	<td align="right">19.00</td>
	<td align="right">5.05</td>
	<td align="right">257,671.75</td>
	<td align="right">32,208.97</td>
	
</tr>
<tr class="udda">
	<th align="right">22:00</th>
	<td align="right">294.23</td>
	<td align="right">3.25</td>
	<td align="right">88,226,782.10</td>
	<td align="right">3.81</td>
	<td align="right">15.80</td>
	<td align="right">4.20</td>
	<td align="right">196,059.52</td>
	<td align="right">24,507.44</td>
	
</tr>
<tr class="jamn">
	<th align="right">23:00</th>
	<td align="right">271.74</td>
	<td align="right">3.00</td>
	<td align="right">81,222,579.23</td>
	<td align="right">3.51</td>
	<td align="right">19.44</td>
	<td align="right">5.17</td>
	<td align="right">180,494.62</td>
	<td align="right">22,561.83</td>
	
</tr>

</tbody>
</table>

