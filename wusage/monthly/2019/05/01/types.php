


<h2>Statistics for http://www.u5645470.fsdata.se, Month of 2019-5-1 to 2019-5-31</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 10 of 30 File Types (Extensions), 
Sorted by Access Count

</h3>
<p>Individual file types as determined by file extensions. All URLs that
do not contain an extension are counted as directories.</p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Type</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th class="hogerstalld">1</th>
	<td>
	js		
	</td>
	<td align="right">74,542</td>
	<td align="right">26.58</td>
	<td align="right">834,223,766</td>
	<td align="right">1.16</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	css		
	</td>
	<td align="right">48,251</td>
	<td align="right">17.21</td>
	<td align="right">781,337,383</td>
	<td align="right">1.09</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	jpg		
	</td>
	<td align="right">46,654</td>
	<td align="right">16.64</td>
	<td align="right">64,189,521,687</td>
	<td align="right">89.37</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	Directory (folder)		
	</td>
	<td align="right">42,747</td>
	<td align="right">15.24</td>
	<td align="right">570,976,034</td>
	<td align="right">0.79</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	png		
	</td>
	<td align="right">36,858</td>
	<td align="right">13.14</td>
	<td align="right">4,101,750,230</td>
	<td align="right">5.71</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	php		
	</td>
	<td align="right">18,029</td>
	<td align="right">6.43</td>
	<td align="right">444,480,972</td>
	<td align="right">0.62</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	ttf		
	</td>
	<td align="right">6,274</td>
	<td align="right">2.24</td>
	<td align="right">553,520,781</td>
	<td align="right">0.77</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	txt		
	</td>
	<td align="right">2,908</td>
	<td align="right">1.04</td>
	<td align="right">335,895</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	gif		
	</td>
	<td align="right">1,434</td>
	<td align="right">0.51</td>
	<td align="right">5,732,104</td>
	<td align="right">0.01</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">10</th>
	<td>
	svg		
	</td>
	<td align="right">907</td>
	<td align="right">0.32</td>
	<td align="right">10,556,607</td>
	<td align="right">0.01</td>
</tr>

</tbody>
</table>

