


<h2>Statistics for http://www.u5645470.fsdata.se, Month of 2017-6-1 to 2020-1-31</h2>

	<h3>History from 2017-6-1 to 2020-1-31</h3>
	<p>Values for completed time periods are final. Values for the 
	reporting period currently in progress are projections, except
	for user-calculated functions, which are not scaled.</p>
	
	<p><img src="3t.png" width="468" height="351"></p>
	<table cellspacing='0' class='styladtabell'>
	<caption>Home Page Accesses</caption>
	<thead>
	<tr>
	<th scope="col" class='centrerad'>Report Date</th>
	<th scope="col" class='centrerad'>Accesses</th>
	<th scope="col" class='centrerad'>Bytes</th>
	<th scope="col" class='centrerad'>Visits</th>
	
	</tr>
        </thead>
	<tbody>
	<tr class="jamn">
	<th align="left">Total</th>
	<td align="right">914,523</td>
	<td align="right">7,095,490,179</td>
	<td align="right">153,671</td>	
	
	</tr>
	<tr class="udda">
	<th align="left">Average</th>
	<td align="right">28,139</td>
	<td align="right">218,322,774</td>
	<td align="right">4,728</td>
	
	</tr>
	<tr class="jamn">
	<th align="left">Latest</th>
	<td align="right">93,214.95</td>
	<td align="right">703,332,353</td>
	<td align="right">3,782</td>
	
	</tr>
	<tr class="udda">
	<th align=left>
	17-06-01
	</th>
	<td align=right>1</td>
	<td align=right>668</td>
	<td align=right>0</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	17-07-01
	</th>
	<td align=right>17,308</td>
	<td align=right>120,839,499</td>
	<td align=right>2,891</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	17-08-01
	</th>
	<td align=right>19,269</td>
	<td align=right>145,946,281</td>
	<td align=right>4,123</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	17-09-01
	</th>
	<td align=right>24,365</td>
	<td align=right>270,581,650</td>
	<td align=right>8,512</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	17-10-01
	</th>
	<td align=right>20,508</td>
	<td align=right>90,606,762</td>
	<td align=right>6,493</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	17-11-01
	</th>
	<td align=right>17,430</td>
	<td align=right>89,980,463</td>
	<td align=right>4,716</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	17-12-01
	</th>
	<td align=right>17,070</td>
	<td align=right>87,073,165</td>
	<td align=right>4,110</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	18-01-01
	</th>
	<td align=right>17,554</td>
	<td align=right>89,160,729</td>
	<td align=right>4,156</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	18-02-01
	</th>
	<td align=right>20,530</td>
	<td align=right>144,266,302</td>
	<td align=right>4,776</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	18-03-01
	</th>
	<td align=right>22,439</td>
	<td align=right>115,634,036</td>
	<td align=right>5,778</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	18-04-01
	</th>
	<td align=right>17,079</td>
	<td align=right>72,754,992</td>
	<td align=right>4,306</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	18-05-01
	</th>
	<td align=right>19,010</td>
	<td align=right>174,847,252</td>
	<td align=right>4,987</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	18-06-01
	</th>
	<td align=right>18,177</td>
	<td align=right>176,225,158</td>
	<td align=right>6,208</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	18-07-01
	</th>
	<td align=right>16,159</td>
	<td align=right>163,163,794</td>
	<td align=right>3,960</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	<a target="_top" href="2018/08/01/index.php">
	18-08-01</a>
	
	</th>
	<td align=right>18,651</td>
	<td align=right>251,580,742</td>
	<td align=right>5,640</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	<a target="_top" href="2018/09/01/index.php">
	18-09-01</a>
	
	</th>
	<td align=right>17,299</td>
	<td align=right>145,489,322</td>
	<td align=right>4,404</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	<a target="_top" href="2018/10/01/index.php">
	18-10-01</a>
	
	</th>
	<td align=right>18,820</td>
	<td align=right>178,309,379</td>
	<td align=right>5,321</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	<a target="_top" href="2018/11/01/index.php">
	18-11-01</a>
	
	</th>
	<td align=right>17,679</td>
	<td align=right>133,233,955</td>
	<td align=right>4,412</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	<a target="_top" href="2018/12/01/index.php">
	18-12-01</a>
	
	</th>
	<td align=right>19,230</td>
	<td align=right>139,077,126</td>
	<td align=right>3,729</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	<a target="_top" href="2019/01/01/index.php">
	19-01-01</a>
	
	</th>
	<td align=right>17,038</td>
	<td align=right>118,994,597</td>
	<td align=right>4,252</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	<a target="_top" href="2019/02/01/index.php">
	19-02-01</a>
	
	</th>
	<td align=right>17,254</td>
	<td align=right>102,139,699</td>
	<td align=right>3,998</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	<a target="_top" href="2019/03/01/index.php">
	19-03-01</a>
	
	</th>
	<td align=right>18,078</td>
	<td align=right>105,468,776</td>
	<td align=right>5,088</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	<a target="_top" href="2019/04/01/index.php">
	19-04-01</a>
	
	</th>
	<td align=right>21,040</td>
	<td align=right>119,508,381</td>
	<td align=right>6,781</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	<a target="_top" href="2019/05/01/index.php">
	19-05-01</a>
	
	</th>
	<td align=right>22,220</td>
	<td align=right>194,751,513</td>
	<td align=right>6,664</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	<a target="_top" href="2019/06/01/index.php">
	19-06-01</a>
	
	</th>
	<td align=right>30,230</td>
	<td align=right>259,843,609</td>
	<td align=right>8,677</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	<a target="_top" href="2019/07/01/index.php">
	19-07-01</a>
	
	</th>
	<td align=right>26,316</td>
	<td align=right>195,332,140</td>
	<td align=right>4,991</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	<a target="_top" href="2019/08/01/index.php">
	19-08-01</a>
	
	</th>
	<td align=right>23,953</td>
	<td align=right>189,317,100</td>
	<td align=right>3,993</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	<a target="_top" href="2019/09/01/index.php">
	19-09-01</a>
	
	</th>
	<td align=right>22,976</td>
	<td align=right>193,090,661</td>
	<td align=right>3,527</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	<a target="_top" href="2019/10/01/index.php">
	19-10-01</a>
	
	</th>
	<td align=right>104,180</td>
	<td align=right>859,886,508</td>
	<td align=right>5,573</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	<a target="_top" href="2019/11/01/index.php">
	19-11-01</a>
	
	</th>
	<td align=right>102,177</td>
	<td align=right>863,430,272</td>
	<td align=right>4,796</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	<a target="_top" href="2019/12/01/index.php">
	19-12-01</a>
	
	</th>
	<td align=right>107,230</td>
	<td align=right>827,694,408</td>
	<td align=right>4,229</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	<a target="_top" href="2020/01/01/index.php">
	20-01-01</a>
	
	</th>
	<td align=right>93,214.95</td>
	<td align=right>703,332,353.68</td>
	<td align=right>3,782.51</td>
	
	</tr>

	</tbody>

	</table>
	