


<h2>Statistics for http://www.u5645470.fsdata.se, Month of 2018-10-1 to 2018-10-31</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 10 of 311 Browsers (User Agents),
Sorted by Access Count

</h3>
<p></p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Product</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th class="hogerstalld">1</th>
	<td>
	Safari/537.36		
	</td>
	<td align="right">49,822</td>
	<td align="right">35.71</td>
	<td align="right">14,660,178,794</td>
	<td align="right">46.14</td>
	<td align="right">2,408</td>
	<td align="right">31.23</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	Safari/604.1		
	</td>
	<td align="right">19,391</td>
	<td align="right">13.90</td>
	<td align="right">5,109,996,917</td>
	<td align="right">16.08</td>
	<td align="right">780</td>
	<td align="right">10.11</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	Mozilla/5.0 		
	</td>
	<td align="right">17,870</td>
	<td align="right">12.81</td>
	<td align="right">3,876,537,397</td>
	<td align="right">12.20</td>
	<td align="right">1,950</td>
	<td align="right">25.29</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	Safari/537.36 Edge/17.17134		
	</td>
	<td align="right">7,880</td>
	<td align="right">5.65</td>
	<td align="right">1,795,621,594</td>
	<td align="right">5.65</td>
	<td align="right">141</td>
	<td align="right">1.83</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	UptimeRobot/2.0		
	</td>
	<td align="right">7,826</td>
	<td align="right">5.61</td>
	<td align="right">122,753</td>
	<td align="right">0.00</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	WordPress/4.9.8		
	</td>
	<td align="right">6,846</td>
	<td align="right">4.91</td>
	<td align="right">152,011</td>
	<td align="right">0.00</td>
	<td align="right">84</td>
	<td align="right">1.09</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	Safari/605.1.15		
	</td>
	<td align="right">6,047</td>
	<td align="right">4.33</td>
	<td align="right">2,030,713,874</td>
	<td align="right">6.39</td>
	<td align="right">236</td>
	<td align="right">3.07</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	bingbot/2.0		
	</td>
	<td align="right">4,432</td>
	<td align="right">3.18</td>
	<td align="right">327,733,934</td>
	<td align="right">1.03</td>
	<td align="right">773</td>
	<td align="right">10.03</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	Safari/602.1		
	</td>
	<td align="right">2,285</td>
	<td align="right">1.64</td>
	<td align="right">590,726,945</td>
	<td align="right">1.86</td>
	<td align="right">80</td>
	<td align="right">1.04</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">10</th>
	<td>
	facebookexternalhit/1.1 		
	</td>
	<td align="right">1,559</td>
	<td align="right">1.12</td>
	<td align="right">583,981,857</td>
	<td align="right">1.84</td>
	<td align="right">108</td>
	<td align="right">1.41</td>
</tr>

</tbody>
</table>

