


<h2>Statistics for http://www.u5645470.fsdata.se, Month of 2018-10-1 to 2018-10-31</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Traffic by Hour</h3>
<p>Figures are averages for that hour
	of each day in the time period analyzed.</p>
<img src="h.png" width="468" height="351">
<br /><br />
<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Hour</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	<th scope='col'>Bits per Sec</th>
	<th scope='col'>Bytes per Sec</th>
	
	</tr>
</thead>
<tbody>
<tr class="jamnudda">
	<th align="right">00:00</th>
	<td align="right">87.68</td>
	<td align="right">1.94</td>
	<td align="right">16,224,667.81</td>
	<td align="right">1.58</td>
	<td align="right">3.44</td>
	<td align="right">1.38</td>
	<td align="right">36,054.82</td>
	<td align="right">4,506.85</td>
	
</tr>
<tr class="jamn">
	<th align="right">01:00</th>
	<td align="right">65.87</td>
	<td align="right">1.46</td>
	<td align="right">10,859,278.87</td>
	<td align="right">1.06</td>
	<td align="right">5.35</td>
	<td align="right">2.14</td>
	<td align="right">24,131.73</td>
	<td align="right">3,016.47</td>
	
</tr>
<tr class="udda">
	<th align="right">02:00</th>
	<td align="right">49.23</td>
	<td align="right">1.09</td>
	<td align="right">4,334,230.45</td>
	<td align="right">0.42</td>
	<td align="right">5.39</td>
	<td align="right">2.16</td>
	<td align="right">9,631.62</td>
	<td align="right">1,203.95</td>
	
</tr>
<tr class="jamn">
	<th align="right">03:00</th>
	<td align="right">66.16</td>
	<td align="right">1.46</td>
	<td align="right">9,344,080.29</td>
	<td align="right">0.91</td>
	<td align="right">5.14</td>
	<td align="right">2.06</td>
	<td align="right">20,764.62</td>
	<td align="right">2,595.58</td>
	
</tr>
<tr class="udda">
	<th align="right">04:00</th>
	<td align="right">52.42</td>
	<td align="right">1.16</td>
	<td align="right">3,571,778.19</td>
	<td align="right">0.35</td>
	<td align="right">4.93</td>
	<td align="right">1.97</td>
	<td align="right">7,937.28</td>
	<td align="right">992.16</td>
	
</tr>
<tr class="jamn">
	<th align="right">05:00</th>
	<td align="right">57.58</td>
	<td align="right">1.27</td>
	<td align="right">4,248,511.68</td>
	<td align="right">0.41</td>
	<td align="right">4.49</td>
	<td align="right">1.80</td>
	<td align="right">9,441.14</td>
	<td align="right">1,180.14</td>
	
</tr>
<tr class="udda">
	<th align="right">06:00</th>
	<td align="right">79.29</td>
	<td align="right">1.75</td>
	<td align="right">10,371,767.45</td>
	<td align="right">1.01</td>
	<td align="right">6.01</td>
	<td align="right">2.41</td>
	<td align="right">23,048.37</td>
	<td align="right">2,881.05</td>
	
</tr>
<tr class="jamn">
	<th align="right">07:00</th>
	<td align="right">106.13</td>
	<td align="right">2.35</td>
	<td align="right">20,117,625.06</td>
	<td align="right">1.96</td>
	<td align="right">7.20</td>
	<td align="right">2.88</td>
	<td align="right">44,705.83</td>
	<td align="right">5,588.23</td>
	
</tr>
<tr class="udda">
	<th align="right">08:00</th>
	<td align="right">222.61</td>
	<td align="right">4.92</td>
	<td align="right">59,747,954.35</td>
	<td align="right">5.81</td>
	<td align="right">9.99</td>
	<td align="right">4.00</td>
	<td align="right">132,773.23</td>
	<td align="right">16,596.65</td>
	
</tr>
<tr class="jamn">
	<th align="right">09:00</th>
	<td align="right">229.71</td>
	<td align="right">5.08</td>
	<td align="right">65,296,822.42</td>
	<td align="right">6.35</td>
	<td align="right">12.12</td>
	<td align="right">4.85</td>
	<td align="right">145,104.05</td>
	<td align="right">18,138.01</td>
	
</tr>
<tr class="udda">
	<th align="right">10:00</th>
	<td align="right">356.94</td>
	<td align="right">7.90</td>
	<td align="right">91,657,040.06</td>
	<td align="right">8.92</td>
	<td align="right">15.20</td>
	<td align="right">6.09</td>
	<td align="right">203,682.31</td>
	<td align="right">25,460.29</td>
	
</tr>
<tr class="jamn">
	<th align="right">11:00</th>
	<td align="right">290.39</td>
	<td align="right">6.42</td>
	<td align="right">73,812,864.84</td>
	<td align="right">7.18</td>
	<td align="right">14.64</td>
	<td align="right">5.86</td>
	<td align="right">164,028.59</td>
	<td align="right">20,503.57</td>
	
</tr>
<tr class="udda">
	<th align="right">12:00</th>
	<td align="right">300.10</td>
	<td align="right">6.64</td>
	<td align="right">78,407,864.52</td>
	<td align="right">7.63</td>
	<td align="right">14.93</td>
	<td align="right">5.98</td>
	<td align="right">174,239.70</td>
	<td align="right">21,779.96</td>
	
</tr>
<tr class="jamn">
	<th align="right">13:00</th>
	<td align="right">359.42</td>
	<td align="right">7.95</td>
	<td align="right">93,107,462.45</td>
	<td align="right">9.06</td>
	<td align="right">16.13</td>
	<td align="right">6.46</td>
	<td align="right">206,905.47</td>
	<td align="right">25,863.18</td>
	
</tr>
<tr class="udda">
	<th align="right">14:00</th>
	<td align="right">302.19</td>
	<td align="right">6.69</td>
	<td align="right">74,452,068.71</td>
	<td align="right">7.25</td>
	<td align="right">16.64</td>
	<td align="right">6.66</td>
	<td align="right">165,449.04</td>
	<td align="right">20,681.13</td>
	
</tr>
<tr class="jamn">
	<th align="right">15:00</th>
	<td align="right">337.26</td>
	<td align="right">7.46</td>
	<td align="right">64,799,570.00</td>
	<td align="right">6.31</td>
	<td align="right">14.36</td>
	<td align="right">5.75</td>
	<td align="right">143,999.04</td>
	<td align="right">17,999.88</td>
	
</tr>
<tr class="udda">
	<th align="right">16:00</th>
	<td align="right">233.84</td>
	<td align="right">5.17</td>
	<td align="right">55,527,790.97</td>
	<td align="right">5.40</td>
	<td align="right">12.28</td>
	<td align="right">4.92</td>
	<td align="right">123,395.09</td>
	<td align="right">15,424.39</td>
	
</tr>
<tr class="jamn">
	<th align="right">17:00</th>
	<td align="right">209.94</td>
	<td align="right">4.64</td>
	<td align="right">43,974,834.52</td>
	<td align="right">4.28</td>
	<td align="right">12.74</td>
	<td align="right">5.10</td>
	<td align="right">97,721.85</td>
	<td align="right">12,215.23</td>
	
</tr>
<tr class="udda">
	<th align="right">18:00</th>
	<td align="right">201.77</td>
	<td align="right">4.46</td>
	<td align="right">47,571,288.58</td>
	<td align="right">4.63</td>
	<td align="right">12.00</td>
	<td align="right">4.81</td>
	<td align="right">105,713.97</td>
	<td align="right">13,214.25</td>
	
</tr>
<tr class="jamn">
	<th align="right">19:00</th>
	<td align="right">184.10</td>
	<td align="right">4.07</td>
	<td align="right">36,252,669.65</td>
	<td align="right">3.53</td>
	<td align="right">10.95</td>
	<td align="right">4.38</td>
	<td align="right">80,561.49</td>
	<td align="right">10,070.19</td>
	
</tr>
<tr class="udda">
	<th align="right">20:00</th>
	<td align="right">221.13</td>
	<td align="right">4.89</td>
	<td align="right">52,413,338.19</td>
	<td align="right">5.10</td>
	<td align="right">11.45</td>
	<td align="right">4.58</td>
	<td align="right">116,474.08</td>
	<td align="right">14,559.26</td>
	
</tr>
<tr class="jamn">
	<th align="right">21:00</th>
	<td align="right">214.58</td>
	<td align="right">4.75</td>
	<td align="right">51,753,845.03</td>
	<td align="right">5.04</td>
	<td align="right">11.16</td>
	<td align="right">4.47</td>
	<td align="right">115,008.54</td>
	<td align="right">14,376.07</td>
	
</tr>
<tr class="udda">
	<th align="right">22:00</th>
	<td align="right">166.29</td>
	<td align="right">3.68</td>
	<td align="right">35,869,263.77</td>
	<td align="right">3.49</td>
	<td align="right">11.46</td>
	<td align="right">4.59</td>
	<td align="right">79,709.48</td>
	<td align="right">9,963.68</td>
	
</tr>
<tr class="jamn">
	<th align="right">23:00</th>
	<td align="right">125.52</td>
	<td align="right">2.78</td>
	<td align="right">23,853,147.74</td>
	<td align="right">2.32</td>
	<td align="right">11.73</td>
	<td align="right">4.70</td>
	<td align="right">53,006.99</td>
	<td align="right">6,625.87</td>
	
</tr>

</tbody>
</table>

