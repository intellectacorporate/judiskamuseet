


<h2>Statistics for http://www.u5645470.fsdata.se, Month of 2018-10-1 to 2018-10-31</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 20 of 459 Referring Sites,
Sorted by Access Count

</h3>
<p></p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Referring Site</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th class="hogerstalld">1</th>
	<td>
	<a href="http://judiskamuseet.se">
	http://judiskamuseet.se</a>
	</td>
	<td align="right">79,365</td>
	<td align="right">68.46</td>
	<td align="right">15,675,435,460</td>
	<td align="right">75.04</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	<a href="https://judiskamuseet.se">
	https://judiskamuseet.se</a>
	</td>
	<td align="right">30,960</td>
	<td align="right">26.71</td>
	<td align="right">4,650,243,851</td>
	<td align="right">22.26</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	<a href="https://www.google.se">
	https://www.google.se</a>
	</td>
	<td align="right">2,461</td>
	<td align="right">2.12</td>
	<td align="right">371,969,877</td>
	<td align="right">1.78</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	<a href="https://www.google.com">
	https://www.google.com</a>
	</td>
	<td align="right">679</td>
	<td align="right">0.59</td>
	<td align="right">54,425,853</td>
	<td align="right">0.26</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	<a href="https://www.google.de">
	https://www.google.de</a>
	</td>
	<td align="right">306</td>
	<td align="right">0.26</td>
	<td align="right">4,566,807</td>
	<td align="right">0.02</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	<a href="http://www.google.com">
	http://www.google.com</a>
	</td>
	<td align="right">124</td>
	<td align="right">0.11</td>
	<td align="right">1,840,820</td>
	<td align="right">0.01</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	<a href="http://www.judiska-museet.se">
	http://www.judiska-museet.se</a>
	</td>
	<td align="right">106</td>
	<td align="right">0.09</td>
	<td align="right">1,629,093</td>
	<td align="right">0.01</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	<a href="http://site.ru">
	http://site.ru</a>
	</td>
	<td align="right">85</td>
	<td align="right">0.07</td>
	<td align="right">2,933,369</td>
	<td align="right">0.01</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	<a href="http://m.facebook.com">
	http://m.facebook.com</a>
	</td>
	<td align="right">81</td>
	<td align="right">0.07</td>
	<td align="right">952,696</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">10</th>
	<td>
	<a href="https://www.google.fi">
	https://www.google.fi</a>
	</td>
	<td align="right">49</td>
	<td align="right">0.04</td>
	<td align="right">17,707,498</td>
	<td align="right">0.08</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">11</th>
	<td>
	<a href="https://www.bing.com">
	https://www.bing.com</a>
	</td>
	<td align="right">41</td>
	<td align="right">0.04</td>
	<td align="right">85,406,742</td>
	<td align="right">0.41</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">12</th>
	<td>
	<a href="http://www.judiskamuseet.se">
	http://www.judiskamuseet.se</a>
	</td>
	<td align="right">40</td>
	<td align="right">0.03</td>
	<td align="right">608,818</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">13</th>
	<td>
	<a href="https://89.221.250.27:443">
	https://89.221.250.27:443</a>
	</td>
	<td align="right">38</td>
	<td align="right">0.03</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">14</th>
	<td>
	<a href="http://www.bing.com">
	http://www.bing.com</a>
	</td>
	<td align="right">31</td>
	<td align="right">0.03</td>
	<td align="right">359,562</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">15</th>
	<td>
	<a href="http://www.israelarkeologi.se">
	http://www.israelarkeologi.se</a>
	</td>
	<td align="right">24</td>
	<td align="right">0.02</td>
	<td align="right">329,584</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">16</th>
	<td>
	<a href="https://www.google.co.uk">
	https://www.google.co.uk</a>
	</td>
	<td align="right">17</td>
	<td align="right">0.01</td>
	<td align="right">452,517</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">17</th>
	<td>
	<a href="https://mega-polis.biz.ua">
	https://mega-polis.biz.ua</a>
	</td>
	<td align="right">16</td>
	<td align="right">0.01</td>
	<td align="right">137,448</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">18</th>
	<td>
	<a href="http://www.google.se">
	http://www.google.se</a>
	</td>
	<td align="right">15</td>
	<td align="right">0.01</td>
	<td align="right">203,032</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">19</th>
	<td>
	<a href="https://elonm.ru">
	https://elonm.ru</a>
	</td>
	<td align="right">14</td>
	<td align="right">0.01</td>
	<td align="right">137,676</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">20</th>
	<td>
	<a href="https://l.instagram.com">
	https://l.instagram.com</a>
	</td>
	<td align="right">14</td>
	<td align="right">0.01</td>
	<td align="right">106,872</td>
	<td align="right">0.00</td>
</tr>

</tbody>
</table>

