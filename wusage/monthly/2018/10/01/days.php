


<h2>Statistics for http://www.u5645470.fsdata.se, Month of 2018-10-1 to 2018-10-31</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Traffic by Day</h3>
<p>The continuous line represents a 7-day moving average.</p>
<img src="d.png" width="468" height="351">
<br /><br />


<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Date</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	<th scope='col'>Bits per Sec</th>
	<th scope='col'>Bytes per Sec</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th align="right">2018-10-1</th>
	<td align="right">4,836	</td>
	<td align="right">3.45	</td>
	<td align="right">1,040,136,012	</td>
	<td align="right">3.27	</td>
	<td align="right">258	</td>
	<td align="right">3.33	</td>
	<td align="right">96,308.89	</td>
	<td align="right">12,038.61	</td>
</tr>
<tr class="jamn">
	<th align="right">2018-10-2</th>
	<td align="right">4,709	</td>
	<td align="right">3.36	</td>
	<td align="right">1,014,876,409	</td>
	<td align="right">3.19	</td>
	<td align="right">276	</td>
	<td align="right">3.57	</td>
	<td align="right">93,970.04	</td>
	<td align="right">11,746.25	</td>
</tr>
<tr class="udda">
	<th align="right">2018-10-3</th>
	<td align="right">5,184	</td>
	<td align="right">3.70	</td>
	<td align="right">1,019,637,494	</td>
	<td align="right">3.20	</td>
	<td align="right">286	</td>
	<td align="right">3.69	</td>
	<td align="right">94,410.88	</td>
	<td align="right">11,801.36	</td>
</tr>
<tr class="jamn">
	<th align="right">2018-10-4</th>
	<td align="right">4,020	</td>
	<td align="right">2.87	</td>
	<td align="right">975,543,537	</td>
	<td align="right">3.06	</td>
	<td align="right">264	</td>
	<td align="right">3.41	</td>
	<td align="right">90,328.11	</td>
	<td align="right">11,291.01	</td>
</tr>
<tr class="udda">
	<th align="right">2018-10-5</th>
	<td align="right">4,241	</td>
	<td align="right">3.03	</td>
	<td align="right">1,043,241,043	</td>
	<td align="right">3.28	</td>
	<td align="right">375	</td>
	<td align="right">4.84	</td>
	<td align="right">96,596.39	</td>
	<td align="right">12,074.55	</td>
</tr>
<tr class="jamn">
	<th align="right">2018-10-6</th>
	<td align="right">3,074	</td>
	<td align="right">2.19	</td>
	<td align="right">676,551,533	</td>
	<td align="right">2.12	</td>
	<td align="right">293	</td>
	<td align="right">3.79	</td>
	<td align="right">62,643.66	</td>
	<td align="right">7,830.46	</td>
</tr>
<tr class="udda">
	<th align="right">2018-10-7</th>
	<td align="right">4,155	</td>
	<td align="right">2.97	</td>
	<td align="right">988,305,218	</td>
	<td align="right">3.10	</td>
	<td align="right">305	</td>
	<td align="right">3.94	</td>
	<td align="right">91,509.74	</td>
	<td align="right">11,438.72	</td>
</tr>
<tr class="jamn">
	<th align="right">2018-10-8</th>
	<td align="right">4,766	</td>
	<td align="right">3.40	</td>
	<td align="right">1,113,342,447	</td>
	<td align="right">3.50	</td>
	<td align="right">287	</td>
	<td align="right">3.71	</td>
	<td align="right">103,087.26	</td>
	<td align="right">12,885.91	</td>
</tr>
<tr class="udda">
	<th align="right">2018-10-9</th>
	<td align="right">5,480	</td>
	<td align="right">3.91	</td>
	<td align="right">868,697,900	</td>
	<td align="right">2.73	</td>
	<td align="right">209	</td>
	<td align="right">2.70	</td>
	<td align="right">80,434.99	</td>
	<td align="right">10,054.37	</td>
</tr>
<tr class="jamn">
	<th align="right">2018-10-10</th>
	<td align="right">4,258	</td>
	<td align="right">3.04	</td>
	<td align="right">953,373,402	</td>
	<td align="right">2.99	</td>
	<td align="right">252	</td>
	<td align="right">3.26	</td>
	<td align="right">88,275.32	</td>
	<td align="right">11,034.41	</td>
</tr>
<tr class="udda">
	<th align="right">2018-10-11</th>
	<td align="right">4,853	</td>
	<td align="right">3.46	</td>
	<td align="right">1,373,191,678	</td>
	<td align="right">4.31	</td>
	<td align="right">233	</td>
	<td align="right">3.01	</td>
	<td align="right">127,147.38	</td>
	<td align="right">15,893.42	</td>
</tr>
<tr class="jamn">
	<th align="right">2018-10-12</th>
	<td align="right">3,843	</td>
	<td align="right">2.74	</td>
	<td align="right">981,227,702	</td>
	<td align="right">3.08	</td>
	<td align="right">184	</td>
	<td align="right">2.38	</td>
	<td align="right">90,854.42	</td>
	<td align="right">11,356.80	</td>
</tr>
<tr class="udda">
	<th align="right">2018-10-13</th>
	<td align="right">2,526	</td>
	<td align="right">1.80	</td>
	<td align="right">500,041,245	</td>
	<td align="right">1.57	</td>
	<td align="right">133	</td>
	<td align="right">1.72	</td>
	<td align="right">46,300.12	</td>
	<td align="right">5,787.51	</td>
</tr>
<tr class="jamn">
	<th align="right">2018-10-14</th>
	<td align="right">2,885	</td>
	<td align="right">2.06	</td>
	<td align="right">479,634,921	</td>
	<td align="right">1.51	</td>
	<td align="right">204	</td>
	<td align="right">2.64	</td>
	<td align="right">44,410.64	</td>
	<td align="right">5,551.33	</td>
</tr>
<tr class="udda">
	<th align="right">2018-10-15</th>
	<td align="right">5,359	</td>
	<td align="right">3.82	</td>
	<td align="right">1,197,908,409	</td>
	<td align="right">3.76	</td>
	<td align="right">233	</td>
	<td align="right">3.01	</td>
	<td align="right">110,917.45	</td>
	<td align="right">13,864.68	</td>
</tr>
<tr class="jamn">
	<th align="right">2018-10-16</th>
	<td align="right">7,490	</td>
	<td align="right">5.35	</td>
	<td align="right">1,473,314,066	</td>
	<td align="right">4.63	</td>
	<td align="right">260	</td>
	<td align="right">3.36	</td>
	<td align="right">136,417.97	</td>
	<td align="right">17,052.25	</td>
</tr>
<tr class="udda">
	<th align="right">2018-10-17</th>
	<td align="right">5,893	</td>
	<td align="right">4.21	</td>
	<td align="right">1,135,243,837	</td>
	<td align="right">3.56	</td>
	<td align="right">252	</td>
	<td align="right">3.26	</td>
	<td align="right">105,115.17	</td>
	<td align="right">13,139.40	</td>
</tr>
<tr class="jamn">
	<th align="right">2018-10-18</th>
	<td align="right">6,279	</td>
	<td align="right">4.48	</td>
	<td align="right">1,304,822,726	</td>
	<td align="right">4.10	</td>
	<td align="right">283	</td>
	<td align="right">3.66	</td>
	<td align="right">120,816.92	</td>
	<td align="right">15,102.11	</td>
</tr>
<tr class="udda">
	<th align="right">2018-10-19</th>
	<td align="right">4,407	</td>
	<td align="right">3.15	</td>
	<td align="right">1,154,451,429	</td>
	<td align="right">3.62	</td>
	<td align="right">259	</td>
	<td align="right">3.35	</td>
	<td align="right">106,893.65	</td>
	<td align="right">13,361.71	</td>
</tr>
<tr class="jamn">
	<th align="right">2018-10-20</th>
	<td align="right">3,383	</td>
	<td align="right">2.41	</td>
	<td align="right">890,145,692	</td>
	<td align="right">2.79	</td>
	<td align="right">216	</td>
	<td align="right">2.79	</td>
	<td align="right">82,420.90	</td>
	<td align="right">10,302.61	</td>
</tr>
<tr class="udda">
	<th align="right">2018-10-21</th>
	<td align="right">3,320	</td>
	<td align="right">2.37	</td>
	<td align="right">596,072,287	</td>
	<td align="right">1.87	</td>
	<td align="right">182	</td>
	<td align="right">2.35	</td>
	<td align="right">55,191.88	</td>
	<td align="right">6,898.98	</td>
</tr>
<tr class="jamn">
	<th align="right">2018-10-22</th>
	<td align="right">5,145	</td>
	<td align="right">3.67	</td>
	<td align="right">1,251,884,083	</td>
	<td align="right">3.93	</td>
	<td align="right">236	</td>
	<td align="right">3.05	</td>
	<td align="right">115,915.19	</td>
	<td align="right">14,489.40	</td>
</tr>
<tr class="udda">
	<th align="right">2018-10-23</th>
	<td align="right">5,418	</td>
	<td align="right">3.87	</td>
	<td align="right">1,496,181,354	</td>
	<td align="right">4.70	</td>
	<td align="right">297	</td>
	<td align="right">3.84	</td>
	<td align="right">138,535.31	</td>
	<td align="right">17,316.91	</td>
</tr>
<tr class="jamn">
	<th align="right">2018-10-24</th>
	<td align="right">4,640	</td>
	<td align="right">3.31	</td>
	<td align="right">1,373,654,812	</td>
	<td align="right">4.31	</td>
	<td align="right">241	</td>
	<td align="right">3.11	</td>
	<td align="right">127,190.26	</td>
	<td align="right">15,898.78	</td>
</tr>
<tr class="udda">
	<th align="right">2018-10-25</th>
	<td align="right">3,862	</td>
	<td align="right">2.76	</td>
	<td align="right">991,648,999	</td>
	<td align="right">3.11	</td>
	<td align="right">281	</td>
	<td align="right">3.63	</td>
	<td align="right">91,819.35	</td>
	<td align="right">11,477.42	</td>
</tr>
<tr class="jamn">
	<th align="right">2018-10-26</th>
	<td align="right">3,599	</td>
	<td align="right">2.57	</td>
	<td align="right">1,009,331,137	</td>
	<td align="right">3.17	</td>
	<td align="right">246	</td>
	<td align="right">3.18	</td>
	<td align="right">93,456.59	</td>
	<td align="right">11,682.07	</td>
</tr>
<tr class="udda">
	<th align="right">2018-10-27</th>
	<td align="right">2,996	</td>
	<td align="right">2.14	</td>
	<td align="right">790,472,587	</td>
	<td align="right">2.48	</td>
	<td align="right">190	</td>
	<td align="right">2.45	</td>
	<td align="right">73,191.91	</td>
	<td align="right">9,148.99	</td>
</tr>
<tr class="jamn">
	<th align="right">2018-10-28</th>
	<td align="right">4,386	</td>
	<td align="right">3.13	</td>
	<td align="right">961,159,701	</td>
	<td align="right">3.02	</td>
	<td align="right">240	</td>
	<td align="right">3.10	</td>
	<td align="right">88,996.27	</td>
	<td align="right">11,124.53	</td>
</tr>
<tr class="udda">
	<th align="right">2018-10-29</th>
	<td align="right">4,549	</td>
	<td align="right">3.25	</td>
	<td align="right">935,675,062	</td>
	<td align="right">2.94	</td>
	<td align="right">217	</td>
	<td align="right">2.80	</td>
	<td align="right">86,636.58	</td>
	<td align="right">10,829.57	</td>
</tr>
<tr class="jamn">
	<th align="right">2018-10-30</th>
	<td align="right">5,497	</td>
	<td align="right">3.92	</td>
	<td align="right">972,661,134	</td>
	<td align="right">3.05	</td>
	<td align="right">297	</td>
	<td align="right">3.84	</td>
	<td align="right">90,061.22	</td>
	<td align="right">11,257.65	</td>
</tr>
<tr class="udda">
	<th align="right">2018-10-31</th>
	<td align="right">5,071	</td>
	<td align="right">3.62	</td>
	<td align="right">1,292,234,878	</td>
	<td align="right">4.06	</td>
	<td align="right">252	</td>
	<td align="right">3.26	</td>
	<td align="right">119,651.38	</td>
	<td align="right">14,956.42	</td>
</tr>

</tbody>
</table>

