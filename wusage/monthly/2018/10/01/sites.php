


<h2>Statistics for http://www.u5645470.fsdata.se, Month of 2018-10-1 to 2018-10-31</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 20 of 6,961 Visitor Sites,
Sorted by Access Count

</h3>
<p></p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Site</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="jamnudda">
	<th class="hogerstalld">1</th>
	<td>
	63.143.42.244		
	</td>
	<td align="right">7,833</td>
	<td align="right">5.59</td>
	<td align="right">107,420</td>
	<td align="right">0.00</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	46.59.25.88		
	</td>
	<td align="right">7,321</td>
	<td align="right">5.22</td>
	<td align="right">1,374,040,003</td>
	<td align="right">4.31</td>
	<td align="right">85</td>
	<td align="right">1.10</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	89.221.250.27		
	</td>
	<td align="right">6,854</td>
	<td align="right">4.89</td>
	<td align="right">135,646</td>
	<td align="right">0.00</td>
	<td align="right">83</td>
	<td align="right">1.07</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	46.182.207.79		
	</td>
	<td align="right">3,089</td>
	<td align="right">2.20</td>
	<td align="right">159,727,012</td>
	<td align="right">0.50</td>
	<td align="right">15</td>
	<td align="right">0.20</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	90.229.237.5		
	</td>
	<td align="right">873</td>
	<td align="right">0.62</td>
	<td align="right">64,316,102</td>
	<td align="right">0.20</td>
	<td align="right">6</td>
	<td align="right">0.09</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	31.192.231.134		
	</td>
	<td align="right">620</td>
	<td align="right">0.44</td>
	<td align="right">27,183,503</td>
	<td align="right">0.09</td>
	<td align="right">10</td>
	<td align="right">0.14</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	23.101.169.3		
	</td>
	<td align="right">615</td>
	<td align="right">0.44</td>
	<td align="right">9,244,115</td>
	<td align="right">0.03</td>
	<td align="right">46</td>
	<td align="right">0.60</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	193.188.156.130		
	</td>
	<td align="right">571</td>
	<td align="right">0.41</td>
	<td align="right">318,355,173</td>
	<td align="right">1.00</td>
	<td align="right">13</td>
	<td align="right">0.17</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	212.37.28.30		
	</td>
	<td align="right">504</td>
	<td align="right">0.36</td>
	<td align="right">202,173,246</td>
	<td align="right">0.63</td>
	<td align="right">17</td>
	<td align="right">0.23</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">10</th>
	<td>
	176.57.193.2		
	</td>
	<td align="right">500</td>
	<td align="right">0.36</td>
	<td align="right">159,254,852</td>
	<td align="right">0.50</td>
	<td align="right">18</td>
	<td align="right">0.24</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">11</th>
	<td>
	148.160.250.14		
	</td>
	<td align="right">480</td>
	<td align="right">0.34</td>
	<td align="right">148,592,062</td>
	<td align="right">0.47</td>
	<td align="right">15</td>
	<td align="right">0.20</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">12</th>
	<td>
	217.78.31.253		
	</td>
	<td align="right">403</td>
	<td align="right">0.29</td>
	<td align="right">61,311,482</td>
	<td align="right">0.19</td>
	<td align="right">6</td>
	<td align="right">0.08</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">13</th>
	<td>
	81.200.160.250		
	</td>
	<td align="right">388</td>
	<td align="right">0.28</td>
	<td align="right">103,144,321</td>
	<td align="right">0.32</td>
	<td align="right">8</td>
	<td align="right">0.11</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">14</th>
	<td>
	66.249.64.15		
	</td>
	<td align="right">310</td>
	<td align="right">0.22</td>
	<td align="right">5,315,151</td>
	<td align="right">0.02</td>
	<td align="right">8</td>
	<td align="right">0.11</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">15</th>
	<td>
	40.77.167.80		
	</td>
	<td align="right">285</td>
	<td align="right">0.20</td>
	<td align="right">45,213,206</td>
	<td align="right">0.14</td>
	<td align="right">69</td>
	<td align="right">0.89</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">16</th>
	<td>
	62.209.162.120		
	</td>
	<td align="right">268</td>
	<td align="right">0.19</td>
	<td align="right">98,851,153</td>
	<td align="right">0.31</td>
	<td align="right">8</td>
	<td align="right">0.11</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">17</th>
	<td>
	50.16.241.114		
	</td>
	<td align="right">262</td>
	<td align="right">0.19</td>
	<td align="right">45,121,362</td>
	<td align="right">0.14</td>
	<td align="right">1</td>
	<td align="right">0.01</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">18</th>
	<td>
	192.176.237.2		
	</td>
	<td align="right">260</td>
	<td align="right">0.19</td>
	<td align="right">106,849,768</td>
	<td align="right">0.34</td>
	<td align="right">10</td>
	<td align="right">0.14</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">19</th>
	<td>
	62.20.62.228		
	</td>
	<td align="right">259</td>
	<td align="right">0.18</td>
	<td align="right">81,968,197</td>
	<td align="right">0.26</td>
	<td align="right">5</td>
	<td align="right">0.07</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">20</th>
	<td>
	81.228.169.239		
	</td>
	<td align="right">248</td>
	<td align="right">0.18</td>
	<td align="right">43,915,646</td>
	<td align="right">0.14</td>
	<td align="right">6</td>
	<td align="right">0.09</td>
</tr>

</tbody>
</table>

