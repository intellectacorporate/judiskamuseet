


<h2>Statistics for http://www.u5645470.fsdata.se, Month of 2018-10-1 to 2018-10-31</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 10 of 12 Operating Systems,
Sorted by Access Count

</h3>
<p></p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Product</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th class="hogerstalld">1</th>
	<td>
	Mac OS X		
	</td>
	<td align="right">97,663</td>
	<td align="right">70.01</td>
	<td align="right">26,752,735,661</td>
	<td align="right">84.21</td>
	<td align="right">4,238</td>
	<td align="right">54.75</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	Other		
	</td>
	<td align="right">27,320</td>
	<td align="right">19.58</td>
	<td align="right">1,707,366,011</td>
	<td align="right">5.37</td>
	<td align="right">1,563</td>
	<td align="right">20.20</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	Windows NT		
	</td>
	<td align="right">12,765</td>
	<td align="right">9.15</td>
	<td align="right">3,170,089,455</td>
	<td align="right">9.98</td>
	<td align="right">1,830</td>
	<td align="right">23.65</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	Windows Codename Longhorn		
	</td>
	<td align="right">912</td>
	<td align="right">0.65</td>
	<td align="right">107,456,402</td>
	<td align="right">0.34</td>
	<td align="right">87</td>
	<td align="right">1.14</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	Windows XP		
	</td>
	<td align="right">347</td>
	<td align="right">0.25</td>
	<td align="right">24,352,135</td>
	<td align="right">0.08</td>
	<td align="right">8</td>
	<td align="right">0.11</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	Linux		
	</td>
	<td align="right">324</td>
	<td align="right">0.23</td>
	<td align="right">6,176,888</td>
	<td align="right">0.02</td>
	<td align="right">10</td>
	<td align="right">0.14</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	Windows 2003		
	</td>
	<td align="right">122</td>
	<td align="right">0.09</td>
	<td align="right">408,742</td>
	<td align="right">0.00</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	Windows 2000		
	</td>
	<td align="right">37</td>
	<td align="right">0.03</td>
	<td align="right">1,571,181</td>
	<td align="right">0.00</td>
	<td align="right">1</td>
	<td align="right">0.01</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	Windows Me		
	</td>
	<td align="right">4</td>
	<td align="right">0.00</td>
	<td align="right">74,747</td>
	<td align="right">0.00</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">10</th>
	<td>
	Windows 95		
	</td>
	<td align="right">2</td>
	<td align="right">0.00</td>
	<td align="right">139,379</td>
	<td align="right">0.00</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>

</tbody>
</table>

