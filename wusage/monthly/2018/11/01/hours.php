


<h2>Statistics for http://www.u5645470.fsdata.se, Month of 2018-11-1 to 2018-11-30</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Traffic by Hour</h3>
<p>Figures are averages for that hour
	of each day in the time period analyzed.</p>
<img src="h.png" width="468" height="351">
<br /><br />
<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Hour</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	<th scope='col'>Bits per Sec</th>
	<th scope='col'>Bytes per Sec</th>
	
	</tr>
</thead>
<tbody>
<tr class="jamnudda">
	<th align="right">00:00</th>
	<td align="right">106.40</td>
	<td align="right">2.19</td>
	<td align="right">18,223,456.80</td>
	<td align="right">1.59</td>
	<td align="right">3.40</td>
	<td align="right">1.40</td>
	<td align="right">40,496.57</td>
	<td align="right">5,062.07</td>
	
</tr>
<tr class="jamn">
	<th align="right">01:00</th>
	<td align="right">67.30</td>
	<td align="right">1.39</td>
	<td align="right">10,056,297.67</td>
	<td align="right">0.88</td>
	<td align="right">4.40</td>
	<td align="right">1.81</td>
	<td align="right">22,347.33</td>
	<td align="right">2,793.42</td>
	
</tr>
<tr class="udda">
	<th align="right">02:00</th>
	<td align="right">66.33</td>
	<td align="right">1.37</td>
	<td align="right">8,345,383.07</td>
	<td align="right">0.73</td>
	<td align="right">4.35</td>
	<td align="right">1.79</td>
	<td align="right">18,545.30</td>
	<td align="right">2,318.16</td>
	
</tr>
<tr class="jamn">
	<th align="right">03:00</th>
	<td align="right">65.43</td>
	<td align="right">1.35</td>
	<td align="right">4,103,287.73</td>
	<td align="right">0.36</td>
	<td align="right">4.80</td>
	<td align="right">1.97</td>
	<td align="right">9,118.42</td>
	<td align="right">1,139.80</td>
	
</tr>
<tr class="udda">
	<th align="right">04:00</th>
	<td align="right">65.90</td>
	<td align="right">1.36</td>
	<td align="right">5,972,047.77</td>
	<td align="right">0.52</td>
	<td align="right">5.05</td>
	<td align="right">2.08</td>
	<td align="right">13,271.22</td>
	<td align="right">1,658.90</td>
	
</tr>
<tr class="jamn">
	<th align="right">05:00</th>
	<td align="right">71.03</td>
	<td align="right">1.46</td>
	<td align="right">7,471,373.90</td>
	<td align="right">0.65</td>
	<td align="right">4.76</td>
	<td align="right">1.96</td>
	<td align="right">16,603.05</td>
	<td align="right">2,075.38</td>
	
</tr>
<tr class="udda">
	<th align="right">06:00</th>
	<td align="right">129.37</td>
	<td align="right">2.66</td>
	<td align="right">19,023,582.27</td>
	<td align="right">1.66</td>
	<td align="right">6.75</td>
	<td align="right">2.78</td>
	<td align="right">42,274.63</td>
	<td align="right">5,284.33</td>
	
</tr>
<tr class="jamn">
	<th align="right">07:00</th>
	<td align="right">109.30</td>
	<td align="right">2.25</td>
	<td align="right">20,939,985.10</td>
	<td align="right">1.83</td>
	<td align="right">6.18</td>
	<td align="right">2.54</td>
	<td align="right">46,533.30</td>
	<td align="right">5,816.66</td>
	
</tr>
<tr class="udda">
	<th align="right">08:00</th>
	<td align="right">225.50</td>
	<td align="right">4.64</td>
	<td align="right">58,127,082.97</td>
	<td align="right">5.07</td>
	<td align="right">8.14</td>
	<td align="right">3.35</td>
	<td align="right">129,171.30</td>
	<td align="right">16,146.41</td>
	
</tr>
<tr class="jamn">
	<th align="right">09:00</th>
	<td align="right">305.87</td>
	<td align="right">6.30</td>
	<td align="right">85,692,753.10</td>
	<td align="right">7.48</td>
	<td align="right">13.67</td>
	<td align="right">5.62</td>
	<td align="right">190,428.34</td>
	<td align="right">23,803.54</td>
	
</tr>
<tr class="udda">
	<th align="right">10:00</th>
	<td align="right">383.83</td>
	<td align="right">7.90</td>
	<td align="right">105,107,531.10</td>
	<td align="right">9.17</td>
	<td align="right">14.88</td>
	<td align="right">6.12</td>
	<td align="right">233,572.29</td>
	<td align="right">29,196.54</td>
	
</tr>
<tr class="jamn">
	<th align="right">11:00</th>
	<td align="right">324.07</td>
	<td align="right">6.67</td>
	<td align="right">81,385,125.17</td>
	<td align="right">7.10</td>
	<td align="right">15.64</td>
	<td align="right">6.43</td>
	<td align="right">180,855.83</td>
	<td align="right">22,606.98</td>
	
</tr>
<tr class="udda">
	<th align="right">12:00</th>
	<td align="right">314.57</td>
	<td align="right">6.47</td>
	<td align="right">77,493,358.17</td>
	<td align="right">6.76</td>
	<td align="right">13.10</td>
	<td align="right">5.39</td>
	<td align="right">172,207.46</td>
	<td align="right">21,525.93</td>
	
</tr>
<tr class="jamn">
	<th align="right">13:00</th>
	<td align="right">311.57</td>
	<td align="right">6.41</td>
	<td align="right">79,824,414.57</td>
	<td align="right">6.97</td>
	<td align="right">14.79</td>
	<td align="right">6.09</td>
	<td align="right">177,387.59</td>
	<td align="right">22,173.45</td>
	
</tr>
<tr class="udda">
	<th align="right">14:00</th>
	<td align="right">309.60</td>
	<td align="right">6.37</td>
	<td align="right">86,092,590.73</td>
	<td align="right">7.51</td>
	<td align="right">14.22</td>
	<td align="right">5.85</td>
	<td align="right">191,316.87</td>
	<td align="right">23,914.61</td>
	
</tr>
<tr class="jamn">
	<th align="right">15:00</th>
	<td align="right">260.97</td>
	<td align="right">5.37</td>
	<td align="right">76,134,661.40</td>
	<td align="right">6.64</td>
	<td align="right">14.98</td>
	<td align="right">6.16</td>
	<td align="right">169,188.14</td>
	<td align="right">21,148.52</td>
	
</tr>
<tr class="udda">
	<th align="right">16:00</th>
	<td align="right">243.80</td>
	<td align="right">5.02</td>
	<td align="right">64,506,361.60</td>
	<td align="right">5.63</td>
	<td align="right">12.18</td>
	<td align="right">5.01</td>
	<td align="right">143,347.47</td>
	<td align="right">17,918.43</td>
	
</tr>
<tr class="jamn">
	<th align="right">17:00</th>
	<td align="right">195.80</td>
	<td align="right">4.03</td>
	<td align="right">59,971,438.80</td>
	<td align="right">5.23</td>
	<td align="right">11.54</td>
	<td align="right">4.74</td>
	<td align="right">133,269.86</td>
	<td align="right">16,658.73</td>
	
</tr>
<tr class="udda">
	<th align="right">18:00</th>
	<td align="right">199.20</td>
	<td align="right">4.10</td>
	<td align="right">39,037,318.53</td>
	<td align="right">3.41</td>
	<td align="right">10.00</td>
	<td align="right">4.11</td>
	<td align="right">86,749.60</td>
	<td align="right">10,843.70</td>
	
</tr>
<tr class="jamn">
	<th align="right">19:00</th>
	<td align="right">219.47</td>
	<td align="right">4.52</td>
	<td align="right">45,066,961.23</td>
	<td align="right">3.93</td>
	<td align="right">10.86</td>
	<td align="right">4.47</td>
	<td align="right">100,148.80</td>
	<td align="right">12,518.60</td>
	
</tr>
<tr class="udda">
	<th align="right">20:00</th>
	<td align="right">346.47</td>
	<td align="right">7.13</td>
	<td align="right">60,393,429.03</td>
	<td align="right">5.27</td>
	<td align="right">17.91</td>
	<td align="right">7.37</td>
	<td align="right">134,207.62</td>
	<td align="right">16,775.95</td>
	
</tr>
<tr class="jamn">
	<th align="right">21:00</th>
	<td align="right">216.70</td>
	<td align="right">4.46</td>
	<td align="right">56,517,514.67</td>
	<td align="right">4.93</td>
	<td align="right">11.57</td>
	<td align="right">4.76</td>
	<td align="right">125,594.48</td>
	<td align="right">15,699.31</td>
	
</tr>
<tr class="udda">
	<th align="right">22:00</th>
	<td align="right">170.43</td>
	<td align="right">3.51</td>
	<td align="right">41,892,324.93</td>
	<td align="right">3.66</td>
	<td align="right">8.98</td>
	<td align="right">3.69</td>
	<td align="right">93,094.06</td>
	<td align="right">11,636.76</td>
	
</tr>
<tr class="jamn">
	<th align="right">23:00</th>
	<td align="right">149.93</td>
	<td align="right">3.09</td>
	<td align="right">34,473,801.40</td>
	<td align="right">3.01</td>
	<td align="right">10.99</td>
	<td align="right">4.52</td>
	<td align="right">76,608.45</td>
	<td align="right">9,576.06</td>
	
</tr>

</tbody>
</table>

