


<h2>Statistics for http://www.u5645470.fsdata.se, Month of 2018-11-1 to 2018-11-30</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 10 of 25 File Types (Extensions), 
Sorted by Access Count

</h3>
<p>Individual file types as determined by file extensions. All URLs that
do not contain an extension are counted as directories.</p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Type</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th class="hogerstalld">1</th>
	<td>
	js		
	</td>
	<td align="right">33,841</td>
	<td align="right">23.22</td>
	<td align="right">443,206,006</td>
	<td align="right">1.29</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	Directory (folder)		
	</td>
	<td align="right">30,171</td>
	<td align="right">20.70</td>
	<td align="right">304,327,787</td>
	<td align="right">0.89</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	jpg		
	</td>
	<td align="right">23,892</td>
	<td align="right">16.39</td>
	<td align="right">31,222,703,390</td>
	<td align="right">90.83</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	png		
	</td>
	<td align="right">20,263</td>
	<td align="right">13.90</td>
	<td align="right">1,194,455,029</td>
	<td align="right">3.47</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	css		
	</td>
	<td align="right">17,414</td>
	<td align="right">11.95</td>
	<td align="right">376,491,037</td>
	<td align="right">1.10</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	php		
	</td>
	<td align="right">13,150</td>
	<td align="right">9.02</td>
	<td align="right">163,112,120</td>
	<td align="right">0.47</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	ttf		
	</td>
	<td align="right">3,538</td>
	<td align="right">2.43</td>
	<td align="right">323,331,450</td>
	<td align="right">0.94</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	txt		
	</td>
	<td align="right">1,997</td>
	<td align="right">1.37</td>
	<td align="right">191,334</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	svg		
	</td>
	<td align="right">463</td>
	<td align="right">0.32</td>
	<td align="right">14,623,199</td>
	<td align="right">0.04</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">10</th>
	<td>
	gif		
	</td>
	<td align="right">365</td>
	<td align="right">0.25</td>
	<td align="right">1,473,605</td>
	<td align="right">0.00</td>
</tr>

</tbody>
</table>

