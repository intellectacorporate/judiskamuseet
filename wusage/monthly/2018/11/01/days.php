


<h2>Statistics for http://www.u5645470.fsdata.se, Month of 2018-11-1 to 2018-11-30</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Traffic by Day</h3>
<p>The continuous line represents a 7-day moving average.</p>
<img src="d.png" width="468" height="351">
<br /><br />


<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Date</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	<th scope='col'>Bits per Sec</th>
	<th scope='col'>Bytes per Sec</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th align="right">2018-11-1</th>
	<td align="right">2,858	</td>
	<td align="right">1.96	</td>
	<td align="right">717,065,800	</td>
	<td align="right">2.09	</td>
	<td align="right">159	</td>
	<td align="right">2.18	</td>
	<td align="right">66,394.98	</td>
	<td align="right">8,299.37	</td>
</tr>
<tr class="jamn">
	<th align="right">2018-11-2</th>
	<td align="right">2,567	</td>
	<td align="right">1.76	</td>
	<td align="right">588,462,213	</td>
	<td align="right">1.71	</td>
	<td align="right">142	</td>
	<td align="right">1.95	</td>
	<td align="right">54,487.24	</td>
	<td align="right">6,810.91	</td>
</tr>
<tr class="udda">
	<th align="right">2018-11-3</th>
	<td align="right">3,306	</td>
	<td align="right">2.27	</td>
	<td align="right">769,233,323	</td>
	<td align="right">2.24	</td>
	<td align="right">184	</td>
	<td align="right">2.52	</td>
	<td align="right">71,225.31	</td>
	<td align="right">8,903.16	</td>
</tr>
<tr class="jamn">
	<th align="right">2018-11-4</th>
	<td align="right">2,959	</td>
	<td align="right">2.03	</td>
	<td align="right">649,992,109	</td>
	<td align="right">1.89	</td>
	<td align="right">199	</td>
	<td align="right">2.73	</td>
	<td align="right">60,184.45	</td>
	<td align="right">7,523.06	</td>
</tr>
<tr class="udda">
	<th align="right">2018-11-5</th>
	<td align="right">4,136	</td>
	<td align="right">2.84	</td>
	<td align="right">1,022,473,350	</td>
	<td align="right">2.97	</td>
	<td align="right">233	</td>
	<td align="right">3.19	</td>
	<td align="right">94,673.46	</td>
	<td align="right">11,834.18	</td>
</tr>
<tr class="jamn">
	<th align="right">2018-11-6</th>
	<td align="right">4,454	</td>
	<td align="right">3.06	</td>
	<td align="right">1,151,476,361	</td>
	<td align="right">3.35	</td>
	<td align="right">228	</td>
	<td align="right">3.13	</td>
	<td align="right">106,618.18	</td>
	<td align="right">13,327.27	</td>
</tr>
<tr class="udda">
	<th align="right">2018-11-7</th>
	<td align="right">5,652	</td>
	<td align="right">3.88	</td>
	<td align="right">1,357,558,902	</td>
	<td align="right">3.95	</td>
	<td align="right">263	</td>
	<td align="right">3.61	</td>
	<td align="right">125,699.90	</td>
	<td align="right">15,712.49	</td>
</tr>
<tr class="jamn">
	<th align="right">2018-11-8</th>
	<td align="right">5,684	</td>
	<td align="right">3.90	</td>
	<td align="right">1,342,253,314	</td>
	<td align="right">3.90	</td>
	<td align="right">296	</td>
	<td align="right">4.06	</td>
	<td align="right">124,282.71	</td>
	<td align="right">15,535.34	</td>
</tr>
<tr class="udda">
	<th align="right">2018-11-9</th>
	<td align="right">5,928	</td>
	<td align="right">4.07	</td>
	<td align="right">1,566,793,751	</td>
	<td align="right">4.56	</td>
	<td align="right">289	</td>
	<td align="right">3.96	</td>
	<td align="right">145,073.50	</td>
	<td align="right">18,134.19	</td>
</tr>
<tr class="jamn">
	<th align="right">2018-11-10</th>
	<td align="right">3,607	</td>
	<td align="right">2.47	</td>
	<td align="right">664,209,654	</td>
	<td align="right">1.93	</td>
	<td align="right">240	</td>
	<td align="right">3.29	</td>
	<td align="right">61,500.89	</td>
	<td align="right">7,687.61	</td>
</tr>
<tr class="udda">
	<th align="right">2018-11-11</th>
	<td align="right">3,222	</td>
	<td align="right">2.21	</td>
	<td align="right">834,689,611	</td>
	<td align="right">2.43	</td>
	<td align="right">184	</td>
	<td align="right">2.52	</td>
	<td align="right">77,286.08	</td>
	<td align="right">9,660.76	</td>
</tr>
<tr class="jamn">
	<th align="right">2018-11-12</th>
	<td align="right">3,972	</td>
	<td align="right">2.72	</td>
	<td align="right">1,243,866,353	</td>
	<td align="right">3.62	</td>
	<td align="right">196	</td>
	<td align="right">2.69	</td>
	<td align="right">115,172.81	</td>
	<td align="right">14,396.60	</td>
</tr>
<tr class="udda">
	<th align="right">2018-11-13</th>
	<td align="right">4,105	</td>
	<td align="right">2.82	</td>
	<td align="right">1,050,522,127	</td>
	<td align="right">3.06	</td>
	<td align="right">234	</td>
	<td align="right">3.21	</td>
	<td align="right">97,270.57	</td>
	<td align="right">12,158.82	</td>
</tr>
<tr class="jamn">
	<th align="right">2018-11-14</th>
	<td align="right">4,894	</td>
	<td align="right">3.36	</td>
	<td align="right">1,368,588,104	</td>
	<td align="right">3.98	</td>
	<td align="right">256	</td>
	<td align="right">3.51	</td>
	<td align="right">126,721.12	</td>
	<td align="right">15,840.14	</td>
</tr>
<tr class="udda">
	<th align="right">2018-11-15</th>
	<td align="right">9,107	</td>
	<td align="right">6.25	</td>
	<td align="right">1,799,515,502	</td>
	<td align="right">5.23	</td>
	<td align="right">486	</td>
	<td align="right">6.66	</td>
	<td align="right">166,621.81	</td>
	<td align="right">20,827.73	</td>
</tr>
<tr class="jamn">
	<th align="right">2018-11-16</th>
	<td align="right">3,658	</td>
	<td align="right">2.51	</td>
	<td align="right">1,068,942,873	</td>
	<td align="right">3.11	</td>
	<td align="right">217	</td>
	<td align="right">2.98	</td>
	<td align="right">98,976.19	</td>
	<td align="right">12,372.02	</td>
</tr>
<tr class="udda">
	<th align="right">2018-11-17</th>
	<td align="right">3,186	</td>
	<td align="right">2.19	</td>
	<td align="right">715,600,603	</td>
	<td align="right">2.08	</td>
	<td align="right">182	</td>
	<td align="right">2.50	</td>
	<td align="right">66,259.32	</td>
	<td align="right">8,282.41	</td>
</tr>
<tr class="jamn">
	<th align="right">2018-11-18</th>
	<td align="right">3,534	</td>
	<td align="right">2.42	</td>
	<td align="right">1,127,976,129	</td>
	<td align="right">3.28	</td>
	<td align="right">190	</td>
	<td align="right">2.60	</td>
	<td align="right">104,442.23	</td>
	<td align="right">13,055.28	</td>
</tr>
<tr class="udda">
	<th align="right">2018-11-19</th>
	<td align="right">4,412	</td>
	<td align="right">3.03	</td>
	<td align="right">1,487,945,637	</td>
	<td align="right">4.33	</td>
	<td align="right">246	</td>
	<td align="right">3.37	</td>
	<td align="right">137,772.74	</td>
	<td align="right">17,221.59	</td>
</tr>
<tr class="jamn">
	<th align="right">2018-11-20</th>
	<td align="right">6,473	</td>
	<td align="right">4.44	</td>
	<td align="right">1,632,219,665	</td>
	<td align="right">4.75	</td>
	<td align="right">410	</td>
	<td align="right">5.62	</td>
	<td align="right">151,131.45	</td>
	<td align="right">18,891.43	</td>
</tr>
<tr class="udda">
	<th align="right">2018-11-21</th>
	<td align="right">7,705	</td>
	<td align="right">5.29	</td>
	<td align="right">1,615,166,014	</td>
	<td align="right">4.70	</td>
	<td align="right">328	</td>
	<td align="right">4.50	</td>
	<td align="right">149,552.41	</td>
	<td align="right">18,694.05	</td>
</tr>
<tr class="jamn">
	<th align="right">2018-11-22</th>
	<td align="right">6,531	</td>
	<td align="right">4.48	</td>
	<td align="right">1,389,481,358	</td>
	<td align="right">4.04	</td>
	<td align="right">257	</td>
	<td align="right">3.52	</td>
	<td align="right">128,655.68	</td>
	<td align="right">16,081.96	</td>
</tr>
<tr class="udda">
	<th align="right">2018-11-23</th>
	<td align="right">7,596	</td>
	<td align="right">5.21	</td>
	<td align="right">958,881,121	</td>
	<td align="right">2.79	</td>
	<td align="right">169	</td>
	<td align="right">2.32	</td>
	<td align="right">88,785.29	</td>
	<td align="right">11,098.16	</td>
</tr>
<tr class="jamn">
	<th align="right">2018-11-24</th>
	<td align="right">2,782	</td>
	<td align="right">1.91	</td>
	<td align="right">520,696,681	</td>
	<td align="right">1.51	</td>
	<td align="right">242	</td>
	<td align="right">3.32	</td>
	<td align="right">48,212.66	</td>
	<td align="right">6,026.58	</td>
</tr>
<tr class="udda">
	<th align="right">2018-11-25</th>
	<td align="right">3,224	</td>
	<td align="right">2.21	</td>
	<td align="right">787,600,007	</td>
	<td align="right">2.29	</td>
	<td align="right">230	</td>
	<td align="right">3.15	</td>
	<td align="right">72,925.93	</td>
	<td align="right">9,115.74	</td>
</tr>
<tr class="jamn">
	<th align="right">2018-11-26</th>
	<td align="right">7,295	</td>
	<td align="right">5.00	</td>
	<td align="right">1,629,031,208	</td>
	<td align="right">4.74	</td>
	<td align="right">251	</td>
	<td align="right">3.44	</td>
	<td align="right">150,836.22	</td>
	<td align="right">18,854.53	</td>
</tr>
<tr class="udda">
	<th align="right">2018-11-27</th>
	<td align="right">5,360	</td>
	<td align="right">3.68	</td>
	<td align="right">1,299,131,170	</td>
	<td align="right">3.78	</td>
	<td align="right">232	</td>
	<td align="right">3.18	</td>
	<td align="right">120,289.92	</td>
	<td align="right">15,036.24	</td>
</tr>
<tr class="jamn">
	<th align="right">2018-11-28</th>
	<td align="right">5,427	</td>
	<td align="right">3.72	</td>
	<td align="right">1,219,964,097	</td>
	<td align="right">3.55	</td>
	<td align="right">208	</td>
	<td align="right">2.85	</td>
	<td align="right">112,959.64	</td>
	<td align="right">14,119.95	</td>
</tr>
<tr class="udda">
	<th align="right">2018-11-29</th>
	<td align="right">5,191	</td>
	<td align="right">3.56	</td>
	<td align="right">1,406,247,166	</td>
	<td align="right">4.09	</td>
	<td align="right">204	</td>
	<td align="right">2.80	</td>
	<td align="right">130,208.07	</td>
	<td align="right">16,276.01	</td>
</tr>
<tr class="jamn">
	<th align="right">2018-11-30</th>
	<td align="right">6,940	</td>
	<td align="right">4.76	</td>
	<td align="right">1,389,978,248	</td>
	<td align="right">4.04	</td>
	<td align="right">339	</td>
	<td align="right">4.65	</td>
	<td align="right">128,701.69	</td>
	<td align="right">16,087.71	</td>
</tr>

</tbody>
</table>

