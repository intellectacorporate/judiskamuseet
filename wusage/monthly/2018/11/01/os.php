


<h2>Statistics for http://www.u5645470.fsdata.se, Month of 2018-11-1 to 2018-11-30</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 10 of 12 Operating Systems,
Sorted by Access Count

</h3>
<p></p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Product</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th class="hogerstalld">1</th>
	<td>
	Mac OS X		
	</td>
	<td align="right">108,135</td>
	<td align="right">74.49</td>
	<td align="right">30,243,121,017</td>
	<td align="right">88.19</td>
	<td align="right">4,697</td>
	<td align="right">64.40</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	Other		
	</td>
	<td align="right">25,864</td>
	<td align="right">17.82</td>
	<td align="right">1,507,972,184</td>
	<td align="right">4.40</td>
	<td align="right">1,800</td>
	<td align="right">24.69</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	Windows NT		
	</td>
	<td align="right">9,073</td>
	<td align="right">6.25</td>
	<td align="right">2,342,276,589</td>
	<td align="right">6.83</td>
	<td align="right">682</td>
	<td align="right">9.36</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	Windows Codename Longhorn		
	</td>
	<td align="right">853</td>
	<td align="right">0.59</td>
	<td align="right">85,071,792</td>
	<td align="right">0.25</td>
	<td align="right">80</td>
	<td align="right">1.10</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	Windows XP		
	</td>
	<td align="right">697</td>
	<td align="right">0.48</td>
	<td align="right">29,568,324</td>
	<td align="right">0.09</td>
	<td align="right">14</td>
	<td align="right">0.19</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	Linux		
	</td>
	<td align="right">352</td>
	<td align="right">0.24</td>
	<td align="right">66,019,166</td>
	<td align="right">0.19</td>
	<td align="right">16</td>
	<td align="right">0.23</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	Windows 2000		
	</td>
	<td align="right">133</td>
	<td align="right">0.09</td>
	<td align="right">16,795,255</td>
	<td align="right">0.05</td>
	<td align="right">2</td>
	<td align="right">0.03</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	Windows 2003		
	</td>
	<td align="right">62</td>
	<td align="right">0.04</td>
	<td align="right">334,805</td>
	<td align="right">0.00</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	Windows Me		
	</td>
	<td align="right">4</td>
	<td align="right">0.00</td>
	<td align="right">44,605</td>
	<td align="right">0.00</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">10</th>
	<td>
	Sun Solaris		
	</td>
	<td align="right">1</td>
	<td align="right">0.00</td>
	<td align="right">12,061</td>
	<td align="right">0.00</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>

</tbody>
</table>

