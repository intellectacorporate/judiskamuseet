


<h2>Statistics for http://www.u5645470.fsdata.se, Month of 2018-11-1 to 2018-11-30</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 10 of 315 Browsers (User Agents),
Sorted by Access Count

</h3>
<p></p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Product</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th class="hogerstalld">1</th>
	<td>
	Safari/537.36		
	</td>
	<td align="right">55,065</td>
	<td align="right">37.93</td>
	<td align="right">15,272,177,298</td>
	<td align="right">44.54</td>
	<td align="right">2,503</td>
	<td align="right">34.50</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	Safari/604.1		
	</td>
	<td align="right">22,310</td>
	<td align="right">15.37</td>
	<td align="right">6,962,043,450</td>
	<td align="right">20.30</td>
	<td align="right">994</td>
	<td align="right">13.70</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	Mozilla/5.0 		
	</td>
	<td align="right">12,332</td>
	<td align="right">8.49</td>
	<td align="right">3,050,938,876</td>
	<td align="right">8.90</td>
	<td align="right">815</td>
	<td align="right">11.23</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	Safari/537.36 Edge/17.17134		
	</td>
	<td align="right">12,020</td>
	<td align="right">8.28</td>
	<td align="right">2,517,642,140</td>
	<td align="right">7.34</td>
	<td align="right">200</td>
	<td align="right">2.76</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	UptimeRobot/2.0		
	</td>
	<td align="right">7,663</td>
	<td align="right">5.28</td>
	<td align="right">415,546</td>
	<td align="right">0.00</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	Safari/605.1.15		
	</td>
	<td align="right">6,540</td>
	<td align="right">4.50</td>
	<td align="right">2,757,804,642</td>
	<td align="right">8.04</td>
	<td align="right">264</td>
	<td align="right">3.64</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	WordPress/4.9.8		
	</td>
	<td align="right">5,475</td>
	<td align="right">3.77</td>
	<td align="right">108,254</td>
	<td align="right">0.00</td>
	<td align="right">80</td>
	<td align="right">1.12</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	bingbot/2.0		
	</td>
	<td align="right">3,588</td>
	<td align="right">2.47</td>
	<td align="right">184,715,209</td>
	<td align="right">0.54</td>
	<td align="right">636</td>
	<td align="right">8.78</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	Safari/602.1		
	</td>
	<td align="right">1,418</td>
	<td align="right">0.98</td>
	<td align="right">270,037,996</td>
	<td align="right">0.79</td>
	<td align="right">67</td>
	<td align="right">0.93</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">10</th>
	<td>
	Safari/537.36 (compatible; Googlebot/2.1;  http://www.google.com/bot.html)		
	</td>
	<td align="right">1,294</td>
	<td align="right">0.89</td>
	<td align="right">15,089,746</td>
	<td align="right">0.04</td>
	<td align="right">188</td>
	<td align="right">2.60</td>
</tr>

</tbody>
</table>

