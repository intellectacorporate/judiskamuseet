


<h2>Statistics for http://www.u5645470.fsdata.se, Month of 2018-11-1 to 2018-11-30</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 20 of 5,875 Visitor Sites,
Sorted by Access Count

</h3>
<p></p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Site</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th class="hogerstalld">1</th>
	<td>
	46.59.25.88		
	</td>
	<td align="right">9,722</td>
	<td align="right">6.67</td>
	<td align="right">1,785,950,163</td>
	<td align="right">5.20</td>
	<td align="right">73</td>
	<td align="right">1.01</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	63.143.42.244		
	</td>
	<td align="right">7,661</td>
	<td align="right">5.26</td>
	<td align="right">212,711</td>
	<td align="right">0.00</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	89.221.250.27		
	</td>
	<td align="right">5,480</td>
	<td align="right">3.76</td>
	<td align="right">108,259</td>
	<td align="right">0.00</td>
	<td align="right">80</td>
	<td align="right">1.11</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	90.229.237.5		
	</td>
	<td align="right">4,595</td>
	<td align="right">3.15</td>
	<td align="right">218,951,778</td>
	<td align="right">0.64</td>
	<td align="right">20</td>
	<td align="right">0.27</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	46.182.207.79		
	</td>
	<td align="right">1,050</td>
	<td align="right">0.72</td>
	<td align="right">49,494,104</td>
	<td align="right">0.14</td>
	<td align="right">8</td>
	<td align="right">0.12</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	31.208.201.126		
	</td>
	<td align="right">992</td>
	<td align="right">0.68</td>
	<td align="right">28,708,195</td>
	<td align="right">0.08</td>
	<td align="right">1</td>
	<td align="right">0.01</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	213.212.29.165		
	</td>
	<td align="right">709</td>
	<td align="right">0.49</td>
	<td align="right">218,433,288</td>
	<td align="right">0.64</td>
	<td align="right">8</td>
	<td align="right">0.12</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	195.3.144.185		
	</td>
	<td align="right">649</td>
	<td align="right">0.45</td>
	<td align="right">10,717,596</td>
	<td align="right">0.03</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	23.101.169.3		
	</td>
	<td align="right">620</td>
	<td align="right">0.43</td>
	<td align="right">9,507,976</td>
	<td align="right">0.03</td>
	<td align="right">45</td>
	<td align="right">0.62</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">10</th>
	<td>
	213.115.65.36		
	</td>
	<td align="right">462</td>
	<td align="right">0.32</td>
	<td align="right">198,272,874</td>
	<td align="right">0.58</td>
	<td align="right">12</td>
	<td align="right">0.18</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">11</th>
	<td>
	188.92.74.189		
	</td>
	<td align="right">432</td>
	<td align="right">0.30</td>
	<td align="right">7,107,444</td>
	<td align="right">0.02</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">12</th>
	<td>
	193.17.67.80		
	</td>
	<td align="right">382</td>
	<td align="right">0.26</td>
	<td align="right">104,724,527</td>
	<td align="right">0.30</td>
	<td align="right">6</td>
	<td align="right">0.09</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">13</th>
	<td>
	78.131.58.190		
	</td>
	<td align="right">358</td>
	<td align="right">0.25</td>
	<td align="right">11,056,633</td>
	<td align="right">0.03</td>
	<td align="right">3</td>
	<td align="right">0.05</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">14</th>
	<td>
	83.241.181.4		
	</td>
	<td align="right">337</td>
	<td align="right">0.23</td>
	<td align="right">75,080,475</td>
	<td align="right">0.22</td>
	<td align="right">14</td>
	<td align="right">0.19</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">15</th>
	<td>
	130.242.48.10		
	</td>
	<td align="right">313</td>
	<td align="right">0.21</td>
	<td align="right">168,305,775</td>
	<td align="right">0.49</td>
	<td align="right">10</td>
	<td align="right">0.15</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">16</th>
	<td>
	83.241.181.2		
	</td>
	<td align="right">297</td>
	<td align="right">0.20</td>
	<td align="right">62,000,329</td>
	<td align="right">0.18</td>
	<td align="right">16</td>
	<td align="right">0.22</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">17</th>
	<td>
	122.49.28.2		
	</td>
	<td align="right">272</td>
	<td align="right">0.19</td>
	<td align="right">49,433,247</td>
	<td align="right">0.14</td>
	<td align="right">13</td>
	<td align="right">0.19</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">18</th>
	<td>
	81.228.169.239		
	</td>
	<td align="right">271</td>
	<td align="right">0.19</td>
	<td align="right">72,830,716</td>
	<td align="right">0.21</td>
	<td align="right">7</td>
	<td align="right">0.10</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">19</th>
	<td>
	158.255.217.32		
	</td>
	<td align="right">269</td>
	<td align="right">0.18</td>
	<td align="right">55,992,876</td>
	<td align="right">0.16</td>
	<td align="right">7</td>
	<td align="right">0.10</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">20</th>
	<td>
	34.209.175.187		
	</td>
	<td align="right">263</td>
	<td align="right">0.18</td>
	<td align="right">23,824,817</td>
	<td align="right">0.07</td>
	<td align="right">7</td>
	<td align="right">0.10</td>
</tr>

</tbody>
</table>

