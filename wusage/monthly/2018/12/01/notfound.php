


<h2>Statistics for http://www.u5645470.fsdata.se, Month of 2018-12-1 to 2018-12-31</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 10 of 293 Documents Not Found,
Sorted by Access Count

</h3>
<p></p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Document</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th class="hogerstalld">1</th>
	<td>
	/autodiscover/autodiscover.xml
	</td>
	<td align="right">2,083</td>
	<td align="right">58.07</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	/
	</td>
	<td align="right">703</td>
	<td align="right">19.60</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	/wp-content/themes/Divi/js/html5.js
	</td>
	<td align="right">56</td>
	<td align="right">1.56</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	/apple-touch-icon.png
	</td>
	<td align="right">52</td>
	<td align="right">1.45</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	/apple-touch-icon-precomposed.png
	</td>
	<td align="right">49</td>
	<td align="right">1.37</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	/tracesofexistence
	</td>
	<td align="right">39</td>
	<td align="right">1.09</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	/wp-content/uploads/2017/06/torakrona-norrköping-1859-1-189x300.jpg
	</td>
	<td align="right">37</td>
	<td align="right">1.03</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	/wp-content/uploads/2017/06/Judiska_vänföreningen_logo_v1-300x238.png
	</td>
	<td align="right">27</td>
	<td align="right">0.75</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	/wp-content/uploads/2017/09/synagogan-på-själagårdsgatan-19.jpg
	</td>
	<td align="right">19</td>
	<td align="right">0.53</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">10</th>
	<td>
	/.well-known/assetlinks.json
	</td>
	<td align="right">17</td>
	<td align="right">0.47</td>
</tr>

</tbody>
</table>

