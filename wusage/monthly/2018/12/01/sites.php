


<h2>Statistics for http://www.u5645470.fsdata.se, Month of 2018-12-1 to 2018-12-31</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 20 of 5,305 Visitor Sites,
Sorted by Access Count

</h3>
<p></p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Site</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="jamnudda">
	<th class="hogerstalld">1</th>
	<td>
	46.59.25.88		
	</td>
	<td align="right">24,312</td>
	<td align="right">15.57</td>
	<td align="right">4,574,108,394</td>
	<td align="right">11.68</td>
	<td align="right">57</td>
	<td align="right">0.90</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	63.143.42.244		
	</td>
	<td align="right">7,877</td>
	<td align="right">5.04</td>
	<td align="right">16,138</td>
	<td align="right">0.00</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	81.170.246.9		
	</td>
	<td align="right">7,240</td>
	<td align="right">4.64</td>
	<td align="right">901,529,992</td>
	<td align="right">2.30</td>
	<td align="right">18</td>
	<td align="right">0.29</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	89.221.250.27		
	</td>
	<td align="right">6,334</td>
	<td align="right">4.06</td>
	<td align="right">124,677</td>
	<td align="right">0.00</td>
	<td align="right">91</td>
	<td align="right">1.44</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	46.182.207.79		
	</td>
	<td align="right">5,564</td>
	<td align="right">3.56</td>
	<td align="right">230,176,385</td>
	<td align="right">0.59</td>
	<td align="right">17</td>
	<td align="right">0.27</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	195.3.144.185		
	</td>
	<td align="right">2,312</td>
	<td align="right">1.48</td>
	<td align="right">37,678,261</td>
	<td align="right">0.10</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	188.92.74.189		
	</td>
	<td align="right">1,114</td>
	<td align="right">0.71</td>
	<td align="right">18,161,860</td>
	<td align="right">0.05</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	23.101.169.3		
	</td>
	<td align="right">656</td>
	<td align="right">0.42</td>
	<td align="right">10,198,080</td>
	<td align="right">0.03</td>
	<td align="right">44</td>
	<td align="right">0.70</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	212.112.54.6		
	</td>
	<td align="right">571</td>
	<td align="right">0.37</td>
	<td align="right">187,552,391</td>
	<td align="right">0.48</td>
	<td align="right">8</td>
	<td align="right">0.13</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">10</th>
	<td>
	77.72.102.150		
	</td>
	<td align="right">456</td>
	<td align="right">0.29</td>
	<td align="right">346,143,910</td>
	<td align="right">0.88</td>
	<td align="right">10</td>
	<td align="right">0.17</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">11</th>
	<td>
	216.244.66.227		
	</td>
	<td align="right">411</td>
	<td align="right">0.26</td>
	<td align="right">40,785,749</td>
	<td align="right">0.10</td>
	<td align="right">103</td>
	<td align="right">1.63</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">12</th>
	<td>
	162.208.51.36		
	</td>
	<td align="right">394</td>
	<td align="right">0.25</td>
	<td align="right">51,317,536</td>
	<td align="right">0.13</td>
	<td align="right">3</td>
	<td align="right">0.05</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">13</th>
	<td>
	66.249.66.214		
	</td>
	<td align="right">381</td>
	<td align="right">0.24</td>
	<td align="right">14,660,513</td>
	<td align="right">0.04</td>
	<td align="right">62</td>
	<td align="right">0.98</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">14</th>
	<td>
	66.249.66.216		
	</td>
	<td align="right">347</td>
	<td align="right">0.22</td>
	<td align="right">6,668,524</td>
	<td align="right">0.02</td>
	<td align="right">58</td>
	<td align="right">0.92</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">15</th>
	<td>
	213.212.29.165		
	</td>
	<td align="right">346</td>
	<td align="right">0.22</td>
	<td align="right">65,764,479</td>
	<td align="right">0.17</td>
	<td align="right">3</td>
	<td align="right">0.05</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">16</th>
	<td>
	10.101.2.49		
	</td>
	<td align="right">342</td>
	<td align="right">0.22</td>
	<td align="right">7,902,333</td>
	<td align="right">0.02</td>
	<td align="right">1</td>
	<td align="right">0.02</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">17</th>
	<td>
	37.9.113.109		
	</td>
	<td align="right">336</td>
	<td align="right">0.22</td>
	<td align="right">5,533,853</td>
	<td align="right">0.01</td>
	<td align="right">108</td>
	<td align="right">1.70</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">18</th>
	<td>
	88.131.44.131		
	</td>
	<td align="right">318</td>
	<td align="right">0.20</td>
	<td align="right">119,415,897</td>
	<td align="right">0.30</td>
	<td align="right">7</td>
	<td align="right">0.12</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">19</th>
	<td>
	66.249.66.222		
	</td>
	<td align="right">300</td>
	<td align="right">0.19</td>
	<td align="right">10,422,386</td>
	<td align="right">0.03</td>
	<td align="right">42</td>
	<td align="right">0.67</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">20</th>
	<td>
	212.247.140.231		
	</td>
	<td align="right">300</td>
	<td align="right">0.19</td>
	<td align="right">53,293,207</td>
	<td align="right">0.14</td>
	<td align="right">18</td>
	<td align="right">0.29</td>
</tr>

</tbody>
</table>

