


<h2>Statistics for http://www.u5645470.fsdata.se, Month of 2018-12-1 to 2018-12-31</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 10 of 22 File Types (Extensions), 
Sorted by Access Count

</h3>
<p>Individual file types as determined by file extensions. All URLs that
do not contain an extension are counted as directories.</p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Type</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th class="hogerstalld">1</th>
	<td>
	js		
	</td>
	<td align="right">35,669</td>
	<td align="right">22.84</td>
	<td align="right">433,563,035</td>
	<td align="right">1.11</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	jpg		
	</td>
	<td align="right">35,185</td>
	<td align="right">22.53</td>
	<td align="right">35,837,239,137</td>
	<td align="right">91.48</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	Directory (folder)		
	</td>
	<td align="right">33,537</td>
	<td align="right">21.48</td>
	<td align="right">345,196,473</td>
	<td align="right">0.88</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	css		
	</td>
	<td align="right">18,696</td>
	<td align="right">11.97</td>
	<td align="right">370,495,251</td>
	<td align="right">0.95</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	php		
	</td>
	<td align="right">16,106</td>
	<td align="right">10.32</td>
	<td align="right">655,144,484</td>
	<td align="right">1.67</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	png		
	</td>
	<td align="right">9,307</td>
	<td align="right">5.96</td>
	<td align="right">972,513,659</td>
	<td align="right">2.48</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	ttf		
	</td>
	<td align="right">3,175</td>
	<td align="right">2.03</td>
	<td align="right">283,332,929</td>
	<td align="right">0.72</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	txt		
	</td>
	<td align="right">1,894</td>
	<td align="right">1.21</td>
	<td align="right">128,975</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	svg		
	</td>
	<td align="right">1,368</td>
	<td align="right">0.88</td>
	<td align="right">14,905,496</td>
	<td align="right">0.04</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">10</th>
	<td>
	gif		
	</td>
	<td align="right">579</td>
	<td align="right">0.37</td>
	<td align="right">2,962,525</td>
	<td align="right">0.01</td>
</tr>

</tbody>
</table>

