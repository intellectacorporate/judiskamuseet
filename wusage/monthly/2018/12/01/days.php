


<h2>Statistics for http://www.u5645470.fsdata.se, Month of 2018-12-1 to 2018-12-31</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Traffic by Day</h3>
<p>The continuous line represents a 7-day moving average.</p>
<img src="d.png" width="468" height="351">
<br /><br />


<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Date</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	<th scope='col'>Bits per Sec</th>
	<th scope='col'>Bytes per Sec</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th align="right">2018-12-1</th>
	<td align="right">3,338	</td>
	<td align="right">2.14	</td>
	<td align="right">846,218,593	</td>
	<td align="right">2.16	</td>
	<td align="right">171	</td>
	<td align="right">2.69	</td>
	<td align="right">78,353.57	</td>
	<td align="right">9,794.20	</td>
</tr>
<tr class="jamn">
	<th align="right">2018-12-2</th>
	<td align="right">4,535	</td>
	<td align="right">2.90	</td>
	<td align="right">1,254,470,118	</td>
	<td align="right">3.20	</td>
	<td align="right">218	</td>
	<td align="right">3.43	</td>
	<td align="right">116,154.64	</td>
	<td align="right">14,519.33	</td>
</tr>
<tr class="udda">
	<th align="right">2018-12-3</th>
	<td align="right">4,999	</td>
	<td align="right">3.20	</td>
	<td align="right">1,354,273,907	</td>
	<td align="right">3.46	</td>
	<td align="right">281	</td>
	<td align="right">4.42	</td>
	<td align="right">125,395.73	</td>
	<td align="right">15,674.47	</td>
</tr>
<tr class="jamn">
	<th align="right">2018-12-4</th>
	<td align="right">5,729	</td>
	<td align="right">3.67	</td>
	<td align="right">2,034,906,243	</td>
	<td align="right">5.19	</td>
	<td align="right">291	</td>
	<td align="right">4.57	</td>
	<td align="right">188,417.24	</td>
	<td align="right">23,552.16	</td>
</tr>
<tr class="udda">
	<th align="right">2018-12-5</th>
	<td align="right">4,603	</td>
	<td align="right">2.95	</td>
	<td align="right">1,197,070,326	</td>
	<td align="right">3.06	</td>
	<td align="right">212	</td>
	<td align="right">3.33	</td>
	<td align="right">110,839.85	</td>
	<td align="right">13,854.98	</td>
</tr>
<tr class="jamn">
	<th align="right">2018-12-6</th>
	<td align="right">4,661	</td>
	<td align="right">2.99	</td>
	<td align="right">1,444,282,432	</td>
	<td align="right">3.69	</td>
	<td align="right">215	</td>
	<td align="right">3.38	</td>
	<td align="right">133,729.85	</td>
	<td align="right">16,716.23	</td>
</tr>
<tr class="udda">
	<th align="right">2018-12-7</th>
	<td align="right">7,185	</td>
	<td align="right">4.60	</td>
	<td align="right">1,966,423,794	</td>
	<td align="right">5.02	</td>
	<td align="right">217	</td>
	<td align="right">3.41	</td>
	<td align="right">182,076.28	</td>
	<td align="right">22,759.53	</td>
</tr>
<tr class="jamn">
	<th align="right">2018-12-8</th>
	<td align="right">2,437	</td>
	<td align="right">1.56	</td>
	<td align="right">728,638,588	</td>
	<td align="right">1.86	</td>
	<td align="right">139	</td>
	<td align="right">2.18	</td>
	<td align="right">67,466.54	</td>
	<td align="right">8,433.32	</td>
</tr>
<tr class="udda">
	<th align="right">2018-12-9</th>
	<td align="right">3,235	</td>
	<td align="right">2.07	</td>
	<td align="right">1,039,006,752	</td>
	<td align="right">2.65	</td>
	<td align="right">187	</td>
	<td align="right">2.94	</td>
	<td align="right">96,204.33	</td>
	<td align="right">12,025.54	</td>
</tr>
<tr class="jamn">
	<th align="right">2018-12-10</th>
	<td align="right">4,281	</td>
	<td align="right">2.74	</td>
	<td align="right">1,469,407,373	</td>
	<td align="right">3.75	</td>
	<td align="right">214	</td>
	<td align="right">3.36	</td>
	<td align="right">136,056.24	</td>
	<td align="right">17,007.03	</td>
</tr>
<tr class="udda">
	<th align="right">2018-12-11</th>
	<td align="right">7,969	</td>
	<td align="right">5.10	</td>
	<td align="right">1,777,675,907	</td>
	<td align="right">4.54	</td>
	<td align="right">246	</td>
	<td align="right">3.87	</td>
	<td align="right">164,599.62	</td>
	<td align="right">20,574.95	</td>
</tr>
<tr class="jamn">
	<th align="right">2018-12-12</th>
	<td align="right">8,886	</td>
	<td align="right">5.69	</td>
	<td align="right">2,058,741,078	</td>
	<td align="right">5.26	</td>
	<td align="right">291	</td>
	<td align="right">4.57	</td>
	<td align="right">190,624.17	</td>
	<td align="right">23,828.02	</td>
</tr>
<tr class="udda">
	<th align="right">2018-12-13</th>
	<td align="right">8,182	</td>
	<td align="right">5.24	</td>
	<td align="right">1,916,600,123	</td>
	<td align="right">4.89	</td>
	<td align="right">296	</td>
	<td align="right">4.65	</td>
	<td align="right">177,462.97	</td>
	<td align="right">22,182.87	</td>
</tr>
<tr class="jamn">
	<th align="right">2018-12-14</th>
	<td align="right">9,696	</td>
	<td align="right">6.21	</td>
	<td align="right">2,097,959,013	</td>
	<td align="right">5.36	</td>
	<td align="right">309	</td>
	<td align="right">4.86	</td>
	<td align="right">194,255.46	</td>
	<td align="right">24,281.93	</td>
</tr>
<tr class="udda">
	<th align="right">2018-12-15</th>
	<td align="right">3,413	</td>
	<td align="right">2.19	</td>
	<td align="right">783,633,623	</td>
	<td align="right">2.00	</td>
	<td align="right">162	</td>
	<td align="right">2.55	</td>
	<td align="right">72,558.67	</td>
	<td align="right">9,069.83	</td>
</tr>
<tr class="jamn">
	<th align="right">2018-12-16</th>
	<td align="right">2,998	</td>
	<td align="right">1.92	</td>
	<td align="right">852,473,064	</td>
	<td align="right">2.18	</td>
	<td align="right">163	</td>
	<td align="right">2.56	</td>
	<td align="right">78,932.69	</td>
	<td align="right">9,866.59	</td>
</tr>
<tr class="udda">
	<th align="right">2018-12-17</th>
	<td align="right">8,175	</td>
	<td align="right">5.24	</td>
	<td align="right">2,003,909,840	</td>
	<td align="right">5.12	</td>
	<td align="right">310	</td>
	<td align="right">4.87	</td>
	<td align="right">185,547.21	</td>
	<td align="right">23,193.40	</td>
</tr>
<tr class="jamn">
	<th align="right">2018-12-18</th>
	<td align="right">13,818	</td>
	<td align="right">8.85	</td>
	<td align="right">2,319,420,197	</td>
	<td align="right">5.92	</td>
	<td align="right">256	</td>
	<td align="right">4.02	</td>
	<td align="right">214,761.13	</td>
	<td align="right">26,845.14	</td>
</tr>
<tr class="udda">
	<th align="right">2018-12-19</th>
	<td align="right">7,059	</td>
	<td align="right">4.52	</td>
	<td align="right">1,473,506,735	</td>
	<td align="right">3.76	</td>
	<td align="right">220	</td>
	<td align="right">3.46	</td>
	<td align="right">136,435.81	</td>
	<td align="right">17,054.48	</td>
</tr>
<tr class="jamn">
	<th align="right">2018-12-20</th>
	<td align="right">7,280	</td>
	<td align="right">4.66	</td>
	<td align="right">2,253,394,619	</td>
	<td align="right">5.75	</td>
	<td align="right">244	</td>
	<td align="right">3.83	</td>
	<td align="right">208,647.65	</td>
	<td align="right">26,080.96	</td>
</tr>
<tr class="udda">
	<th align="right">2018-12-21</th>
	<td align="right">4,882	</td>
	<td align="right">3.13	</td>
	<td align="right">1,137,852,885	</td>
	<td align="right">2.90	</td>
	<td align="right">239	</td>
	<td align="right">3.76	</td>
	<td align="right">105,356.75	</td>
	<td align="right">13,169.59	</td>
</tr>
<tr class="jamn">
	<th align="right">2018-12-22</th>
	<td align="right">3,154	</td>
	<td align="right">2.02	</td>
	<td align="right">668,871,207	</td>
	<td align="right">1.71	</td>
	<td align="right">176	</td>
	<td align="right">2.77	</td>
	<td align="right">61,932.52	</td>
	<td align="right">7,741.56	</td>
</tr>
<tr class="udda">
	<th align="right">2018-12-23</th>
	<td align="right">2,211	</td>
	<td align="right">1.42	</td>
	<td align="right">479,004,084	</td>
	<td align="right">1.22	</td>
	<td align="right">152	</td>
	<td align="right">2.39	</td>
	<td align="right">44,352.23	</td>
	<td align="right">5,544.03	</td>
</tr>
<tr class="jamn">
	<th align="right">2018-12-24</th>
	<td align="right">1,820	</td>
	<td align="right">1.17	</td>
	<td align="right">272,584,532	</td>
	<td align="right">0.70	</td>
	<td align="right">136	</td>
	<td align="right">2.14	</td>
	<td align="right">25,239.31	</td>
	<td align="right">3,154.91	</td>
</tr>
<tr class="udda">
	<th align="right">2018-12-25</th>
	<td align="right">4,151	</td>
	<td align="right">2.66	</td>
	<td align="right">1,138,294,001	</td>
	<td align="right">2.91	</td>
	<td align="right">188	</td>
	<td align="right">2.95	</td>
	<td align="right">105,397.59	</td>
	<td align="right">13,174.70	</td>
</tr>
<tr class="jamn">
	<th align="right">2018-12-26</th>
	<td align="right">3,212	</td>
	<td align="right">2.06	</td>
	<td align="right">669,015,829	</td>
	<td align="right">1.71	</td>
	<td align="right">129	</td>
	<td align="right">2.03	</td>
	<td align="right">61,945.91	</td>
	<td align="right">7,743.24	</td>
</tr>
<tr class="udda">
	<th align="right">2018-12-27</th>
	<td align="right">2,758	</td>
	<td align="right">1.77	</td>
	<td align="right">830,966,253	</td>
	<td align="right">2.12	</td>
	<td align="right">148	</td>
	<td align="right">2.33	</td>
	<td align="right">76,941.32	</td>
	<td align="right">9,617.66	</td>
</tr>
<tr class="jamn">
	<th align="right">2018-12-28</th>
	<td align="right">2,982	</td>
	<td align="right">1.91	</td>
	<td align="right">856,582,643	</td>
	<td align="right">2.19	</td>
	<td align="right">163	</td>
	<td align="right">2.56	</td>
	<td align="right">79,313.21	</td>
	<td align="right">9,914.15	</td>
</tr>
<tr class="udda">
	<th align="right">2018-12-29</th>
	<td align="right">3,493	</td>
	<td align="right">2.24	</td>
	<td align="right">1,005,456,946	</td>
	<td align="right">2.57	</td>
	<td align="right">166	</td>
	<td align="right">2.61	</td>
	<td align="right">93,097.87	</td>
	<td align="right">11,637.23	</td>
</tr>
<tr class="jamn">
	<th align="right">2018-12-30</th>
	<td align="right">2,529	</td>
	<td align="right">1.62	</td>
	<td align="right">748,415,059	</td>
	<td align="right">1.91	</td>
	<td align="right">109	</td>
	<td align="right">1.71	</td>
	<td align="right">69,297.69	</td>
	<td align="right">8,662.21	</td>
</tr>
<tr class="udda">
	<th align="right">2018-12-31</th>
	<td align="right">2,470	</td>
	<td align="right">1.58	</td>
	<td align="right">497,120,227	</td>
	<td align="right">1.27	</td>
	<td align="right">115	</td>
	<td align="right">1.81	</td>
	<td align="right">46,029.65	</td>
	<td align="right">5,753.71	</td>
</tr>

</tbody>
</table>

