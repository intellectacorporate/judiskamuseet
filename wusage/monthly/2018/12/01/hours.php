


<h2>Statistics for http://www.u5645470.fsdata.se, Month of 2018-12-1 to 2018-12-31</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Traffic by Hour</h3>
<p>Figures are averages for that hour
	of each day in the time period analyzed.</p>
<img src="h.png" width="468" height="351">
<br /><br />
<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Hour</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	<th scope='col'>Bits per Sec</th>
	<th scope='col'>Bytes per Sec</th>
	
	</tr>
</thead>
<tbody>
<tr class="jamnudda">
	<th align="right">00:00</th>
	<td align="right">106.84</td>
	<td align="right">2.12</td>
	<td align="right">33,507,275.65</td>
	<td align="right">2.65</td>
	<td align="right">3.50</td>
	<td align="right">1.71</td>
	<td align="right">74,460.61</td>
	<td align="right">9,307.58</td>
	
</tr>
<tr class="jamn">
	<th align="right">01:00</th>
	<td align="right">77.58</td>
	<td align="right">1.54</td>
	<td align="right">18,079,298.10</td>
	<td align="right">1.43</td>
	<td align="right">4.62</td>
	<td align="right">2.25</td>
	<td align="right">40,176.22</td>
	<td align="right">5,022.03</td>
	
</tr>
<tr class="udda">
	<th align="right">02:00</th>
	<td align="right">64.87</td>
	<td align="right">1.29</td>
	<td align="right">6,921,684.58</td>
	<td align="right">0.55</td>
	<td align="right">3.36</td>
	<td align="right">1.64</td>
	<td align="right">15,381.52</td>
	<td align="right">1,922.69</td>
	
</tr>
<tr class="jamn">
	<th align="right">03:00</th>
	<td align="right">71.58</td>
	<td align="right">1.42</td>
	<td align="right">12,326,885.00</td>
	<td align="right">0.98</td>
	<td align="right">4.07</td>
	<td align="right">1.98</td>
	<td align="right">27,393.08</td>
	<td align="right">3,424.13</td>
	
</tr>
<tr class="udda">
	<th align="right">04:00</th>
	<td align="right">74.81</td>
	<td align="right">1.49</td>
	<td align="right">9,231,249.19</td>
	<td align="right">0.73</td>
	<td align="right">5.02</td>
	<td align="right">2.44</td>
	<td align="right">20,513.89</td>
	<td align="right">2,564.24</td>
	
</tr>
<tr class="jamn">
	<th align="right">05:00</th>
	<td align="right">56.81</td>
	<td align="right">1.13</td>
	<td align="right">6,674,229.16</td>
	<td align="right">0.53</td>
	<td align="right">4.07</td>
	<td align="right">1.98</td>
	<td align="right">14,831.62</td>
	<td align="right">1,853.95</td>
	
</tr>
<tr class="udda">
	<th align="right">06:00</th>
	<td align="right">79.03</td>
	<td align="right">1.57</td>
	<td align="right">14,019,840.32</td>
	<td align="right">1.11</td>
	<td align="right">4.27</td>
	<td align="right">2.08</td>
	<td align="right">31,155.20</td>
	<td align="right">3,894.40</td>
	
</tr>
<tr class="jamn">
	<th align="right">07:00</th>
	<td align="right">98.87</td>
	<td align="right">1.96</td>
	<td align="right">26,870,457.61</td>
	<td align="right">2.13</td>
	<td align="right">5.17</td>
	<td align="right">2.52</td>
	<td align="right">59,712.13</td>
	<td align="right">7,464.02</td>
	
</tr>
<tr class="udda">
	<th align="right">08:00</th>
	<td align="right">233.29</td>
	<td align="right">4.63</td>
	<td align="right">53,644,733.23</td>
	<td align="right">4.24</td>
	<td align="right">6.83</td>
	<td align="right">3.33</td>
	<td align="right">119,210.52</td>
	<td align="right">14,901.31</td>
	
</tr>
<tr class="jamn">
	<th align="right">09:00</th>
	<td align="right">404.16</td>
	<td align="right">8.02</td>
	<td align="right">78,856,224.90</td>
	<td align="right">6.24</td>
	<td align="right">10.04</td>
	<td align="right">4.89</td>
	<td align="right">175,236.06</td>
	<td align="right">21,904.51</td>
	
</tr>
<tr class="udda">
	<th align="right">10:00</th>
	<td align="right">369.10</td>
	<td align="right">7.33</td>
	<td align="right">91,968,257.29</td>
	<td align="right">7.28</td>
	<td align="right">11.82</td>
	<td align="right">5.76</td>
	<td align="right">204,373.91</td>
	<td align="right">25,546.74</td>
	
</tr>
<tr class="jamn">
	<th align="right">11:00</th>
	<td align="right">371.52</td>
	<td align="right">7.38</td>
	<td align="right">89,452,219.65</td>
	<td align="right">7.08</td>
	<td align="right">12.06</td>
	<td align="right">5.88</td>
	<td align="right">198,782.71</td>
	<td align="right">24,847.84</td>
	
</tr>
<tr class="udda">
	<th align="right">12:00</th>
	<td align="right">348.16</td>
	<td align="right">6.91</td>
	<td align="right">91,277,125.77</td>
	<td align="right">7.22</td>
	<td align="right">10.92</td>
	<td align="right">5.32</td>
	<td align="right">202,838.06</td>
	<td align="right">25,354.76</td>
	
</tr>
<tr class="jamn">
	<th align="right">13:00</th>
	<td align="right">340.87</td>
	<td align="right">6.77</td>
	<td align="right">113,180,692.58</td>
	<td align="right">8.96</td>
	<td align="right">11.72</td>
	<td align="right">5.71</td>
	<td align="right">251,512.65</td>
	<td align="right">31,439.08</td>
	
</tr>
<tr class="udda">
	<th align="right">14:00</th>
	<td align="right">344.03</td>
	<td align="right">6.83</td>
	<td align="right">101,623,834.39</td>
	<td align="right">8.04</td>
	<td align="right">12.58</td>
	<td align="right">6.13</td>
	<td align="right">225,830.74</td>
	<td align="right">28,228.84</td>
	
</tr>
<tr class="jamn">
	<th align="right">15:00</th>
	<td align="right">310.52</td>
	<td align="right">6.16</td>
	<td align="right">82,937,274.32</td>
	<td align="right">6.56</td>
	<td align="right">11.53</td>
	<td align="right">5.62</td>
	<td align="right">184,305.05</td>
	<td align="right">23,038.13</td>
	
</tr>
<tr class="udda">
	<th align="right">16:00</th>
	<td align="right">299.84</td>
	<td align="right">5.95</td>
	<td align="right">66,103,598.74</td>
	<td align="right">5.23</td>
	<td align="right">10.71</td>
	<td align="right">5.22</td>
	<td align="right">146,896.89</td>
	<td align="right">18,362.11</td>
	
</tr>
<tr class="jamn">
	<th align="right">17:00</th>
	<td align="right">221.90</td>
	<td align="right">4.41</td>
	<td align="right">56,533,104.58</td>
	<td align="right">4.47</td>
	<td align="right">11.20</td>
	<td align="right">5.46</td>
	<td align="right">125,629.12</td>
	<td align="right">15,703.64</td>
	
</tr>
<tr class="udda">
	<th align="right">18:00</th>
	<td align="right">208.68</td>
	<td align="right">4.14</td>
	<td align="right">58,600,460.61</td>
	<td align="right">4.64</td>
	<td align="right">9.89</td>
	<td align="right">4.82</td>
	<td align="right">130,223.25</td>
	<td align="right">16,277.91</td>
	
</tr>
<tr class="jamn">
	<th align="right">19:00</th>
	<td align="right">185.45</td>
	<td align="right">3.68</td>
	<td align="right">53,492,276.42</td>
	<td align="right">4.23</td>
	<td align="right">10.53</td>
	<td align="right">5.13</td>
	<td align="right">118,871.73</td>
	<td align="right">14,858.97</td>
	
</tr>
<tr class="udda">
	<th align="right">20:00</th>
	<td align="right">226.26</td>
	<td align="right">4.49</td>
	<td align="right">58,468,467.29</td>
	<td align="right">4.63</td>
	<td align="right">10.13</td>
	<td align="right">4.94</td>
	<td align="right">129,929.93</td>
	<td align="right">16,241.24</td>
	
</tr>
<tr class="jamn">
	<th align="right">21:00</th>
	<td align="right">250.35</td>
	<td align="right">4.97</td>
	<td align="right">64,733,715.03</td>
	<td align="right">5.12</td>
	<td align="right">12.09</td>
	<td align="right">5.89</td>
	<td align="right">143,852.70</td>
	<td align="right">17,981.59</td>
	
</tr>
<tr class="udda">
	<th align="right">22:00</th>
	<td align="right">152.61</td>
	<td align="right">3.03</td>
	<td align="right">36,482,676.45</td>
	<td align="right">2.89</td>
	<td align="right">8.99</td>
	<td align="right">4.38</td>
	<td align="right">81,072.61</td>
	<td align="right">10,134.08</td>
	
</tr>
<tr class="jamn">
	<th align="right">23:00</th>
	<td align="right">139.68</td>
	<td align="right">2.77</td>
	<td align="right">38,762,031.74</td>
	<td align="right">3.07</td>
	<td align="right">10.13</td>
	<td align="right">4.94</td>
	<td align="right">86,137.85</td>
	<td align="right">10,767.23</td>
	
</tr>

</tbody>
</table>

