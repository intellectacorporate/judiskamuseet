


<h2>Statistics for http://www.u5645470.fsdata.se, Month of 2018-12-1 to 2018-12-31</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 10 of 292 Browsers (User Agents),
Sorted by Access Count

</h3>
<p></p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Product</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th class="hogerstalld">1</th>
	<td>
	Safari/537.36		
	</td>
	<td align="right">38,261</td>
	<td align="right">24.64</td>
	<td align="right">15,092,037,224</td>
	<td align="right">38.66</td>
	<td align="right">1,918</td>
	<td align="right">30.38</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	Safari/537.36 Edge/17.17134		
	</td>
	<td align="right">32,780</td>
	<td align="right">21.11</td>
	<td align="right">6,073,726,998</td>
	<td align="right">15.56</td>
	<td align="right">156</td>
	<td align="right">2.47</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	Safari/604.1		
	</td>
	<td align="right">19,902</td>
	<td align="right">12.82</td>
	<td align="right">7,444,046,510</td>
	<td align="right">19.07</td>
	<td align="right">873</td>
	<td align="right">13.82</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	Mozilla/5.0 		
	</td>
	<td align="right">16,319</td>
	<td align="right">10.51</td>
	<td align="right">3,725,865,441</td>
	<td align="right">9.54</td>
	<td align="right">649</td>
	<td align="right">10.28</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	UptimeRobot/2.0		
	</td>
	<td align="right">7,863</td>
	<td align="right">5.06</td>
	<td align="right">16,124</td>
	<td align="right">0.00</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	bingbot/2.0		
	</td>
	<td align="right">5,347</td>
	<td align="right">3.44</td>
	<td align="right">240,200,990</td>
	<td align="right">0.62</td>
	<td align="right">850</td>
	<td align="right">13.47</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	Safari/605.1.15		
	</td>
	<td align="right">4,918</td>
	<td align="right">3.17</td>
	<td align="right">1,945,835,858</td>
	<td align="right">4.98</td>
	<td align="right">191</td>
	<td align="right">3.03</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	curl/7.54.0		
	</td>
	<td align="right">3,417</td>
	<td align="right">2.20</td>
	<td align="right">55,776,045</td>
	<td align="right">0.14</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	WordPress/4.9.8		
	</td>
	<td align="right">2,830</td>
	<td align="right">1.82</td>
	<td align="right">72,030</td>
	<td align="right">0.00</td>
	<td align="right">46</td>
	<td align="right">0.73</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">10</th>
	<td>
	WordPress/5.0.2		
	</td>
	<td align="right">2,277</td>
	<td align="right">1.47</td>
	<td align="right">44,844</td>
	<td align="right">0.00</td>
	<td align="right">28</td>
	<td align="right">0.44</td>
</tr>

</tbody>
</table>

