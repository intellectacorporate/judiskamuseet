


<h2>Statistics for http://www.u5645470.fsdata.se, Month of 2018-8-1 to 2018-8-31</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 10 of 18 Operating Systems,
Sorted by Access Count

</h3>
<p></p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Product</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th class="hogerstalld">1</th>
	<td>
	Mac OS X		
	</td>
	<td align="right">75,733</td>
	<td align="right">61.18</td>
	<td align="right">21,059,725,857</td>
	<td align="right">67.07</td>
	<td align="right">3,194</td>
	<td align="right">37.77</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	Other		
	</td>
	<td align="right">31,352</td>
	<td align="right">25.33</td>
	<td align="right">6,896,809,789</td>
	<td align="right">21.97</td>
	<td align="right">2,035</td>
	<td align="right">24.07</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	Windows NT		
	</td>
	<td align="right">14,388</td>
	<td align="right">11.62</td>
	<td align="right">3,224,280,514</td>
	<td align="right">10.27</td>
	<td align="right">3,111</td>
	<td align="right">36.80</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	Windows XP		
	</td>
	<td align="right">814</td>
	<td align="right">0.66</td>
	<td align="right">21,813,724</td>
	<td align="right">0.07</td>
	<td align="right">26</td>
	<td align="right">0.31</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	Windows Codename Longhorn		
	</td>
	<td align="right">661</td>
	<td align="right">0.53</td>
	<td align="right">86,916,439</td>
	<td align="right">0.28</td>
	<td align="right">64</td>
	<td align="right">0.76</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	Linux		
	</td>
	<td align="right">533</td>
	<td align="right">0.43</td>
	<td align="right">94,509,301</td>
	<td align="right">0.30</td>
	<td align="right">13</td>
	<td align="right">0.16</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	Windows 2003		
	</td>
	<td align="right">143</td>
	<td align="right">0.12</td>
	<td align="right">188,610</td>
	<td align="right">0.00</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	Windows 2000		
	</td>
	<td align="right">100</td>
	<td align="right">0.08</td>
	<td align="right">12,219,086</td>
	<td align="right">0.04</td>
	<td align="right">5</td>
	<td align="right">0.07</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	Windows 95		
	</td>
	<td align="right">15</td>
	<td align="right">0.01</td>
	<td align="right">463,044</td>
	<td align="right">0.00</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">10</th>
	<td>
	Mac OS		
	</td>
	<td align="right">12</td>
	<td align="right">0.01</td>
	<td align="right">577,468</td>
	<td align="right">0.00</td>
	<td align="right">2</td>
	<td align="right">0.03</td>
</tr>

</tbody>
</table>

