


<h2>Statistics for http://www.u5645470.fsdata.se, Month of 2018-8-1 to 2018-8-31</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Traffic by Hour</h3>
<p>Figures are averages for that hour
	of each day in the time period analyzed.</p>
<img src="h.png" width="468" height="351">
<br /><br />
<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Hour</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	<th scope='col'>Bits per Sec</th>
	<th scope='col'>Bytes per Sec</th>
	
	</tr>
</thead>
<tbody>
<tr class="jamnudda">
	<th align="right">00:00</th>
	<td align="right">107.19</td>
	<td align="right">2.67</td>
	<td align="right">28,478,632.81</td>
	<td align="right">2.74</td>
	<td align="right">5.94</td>
	<td align="right">2.18</td>
	<td align="right">63,285.85</td>
	<td align="right">7,910.73</td>
	
</tr>
<tr class="jamn">
	<th align="right">01:00</th>
	<td align="right">85.94</td>
	<td align="right">2.14</td>
	<td align="right">15,323,682.68</td>
	<td align="right">1.48</td>
	<td align="right">7.30</td>
	<td align="right">2.68</td>
	<td align="right">34,052.63</td>
	<td align="right">4,256.58</td>
	
</tr>
<tr class="udda">
	<th align="right">02:00</th>
	<td align="right">86.23</td>
	<td align="right">2.15</td>
	<td align="right">14,553,899.65</td>
	<td align="right">1.40</td>
	<td align="right">6.81</td>
	<td align="right">2.50</td>
	<td align="right">32,342.00</td>
	<td align="right">4,042.75</td>
	
</tr>
<tr class="jamn">
	<th align="right">03:00</th>
	<td align="right">66.32</td>
	<td align="right">1.65</td>
	<td align="right">9,046,110.10</td>
	<td align="right">0.87</td>
	<td align="right">6.76</td>
	<td align="right">2.48</td>
	<td align="right">20,102.47</td>
	<td align="right">2,512.81</td>
	
</tr>
<tr class="udda">
	<th align="right">04:00</th>
	<td align="right">69.94</td>
	<td align="right">1.74</td>
	<td align="right">10,682,593.13</td>
	<td align="right">1.03</td>
	<td align="right">8.36</td>
	<td align="right">3.07</td>
	<td align="right">23,739.10</td>
	<td align="right">2,967.39</td>
	
</tr>
<tr class="jamn">
	<th align="right">05:00</th>
	<td align="right">55.52</td>
	<td align="right">1.38</td>
	<td align="right">9,162,530.16</td>
	<td align="right">0.88</td>
	<td align="right">5.34</td>
	<td align="right">1.96</td>
	<td align="right">20,361.18</td>
	<td align="right">2,545.15</td>
	
</tr>
<tr class="udda">
	<th align="right">06:00</th>
	<td align="right">66.10</td>
	<td align="right">1.65</td>
	<td align="right">9,435,390.06</td>
	<td align="right">0.91</td>
	<td align="right">6.66</td>
	<td align="right">2.44</td>
	<td align="right">20,967.53</td>
	<td align="right">2,620.94</td>
	
</tr>
<tr class="jamn">
	<th align="right">07:00</th>
	<td align="right">112.35</td>
	<td align="right">2.80</td>
	<td align="right">25,438,245.23</td>
	<td align="right">2.45</td>
	<td align="right">7.25</td>
	<td align="right">2.66</td>
	<td align="right">56,529.43</td>
	<td align="right">7,066.18</td>
	
</tr>
<tr class="udda">
	<th align="right">08:00</th>
	<td align="right">204.81</td>
	<td align="right">5.10</td>
	<td align="right">51,248,717.55</td>
	<td align="right">4.93</td>
	<td align="right">11.07</td>
	<td align="right">4.06</td>
	<td align="right">113,886.04</td>
	<td align="right">14,235.75</td>
	
</tr>
<tr class="jamn">
	<th align="right">09:00</th>
	<td align="right">240.42</td>
	<td align="right">5.99</td>
	<td align="right">79,671,001.71</td>
	<td align="right">7.67</td>
	<td align="right">12.04</td>
	<td align="right">4.41</td>
	<td align="right">177,046.67</td>
	<td align="right">22,130.83</td>
	
</tr>
<tr class="udda">
	<th align="right">10:00</th>
	<td align="right">222.26</td>
	<td align="right">5.53</td>
	<td align="right">69,366,469.90</td>
	<td align="right">6.68</td>
	<td align="right">13.90</td>
	<td align="right">5.09</td>
	<td align="right">154,147.71</td>
	<td align="right">19,268.46</td>
	
</tr>
<tr class="jamn">
	<th align="right">11:00</th>
	<td align="right">237.90</td>
	<td align="right">5.92</td>
	<td align="right">76,027,161.10</td>
	<td align="right">7.32</td>
	<td align="right">15.18</td>
	<td align="right">5.56</td>
	<td align="right">168,949.25</td>
	<td align="right">21,118.66</td>
	
</tr>
<tr class="udda">
	<th align="right">12:00</th>
	<td align="right">222.45</td>
	<td align="right">5.54</td>
	<td align="right">60,570,672.13</td>
	<td align="right">5.83</td>
	<td align="right">14.54</td>
	<td align="right">5.33</td>
	<td align="right">134,601.49</td>
	<td align="right">16,825.19</td>
	
</tr>
<tr class="jamn">
	<th align="right">13:00</th>
	<td align="right">209.48</td>
	<td align="right">5.22</td>
	<td align="right">61,700,921.03</td>
	<td align="right">5.94</td>
	<td align="right">13.49</td>
	<td align="right">4.94</td>
	<td align="right">137,113.16</td>
	<td align="right">17,139.14</td>
	
</tr>
<tr class="udda">
	<th align="right">14:00</th>
	<td align="right">233.58</td>
	<td align="right">5.82</td>
	<td align="right">63,228,171.00</td>
	<td align="right">6.09</td>
	<td align="right">12.04</td>
	<td align="right">4.41</td>
	<td align="right">140,507.05</td>
	<td align="right">17,563.38</td>
	
</tr>
<tr class="jamn">
	<th align="right">15:00</th>
	<td align="right">264.39</td>
	<td align="right">6.58</td>
	<td align="right">60,275,045.26</td>
	<td align="right">5.80</td>
	<td align="right">13.67</td>
	<td align="right">5.01</td>
	<td align="right">133,944.55</td>
	<td align="right">16,743.07</td>
	
</tr>
<tr class="udda">
	<th align="right">16:00</th>
	<td align="right">243.81</td>
	<td align="right">6.07</td>
	<td align="right">62,260,552.65</td>
	<td align="right">6.00</td>
	<td align="right">14.50</td>
	<td align="right">5.31</td>
	<td align="right">138,356.78</td>
	<td align="right">17,294.60</td>
	
</tr>
<tr class="jamn">
	<th align="right">17:00</th>
	<td align="right">232.55</td>
	<td align="right">5.79</td>
	<td align="right">47,164,494.61</td>
	<td align="right">4.54</td>
	<td align="right">13.52</td>
	<td align="right">4.95</td>
	<td align="right">104,809.99</td>
	<td align="right">13,101.25</td>
	
</tr>
<tr class="udda">
	<th align="right">18:00</th>
	<td align="right">196.97</td>
	<td align="right">4.90</td>
	<td align="right">43,958,385.35</td>
	<td align="right">4.23</td>
	<td align="right">11.93</td>
	<td align="right">4.37</td>
	<td align="right">97,685.30</td>
	<td align="right">12,210.66</td>
	
</tr>
<tr class="jamn">
	<th align="right">19:00</th>
	<td align="right">167.16</td>
	<td align="right">4.16</td>
	<td align="right">39,726,652.52</td>
	<td align="right">3.83</td>
	<td align="right">12.62</td>
	<td align="right">4.63</td>
	<td align="right">88,281.45</td>
	<td align="right">11,035.18</td>
	
</tr>
<tr class="udda">
	<th align="right">20:00</th>
	<td align="right">197.42</td>
	<td align="right">4.92</td>
	<td align="right">50,746,619.48</td>
	<td align="right">4.89</td>
	<td align="right">14.03</td>
	<td align="right">5.14</td>
	<td align="right">112,770.27</td>
	<td align="right">14,096.28</td>
	
</tr>
<tr class="jamn">
	<th align="right">21:00</th>
	<td align="right">188.23</td>
	<td align="right">4.69</td>
	<td align="right">50,954,312.10</td>
	<td align="right">4.91</td>
	<td align="right">16.14</td>
	<td align="right">5.92</td>
	<td align="right">113,231.80</td>
	<td align="right">14,153.98</td>
	
</tr>
<tr class="udda">
	<th align="right">22:00</th>
	<td align="right">163.23</td>
	<td align="right">4.06</td>
	<td align="right">52,197,322.16</td>
	<td align="right">5.03</td>
	<td align="right">13.99</td>
	<td align="right">5.13</td>
	<td align="right">115,994.05</td>
	<td align="right">14,499.26</td>
	
</tr>
<tr class="jamn">
	<th align="right">23:00</th>
	<td align="right">142.06</td>
	<td align="right">3.54</td>
	<td align="right">47,268,570.00</td>
	<td align="right">4.55</td>
	<td align="right">15.73</td>
	<td align="right">5.77</td>
	<td align="right">105,041.27</td>
	<td align="right">13,130.16</td>
	
</tr>

</tbody>
</table>

