


<h2>Statistics for http://www.u5645470.fsdata.se, Month of 2018-8-1 to 2018-8-31</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Traffic by Day</h3>
<p>The continuous line represents a 7-day moving average.</p>
<img src="d.png" width="468" height="351">
<br /><br />


<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Date</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	<th scope='col'>Bits per Sec</th>
	<th scope='col'>Bytes per Sec</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th align="right">2018-8-1</th>
	<td align="right">2,372	</td>
	<td align="right">1.91	</td>
	<td align="right">569,121,415	</td>
	<td align="right">1.77	</td>
	<td align="right">184	</td>
	<td align="right">2.18	</td>
	<td align="right">52,696.43	</td>
	<td align="right">6,587.05	</td>
</tr>
<tr class="jamn">
	<th align="right">2018-8-2</th>
	<td align="right">3,351	</td>
	<td align="right">2.69	</td>
	<td align="right">740,500,682	</td>
	<td align="right">2.30	</td>
	<td align="right">236	</td>
	<td align="right">2.79	</td>
	<td align="right">68,564.88	</td>
	<td align="right">8,570.61	</td>
</tr>
<tr class="udda">
	<th align="right">2018-8-3</th>
	<td align="right">3,200	</td>
	<td align="right">2.57	</td>
	<td align="right">666,005,770	</td>
	<td align="right">2.07	</td>
	<td align="right">253	</td>
	<td align="right">2.99	</td>
	<td align="right">61,667.20	</td>
	<td align="right">7,708.40	</td>
</tr>
<tr class="jamn">
	<th align="right">2018-8-4</th>
	<td align="right">2,357	</td>
	<td align="right">1.89	</td>
	<td align="right">539,138,682	</td>
	<td align="right">1.67	</td>
	<td align="right">184	</td>
	<td align="right">2.18	</td>
	<td align="right">49,920.25	</td>
	<td align="right">6,240.03	</td>
</tr>
<tr class="udda">
	<th align="right">2018-8-5</th>
	<td align="right">2,969	</td>
	<td align="right">2.38	</td>
	<td align="right">752,837,231	</td>
	<td align="right">2.34	</td>
	<td align="right">327	</td>
	<td align="right">3.87	</td>
	<td align="right">69,707.15	</td>
	<td align="right">8,713.39	</td>
</tr>
<tr class="jamn">
	<th align="right">2018-8-6</th>
	<td align="right">3,458	</td>
	<td align="right">2.78	</td>
	<td align="right">846,175,619	</td>
	<td align="right">2.63	</td>
	<td align="right">455	</td>
	<td align="right">5.38	</td>
	<td align="right">78,349.59	</td>
	<td align="right">9,793.70	</td>
</tr>
<tr class="udda">
	<th align="right">2018-8-7</th>
	<td align="right">3,870	</td>
	<td align="right">3.11	</td>
	<td align="right">862,673,451	</td>
	<td align="right">2.68	</td>
	<td align="right">619	</td>
	<td align="right">7.32	</td>
	<td align="right">79,877.17	</td>
	<td align="right">9,984.65	</td>
</tr>
<tr class="jamn">
	<th align="right">2018-8-8</th>
	<td align="right">4,447	</td>
	<td align="right">3.57	</td>
	<td align="right">1,159,634,638	</td>
	<td align="right">3.60	</td>
	<td align="right">525	</td>
	<td align="right">6.21	</td>
	<td align="right">107,373.58	</td>
	<td align="right">13,421.70	</td>
</tr>
<tr class="udda">
	<th align="right">2018-8-9</th>
	<td align="right">8,277	</td>
	<td align="right">6.65	</td>
	<td align="right">986,575,948	</td>
	<td align="right">3.06	</td>
	<td align="right">589	</td>
	<td align="right">6.96	</td>
	<td align="right">91,349.62	</td>
	<td align="right">11,418.70	</td>
</tr>
<tr class="jamn">
	<th align="right">2018-8-10</th>
	<td align="right">4,884	</td>
	<td align="right">3.92	</td>
	<td align="right">1,226,428,928	</td>
	<td align="right">3.81	</td>
	<td align="right">373	</td>
	<td align="right">4.41	</td>
	<td align="right">113,558.23	</td>
	<td align="right">14,194.78	</td>
</tr>
<tr class="udda">
	<th align="right">2018-8-11</th>
	<td align="right">3,782	</td>
	<td align="right">3.04	</td>
	<td align="right">1,183,169,921	</td>
	<td align="right">3.68	</td>
	<td align="right">216	</td>
	<td align="right">2.55	</td>
	<td align="right">109,552.77	</td>
	<td align="right">13,694.10	</td>
</tr>
<tr class="jamn">
	<th align="right">2018-8-12</th>
	<td align="right">3,338	</td>
	<td align="right">2.68	</td>
	<td align="right">1,171,584,052	</td>
	<td align="right">3.64	</td>
	<td align="right">203	</td>
	<td align="right">2.40	</td>
	<td align="right">108,480.00	</td>
	<td align="right">13,560.00	</td>
</tr>
<tr class="udda">
	<th align="right">2018-8-13</th>
	<td align="right">4,438	</td>
	<td align="right">3.56	</td>
	<td align="right">1,179,675,833	</td>
	<td align="right">3.66	</td>
	<td align="right">213	</td>
	<td align="right">2.52	</td>
	<td align="right">109,229.24	</td>
	<td align="right">13,653.66	</td>
</tr>
<tr class="jamn">
	<th align="right">2018-8-14</th>
	<td align="right">4,126	</td>
	<td align="right">3.31	</td>
	<td align="right">1,337,131,084	</td>
	<td align="right">4.15	</td>
	<td align="right">201	</td>
	<td align="right">2.38	</td>
	<td align="right">123,808.43	</td>
	<td align="right">15,476.05	</td>
</tr>
<tr class="udda">
	<th align="right">2018-8-15</th>
	<td align="right">3,847	</td>
	<td align="right">3.09	</td>
	<td align="right">1,270,960,364	</td>
	<td align="right">3.95	</td>
	<td align="right">180	</td>
	<td align="right">2.13	</td>
	<td align="right">117,681.52	</td>
	<td align="right">14,710.19	</td>
</tr>
<tr class="jamn">
	<th align="right">2018-8-16</th>
	<td align="right">3,008	</td>
	<td align="right">2.42	</td>
	<td align="right">929,876,552	</td>
	<td align="right">2.89	</td>
	<td align="right">179	</td>
	<td align="right">2.12	</td>
	<td align="right">86,099.68	</td>
	<td align="right">10,762.46	</td>
</tr>
<tr class="udda">
	<th align="right">2018-8-17</th>
	<td align="right">4,237	</td>
	<td align="right">3.40	</td>
	<td align="right">942,261,909	</td>
	<td align="right">2.93	</td>
	<td align="right">204	</td>
	<td align="right">2.41	</td>
	<td align="right">87,246.47	</td>
	<td align="right">10,905.81	</td>
</tr>
<tr class="jamn">
	<th align="right">2018-8-18</th>
	<td align="right">2,884	</td>
	<td align="right">2.32	</td>
	<td align="right">858,036,567	</td>
	<td align="right">2.67	</td>
	<td align="right">177	</td>
	<td align="right">2.09	</td>
	<td align="right">79,447.83	</td>
	<td align="right">9,930.98	</td>
</tr>
<tr class="udda">
	<th align="right">2018-8-19</th>
	<td align="right">3,434	</td>
	<td align="right">2.76	</td>
	<td align="right">896,372,693	</td>
	<td align="right">2.78	</td>
	<td align="right">197	</td>
	<td align="right">2.33	</td>
	<td align="right">82,997.47	</td>
	<td align="right">10,374.68	</td>
</tr>
<tr class="jamn">
	<th align="right">2018-8-20</th>
	<td align="right">3,400	</td>
	<td align="right">2.73	</td>
	<td align="right">1,049,108,089	</td>
	<td align="right">3.26	</td>
	<td align="right">187	</td>
	<td align="right">2.21	</td>
	<td align="right">97,139.64	</td>
	<td align="right">12,142.45	</td>
</tr>
<tr class="udda">
	<th align="right">2018-8-21</th>
	<td align="right">7,412	</td>
	<td align="right">5.95	</td>
	<td align="right">1,040,726,794	</td>
	<td align="right">3.23	</td>
	<td align="right">196	</td>
	<td align="right">2.32	</td>
	<td align="right">96,363.59	</td>
	<td align="right">12,045.45	</td>
</tr>
<tr class="jamn">
	<th align="right">2018-8-22</th>
	<td align="right">5,709	</td>
	<td align="right">4.59	</td>
	<td align="right">1,402,937,945	</td>
	<td align="right">4.36	</td>
	<td align="right">244	</td>
	<td align="right">2.89	</td>
	<td align="right">129,901.66	</td>
	<td align="right">16,237.71	</td>
</tr>
<tr class="udda">
	<th align="right">2018-8-23</th>
	<td align="right">4,359	</td>
	<td align="right">3.50	</td>
	<td align="right">1,625,580,747	</td>
	<td align="right">5.05	</td>
	<td align="right">290	</td>
	<td align="right">3.43	</td>
	<td align="right">150,516.74	</td>
	<td align="right">18,814.59	</td>
</tr>
<tr class="jamn">
	<th align="right">2018-8-24</th>
	<td align="right">3,483	</td>
	<td align="right">2.80	</td>
	<td align="right">970,364,212	</td>
	<td align="right">3.01	</td>
	<td align="right">235	</td>
	<td align="right">2.78	</td>
	<td align="right">89,848.54	</td>
	<td align="right">11,231.07	</td>
</tr>
<tr class="udda">
	<th align="right">2018-8-25</th>
	<td align="right">4,100	</td>
	<td align="right">3.29	</td>
	<td align="right">1,050,897,515	</td>
	<td align="right">3.26	</td>
	<td align="right">217	</td>
	<td align="right">2.57	</td>
	<td align="right">97,305.33	</td>
	<td align="right">12,163.17	</td>
</tr>
<tr class="jamn">
	<th align="right">2018-8-26</th>
	<td align="right">3,652	</td>
	<td align="right">2.93	</td>
	<td align="right">1,313,157,701	</td>
	<td align="right">4.08	</td>
	<td align="right">207	</td>
	<td align="right">2.45	</td>
	<td align="right">121,588.68	</td>
	<td align="right">15,198.58	</td>
</tr>
<tr class="udda">
	<th align="right">2018-8-27</th>
	<td align="right">5,434	</td>
	<td align="right">4.36	</td>
	<td align="right">1,108,696,896	</td>
	<td align="right">3.44	</td>
	<td align="right">377	</td>
	<td align="right">4.46	</td>
	<td align="right">102,657.12	</td>
	<td align="right">12,832.14	</td>
</tr>
<tr class="jamn">
	<th align="right">2018-8-28</th>
	<td align="right">4,196	</td>
	<td align="right">3.37	</td>
	<td align="right">1,478,968,787	</td>
	<td align="right">4.59	</td>
	<td align="right">279	</td>
	<td align="right">3.30	</td>
	<td align="right">136,941.55	</td>
	<td align="right">17,117.69	</td>
</tr>
<tr class="udda">
	<th align="right">2018-8-29</th>
	<td align="right">3,502	</td>
	<td align="right">2.81	</td>
	<td align="right">1,023,075,944	</td>
	<td align="right">3.18	</td>
	<td align="right">248	</td>
	<td align="right">2.93	</td>
	<td align="right">94,729.25	</td>
	<td align="right">11,841.16	</td>
</tr>
<tr class="jamn">
	<th align="right">2018-8-30</th>
	<td align="right">3,806	</td>
	<td align="right">3.06	</td>
	<td align="right">1,187,714,642	</td>
	<td align="right">3.69	</td>
	<td align="right">247	</td>
	<td align="right">2.92	</td>
	<td align="right">109,973.58	</td>
	<td align="right">13,746.70	</td>
</tr>
<tr class="udda">
	<th align="right">2018-8-31</th>
	<td align="right">3,173	</td>
	<td align="right">2.55	</td>
	<td align="right">823,680,112	</td>
	<td align="right">2.56	</td>
	<td align="right">215	</td>
	<td align="right">2.54	</td>
	<td align="right">76,266.68	</td>
	<td align="right">9,533.33	</td>
</tr>

</tbody>
</table>

