


<h2>Statistics for http://www.u5645470.fsdata.se, Month of 2018-8-1 to 2018-8-31</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 10 of 23 File Types (Extensions), 
Sorted by Access Count

</h3>
<p>Individual file types as determined by file extensions. All URLs that
do not contain an extension are counted as directories.</p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Type</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th class="hogerstalld">1</th>
	<td>
	Directory (folder)		
	</td>
	<td align="right">33,371</td>
	<td align="right">26.80</td>
	<td align="right">463,916,933</td>
	<td align="right">1.44</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	js		
	</td>
	<td align="right">23,995</td>
	<td align="right">19.27</td>
	<td align="right">294,010,110</td>
	<td align="right">0.91</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	jpg		
	</td>
	<td align="right">22,230</td>
	<td align="right">17.85</td>
	<td align="right">28,850,327,985</td>
	<td align="right">89.62</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	png		
	</td>
	<td align="right">15,071</td>
	<td align="right">12.10</td>
	<td align="right">1,539,468,226</td>
	<td align="right">4.78</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	css		
	</td>
	<td align="right">12,404</td>
	<td align="right">9.96</td>
	<td align="right">246,579,324</td>
	<td align="right">0.77</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	php		
	</td>
	<td align="right">12,154</td>
	<td align="right">9.76</td>
	<td align="right">168,824,596</td>
	<td align="right">0.52</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	ttf		
	</td>
	<td align="right">2,213</td>
	<td align="right">1.78</td>
	<td align="right">196,769,379</td>
	<td align="right">0.61</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	txt		
	</td>
	<td align="right">1,682</td>
	<td align="right">1.35</td>
	<td align="right">114,936</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	gif		
	</td>
	<td align="right">435</td>
	<td align="right">0.35</td>
	<td align="right">2,202,938</td>
	<td align="right">0.01</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">10</th>
	<td>
	jpeg		
	</td>
	<td align="right">260</td>
	<td align="right">0.21</td>
	<td align="right">47,906,288</td>
	<td align="right">0.15</td>
</tr>

</tbody>
</table>

