


<h2>Statistics for http://www.u5645470.fsdata.se, Month of 2018-8-1 to 2018-8-31</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 20 of 8,393 Visitor Sites,
Sorted by Access Count

</h3>
<p></p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Site</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="jamnudda">
	<th class="hogerstalld">1</th>
	<td>
	63.143.42.244		
	</td>
	<td align="right">8,133</td>
	<td align="right">6.53</td>
	<td align="right">60,652</td>
	<td align="right">0.00</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	46.182.207.79		
	</td>
	<td align="right">6,252</td>
	<td align="right">5.02</td>
	<td align="right">268,851,227</td>
	<td align="right">0.84</td>
	<td align="right">16</td>
	<td align="right">0.20</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	89.221.250.27		
	</td>
	<td align="right">5,642</td>
	<td align="right">4.53</td>
	<td align="right">140,329</td>
	<td align="right">0.00</td>
	<td align="right">60</td>
	<td align="right">0.72</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	85.228.32.74		
	</td>
	<td align="right">1,995</td>
	<td align="right">1.60</td>
	<td align="right">52,787,054</td>
	<td align="right">0.16</td>
	<td align="right">11</td>
	<td align="right">0.13</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	80.216.27.25		
	</td>
	<td align="right">1,666</td>
	<td align="right">1.34</td>
	<td align="right">108,590,647</td>
	<td align="right">0.34</td>
	<td align="right">19</td>
	<td align="right">0.23</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	148.160.250.6		
	</td>
	<td align="right">770</td>
	<td align="right">0.62</td>
	<td align="right">334,943,424</td>
	<td align="right">1.04</td>
	<td align="right">31</td>
	<td align="right">0.38</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	90.229.237.5		
	</td>
	<td align="right">758</td>
	<td align="right">0.61</td>
	<td align="right">85,305,302</td>
	<td align="right">0.26</td>
	<td align="right">12</td>
	<td align="right">0.15</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	151.177.34.22		
	</td>
	<td align="right">593</td>
	<td align="right">0.48</td>
	<td align="right">72,077,527</td>
	<td align="right">0.22</td>
	<td align="right">6</td>
	<td align="right">0.08</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	2.249.106.47		
	</td>
	<td align="right">486</td>
	<td align="right">0.39</td>
	<td align="right">204,746,212</td>
	<td align="right">0.64</td>
	<td align="right">9</td>
	<td align="right">0.11</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">10</th>
	<td>
	46.59.25.88		
	</td>
	<td align="right">473</td>
	<td align="right">0.38</td>
	<td align="right">100,546,900</td>
	<td align="right">0.31</td>
	<td align="right">11</td>
	<td align="right">0.14</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">11</th>
	<td>
	66.249.64.17		
	</td>
	<td align="right">472</td>
	<td align="right">0.38</td>
	<td align="right">15,613,550</td>
	<td align="right">0.05</td>
	<td align="right">90</td>
	<td align="right">1.07</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">12</th>
	<td>
	23.101.169.3		
	</td>
	<td align="right">431</td>
	<td align="right">0.35</td>
	<td align="right">6,614,017</td>
	<td align="right">0.02</td>
	<td align="right">34</td>
	<td align="right">0.41</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">13</th>
	<td>
	66.249.66.214		
	</td>
	<td align="right">430</td>
	<td align="right">0.35</td>
	<td align="right">7,634,743</td>
	<td align="right">0.02</td>
	<td align="right">83</td>
	<td align="right">0.99</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">14</th>
	<td>
	89.160.32.242		
	</td>
	<td align="right">406</td>
	<td align="right">0.33</td>
	<td align="right">37,896,557</td>
	<td align="right">0.12</td>
	<td align="right">5</td>
	<td align="right">0.07</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">15</th>
	<td>
	216.244.66.227		
	</td>
	<td align="right">376</td>
	<td align="right">0.30</td>
	<td align="right">100,219,660</td>
	<td align="right">0.31</td>
	<td align="right">183</td>
	<td align="right">2.17</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">16</th>
	<td>
	66.249.66.216		
	</td>
	<td align="right">345</td>
	<td align="right">0.28</td>
	<td align="right">3,615,215</td>
	<td align="right">0.01</td>
	<td align="right">66</td>
	<td align="right">0.78</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">17</th>
	<td>
	40.77.167.89		
	</td>
	<td align="right">342</td>
	<td align="right">0.27</td>
	<td align="right">15,898,239</td>
	<td align="right">0.05</td>
	<td align="right">49</td>
	<td align="right">0.58</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">18</th>
	<td>
	80.216.13.150		
	</td>
	<td align="right">337</td>
	<td align="right">0.27</td>
	<td align="right">113,344,979</td>
	<td align="right">0.35</td>
	<td align="right">11</td>
	<td align="right">0.13</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">19</th>
	<td>
	207.46.13.150		
	</td>
	<td align="right">319</td>
	<td align="right">0.26</td>
	<td align="right">7,516,751</td>
	<td align="right">0.02</td>
	<td align="right">36</td>
	<td align="right">0.43</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">20</th>
	<td>
	134.25.0.133		
	</td>
	<td align="right">304</td>
	<td align="right">0.24</td>
	<td align="right">56,464,467</td>
	<td align="right">0.18</td>
	<td align="right">5</td>
	<td align="right">0.07</td>
</tr>

</tbody>
</table>

