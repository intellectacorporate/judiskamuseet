


<h2>Statistics for http://www.u5645470.fsdata.se, Month of 2018-8-1 to 2018-8-31</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 10 of 362 Browsers (User Agents),
Sorted by Access Count

</h3>
<p></p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Product</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th class="hogerstalld">1</th>
	<td>
	Safari/537.36		
	</td>
	<td align="right">31,224</td>
	<td align="right">25.22</td>
	<td align="right">9,568,732,327</td>
	<td align="right">30.48</td>
	<td align="right">1,358</td>
	<td align="right">16.11</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	Mozilla/5.0 		
	</td>
	<td align="right">26,981</td>
	<td align="right">21.80</td>
	<td align="right">4,598,408,757</td>
	<td align="right">14.65</td>
	<td align="right">3,313</td>
	<td align="right">39.30</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	Safari/604.1		
	</td>
	<td align="right">18,496</td>
	<td align="right">14.94</td>
	<td align="right">6,251,145,154</td>
	<td align="right">19.91</td>
	<td align="right">848</td>
	<td align="right">10.06</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	UptimeRobot/2.0		
	</td>
	<td align="right">8,119</td>
	<td align="right">6.56</td>
	<td align="right">60,638</td>
	<td align="right">0.00</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	bingbot/2.0		
	</td>
	<td align="right">6,092</td>
	<td align="right">4.92</td>
	<td align="right">348,578,926</td>
	<td align="right">1.11</td>
	<td align="right">640</td>
	<td align="right">7.60</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	WordPress/4.9.8		
	</td>
	<td align="right">5,236</td>
	<td align="right">4.23</td>
	<td align="right">131,830</td>
	<td align="right">0.00</td>
	<td align="right">56</td>
	<td align="right">0.67</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	facebookexternalhit/1.1 		
	</td>
	<td align="right">3,305</td>
	<td align="right">2.67</td>
	<td align="right">5,345,878,791</td>
	<td align="right">17.03</td>
	<td align="right">144</td>
	<td align="right">1.71</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	Safari/605.1.15		
	</td>
	<td align="right">2,829</td>
	<td align="right">2.29</td>
	<td align="right">902,589,364</td>
	<td align="right">2.87</td>
	<td align="right">119</td>
	<td align="right">1.42</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	Safari/537.36 Edge/17.17134		
	</td>
	<td align="right">2,658</td>
	<td align="right">2.15</td>
	<td align="right">733,662,603</td>
	<td align="right">2.34</td>
	<td align="right">145</td>
	<td align="right">1.73</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">10</th>
	<td>
	Googlebot/2.1		
	</td>
	<td align="right">1,955</td>
	<td align="right">1.58</td>
	<td align="right">14,729,346</td>
	<td align="right">0.05</td>
	<td align="right">387</td>
	<td align="right">4.59</td>
</tr>

</tbody>
</table>

