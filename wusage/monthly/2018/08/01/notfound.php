


<h2>Statistics for http://www.u5645470.fsdata.se, Month of 2018-8-1 to 2018-8-31</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 10 of 364 Documents Not Found,
Sorted by Access Count

</h3>
<p></p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Document</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th class="hogerstalld">1</th>
	<td>
	/autodiscover/autodiscover.xml
	</td>
	<td align="right">401</td>
	<td align="right">24.50</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	/
	</td>
	<td align="right">329</td>
	<td align="right">20.10</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	/apple-touch-icon.png
	</td>
	<td align="right">47</td>
	<td align="right">2.87</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	/apple-touch-icon-precomposed.png
	</td>
	<td align="right">43</td>
	<td align="right">2.63</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	/wp-content/themes/Divi/js/html5.js
	</td>
	<td align="right">41</td>
	<td align="right">2.50</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	/tracesofexistence
	</td>
	<td align="right">39</td>
	<td align="right">2.38</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	/wp-content/plugins/the-events-calendar/vendor/bootstrap-datepicker/css/datepicker.css
	</td>
	<td align="right">22</td>
	<td align="right">1.34</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	/SOS-Admin-Help
	</td>
	<td align="right">16</td>
	<td align="right">0.98</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	/.well-known/assetlinks.json
	</td>
	<td align="right">16</td>
	<td align="right">0.98</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">10</th>
	<td>
	/wp-content/uploads/2017/09/synagogan-på-själagårdsgatan-19.jpg
	</td>
	<td align="right">14</td>
	<td align="right">0.86</td>
</tr>

</tbody>
</table>

