


<h2>Statistics for http://www.u5645470.fsdata.se, Month of 2018-9-1 to 2018-9-30</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 20 of 454 Referring Sites,
Sorted by Access Count

</h3>
<p></p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Referring Site</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th class="hogerstalld">1</th>
	<td>
	<a href="http://judiskamuseet.se">
	http://judiskamuseet.se</a>
	</td>
	<td align="right">84,436</td>
	<td align="right">75.64</td>
	<td align="right">15,319,335,886</td>
	<td align="right">79.28</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	<a href="https://judiskamuseet.se">
	https://judiskamuseet.se</a>
	</td>
	<td align="right">21,817</td>
	<td align="right">19.55</td>
	<td align="right">3,611,768,239</td>
	<td align="right">18.69</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	<a href="https://www.google.se">
	https://www.google.se</a>
	</td>
	<td align="right">1,934</td>
	<td align="right">1.73</td>
	<td align="right">240,553,250</td>
	<td align="right">1.24</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	<a href="https://www.google.com">
	https://www.google.com</a>
	</td>
	<td align="right">626</td>
	<td align="right">0.56</td>
	<td align="right">85,423,002</td>
	<td align="right">0.44</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	<a href="http://m.facebook.com">
	http://m.facebook.com</a>
	</td>
	<td align="right">483</td>
	<td align="right">0.43</td>
	<td align="right">6,144,374</td>
	<td align="right">0.03</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	<a href="https://www.google.de">
	https://www.google.de</a>
	</td>
	<td align="right">178</td>
	<td align="right">0.16</td>
	<td align="right">2,801,569</td>
	<td align="right">0.01</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	<a href="http://site.ru">
	http://site.ru</a>
	</td>
	<td align="right">161</td>
	<td align="right">0.14</td>
	<td align="right">6,325,710</td>
	<td align="right">0.03</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	<a href="http://www.google.com">
	http://www.google.com</a>
	</td>
	<td align="right">148</td>
	<td align="right">0.13</td>
	<td align="right">2,515,323</td>
	<td align="right">0.01</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	<a href="http://www.judiska-museet.se">
	http://www.judiska-museet.se</a>
	</td>
	<td align="right">108</td>
	<td align="right">0.10</td>
	<td align="right">1,530,171</td>
	<td align="right">0.01</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">10</th>
	<td>
	<a href="android-app://com.google.android.googlequicksearchbox">
	android-app://com.google.android.googlequicksearchbox</a>
	</td>
	<td align="right">46</td>
	<td align="right">0.04</td>
	<td align="right">587,534</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">11</th>
	<td>
	<a href="https://www.facebook.com">
	https://www.facebook.com</a>
	</td>
	<td align="right">37</td>
	<td align="right">0.03</td>
	<td align="right">523,085</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">12</th>
	<td>
	<a href="https://89.221.250.27:443">
	https://89.221.250.27:443</a>
	</td>
	<td align="right">36</td>
	<td align="right">0.03</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">13</th>
	<td>
	<a href="https://www.google.fi">
	https://www.google.fi</a>
	</td>
	<td align="right">35</td>
	<td align="right">0.03</td>
	<td align="right">1,266,883</td>
	<td align="right">0.01</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">14</th>
	<td>
	<a href="https://www.bing.com">
	https://www.bing.com</a>
	</td>
	<td align="right">34</td>
	<td align="right">0.03</td>
	<td align="right">28,752,542</td>
	<td align="right">0.15</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">15</th>
	<td>
	<a href="http://www.judiskamuseet.se">
	http://www.judiskamuseet.se</a>
	</td>
	<td align="right">25</td>
	<td align="right">0.02</td>
	<td align="right">315,097</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">16</th>
	<td>
	<a href="http://www.bing.com">
	http://www.bing.com</a>
	</td>
	<td align="right">25</td>
	<td align="right">0.02</td>
	<td align="right">327,381</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">17</th>
	<td>
	<a href="http://www.google.se">
	http://www.google.se</a>
	</td>
	<td align="right">25</td>
	<td align="right">0.02</td>
	<td align="right">362,479</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">18</th>
	<td>
	<a href="android-app://com.google.android.gm">
	android-app://com.google.android.gm</a>
	</td>
	<td align="right">18</td>
	<td align="right">0.02</td>
	<td align="right">130,682</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">19</th>
	<td>
	<a href="https://serdechnic.com">
	https://serdechnic.com</a>
	</td>
	<td align="right">15</td>
	<td align="right">0.01</td>
	<td align="right">513,401</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">20</th>
	<td>
	<a href="https://applications.itslearning.com">
	https://applications.itslearning.com</a>
	</td>
	<td align="right">14</td>
	<td align="right">0.01</td>
	<td align="right">191,206</td>
	<td align="right">0.00</td>
</tr>

</tbody>
</table>

