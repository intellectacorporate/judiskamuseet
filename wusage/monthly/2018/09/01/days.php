


<h2>Statistics for http://www.u5645470.fsdata.se, Month of 2018-9-1 to 2018-9-30</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Traffic by Day</h3>
<p>The continuous line represents a 7-day moving average.</p>
<img src="d.png" width="468" height="351">
<br /><br />


<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Date</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	<th scope='col'>Bits per Sec</th>
	<th scope='col'>Bytes per Sec</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th align="right">2018-9-1</th>
	<td align="right">2,814	</td>
	<td align="right">2.23	</td>
	<td align="right">769,239,378	</td>
	<td align="right">2.74	</td>
	<td align="right">208	</td>
	<td align="right">3.00	</td>
	<td align="right">71,225.87	</td>
	<td align="right">8,903.23	</td>
</tr>
<tr class="jamn">
	<th align="right">2018-9-2</th>
	<td align="right">3,175	</td>
	<td align="right">2.52	</td>
	<td align="right">751,043,583	</td>
	<td align="right">2.68	</td>
	<td align="right">226	</td>
	<td align="right">3.26	</td>
	<td align="right">69,541.07	</td>
	<td align="right">8,692.63	</td>
</tr>
<tr class="udda">
	<th align="right">2018-9-3</th>
	<td align="right">4,005	</td>
	<td align="right">3.18	</td>
	<td align="right">1,182,457,832	</td>
	<td align="right">4.21	</td>
	<td align="right">198	</td>
	<td align="right">2.86	</td>
	<td align="right">109,486.84	</td>
	<td align="right">13,685.85	</td>
</tr>
<tr class="jamn">
	<th align="right">2018-9-4</th>
	<td align="right">4,908	</td>
	<td align="right">3.89	</td>
	<td align="right">848,614,785	</td>
	<td align="right">3.02	</td>
	<td align="right">287	</td>
	<td align="right">4.14	</td>
	<td align="right">78,575.44	</td>
	<td align="right">9,821.93	</td>
</tr>
<tr class="udda">
	<th align="right">2018-9-5</th>
	<td align="right">4,863	</td>
	<td align="right">3.86	</td>
	<td align="right">1,070,152,381	</td>
	<td align="right">3.81	</td>
	<td align="right">243	</td>
	<td align="right">3.51	</td>
	<td align="right">99,088.18	</td>
	<td align="right">12,386.02	</td>
</tr>
<tr class="jamn">
	<th align="right">2018-9-6</th>
	<td align="right">3,601	</td>
	<td align="right">2.86	</td>
	<td align="right">902,577,956	</td>
	<td align="right">3.22	</td>
	<td align="right">200	</td>
	<td align="right">2.89	</td>
	<td align="right">83,572.03	</td>
	<td align="right">10,446.50	</td>
</tr>
<tr class="udda">
	<th align="right">2018-9-7</th>
	<td align="right">3,737	</td>
	<td align="right">2.96	</td>
	<td align="right">953,442,983	</td>
	<td align="right">3.40	</td>
	<td align="right">213	</td>
	<td align="right">3.07	</td>
	<td align="right">88,281.76	</td>
	<td align="right">11,035.22	</td>
</tr>
<tr class="jamn">
	<th align="right">2018-9-8</th>
	<td align="right">3,174	</td>
	<td align="right">2.52	</td>
	<td align="right">708,822,706	</td>
	<td align="right">2.53	</td>
	<td align="right">241	</td>
	<td align="right">3.48	</td>
	<td align="right">65,631.73	</td>
	<td align="right">8,203.97	</td>
</tr>
<tr class="udda">
	<th align="right">2018-9-9</th>
	<td align="right">3,064	</td>
	<td align="right">2.43	</td>
	<td align="right">535,716,982	</td>
	<td align="right">1.91	</td>
	<td align="right">285	</td>
	<td align="right">4.11	</td>
	<td align="right">49,603.42	</td>
	<td align="right">6,200.43	</td>
</tr>
<tr class="jamn">
	<th align="right">2018-9-10</th>
	<td align="right">7,587	</td>
	<td align="right">6.02	</td>
	<td align="right">2,436,936,375	</td>
	<td align="right">8.69	</td>
	<td align="right">335	</td>
	<td align="right">4.83	</td>
	<td align="right">225,642.26	</td>
	<td align="right">28,205.28	</td>
</tr>
<tr class="udda">
	<th align="right">2018-9-11</th>
	<td align="right">4,297	</td>
	<td align="right">3.41	</td>
	<td align="right">827,262,841	</td>
	<td align="right">2.95	</td>
	<td align="right">280	</td>
	<td align="right">4.04	</td>
	<td align="right">76,598.41	</td>
	<td align="right">9,574.80	</td>
</tr>
<tr class="jamn">
	<th align="right">2018-9-12</th>
	<td align="right">4,454	</td>
	<td align="right">3.53	</td>
	<td align="right">1,071,549,233	</td>
	<td align="right">3.82	</td>
	<td align="right">275	</td>
	<td align="right">3.97	</td>
	<td align="right">99,217.52	</td>
	<td align="right">12,402.19	</td>
</tr>
<tr class="udda">
	<th align="right">2018-9-13</th>
	<td align="right">3,932	</td>
	<td align="right">3.12	</td>
	<td align="right">1,001,309,867	</td>
	<td align="right">3.57	</td>
	<td align="right">226	</td>
	<td align="right">3.26	</td>
	<td align="right">92,713.88	</td>
	<td align="right">11,589.23	</td>
</tr>
<tr class="jamn">
	<th align="right">2018-9-14</th>
	<td align="right">3,801	</td>
	<td align="right">3.01	</td>
	<td align="right">802,472,466	</td>
	<td align="right">2.86	</td>
	<td align="right">222	</td>
	<td align="right">3.20	</td>
	<td align="right">74,303.01	</td>
	<td align="right">9,287.88	</td>
</tr>
<tr class="udda">
	<th align="right">2018-9-15</th>
	<td align="right">2,661	</td>
	<td align="right">2.11	</td>
	<td align="right">649,883,146	</td>
	<td align="right">2.32	</td>
	<td align="right">151	</td>
	<td align="right">2.18	</td>
	<td align="right">60,174.37	</td>
	<td align="right">7,521.80	</td>
</tr>
<tr class="jamn">
	<th align="right">2018-9-16</th>
	<td align="right">3,553	</td>
	<td align="right">2.82	</td>
	<td align="right">813,731,356	</td>
	<td align="right">2.90	</td>
	<td align="right">171	</td>
	<td align="right">2.47	</td>
	<td align="right">75,345.50	</td>
	<td align="right">9,418.19	</td>
</tr>
<tr class="udda">
	<th align="right">2018-9-17</th>
	<td align="right">5,457	</td>
	<td align="right">4.33	</td>
	<td align="right">1,042,548,095	</td>
	<td align="right">3.72	</td>
	<td align="right">194	</td>
	<td align="right">2.80	</td>
	<td align="right">96,532.23	</td>
	<td align="right">12,066.53	</td>
</tr>
<tr class="jamn">
	<th align="right">2018-9-18</th>
	<td align="right">6,739	</td>
	<td align="right">5.34	</td>
	<td align="right">832,364,498	</td>
	<td align="right">2.97	</td>
	<td align="right">219	</td>
	<td align="right">3.16	</td>
	<td align="right">77,070.79	</td>
	<td align="right">9,633.85	</td>
</tr>
<tr class="udda">
	<th align="right">2018-9-19</th>
	<td align="right">4,028	</td>
	<td align="right">3.19	</td>
	<td align="right">935,922,168	</td>
	<td align="right">3.34	</td>
	<td align="right">202	</td>
	<td align="right">2.91	</td>
	<td align="right">86,659.46	</td>
	<td align="right">10,832.43	</td>
</tr>
<tr class="jamn">
	<th align="right">2018-9-20</th>
	<td align="right">4,495	</td>
	<td align="right">3.57	</td>
	<td align="right">979,066,102	</td>
	<td align="right">3.49	</td>
	<td align="right">229	</td>
	<td align="right">3.30	</td>
	<td align="right">90,654.27	</td>
	<td align="right">11,331.78	</td>
</tr>
<tr class="udda">
	<th align="right">2018-9-21</th>
	<td align="right">4,318	</td>
	<td align="right">3.42	</td>
	<td align="right">1,042,228,244	</td>
	<td align="right">3.71	</td>
	<td align="right">240	</td>
	<td align="right">3.46	</td>
	<td align="right">96,502.62	</td>
	<td align="right">12,062.83	</td>
</tr>
<tr class="jamn">
	<th align="right">2018-9-22</th>
	<td align="right">3,586	</td>
	<td align="right">2.84	</td>
	<td align="right">583,798,324	</td>
	<td align="right">2.08	</td>
	<td align="right">230	</td>
	<td align="right">3.32	</td>
	<td align="right">54,055.40	</td>
	<td align="right">6,756.93	</td>
</tr>
<tr class="udda">
	<th align="right">2018-9-23</th>
	<td align="right">3,515	</td>
	<td align="right">2.79	</td>
	<td align="right">821,677,159	</td>
	<td align="right">2.93	</td>
	<td align="right">201	</td>
	<td align="right">2.90	</td>
	<td align="right">76,081.22	</td>
	<td align="right">9,510.15	</td>
</tr>
<tr class="jamn">
	<th align="right">2018-9-24</th>
	<td align="right">5,064	</td>
	<td align="right">4.02	</td>
	<td align="right">1,127,596,523	</td>
	<td align="right">4.02	</td>
	<td align="right">247	</td>
	<td align="right">3.56	</td>
	<td align="right">104,407.09	</td>
	<td align="right">13,050.89	</td>
</tr>
<tr class="udda">
	<th align="right">2018-9-25</th>
	<td align="right">5,099	</td>
	<td align="right">4.04	</td>
	<td align="right">1,043,422,979	</td>
	<td align="right">3.72	</td>
	<td align="right">222	</td>
	<td align="right">3.20	</td>
	<td align="right">96,613.24	</td>
	<td align="right">12,076.65	</td>
</tr>
<tr class="jamn">
	<th align="right">2018-9-26</th>
	<td align="right">4,080	</td>
	<td align="right">3.24	</td>
	<td align="right">1,065,060,235	</td>
	<td align="right">3.80	</td>
	<td align="right">197	</td>
	<td align="right">2.84	</td>
	<td align="right">98,616.69	</td>
	<td align="right">12,327.09	</td>
</tr>
<tr class="udda">
	<th align="right">2018-9-27</th>
	<td align="right">4,788	</td>
	<td align="right">3.80	</td>
	<td align="right">986,559,342	</td>
	<td align="right">3.52	</td>
	<td align="right">270	</td>
	<td align="right">3.90	</td>
	<td align="right">91,348.09	</td>
	<td align="right">11,418.51	</td>
</tr>
<tr class="jamn">
	<th align="right">2018-9-28</th>
	<td align="right">4,948	</td>
	<td align="right">3.92	</td>
	<td align="right">1,104,175,466	</td>
	<td align="right">3.94	</td>
	<td align="right">262	</td>
	<td align="right">3.78	</td>
	<td align="right">102,238.47	</td>
	<td align="right">12,779.81	</td>
</tr>
<tr class="udda">
	<th align="right">2018-9-29</th>
	<td align="right">2,697	</td>
	<td align="right">2.14	</td>
	<td align="right">443,580,615	</td>
	<td align="right">1.58	</td>
	<td align="right">196	</td>
	<td align="right">2.83	</td>
	<td align="right">41,072.28	</td>
	<td align="right">5,134.03	</td>
</tr>
<tr class="jamn">
	<th align="right">2018-9-30</th>
	<td align="right">3,642	</td>
	<td align="right">2.89	</td>
	<td align="right">721,502,602	</td>
	<td align="right">2.57	</td>
	<td align="right">261	</td>
	<td align="right">3.77	</td>
	<td align="right">66,805.80	</td>
	<td align="right">8,350.72	</td>
</tr>

</tbody>
</table>

