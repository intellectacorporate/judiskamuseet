


<h2>Statistics for http://www.u5645470.fsdata.se, Month of 2018-9-1 to 2018-9-30</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Traffic by Hour</h3>
<p>Figures are averages for that hour
	of each day in the time period analyzed.</p>
<img src="h.png" width="468" height="351">
<br /><br />
<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Hour</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	<th scope='col'>Bits per Sec</th>
	<th scope='col'>Bytes per Sec</th>
	
	</tr>
</thead>
<tbody>
<tr class="jamnudda">
	<th align="right">00:00</th>
	<td align="right">100.63</td>
	<td align="right">2.39</td>
	<td align="right">22,607,425.63</td>
	<td align="right">2.42</td>
	<td align="right">3.13</td>
	<td align="right">1.35</td>
	<td align="right">50,238.72</td>
	<td align="right">6,279.84</td>
	
</tr>
<tr class="jamn">
	<th align="right">01:00</th>
	<td align="right">78.37</td>
	<td align="right">1.86</td>
	<td align="right">10,811,482.33</td>
	<td align="right">1.16</td>
	<td align="right">5.63</td>
	<td align="right">2.44</td>
	<td align="right">24,025.52</td>
	<td align="right">3,003.19</td>
	
</tr>
<tr class="udda">
	<th align="right">02:00</th>
	<td align="right">68.27</td>
	<td align="right">1.62</td>
	<td align="right">11,078,370.37</td>
	<td align="right">1.18</td>
	<td align="right">4.67</td>
	<td align="right">2.02</td>
	<td align="right">24,618.60</td>
	<td align="right">3,077.33</td>
	
</tr>
<tr class="jamn">
	<th align="right">03:00</th>
	<td align="right">73.30</td>
	<td align="right">1.74</td>
	<td align="right">7,216,158.20</td>
	<td align="right">0.77</td>
	<td align="right">4.98</td>
	<td align="right">2.16</td>
	<td align="right">16,035.91</td>
	<td align="right">2,004.49</td>
	
</tr>
<tr class="udda">
	<th align="right">04:00</th>
	<td align="right">62.90</td>
	<td align="right">1.50</td>
	<td align="right">4,846,664.70</td>
	<td align="right">0.52</td>
	<td align="right">5.41</td>
	<td align="right">2.34</td>
	<td align="right">10,770.37</td>
	<td align="right">1,346.30</td>
	
</tr>
<tr class="jamn">
	<th align="right">05:00</th>
	<td align="right">61.87</td>
	<td align="right">1.47</td>
	<td align="right">3,573,841.43</td>
	<td align="right">0.38</td>
	<td align="right">4.55</td>
	<td align="right">1.97</td>
	<td align="right">7,941.87</td>
	<td align="right">992.73</td>
	
</tr>
<tr class="udda">
	<th align="right">06:00</th>
	<td align="right">73.43</td>
	<td align="right">1.75</td>
	<td align="right">7,364,647.47</td>
	<td align="right">0.79</td>
	<td align="right">5.89</td>
	<td align="right">2.55</td>
	<td align="right">16,365.88</td>
	<td align="right">2,045.74</td>
	
</tr>
<tr class="jamn">
	<th align="right">07:00</th>
	<td align="right">90.03</td>
	<td align="right">2.14</td>
	<td align="right">10,089,816.00</td>
	<td align="right">1.08</td>
	<td align="right">6.09</td>
	<td align="right">2.63</td>
	<td align="right">22,421.81</td>
	<td align="right">2,802.73</td>
	
</tr>
<tr class="udda">
	<th align="right">08:00</th>
	<td align="right">165.40</td>
	<td align="right">3.94</td>
	<td align="right">39,965,347.27</td>
	<td align="right">4.27</td>
	<td align="right">8.01</td>
	<td align="right">3.47</td>
	<td align="right">88,811.88</td>
	<td align="right">11,101.49</td>
	
</tr>
<tr class="jamn">
	<th align="right">09:00</th>
	<td align="right">269.37</td>
	<td align="right">6.41</td>
	<td align="right">68,185,996.53</td>
	<td align="right">7.29</td>
	<td align="right">11.84</td>
	<td align="right">5.12</td>
	<td align="right">151,524.44</td>
	<td align="right">18,940.55</td>
	
</tr>
<tr class="udda">
	<th align="right">10:00</th>
	<td align="right">251.17</td>
	<td align="right">5.98</td>
	<td align="right">63,970,214.40</td>
	<td align="right">6.84</td>
	<td align="right">13.72</td>
	<td align="right">5.94</td>
	<td align="right">142,156.03</td>
	<td align="right">17,769.50</td>
	
</tr>
<tr class="jamn">
	<th align="right">11:00</th>
	<td align="right">253.00</td>
	<td align="right">6.02</td>
	<td align="right">78,412,963.27</td>
	<td align="right">8.39</td>
	<td align="right">12.51</td>
	<td align="right">5.41</td>
	<td align="right">174,251.03</td>
	<td align="right">21,781.38</td>
	
</tr>
<tr class="udda">
	<th align="right">12:00</th>
	<td align="right">236.40</td>
	<td align="right">5.62</td>
	<td align="right">59,005,822.40</td>
	<td align="right">6.31</td>
	<td align="right">13.29</td>
	<td align="right">5.75</td>
	<td align="right">131,124.05</td>
	<td align="right">16,390.51</td>
	
</tr>
<tr class="jamn">
	<th align="right">13:00</th>
	<td align="right">319.17</td>
	<td align="right">7.59</td>
	<td align="right">77,249,139.20</td>
	<td align="right">8.26</td>
	<td align="right">14.11</td>
	<td align="right">6.11</td>
	<td align="right">171,664.75</td>
	<td align="right">21,458.09</td>
	
</tr>
<tr class="udda">
	<th align="right">14:00</th>
	<td align="right">249.57</td>
	<td align="right">5.94</td>
	<td align="right">66,595,353.33</td>
	<td align="right">7.12</td>
	<td align="right">14.36</td>
	<td align="right">6.21</td>
	<td align="right">147,989.67</td>
	<td align="right">18,498.71</td>
	
</tr>
<tr class="jamn">
	<th align="right">15:00</th>
	<td align="right">259.93</td>
	<td align="right">6.18</td>
	<td align="right">57,656,507.20</td>
	<td align="right">6.17</td>
	<td align="right">12.63</td>
	<td align="right">5.47</td>
	<td align="right">128,125.57</td>
	<td align="right">16,015.70</td>
	
</tr>
<tr class="udda">
	<th align="right">16:00</th>
	<td align="right">241.00</td>
	<td align="right">5.73</td>
	<td align="right">52,847,137.47</td>
	<td align="right">5.65</td>
	<td align="right">14.45</td>
	<td align="right">6.25</td>
	<td align="right">117,438.08</td>
	<td align="right">14,679.76</td>
	
</tr>
<tr class="jamn">
	<th align="right">17:00</th>
	<td align="right">182.13</td>
	<td align="right">4.33</td>
	<td align="right">35,974,684.33</td>
	<td align="right">3.85</td>
	<td align="right">11.91</td>
	<td align="right">5.16</td>
	<td align="right">79,943.74</td>
	<td align="right">9,992.97</td>
	
</tr>
<tr class="udda">
	<th align="right">18:00</th>
	<td align="right">176.60</td>
	<td align="right">4.20</td>
	<td align="right">36,070,952.10</td>
	<td align="right">3.86</td>
	<td align="right">9.87</td>
	<td align="right">4.27</td>
	<td align="right">80,157.67</td>
	<td align="right">10,019.71</td>
	
</tr>
<tr class="jamn">
	<th align="right">19:00</th>
	<td align="right">173.00</td>
	<td align="right">4.12</td>
	<td align="right">36,195,372.43</td>
	<td align="right">3.87</td>
	<td align="right">10.24</td>
	<td align="right">4.43</td>
	<td align="right">80,434.16</td>
	<td align="right">10,054.27</td>
	
</tr>
<tr class="udda">
	<th align="right">20:00</th>
	<td align="right">196.17</td>
	<td align="right">4.67</td>
	<td align="right">36,274,119.27</td>
	<td align="right">3.88</td>
	<td align="right">10.29</td>
	<td align="right">4.45</td>
	<td align="right">80,609.15</td>
	<td align="right">10,076.14</td>
	
</tr>
<tr class="jamn">
	<th align="right">21:00</th>
	<td align="right">295.00</td>
	<td align="right">7.02</td>
	<td align="right">82,744,168.00</td>
	<td align="right">8.85</td>
	<td align="right">13.15</td>
	<td align="right">5.69</td>
	<td align="right">183,875.93</td>
	<td align="right">22,984.49</td>
	
</tr>
<tr class="udda">
	<th align="right">22:00</th>
	<td align="right">198.00</td>
	<td align="right">4.71</td>
	<td align="right">43,327,351.67</td>
	<td align="right">4.63</td>
	<td align="right">9.31</td>
	<td align="right">4.03</td>
	<td align="right">96,283.00</td>
	<td align="right">12,035.38</td>
	
</tr>
<tr class="jamn">
	<th align="right">23:00</th>
	<td align="right">128.03</td>
	<td align="right">3.05</td>
	<td align="right">23,093,672.40</td>
	<td align="right">2.47</td>
	<td align="right">11.01</td>
	<td align="right">4.76</td>
	<td align="right">51,319.27</td>
	<td align="right">6,414.91</td>
	
</tr>

</tbody>
</table>

