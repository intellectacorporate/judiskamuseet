


<h2>Statistics for http://www.u5645470.fsdata.se, Month of 2018-9-1 to 2018-9-30</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 10 of 13 Operating Systems,
Sorted by Access Count

</h3>
<p></p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Product</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th class="hogerstalld">1</th>
	<td>
	Mac OS X		
	</td>
	<td align="right">82,253</td>
	<td align="right">65.41</td>
	<td align="right">21,777,692,426</td>
	<td align="right">77.77</td>
	<td align="right">3,804</td>
	<td align="right">54.89</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	Other		
	</td>
	<td align="right">25,541</td>
	<td align="right">20.31</td>
	<td align="right">2,804,806,429</td>
	<td align="right">10.02</td>
	<td align="right">1,763</td>
	<td align="right">25.44</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	Windows NT		
	</td>
	<td align="right">15,871</td>
	<td align="right">12.62</td>
	<td align="right">3,210,850,733</td>
	<td align="right">11.47</td>
	<td align="right">1,241</td>
	<td align="right">17.91</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	Windows Codename Longhorn		
	</td>
	<td align="right">800</td>
	<td align="right">0.64</td>
	<td align="right">85,469,242</td>
	<td align="right">0.31</td>
	<td align="right">90</td>
	<td align="right">1.31</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	Windows XP		
	</td>
	<td align="right">660</td>
	<td align="right">0.52</td>
	<td align="right">12,081,211</td>
	<td align="right">0.04</td>
	<td align="right">12</td>
	<td align="right">0.18</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	Linux		
	</td>
	<td align="right">381</td>
	<td align="right">0.30</td>
	<td align="right">104,363,802</td>
	<td align="right">0.37</td>
	<td align="right">12</td>
	<td align="right">0.18</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	Windows 2000		
	</td>
	<td align="right">115</td>
	<td align="right">0.09</td>
	<td align="right">4,665,411</td>
	<td align="right">0.02</td>
	<td align="right">6</td>
	<td align="right">0.10</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	Windows 2003		
	</td>
	<td align="right">106</td>
	<td align="right">0.08</td>
	<td align="right">271,219</td>
	<td align="right">0.00</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	Windows 95		
	</td>
	<td align="right">6</td>
	<td align="right">0.00</td>
	<td align="right">407,911</td>
	<td align="right">0.00</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">10</th>
	<td>
	Windows Me		
	</td>
	<td align="right">4</td>
	<td align="right">0.00</td>
	<td align="right">68,386</td>
	<td align="right">0.00</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>

</tbody>
</table>

