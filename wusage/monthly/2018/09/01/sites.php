


<h2>Statistics for http://www.u5645470.fsdata.se, Month of 2018-9-1 to 2018-9-30</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 20 of 6,161 Visitor Sites,
Sorted by Access Count

</h3>
<p></p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Site</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th class="hogerstalld">1</th>
	<td>
	63.143.42.244		
	</td>
	<td align="right">7,874</td>
	<td align="right">6.25</td>
	<td align="right">91,019</td>
	<td align="right">0.00</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	89.221.250.27		
	</td>
	<td align="right">4,646</td>
	<td align="right">3.68</td>
	<td align="right">91,828</td>
	<td align="right">0.00</td>
	<td align="right">63</td>
	<td align="right">0.91</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	90.229.237.5		
	</td>
	<td align="right">3,447</td>
	<td align="right">2.73</td>
	<td align="right">6,862,347</td>
	<td align="right">0.02</td>
	<td align="right">7</td>
	<td align="right">0.11</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	46.59.25.88		
	</td>
	<td align="right">3,346</td>
	<td align="right">2.65</td>
	<td align="right">481,343,239</td>
	<td align="right">1.72</td>
	<td align="right">48</td>
	<td align="right">0.70</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	80.216.13.150		
	</td>
	<td align="right">1,072</td>
	<td align="right">0.85</td>
	<td align="right">112,275,063</td>
	<td align="right">0.40</td>
	<td align="right">14</td>
	<td align="right">0.21</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	66.249.64.86		
	</td>
	<td align="right">801</td>
	<td align="right">0.64</td>
	<td align="right">35,321,761</td>
	<td align="right">0.13</td>
	<td align="right">156</td>
	<td align="right">2.26</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	46.182.207.79		
	</td>
	<td align="right">694</td>
	<td align="right">0.55</td>
	<td align="right">90,958,417</td>
	<td align="right">0.32</td>
	<td align="right">18</td>
	<td align="right">0.26</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	66.249.64.88		
	</td>
	<td align="right">617</td>
	<td align="right">0.49</td>
	<td align="right">14,858,133</td>
	<td align="right">0.05</td>
	<td align="right">112</td>
	<td align="right">1.63</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	23.101.169.3		
	</td>
	<td align="right">614</td>
	<td align="right">0.49</td>
	<td align="right">9,465,406</td>
	<td align="right">0.03</td>
	<td align="right">47</td>
	<td align="right">0.69</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">10</th>
	<td>
	195.22.74.51		
	</td>
	<td align="right">566</td>
	<td align="right">0.45</td>
	<td align="right">111,510,261</td>
	<td align="right">0.40</td>
	<td align="right">13</td>
	<td align="right">0.19</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">11</th>
	<td>
	2.249.106.47		
	</td>
	<td align="right">528</td>
	<td align="right">0.42</td>
	<td align="right">154,635,378</td>
	<td align="right">0.55</td>
	<td align="right">13</td>
	<td align="right">0.19</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">12</th>
	<td>
	81.170.150.42		
	</td>
	<td align="right">503</td>
	<td align="right">0.40</td>
	<td align="right">92,661,665</td>
	<td align="right">0.33</td>
	<td align="right">5</td>
	<td align="right">0.08</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">13</th>
	<td>
	66.249.64.90		
	</td>
	<td align="right">469</td>
	<td align="right">0.37</td>
	<td align="right">6,465,230</td>
	<td align="right">0.02</td>
	<td align="right">92</td>
	<td align="right">1.34</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">14</th>
	<td>
	62.209.162.120		
	</td>
	<td align="right">433</td>
	<td align="right">0.34</td>
	<td align="right">52,344,204</td>
	<td align="right">0.19</td>
	<td align="right">14</td>
	<td align="right">0.21</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">15</th>
	<td>
	34.228.147.206		
	</td>
	<td align="right">409</td>
	<td align="right">0.32</td>
	<td align="right">87,832,034</td>
	<td align="right">0.31</td>
	<td align="right">1</td>
	<td align="right">0.02</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">16</th>
	<td>
	66.102.8.214		
	</td>
	<td align="right">377</td>
	<td align="right">0.30</td>
	<td align="right">177,957,733</td>
	<td align="right">0.63</td>
	<td align="right">2</td>
	<td align="right">0.03</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">17</th>
	<td>
	64.233.172.182		
	</td>
	<td align="right">363</td>
	<td align="right">0.29</td>
	<td align="right">139,075,323</td>
	<td align="right">0.50</td>
	<td align="right">2</td>
	<td align="right">0.03</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">18</th>
	<td>
	66.249.83.82		
	</td>
	<td align="right">338</td>
	<td align="right">0.27</td>
	<td align="right">175,980,001</td>
	<td align="right">0.63</td>
	<td align="right">2</td>
	<td align="right">0.03</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">19</th>
	<td>
	66.102.8.216		
	</td>
	<td align="right">338</td>
	<td align="right">0.27</td>
	<td align="right">174,792,792</td>
	<td align="right">0.62</td>
	<td align="right">3</td>
	<td align="right">0.05</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">20</th>
	<td>
	194.71.153.205		
	</td>
	<td align="right">334</td>
	<td align="right">0.26</td>
	<td align="right">173,461,739</td>
	<td align="right">0.62</td>
	<td align="right">4</td>
	<td align="right">0.06</td>
</tr>

</tbody>
</table>

