


<h2>Statistics for http://www.u5645470.fsdata.se, Month of 2018-9-1 to 2018-9-30</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 10 of 17 File Types (Extensions), 
Sorted by Access Count

</h3>
<p>Individual file types as determined by file extensions. All URLs that
do not contain an extension are counted as directories.</p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Type</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th class="hogerstalld">1</th>
	<td>
	Directory (folder)		
	</td>
	<td align="right">29,487</td>
	<td align="right">23.39</td>
	<td align="right">312,417,160</td>
	<td align="right">1.11</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	js		
	</td>
	<td align="right">28,195</td>
	<td align="right">22.36</td>
	<td align="right">368,496,835</td>
	<td align="right">1.31</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	jpg		
	</td>
	<td align="right">22,411</td>
	<td align="right">17.77</td>
	<td align="right">24,965,013,271</td>
	<td align="right">88.99</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	png		
	</td>
	<td align="right">16,525</td>
	<td align="right">13.11</td>
	<td align="right">1,427,652,212</td>
	<td align="right">5.09</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	css		
	</td>
	<td align="right">14,144</td>
	<td align="right">11.22</td>
	<td align="right">304,735,348</td>
	<td align="right">1.09</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	php		
	</td>
	<td align="right">9,695</td>
	<td align="right">7.69</td>
	<td align="right">38,021,180</td>
	<td align="right">0.14</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	ttf		
	</td>
	<td align="right">2,855</td>
	<td align="right">2.26</td>
	<td align="right">260,438,670</td>
	<td align="right">0.93</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	txt		
	</td>
	<td align="right">1,844</td>
	<td align="right">1.46</td>
	<td align="right">275,667</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	gif		
	</td>
	<td align="right">259</td>
	<td align="right">0.21</td>
	<td align="right">1,211,254</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">10</th>
	<td>
	eot		
	</td>
	<td align="right">177</td>
	<td align="right">0.14</td>
	<td align="right">13,232,800</td>
	<td align="right">0.05</td>
</tr>

</tbody>
</table>

