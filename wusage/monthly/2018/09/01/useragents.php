


<h2>Statistics for http://www.u5645470.fsdata.se, Month of 2018-9-1 to 2018-9-30</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 10 of 286 Browsers (User Agents),
Sorted by Access Count

</h3>
<p></p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Product</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th class="hogerstalld">1</th>
	<td>
	Safari/537.36		
	</td>
	<td align="right">40,762</td>
	<td align="right">32.42</td>
	<td align="right">11,776,153,705</td>
	<td align="right">42.06</td>
	<td align="right">1,717</td>
	<td align="right">24.87</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	Mozilla/5.0 		
	</td>
	<td align="right">19,936</td>
	<td align="right">15.85</td>
	<td align="right">3,879,361,566</td>
	<td align="right">13.85</td>
	<td align="right">1,415</td>
	<td align="right">20.49</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	Safari/604.1		
	</td>
	<td align="right">17,998</td>
	<td align="right">14.31</td>
	<td align="right">4,634,904,796</td>
	<td align="right">16.55</td>
	<td align="right">809</td>
	<td align="right">11.73</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	UptimeRobot/2.0		
	</td>
	<td align="right">7,864</td>
	<td align="right">6.25</td>
	<td align="right">204,120</td>
	<td align="right">0.00</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	WordPress/4.9.8		
	</td>
	<td align="right">4,644</td>
	<td align="right">3.69</td>
	<td align="right">107,861</td>
	<td align="right">0.00</td>
	<td align="right">64</td>
	<td align="right">0.93</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	Safari/605.1.15		
	</td>
	<td align="right">4,514</td>
	<td align="right">3.59</td>
	<td align="right">1,401,461,551</td>
	<td align="right">5.01</td>
	<td align="right">187</td>
	<td align="right">2.71</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	bingbot/2.0		
	</td>
	<td align="right">4,108</td>
	<td align="right">3.27</td>
	<td align="right">579,099,540</td>
	<td align="right">2.07</td>
	<td align="right">663</td>
	<td align="right">9.60</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	Safari/602.1		
	</td>
	<td align="right">2,533</td>
	<td align="right">2.01</td>
	<td align="right">696,699,093</td>
	<td align="right">2.49</td>
	<td align="right">104</td>
	<td align="right">1.52</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	Safari/537.36 Edge/17.17134		
	</td>
	<td align="right">2,406</td>
	<td align="right">1.91</td>
	<td align="right">559,688,662</td>
	<td align="right">2.00</td>
	<td align="right">84</td>
	<td align="right">1.22</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">10</th>
	<td>
	Safari/537.36 (compatible; Googlebot/2.1;  http://www.google.com/bot.html)		
	</td>
	<td align="right">2,397</td>
	<td align="right">1.91</td>
	<td align="right">29,914,949</td>
	<td align="right">0.11</td>
	<td align="right">422</td>
	<td align="right">6.12</td>
</tr>

</tbody>
</table>

