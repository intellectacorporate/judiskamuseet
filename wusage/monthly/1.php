


<h2>Statistics for http://www.u5645470.fsdata.se, Month of 2017-6-1 to 2020-1-31</h2>

	<h3>History from 2017-6-1 to 2020-1-31</h3>
	<p>Values for completed time periods are final. Values for the 
	reporting period currently in progress are projections, except
	for user-calculated functions, which are not scaled.</p>
	
	<p><img src="1t.png" width="468" height="351"></p>
	<table cellspacing='0' class='styladtabell'>
	<caption>Overall Accesses</caption>
	<thead>
	<tr>
	<th scope="col" class='centrerad'>Report Date</th>
	<th scope="col" class='centrerad'>Accesses</th>
	<th scope="col" class='centrerad'>Bytes</th>
	<th scope="col" class='centrerad'>Visits</th>
	
	</tr>
        </thead>
	<tbody>
	<tr class="jamn">
	<th align="left">Total</th>
	<td align="right">6,289,297</td>
	<td align="right">1,092,773,978,293</td>
	<td align="right">289,654</td>	
	
	</tr>
	<tr class="udda">
	<th align="left">Average</th>
	<td align="right">193,516</td>
	<td align="right">33,623,814,716</td>
	<td align="right">8,912</td>
	
	</tr>
	<tr class="jamn">
	<th align="left">Latest</th>
	<td align="right">290,326.11</td>
	<td align="right">16,512,027,456</td>
	<td align="right">12,350</td>
	
	</tr>
	<tr class="udda">
	<th align=left>
	17-06-01
	</th>
	<td align=right>1</td>
	<td align=right>668</td>
	<td align=right>0</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	17-07-01
	</th>
	<td align=right>134,159</td>
	<td align=right>13,632,615,944</td>
	<td align=right>4,539</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	17-08-01
	</th>
	<td align=right>118,186</td>
	<td align=right>16,608,347,924</td>
	<td align=right>6,285</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	17-09-01
	</th>
	<td align=right>244,836</td>
	<td align=right>42,491,800,845</td>
	<td align=right>10,248</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	17-10-01
	</th>
	<td align=right>204,275</td>
	<td align=right>40,610,436,593</td>
	<td align=right>8,584</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	17-11-01
	</th>
	<td align=right>146,980</td>
	<td align=right>30,461,823,359</td>
	<td align=right>6,392</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	17-12-01
	</th>
	<td align=right>127,804</td>
	<td align=right>22,268,191,498</td>
	<td align=right>6,267</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	18-01-01
	</th>
	<td align=right>137,524</td>
	<td align=right>26,924,822,069</td>
	<td align=right>5,905</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	18-02-01
	</th>
	<td align=right>138,324</td>
	<td align=right>22,545,860,544</td>
	<td align=right>6,999</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	18-03-01
	</th>
	<td align=right>195,247</td>
	<td align=right>34,385,508,490</td>
	<td align=right>8,659</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	18-04-01
	</th>
	<td align=right>158,734</td>
	<td align=right>30,301,435,718</td>
	<td align=right>7,368</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	18-05-01
	</th>
	<td align=right>155,003</td>
	<td align=right>30,483,971,926</td>
	<td align=right>7,473</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	18-06-01
	</th>
	<td align=right>162,940</td>
	<td align=right>29,597,193,843</td>
	<td align=right>10,122</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	18-07-01
	</th>
	<td align=right>100,950</td>
	<td align=right>22,041,300,001</td>
	<td align=right>6,250</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	<a target="_top" href="2018/08/01/index.php">
	18-08-01</a>
	
	</th>
	<td align=right>124,505</td>
	<td align=right>32,193,070,723</td>
	<td align=right>8,457</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	<a target="_top" href="2018/09/01/index.php">
	18-09-01</a>
	
	</th>
	<td align=right>126,082</td>
	<td align=right>28,054,716,222</td>
	<td align=right>6,931</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	<a target="_top" href="2018/10/01/index.php">
	18-10-01</a>
	
	</th>
	<td align=right>140,124</td>
	<td align=right>31,854,662,734</td>
	<td align=right>7,741</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	<a target="_top" href="2018/11/01/index.php">
	18-11-01</a>
	
	</th>
	<td align=right>145,765</td>
	<td align=right>34,375,562,451</td>
	<td align=right>7,294</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	<a target="_top" href="2018/12/01/index.php">
	18-12-01</a>
	
	</th>
	<td align=right>156,141</td>
	<td align=right>39,176,175,991</td>
	<td align=right>6,363</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	<a target="_top" href="2019/01/01/index.php">
	19-01-01</a>
	
	</th>
	<td align=right>157,766</td>
	<td align=right>55,128,454,592</td>
	<td align=right>7,070</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	<a target="_top" href="2019/02/01/index.php">
	19-02-01</a>
	
	</th>
	<td align=right>130,986</td>
	<td align=right>35,525,705,758</td>
	<td align=right>6,537</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	<a target="_top" href="2019/03/01/index.php">
	19-03-01</a>
	
	</th>
	<td align=right>155,478</td>
	<td align=right>45,644,923,100</td>
	<td align=right>8,961</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	<a target="_top" href="2019/04/01/index.php">
	19-04-01</a>
	
	</th>
	<td align=right>207,700</td>
	<td align=right>60,439,913,084</td>
	<td align=right>10,531</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	<a target="_top" href="2019/05/01/index.php">
	19-05-01</a>
	
	</th>
	<td align=right>280,424</td>
	<td align=right>71,822,929,995</td>
	<td align=right>11,658</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	<a target="_top" href="2019/06/01/index.php">
	19-06-01</a>
	
	</th>
	<td align=right>554,482</td>
	<td align=right>107,730,403,684</td>
	<td align=right>18,709</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	<a target="_top" href="2019/07/01/index.php">
	19-07-01</a>
	
	</th>
	<td align=right>264,536</td>
	<td align=right>43,960,590,859</td>
	<td align=right>12,748</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	<a target="_top" href="2019/08/01/index.php">
	19-08-01</a>
	
	</th>
	<td align=right>251,391</td>
	<td align=right>42,980,652,854</td>
	<td align=right>11,206</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	<a target="_top" href="2019/09/01/index.php">
	19-09-01</a>
	
	</th>
	<td align=right>320,394</td>
	<td align=right>22,237,278,399</td>
	<td align=right>13,000</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	<a target="_top" href="2019/10/01/index.php">
	19-10-01</a>
	
	</th>
	<td align=right>369,354</td>
	<td align=right>27,765,385,888</td>
	<td align=right>16,981</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	<a target="_top" href="2019/11/01/index.php">
	19-11-01</a>
	
	</th>
	<td align=right>348,555</td>
	<td align=right>23,488,632,878</td>
	<td align=right>16,838</td>
	
	</tr>
<tr class="udda">
	<th align=left>
	<a target="_top" href="2019/12/01/index.php">
	19-12-01</a>
	
	</th>
	<td align=right>333,644</td>
	<td align=right>16,837,019,599</td>
	<td align=right>15,157</td>
	
	</tr>
<tr class="jamn">
	<th align=left>
	<a target="_top" href="2020/01/01/index.php">
	20-01-01</a>
	
	</th>
	<td align=right>290,326.11</td>
	<td align=right>16,512,027,456.84</td>
	<td align=right>12,350.95</td>
	
	</tr>

	</tbody>

	</table>
	