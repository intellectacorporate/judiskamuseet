


<h2>Statistics for http://www.u5645470.fsdata.se, Month of 2020-1-1 to 2020-1-31</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 20 of 7,735 Visitor Sites,
Sorted by Access Count

</h3>
<p></p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Site</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="jamnudda">
	<th class="hogerstalld">1</th>
	<td>
	94.75.211.74		
	</td>
	<td align="right">8,014</td>
	<td align="right">4.07</td>
	<td align="right">65,445,944</td>
	<td align="right">0.58</td>
	<td align="right">21</td>
	<td align="right">0.26</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	185.152.65.167		
	</td>
	<td align="right">7,663</td>
	<td align="right">3.89</td>
	<td align="right">62,283,282</td>
	<td align="right">0.56</td>
	<td align="right">19</td>
	<td align="right">0.23</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	185.39.146.214		
	</td>
	<td align="right">6,549</td>
	<td align="right">3.32</td>
	<td align="right">53,829,092</td>
	<td align="right">0.48</td>
	<td align="right">15</td>
	<td align="right">0.19</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	94.247.174.83		
	</td>
	<td align="right">6,130</td>
	<td align="right">3.11</td>
	<td align="right">49,870,687</td>
	<td align="right">0.45</td>
	<td align="right">17</td>
	<td align="right">0.20</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	82.103.145.126		
	</td>
	<td align="right">5,494</td>
	<td align="right">2.79</td>
	<td align="right">45,668,270</td>
	<td align="right">0.41</td>
	<td align="right">13</td>
	<td align="right">0.16</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	109.123.101.103		
	</td>
	<td align="right">5,253</td>
	<td align="right">2.67</td>
	<td align="right">40,399,296</td>
	<td align="right">0.36</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	46.246.122.10		
	</td>
	<td align="right">5,173</td>
	<td align="right">2.63</td>
	<td align="right">39,628,934</td>
	<td align="right">0.35</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	185.136.156.82		
	</td>
	<td align="right">3,838</td>
	<td align="right">1.95</td>
	<td align="right">29,513,111</td>
	<td align="right">0.26</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	82.103.139.165		
	</td>
	<td align="right">3,769</td>
	<td align="right">1.91</td>
	<td align="right">29,148,589</td>
	<td align="right">0.26</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">10</th>
	<td>
	81.17.62.205		
	</td>
	<td align="right">3,768</td>
	<td align="right">1.91</td>
	<td align="right">30,964,409</td>
	<td align="right">0.28</td>
	<td align="right">10</td>
	<td align="right">0.12</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">11</th>
	<td>
	46.59.25.88		
	</td>
	<td align="right">3,601</td>
	<td align="right">1.83</td>
	<td align="right">247,108,385</td>
	<td align="right">2.21</td>
	<td align="right">72</td>
	<td align="right">0.86</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">12</th>
	<td>
	63.143.42.242		
	</td>
	<td align="right">3,381</td>
	<td align="right">1.72</td>
	<td align="right">118,452</td>
	<td align="right">0.00</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">13</th>
	<td>
	37.252.231.50		
	</td>
	<td align="right">3,145</td>
	<td align="right">1.60</td>
	<td align="right">25,902,123</td>
	<td align="right">0.23</td>
	<td align="right">11</td>
	<td align="right">0.14</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">14</th>
	<td>
	85.195.116.134		
	</td>
	<td align="right">2,961</td>
	<td align="right">1.50</td>
	<td align="right">24,138,566</td>
	<td align="right">0.22</td>
	<td align="right">9</td>
	<td align="right">0.11</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">15</th>
	<td>
	5.172.196.188		
	</td>
	<td align="right">2,906</td>
	<td align="right">1.48</td>
	<td align="right">24,239,247</td>
	<td align="right">0.22</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">16</th>
	<td>
	185.70.76.23		
	</td>
	<td align="right">2,817</td>
	<td align="right">1.43</td>
	<td align="right">23,447,719</td>
	<td align="right">0.21</td>
	<td align="right">6</td>
	<td align="right">0.08</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">17</th>
	<td>
	151.106.52.134		
	</td>
	<td align="right">2,536</td>
	<td align="right">1.29</td>
	<td align="right">22,190,403</td>
	<td align="right">0.20</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">18</th>
	<td>
	185.246.208.82		
	</td>
	<td align="right">2,044</td>
	<td align="right">1.04</td>
	<td align="right">17,910,345</td>
	<td align="right">0.16</td>
	<td align="right">3</td>
	<td align="right">0.04</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">19</th>
	<td>
	46.20.45.18		
	</td>
	<td align="right">1,970</td>
	<td align="right">1.00</td>
	<td align="right">17,867,900</td>
	<td align="right">0.16</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">20</th>
	<td>
	82.103.136.16		
	</td>
	<td align="right">1,148</td>
	<td align="right">0.58</td>
	<td align="right">10,412,360</td>
	<td align="right">0.09</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>

</tbody>
</table>

