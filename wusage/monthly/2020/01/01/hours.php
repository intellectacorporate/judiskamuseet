


<h2>Statistics for http://www.u5645470.fsdata.se, Month of 2020-1-1 to 2020-1-31</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Traffic by Hour</h3>
<p>Figures are averages for that hour
	of each day in the time period analyzed.</p>
<img src="h.png" width="468" height="351">
<br /><br />
<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Hour</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	<th scope='col'>Bits per Sec</th>
	<th scope='col'>Bytes per Sec</th>
	
	</tr>
</thead>
<tbody>
<tr class="jamnudda">
	<th align="right">00:00</th>
	<td align="right">201.45</td>
	<td align="right">3.17</td>
	<td align="right">9,687,174.06</td>
	<td align="right">2.68</td>
	<td align="right">4.80</td>
	<td align="right">1.77</td>
	<td align="right">21,527.05</td>
	<td align="right">2,690.88</td>
	
</tr>
<tr class="jamn">
	<th align="right">01:00</th>
	<td align="right">191.48</td>
	<td align="right">3.01</td>
	<td align="right">6,990,647.87</td>
	<td align="right">1.93</td>
	<td align="right">6.83</td>
	<td align="right">2.53</td>
	<td align="right">15,534.77</td>
	<td align="right">1,941.85</td>
	
</tr>
<tr class="udda">
	<th align="right">02:00</th>
	<td align="right">164.00</td>
	<td align="right">2.58</td>
	<td align="right">3,766,455.06</td>
	<td align="right">1.04</td>
	<td align="right">5.46</td>
	<td align="right">2.02</td>
	<td align="right">8,369.90</td>
	<td align="right">1,046.24</td>
	
</tr>
<tr class="jamn">
	<th align="right">03:00</th>
	<td align="right">164.94</td>
	<td align="right">2.60</td>
	<td align="right">3,094,392.55</td>
	<td align="right">0.86</td>
	<td align="right">4.96</td>
	<td align="right">1.83</td>
	<td align="right">6,876.43</td>
	<td align="right">859.55</td>
	
</tr>
<tr class="udda">
	<th align="right">04:00</th>
	<td align="right">161.58</td>
	<td align="right">2.54</td>
	<td align="right">3,729,362.10</td>
	<td align="right">1.03</td>
	<td align="right">4.92</td>
	<td align="right">1.82</td>
	<td align="right">8,287.47</td>
	<td align="right">1,035.93</td>
	
</tr>
<tr class="jamn">
	<th align="right">05:00</th>
	<td align="right">159.16</td>
	<td align="right">2.50</td>
	<td align="right">4,068,286.06</td>
	<td align="right">1.13</td>
	<td align="right">6.92</td>
	<td align="right">2.56</td>
	<td align="right">9,040.64</td>
	<td align="right">1,130.08</td>
	
</tr>
<tr class="udda">
	<th align="right">06:00</th>
	<td align="right">172.19</td>
	<td align="right">2.71</td>
	<td align="right">5,427,925.87</td>
	<td align="right">1.50</td>
	<td align="right">5.67</td>
	<td align="right">2.10</td>
	<td align="right">12,062.06</td>
	<td align="right">1,507.76</td>
	
</tr>
<tr class="jamn">
	<th align="right">07:00</th>
	<td align="right">188.81</td>
	<td align="right">2.97</td>
	<td align="right">7,438,703.03</td>
	<td align="right">2.06</td>
	<td align="right">6.80</td>
	<td align="right">2.52</td>
	<td align="right">16,530.45</td>
	<td align="right">2,066.31</td>
	
</tr>
<tr class="udda">
	<th align="right">08:00</th>
	<td align="right">250.03</td>
	<td align="right">3.93</td>
	<td align="right">16,687,225.77</td>
	<td align="right">4.62</td>
	<td align="right">9.15</td>
	<td align="right">3.39</td>
	<td align="right">37,082.72</td>
	<td align="right">4,635.34</td>
	
</tr>
<tr class="jamn">
	<th align="right">09:00</th>
	<td align="right">357.84</td>
	<td align="right">5.63</td>
	<td align="right">23,715,414.87</td>
	<td align="right">6.56</td>
	<td align="right">13.57</td>
	<td align="right">5.02</td>
	<td align="right">52,700.92</td>
	<td align="right">6,587.62</td>
	
</tr>
<tr class="udda">
	<th align="right">10:00</th>
	<td align="right">356.06</td>
	<td align="right">5.60</td>
	<td align="right">23,703,416.87</td>
	<td align="right">6.56</td>
	<td align="right">16.33</td>
	<td align="right">6.04</td>
	<td align="right">52,674.26</td>
	<td align="right">6,584.28</td>
	
</tr>
<tr class="jamn">
	<th align="right">11:00</th>
	<td align="right">360.52</td>
	<td align="right">5.67</td>
	<td align="right">27,902,630.00</td>
	<td align="right">7.72</td>
	<td align="right">16.66</td>
	<td align="right">6.16</td>
	<td align="right">62,005.84</td>
	<td align="right">7,750.73</td>
	
</tr>
<tr class="udda">
	<th align="right">12:00</th>
	<td align="right">341.42</td>
	<td align="right">5.37</td>
	<td align="right">26,300,012.03</td>
	<td align="right">7.28</td>
	<td align="right">14.80</td>
	<td align="right">5.47</td>
	<td align="right">58,444.47</td>
	<td align="right">7,305.56</td>
	
</tr>
<tr class="jamn">
	<th align="right">13:00</th>
	<td align="right">341.39</td>
	<td align="right">5.37</td>
	<td align="right">23,940,670.23</td>
	<td align="right">6.62</td>
	<td align="right">15.49</td>
	<td align="right">5.73</td>
	<td align="right">53,201.49</td>
	<td align="right">6,650.19</td>
	
</tr>
<tr class="udda">
	<th align="right">14:00</th>
	<td align="right">369.55</td>
	<td align="right">5.82</td>
	<td align="right">22,615,674.26</td>
	<td align="right">6.26</td>
	<td align="right">15.67</td>
	<td align="right">5.80</td>
	<td align="right">50,257.05</td>
	<td align="right">6,282.13</td>
	
</tr>
<tr class="jamn">
	<th align="right">15:00</th>
	<td align="right">311.65</td>
	<td align="right">4.90</td>
	<td align="right">21,887,467.13</td>
	<td align="right">6.06</td>
	<td align="right">14.28</td>
	<td align="right">5.28</td>
	<td align="right">48,638.82</td>
	<td align="right">6,079.85</td>
	
</tr>
<tr class="udda">
	<th align="right">16:00</th>
	<td align="right">326.71</td>
	<td align="right">5.14</td>
	<td align="right">20,025,410.42</td>
	<td align="right">5.54</td>
	<td align="right">13.44</td>
	<td align="right">4.97</td>
	<td align="right">44,500.91</td>
	<td align="right">5,562.61</td>
	
</tr>
<tr class="jamn">
	<th align="right">17:00</th>
	<td align="right">302.13</td>
	<td align="right">4.75</td>
	<td align="right">17,192,853.39</td>
	<td align="right">4.76</td>
	<td align="right">14.11</td>
	<td align="right">5.22</td>
	<td align="right">38,206.34</td>
	<td align="right">4,775.79</td>
	
</tr>
<tr class="udda">
	<th align="right">18:00</th>
	<td align="right">279.13</td>
	<td align="right">4.39</td>
	<td align="right">15,735,421.81</td>
	<td align="right">4.35</td>
	<td align="right">11.97</td>
	<td align="right">4.43</td>
	<td align="right">34,967.60</td>
	<td align="right">4,370.95</td>
	
</tr>
<tr class="jamn">
	<th align="right">19:00</th>
	<td align="right">291.94</td>
	<td align="right">4.59</td>
	<td align="right">18,296,987.03</td>
	<td align="right">5.06</td>
	<td align="right">13.22</td>
	<td align="right">4.89</td>
	<td align="right">40,659.97</td>
	<td align="right">5,082.50</td>
	
</tr>
<tr class="udda">
	<th align="right">20:00</th>
	<td align="right">293.61</td>
	<td align="right">4.62</td>
	<td align="right">16,351,407.39</td>
	<td align="right">4.52</td>
	<td align="right">12.51</td>
	<td align="right">4.63</td>
	<td align="right">36,336.46</td>
	<td align="right">4,542.06</td>
	
</tr>
<tr class="jamn">
	<th align="right">21:00</th>
	<td align="right">275.23</td>
	<td align="right">4.33</td>
	<td align="right">15,789,410.06</td>
	<td align="right">4.37</td>
	<td align="right">13.71</td>
	<td align="right">5.07</td>
	<td align="right">35,087.58</td>
	<td align="right">4,385.95</td>
	
</tr>
<tr class="udda">
	<th align="right">22:00</th>
	<td align="right">263.52</td>
	<td align="right">4.15</td>
	<td align="right">15,364,089.23</td>
	<td align="right">4.25</td>
	<td align="right">11.62</td>
	<td align="right">4.30</td>
	<td align="right">34,142.42</td>
	<td align="right">4,267.80</td>
	
</tr>
<tr class="jamn">
	<th align="right">23:00</th>
	<td align="right">230.74</td>
	<td align="right">3.63</td>
	<td align="right">11,727,351.94</td>
	<td align="right">3.24</td>
	<td align="right">17.47</td>
	<td align="right">6.46</td>
	<td align="right">26,060.78</td>
	<td align="right">3,257.60</td>
	
</tr>

</tbody>
</table>

