


<h2>Statistics for http://www.u5645470.fsdata.se, Month of 2020-1-1 to 2020-1-31</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Traffic by Day</h3>
<p>The continuous line represents a 7-day moving average.</p>
<img src="d.png" width="468" height="351">
<br /><br />


<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Date</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	<th scope='col'>Bits per Sec</th>
	<th scope='col'>Bytes per Sec</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th align="right">2020-1-1</th>
	<td align="right">8,954	</td>
	<td align="right">4.55	</td>
	<td align="right">484,495,549	</td>
	<td align="right">4.32	</td>
	<td align="right">313	</td>
	<td align="right">3.73	</td>
	<td align="right">44,860.70	</td>
	<td align="right">5,607.59	</td>
</tr>
<tr class="jamn">
	<th align="right">2020-1-2</th>
	<td align="right">9,962	</td>
	<td align="right">5.06	</td>
	<td align="right">602,318,604	</td>
	<td align="right">5.38	</td>
	<td align="right">354	</td>
	<td align="right">4.22	</td>
	<td align="right">55,770.24	</td>
	<td align="right">6,971.28	</td>
</tr>
<tr class="udda">
	<th align="right">2020-1-3</th>
	<td align="right">9,759	</td>
	<td align="right">4.95	</td>
	<td align="right">565,553,364	</td>
	<td align="right">5.05	</td>
	<td align="right">385	</td>
	<td align="right">4.59	</td>
	<td align="right">52,366.05	</td>
	<td align="right">6,545.76	</td>
</tr>
<tr class="jamn">
	<th align="right">2020-1-4</th>
	<td align="right">10,256	</td>
	<td align="right">5.21	</td>
	<td align="right">587,207,076	</td>
	<td align="right">5.24	</td>
	<td align="right">444	</td>
	<td align="right">5.30	</td>
	<td align="right">54,371.03	</td>
	<td align="right">6,796.38	</td>
</tr>
<tr class="udda">
	<th align="right">2020-1-5</th>
	<td align="right">8,652	</td>
	<td align="right">4.39	</td>
	<td align="right">412,746,777	</td>
	<td align="right">3.68	</td>
	<td align="right">367	</td>
	<td align="right">4.38	</td>
	<td align="right">38,217.29	</td>
	<td align="right">4,777.16	</td>
</tr>
<tr class="jamn">
	<th align="right">2020-1-6</th>
	<td align="right">8,713	</td>
	<td align="right">4.42	</td>
	<td align="right">411,804,334	</td>
	<td align="right">3.68	</td>
	<td align="right">341	</td>
	<td align="right">4.07	</td>
	<td align="right">38,130.03	</td>
	<td align="right">4,766.25	</td>
</tr>
<tr class="udda">
	<th align="right">2020-1-7</th>
	<td align="right">11,643	</td>
	<td align="right">5.91	</td>
	<td align="right">707,324,672	</td>
	<td align="right">6.31	</td>
	<td align="right">673	</td>
	<td align="right">8.03	</td>
	<td align="right">65,493.03	</td>
	<td align="right">8,186.63	</td>
</tr>
<tr class="jamn">
	<th align="right">2020-1-8</th>
	<td align="right">10,993	</td>
	<td align="right">5.58	</td>
	<td align="right">589,966,854	</td>
	<td align="right">5.27	</td>
	<td align="right">609	</td>
	<td align="right">7.27	</td>
	<td align="right">54,626.56	</td>
	<td align="right">6,828.32	</td>
</tr>
<tr class="udda">
	<th align="right">2020-1-9</th>
	<td align="right">11,740	</td>
	<td align="right">5.96	</td>
	<td align="right">869,819,317	</td>
	<td align="right">7.76	</td>
	<td align="right">495	</td>
	<td align="right">5.91	</td>
	<td align="right">80,538.83	</td>
	<td align="right">10,067.35	</td>
</tr>
<tr class="jamn">
	<th align="right">2020-1-10</th>
	<td align="right">10,959	</td>
	<td align="right">5.56	</td>
	<td align="right">713,890,961	</td>
	<td align="right">6.37	</td>
	<td align="right">476	</td>
	<td align="right">5.68	</td>
	<td align="right">66,101.01	</td>
	<td align="right">8,262.63	</td>
</tr>
<tr class="udda">
	<th align="right">2020-1-11</th>
	<td align="right">9,010	</td>
	<td align="right">4.57	</td>
	<td align="right">432,809,212	</td>
	<td align="right">3.86	</td>
	<td align="right">416	</td>
	<td align="right">4.96	</td>
	<td align="right">40,074.93	</td>
	<td align="right">5,009.37	</td>
</tr>
<tr class="jamn">
	<th align="right">2020-1-12</th>
	<td align="right">10,078	</td>
	<td align="right">5.12	</td>
	<td align="right">553,996,570	</td>
	<td align="right">4.94	</td>
	<td align="right">372	</td>
	<td align="right">4.44	</td>
	<td align="right">51,295.98	</td>
	<td align="right">6,412.00	</td>
</tr>
<tr class="udda">
	<th align="right">2020-1-13</th>
	<td align="right">10,967	</td>
	<td align="right">5.57	</td>
	<td align="right">561,340,896	</td>
	<td align="right">5.01	</td>
	<td align="right">518	</td>
	<td align="right">6.18	</td>
	<td align="right">51,976.01	</td>
	<td align="right">6,497.00	</td>
</tr>
<tr class="jamn">
	<th align="right">2020-1-14</th>
	<td align="right">10,704	</td>
	<td align="right">5.43	</td>
	<td align="right">599,259,048	</td>
	<td align="right">5.35	</td>
	<td align="right">493	</td>
	<td align="right">5.88	</td>
	<td align="right">55,486.95	</td>
	<td align="right">6,935.87	</td>
</tr>
<tr class="udda">
	<th align="right">2020-1-15</th>
	<td align="right">12,578	</td>
	<td align="right">6.38	</td>
	<td align="right">699,468,895	</td>
	<td align="right">6.24	</td>
	<td align="right">554	</td>
	<td align="right">6.61	</td>
	<td align="right">64,765.64	</td>
	<td align="right">8,095.70	</td>
</tr>
<tr class="jamn">
	<th align="right">2020-1-16</th>
	<td align="right">11,641	</td>
	<td align="right">5.91	</td>
	<td align="right">710,863,603	</td>
	<td align="right">6.34	</td>
	<td align="right">435	</td>
	<td align="right">5.19	</td>
	<td align="right">65,820.70	</td>
	<td align="right">8,227.59	</td>
</tr>
<tr class="udda">
	<th align="right">2020-1-17</th>
	<td align="right">10,750	</td>
	<td align="right">5.46	</td>
	<td align="right">598,368,294	</td>
	<td align="right">5.34	</td>
	<td align="right">416	</td>
	<td align="right">4.96	</td>
	<td align="right">55,404.47	</td>
	<td align="right">6,925.56	</td>
</tr>
<tr class="jamn">
	<th align="right">2020-1-18</th>
	<td align="right">9,704	</td>
	<td align="right">4.93	</td>
	<td align="right">524,677,147	</td>
	<td align="right">4.68	</td>
	<td align="right">364	</td>
	<td align="right">4.34	</td>
	<td align="right">48,581.22	</td>
	<td align="right">6,072.65	</td>
</tr>
<tr class="udda">
	<th align="right">2020-1-19</th>
	<td align="right">9,944	</td>
	<td align="right">5.05	</td>
	<td align="right">578,678,887	</td>
	<td align="right">5.16	</td>
	<td align="right">356	</td>
	<td align="right">4.25	</td>
	<td align="right">53,581.38	</td>
	<td align="right">6,697.67	</td>
</tr>
<tr class="jamn">
	<th align="right">2020-1-20</th>
	<td align="right">0	</td>
	<td align="right">0.00	</td>
	<td align="right">0	</td>
	<td align="right">0.00	</td>
	<td align="right">0	</td>
	<td align="right">0.00	</td>
	<td align="right">0.00	</td>
	<td align="right">0.00	</td>
</tr>
<tr class="udda">
	<th align="right">2020-1-21</th>
	<td align="right">0	</td>
	<td align="right">0.00	</td>
	<td align="right">0	</td>
	<td align="right">0.00	</td>
	<td align="right">0	</td>
	<td align="right">0.00	</td>
	<td align="right">0.00	</td>
	<td align="right">0.00	</td>
</tr>
<tr class="jamn">
	<th align="right">2020-1-22</th>
	<td align="right">0	</td>
	<td align="right">0.00	</td>
	<td align="right">0	</td>
	<td align="right">0.00	</td>
	<td align="right">0	</td>
	<td align="right">0.00	</td>
	<td align="right">0.00	</td>
	<td align="right">0.00	</td>
</tr>
<tr class="udda">
	<th align="right">2020-1-23</th>
	<td align="right">0	</td>
	<td align="right">0.00	</td>
	<td align="right">0	</td>
	<td align="right">0.00	</td>
	<td align="right">0	</td>
	<td align="right">0.00	</td>
	<td align="right">0.00	</td>
	<td align="right">0.00	</td>
</tr>
<tr class="jamn">
	<th align="right">2020-1-24</th>
	<td align="right">0	</td>
	<td align="right">0.00	</td>
	<td align="right">0	</td>
	<td align="right">0.00	</td>
	<td align="right">0	</td>
	<td align="right">0.00	</td>
	<td align="right">0.00	</td>
	<td align="right">0.00	</td>
</tr>
<tr class="udda">
	<th align="right">2020-1-25</th>
	<td align="right">0	</td>
	<td align="right">0.00	</td>
	<td align="right">0	</td>
	<td align="right">0.00	</td>
	<td align="right">0	</td>
	<td align="right">0.00	</td>
	<td align="right">0.00	</td>
	<td align="right">0.00	</td>
</tr>
<tr class="jamn">
	<th align="right">2020-1-26</th>
	<td align="right">0	</td>
	<td align="right">0.00	</td>
	<td align="right">0	</td>
	<td align="right">0.00	</td>
	<td align="right">0	</td>
	<td align="right">0.00	</td>
	<td align="right">0.00	</td>
	<td align="right">0.00	</td>
</tr>
<tr class="udda">
	<th align="right">2020-1-27</th>
	<td align="right">0	</td>
	<td align="right">0.00	</td>
	<td align="right">0	</td>
	<td align="right">0.00	</td>
	<td align="right">0	</td>
	<td align="right">0.00	</td>
	<td align="right">0.00	</td>
	<td align="right">0.00	</td>
</tr>
<tr class="jamn">
	<th align="right">2020-1-28</th>
	<td align="right">0	</td>
	<td align="right">0.00	</td>
	<td align="right">0	</td>
	<td align="right">0.00	</td>
	<td align="right">0	</td>
	<td align="right">0.00	</td>
	<td align="right">0.00	</td>
	<td align="right">0.00	</td>
</tr>
<tr class="udda">
	<th align="right">2020-1-29</th>
	<td align="right">0	</td>
	<td align="right">0.00	</td>
	<td align="right">0	</td>
	<td align="right">0.00	</td>
	<td align="right">0	</td>
	<td align="right">0.00	</td>
	<td align="right">0.00	</td>
	<td align="right">0.00	</td>
</tr>
<tr class="jamn">
	<th align="right">2020-1-30</th>
	<td align="right">0	</td>
	<td align="right">0.00	</td>
	<td align="right">0	</td>
	<td align="right">0.00	</td>
	<td align="right">0	</td>
	<td align="right">0.00	</td>
	<td align="right">0.00	</td>
	<td align="right">0.00	</td>
</tr>
<tr class="udda">
	<th align="right">2020-1-31</th>
	<td align="right">0	</td>
	<td align="right">0.00	</td>
	<td align="right">0	</td>
	<td align="right">0.00	</td>
	<td align="right">0	</td>
	<td align="right">0.00	</td>
	<td align="right">0.00	</td>
	<td align="right">0.00	</td>
</tr>

</tbody>
</table>

