


<h2>Statistics for http://www.u5645470.fsdata.se, Month of 2020-1-1 to 2020-1-31</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 20 of 276 Referring Sites,
Sorted by Access Count

</h3>
<p></p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Referring Site</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th class="hogerstalld">1</th>
	<td>
	<a href="https://judiskamuseet.se">
	https://judiskamuseet.se</a>
	</td>
	<td align="right">81,570</td>
	<td align="right">88.95</td>
	<td align="right">9,016,749,080</td>
	<td align="right">97.09</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	<a href="http://judiskamuseet.se">
	http://judiskamuseet.se</a>
	</td>
	<td align="right">4,918</td>
	<td align="right">5.36</td>
	<td align="right">58,043,790</td>
	<td align="right">0.63</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	<a href="https://www.google.com">
	https://www.google.com</a>
	</td>
	<td align="right">2,353</td>
	<td align="right">2.57</td>
	<td align="right">98,228,066</td>
	<td align="right">1.06</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	<a href="https://www.google.se">
	https://www.google.se</a>
	</td>
	<td align="right">1,259</td>
	<td align="right">1.37</td>
	<td align="right">52,614,902</td>
	<td align="right">0.57</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	<a href="http://www.google.com">
	http://www.google.com</a>
	</td>
	<td align="right">178</td>
	<td align="right">0.19</td>
	<td align="right">1,280,908</td>
	<td align="right">0.01</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	<a href="https://www.bing.com">
	https://www.bing.com</a>
	</td>
	<td align="right">86</td>
	<td align="right">0.09</td>
	<td align="right">7,619,359</td>
	<td align="right">0.08</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	<a href="http://m.facebook.com">
	http://m.facebook.com</a>
	</td>
	<td align="right">71</td>
	<td align="right">0.08</td>
	<td align="right">861,578</td>
	<td align="right">0.01</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	<a href="https://webcache.googleusercontent.com">
	https://webcache.googleusercontent.com</a>
	</td>
	<td align="right">39</td>
	<td align="right">0.04</td>
	<td align="right">1,740,656</td>
	<td align="right">0.02</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	<a href="http://www.judiska-museet.se">
	http://www.judiska-museet.se</a>
	</td>
	<td align="right">39</td>
	<td align="right">0.04</td>
	<td align="right">99,070</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">10</th>
	<td>
	<a href="http://www.stockholmmuseums.se">
	http://www.stockholmmuseums.se</a>
	</td>
	<td align="right">24</td>
	<td align="right">0.03</td>
	<td align="right">177,920</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">11</th>
	<td>
	<a href="https://stop-nark.ru">
	https://stop-nark.ru</a>
	</td>
	<td align="right">24</td>
	<td align="right">0.03</td>
	<td align="right">1,365,906</td>
	<td align="right">0.01</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">12</th>
	<td>
	<a href="https://furniturehomewares.com">
	https://furniturehomewares.com</a>
	</td>
	<td align="right">21</td>
	<td align="right">0.02</td>
	<td align="right">1,133,991</td>
	<td align="right">0.01</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">13</th>
	<td>
	<a href="https://duckduckgo.com">
	https://duckduckgo.com</a>
	</td>
	<td align="right">21</td>
	<td align="right">0.02</td>
	<td align="right">286,077</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">14</th>
	<td>
	<a href="https://bpro1.top">
	https://bpro1.top</a>
	</td>
	<td align="right">21</td>
	<td align="right">0.02</td>
	<td align="right">1,134,039</td>
	<td align="right">0.01</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">15</th>
	<td>
	<a href="https://myactivity.google.com">
	https://myactivity.google.com</a>
	</td>
	<td align="right">20</td>
	<td align="right">0.02</td>
	<td align="right">3,232,437</td>
	<td align="right">0.03</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">16</th>
	<td>
	<a href="https://www.kurbits.nu">
	https://www.kurbits.nu</a>
	</td>
	<td align="right">19</td>
	<td align="right">0.02</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">17</th>
	<td>
	<a href="https://www.google.de">
	https://www.google.de</a>
	</td>
	<td align="right">18</td>
	<td align="right">0.02</td>
	<td align="right">247,888</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">18</th>
	<td>
	<a href="https://docs.google.com">
	https://docs.google.com</a>
	</td>
	<td align="right">16</td>
	<td align="right">0.02</td>
	<td align="right">174,576</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">19</th>
	<td>
	<a href="https://classroom.google.com">
	https://classroom.google.com</a>
	</td>
	<td align="right">15</td>
	<td align="right">0.02</td>
	<td align="right">128,919</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">20</th>
	<td>
	<a href="https://fit-discount.ru">
	https://fit-discount.ru</a>
	</td>
	<td align="right">15</td>
	<td align="right">0.02</td>
	<td align="right">810,021</td>
	<td align="right">0.01</td>
</tr>

</tbody>
</table>

