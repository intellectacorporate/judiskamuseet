


<h2>Statistics for http://www.u5645470.fsdata.se, Month of 2020-1-1 to 2020-1-31</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 10 of 294 Browsers (User Agents),
Sorted by Access Count

</h3>
<p></p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Product</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th class="hogerstalld">1</th>
	<td>
	Pingdom.com_bot_version_1.4_		
	</td>
	<td align="right">80,023</td>
	<td align="right">40.71</td>
	<td align="right">652,869,824</td>
	<td align="right">5.85</td>
	<td align="right">132</td>
	<td align="right">1.59</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	Safari/537.36		
	</td>
	<td align="right">40,367</td>
	<td align="right">20.53</td>
	<td align="right">3,930,590,801</td>
	<td align="right">35.24</td>
	<td align="right">2,045</td>
	<td align="right">24.57</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	Safari/604.1		
	</td>
	<td align="right">23,826</td>
	<td align="right">12.12</td>
	<td align="right">2,532,387,852</td>
	<td align="right">22.70</td>
	<td align="right">1,429</td>
	<td align="right">17.17</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	Mozilla/5.0 		
	</td>
	<td align="right">11,676</td>
	<td align="right">5.94</td>
	<td align="right">885,172,072</td>
	<td align="right">7.94</td>
	<td align="right">1,281</td>
	<td align="right">15.40</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	Safari/605.1.15		
	</td>
	<td align="right">7,315</td>
	<td align="right">3.72</td>
	<td align="right">768,925,393</td>
	<td align="right">6.89</td>
	<td align="right">403</td>
	<td align="right">4.84</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	UptimeRobot/2.0		
	</td>
	<td align="right">3,381</td>
	<td align="right">1.72</td>
	<td align="right">118,452</td>
	<td align="right">0.00</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	bingbot/2.0		
	</td>
	<td align="right">3,027</td>
	<td align="right">1.54</td>
	<td align="right">254,014,093</td>
	<td align="right">2.28</td>
	<td align="right">716</td>
	<td align="right">8.61</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	Safari/537.36 Edge/18.18362		
	</td>
	<td align="right">2,587</td>
	<td align="right">1.32</td>
	<td align="right">371,336,876</td>
	<td align="right">3.33</td>
	<td align="right">107</td>
	<td align="right">1.29</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	Safari/537.36 (compatible; Googlebot/2.1;  http://www.google.com/bot.html)		
	</td>
	<td align="right">2,247</td>
	<td align="right">1.14</td>
	<td align="right">22,907,882</td>
	<td align="right">0.21</td>
	<td align="right">525</td>
	<td align="right">6.31</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">10</th>
	<td>
	facebookexternalhit/1.1 		
	</td>
	<td align="right">1,531</td>
	<td align="right">0.78</td>
	<td align="right">125,321,149</td>
	<td align="right">1.12</td>
	<td align="right">196</td>
	<td align="right">2.36</td>
</tr>

</tbody>
</table>

