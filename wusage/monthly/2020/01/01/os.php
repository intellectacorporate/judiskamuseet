


<h2>Statistics for http://www.u5645470.fsdata.se, Month of 2020-1-1 to 2020-1-31</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 10 of 12 Operating Systems,
Sorted by Access Count

</h3>
<p></p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Product</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	<th scope='col'>Visits</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th class="hogerstalld">1</th>
	<td>
	Other		
	</td>
	<td align="right">99,662</td>
	<td align="right">50.70</td>
	<td align="right">1,733,066,132</td>
	<td align="right">15.54</td>
	<td align="right">2,113</td>
	<td align="right">25.22</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	Mac OS X		
	</td>
	<td align="right">86,552</td>
	<td align="right">44.03</td>
	<td align="right">8,701,290,904</td>
	<td align="right">78.01</td>
	<td align="right">5,071</td>
	<td align="right">60.51</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	Windows NT		
	</td>
	<td align="right">6,838</td>
	<td align="right">3.48</td>
	<td align="right">675,310,904</td>
	<td align="right">6.05</td>
	<td align="right">285</td>
	<td align="right">3.40</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	Linux		
	</td>
	<td align="right">2,896</td>
	<td align="right">1.47</td>
	<td align="right">11,096,771</td>
	<td align="right">0.10</td>
	<td align="right">892</td>
	<td align="right">10.65</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	Windows XP		
	</td>
	<td align="right">329</td>
	<td align="right">0.17</td>
	<td align="right">16,059,825</td>
	<td align="right">0.14</td>
	<td align="right">11</td>
	<td align="right">0.14</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	Windows Codename Longhorn		
	</td>
	<td align="right">118</td>
	<td align="right">0.06</td>
	<td align="right">10,632,914</td>
	<td align="right">0.10</td>
	<td align="right">5</td>
	<td align="right">0.07</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	Windows 2000		
	</td>
	<td align="right">77</td>
	<td align="right">0.04</td>
	<td align="right">3,880,557</td>
	<td align="right">0.03</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	Windows 2003		
	</td>
	<td align="right">49</td>
	<td align="right">0.02</td>
	<td align="right">545,155</td>
	<td align="right">0.00</td>
	<td align="right">1</td>
	<td align="right">0.01</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	Windows 95		
	</td>
	<td align="right">44</td>
	<td align="right">0.02</td>
	<td align="right">2,385,138</td>
	<td align="right">0.02</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">10</th>
	<td>
	Mac OS		
	</td>
	<td align="right">9</td>
	<td align="right">0.00</td>
	<td align="right">22,982</td>
	<td align="right">0.00</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>

</tbody>
</table>

