


<h2>Statistics for http://www.u5645470.fsdata.se, Month of 2020-1-1 to 2020-1-31</h2>


<h3>Menu</h3>

<div class='colmenu'>
<div class='column'>
<ul id='infolankar'>
<li><a href="../../../../index.php">Calendar of Reports</a></li>
<li><a href="index.php">Executive Summary and Totals</a></li>
<li><a href="hours.php">Accesses by Hour</a></li>
<li><a href="days.php">Accesses by Day</a></li>
<li><a href="documents.php">Top Documents</a></li>
<li><a href="directories.php">Documents by Directory</a></li>
<li><a href="types.php">File Types (Extensions)</a></li>

</ul>
</div>
<div class='column'>
<ul id='infolankar'>
<li><a href="sites.php">Top Visitor Sites</a></li>
<li><a href="useragents.php">Top Web Browsers</a></li>
<li><a href="os.php">Top Operating Systems</a></li>
<li><a href="referringsites.php">Top Referring Sites</a></li>
<li><a href="notfound.php">Documents Not Found</a></li>
<li><a href="resultcodes.php">Accesses by Result Code</a></li>

</ul>
</div>
<div class='colmenufoot'></div>
</div>

<h3>Top 10 of 26 File Types (Extensions), 
Sorted by Access Count

</h3>
<p>Individual file types as determined by file extensions. All URLs that
do not contain an extension are counted as directories.</p>
<div align="center">

</div>

<table cellspacing='0' class='styladtabell'>
<thead>
	<tr>
	<th scope='col'>Rank</th>
	<th scope='col'>Type</th>
	<th scope='col'>Accesses</th>
	<th scope='col'>%</th>
	<th scope='col'>Bytes</th>
	<th scope='col'>%</th>
	</tr>
</thead>
<tbody>
<tr class="udda">
	<th class="hogerstalld">1</th>
	<td>
	Directory (folder)		
	</td>
	<td align="right">82,878</td>
	<td align="right">42.07</td>
	<td align="right">710,681,982</td>
	<td align="right">6.34</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">2</th>
	<td>
	png		
	</td>
	<td align="right">47,705</td>
	<td align="right">24.21</td>
	<td align="right">577,738,778</td>
	<td align="right">5.16</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">3</th>
	<td>
	js		
	</td>
	<td align="right">24,218</td>
	<td align="right">12.29</td>
	<td align="right">500,394,932</td>
	<td align="right">4.47</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">4</th>
	<td>
	jpg		
	</td>
	<td align="right">23,664</td>
	<td align="right">12.01</td>
	<td align="right">8,160,103,327</td>
	<td align="right">72.83</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">5</th>
	<td>
	css		
	</td>
	<td align="right">9,509</td>
	<td align="right">4.83</td>
	<td align="right">515,965,926</td>
	<td align="right">4.60</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">6</th>
	<td>
	php		
	</td>
	<td align="right">4,438</td>
	<td align="right">2.25</td>
	<td align="right">43,639,812</td>
	<td align="right">0.39</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">7</th>
	<td>
	ttf		
	</td>
	<td align="right">3,126</td>
	<td align="right">1.59</td>
	<td align="right">117,406,131</td>
	<td align="right">1.05</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">8</th>
	<td>
	txt		
	</td>
	<td align="right">857</td>
	<td align="right">0.44</td>
	<td align="right">56,282</td>
	<td align="right">0.00</td>
</tr>
<tr class="udda">
	<th class="hogerstalld">9</th>
	<td>
	jpeg		
	</td>
	<td align="right">251</td>
	<td align="right">0.13</td>
	<td align="right">406,713,521</td>
	<td align="right">3.63</td>
</tr>
<tr class="jamn">
	<th class="hogerstalld">10</th>
	<td>
	ico		
	</td>
	<td align="right">104</td>
	<td align="right">0.05</td>
	<td align="right">0</td>
	<td align="right">0.00</td>
</tr>

</tbody>
</table>

