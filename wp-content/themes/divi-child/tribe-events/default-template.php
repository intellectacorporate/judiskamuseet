<?php
/**
 * Default Events Template
 * This file is the basic wrapper template for all the views if 'Default Events Template'
 * is selected in Events -> Settings -> Template -> Events Template.
 *
 * Override this template in your own theme by creating a file at [your-theme]/tribe-events/default-template.php
 *
 * @package TribeEventsCalendar
 *
 */

if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

get_header();
?>


<?php
    $events = tribe_get_events( array(
        'posts_per_page' => 25000,
        'start_date' => date( 'Y-m-d H:i:s' )
    ) );
 ?>
 <div class="jm-events-header">
    <div class="jm-event-date-picker clearfix">

        <?php
        $days = date("t");
        // var_dump($days); exit;
        $eveny_days = array();
        foreach ($events as $event) {
            $event_days[] = tribe_get_start_date( $event, false, 'd' );       //TODO hämta för endast vald månad
        }

        for ($day=1; $day <= $days; $day++) {
            $class = '';
            if (in_array($day, $event_days)) {
                $class='class="has-event"';
            }
            if ($day == date('d')) {
                $class='class="is-today"';
            }
            echo( '<p '. $class .'>'. $day .'</p>' );
        }

         ?>

    </div>
    <div class="jm-event-filters">
    <div class="jm-event-month-filter">


        <?php
        //get all dates from events
        $dates = array();
        foreach ($events as $event) {
            $dates[]= tribe_get_start_date( $event, false, 'Y-m' );       //2017-09
        }
        //make dates readable & remove duplicate months
        $readable_dates = array();
        foreach ( $dates as $date ) {
            $month = substr($date, 5);
            $year = substr($date, 0, 4);
            $month_name = tribe_wp_locale_month( $month, 'full');
            $readable_dates[$date] = $month_name .' '. $year;
        }
        //remove duplicates
        $months = array_unique($readable_dates);

        foreach ($months as $num => $text) {
            if ($num == date('Y-m')) {
                echo '<p class="month" data-month="' . $num . '">'. $text .'</p><ul>';
            }
            else {
                echo '<li class="month" data-month="' . $num . '"><a href="#">' . $text .'</a></li>';
            }
        }

        ?>
        </ul>
    </div>

    <div class="jm-event-category-filter">
        <p class="jm-all-events" data-category="all">Visa alla kategorier</p>
        <ul>
        <?php
        $terms = get_terms('tribe_events_cat');

        foreach ( $terms as $term ) {
            echo '<li class="cat" data-category="' . $term->name . '"><a href="' . esc_url( $term->link ) . '">' . $term->name . '</a></li>';
        }
        ?>
        </ul>
    </div>
    </div>
</div>

<div id="jm-all-events" class="">
    <?php


        foreach ( $events as $event ) {

            $date_start = tribe_get_start_date( $event );
            $date_end = ' - <span class="tribe-event-time">'. tribe_get_display_end_date( $event ).'</span>';
            $title = $event->post_title;
            $permalink = get_permalink( $event );
            $excerpt = $event->post_content;
            $month = tribe_get_start_date( $event, false, 'Y-m' );
            $full_date = tribe_get_start_date( $event, false, 'Y-m-d' );
            $category = get_the_terms( $event->ID, 'tribe_events_cat');

            echo'
            <div id="'. $full_date .'" class="jm-event" data-date="'. $full_date .'" data-month="'. $month .'"
              data-category="'. $category[0]->name .'">
		          <div class="tribe-event-schedule-details">
			               <span class="tribe-event-date-start">'. $date_start .'</span>'. $date_end.'
                   </div>

                    <h2 class="tribe-events-list-event-title">
                    	<a class="tribe-event-url" href="'. $permalink .'"
                        title="'. $title.'" rel="bookmark">'. $title .'</a>
                    </h2>

                    <div class="tribe-events-list-event-description tribe-events-content description entry-summary">
                        <p>'. $excerpt .'</p>
                        <a href="'. $permalink .'" class="tribe-events-read-more jm-arrowed-link" rel="bookmark">Läs mer om detta</a>
                    </div>

            </div>';
        }

     ?>
</div> <!-- #tribe-events-pg-template -->
<?php
get_footer();
