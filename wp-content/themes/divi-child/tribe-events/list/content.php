<?php
/**
 * List View Content Template
 * The content template for the list view. This template is also used for
 * the response that is returned on list view ajax requests.
 *
 * Override this template in your own theme by creating a file at [your-theme]/tribe-events/list/content.php
 *
 * @package TribeEventsCalendar
 * @version  4.3
 *
 */

if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
} ?>

<div id="tribe-events-content" class="tribe-events-list clearfix">

	<?php
	    $events = tribe_get_events( array(
	        'posts_per_page' => 25000,
			'start_date' => date( 'Y-m-d H:i:s' ),
	    ) );
	 ?>

	<div id="jm-all-events" class="clearfix">
	    <?php


	        foreach ( $events as $event ) {

	            $date_start = tribe_get_start_date( $event );
	            $date_end = '';
	            $title = $event->post_title;
	            $permalink = get_permalink( $event );
				$content =  $event->post_content;
	            $excerpt = wp_trim_words( $content, 20, '...' );
	            $month = tribe_get_start_date( $event, false, 'Y-m' );
	            $full_date = tribe_get_start_date( $event, false, 'Y-m-d' );
	            // $category = get_the_terms( $event->ID, 'tribe_events_cat');
				$class = '';
				$thumb = '';

				if(tribe_get_end_date( $event, true, 'H:i' ) != '23:59') {
					$date_end = ' - <span class="tribe-event-time">'. tribe_get_end_date( $event, true, 'H:i' ).'</span>';
				}
				if ( tribe_is_recurring_event( $event ) ) {
					$recurring = 'Fler tillfällen finns';
				}
				else {
					$recurring = '';
				}

				if(has_post_thumbnail($event->ID)) {
					$class = 'list-event-has-img';
					$thumb = get_the_post_thumbnail( $event->ID, 'medium' );
				}

	            echo'
				<div id="'. $full_date .'" class="jm-event '. $class .'" data-date="'. $full_date .'" data-month="'. $month .'">
					
					<div class="tribe-events-event-image">' . $thumb . '</div>
					<div class="event-text-wrapper">
						<div class="tribe-event-schedule-details">
								<span class="tribe-event-date-start">'. $date_start .'</span>'. $date_end.'
								<span class="event-is-recurring">'. $recurring .'</span>
						</div>

						<h2 class="tribe-events-list-event-title">
							<a class="tribe-event-url" href="'. $permalink .'"
							title="'. $title.'" rel="bookmark">'. $title .'</a>
						</h2>

						<div class="tribe-events-list-event-description tribe-events-content description entry-summary">
							<p>'. $excerpt .'</p>
							<a href="'. $permalink .'" class="tribe-events-read-more jm-arrowed-link" rel="bookmark">Läs mer om detta</a>
						</div>
					</div>
						
	            </div>';
	        }

		?> 
	</div> <!-- #tjm-all-events -->

</div><!-- #tribe-events-content -->
