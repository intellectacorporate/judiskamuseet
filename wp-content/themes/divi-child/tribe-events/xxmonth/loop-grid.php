<?php
/**
 * Month View Grid Loop
 * This file sets up the structure for the month grid loop
 *
 * Override this template in your own theme by creating a file at [your-theme]/tribe-events/month/loop-grid.php
 *
 * @package TribeEventsCalendar
 *
 */

if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
} ?>

<?php

$days_of_week = tribe_events_get_days_of_week();
$week = 0;
global $wp_locale;


?>

<?php do_action( 'tribe_events_before_the_grid' ) ?>
	<div class="tribe-events-calendar clearfix">

			<?php tribe_events_the_previous_month_link(); ?>

			<?php while ( tribe_events_have_month_days() ) : tribe_events_the_month_day(); ?>
			<?php if ( $week != tribe_events_get_current_week() ) : $week ++; ?>

			<?php endif; ?>

			<?php
			// Get data for this day within the loop.
			$daydata = tribe_events_get_current_month_day(); ?>

			<span class="<?php tribe_events_the_month_day_classes() ?>"
				data-day="<?php echo esc_attr( isset( $daydata['daynum'] ) ? $daydata['date'] : '' ); ?>"
				data-tribejson='<?php echo tribe_events_template_data( null, array( 'date_name' => tribe_format_date( $daydata['date'], false ) ) ); ?>'
				>
				<?php tribe_get_template_part( 'month/single', 'day' ) ?>
			</span>
			<?php endwhile; ?>

			<div><?php tribe_events_the_next_month_link(); ?></div>

			<!-- link to months & categories -->
			<ul class="jm-calendar-months">
				<li>oktober</li>
				<li>november</li>
			<?php

			// foreach (  ) {
			// 	echo '<li><a href="' . esc_url( $term_link ) . '">' . $term->name . '</a></li>';
			// }

			?>
			</ul>

			<div class="jm-calendar-categories">
				<p>Visa alla kategorier</p>
			<ul>
			<?php
			$terms = get_terms('tribe_events_cat');

			foreach ( $terms as $term ) {
				echo '<li><a href="' . esc_url( $term_link ) . '">' . $term->name . '</a></li>';
			}
			?>
			</ul>
			</div>


	</div><!-- .tribe-events-calendar -->
<?php
do_action( 'tribe_events_after_the_grid' );
?>

