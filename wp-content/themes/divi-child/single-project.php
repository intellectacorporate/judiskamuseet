<?php

get_header();

$is_page_builder_used = et_pb_is_pagebuilder_used( get_the_ID() );

$show_navigation = get_post_meta( get_the_ID(), '_et_pb_project_nav', true );

?>

<div id="main-content">

<?php if ( ! $is_page_builder_used ) : ?>
<!-- header -->
<?php
$contributor_name = get_field('contributor_name');
if ($contributor_name == '') {
	$contributor_name = 'Anonym';
}
 ?>
<div class="contrib-meta">
	<div class="inner clearfix">
		<div class="jm-user-icon"></div>
		<div class="jm-contrib-meta-user">
			<p>Postad av: <?php echo $contributor_name; ?> </p>
			<p><?php echo get_the_date(); ?></p>
		</div>
		<div class="clearfix jm-contrib-meta-share">
			
			<?php echo do_shortcode( '[jm_share_buttons]' ); ?>
		</div>
	</div>
</div>
	<div class="container">
		<div id="content-area" class="clearfix">
			<!-- <div id="left-area"> -->

<?php endif; ?>

			<?php while ( have_posts() ) : the_post(); ?>

				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

				<?php if ( ! $is_page_builder_used ) : ?>

					<div class="et_main_title">
						<h1 class="entry-title"><?php the_title(); ?></h1>
					</div>

				<?php
					$thumb = '';

					$width = (int) apply_filters( 'et_pb_portfolio_single_image_width', 1080 );
					$height = (int) apply_filters( 'et_pb_portfolio_single_image_height', 9999 );
					$classtext = 'et_featured_image';
					$titletext = get_the_title();
					$thumbnail = get_thumbnail( $width, $height, $classtext, $titletext, $titletext, false, 'Projectimage' );
					$thumb = $thumbnail["thumb"];

					$page_layout = get_post_meta( get_the_ID(), '_et_pb_page_layout', true );

					if ( '' !== $thumb )
						print_thumbnail( $thumb, $thumbnail["use_timthumb"], $titletext, $width, $height );
				?>

				<?php endif; ?>

					<div class="entry-content">
					<?php
						the_content();

						//if we have extra images, display it after content
						$images = get_attached_media( 'image' );

						$featured_image_id = get_post_thumbnail_id();	//string

						foreach ($images as $img) {
							$id = (string)$img->ID;
							if ( $id !== $featured_image_id) {
								echo wp_get_attachment_image($img->ID, array('1080', '9999'));
							}
						}
						if ( ! $is_page_builder_used )
							wp_link_pages( array( 'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'Divi' ), 'after' => '</div>' ) );
					?>
					</div> <!-- .entry-content -->

				<div class="bottom-navigation clearfix">
					<?php previous_post_link('%link', 'Tidigare'); ?>
					<?php next_post_link('%link', 'Nästa'); ?>
				</div>

				</article> <!-- .et_pb_post -->


			<?php endwhile; ?>

<?php if ( ! $is_page_builder_used ) : ?>

		</div> <!-- #content-area -->
	</div> <!-- .container -->

<?php endif; ?>

</div> <!-- #main-content -->

<?php get_footer(); ?>
