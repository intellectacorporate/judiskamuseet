/**
 * filtering events in calendar
 */

(function($) {
$( document ).ready(function() {
    var currentTime = new Date();
    var month = currentTime.getMonth() + 1;
    month = '' + month;
    if (month.charAt( 0 ) != '1') {
        month = '0'+month;
    }
    var year = currentTime.getFullYear();

    datepickerFillUp(year+'-'+month);     //data-month="2017-09"

    $( ".jm-event-category-filter .cat" ).change(function () {
        $( ".jm-all-events:selected" ).each(function(e) {
            // e.preventDefault();
            $('.month').removeClass('selected');
            $('.cat').removeClass('selected');
            $('.month:selected').remove();

            $(this).addClass('selected');
         	$('.jm-event').show();
        });

        $( ".jm-event-category-filter .cat:selected" ).each(function(e) {
            $('.jm-event').hide();
            //e.preventDefault();
            $('.month').removeClass('selected');
            $('.cat').removeClass('selected');
            $('.jm-all-events').removeClass('selected');
            $('.month:selected').remove();

            $(this).addClass('selected');
             $cat = $(this).data('category');
         	$('.jm-event').filter(function(){
              return($(this).data('category')== $cat);
            }).show();
        });
    });

function daysInMonth(iMonth, iYear) {
  return 32 - new Date(iYear, iMonth-1, 32).getDate();
}

function datepickerFillUp(month) {
    // month view filter thing
    $('.jm-event-date-picker').empty();
    $month_num = month.substr(5, 2);
    if ( $month_num.charAt( 0 ) == '0') { $month_num = $month_num.slice(1); }
    $year_num = month.substr(0, 4);

    $days_in_month = daysInMonth($month_num, $year_num);

    $visable_events = $('.jm-event:visible');

    $event_list = [];
    $visable_events.each(function() {
        $data_date = $(this).data('date').substr(8,2);
        if ( $data_date.charAt( 0 ) == '0') { $data_date = $data_date.slice(1); }

        $event_list[$data_date] = $(this).data('date');

    });

    for (var i = 1; i <= $days_in_month; i++) {

        if( i in $event_list) {

            $('.jm-event-date-picker').append('<a class="has-event" href="#'+ $event_list[i] +'">'+i+'</a>');

        }
        else {
            $('.jm-event-date-picker').append('<p>'+i+'</p>');

         }


    }
}

$( ".jm-event-month-filter .month" ).change(function () {
    $( ".jm-event-month-filter .month:selected" ).each(function(e) {
        $('.jm-event').hide();
        // e.preventDefault();

         $month = $(this).data('month');        //data-month="2017-09"
         $('.month').removeClass('selected');
         $('.cat').removeClass('selected');
         $('.jm-all-events').removeClass('selected');
         $('.cat:selected').remove();

         $(this).addClass('selected');
     	$('.jm-event').filter(function(){
            $results = ($(this).data('month')== $month);

            return $results;
        }).show();

        datepickerFillUp($month);

    });
});



});
})(jQuery);
