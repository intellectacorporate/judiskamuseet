<?php
if ( ( is_single() || is_page() ) && 'et_full_width_page' === get_post_meta( get_queried_object_id(), '_et_pb_page_layout', true ) )
	return;

if ( is_active_sidebar( 'et_pb_widget_area_5' ) || is_active_sidebar( 'et_pb_widget_area_7' ) ) : ?>
	<div id="sidebar" class="sidebar-press">
		<?php dynamic_sidebar( 'et_pb_widget_area_5' ); ?>
		<?php dynamic_sidebar( 'et_pb_widget_area_7' ); ?>
	</div> <!-- end #sidebar -->
<?php endif; ?>
