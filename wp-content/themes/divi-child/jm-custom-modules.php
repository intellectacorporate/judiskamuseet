<?php
function ex_divi_child_theme_setup() {

  if ( class_exists('ET_Builder_Module')) {


  	class DS_Custom_Module_Blog extends ET_Builder_Module {
  		function init() {
  			$this->name       = esc_html__( 'Post list', 'et_builder' );
  			$this->slug       = 'et_pb_blog_2';
  			$this->fb_support = true;

  			$this->whitelisted_fields = array(
  				'fullwidth',
  				'posts_number',
  				'include_categories',
  	            'include_post_types',
  				'meta_date',
  				'show_thumbnail',
  				'show_content',
  				'show_more',
  				'show_author',
  				'show_date',
  				'show_categories',
  	            'show_post_types',
  				'show_comments',
  				'show_pagination',
  				'offset_number',
  				'background_layout',
  				'admin_label',
  				'module_id',
                'full_height_img',
  				'module_class',
  				'masonry_tile_background_color',
  				'use_dropshadow',
  				'use_overlay',
  				'overlay_icon_color',
  				'hover_overlay_color',
  				'hover_icon',
  			);

  			$this->fields_defaults = array(
  				'fullwidth'         => array( 'on' ),
  				'posts_number'      => array( 10, 'add_default_setting' ),
  				'meta_date'         => array( 'M j, Y', 'add_default_setting' ),
  				'show_thumbnail'    => array( 'on' ),
  				'show_content'      => array( 'off' ),
  				'show_more'         => array( 'off' ),
  				'show_author'       => array( 'on' ),
  				'show_date'         => array( 'on' ),
  				'show_categories'   => array( 'on' ),
  	            'show_post_types'   => array( 'on' ),
  				'show_comments'     => array( 'off' ),
  				'show_pagination'   => array( 'on' ),
  				'offset_number'     => array( 0, 'only_default_setting' ),
  				'background_layout' => array( 'light' ),
  				'use_dropshadow'    => array( 'off' ),
  				'use_overlay'       => array( 'off' ),
  			);

  			$this->main_css_element = '%%order_class%% .et_pb_post';
  			$this->advanced_options = array(
  				'fonts' => array(
  					'header' => array(
  						'label'    => esc_html__( 'Header', 'et_builder' ),
  						'css'      => array(
  							'main' => "{$this->main_css_element} .entry-title",
  							'plugin_main' => "{$this->main_css_element} .entry-title, {$this->main_css_element} .entry-title a",
  							'important' => 'all',
  						),
  					),
  					'meta' => array(
  						'label'    => esc_html__( 'Meta', 'et_builder' ),
  						'css'      => array(
  							'main'        => "{$this->main_css_element} .post-meta, {$this->main_css_element} .post-meta a",
  							'plugin_main' => "{$this->main_css_element} .post-meta, {$this->main_css_element} .post-meta a, {$this->main_css_element} .post-meta span",
  						),
  					),
  					'body'   => array(
  						'label'    => esc_html__( 'Body', 'et_builder' ),
  						'css'      => array(
  							'main'        => "{$this->main_css_element}, %%order_class%%.et_pb_bg_layout_light .et_pb_post .post-content p, %%order_class%%.et_pb_bg_layout_dark .et_pb_post .post-content p",
  							'color'       => "{$this->main_css_element}, {$this->main_css_element} .post-content *",
  							'line_height' => "{$this->main_css_element} p",
  							'plugin_main' => "{$this->main_css_element}, %%order_class%%.et_pb_bg_layout_light .et_pb_post .post-content p, %%order_class%%.et_pb_bg_layout_dark .et_pb_post .post-content p, %%order_class%%.et_pb_bg_layout_light .et_pb_post a.more-link, %%order_class%%.et_pb_bg_layout_dark .et_pb_post a.more-link",
  						),
  					),
  				),
  				'border' => array(
  					'css'      => array(
  						'main' => "%%order_class%%.et_pb_module .et_pb_post",
  						'important' => 'plugin_only',
  					),
  				),
  			);
  			$this->custom_css_options = array(
  				'title' => array(
  					'label'    => esc_html__( 'Title', 'et_builder' ),
  					'selector' => '.et_pb_post .entry-title',
  				),
  				'post_meta' => array(
  					'label'    => esc_html__( 'Post Meta', 'et_builder' ),
  					'selector' => '.et_pb_post .post-meta',
  				),
  				'pagenavi' => array(
  					'label'    => esc_html__( 'Pagenavi', 'et_builder' ),
  					'selector' => '.wp_pagenavi',
  				),
  				'featured_image' => array(
  					'label'    => esc_html__( 'Featured Image', 'et_builder' ),
  					'selector' => '.et_pb_image_container',
  				),
  				'read_more' => array(
  					'label'    => esc_html__( 'Read More Button', 'et_builder' ),
  					'selector' => '.et_pb_post .more-link',
  				),
  			);
  		}

  		function get_fields() {
  			$fields = array(
  				'fullwidth' => array(
  					'label'             => esc_html__( 'Layout', 'et_builder' ),
  					'type'              => 'select',
  					'option_category'   => 'layout',
  	                'option_post_type'   => 'layout',
  					'options'           => array(
  						'on'  => esc_html__( 'Fullwidth', 'et_builder' ),
  						'off' => esc_html__( 'Grid', 'et_builder' ),
  					),
  					'affects'           => array(
  						'background_layout',
  						'use_dropshadow',
  						'masonry_tile_background_color',
  					),
  					'description'        => esc_html__( 'Toggle between the various blog layout types.', 'et_builder' ),
  					'computed_affects'   => array(
  						'__posts',
  					),
  				),
  				'posts_number' => array(
  					'label'             => esc_html__( 'Posts Number', 'et_builder' ),
  					'type'              => 'text',
  					'option_category'   => 'configuration',
  					'description'       => esc_html__( 'Choose how much posts you would like to display per page.', 'et_builder' ),
  					'computed_affects'   => array(
  						'__posts',
  					),
  				),
  				'include_categories' => array(
  					'label'            => esc_html__( 'Include Categories', 'et_builder' ),
  					'renderer'         => 'et_builder_include_categories_option',
  					'option_category'  => 'basic_option',
  					'renderer_options' => array(
  						'use_terms' => false,
  					),
  					'description'      => esc_html__( 'Choose which categories you would like to include in the feed.', 'et_builder' ),
  					'computed_affects'   => array(
  						'__posts',
  					),
  				),


			  'include_post_types' => array(
					'label'           => esc_html__( 'Include Post types', 'et_builder' ),
					//'type'            => 'multiple_checkboxes',
					  //'type'            => 'checkbox',
					'type'              => 'select',		//Detta funkar m 1 st
					//'checkbox
					'options'           => array(
					    'post'  			=> esc_html__( 'Endast Inlägg', 'et_builder' ),
						'page'     	  		=> esc_html__( 'Endast Sidor', 'et_builder' ),
                        'press'     	  		=> esc_html__( 'Endast Press', 'et_builder' )

					),
					//'additional_att'  => 'disable_on',
					//'additional_att'  => 'include_post_types',
					//'option_category' => 'basic_option',
					'description'     => esc_html__( 'Välj vilka post types', 'et_builder' ),
				),

                'full_height_img' => array(
                     'label'             => esc_html__( 'Bakgrundsbild med fullhöjd', 'et_builder' ),
                     'type'              => 'select',
                     'options'           => array(
                        'no'  => esc_html__( 'nej', 'et_builder' ),
                        'yes'  => esc_html__( 'ja', 'et_builder' ),
                     ),
                  ),



  				'meta_date' => array(
  					'label'             => esc_html__( 'Meta Date Format', 'et_builder' ),
  					'type'              => 'text',
  					'option_category'   => 'configuration',
  					'description'       => esc_html__( 'If you would like to adjust the date format, input the appropriate PHP date format here.', 'et_builder' ),
  					'computed_affects'   => array(
  						'__posts',
  					),
  				),
  				'show_thumbnail' => array(
  					'label'             => esc_html__( 'Show Featured Image', 'et_builder' ),
  					'type'              => 'yes_no_button',
  					'option_category'   => 'configuration',
  					'options'           => array(
  						'on'  => esc_html__( 'Yes', 'et_builder' ),
  						'off' => esc_html__( 'No', 'et_builder' ),
  					),
  					'description'        => esc_html__( 'This will turn thumbnails on and off.', 'et_builder' ),
  					'computed_affects'   => array(
  						'__posts',
  					),
  				),
  				'show_content' => array(
  					'label'             => esc_html__( 'Content', 'et_builder' ),
  					'type'              => 'select',
  					'option_category'   => 'configuration',
  					'options'           => array(
  						'off' => esc_html__( 'Show Excerpt', 'et_builder' ),
  						'on'  => esc_html__( 'Show Content', 'et_builder' ),
  					),
  					'affects'           => array(
  						'show_more',
  					),
  					'description'        => esc_html__( 'Showing the full content will not truncate your posts on the index page. Showing the excerpt will only display your excerpt text.', 'et_builder' ),
  					'computed_affects'   => array(
  						'__posts',
  					),
  				),
  				'show_more' => array(
  					'label'             => esc_html__( 'Read More Button', 'et_builder' ),
  					'type'              => 'yes_no_button',
  					'option_category'   => 'configuration',
  					'options'           => array(
  						'off' => esc_html__( 'Off', 'et_builder' ),
  						'on'  => esc_html__( 'On', 'et_builder' ),
  					),
  					'depends_show_if'   => 'off',
  					'description'       => esc_html__( 'Here you can define whether to show "read more" link after the excerpts or not.', 'et_builder' ),
  					'computed_affects'   => array(
  						'__posts',
  					),
  				),
  				'show_author' => array(
  					'label'             => esc_html__( 'Show Author', 'et_builder' ),
  					'type'              => 'yes_no_button',
  					'option_category'   => 'configuration',
  					'options'           => array(
  						'on'  => esc_html__( 'Yes', 'et_builder' ),
  						'off' => esc_html__( 'No', 'et_builder' ),
  					),
  					'description'        => esc_html__( 'Turn on or off the author link.', 'et_builder' ),
  					'computed_affects'   => array(
  						'__posts',
  					),
  				),
  				'show_date' => array(
  					'label'             => esc_html__( 'Show Date', 'et_builder' ),
  					'type'              => 'yes_no_button',
  					'option_category'   => 'configuration',
  					'options'           => array(
  						'on'  => esc_html__( 'Yes', 'et_builder' ),
  						'off' => esc_html__( 'No', 'et_builder' ),
  					),
  					'description'        => esc_html__( 'Turn the date on or off.', 'et_builder' ),
  					'computed_affects'   => array(
  						'__posts',
  					),
  				),
  				'show_categories' => array(
  					'label'             => esc_html__( 'Show Categories', 'et_builder' ),
  					'type'              => 'yes_no_button',
  					'option_category'   => 'configuration',
  					'options'           => array(
  						'on'  => esc_html__( 'Yes', 'et_builder' ),
  						'off' => esc_html__( 'No', 'et_builder' ),
  					),
  					'description'        => esc_html__( 'Turn the category links on or off.', 'et_builder' ),
  					'computed_affects'   => array(
  						'__posts',
  					),
  				),
  	            'show_post_types' => array(
  					'label'             => esc_html__( 'Show Post types', 'et_builder' ),
  					'type'              => 'yes_no_button',
  					'option_category'   => 'configuration',
  					'options'           => array(
  						'on'  => esc_html__( 'Yes', 'et_builder' ),
  						'off' => esc_html__( 'No', 'et_builder' ),
  					),
  					'description'        => esc_html__( 'Turn the post type links on or off.', 'et_builder' ),
  					'computed_affects'   => array(
  						'__posts',
  					),
  				),
  				'show_comments' => array(
  					'label'             => esc_html__( 'Show Comment Count', 'et_builder' ),
  					'type'              => 'yes_no_button',
  					'option_category'   => 'configuration',
  					'options'           => array(
  						'on'  => esc_html__( 'Yes', 'et_builder' ),
  						'off' => esc_html__( 'No', 'et_builder' ),
  					),
  					'description'        => esc_html__( 'Turn comment count on and off.', 'et_builder' ),
  					'computed_affects'   => array(
  						'__posts',
  					),
  				),
  				'show_pagination' => array(
  					'label'             => esc_html__( 'Show Pagination', 'et_builder' ),
  					'type'              => 'yes_no_button',
  					'option_category'   => 'configuration',
  					'options'           => array(
  						'on'  => esc_html__( 'Yes', 'et_builder' ),
  						'off' => esc_html__( 'No', 'et_builder' ),
  					),
  					'description'        => esc_html__( 'Turn pagination on and off.', 'et_builder' ),
  					'computed_affects'   => array(
  						'__posts',
  					),
  				),
  				'offset_number' => array(
  					'label'           => esc_html__( 'Offset Number', 'et_builder' ),
  					'type'            => 'text',
  					'option_category' => 'configuration',
  					'description'     => esc_html__( 'Choose how many posts you would like to offset by', 'et_builder' ),
  					'computed_affects'   => array(
  						'__posts',
  					),
  				),
  				'use_overlay' => array(
  					'label'             => esc_html__( 'Featured Image Overlay', 'et_builder' ),
  					'type'              => 'yes_no_button',
  					'option_category'   => 'layout',
  					'options'           => array(
  						'off' => esc_html__( 'Off', 'et_builder' ),
  						'on'  => esc_html__( 'On', 'et_builder' ),
  					),
  					'affects'           => array(
  						'overlay_icon_color',
  						'hover_overlay_color',
  						'hover_icon',
  					),
  					'description'       => esc_html__( 'If enabled, an overlay color and icon will be displayed when a visitors hovers over the featured image of a post.', 'et_builder' ),
  					'computed_affects'   => array(
  						'__posts',
  					),
  				),
  				'overlay_icon_color' => array(
  					'label'             => esc_html__( 'Overlay Icon Color', 'et_builder' ),
  					'type'              => 'color',
  					'custom_color'      => true,
  					'depends_show_if'   => 'on',
  					'description'       => esc_html__( 'Here you can define a custom color for the overlay icon', 'et_builder' ),
  				),
  				'hover_overlay_color' => array(
  					'label'             => esc_html__( 'Hover Overlay Color', 'et_builder' ),
  					'type'              => 'color-alpha',
  					'custom_color'      => true,
  					'depends_show_if'   => 'on',
  					'description'       => esc_html__( 'Here you can define a custom color for the overlay', 'et_builder' ),
  				),
  				'hover_icon' => array(
  					'label'               => esc_html__( 'Hover Icon Picker', 'et_builder' ),
  					'type'                => 'text',
  					'option_category'     => 'configuration',
  					'class'               => array( 'et-pb-font-icon' ),
  					'renderer'            => 'et_pb_get_font_icon_list',
  					'renderer_with_field' => true,
  					'depends_show_if'     => 'on',
  					'description'         => esc_html__( 'Here you can define a custom icon for the overlay', 'et_builder' ),
  					'computed_affects'   => array(
  						'__posts',
  					),
  				),
  				'background_layout' => array(
  					'label'       => esc_html__( 'Text Color', 'et_builder' ),
  					'type'        => 'select',
  					'option_category' => 'color_option',
  					'options'           => array(
  						'light' => esc_html__( 'Dark', 'et_builder' ),
  						'dark'  => esc_html__( 'Light', 'et_builder' ),
  					),
  					'depends_default' => true,
  					'description' => esc_html__( 'Here you can choose whether your text should be light or dark. If you are working with a dark background, then your text should be light. If your background is light, then your text should be set to dark.', 'et_builder' ),
  				),
  				'masonry_tile_background_color' => array(
  					'label'             => esc_html__( 'Grid Tile Background Color', 'et_builder' ),
  					'type'              => 'color-alpha',
  					'custom_color'      => true,
  					'tab_slug'          => 'advanced',
  					'depends_show_if'   => 'off',
  					'depends_to'        => array(
  						'fullwidth'
  					),
  				),
  				'use_dropshadow' => array(
  					'label'             => esc_html__( 'Use Dropshadow', 'et_builder' ),
  					'type'              => 'yes_no_button',
  					'option_category'   => 'layout',
  					'options'           => array(
  						'off' => esc_html__( 'Off', 'et_builder' ),
  						'on'  => esc_html__( 'On', 'et_builder' ),
  					),
  					'tab_slug'          => 'advanced',
  					'depends_show_if'   => 'off',
  					'depends_to'        => array(
  						'fullwidth'
  					),
  				),
  				'disabled_on' => array(
  					'label'           => esc_html__( 'Disable on', 'et_builder' ),
  					'type'            => 'multiple_checkboxes',
  					'options'         => array(
  						'phone'   => esc_html__( 'Phone', 'et_builder' ),
  						'tablet'  => esc_html__( 'Tablet', 'et_builder' ),
  						'desktop' => esc_html__( 'Desktop', 'et_builder' ),
  					),
  					'additional_att'  => 'disable_on',
  					'option_category' => 'configuration',
  					'description'     => esc_html__( 'This will disable the module on selected devices', 'et_builder' ),
  				),
  				'admin_label' => array(
  					'label'       => esc_html__( 'Admin Label', 'et_builder' ),
  					'type'        => 'text',
  					'description' => esc_html__( 'This will change the label of the module in the builder for easy identification.', 'et_builder' ),
  				),
  				'module_id' => array(
  					'label'           => esc_html__( 'CSS ID', 'et_builder' ),
  					'type'            => 'text',
  					'option_category' => 'configuration',
  					'tab_slug'        => 'custom_css',
  					'option_class'    => 'et_pb_custom_css_regular',
  				),
  				'module_class' => array(
  					'label'           => esc_html__( 'CSS Class', 'et_builder' ),
  					'type'            => 'text',
  					'option_category' => 'configuration',
  					'tab_slug'        => 'custom_css',
  					'option_class'    => 'et_pb_custom_css_regular',
  				),
  				'__posts' => array(
  					'type' => 'computed',
  					'computed_callback' => array( 'ET_Builder_Module_Blog', 'get_blog_posts' ),
  					'computed_depends_on' => array(
  						'fullwidth',
  						'posts_number',
  						'include_categories',
  	                    'include_post_types',
  						'meta_date',
  						'show_thumbnail',
  						'show_content',
  						'show_more',
  						'show_author',
  						'show_date',
  						'show_categories',
  	                    'show_post_types',
  						'show_comments',
  						'show_pagination',
  						'offset_number',
  						'use_overlay',
  						'hover_icon',
  					),
  				),
  			);
  			return $fields;
  		}

  		/**
  		 * Get blog posts for blog module
  		 *
  		 * @param array   arguments that is being used by et_pb_blog
  		 * @return string blog post markup
  		 */
  		static function get_blog_posts( $args = array(), $conditional_tags = array(), $current_page = array() ) {
  			global $paged, $post, $wp_query, $et_fb_processing_shortcode_object, $et_pb_rendering_column_content;

  			$global_processing_original_value = $et_fb_processing_shortcode_object;

  			// Default params are combination of attributes that is used by et_pb_blog and
  			// conditional tags that need to be simulated (due to AJAX nature) by passing args
  			$defaults = array(
  				'fullwidth'                     => '',
  				'posts_number'                  => '',
  				'include_categories'            => '',
  	            'include_post_types'            => '',
  				'meta_date'                     => '',
  				'show_thumbnail'                => '',
  				'show_content'                  => '',
  				'show_author'                   => '',
  				'show_date'                     => '',
  				'show_categories'               => '',
  	            'show_post_types'               => '',
  				'show_comments'                 => '',
  				'show_pagination'               => '',
  				'background_layout'             => '',
  				'show_more'                     => '',
  				'offset_number'                 => '',
  				'masonry_tile_background_color' => '',
  				'use_dropshadow'                => '',
  				'overlay_icon_color'            => '',
  				'hover_overlay_color'           => '',
  				'hover_icon'                    => '',
  				'use_overlay'                   => '',
                'full_height_img'               => '',
  			);

  			// WordPress' native conditional tag is only available during page load. It'll fail during component update because
  			// et_pb_process_computed_property() is loaded in admin-ajax.php. Thus, use WordPress' conditional tags on page load and
  			// rely to passed $conditional_tags for AJAX call
  			$is_front_page               = et_fb_conditional_tag( 'is_front_page', $conditional_tags );
  			$is_search                   = et_fb_conditional_tag( 'is_search', $conditional_tags );
  			$is_single                   = et_fb_conditional_tag( 'is_single', $conditional_tags );
  			$et_is_builder_plugin_active = et_fb_conditional_tag( 'et_is_builder_plugin_active', $conditional_tags );

  			$container_is_closed = false;

  			// remove all filters from WP audio shortcode to make sure current theme doesn't add any elements into audio module
  			remove_all_filters( 'wp_audio_shortcode_library' );
  			remove_all_filters( 'wp_audio_shortcode' );
  			remove_all_filters( 'wp_audio_shortcode_class');

  			$args = wp_parse_args( $args, $defaults );

  			$overlay_output = '';
  			$hover_icon = '';

  			if ( 'on' === $args['use_overlay'] ) {
  				$data_icon = '' !== $args['hover_icon']
  					? sprintf(
  						' data-icon="%1$s"',
  						esc_attr( et_pb_process_font_icon( $args['hover_icon'] ) )
  					)
  					: '';

  				$overlay_output = sprintf(
  					'<span class="et_overlay%1$s"%2$s></span>',
  					( '' !== $args['hover_icon'] ? ' et_pb_inline_icon' : '' ),
  					$data_icon
  				);
  			}

  			$overlay_class = 'on' === $args['use_overlay'] ? ' et_pb_has_overlay' : '';

  			$query_args = array(
  				'posts_per_page' => intval( $args['posts_number'] ),
  				'post_status'    => 'publish',
  			);

  			if ( defined( 'DOING_AJAX' ) && isset( $current_page[ 'paged'] ) ) {
  				$paged = intval( $current_page[ 'paged' ] );
  			} else {
  				$paged = $is_front_page ? get_query_var( 'page' ) : get_query_var( 'paged' );
  			}

  			if ( '' !== $args['include_categories'] ) {
  				$query_args['cat'] = $args['include_categories'];
  			}
  	        if ( '' !== $args['include_post_types'] ) {
  				$query_args['post_type'] = $args['include_post_types'];
  			}

  			if ( ! $is_search ) {
  				$query_args['paged'] = $paged;
  			}

  			if ( '' !== $args['offset_number'] && ! empty( $args['offset_number'] ) ) {
  				/**
  				 * Offset + pagination don't play well. Manual offset calculation required
  				 * @see: https://codex.wordpress.org/Making_Custom_Queries_using_Offset_and_Pagination
  				 */
  				if ( $paged > 1 ) {
  					$query_args['offset'] = ( ( $paged - 1 ) * intval( $args['posts_number'] ) ) + intval( $args['offset_number'] );
  				} else {
  					$query_args['offset'] = intval( $args['offset_number'] );
  				}
  			}

  			if ( $is_single ) {
  				$query_args['post__not_in'][] = get_the_ID();
  			}

  			// Get query
  			$query = new WP_Query( $query_args );

  			// Keep page's $wp_query global
  			$wp_query_page = $wp_query;

  			// Turn page's $wp_query into this module's query
  			$wp_query = $query;

  			ob_start();

  			if ( $query->have_posts() ) {
                if ( 'on' !== $args['fullwidth'] ) {
				                echo '<div class="et_pb_salvattore_content" data-columns>';
		         }

  				while( $query->have_posts() ) {
  					$query->the_post();
  					global $et_fb_processing_shortcode_object;

  					$global_processing_original_value = $et_fb_processing_shortcode_object;

  					// reset the fb processing flag
  					$et_fb_processing_shortcode_object = false;

  					$thumb          = '';
  					$width          = 'on' === $args['fullwidth'] ? 620 : 400;
  					$width          = (int) apply_filters( 'et_pb_blog_image_width', $width );
  					$height         = 'on' === $args['fullwidth'] ? 413 : 267;
  					$height         = (int) apply_filters( 'et_pb_blog_image_height', $height );
  					$classtext      = 'on' === $args['fullwidth'] ? 'et_pb_post_main_image' : '';
  					$titletext      = get_the_title();
  					$thumbnail      = get_thumbnail( $width, $height, $classtext, $titletext, $titletext, false, 'Blogimage' );
  					$thumb          = $thumbnail["thumb"];
  					$no_thumb_class = '' === $thumb || 'off' === $args['show_thumbnail'] ? ' et_pb_no_thumb' : '';

  					$post_format = et_pb_post_format();
  					if ( in_array( $post_format, array( 'video', 'gallery' ) ) ) {
  						$no_thumb_class = '';
  					}

  					// Print output
  					?>
  						<article id="" <?php post_class( 'et_pb_post clearfix' . $no_thumb_class . $overlay_class ) ?>>
  							<?php
  								et_divi_post_format_content();

  								if ( ! in_array( $post_format, array( 'link', 'audio', 'quote' ) ) ) {
  									if ( 'video' === $post_format && false !== ( $first_video = et_get_first_video() ) ) :
  										$video_overlay = has_post_thumbnail() ? sprintf(
  											'<div class="et_pb_video_overlay" style="background-image: url(%1$s); background-size: cover;">
  												<div class="et_pb_video_overlay_hover">
  													<a href="#" class="et_pb_video_play"></a>
  												</div>
  											</div>',
  											$thumb
  										) : '';

  										printf(
  											'<div class="et_main_video_container">
  												%1$s
  												%2$s
  											</div>',
  											$video_overlay,
  											$first_video
  										);
  									elseif ( 'gallery' === $post_format ) :
  										et_pb_gallery_images( 'slider' );
  									elseif ( '' !== $thumb && 'on' === $args['show_thumbnail'] ) :
  										if ( 'on' !== $args['fullwidth'] ) echo '<div class="et_pb_image_container">'; ?>
  											<div class="ev-category"> <?php get_the_category_list(', '); ?></div>
  											<a href="<?php esc_url( the_permalink() ); ?>" class="entry-featured-image-url">
  												<?php print_thumbnail( $thumb, $thumbnail["use_timthumb"], $titletext, $width, $height ); ?>
  												<?php if ( 'on' === $args['use_overlay'] ) {
  													echo $overlay_output;
  												} ?>
  											</a>
  									<?php
  										if ( 'on' !== $args['fullwidth'] ) echo '</div> <!-- .et_pb_image_container -->';
  									endif;
  								}
  							?>

  							<?php if ( 'off' === $args['fullwidth'] || ! in_array( $post_format, array( 'link', 'audio', 'quote' ) ) ) { ?>
  								<?php if ( ! in_array( $post_format, array( 'link', 'audio' ) ) ) { ?>
  									<h2 class="entry-title"><a href="<?php esc_url( the_permalink() ); ?>"><?php the_title(); ?></a></h2>
  								<?php } ?>

  								<?php
  									if ( 'on' === $args['show_author'] || 'on' === $args['show_date'] ) {
  										printf( '<p class="post-meta">%1$s %2$s %3$s %4$s %5$s %6$s</p>',
  											(
  												'on' === $args['show_author']
  													? et_get_safe_localization( sprintf( __( 'by %s', 'et_builder' ), '<span class="author vcard">' .  et_pb_get_the_author_posts_link() . '</span>' ) )
  													: ''
  											),
  											(
  												( 'on' === $args['show_author'] && 'on' === $args['show_date'] )
  													? ' | '
  													: ''
  											),
  											(
  												'on' === $args['show_date']
  													? et_get_safe_localization( sprintf( __( '%s', 'et_builder' ), '<span class="published">' . esc_html( get_the_date( $args['meta_date'] ) ) . '</span>' ) )
  													: ''
  											),
  											(
  												(( 'on' === $args['show_author'] || 'on' === $args['show_date'] ) && 'on' === $args['show_categories'] )
  													? ' | '
  													: ''
  											),
  											// (
  											// 	'on' === $args['show_categories']
  											// 		? get_the_category_list(', ')
  											// 		: ''
  											// ),
  	                                        (
  												'on' === $args['show_post_types']
  													? get_post_types()
  													: ''
  											),
  											(
  												(( 'on' === $args['show_author'] || 'on' === $args['show_date'] || 'on' === $args['show_categories'] ) && 'on' === $args['show_comments'])
  													? ' | '
  													: ''
  											),
  											(
  												'on' === $args['show_comments']
  													? sprintf( esc_html( _nx( '1 Comment', '%s Comments', get_comments_number(), 'number of comments', 'et_builder' ) ), number_format_i18n( get_comments_number() ) )
  													: ''
  											)
  										);
  									}

  									$post_content = et_strip_shortcodes( et_delete_post_first_video( get_the_content() ), true );

  									// reset the fb processing flag
  									$et_fb_processing_shortcode_object = false;
  									// set the flag to indicate that we're processing internal content
  									$et_pb_rendering_column_content = true;
  									// reset all the attributes required to properly generate the internal styles
  									ET_Builder_Element::clean_internal_modules_styles();

  									echo '<div class="post-content">';



  									if ( 'on' === $args['show_content'] ) {
  										global $more;

  										// page builder doesn't support more tag, so display the_content() in case of post made with page builder
  										if ( et_pb_is_pagebuilder_used( get_the_ID() ) ) {
  											$more = 1;

  											echo apply_filters( 'the_content', $post_content );

  										} else {
  											$more = null;
  											echo apply_filters( 'the_content', et_delete_post_first_video( get_the_content( esc_html__( 'read more...', 'et_builder' ) ) ) );
  										}
  									} else {
  										if ( has_excerpt() ) {
  											the_excerpt();
  										} else {
  											if ( '' !== $post_content ) {
  												// set the $et_fb_processing_shortcode_object to false, to retrieve the content inside truncate_post() correctly
  												$et_fb_processing_shortcode_object = false;
  												echo wpautop( et_delete_post_first_video( strip_shortcodes( truncate_post( 125, false, '', true ) ) ) );
  												// reset the $et_fb_processing_shortcode_object to its original value
  												$et_fb_processing_shortcode_object = $global_processing_original_value;
  											} else {
  												echo '';
  											}
  										}
  									}

  									$et_fb_processing_shortcode_object = $global_processing_original_value;
  									// retrieve the styles for the modules inside Blog content
  									$internal_style = ET_Builder_Element::get_style( true );
  									// reset all the attributes after we retrieved styles
  									ET_Builder_Element::clean_internal_modules_styles( false );
  									$et_pb_rendering_column_content = false;
  									// append styles to the blog content
  									if ( $internal_style ) {
  										printf(
  											'<style type="text/css" class="et_fb_blog_inner_content_styles">
  												%1$s
  											</style>',
  											$internal_style
  										);
  									}

  									echo '</div>';

  									if ( 'on' !== $args['show_content'] ) {
  										$more = 'on' == $args['show_more'] ? sprintf( ' <a href="%1$s" class="more-link" >%2$s</a>' , esc_url( get_permalink() ), esc_html__( 'Läs mer', 'et_builder' ) )  : '';
  										echo $more;
  									}
  									?>
  							<?php } // 'off' === $fullwidth || ! in_array( $post_format, array( 'link', 'audio', 'quote', 'gallery' ?>
  						</article>
  					<?php

  					$et_fb_processing_shortcode_object = $global_processing_original_value;
  				} // endwhile

                if ( 'on' !== $args['fullwidth'] ) {
				                echo '</div>';
			    }

  				if ( 'on' === $args['show_pagination'] && ! $is_search ) {
  					// echo '</div> <!-- .et_pb_posts -->'; // @todo this causes closing tag issue

  					$container_is_closed = true;

  					if ( function_exists( 'wp_pagenavi' ) ) {
  						wp_pagenavi( array(
  							'query' => $query
  						) );
  					} else {
  						if ( $et_is_builder_plugin_active ) {
  							include( ET_BUILDER_PLUGIN_DIR . 'includes/navigation.php' );
  						} else {
  							get_template_part( 'includes/navigation', 'index' );
  						}
  					}
  				}

  				wp_reset_query();
  			} else {
  				if ( $et_is_builder_plugin_active ) {
  					include( ET_BUILDER_PLUGIN_DIR . 'includes/no-results.php' );
  				} else {
  					get_template_part( 'includes/no-results', 'index' );
  				}
  			}

  			wp_reset_postdata();

  			// Reset $wp_query to its origin
  			$wp_query = $wp_query_page;

  			$posts = ob_get_contents();

  			ob_end_clean();

  			return $posts;
  		}

  		function shortcode_callback( $atts, $content = null, $function_name ) {
  			/**
  			 * Cached $wp_filter so it can be restored at the end of the callback.
  			 * This is needed because this callback uses the_content filter / calls a function
  			 * which uses the_content filter. WordPress doesn't support nested filter
  			 */
  			global $wp_filter;
  			$wp_filter_cache = $wp_filter;

  			$module_id           = $this->shortcode_atts['module_id'];
  			$module_class        = $this->shortcode_atts['module_class'];
  			$fullwidth           = $this->shortcode_atts['fullwidth'];
  			$posts_number        = $this->shortcode_atts['posts_number'];
  			$include_categories  = $this->shortcode_atts['include_categories'];
  	        $include_post_types  = $this->shortcode_atts['include_post_types'];
  			$meta_date           = $this->shortcode_atts['meta_date'];
  			$show_thumbnail      = $this->shortcode_atts['show_thumbnail'];
  			$show_content        = $this->shortcode_atts['show_content'];
  			$show_author         = $this->shortcode_atts['show_author'];
  			$show_date           = $this->shortcode_atts['show_date'];
  			$show_categories     = $this->shortcode_atts['show_categories'];
  	        $show_post_types     = $this->shortcode_atts['show_post_types'];
  			$show_comments       = $this->shortcode_atts['show_comments'];
  			$show_pagination     = $this->shortcode_atts['show_pagination'];
  			$background_layout   = $this->shortcode_atts['background_layout'];
  			$show_more           = $this->shortcode_atts['show_more'];
  			$offset_number       = $this->shortcode_atts['offset_number'];
  			$masonry_tile_background_color = $this->shortcode_atts['masonry_tile_background_color'];
  			$use_dropshadow      = $this->shortcode_atts['use_dropshadow'];
  			$overlay_icon_color  = $this->shortcode_atts['overlay_icon_color'];
  			$hover_overlay_color = $this->shortcode_atts['hover_overlay_color'];
  			$hover_icon          = $this->shortcode_atts['hover_icon'];
  			$use_overlay         = $this->shortcode_atts['use_overlay'];
            $full_height_img     = $this->shortcode_atts['full_height_img'];

  			// var_dump($include_categories);
  			// exit;

  			global $paged;

  			$module_class = ET_Builder_Element::add_module_order_class( $module_class, $function_name );

  			$container_is_closed = false;

  			// remove all filters from WP audio shortcode to make sure current theme doesn't add any elements into audio module
  			remove_all_filters( 'wp_audio_shortcode_library' );
  			remove_all_filters( 'wp_audio_shortcode' );
  			remove_all_filters( 'wp_audio_shortcode_class');

  			if ( '' !== $masonry_tile_background_color ) {
  				ET_Builder_Element::set_style( $function_name, array(
  					'selector'    => '%%order_class%%.et_pb_blog_grid .et_pb_post',
  					'declaration' => sprintf(
  						'background-color: %1$s;',
  						esc_html( $masonry_tile_background_color )
  					),
  				) );
  			}

  			if ( '' !== $overlay_icon_color ) {
  				ET_Builder_Element::set_style( $function_name, array(
  					'selector'    => '%%order_class%% .et_overlay:before',
  					'declaration' => sprintf(
  						'color: %1$s !important;',
  						esc_html( $overlay_icon_color )
  					),
  				) );
  			}

  			if ( '' !== $hover_overlay_color ) {
  				ET_Builder_Element::set_style( $function_name, array(
  					'selector'    => '%%order_class%% .et_overlay',
  					'declaration' => sprintf(
  						'background-color: %1$s;',
  						esc_html( $hover_overlay_color )
  					),
  				) );
  			}

  			if ( 'on' === $use_overlay ) {
  				$data_icon = '' !== $hover_icon
  					? sprintf(
  						' data-icon="%1$s"',
  						esc_attr( et_pb_process_font_icon( $hover_icon ) )
  					)
  					: '';

  				$overlay_output = sprintf(
  					'<span class="et_overlay%1$s"%2$s></span>',
  					( '' !== $hover_icon ? ' et_pb_inline_icon' : '' ),
  					$data_icon
  				);
  			}

  			$overlay_class = 'on' === $use_overlay ? ' et_pb_has_overlay' : '';

  			if ( 'on' !== $fullwidth ){
  				if ( 'on' === $use_dropshadow ) {
  					$module_class .= ' et_pb_blog_grid_dropshadow';
  				}

  				wp_enqueue_script( 'salvattore' );

  				$background_layout = 'light';
  			}

  			$args = array( 'posts_per_page' => (int) $posts_number );

  			$et_paged = is_front_page() ? get_query_var( 'page' ) : get_query_var( 'paged' );

  			if ( is_front_page() ) {
  				$paged = $et_paged;
  			}

  			if ( '' !== $include_categories ) {
  				$args['cat'] = $include_categories;
  			}

  	        // if ( '' !== $include_post_types ) {
  			// 	$args['post_type'] = 'interview';//$include_post_types;
  			// }



			if('' !== $include_post_types) {

				switch ($include_post_types) {
					case '1':
						$include_post_types = array(
							'interview',
							'enhanced_article',
							'good_example'
						);
						break;
					case '2':
						$include_post_types = array(
							'interview',
							'enhanced_article',
							'good_example',
							'news'
						);
						break;

				}

				$args['post_type'] = $include_post_types;
			}

			//  var_dump($include_categories);			//TODO
			// var_dump($include_post_types);
			// exit;

  			if ( ! is_search() ) {
  				$args['paged'] = $et_paged;
  			}

  			if ( '' !== $offset_number && ! empty( $offset_number ) ) {
  				/**
  				 * Offset + pagination don't play well. Manual offset calculation required
  				 * @see: https://codex.wordpress.org/Making_Custom_Queries_using_Offset_and_Pagination
  				 */
  				if ( $paged > 1 ) {
  					$args['offset'] = ( ( $et_paged - 1 ) * intval( $posts_number ) ) + intval( $offset_number );
  				} else {
  					$args['offset'] = intval( $offset_number );
  				}
  			}

  			if ( is_single() && ! isset( $args['post__not_in'] ) ) {
  				$args['post__not_in'] = array( get_the_ID() );
  			}

  			ob_start();

  			query_posts( $args );

  			if ( have_posts() ) {

                if ( 'off' === $fullwidth ) {
				   echo '<div class="et_pb_salvattore_content" data-columns>';
			    }

  				while ( have_posts() ) {
  					the_post();

  					$post_format = et_pb_post_format();

  					$thumb = '';

  					$width = 'on' === $fullwidth ? 620 : 400;
  					$width = (int) apply_filters( 'et_pb_blog_image_width', $width );

  					$height = 'on' === $fullwidth ? 413 : 267;
  					$height = (int) apply_filters( 'et_pb_blog_image_height', $height );
  					$classtext = 'on' === $fullwidth ? 'et_pb_post_main_image' : '';
  					$titletext = get_the_title();
            $thumbnail_url = get_the_post_thumbnail_url();
  					$thumbnail = get_thumbnail( $width, $height, $classtext, $titletext, $titletext, false, 'Blogimage' );
  					$thumb = $thumbnail["thumb"];

  					$no_thumb_class = '' === $thumb || 'off' === $show_thumbnail ? ' et_pb_no_thumb' : '';

  					if ( in_array( $post_format, array( 'video', 'gallery' ) ) ) {
  						$no_thumb_class = '';
  					}



                    //if we want to use layout with full background img & white text
                    if ($full_height_img === 'yes') {
                        ?><article id="post-<?php the_ID(); ?>" style="background-image: url(<?php echo($thumbnail_url) ?>);" <?php post_class( 'et_pb_post clearfix' . $no_thumb_class . $overlay_class  ); ?>><?php
                    }
                    else {
                        ?><article id="post-<?php the_ID(); ?>" <?php post_class( 'et_pb_post clearfix' . $no_thumb_class . $overlay_class  ); ?>><?php
                    }

  					et_divi_post_format_content();

  					if ( ! in_array( $post_format, array( 'link', 'audio', 'quote' ) ) ) {
  						if ( 'video' === $post_format && false !== ( $first_video = et_get_first_video() ) ) :
  							$video_overlay = has_post_thumbnail() ? sprintf(
  								'<div class="et_pb_video_overlay" style="background-image: url(%1$s); background-size: cover;">
  									<div class="et_pb_video_overlay_hover">
  										<a href="#" class="et_pb_video_play"></a>
  									</div>
  								</div>',
  								$thumb
  							) : '';

  							printf(
  								'<div class="et_main_video_container">
  									%1$s
  									%2$s
  								</div>',
  								$video_overlay,
  								$first_video
  							);
  						elseif ( 'gallery' === $post_format ) :
  							et_pb_gallery_images( 'slider' );
  						elseif ( '' !== $thumb && 'on' === $show_thumbnail ) :
  							if ( 'on' !== $fullwidth ) echo '<div class="et_pb_image_container">';
  								if('on' === $show_categories) {
  									$categories = get_the_category();
  									if ( ! empty( $categories ) ) {
  									    //echo esc_html( $categories[0]->name );
  										echo '<div class="post-list-category">' .esc_html( $categories[0]->name ). '</div>';
  									}
  								} ?>
  								<a href="<?php esc_url( the_permalink() ); ?>" class="entry-featured-image-url">
                    <div class="postlist_image_block" style="background-image: url(<?php echo($thumbnail_url) ?>);"></div>
  									<?php if ( 'on' === $use_overlay ) {
  										echo $overlay_output;
  									} ?>
  								</a>
  						<?php
  							if ( 'on' !== $fullwidth ) echo '</div> <!-- .et_pb_image_container -->';
  						endif;
  					} ?>

  				<?php if ( 'off' === $fullwidth || ! in_array( $post_format, array( 'link', 'audio', 'quote' ) ) ) { ?>
  					<?php if ( ! in_array( $post_format, array( 'link', 'audio' ) ) ) { ?>
  						<h2 class="entry-title"><a href="<?php esc_url( the_permalink() ); ?>"><?php the_title(); ?></a></h2>
  					<?php } ?>

  					<?php
  						if ( 'on' === $show_author || 'on' === $show_date ) {
                            // if ('on' === $show_date) {
                            //     # code...
                            // }
                            // if (get_post_meta( $id, 'guest-author', true ) != '') {
                            //     $author = get_post_meta( $id, 'guest-author', true );
                            // }
                            // else {
                            //     $author = get_the_author();
                            // }
                            $author = get_the_author();

                     		printf( '<p class="post-meta">%1$s %2$s</p>',
                            (
								'on' === $show_date
									? et_get_safe_localization( sprintf( __( '%s', 'et_builder' ), '<span class="published">Publicerad ' . get_the_date() . '</span>' ) )
									: ''
                            ),
							(
								'on' === $show_author
									? et_get_safe_localization( sprintf( __( 'by %s', 'et_builder' ), '<span class="author vcard">' .  $author . '</span>' ) )
									: ''
							)
  					// 			(
  					// 				( 'on' === $show_author && 'on' === $show_date )
  					// 					? ' | '
  					// 					: ''
  					// 			),
  					// 			(
  					// 				'on' === $show_date
  					// 					? et_get_safe_localization( sprintf( __( '%s', 'et_builder' ), '<span class="published">' . esc_html( get_the_date( $meta_date ) ) . '</span>' ) )
  					// 					: ''
  					// 			),
  					// 			(
  					// 				(( 'on' === $show_author || 'on' === $show_date ) && 'on' === $show_categories)
  					// 					? ' | '
  					// 					: ''
  					// 			),
  								// (
  								// 	'on' === $show_categories
  								// 		? get_the_category_list(', ')
  								// 		: ''
  								// ),

  						// 		(
  						// 			(( 'on' === $show_author || 'on' === $show_date || 'on' === $show_categories ) && 'on' === $show_comments)
  						// 				? ' | '
  						// 				: ''
  						// 		),
  						// 		(
  						// 			'on' === $show_comments
  						// 				? sprintf( esc_html( _nx( '1 Comment', '%s Comments', get_comments_number(), 'number of comments', 'et_builder' ) ), number_format_i18n( get_comments_number() ) )
  						// 				: ''
  						// 		)
  							);
  						}

  						echo '<div class="post-content">';

  						$obj = get_post_type_object( get_post_type() );
                        if ('on' === $show_post_types) {
                            echo '<div class="post-list-post-type"><i class="material-icons">fiber_manual_record</i>'. $obj->labels->singular_name .'</div>';

                        }

  						global $et_pb_rendering_column_content;

  						$post_content = et_strip_shortcodes( et_delete_post_first_video( get_the_content() ), true );

  						$et_pb_rendering_column_content = true;

  						if ( 'on' === $show_content ) {
  							global $more;

  							// page builder doesn't support more tag, so display the_content() in case of post made with page builder
  							if ( et_pb_is_pagebuilder_used( get_the_ID() ) ) {
  								$more = 1;
  								echo apply_filters( 'the_content', $post_content );
  							} else {
  								$more = null;
  								echo apply_filters( 'the_content', et_delete_post_first_video( get_the_content( esc_html__( 'read more...', 'et_builder' ) ) ) );
  							}
  						} else {
  							if ( has_excerpt() ) {
  								the_excerpt();
  							} else {
  								echo wpautop( et_delete_post_first_video( strip_shortcodes( truncate_post( 125, false, '', true ) ) ) );
  							}
  						}

  						$et_pb_rendering_column_content = false;

  						if ( 'on' !== $show_content ) {
  							$more = 'on' == $show_more ? sprintf( ' <a href="%1$s" class="more-link" >%2$s</a>' , esc_url( get_permalink() ), esc_html__( 'Läs mer', 'et_builder' ) )  : '';
  							echo $more;
  						}

  						echo '</div>';
  						?>
  				<?php } // 'off' === $fullwidth || ! in_array( $post_format, array( 'link', 'audio', 'quote', 'gallery' ?>

  				</article> <!-- .et_pb_post -->
  		<?php
  				} // endwhile

                if ( 'off' === $fullwidth ) {
 				echo '</div><!-- .et_pb_salvattore_content -->';
 			}

			if ( 'on' === $show_pagination && ! is_search() ) {

  					if ( function_exists( 'wp_pagenavi' ) ) {
  						wp_pagenavi();
  					} else {
  						if ( et_is_builder_plugin_active() ) {
  							include( ET_BUILDER_PLUGIN_DIR . 'includes/navigation.php' );
  						} else {
  							get_template_part( 'includes/navigation', 'index' );
  						}
  					}
                    echo '</div> <!-- .et_pb_posts -->';

				$container_is_closed = true;
  				}
  			} else {
  				if ( et_is_builder_plugin_active() ) {
  					include( ET_BUILDER_PLUGIN_DIR . 'includes/no-results.php' );
  				} else {
  					get_template_part( 'includes/no-results', 'index' );
  				}
  			}

  			wp_reset_query();

  			$posts = ob_get_contents();

  			ob_end_clean();

  			$class = " et_pb_module et_pb_bg_layout_{$background_layout}";

  			$output = sprintf(
                '<div%5$s class="%1$s%3$s%6$s">
				<div class="et_pb_ajax_pagination_container">
					%2$s
				</div>
  				%4$s',
  				( 'on' === $fullwidth ? 'et_pb_posts' : 'et_pb_blog_grid clearfix' ),
  				$posts,
  				esc_attr( $class ),
  				( ! $container_is_closed ? '</div> <!-- .et_pb_posts -->' : '' ),
  				( '' !== $module_id ? sprintf( ' id="%1$s"', esc_attr( $module_id ) ) : '' ),
  				( '' !== $module_class ? sprintf( ' %1$s', esc_attr( $module_class ) ) : '' ),
  				( '' !== $module_class ? sprintf( ' %1$s', esc_attr( $module_class ) ) : '' ),
                ( 'yes' === $full_height_img ? ' post-list-full-bgr-img' : '' )
  			);

  			if ( 'on' !== $fullwidth )
  				$output = sprintf( '<div class="et_pb_blog_grid_wrapper">%1$s</div>', $output );

  			// Restore $wp_filter
  			$wp_filter = $wp_filter_cache;
  			unset($wp_filter_cache);

  			return $output;
  		}
  	}
  	//new DS_Custom_Module_Blog;
  	$et_builder_module_blog2 = new DS_Custom_Module_Blog();
    add_shortcode( 'et_pb_blog_2', array($et_builder_module_blog2, '_shortcode_callback') );

    class ET_Builder_Module_Image_2 extends ET_Builder_Module {
    	function init() {
    		$this->name       = esc_html__( 'Image', 'et_builder' );
    		$this->slug       = 'et_pb_image';
    		$this->fb_support = true;

    		$this->whitelisted_fields = array(
    			'src',
    			'alt',
    			'title_text',
    			'show_in_lightbox',
    			'url',
    			'url_new_window',
    			'animation',
    			'sticky',
    			'align',
    			'admin_label',
    			'module_id',
    			'module_class',
    			'max_width',
    			'force_fullwidth',
    			'always_center_on_mobile',
    			'use_overlay',
    			'overlay_icon_color',
    			'hover_overlay_color',
    			'hover_icon',
    			'max_width_tablet',
    			'max_width_phone',
    			'max_width_last_edited',
    		);

    		$animation_option_name = sprintf( '%1$s-animation', $this->slug );
    		$global_animation_direction = ET_Global_Settings::get_value( $animation_option_name );
    		$default_animation = $global_animation_direction && '' !== $global_animation_direction ? $global_animation_direction : 'left';

    		$this->fields_defaults = array(
    			'show_in_lightbox'        => array( 'off' ),
    			'url_new_window'          => array( 'off' ),
    			'animation'               => array( $default_animation ),
    			'sticky'                  => array( 'off' ),
    			'align'                   => array( 'left' ),
    			'force_fullwidth'         => array( 'off' ),
    			'always_center_on_mobile' => array( 'on' ),
    			'use_overlay'             => array( 'off' ),
    		);

    		$this->advanced_options = array(
    			'border'                => array(),
    			'custom_margin_padding' => array(
    				'use_padding' => false,
    				'css' => array(
    					'important' => 'all',
    				),
    			),
    		);
    	}

    	function get_fields() {
    		// List of animation options
    		$animation_options_list = array(
    			'left'    => esc_html__( 'Left To Right', 'et_builder' ),
    			'right'   => esc_html__( 'Right To Left', 'et_builder' ),
    			'top'     => esc_html__( 'Top To Bottom', 'et_builder' ),
    			'bottom'  => esc_html__( 'Bottom To Top', 'et_builder' ),
    			'fade_in' => esc_html__( 'Fade In', 'et_builder' ),
    			'off'     => esc_html__( 'No Animation', 'et_builder' ),
    		);

    		$animation_option_name       = sprintf( '%1$s-animation', $this->slug );
    		$default_animation_direction = ET_Global_Settings::get_value( $animation_option_name );

    		// If user modifies default animation option via Customizer, we'll need to change the order
    		if ( 'left' !== $default_animation_direction && ! empty( $default_animation_direction ) && array_key_exists( $default_animation_direction, $animation_options_list ) ) {
    			// The options, sans user's preferred direction
    			$animation_options_wo_default = $animation_options_list;
    			unset( $animation_options_wo_default[ $default_animation_direction ] );

    			// All animation options
    			$animation_options = array_merge(
    				array( $default_animation_direction => $animation_options_list[$default_animation_direction] ),
    				$animation_options_wo_default
    			);
    		} else {
    			// Simply copy the animation options
    			$animation_options = $animation_options_list;
    		}

    		$fields = array(
    			'src' => array(
    				'label'              => esc_html__( 'Image URL', 'et_builder' ),
    				'type'               => 'upload',
    				'option_category'    => 'basic_option',
    				'upload_button_text' => esc_attr__( 'Upload an image', 'et_builder' ),
    				'choose_text'        => esc_attr__( 'Choose an Image', 'et_builder' ),
    				'update_text'        => esc_attr__( 'Set As Image', 'et_builder' ),
    				'description'        => esc_html__( 'Upload your desired image, or type in the URL to the image you would like to display.', 'et_builder' ),
    			),
    			'alt' => array(
    				'label'           => esc_html__( 'Image Alternative Text', 'et_builder' ),
    				'type'            => 'text',
    				'option_category' => 'basic_option',
    				'description'     => esc_html__( 'This defines the HTML ALT text. A short description of your image can be placed here.', 'et_builder' ),
    			),
    			'title_text' => array(
    				'label'           => esc_html__( 'Image Title Text', 'et_builder' ),
    				'type'            => 'text',
    				'option_category' => 'basic_option',
    				'description'     => esc_html__( 'This defines the HTML Title text.', 'et_builder' ),
    			),
    			'show_in_lightbox' => array(
    				'label'             => esc_html__( 'Open in Lightbox', 'et_builder' ),
    				'type'              => 'yes_no_button',
    				'option_category'   => 'configuration',
    				'options'           => array(
    					'off' => esc_html__( "No", 'et_builder' ),
    					'on'  => esc_html__( 'Yes', 'et_builder' ),
    				),
    				'affects'           => array(
    					'url',
    					'url_new_window',
    					'use_overlay'
    				),
    				'description'       => esc_html__( 'Here you can choose whether or not the image should open in Lightbox. Note: if you select to open the image in Lightbox, url options below will be ignored.', 'et_builder' ),
    			),
    			'url' => array(
    				'label'           => esc_html__( 'Link URL', 'et_builder' ),
    				'type'            => 'text',
    				'option_category' => 'basic_option',
    				'depends_show_if' => 'off',
    				'affects'         => array(
    					'use_overlay',
    				),
    				'description'     => esc_html__( 'If you would like your image to be a link, input your destination URL here. No link will be created if this field is left blank.', 'et_builder' ),
    			),
    			'url_new_window' => array(
    				'label'             => esc_html__( 'Url Opens', 'et_builder' ),
    				'type'              => 'select',
    				'option_category'   => 'configuration',
    				'options'           => array(
    					'off' => esc_html__( 'In The Same Window', 'et_builder' ),
    					'on'  => esc_html__( 'In The New Tab', 'et_builder' ),
    				),
    				'depends_show_if'   => 'off',
    				'description'       => esc_html__( 'Here you can choose whether or not your link opens in a new window', 'et_builder' ),
    			),
    			'use_overlay' => array(
    				'label'             => esc_html__( 'Image Overlay', 'et_builder' ),
    				'type'              => 'yes_no_button',
    				'option_category'   => 'layout',
    				'options'           => array(
    					'off' => esc_html__( 'Off', 'et_builder' ),
    					'on'  => esc_html__( 'On', 'et_builder' ),
    				),
    				'affects'           => array(
    					'overlay_icon_color',
    					'hover_overlay_color',
    					'hover_icon',
    				),
    				'depends_default'   => true,
    				'description'       => esc_html__( 'If enabled, an overlay color and icon will be displayed when a visitors hovers over the image', 'et_builder' ),
    			),
    			'overlay_icon_color' => array(
    				'label'             => esc_html__( 'Overlay Icon Color', 'et_builder' ),
    				'type'              => 'color',
    				'custom_color'      => true,
    				'depends_show_if'   => 'on',
    				'description'       => esc_html__( 'Here you can define a custom color for the overlay icon', 'et_builder' ),
    			),
    			'hover_overlay_color' => array(
    				'label'             => esc_html__( 'Hover Overlay Color', 'et_builder' ),
    				'type'              => 'color-alpha',
    				'custom_color'      => true,
    				'depends_show_if'   => 'on',
    				'description'       => esc_html__( 'Here you can define a custom color for the overlay', 'et_builder' ),
    			),
    			'hover_icon' => array(
    				'label'               => esc_html__( 'Hover Icon Picker', 'et_builder' ),
    				'type'                => 'text',
    				'option_category'     => 'configuration',
    				'class'               => array( 'et-pb-font-icon' ),
    				'renderer'            => 'et_pb_get_font_icon_list',
    				'renderer_with_field' => true,
    				'depends_show_if'     => 'on',
    				'description'       => esc_html__( 'Here you can define a custom icon for the overlay', 'et_builder' ),
    			),
    			'animation' => array(
    				'label'             => esc_html__( 'Animation', 'et_builder' ),
    				'type'              => 'select',
    				'option_category'   => 'configuration',
    				'options'           => $animation_options,
    				'description'       => esc_html__( 'This controls the direction of the lazy-loading animation.', 'et_builder' ),
    			),
    			'sticky' => array(
    				'label'             => esc_html__( 'Remove Space Below The Image', 'et_builder' ),
    				'type'              => 'yes_no_button',
    				'option_category'   => 'layout',
    				'options'           => array(
    					'off'     => esc_html__( 'No', 'et_builder' ),
    					'on'      => esc_html__( 'Yes', 'et_builder' ),
    				),
    				'description'       => esc_html__( 'Here you can choose whether or not the image should have a space below it.', 'et_builder' ),
    			),
    			'align' => array(
    				'label'           => esc_html__( 'Image Alignment', 'et_builder' ),
    				'type'            => 'select',
    				'option_category' => 'layout',
    				'options' => array(
    					'left'   => esc_html__( 'Left', 'et_builder' ),
    					'center' => esc_html__( 'Center', 'et_builder' ),
    					'right'  => esc_html__( 'Right', 'et_builder' ),
    				),
    				'description'       => esc_html__( 'Here you can choose the image alignment.', 'et_builder' ),
    			),
    			'max_width' => array(
    				'label'           => esc_html__( 'Image Max Width', 'et_builder' ),
    				'type'            => 'text',
    				'option_category' => 'layout',
    				'tab_slug'        => 'advanced',
    				'mobile_options'  => true,
    				'validate_unit'   => true,
    			),
    			'max_width_last_edited' => array(
    				'type'     => 'skip',
    				'tab_slug' => 'advanced',
    			),
    			'force_fullwidth' => array(
    				'label'             => esc_html__( 'Force Fullwidth', 'et_builder' ),
    				'type'              => 'yes_no_button',
    				'option_category'   => 'layout',
    				'options'           => array(
    					'off' => esc_html__( "No", 'et_builder' ),
    					'on'  => esc_html__( 'Yes', 'et_builder' ),
    				),
    				'tab_slug'    => 'advanced',
    			),
    			'always_center_on_mobile' => array(
    				'label'             => esc_html__( 'Always Center Image On Mobile', 'et_builder' ),
    				'type'              => 'yes_no_button',
    				'option_category'   => 'layout',
    				'options'           => array(
    					'on'  => esc_html__( 'Yes', 'et_builder' ),
    					'off' => esc_html__( "No", 'et_builder' ),
    				),
    				'tab_slug'    => 'advanced',
    			),
    			'max_width_tablet' => array(
    				'type'     => 'skip',
    				'tab_slug' => 'advanced',
    			),
    			'max_width_phone' => array(
    				'type'     => 'skip',
    				'tab_slug' => 'advanced',
    			),
    			'disabled_on' => array(
    				'label'           => esc_html__( 'Disable on', 'et_builder' ),
    				'type'            => 'multiple_checkboxes',
    				'options'         => array(
    					'phone'   => esc_html__( 'Phone', 'et_builder' ),
    					'tablet'  => esc_html__( 'Tablet', 'et_builder' ),
    					'desktop' => esc_html__( 'Desktop', 'et_builder' ),
    				),
    				'additional_att'  => 'disable_on',
    				'option_category' => 'configuration',
    				'description'     => esc_html__( 'This will disable the module on selected devices', 'et_builder' ),
    			),
    			'admin_label' => array(
    				'label'       => esc_html__( 'Admin Label', 'et_builder' ),
    				'type'        => 'text',
    				'description' => esc_html__( 'This will change the label of the module in the builder for easy identification.', 'et_builder' ),
    			),
    			'module_id' => array(
    				'label'           => esc_html__( 'CSS ID', 'et_builder' ),
    				'type'            => 'text',
    				'option_category' => 'configuration',
    				'tab_slug'        => 'custom_css',
    				'option_class'    => 'et_pb_custom_css_regular',
    			),
    			'module_class' => array(
    				'label'           => esc_html__( 'CSS Class', 'et_builder' ),
    				'type'            => 'text',
    				'option_category' => 'configuration',
    				'tab_slug'        => 'custom_css',
    				'option_class'    => 'et_pb_custom_css_regular',
    			),
    		);

    		return $fields;
    	}

    	function shortcode_callback( $atts, $content = null, $function_name ) {
    		$module_id               = $this->shortcode_atts['module_id'];
    		$module_class            = $this->shortcode_atts['module_class'];
    		$src                     = $this->shortcode_atts['src'];
    		$alt                     = $this->shortcode_atts['alt'];
    		$title_text              = $this->shortcode_atts['title_text'];
    		$animation               = $this->shortcode_atts['animation'];
    		$url                     = $this->shortcode_atts['url'];
    		$url_new_window          = $this->shortcode_atts['url_new_window'];
    		$show_in_lightbox        = $this->shortcode_atts['show_in_lightbox'];
    		$sticky                  = $this->shortcode_atts['sticky'];
    		$align                   = $this->shortcode_atts['align'];
    		$max_width               = $this->shortcode_atts['max_width'];
    		$max_width_tablet        = $this->shortcode_atts['max_width_tablet'];
    		$max_width_phone         = $this->shortcode_atts['max_width_phone'];
    		$max_width_last_edited   = $this->shortcode_atts['max_width_last_edited'];
    		$force_fullwidth         = $this->shortcode_atts['force_fullwidth'];
    		$always_center_on_mobile = $this->shortcode_atts['always_center_on_mobile'];
    		$overlay_icon_color      = $this->shortcode_atts['overlay_icon_color'];
    		$hover_overlay_color     = $this->shortcode_atts['hover_overlay_color'];
    		$hover_icon              = $this->shortcode_atts['hover_icon'];
    		$use_overlay             = $this->shortcode_atts['use_overlay'];

    		$module_class = ET_Builder_Element::add_module_order_class( $module_class, $function_name );

    		if ( 'on' === $always_center_on_mobile ) {
    			$module_class .= ' et_always_center_on_mobile';
    		}

    		// overlay can be applied only if image has link or if lightbox enabled
    		$is_overlay_applied = 'on' === $use_overlay && ( 'on' === $show_in_lightbox || ( 'off' === $show_in_lightbox && '' !== $url ) ) ? 'on' : 'off';

    		if ( '' !== $max_width_tablet || '' !== $max_width_phone || '' !== $max_width ) {
    			$max_width_responsive_active = et_pb_get_responsive_status( $max_width_last_edited );

    			$max_width_values = array(
    				'desktop' => $max_width,
    				'tablet'  => $max_width_responsive_active ? $max_width_tablet : '',
    				'phone'   => $max_width_responsive_active ? $max_width_phone : '',
    			);

    			et_pb_generate_responsive_css( $max_width_values, '%%order_class%%', 'max-width', $function_name );
    		}

    		if ( 'on' === $force_fullwidth ) {
    			ET_Builder_Element::set_style( $function_name, array(
    				'selector'    => '%%order_class%% img',
    				'declaration' => 'width: 100%;',
    			) );
    		}

    		if ( $this->fields_defaults['align'][0] !== $align ) {
    			ET_Builder_Element::set_style( $function_name, array(
    				'selector'    => '%%order_class%%',
    				'declaration' => sprintf(
    					'text-align: %1$s;',
    					esc_html( $align )
    				),
    			) );
    		}

    		if ( 'center' !== $align ) {
    			ET_Builder_Element::set_style( $function_name, array(
    				'selector'    => '%%order_class%%',
    				'declaration' => sprintf(
    					'margin-%1$s: 0;',
    					esc_html( $align )
    				),
    			) );
    		}

    		if ( 'on' === $is_overlay_applied ) {
    			if ( '' !== $overlay_icon_color ) {
    				ET_Builder_Element::set_style( $function_name, array(
    					'selector'    => '%%order_class%% .et_overlay:before',
    					'declaration' => sprintf(
    						'color: %1$s !important;',
    						esc_html( $overlay_icon_color )
    					),
    				) );
    			}

    			if ( '' !== $hover_overlay_color ) {
    				ET_Builder_Element::set_style( $function_name, array(
    					'selector'    => '%%order_class%% .et_overlay',
    					'declaration' => sprintf(
    						'background-color: %1$s;',
    						esc_html( $hover_overlay_color )
    					),
    				) );
    			}

    			$data_icon = '' !== $hover_icon
    				? sprintf(
    					' data-icon="%1$s"',
    					esc_attr( et_pb_process_font_icon( $hover_icon ) )
    				)
    				: '';

    			$overlay_output = sprintf(
    				'<span class="et_overlay%1$s"%2$s></span>',
    				( '' !== $hover_icon ? ' et_pb_inline_icon' : '' ),
    				$data_icon
    			);
    		}

    		$output = sprintf(
    			'<img src="%1$s" alt="%2$s"%3$s /> %5$s
    			%4$s',
    			esc_url( $src ),
    			esc_attr( $alt ),
    			( '' !== $title_text ? sprintf( ' title="%1$s"', esc_attr( $title_text ) ) : '' ),
    			'on' === $is_overlay_applied ? $overlay_output : '',
    			( '' !== $title_text ? sprintf( ' <p class="image-caption">%1$s</p>', esc_attr( $title_text ) ) : '' )
    		);


    		if ( 'on' === $show_in_lightbox ) {
    			$output = sprintf( '<a href="%1$s" class="et_pb_lightbox_image" title="%3$s">%2$s</a>',
    				esc_url( $src ),
    				$output,
    				esc_attr( $alt )
    			);
    		} else if ( '' !== $url ) {
    			$output = sprintf( '<a href="%1$s"%3$s>%2$s</a>',
    				esc_url( $url ),
    				$output,
    				( 'on' === $url_new_window ? ' target="_blank"' : '' )
    			);
    		}

    		$animation = '' === $animation ? ET_Global_Settings::get_value( 'et_pb_image-animation' ) : $animation;

    		$output = sprintf(
    			'<div%5$s class="et_pb_module et-waypoint et_pb_image%2$s%3$s%4$s%6$s">
    				%1$s
    			</div>',
    			$output,
    			esc_attr( " et_pb_animation_{$animation}" ),
    			( '' !== $module_class ? sprintf( ' %1$s', esc_attr( ltrim( $module_class ) ) ) : '' ),
    			( 'on' === $sticky ? esc_attr( ' et_pb_image_sticky' ) : '' ),
    			( '' !== $module_id ? sprintf( ' id="%1$s"', esc_attr( $module_id ) ) : '' ),
    			'on' === $is_overlay_applied ? ' et_pb_has_overlay' : ''
    		);

    		return $output;
    	}
    }
    //new ET_Builder_Module_Image;
    //extend DS_Custom_Module_Image;
  	$et_builder_module_img_2 = new ET_Builder_Module_Image_2;
  	add_shortcode( 'et_pb_image', array($et_builder_module_img_2, '_shortcode_callback') );

    class ET_Builder_Module_Filterable_Portfolio2 extends ET_Builder_Module {
    	function init() {
    		$this->name       = esc_html__( 'Filterable Portfolio', 'et_builder' );
    		$this->slug       = 'et_pb_filterable_portfolio';
    		$this->fb_support = true;

    		$this->whitelisted_fields = array(
    			'fullwidth',
    			'posts_number',
    			'include_categories',
    			'show_title',
    			'show_categories',
    			'show_pagination',
    			'background_layout',
    			'admin_label',
    			'module_id',
    			'module_class',
    			'hover_icon',
    			'zoom_icon_color',
    			'hover_overlay_color',
    		);

    		$this->fields_defaults = array(
    			'fullwidth'         => array( 'on' ),
    			'posts_number'      => array( 10, 'add_default_setting' ),
    			'show_title'        => array( 'on' ),
    			'show_categories'   => array( 'on' ),
    			'show_pagination'   => array( 'on' ),
    			'background_layout' => array( 'light' ),
    		);

    		$this->main_css_element = '%%order_class%%.et_pb_filterable_portfolio';

    		$this->options_toggles = array(
    			'general'  => array(
    				'toggles' => array(
    					'main_content' => esc_html__( 'Content', 'et_builder' ),
    					'elements'     => esc_html__( 'Elements', 'et_builder' ),
    				),
    			),
    			'advanced' => array(
    				'toggles' => array(
    					'layout'  => esc_html__( 'Layout', 'et_builder' ),
    					'overlay' => esc_html__( 'Overlay', 'et_builder' ),
    					'text'    => array(
    						'title'    => esc_html__( 'Text', 'et_builder' ),
    						'priority' => 49,
    					),
    				),
    			),
    		);

    		$this->advanced_options = array(
    			'fonts' => array(
    				'title'   => array(
    					'label'    => esc_html__( 'Title', 'et_builder' ),
    					'css'      => array(
    						'main' => "{$this->main_css_element} h2",
    						'plugin_main' => "{$this->main_css_element} h2, {$this->main_css_element} h2 a",
    						'important' => 'all',
    					),
    				),
    				'filter' => array(
    					'label'    => esc_html__( 'Filter', 'et_builder' ),
    					'css'      => array(
    						'main' => "{$this->main_css_element} .et_pb_portfolio_filter",
    						'plugin_main' => "{$this->main_css_element} .et_pb_portfolio_filter, {$this->main_css_element} .et_pb_portfolio_filter a",
    						'color' => "{$this->main_css_element} .et_pb_portfolio_filter a",
    					),
    				),
    				'caption' => array(
    					'label'    => esc_html__( 'Meta', 'et_builder' ),
    					'css'      => array(
    						'main' => "{$this->main_css_element} .post-meta, {$this->main_css_element} .post-meta a",
    					),
    				),
    			),
    			'background' => array(
    				'settings' => array(
    					'color' => 'alpha',
    				),
    			),
    			'border' => array(
    				'css' => array(
    					'main' => "{$this->main_css_element} .et_pb_portfolio_item",
    				),
    			),
    		);
    		$this->custom_css_options = array(
    			'portfolio_filters' => array(
    				'label'    => esc_html__( 'Portfolio Filters', 'et_builder' ),
    				'selector' => '.et_pb_filterable_portfolio .et_pb_portfolio_filters',
    				'no_space_before_selector' => true,
    			),
    			'active_portfolio_filter' => array(
    				'label'    => esc_html__( 'Active Portfolio Filter', 'et_builder' ),
    				'selector' => '.et_pb_filterable_portfolio .et_pb_portfolio_filters li a.active',
    				'no_space_before_selector' => true,
    			),
    			'portfolio_image' => array(
    				'label'    => esc_html__( 'Portfolio Image', 'et_builder' ),
    				'selector' => '.et_portfolio_image',
    			),
    			'overlay' => array(
    				'label'    => esc_html__( 'Overlay', 'et_builder' ),
    				'selector' => '.et_overlay',
    			),
    			'overlay_icon' => array(
    				'label'    => esc_html__( 'Overlay Icon', 'et_builder' ),
    				'selector' => '.et_overlay:before',
    			),
    			'portfolio_title' => array(
    				'label'    => esc_html__( 'Portfolio Title', 'et_builder' ),
    				'selector' => '.et_pb_portfolio_item h2',
    			),
    			'portfolio_post_meta' => array(
    				'label'    => esc_html__( 'Portfolio Post Meta', 'et_builder' ),
    				'selector' => '.et_pb_portfolio_item .post-meta',
    			),
    			'portfolio_pagination' => array(
    				'label'    => esc_html__( 'Portfolio Pagination', 'et_builder' ),
    				'selector' => '.et_pb_portofolio_pagination',
    			),
    			'portfolio_pagination_active' => array(
    				'label'    => esc_html__( 'Pagination Active Page', 'et_builder' ),
    				'selector' => '.et_pb_portofolio_pagination a.active',
    			),
    		);
    	}

    	function get_fields() {
    		$fields = array(
    			'fullwidth' => array(
    				'label'           => esc_html__( 'Layout', 'et_builder' ),
    				'type'            => 'select',
    				'option_category' => 'layout',
    				'options'         => array(
    					'on'  => esc_html__( 'Fullwidth', 'et_builder' ),
    					'off' => esc_html__( 'Grid', 'et_builder' ),
    				),
    				'affects' => array(
    					'hover_icon',
    					'zoom_icon_color',
    					'hover_overlay_color',
    				),
    				'description'      => esc_html__( 'Choose your desired portfolio layout style.', 'et_builder' ),
    				'computed_affects' => array(
    					'__projects',
    				),
    				'tab_slug'         => 'advanced',
    				'toggle_slug'      => 'layout',
    			),
    			'posts_number' => array(
    				'label'            => esc_html__( 'Posts Number', 'et_builder' ),
    				'type'             => 'text',
    				'option_category'  => 'configuration',
    				'description'      => esc_html__( 'Define the number of projects that should be displayed per page.', 'et_builder' ),
    				'computed_affects' => array(
    					'__projects',
    				),
    				'toggle_slug'      => 'main_content',
    			),
    			'include_categories' => array(
    				'label'            => esc_html__( 'Include Categories', 'et_builder' ),
    				'renderer'         => 'et_builder_include_categories_option',
    				'option_category'  => 'basic_option',
    				'description'      => esc_html__( 'Select the categories that you would like to include in the feed.', 'et_builder' ),
    				'computed_affects' => array(
    					'__project_terms',
    					'__projects',
    				),
    				'taxonomy_name'    => 'project_category',
    				'toggle_slug'      => 'main_content',
    			),
    			'show_title' => array(
    				'label'             => esc_html__( 'Show Title', 'et_builder' ),
    				'type'              => 'yes_no_button',
    				'option_category'   => 'configuration',
    				'options'           => array(
    					'on'  => esc_html__( 'Yes', 'et_builder' ),
    					'off' => esc_html__( 'No', 'et_builder' ),
    				),
    				'toggle_slug'       => 'elements',
    				'description'       => esc_html__( 'Turn project titles on or off.', 'et_builder' ),
    			),
    			'show_categories' => array(
    				'label'             => esc_html__( 'Show Categories', 'et_builder' ),
    				'type'              => 'yes_no_button',
    				'option_category'   => 'configuration',
    				'options'           => array(
    					'on'  => esc_html__( 'Yes', 'et_builder' ),
    					'off' => esc_html__( 'No', 'et_builder' ),
    				),
    				'toggle_slug'       => 'elements',
    				'description'       => esc_html__( 'Turn the category links on or off.', 'et_builder' ),
    			),
    			'show_pagination' => array(
    				'label'             => esc_html__( 'Show Pagination', 'et_builder' ),
    				'type'              => 'yes_no_button',
    				'option_category'   => 'configuration',
    				'options'           => array(
    					'on'  => esc_html__( 'Yes', 'et_builder' ),
    					'off' => esc_html__( 'No', 'et_builder' ),
    				),
    				'toggle_slug'       => 'elements',
    				'description'       => esc_html__( 'Enable or disable pagination for this feed.', 'et_builder' ),
    			),
    			'background_layout' => array(
    				'label'           => esc_html__( 'Text Color', 'et_builder' ),
    				'type'            => 'select',
    				'option_category' => 'color_option',
    				'options' => array(
    					'light'  => esc_html__( 'Dark', 'et_builder' ),
    					'dark' => esc_html__( 'Light', 'et_builder' ),
    				),
    				'tab_slug'        => 'advanced',
    				'toggle_slug'     => 'text',
    				'description'     => esc_html__( 'Here you can choose whether your text should be light or dark. If you are working with a dark background, then your text should be light. If your background is light, then your text should be set to dark.', 'et_builder' ),
    			),
    			'hover_icon' => array(
    				'label'               => esc_html__( 'Hover Icon Picker', 'et_builder' ),
    				'type'                => 'text',
    				'option_category'     => 'configuration',
    				'class'               => array( 'et-pb-font-icon' ),
    				'renderer'            => 'et_pb_get_font_icon_list',
    				'renderer_with_field' => true,
    				'depends_show_if'     => 'off',
    				'tab_slug'            => 'advanced',
    				'toggle_slug'         => 'overlay',
    			),
    			'zoom_icon_color' => array(
    				'label'             => esc_html__( 'Zoom Icon Color', 'et_builder' ),
    				'type'              => 'color-alpha',
    				'custom_color'      => true,
    				'depends_show_if'   => 'off',
    				'tab_slug'          => 'advanced',
    				'toggle_slug'       => 'overlay',
    			),
    			'hover_overlay_color' => array(
    				'label'             => esc_html__( 'Hover Overlay Color', 'et_builder' ),
    				'type'              => 'color-alpha',
    				'custom_color'      => true,
    				'depends_show_if'   => 'off',
    				'tab_slug'          => 'advanced',
    				'toggle_slug'       => 'overlay',
    			),
    			'__project_terms' => array(
    				'type'                => 'computed',
    				'computed_callback'   => array( 'ET_Builder_Module_Filterable_Portfolio', 'get_portfolio_terms' ),
    				'computed_depends_on' => array(
    					'include_categories',
    				),
    			),
    			'__projects' => array(
    				'type'                => 'computed',
    				'computed_callback'   => array( 'ET_Builder_Module_Filterable_Portfolio', 'get_portfolio_item' ),
    				'computed_depends_on' => array(
    					'show_pagination',
    					'posts_number',
    					'include_categories',
    					'fullwidth',
    				),
    			),
    			'disabled_on' => array(
    				'label'           => esc_html__( 'Disable on', 'et_builder' ),
    				'type'            => 'multiple_checkboxes',
    				'options'         => array(
    					'phone'   => esc_html__( 'Phone', 'et_builder' ),
    					'tablet'  => esc_html__( 'Tablet', 'et_builder' ),
    					'desktop' => esc_html__( 'Desktop', 'et_builder' ),
    				),
    				'additional_att'  => 'disable_on',
    				'option_category' => 'configuration',
    				'description'     => esc_html__( 'This will disable the module on selected devices', 'et_builder' ),
    				'tab_slug'        => 'custom_css',
    				'toggle_slug'     => 'visibility',
    			),
    			'admin_label' => array(
    				'label'       => esc_html__( 'Admin Label', 'et_builder' ),
    				'type'        => 'text',
    				'description' => esc_html__( 'This will change the label of the module in the builder for easy identification.', 'et_builder' ),
    				'toggle_slug' => 'admin_label',
    			),
    			'module_id' => array(
    				'label'           => esc_html__( 'CSS ID', 'et_builder' ),
    				'type'            => 'text',
    				'option_category' => 'configuration',
    				'tab_slug'        => 'custom_css',
    				'toggle_slug'     => 'classes',
    				'option_class'    => 'et_pb_custom_css_regular',
    			),
    			'module_class' => array(
    				'label'           => esc_html__( 'CSS Class', 'et_builder' ),
    				'type'            => 'text',
    				'option_category' => 'configuration',
    				'tab_slug'        => 'custom_css',
    				'toggle_slug'     => 'classes',
    				'option_class'    => 'et_pb_custom_css_regular',
    			),
    		);
    		return $fields;
    	}

    	static function get_portfolio_item( $args = array(), $conditional_tags = array(), $current_page = array() ) {
    		global $et_fb_processing_shortcode_object;

    		$global_processing_original_value = $et_fb_processing_shortcode_object;

    		$defaults = array(
    			'show_pagination'    => 'on',
    			'posts_number'       => '10',
    			'include_categories' => '',
    			'fullwidth'          => 'on',
    		);

    		$args = wp_parse_args( $args, $defaults );

    		if( 'on' === $args['show_pagination'] ) {
    			$query_args['nopaging'] = true;
    		} else {
    			$query_args['posts_per_page'] = (int) $args['posts_number'];
    		}

    		if ( '' !== $args['include_categories'] ) {
    			$query_args['tax_query'] = array(
    				array(
    					'taxonomy' => 'project_category',
    					'field' => 'id',
    					'terms' => explode( ',', $args['include_categories'] ),
    					'operator' => 'IN',
    				)
    			);
    		}

    		$default_query_args = array(
    			'post_type'   => 'project',
    			'post_status' => 'publish',
    		);

    		$query_args = wp_parse_args( $query_args, $default_query_args );

    		// Get portfolio query
    		$query = new WP_Query( $query_args );

    		// Format portfolio output, and add supplementary data
    		$width     = 'on' === $args['fullwidth'] ?  1080 : 400;
    		$width     = (int) apply_filters( 'et_pb_portfolio_image_width', $width );
    		$height    = 'on' === $args['fullwidth'] ?  9999 : 400;
    		$height    = (int) apply_filters( 'et_pb_portfolio_image_height', $height );
    		$classtext = 'on' === $args['fullwidth'] ? 'et_pb_post_main_image' : '';
    		$titletext = get_the_title();

    		// Loop portfolio item and add supplementary data
    		if( $query->have_posts() ) {
    			$post_index = 0;
    			while ( $query->have_posts() ) {
    				$query->the_post();

    				$categories = array();

    				$category_classes = array( 'et_pb_portfolio_item' );

    				if ( 'on' !== $args['fullwidth'] ) {
    					$category_classes[] = 'et_pb_grid_item';
    				}

    				$categories_object = get_the_terms( get_the_ID(), 'project_category' );
    				if ( ! empty( $categories_object ) ) {
    					foreach ( $categories_object as $category ) {
    						// Update category classes which will be used for post_class
    						$category_classes[] = 'project_category_' . urldecode( $category->slug );

    						// Push category data
    						$categories[] = array(
    							'id'        => $category->term_id,
    							'slug'      => $category->slug,
    							'label'     => $category->name,
    							'permalink' => get_term_link( $category ),
    						);
    					}
    				}

    				// need to disable processnig to make sure get_thumbnail() doesn't generate errors
    				$et_fb_processing_shortcode_object = false;

    				// Get thumbnail
    				$thumbnail = get_thumbnail( $width, $height, $classtext, $titletext, $titletext, false, 'Blogimage' );

    				$et_fb_processing_shortcode_object = $global_processing_original_value;

    				// Append value to query post
    				$query->posts[ $post_index ]->post_permalink 	= get_permalink();
    				$query->posts[ $post_index ]->post_thumbnail 	= print_thumbnail( $thumbnail['thumb'], $thumbnail['use_timthumb'], $titletext, $width, $height, '', false, true );
    				$query->posts[ $post_index ]->post_categories 	= $categories;
    				$query->posts[ $post_index ]->post_class_name 	= array_merge( get_post_class( '', get_the_ID() ), $category_classes );

    				// Append category classes
    				$category_classes = implode( ' ', $category_classes );

    				$post_index++;
    			}
    		}

    		wp_reset_postdata();

    		return $query;
    	}

    	static function get_portfolio_terms( $args = array(), $conditional_tags = array(), $current_page = array() ) {
    		$portfolio = self::get_portfolio_item( $args, $conditional_tags, $current_page );

    		$terms = array();

    		if ( ! empty( $portfolio->posts ) ) {
    			foreach ( $portfolio->posts as $post ) {
    				if ( ! empty( $post->post_categories ) ) {
    					foreach ( $post->post_categories as $category ) {
    						$terms[ $category['slug'] ] = $category;
    					}
    				}
    			}
    		}

    		return $terms;
    	}

    	function shortcode_callback( $atts, $content = null, $function_name ) {
    		$module_id          = $this->shortcode_atts['module_id'];
    		$module_class       = $this->shortcode_atts['module_class'];
    		$fullwidth          = $this->shortcode_atts['fullwidth'];
    		$posts_number       = $this->shortcode_atts['posts_number'];
    		$include_categories = $this->shortcode_atts['include_categories'];
    		$show_title         = $this->shortcode_atts['show_title'];
    		$show_categories    = $this->shortcode_atts['show_categories'];
    		$show_pagination    = $this->shortcode_atts['show_pagination'];
    		$background_layout  = $this->shortcode_atts['background_layout'];
    		$hover_icon          = $this->shortcode_atts['hover_icon'];
    		$zoom_icon_color     = $this->shortcode_atts['zoom_icon_color'];
    		$hover_overlay_color = $this->shortcode_atts['hover_overlay_color'];

    		$module_class = ET_Builder_Element::add_module_order_class( $module_class, $function_name );

    		wp_enqueue_script( 'hashchange' );

    		if ( '' !== $zoom_icon_color ) {
    			ET_Builder_Element::set_style( $function_name, array(
    				'selector'    => '%%order_class%% .et_overlay:before',
    				'declaration' => sprintf(
    					'color: %1$s !important;',
    					esc_html( $zoom_icon_color )
    				),
    			) );
    		}

    		if ( '' !== $hover_overlay_color ) {
    			ET_Builder_Element::set_style( $function_name, array(
    				'selector'    => '%%order_class%% .et_overlay',
    				'declaration' => sprintf(
    					'background-color: %1$s;
    					border-color: %1$s;',
    					esc_html( $hover_overlay_color )
    				),
    			) );
    		}

    		$projects = self::get_portfolio_item( array(
    			'show_pagination'    => $show_pagination,
    			'posts_number'       => $posts_number,
    			'include_categories' => $include_categories,
    			'fullwidth'          => $fullwidth,
    		) );

    		$categories_included = array();
    		ob_start();
    		if( $projects->post_count > 0 ) {
    			while ( $projects->have_posts() ) {
    				$projects->the_post();

    				$category_classes = array();
    				$categories = get_the_terms( get_the_ID(), 'project_category' );
    				if ( $categories ) {
    					foreach ( $categories as $category ) {
    						$category_classes[] = 'project_category_' . urldecode( $category->slug );
    						$categories_included[] = $category->term_id;
    					}
    				}

    				$category_classes = implode( ' ', $category_classes );

    				$main_post_class = sprintf(
    					'et_pb_portfolio_item%1$s %2$s',
    					( 'on' !== $fullwidth ? ' et_pb_grid_item' : '' ),
    					$category_classes
    				);


    					$thumb = '';

    					$width = 'on' === $fullwidth ?  1080 : 9999;
    					$width = (int) apply_filters( 'et_pb_portfolio_image_width', $width );

    					$height = 'on' === $fullwidth ?  9999 : 400;
    					$height = (int) apply_filters( 'et_pb_portfolio_image_height', $height );
    					$classtext = 'on' === $fullwidth ? 'et_pb_post_main_image' : '';
    					$titletext = get_the_title();
    					$permalink = get_permalink();
    					$post_meta = ""; //get_the_term_list( get_the_ID(), 'project_category', '', ', ' );  REMOVED BECAUSE CATEGORY LISTING NOT WORKING
    					$thumbnail = get_thumbnail( $width, $height, $classtext, $titletext, $titletext, false, 'Blogimage' );
    					$thumb = $thumbnail["thumb"];
    					//$thumb = get_the_post_thumbnail(get_the_ID(), array('500', '500') );

                        if('' == $thumb) {
                            $main_post_class = $main_post_class. ' jm-contributing-no-img';
                        }

                        ?>

        				<div id="post-<?php the_ID(); ?>" <?php post_class( $main_post_class ); ?>>

        				<?php
    					if ( '' !== $thumb ) : ?>
    						<a href="<?php echo esc_url( $permalink ); ?>">
    						<?php if ( 'on' !== $fullwidth ) : ?>
                                <div class="postlist_image_block" style="background-image: url( <?php echo get_the_post_thumbnail_url(get_the_ID(), array('500', '500'));  ?> );"></div>
    							<!-- <span class="et_portfolio_image" style="background-image:url()"> -->
    						<?php endif; ?>
    								<?php //print_thumbnail( $thumb, $thumbnail["use_timthumb"], $titletext, $width, $height ); ?>
                                    <?php //echo $thumb;//get_the_post_thumbnail(get_the_ID(), array('500', '500')); ?>
    						<?php if ( 'on' !== $fullwidth ) :

    								$data_icon = '' !== $hover_icon
    									? sprintf(
    										' data-icon="%1$s"',
    										esc_attr( et_pb_process_font_icon( $hover_icon ) )
    									)
    									: '';

    								printf( '<span class="et_overlay%1$s"%2$s></span>',
    									( '' !== $hover_icon ? ' et_pb_inline_icon' : '' ),
    									$data_icon
    								);

    						?>
    							<!-- </span> -->
    						<?php endif; ?>

                            </a>
                            <?php else:
                                $colors = array('#172c4d', '#165547');
                                $rand = rand(0, 1);
                                $bgr_color = $colors[$rand];
                                ?>

                                <a href="<?php echo esc_url( $permalink ); ?>">
                                <div class="postlist_image_block"></div>
                                </a>
    				<?php
    					endif;
    				?>
                    <div class="post-meta">
                        <?php if ( 'on' === $show_categories ) : ?>
        					<p class="project-category"><?php echo $post_meta; ?></p>
        				<?php endif; ?>
        				<?php if ( 'on' === $show_title ) : ?>
        					<h2><a href="<?php echo esc_url( $permalink ); ?>"><?php echo $titletext; ?></a></h2>
        				<?php endif; ?>
                        <div class="jm-user-icon"></div>
                        <?php
                        $public_name = get_post_meta( get_the_ID(), 'contributor_name', true );
                        if (!empty($public_name)): ?>
                            <p>Postat av: <?php echo esc_attr($public_name) ?></p>
                        <?php endif; ?>
                        <p><?php echo get_the_date(); ?></p>
                    </div>
    				</div><!-- .et_pb_portfolio_item -->
    				<?php
    			}
    		}

    		wp_reset_postdata();

    		$posts = ob_get_clean();

    		$categories_included = explode ( ',', $include_categories );
    		$terms_args = array(
    			'include' => $categories_included,
    			'orderby' => 'name',
    			'order' => 'ASC',
    		);
    		$terms = get_terms( 'project_category', $terms_args );

    		$category_filters = '<ul class="clearfix">';
    		$category_filters .= sprintf( '<li class="et_pb_portfolio_filter et_pb_portfolio_filter_all"><a href="#" class="active" data-category-slug="all">%1$s</a></li>',
    			esc_html__( 'All', 'et_builder' )
    		);
    		foreach ( $terms as $term  ) {
    			$category_filters .= sprintf( '<li class="et_pb_portfolio_filter"><a href="#" data-category-slug="%1$s">%2$s</a></li>',
    				esc_attr( urldecode( $term->slug ) ),
    				esc_html( $term->name )
    			);
    		}
    		$category_filters .= '</ul>';

    		$video_background = $this->video_background();
    		$parallax_image_background = $this->get_parallax_image_background();

    		$class = " et_pb_module et_pb_bg_layout_{$background_layout}";

    		$output = sprintf(
    			'<div%5$s class="et_pb_filterable_portfolio et_pb_portfolio %1$s%4$s%6$s%11$s%13$s" data-posts-number="%7$d"%10$s>
    				%14$s
    				%12$s
    				<div class="et_pb_portfolio_filters clearfix">%2$s</div><!-- .et_pb_portfolio_filters -->

    				<div class="et_pb_portfolio_items_wrapper %8$s">
    					<div class="et_pb_portfolio_items">%3$s</div><!-- .et_pb_portfolio_items -->
    				</div>
    				%9$s
    			</div> <!-- .et_pb_filterable_portfolio -->',
    			( 'on' === $fullwidth ? 'et_pb_filterable_portfolio_fullwidth' : 'et_pb_filterable_portfolio_grid clearfix' ),
    			$category_filters,
    			$posts,
    			esc_attr( $class ),
    			( '' !== $module_id ? sprintf( ' id="%1$s"', esc_attr( $module_id ) ) : '' ),
    			( '' !== $module_class ? sprintf( ' %1$s', esc_attr( $module_class ) ) : '' ),
    			esc_attr( $posts_number),
    			('on' === $show_pagination ? '' : 'no_pagination' ),
    			('on' === $show_pagination ? '<div class="et_pb_portofolio_pagination"></div>' : '' ),
    			is_rtl() ? ' data-rtl="true"' : '',
    			'' !== $video_background ? ' et_pb_section_video et_pb_preload' : '',
    			$video_background,
    			'' !== $parallax_image_background ? ' et_pb_section_parallax' : '',
    			$parallax_image_background
    		);

    		return $output;
    	}
    }
    $et_builder_filter_portfolio_2 = new ET_Builder_Module_Filterable_Portfolio2;
    add_shortcode( 'et_pb_filterable_portfolio', array($et_builder_filter_portfolio_2, '_shortcode_callback') );



  }

}
add_action('et_builder_ready', 'ex_divi_child_theme_setup');
