<?php

get_header();

$is_page_builder_used = et_pb_is_pagebuilder_used( get_the_ID() );

?>

<div id="main-content">

<?php
$classes = get_body_class();
if ( in_array('post-type-archive-tribe_events',$classes) ||
	is_singular('tribe_events') ) { ?>

	<div class="jm-calendar-header">
		<div class="jm-blog-header-overlay"><p>Kalendarium</p></div>
	</div>

	<?php
	if ( in_array('post-type-archive-tribe_events',$classes) ||
		tribe_is_past() || tribe_is_upcoming() && !is_tax() ) { ?>
	<?php
	    $events = tribe_get_events( array(
	        'posts_per_page' => 25000,
	        'start_date' => date( 'Y-m-d H:i:s' )
	    ) );
	 ?>
	
<?php } ?>

<?php } ?>

<?php if ( ! $is_page_builder_used ) : ?>

	<div class="container">
		<div id="content-area" class="clearfix">
			<div id="left-area">

<?php endif; ?>

			<?php while ( have_posts() ) : the_post(); ?>

				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

				<?php if ( ! $is_page_builder_used ) : ?>

					<h1 class="entry-title main_title"><?php the_title(); ?></h1>
				<?php
					$thumb = '';

					$width = (int) apply_filters( 'et_pb_index_blog_image_width', 1080 );

					$height = (int) apply_filters( 'et_pb_index_blog_image_height', 675 );
					$classtext = 'et_featured_image';
					$titletext = get_the_title();
					$thumbnail = get_thumbnail( $width, $height, $classtext, $titletext, $titletext, false, 'Blogimage' );
					$thumb = $thumbnail["thumb"];

					if ( 'on' === et_get_option( 'divi_page_thumbnails', 'false' ) && '' !== $thumb )
						print_thumbnail( $thumb, $thumbnail["use_timthumb"], $titletext, $width, $height );
				?>

				<?php endif; ?>

					<div class="entry-content">
					<?php
						the_content();

						if ( ! $is_page_builder_used )
							wp_link_pages( array( 'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'Divi' ), 'after' => '</div>' ) );
					?>
					</div> <!-- .entry-content -->

				<?php
					if ( ! $is_page_builder_used && comments_open() && 'on' === et_get_option( 'divi_show_pagescomments', 'false' ) ) comments_template( '', true );
				?>

				</article> <!-- .et_pb_post -->

			<?php endwhile; ?>

<?php if ( ! $is_page_builder_used ) : ?>

			</div> <!-- #left-area -->

			<?php if ( tribe_is_event() && is_single() ) { ?>
			<div id="sidebar">
				<?php
				//Event meta
				do_action( 'tribe_events_single_event_before_the_meta' );
				tribe_get_template_part( 'modules/meta' );
				do_action( 'tribe_events_single_event_after_the_meta' );
				?>
			</div>
			<?php
			}
			else {
			get_sidebar();
			}
			?>


		</div> <!-- #content-area -->
	</div> <!-- .container -->

<?php endif; ?>

</div> <!-- #main-content -->

<?php get_footer(); ?>
