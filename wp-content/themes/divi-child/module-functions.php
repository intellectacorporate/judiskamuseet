<?php

if ( ! function_exists( 'et_builder_include_post_types_option' ) ) :
function et_builder_include_post_types_option( $args = array() ) {
	$defaults = apply_filters( 'et_builder_include_post_types_defaults', array (
		//'use_terms' => true,
		//'term_name' => 'project_category',
	) );

	$args = wp_parse_args( $args, $defaults );

    $output = "\t" . "<% var et_pb_include_post_type_temp = typeof et_pb_include_post_type !== 'undefined' ? et_pb_include_post_type.split( ',' ) : []; %>" . "\n";
	//$output = 'hej';

	// if ( $args['use_terms'] ) {
	// 	//$types_array = get_terms( $args['term_name'] );
	// } else {

    $type_args = array(
       'public'   => true
    );

	$types_array = get_post_types($type_args);//( apply_filters( 'et_builder_get_post_types_args', 'hide_empty=0' ) );
	// }




	if ( empty( $types_array ) ) {
		$output = '<p>' . esc_html__( "Just nu finns inga innehållstyper.", 'et_builder' ) . '</p>';
	}

	foreach ( $types_array as $type ) {
		$contains = sprintf(
			'<%%= _.contains( et_pb_include_post_type_temp, "%1$s" ) ? checked="checked" : "" %%>',
			esc_html( $type )
		);

		$output .= sprintf(
			'%4$s<label><input type="checkbox" name="et_pb_include_post_type" value="%1$s"%3$s> %2$s</label><br/>',
			esc_attr( $type ),
			esc_html( $type ),
			$contains,
			"\n\t\t\t\t\t"
		);

        $output .= '<label><input type="checkbox" name="et_pb_include_get_post_types" value="'.$type.'">'.$type.'</label><br/>';

	}

	$output = '<div id="et_pb_include_get_post_types">' . $output . '</div>';

	return apply_filters( 'et_builder_include_post_types_option_html', $output );
	//return $output; //$types_array;
}
endif;
