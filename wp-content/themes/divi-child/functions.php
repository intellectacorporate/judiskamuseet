<?php
/**
 * Intellecta divi adaption
 */

/**
* Parent theme style
*/
function my_theme_enqueue_styles() {

    $parent_style = 'parent-style';

    wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' );
    wp_enqueue_script( 'divi-child', get_stylesheet_directory_uri() . '/js/divi-child.js', array( 'jquery' ) );

}
add_action( 'wp_enqueue_scripts', 'my_theme_enqueue_styles' );

/**
 * Including custom made divi modules for judiska museet
 */
// include(get_stylesheet_directory() . '/jm-custom-modules.php');
// include(get_stylesheet_directory() . '/module-functions.php');
// if(file_exists(__DIR__.'/selected_posts_widget.php')) require_once('selected_posts_widget.php');

/**
 * Removing website url field on comment form
 */
function jm_disable_comment_url($fields) {
    unset($fields['url']);
    return $fields;
}
add_filter('comment_form_default_fields','jm_disable_comment_url');

/**
 * Shortcode for listing subpages of a specific page. Displays titel & selected image
 */
add_shortcode('subpage_list', 'subpage_list_shortcode');
function subpage_list_shortcode($atts) {

    $a = shortcode_atts( array(
    'id' => '',
    ), $atts );

    $id = $a['id'];

    $args = array(
        'post_type'      => 'page',
        'post_parent'    => $id,
        'orderby'        => 'title',
	    'order'          => 'ASC',
    );
    $children = get_children( $args );

    $output = '<div class="jm-subpage-list"><ul>';

    foreach ($children as $child) {

        $featured_img_url = get_the_post_thumbnail_url($child->ID, 'et-pb-post-main-image');
        $perm_link = get_permalink($child->ID);

        $title = '';
        if (get_field('puff_title', $child->ID) !='') {
            $title = get_field('puff_title', $child->ID);
        }

        $output .='<li style="background-image: url('. $featured_img_url .')">
            <a href="'. $perm_link .'"> '. $title .'</a>
        </li>';
    }

    $output .='</ul></div>';

    return $output;
}

/**
 * Creates an excerpt if needed; and shortens the manual excerpt as well
 */
function wp_trim_all_excerpt($text) {

 global $post;
    $raw_excerpt = $text;
    if ( '' == $text ) {
       $text = get_the_content('');
       $text = strip_shortcodes( $text );
       $text = apply_filters('the_content', $text);
       $text = str_replace(']]>', ']]&gt;', $text);
    }

 $text = strip_tags($text);
 $excerpt_length = apply_filters('excerpt_length', 15);
 $excerpt_more = apply_filters('excerpt_more', ' ' . '...');
 $text = wp_trim_words( $text, $excerpt_length, $excerpt_more );

 return apply_filters('wp_trim_excerpt', $text, $raw_excerpt);
 }

 remove_filter('get_the_excerpt', 'wp_trim_excerpt');
 add_filter('get_the_excerpt', 'wp_trim_all_excerpt');

 /**
  * Shortcode for share buttons
  */
 add_shortcode('jm_share_buttons', 'jm_share_buttons_shortcode');
 function jm_share_buttons_shortcode(){
     global $post;
     $urlcode=urlencode(get_permalink());
     $excerpt=urlencode(get_the_excerpt());
     $title  =urlencode(get_the_title());

     $output =
     '<div class="jm-share-buttons">
     <p>Dela sidan</p>
     <ul class="clearfix">
         <li class="jm-icons et-social-facebook">
             <a href="https://www.facebook.com/sharer/sharer.php?u='. $urlcode .'" class="icon" title="Facebook" target="_blank">
             </a>
         </li>
         <li class="jm-icons et-social-twitter">
             <a href="https://twitter.com/intent/tweet?url='. $urlcode .'" class="icon" title="Twitter" target="_blank">
             </a>
         </li>
     </ul></div>';

     return $output;
 }

 /**
  * Editor user role needs some admin capabilities
  */
  $roleObject = get_role( 'editor' );
 if (!$roleObject->has_cap( 'edit_theme_options' ) ) {
     $roleObject->add_cap( 'edit_theme_options' );
 }
 if (!$roleObject->has_cap( 'gravityforms_view_entries' ) ) {
     $roleObject->add_cap( 'gravityforms_view_entries' );
 }

/**
 * Search in mobile menu
 */
 if ( wp_is_mobile() ) {
  add_filter('wp_nav_menu_items', 'add_search_form', 10, 2);

    function add_search_form($nav, $args) {
        if( $args->theme_location == 'primary-menu' ) {
            $searchmenuitem = '<li class="ev-header-search">
            <form role="search" method="get" id="searchform" action="'.home_url( '/' ).'">
            <input type="text" placeholder="Sök" name="s" id="s" />
            <button type="submit" class="search-submit">
            <span class="icon_search"></span>
            </button>
            </form>
            </li>';
            $nav = $searchmenuitem.$nav;

            return $nav;
        }
    }
}

 /**
  * Custom translation
  */
  add_action( 'after_setup_theme', function () {
      load_child_theme_textdomain( 'Divi', get_stylesheet_directory() . '/lang/' );
 } );

 /**
  * Shortcode for press releases attachments
  */
  add_shortcode('jm-press', 'jm_press_shortcode');
  function jm_press_shortcode() {
      global $post;

    //attachments
    $attachments[] = 'bilagor_1';
    $attachments[] = 'bilagor_2';
    $attachments[] = 'bilagor_3';
    $attachments[] = 'bilagor_4';
    $attachments[] = 'bilagor_5';

    $output = '<div class="jm-press-attachments">';

    foreach ($attachments as $attachment) {

        if (get_field($attachment) !='') {

            $file = get_field($attachment);

            $output .= '<a  class="jm-arrowed-link" href="'. $file['url'] .'">'. $file['title']. '</a>';
        }

    }
    $output .= '</div>';

    return $output;
  }