<?php
/**
 * Adds ic_linklist_widget widget.
 */
class ic_selected_posts_widget extends WP_Widget {

	/**
	 * Sets up the widgets name etc
	 */
	public function __construct() {
		$widget_ops = array(
			'classname' => 'ic_selected_posts_widget',
			'description' => 'Välj specifik artikel att visa',
		);
		parent::__construct( 'ic_selected_posts_widget', 'IC Utvalt innehåll widget', $widget_ops );
	}

	/**
	 * Outputs the content of the widget
	 *
	 * @param array $args
	 * @param array $instance
	 */
	public function widget( $args, $instance ) {
		global $post;

			echo $args['before_widget'];

			if ( ! empty( $instance['title'] ) ) {
				echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ) . $args['after_title'];
			}
			$selected_post = $instance['selected_post'];
			$class= $instance['template'];		// 'circle-img';		//post-list-full-bgr-img
			$post_id = $selected_post;
			$categories = get_the_category($selected_post);

			if ( ! empty( $categories ) ) {
		    	$category = $categories[0]->name;
			}

			$perma_link = get_permalink($selected_post);
			$img_url = get_the_post_thumbnail_url($selected_post);
			$post_title = get_the_title($selected_post);
			$obj = get_post_type_object( get_post_type($selected_post) );
			$post_type = $obj->labels->singular_name;

			// ha fullhöjdbild på artikeln style="background-image: url(http://localhost/energivarlden/wp-content/uploads/2017/03/P1R1232_beskuren.jpg);"
			echo('<div class="post-list-grid '.$class.'">');
			if ($class ==='post-list-full-bgr-img') {
				$bgr_img = 'style="background-image: url('. $img_url .')"';
			} else {
				$bgr_img ='';
			}

			echo('<article id="post-'.$post_id.'" class="et_pb_post clearfix status-publish format-standard has-post-thumbnail
			hentry" '. $bgr_img .'>
				        <a href="'.$perma_link.'" class="entry-featured-image-url">
				            <div class="postlist_image_block" style="background-image: url('.$img_url.');"></div>
						</a>
				  		<h2 class="entry-title"><a href="'.$perma_link.'">'.$post_title.'</a></h2>');
			if($author_date){
				echo('<p class="post-meta">
					<span class="published">Publicerad 2017-03-11</span> av <span class="author vcard">Sofie Haglund</span>
					</p>');
			}
			echo('<div class="post-content">
					<p>'.get_the_excerpt($selected_post).'</p>
					<a class="more-link" href="'.$perma_link.'">Läs mer</a>
				</div>
				</article> <!-- .et_pb_post -->
	 			</div>');

			echo $args['after_widget'];

	}

	/**
	 * Outputs the options form on admin
	 *
	 * @param array $instance The widget options
	 */
	public function form( $instance ) {
		// outputs the options form on admin
		$title = ! empty( $instance['title'] ) ? $instance['title'] : esc_html__( '', 'ev' );
		$selected_post = ! empty( $instance['selected_post'] ) ? $instance['selected_post'] : esc_html__( '', 'ev' );
		$template = ! empty( $instance['template'] ) ? $instance['template'] : esc_html__( '', 'ev' );
		$author_date = $instance[ 'author_date' ] ? 'true' : 'false';
		?>
		<p>
		<label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php esc_attr_e( 'Title:', 'ev' ); ?></label>
		<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"
			name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
		</p>
		<p>
			<label for="selected_post">
				<?php esc_attr_e( 'Välj artikel:', 'ev' ); ?>
			</label><br>
			<select name="<?php echo(esc_attr( $this->get_field_name( 'selected_post' ) ) ); ?>"  id="<?php echo esc_attr( $this->get_field_id( 'selected_post' ) ); ?>" >

 			<?php

				//list of all posts from some post_types
				$args = array(
					'numberposts' => -1,
					'post_type' => array(
						'page',

					),
					'orderby' => 'title',
					'order' => 'ASC',
					'post_status' => array('publish'),
					//'suppress_filters' => false,
				);

				$posts = get_posts( $args );

				if($posts) {

					foreach( $posts as $p ) {

						// title
						$title = get_the_title( $p->ID );

						// empty
						if( $title === '' ) {
							$title = __('(no title)', 'acf');
						}
						?>
						<option value="<?php echo($p->ID); ?>" <?php echo (($p->ID == $selected_post) ? ('selected="selected" ') : ("")); ?>>
							<?php echo($title); ?>
						</option>
						<?php
					}
				}
				?>
			</select>
		</p>
		<p>
			<label for="template"><?php esc_attr_e( 'Välj utseende', 'ev' ); ?></label><br>
			<select name="<?php echo(esc_attr( $this->get_field_name( 'template' ) ) ); ?>"  id="<?php echo esc_attr( $this->get_field_id( 'template' ) ); ?>" >

				<option value="standard" <?php echo (("standard" == $template) ? ('selected="selected" ') : ("")); ?>>Standard</option>
			</select>
		</p>

		<p>
		    <input class="checkbox" type="checkbox" <?php checked( $instance[ 'author_date' ], 'on' ); ?>
				id="<?php echo $this->get_field_id( 'author_date' ); ?>" name="<?php echo $this->get_field_name( 'author_date' ); ?>" />
		    <label for="<?php echo $this->get_field_id( 'author_date' ); ?>">Visa författare & datum</label>
		</p>


		<?php

	}

	/**
	 * Processing widget options on save
	 *
	 * @param array $new_instance The new options
	 * @param array $old_instance The previous options
	 */
	public function update( $new_instance, $old_instance ) {
		// processes widget options to be saved
		$instance = $old_instance;


		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
		$instance['selected_post'] =  ( ! empty( $new_instance['selected_post'] ) ) ? strip_tags( $new_instance['selected_post'] ) : $old_instance['selected_post'];
		$instance['template'] =  ( ! empty( $new_instance['template'] ) ) ? strip_tags( $new_instance['template'] ) : $old_instance['template'];
		$instance['author_date'] =  $new_instance['author_date'];

		return $instance;

	}
}

// register ic_postlist_widget widget
function register_ic_selected_posts_widget() {
    register_widget( 'ic_selected_posts_widget' );
}
add_action( 'widgets_init', 'register_ic_selected_posts_widget' );
