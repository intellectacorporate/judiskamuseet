

	Att anv�nda FormMail hos F S DATA

Snabbstart:
===========

Vill Du bara anv�nda formmail.cgi f�r att skicka e-post till adresser
inom u5645470.fsdata.se s� beh�ver Du inte g�ra n�gra speciella inst�llningar
eller �ndringar.

Beh�ver Du skicka e-post till andra adresser �n inom u5645470.fsdata.se s�
m�ste Du g�ra extra inst�llningar i filen www/cgi-bin/formmail_settings.pl.
I filen finns beskrivningar av vad som beh�ver g�ras i detalj med
flera exempel.

Detaljer:
=========

Cgi-scriptet FormMail �r ett av v�rldens mest spridda och anv�nda
cgi-script. Det �r dessv�rre ocks� ett program som missbrukats v�ldigt
mycket av sk "spammers", dvs individer som sitter och skickar
skr�ppost till miljoner mottagare �ver hela v�rlden. F�r att hindra
missbruk av formmail.cgi har vi p� F S DATA satt upp FormMail enligt
f�ljande:

	1. formmail.cgi och formmail.pl i www/cgi-bin katalogen �r
           egentligen l�nkar till ett centralt placerat
           script. Filerna g�r inte att �ndra eller skriva �ver.

	2. Inst�llningar f�r respektive FormMail g�rs i en separat
           fil, "formmail_settings.pl". I denna fil st�ller man in
           vilka dom�ner och adresser som �r godk�nda anv�ndare
           respektive mottagare av e-post fr�n FormMail. Filen
           inneh�ller instruktioner och exempel.

Mvh,
F S DATA Support
---------------------------------------------------------- 
   F S DATA                 
   Box 5026                     
   250 05 Helsingborg       Tel 042-197000 
   www.fsdata.se            Fax 042-197001 

   Support                  Tel 0200-387001 
   support@fsdata.se        Fax 0200-387003 
---------------------------------------------------------- 