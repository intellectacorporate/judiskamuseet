# formmail_settings.pl
#
# Denna fil inneh�ller konfigurationsinst�llningar f�r scriptet formmail.cgi
# F�r mer information om formmail.cgi hos F S Data, l�s filen 
# /home/u/u5645470/www/cgi-bin/formmail-README.txt

# @referers anger vilka dom�ner som kan anv�nda scriptet.
# Normalt b�r den enbart inneh�lla u5645470.fsdata.se. Om man har flera dom�ner
# knutna till samma konto kan det dock beh�vas att man l�gger till dessa.
# Exempel:
# 1. @referers = ('u5645470.fsdata.se');
# 2. @referers = ('u5645470.fsdata.se', 'extradom�n.com', 'extradom�n.nu');

  @referers = ('u5645470.fsdata.se', 'www.u5645470.fsdata.se');

# @recipients definierar de e-postadresser och dom�nnamn som e-post kan
# skickas till. Detta m�ste fyllas i korrekt f�r att hindra att n�gon
# missbrukar scriptet f�r att skicka skr�ppost till tusentals mottagare.
#
# Exempel:
# 1. Kan skicka till alla adresser @u5645470.fsdata.se
#    @recipients('@u5645470.fsdata.se');
#
# 2. Kan skicka till alla adresser @u5645470.fsdata.se + bill@gates.com
#    @recipients('@u5645470.fsdata.se', 'bill@gates.com');
#
# F�rs�k att vara restriktiv, dvs ange INTE '@telia.com' eller '@hotmail.com'
# utan ange de exakta mottagaradresserna, tex 'namn.efternamn@telia.com'

  @recipients = ('@u5645470.fsdata.se');


# Nedanst�ende siffra m�ste finnas f�r att scriptet skall fungera, radera inte!
1;
